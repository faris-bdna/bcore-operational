import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/request/mrReq.dart';
import 'package:bcore_inventory_management/models/request/mtoReq.dart';
import 'package:bcore_inventory_management/models/request/II/iiReq.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/request/stockDetailReq.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetMrRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetSRRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetTrReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/GetSalesStockDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/SaveOfResp.dart';
import 'package:bcore_inventory_management/models/response/SalesItem/getItemPriceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/accountsResp.dart';
import 'package:bcore_inventory_management/models/response/getProductGroupResp.dart';
import 'package:bcore_inventory_management/models/response/grn_details_print.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetGRNReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getGRNEntryNobyItemResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:bcore_inventory_management/models/response/forgotUserPin_resp.dart';
import 'package:bcore_inventory_management/models/response/login_resp.dart';
import 'package:bcore_inventory_management/models/response/pendingSIResp.dart';
import 'package:bcore_inventory_management/models/response/resetPassword_resp.dart';
import 'package:bcore_inventory_management/models/response/salesType_resp.dart';
import 'package:bcore_inventory_management/models/response/updateUserPinResp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getAdditionalServicesResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSOListResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/termsAndConditionsResp.dart';
import 'package:bcore_inventory_management/models/saveGrn_resp.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/saveSalesOrderResp.dart';
import 'package:flutter/cupertino.dart';
import 'bCore_api_client.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/getPaymentTypeResp.dart';

class BCRepository {
  final BCApiClient bcApiClient;

  BCRepository({@required this.bcApiClient}) : assert(bcApiClient != null);

  Future<LoginResponse> login({String username, String password}) async {
    return await bcApiClient.login(username, password);
  }


  Future<String> authenticate({
    @required String username,
    @required String password,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    return 'token';
  }

  Future<void> deleteToken() async {
    /// delete from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<bool> hasToken() async {
    /// read from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return false;
  }

  Future<UpdateUserPinResponse> updateUserPin({
    String username,
    String uPin,
  }) async {
    return await bcApiClient.updateUserPin(username, uPin);
  }

  Future<ForgotUserPinResponse> forgotUserPin({
    String username,
  }) async {
    return await bcApiClient.forgotUserPin(username);
  }

  Future<ResetPasswordResponse> resetPassword({
    @required String username,
    @required String password,
  }) async {
    return await bcApiClient.resetPassword(username, password);
  }

  Future<GetCompanyResponse> getCompany({
    @required int mode,
  }) async {
    return await bcApiClient.getCompany(mode);
  }

  Future<GetCommonResp> getDivision({
    @required int companyId,
    @required int mode,
  }) async {
    return await bcApiClient.getDivision(companyId, mode);
  }

  /// Get Item Barcode Master
  Future<GetItemBarcodeMasterResp> getItemBarcodeMaster({String companyId,
    String divisionId,
    String locationId,
    String supplierId,
    String lastFetchDate,
    String recordCount}) async {
    return await bcApiClient.getItemBarcodeMaster(
        companyId, divisionId, locationId, supplierId, lastFetchDate,recordCount);
  }

  /// Get Company Master
  Future<SupplierResponse> getSupplierMaster({String companyId}) async {
    return await bcApiClient.getSupplierMaster(companyId);
  }

  /// Get Currency Master
  Future<GetCommonResp> getCurrencyMaster() async {
    return await bcApiClient.getCurrencyMaster();
  }

  /// Get Company Master
  Future<GetCompanyResponse> getCompanyMaster({int mode}) async {
    return await bcApiClient.getCompanyMaster(mode);
  }

  /// Get Division Master
  Future<GetCommonResp> getDivisionMaster({String companyId, int mode}) async {
    return await bcApiClient.getDivisionMaster(companyId, mode);
  }

  ///Get Origin and Brand Master
  Future<GetCommonResp> getSortedItem({ String brand, String origin}) async {
    return await bcApiClient.getFilterItem(brand, origin);
  }


  /// Get Location Master
  Future<GetCommonResp> getLocationMaster(
      {String companyId, String divisionId, int mode}) async {
    return await bcApiClient.getLocationMaster(companyId, divisionId, mode);
  }


  /// Get Product Group
  Future<GetProductGroupResp> getProductGroup() async {
    return await bcApiClient.getProductGroup();
  }

  /// Get Department Master
  Future<GetCommonResp> getDepartmentMaster(
      {String companyId, int mode}) async {
    return await bcApiClient.getDepartmentMaster(companyId, mode);
  }

  /// Get Item Packing Master
  Future<GetItemPackingMasterResp> getItemPackingMaster({String companyId,
    String divisionId,
    String locationId,
    String supplierId,
    String recordCount,
    String lastFetchDate}) async {
    return await bcApiClient.getItemPackingMaster(
        companyId, divisionId, locationId, supplierId, recordCount,lastFetchDate);
  }

  /// Get Item Batch Master
  Future<GetItemBatchMasterResp> getItemBatchMaster({String companyId,
    String divisionId,
    String locationId,
    String supplierId,
    String recordCount,
    String lastFetchDate}) async {
    return await bcApiClient.getItemBatchMaster(
        companyId, divisionId, locationId, supplierId,recordCount, lastFetchDate);
  }

  /// Get Item Batch Master
  Future<GetCommonResp> getCommonMaster(ScmMasters scmMaster) async {
    return await bcApiClient.getCommonResp(scmMaster);
  }

  /// Save Material Request
  Future<MRResp> sendMaterialRequest(MRReq materialReq) async {
    return await bcApiClient.sendMaterialRequest(materialReq);
  }

  ///Save Store Issue
  Future<MRResp> saveStoreIssue(SIReq siReq) async {
    return await bcApiClient.saveStoreIssue(siReq);
  }

  ///Save Internal Issue
  Future<MRResp> sendInternalIssue(SaveIIReq iiReq) async {
    return await bcApiClient.sendInternalIssue(iiReq);
  }

  ///Get Pending Store Issue
  Future<PendingSITOResp> getPendingSI(String companyId,
      String divisionId,
      String locationId,) async {
    return await bcApiClient.getPendingSI(companyId, divisionId, locationId);
  }

  ///Save Material Transfer out
  Future<MRResp> saveTransferOut(MTOReq mtoReq) async {
    return await bcApiClient.saveTransferOut(mtoReq);
  }

  /// Get Projects Master
  Future<GetCommonResp> getProjectsMaster({String locationId}) async {
    return await bcApiClient.getProjectMaster(locationId);
  }

  /// Save GRN
  Future<SaveGrnResp> postGrn({String grnReq}) async {
    return await bcApiClient.saveGrn(grnReq);
  }

  Future<SaveGrnResp> postPr({String prReq}) async {
    return await bcApiClient.savePr(prReq);
  }

  Future<GetCommonResp> getWareHouse() async {
    return await bcApiClient.getWarehouseMaster();
  }

  Future<GetCommonResp> getWareHouseZone({
    @required String warehouseId
  }) async {
    return await bcApiClient.getWarehouseZoneMaster(warehouseId);
  }

  Future<GetCommonResp> getWareHouseRack({
    @required String zoneId
  }) async {
    return await bcApiClient.getWarehouseRackMaster(zoneId);
  }

  Future<GetWarehouseBinMasterResp> getWareHouseBin({
    @required String rackId
  }) async {
    return await bcApiClient.getWarehouseBinMaster(rackId);
  }

  // Get Packing Details Master
  Future<GetScmItemPriceDetailsResp> getScmItemPriceDetails({
    TransactionType type,
    String companyId,
    String divisionId,
    String locationId,
    String toCompanyId,
    String toDivisionId,
    String toLocationId,
    String supplierId,
    String itemId,
    String packingId}) async {
    return await bcApiClient.getScmItemPriceDetails(type: type,
        companyId: companyId,
        divisionId: divisionId,
        locationId: locationId,
        toCompanyId: toCompanyId,
        toDivisionId: toDivisionId,
        toLocationId: toLocationId,
        supplierId: supplierId,
        itemId: itemId,
        packingId: packingId);
  }

  Future<GetCommonResp> getPriceType() async {
    return await bcApiClient.getPriceType();
  }

  Future<GetCommonResp> getReason() async {
    return await bcApiClient.getReason();
  }

  Future<GetGRNEntryNobyItemResp> getGRNEntryNobyItem({String req}) async {
    return await bcApiClient.getGRNEntryNobyItem(req);
  }

  Future<ReferencePOGrnResp> getPendingPurchaseOrder({String grnType}) async {
    return await bcApiClient.getPendingPurchaseOrder(grnType);
  }

  Future<GetGRNReferenceDetailsResp> getGRNReferenceDetails(
      {String refTypeId, String refId}) async {
    return await bcApiClient.getGRNReferenceDetails(refTypeId, refId);
  }

  Future<ReferencePOGrnResp> getGRNEntryNo() async {
    return await bcApiClient.getGRNEntryNo();
  }
  Future<GRNDetails> getGRNEntryNoDetails(String grnNumb) async {
    return await bcApiClient.getGRNEntryNoDetails(grnNumb);
  }

  Future<GetPRReferenceDetailsResp> getPurchaseReturnReferenceDetails(
      {String refId}) async {
    return await bcApiClient.getPurchaseReturnReferenceDetails(refId);
  }

  Future<GetStockReferenceNoResp> getStockReferenceNo() async {
    return await bcApiClient.getStockReferenceNo();
  }

  Future<GetStockReferenceDetailsResp> getStockReferenceDetails({
    @required String refId
  }) async {
    return await bcApiClient.getStockReferenceDetails(refId);
  }

  Future<SaveGrnResp> saveStockTaking({
    @required SaveSTReq req
  }) async {
    return await bcApiClient.saveStockTaking(req);
  }

  Future<SaveGrnResp> saveOpeningBalance({
    @required SaveOBReq req
  }) async {
    return await bcApiClient.saveOpeningBalance(req);
  }

  Future<SalesTypeResponse> getSalesType() async {
    return await bcApiClient.getSalesType();
  }

  Future<GetCommonResp> getAccountType() async {
    return await bcApiClient.getAccountType();
  }

  Future<AccountsResp> getAccounts({int contractType, int companyId}) async {
    return await bcApiClient.getAccounts(contractType, companyId);
  }

  Future<GetCommonResp> getSalesman(
      {int clientTypeID, int customerId, int companyId}) async {
    return await bcApiClient.getSalesman(clientTypeID, customerId, companyId);
  }

  Future<GetItemPriceDetailsResp> getItemPriceDetails({
    int custContactTypeId,
    int refContactId,
    int itemId,
    int packingId}) async {
    return await bcApiClient.getItemPriceDetails(
        custContactTypeId, refContactId, itemId, packingId);
  }

  Future<SaveSalesResp> saveSAI({String saiReq}) async {
    return await bcApiClient.saveSAI(saiReq);
  }

  Future<GetPaymentTypeResp> getPaymentType() async {
    return await bcApiClient.getPaymentType();
  }

  Future<GetAdditionalServicesResp> getAdditionalServices() async {
    return await bcApiClient.getAdditionalServicesList();
  }

  Future<TermsAndConditionsResp> getTermsAndConditionList() async {
    return await bcApiClient.getTermsAndConditionList();
  }

  Future<GetPaymentTypeResp> getPaymentSubType({String id}) async {
    return await bcApiClient.getPaymentSubType(id);
  }

  Future<GetSalesStockDetailsResp> getSalesStockDetails(
      {StockDetailReq stockDetailReq}) async {
    return await bcApiClient.getSalesStockDetails(stockDetailReq);
  }

  Future<SaveOFResp> saveOF({String ofReq}) async {
    return await bcApiClient.saveOF(ofReq);
  }

  Future<GetSRRefDetailsResp> getStoreReceiptReferenceDetails(
      {String refId}) async {
    return await bcApiClient.getStoreReceiptReferenceDetails(refId);
  }

  ///Save Store Receipt
  Future<MRResp> saveStoreReceipt(String req) async {
    return await bcApiClient.saveStoreReceipt(req);
  }

  Future<SaveSalesResp> salesOrder(String req) async {
    return await bcApiClient.salesOrder(req);
  }

  Future<GetSOListResp> getSalesOrderList() async {
    return await bcApiClient.getSalesOrderList();
  }

  Future<GetSODetailsResp> getSalesOrderDetails(int salesOrderId) async {
    return await bcApiClient.getSalesOrderDetails(salesOrderId);
  }

  Future<SaveSalesResp> saveDN(String req) async {
    return await bcApiClient.saveDN(req);
  }

  Future<PendingSITOResp> getPendingTransferOut(String req) async {
    return await bcApiClient.getPendingTransferOut(req);
  }

  Future<GetTrReferenceDetailsResp> getTransferReceiptReferenceDetails(
      {String refId}) async {
    return await bcApiClient.getTransferReceiptReferenceDetails(refId);
  }

  Future<MRResp> saveTransferReceipt(String req) async {
    return await bcApiClient.saveTransferReceipt(req);
  }

  Future<PendingSITOResp> getPendingMaterialRequest(
      TransactionType type) async {
    return await bcApiClient.getPendingMaterialRequest(type: type);
  }

  Future<GetMrRefDetailsResp> getMrRefDetails(
      {String refId, TransactionType type}) async {
    return await bcApiClient.getMaterialRequestReferenceDetails(refId, type);
  }
}
