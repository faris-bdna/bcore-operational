
import 'dart:convert';
import 'dart:io';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/models/request/stockDetailReq.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetMrRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetSRRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetTrReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/GetSalesStockDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/SaveOfResp.dart';
import 'package:bcore_inventory_management/models/response/getProductGroupResp.dart';
import 'package:bcore_inventory_management/models/response/grn_details_print.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSOListResp.dart';
import 'package:bcore_inventory_management/models/view/CommonCRMSaveResp.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/request/mrReq.dart';
import 'package:bcore_inventory_management/models/request/mtoReq.dart';
import 'package:bcore_inventory_management/models/request/II/iiReq.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart';
import 'package:bcore_inventory_management/models/response/SalesItem/getItemPriceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/accountsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetGRNReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/models/response/forgotUserPin_resp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getGRNEntryNobyItemResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/login_resp.dart';
import 'package:bcore_inventory_management/models/response/pendingSIResp.dart';
import 'package:bcore_inventory_management/models/response/resetPassword_resp.dart';
import 'package:bcore_inventory_management/models/response/salesType_resp.dart';
import 'package:bcore_inventory_management/models/response/updateUserPinResp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getAdditionalServicesResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/termsAndConditionsResp.dart';
import 'package:bcore_inventory_management/models/saveGrn_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:http/http.dart' as http;
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/saveSalesOrderResp.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/getPaymentTypeResp.dart';

class BCApiClient {
  final _baseAPIUrl = "${Constants().baseUrl}/api";

  Map<String, String> headers = {"Content-type": "application/json"};
  final http.Client httpClient;

  BCApiClient({@required this.httpClient}) : assert(httpClient != null);

  Future<LoginResponse> login(String username, String password) async {
    var authn = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

    var data = {
      'scope':'MobileApp',
      'grant_type': 'password',
      'username': '$username',
      'password': '$password',
    };

    final url = '$_baseAPIUrl/User/Login';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: {'Authorization': authn,}, body: data);


    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);

      return LoginResponse.fromJson(body);
    }

    else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Update User PIN
  Future<UpdateUserPinResponse> updateUserPin(
      String username, String uPin) async {
    final url = '$_baseAPIUrl/User/UpdateUserPin';
    String req = '{"Username" : "$username", "UPin" : "$uPin"}';
    final response =
        await this.httpClient.post(Uri.parse(url), headers: headers, body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return UpdateUserPinResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Forgot User PIN
  Future<ForgotUserPinResponse> forgotUserPin(String username) async {
    final url = '$_baseAPIUrl/UserVerification/ForgotPassword';

    String req = '{"Username" : "$username"}';
    final response =
        await this.httpClient.post(Uri.parse(url), headers: headers, body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return ForgotUserPinResponse.fromJson(body);
    } else if (response.statusCode == 404) {
      final body = jsonDecode(response.body);
      return ForgotUserPinResponse.fromJson(body);
    } else {
//      final body = jsonDecode(response.body);
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Reset Password
  Future<ResetPasswordResponse> resetPassword(
      String username, String password) async {
    final url = '$_baseAPIUrl/UserVerification/ResetPassword';
    String req = '{"Username" : "$username", "Password" : "$password"}';
    final response =
        await this.httpClient.post(Uri.parse(url), headers: headers, body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return ResetPasswordResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetCompanyResponse> getCompany(int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");

    final url = '$_baseAPIUrl/SCMMasters/GetCompany';
    String req = '{"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return GetCompanyResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetCommonResp> getDivision(int companyId, int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");

    final url = '$_baseAPIUrl/SCMMasters/GetDivision';
    String req =
        '{"CompanyId" : $companyId, "${Constants.accessMode}" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Map<String, String> getAuthorizedHeader(String token) => {
    HttpHeaders.authorizationHeader: 'Bearer $token',
    HttpHeaders.contentTypeHeader: 'application/json'
  };
  Map<String, String> getHeader(String token) => {
    HttpHeaders.authorizationHeader: 'Bearer $token'
  };

  // Get Item Barcode Master
  Future<GetItemBarcodeMasterResp> getItemBarcodeMaster(
      String companyId,
      String divisionId,
      String locationId,
      String supplierId,
      String lastFetchDate,
      String recordCount) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetItemBarcode';
    String req = '{"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"LocationId" : "$locationId", '
        '"SupplierId" : "$supplierId", '
        '"LastFetchDate" : "",'
        '"RecordCount" : "$recordCount"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetItemBarcodeMasterResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  /// Get Supplier Master
  Future<SupplierResponse> getSupplierMaster(String companyId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String lastFetchDate = Utils.getDateByFormat(
        format: fromAddress.fromCompanyDateFormat,
        passedDateTime: DateFormat("dd/MM/yyyy").parse("01/01/2020"));
    final url = '$_baseAPIUrl/SCMMasters/GetSupplier';
    // String req = '{"CompanyId" : "$companyId", "LastFetchDate" : "$lastFetchDate"}';
    String req = '{"CompanyId" : "$companyId", "LastFetchDate" : ""}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return SupplierResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Currency Master
  Future<GetCommonResp> getCurrencyMaster() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetCurrency';
    //String req = '{"CompanyId" : "$companyId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), /*body: req*/);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Company Master
  Future<GetCompanyResponse> getCompanyMaster(int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetCompany';
    String req = '{"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCompanyResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Division Master
  Future<GetCommonResp> getDivisionMaster(String companyId, int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetDivision';
    String req = '{"CompanyId" : "$companyId", '
        '"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Department Master
  Future<GetCommonResp> getDepartmentMaster(String companyId, int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetDepartment';
    String req = '{"CompanyId" : "$companyId", '
        '"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Location Master
  Future<GetCommonResp> getLocationMaster(
      String companyId, String divisionId, int mode) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetLocation';
    String req = '{"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  /// Get Product Group
  Future<GetProductGroupResp> getProductGroup() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/GetProductGroupMaster';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token));

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetProductGroupResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Item Packing Master
  Future<GetItemPackingMasterResp> getItemPackingMaster(
      String companyId,
      String divisionId,
      String locationId,
      String supplierId,
      String recordCount,
      String lastFetchDate) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetItemPacking';
    String req = '{"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"LocationId" : "$locationId", '
        '"SupplierId" : "$supplierId", '
        '"LastFetchDate" : "",'
        '"RecordCount" : "$recordCount"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetItemPackingMasterResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Item Batch Master
  Future<GetItemBatchMasterResp> getItemBatchMaster(
      String companyId,
      String divisionId,
      String locationId,
      String supplierId,
      String recordCount,
      String lastFetchDate) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetItemBatch';
    String req = '{"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"LocationId" : "$locationId", '
        '"SupplierId" : "$supplierId", '
        '"LastFetchDate" : "",'
        '"RecordCount" : "$recordCount"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetItemBatchMasterResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Warehouse Master
  Future<GetCommonResp> getWarehouseMaster() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetWareHouse';
//    String req = '{"CompanyId" : "$companyId", '
//        '"Mode" : "$mode"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token));

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Warehouse Zone Master
  Future<GetCommonResp> getWarehouseZoneMaster(String warehouseId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetWareHouseZone';
    String req = '{"WarehouseId" : "$warehouseId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Warehouse Rack Master
  Future<GetCommonResp> getWarehouseRackMaster(String zoneId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetWareHouseRack';
    String req = '{"ZoneId" : "$zoneId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Warehouse Bin Master
  Future<GetWarehouseBinMasterResp> getWarehouseBinMaster(String rackId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetWareHouseBin';
    String req = '{"RackId" : "$rackId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetWarehouseBinMasterResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Scale Group Master
  Future<GetCommonResp> getScaleGroupMaster(
      String clientId,
      String dtRoleId,
      String companyId,
      String divisionId,
      String locationId,
      String referenceId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/StockTaking/GetScaleGroup';
    String req = '{"ClientId" : "$clientId", '
        '"DtRoleId" : "$dtRoleId", '
        '"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"LocationId" : "$locationId", '
        '"ReferenceId" : "$referenceId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get  Response based on Origin and brand
  Future<GetCommonResp> getFilterItem(var scmMasterUri, String origin) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    var url = '';
    String req = '';
    switch (scmMasterUri) {
      case ScmMasters.ProductBrand:
        SharedPreferences pref = await SharedPreferences.getInstance();
        String lastFetchDate = pref.getString("lastFetchDate") ?? DateTime.now().toString();
        url = '$_baseAPIUrl/SCMMasters/GetProductBrand';
        req = '{"LastFetchDate": ""}';
        break;
      case ScmMasters.CountryOfOrigin:
        url = '$_baseAPIUrl/SCMMasters/GetCountryOfOrigin';
        break;
      default:
        url = '';
    }
    if (url.isEmpty) {
      throw new Exception('Uri passed empty');
    } else {
      final response = await this
          .httpClient
          .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

      if ((response.statusCode == 200) || (response.statusCode == 400)) {
        final body = jsonDecode(response.body);
        return GetCommonResp.fromJson(body);
      } else {
        throw new Exception(response.reasonPhrase);
      }
    }
    }

    /// Get Common Response based on passed Uri
  Future<GetCommonResp> getCommonResp(var scmMasterUri) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    var url = '';
    String req = '';

    switch (scmMasterUri) {
      case ScmMasters.ProductStockType:
        url = '$_baseAPIUrl/SCMMasters/GetProductStockType';
        break;
      case ScmMasters.ProductBrand:
        SharedPreferences pref = await SharedPreferences.getInstance();
        String lastFetchDate = pref.getString("lastFetchDate") ?? DateTime.now().toString();
        url = '$_baseAPIUrl/SCMMasters/GetProductBrand';
        req = '{"LastFetchDate": ""}';
        break;
      case ScmMasters.CountryOfOrigin:
        url = '$_baseAPIUrl/SCMMasters/GetCountryOfOrigin';
        break;
      case ScmMasters.DeliveryPriority:
        url = '$_baseAPIUrl/SCMMasters/GetDeliveryPriority';
        break;
      case ScmMasters.ClientUsers:
        url = '$_baseAPIUrl/SCMMasters/GetClientUsers';
        break;
      case ScmMasters.GrnType:
        url = '$_baseAPIUrl/SCMMasters/GetGRNType';
        break;
      case ScmMasters.ServiceCategory:
        url = '$_baseAPIUrl/SCMMasters/GetServiceCategory';
        break;
      case ScmMasters.Currency:
        url = '$_baseAPIUrl/SCMMasters/GetCurrency';
        break;
      case ScmMasters.Unit:
        url = '$_baseAPIUrl/SCMMasters/GetUnit';
        break;
      case ScmMasters.PurchaseReturnType:
        url = '$_baseAPIUrl/SCMMasters/GetPurchaseReturnType';
        break;
      case ScmMasters.PriceType:
        url = '$_baseAPIUrl/SCMMasters/GetPriceType';
        break;
      case ScmMasters.ConsignmentType:
        url = '$_baseAPIUrl/SCMMasters/GetConsignmentType';
        break;
      case ScmMasters.JobOrderType:
        url = '$_baseAPIUrl/SCMMasters/GetJobOrderType';
        break;
      case ScmMasters.ModuleSettings:
        url = '$_baseAPIUrl/SCMMasters/GetModuleSettings';
        break;
      default:
        url = '';
    }
    if (url.isEmpty) {
      throw new Exception('Uri passed empty');
    } else {
      final response = await this
          .httpClient
          .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

      if ((response.statusCode == 200) || (response.statusCode == 400)) {
        final body = jsonDecode(response.body);
        return GetCommonResp.fromJson(body);
      } else {
        throw new Exception(response.reasonPhrase);
      }
    }
  }

  /// Send Material RequestmaterialReq
  Future<MRResp> sendMaterialRequest(MRReq materialReq) async {
      final url = '$_baseAPIUrl/SCMTransaction/SaveMaterialRequest';
      SharedPreferences pref = await SharedPreferences.getInstance();
      String token = pref.getString("Token");
      String apContent = jsonEncode(materialReq.toJson());
      final response =
      await this.httpClient.post(
          Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

      if ((response.statusCode == 200)) {
        final body = jsonDecode(response.body);
        return MRResp.fromJson(body);
      } else {
        throw new Exception(MRResp.fromJson(jsonDecode(
            response.body)).validationDetails.errorDetails[0].errorMessageDescription);

      }
    }

  /// Send Store Issue
  Future<MRResp> saveStoreIssue(SIReq siReq) async {
    final url = '$_baseAPIUrl/SCMTransaction/SaveStoreIssue';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String apContent = jsonEncode(siReq);
    final response =
    await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return MRResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Send Internal Issue
  Future<MRResp> sendInternalIssue(SaveIIReq iiReq) async {
    final url = '$_baseAPIUrl/SCMTransaction/SaveInternalIssue';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String apContent = jsonEncode(iiReq);
    final response =
    await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return MRResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Get Pending Store Issue
  Future<PendingSITOResp> getPendingSI(
      String companyId,
      String divisionId,
      String locationId) async {
    final url = '$_baseAPIUrl/SCMMasters/GetPendingStoreIssue';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req = '{'
        '"CompanyId" : "$companyId", '
        '"DivisionId" : "$divisionId", '
        '"LocationId" : "$locationId"'
        '}';

    final response =
    await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return PendingSITOResp.fromJson(body);
    } else {
      throw new Exception(PendingSITOResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Send material Transfer Out
  Future<MRResp> saveTransferOut(MTOReq mtoReq) async {
    final url = '$_baseAPIUrl/SCMTransaction/SaveTransferOut';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String apContent = jsonEncode(mtoReq);
    final response =
    await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return MRResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Get Projects Master
  Future<GetCommonResp> getProjectMaster(String locationId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetProject';
    String req = '{"LocationId" : "$locationId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Save GRN
  Future<SaveGrnResp> saveGrn(String grnReq) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/SaveGRN';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: grnReq);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveGrnResp.fromJson(body);
    } else {
      throw new Exception(SaveGrnResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Get Item Packing Details
  Future<GetScmItemPriceDetailsResp> getScmItemPriceDetails({
      TransactionType type,
      String companyId, 
      String divisionId,
      String locationId,
      String toCompanyId,
      String toDivisionId,
      String toLocationId,
      String supplierId,
      String itemId,
      String packingId}) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetItemPriceDetails';

    String req;
    if (type == TransactionType.MR){
      req = '{"TranName" : "MR",'
          '"CompanyId" : "$companyId", '
          '"DivisionId" : "$divisionId", '
          '"LocationId" : "$locationId", '
          '"ToCompanyId" : "$toCompanyId", '
          '"ToDivisionId" : "$toDivisionId", '
          '"ToLocationId" : "$toLocationId", '
          '"ItemId" : "$itemId",'
          '"PackingId" : "$packingId"}'; /// No need to add supplier
    } else if (type == TransactionType.GRN || type == TransactionType.PR){
      req = '{"TranName" : "GRN",'
          '"CompanyId" : "$companyId", '
          '"DivisionId" : "$divisionId", '
          '"LocationId" : "$locationId", '
          '"SupplierId" : "$supplierId", '
          '"ItemId" : "$itemId",'
          '"PackingId" : "$packingId"}';
    }
    else{
      String tranName;
          if (type == TransactionType.II)  tranName = "IIS";
          else if (type == TransactionType.SI)  tranName = "SI";
          else if (type == TransactionType.MTO)  tranName = "TO";
          else tranName = "MR";
          /// If required, pass tranName = ST for Stock Taking

      req = '{"TranName" : "$tranName",'
          '"CompanyId" : "$companyId",'
          '"DivisionId" : "$divisionId",'
          '"LocationId" : "$locationId",'
          '"ToCompanyId" : "$toCompanyId",'
          '"ToDivisionId" : "$toDivisionId",'
          '"ToLocationId" : "$toLocationId",'
          '"ItemId" : "$itemId",'
          '"PackingId" : "$packingId"}';
    }

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetScmItemPriceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetCommonResp> getPriceType() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetPriceType';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token));

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetCommonResp> getReason() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetReason';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token));

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetGRNEntryNobyItemResp> getGRNEntryNobyItem(String req) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetGRNEntryNobyItem';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetGRNEntryNobyItemResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  /// Save PR
  Future<SaveGrnResp> savePr(String prReq) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/SavePurchaseReturn';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: prReq);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveGrnResp.fromJson(body);
    } else {
      throw new Exception(SaveGrnResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// GetPendingPurchaseOrder
  Future<ReferencePOGrnResp> getPendingPurchaseOrder(String grnType) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    // String req = '{"GRNType" : "$grnType"}';

    String req = '{"GRNType" : "$grnType", "CompanyId" : "${FromAddress.shared.fromCompanyId}",'
        '"DivisionId" : "${FromAddress.shared.fromDivisionId}",'
        '"LocationId" : "${FromAddress.shared.fromLocationId}"}';
    print(req);
    final url = '$_baseAPIUrl/SCMMasters/GetPendingPurchaseOrder';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return ReferencePOGrnResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetGRNReferenceDetailsResp> getGRNReferenceDetails(String refTypeId, String refId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req = '{"ReferenceTypeId" : "$refTypeId","ReferenceId" : "$refId"}';
    final url = '$_baseAPIUrl/SCMTransaction/GetGRNReferenceDetails';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      print("GetGRNReferenceDetails resp: $body");
      return GetGRNReferenceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<ReferencePOGrnResp> getGRNEntryNo() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req = '{"CompanyId" : "${FromAddress.shared.fromCompanyId}",'
        '"DivisionId" : "${FromAddress.shared.fromDivisionId}",'
        '"LocationId" : "${FromAddress.shared.fromLocationId}"}';
    final url = '$_baseAPIUrl/SCMMasters/GetGRNEntryNo';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return ReferencePOGrnResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  Future<GRNDetails> getGRNEntryNoDetails(String grnNumb) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req = '{"ReferenceNo" : "$grnNumb"}';
    final url = '$_baseAPIUrl/SCMTransaction/GetGRNItemDetailsbyEntryNo';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GRNDetails.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetPRReferenceDetailsResp> getPurchaseReturnReferenceDetails(String refId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/GetPurchaseReturnReferenceDetails';
    String req = '{"ReferenceId" : "$refId"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print("GetPurchaseReturnReferenceDetails resp: $body");
      return GetPRReferenceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// GetStockReferences
  Future<GetStockReferenceNoResp> getStockReferenceNo() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req =
        '{"CompanyId" : "${fromAddress.fromCompanyId}","DivisionId" : "${fromAddress.fromDivisionId}","LocationId" : "${fromAddress.fromLocationId}"}';
    final url = '$_baseAPIUrl/SCMMasters/GetStockReferenceNo';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      print("GetStockReferenceNoResp resp: $body");
      return GetStockReferenceNoResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  Future<GetStockReferenceDetailsResp> getStockReferenceDetails(String refId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/GetStockReferenceDetails';
    String req = '{"ReferenceId" : "$refId", "RecordCount" : "0"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print("GetStockReferenceDetails resp: $body");
      return GetStockReferenceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  ///  Save Store Taking
  Future<SaveGrnResp> saveStockTaking(SaveSTReq req) async {
    final url = '$_baseAPIUrl/SCMTransaction/SaveStockTaking';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String apContent = jsonEncode(req);
    final response =
    await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return SaveGrnResp.fromJson(body);
    } else {
      throw new Exception(SaveGrnResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }


  ///  Save Store Taking
  Future<SaveGrnResp> saveOpeningBalance(SaveOBReq req) async {
    final url = '$_baseAPIUrl/SCMTransaction/SaveOpeningBalance';
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String apContent = jsonEncode(req);
    final response = await this.httpClient.post(
        Uri.parse(url), headers: getAuthorizedHeader(token), body: apContent);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return SaveGrnResp.fromJson(body);
    } else {
      throw new Exception(SaveGrnResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Get Sales type
  Future<SalesTypeResponse> getSalesType() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/Basic/GetSalesTypeDropDownForMobileApp';
    final response = await this
        .httpClient
        .get(Uri.parse(url), headers: getHeader(token));
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return SalesTypeResponse.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Account type
  Future<GetCommonResp> getAccountType() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/Basic/GetBasicMastersForMobileApp';
    String req = '{ "isAddNew": true ,"datafor": "AccountClientType"}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Accounts of selected Account type
  Future<AccountsResp> getAccounts(int contractType, int companyId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/ContractsServices/GetAllClientsByType';
    String req = '{"ContractType": $contractType,"CompanyId": $companyId}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return AccountsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get Salesman
  Future<GetCommonResp> getSalesman(int clientTypeID,int customerId, int companyId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '${Constants().baseUrl}/Company/GetSalesmanForMobileApp';
    String req ='{"ClientTypeID": $clientTypeID,"CustomerId": $customerId,"CompanyId": $companyId}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetCommonResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get ItemPriceDetails
  Future<GetItemPriceDetailsResp> getItemPriceDetails(
      int custContactTypeId,
      int refContactId,
      int itemId,
      int packingId) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/ItemMaterials/GetItemPriceDetails';
    String req =
        '{"CompanyId": ${fromAddress.fromCompanyId ?? ""},"DivisionId": ${fromAddress.fromDivisionId ?? ""},'
        '"LocationId": ${fromAddress.fromLocationId ?? ""},"CustContactTypeId": $custContactTypeId,'
        '"RefContactId": $refContactId,"ItemId": $itemId,"PackingId": $packingId}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetItemPriceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Save SAI
  Future<SaveSalesResp> saveSAI(String req) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SalesInvoice/SaveSalesInvoice';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveSalesResp.fromJson(body);
    } else {
      throw new Exception(CommonCRMSaveResp.fromJson(jsonDecode(
          response.body)).errorDetails[0].errorMessageDescription);
    }
  }

  /// Get PaymentType
  Future<GetPaymentTypeResp> getPaymentType() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/PaymentType/GetPaymentType';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token));
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetPaymentTypeResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get AdditionalServicesList
  Future<GetAdditionalServicesResp> getAdditionalServicesList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/AdditionalServices/GetAdditionalServicesList';
    String req ='{"CompanyId": ${fromAddress.fromCompanyId}, "DivisionId": ${fromAddress.fromDivisionId}}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetAdditionalServicesResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get TermsAndConditionList
  Future<TermsAndConditionsResp> getTermsAndConditionList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/TermsAndConditions/GetTermsAndConditionList';
    String req ='{"CompanyId": ${fromAddress.fromCompanyId}}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return TermsAndConditionsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get PaymentSubType
  Future<GetPaymentTypeResp> getPaymentSubType(String id) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/PaymentType/GetPaymentSubType';
    String req ='{"Id": "$id"}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);
    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetPaymentTypeResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get ItemPriceDetails
  Future<GetSalesStockDetailsResp> getSalesStockDetails(StockDetailReq stockDetailReq) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/GetSalesStockDetails';

    String req =jsonEncode(stockDetailReq.toJson());

    print(req);
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if ((response.statusCode == 200) || (response.statusCode == 400)) {
      final body = jsonDecode(response.body);
      return GetSalesStockDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  /// Save OF
  Future<SaveOFResp> saveOF(String ofReq) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/SaveOrderForm';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: ofReq);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveOFResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  Future<GetSRRefDetailsResp>getStoreReceiptReferenceDetails(String refId) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");

    final url = '$_baseAPIUrl/SCMTransaction/GetStoreReceiptReferenceDetails';
    String req =
        '{"ReferenceId" : $refId}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if ((response.statusCode == 200)) {
      final body = jsonDecode(response.body);
      return GetSRRefDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Save SR
  Future<MRResp> saveStoreReceipt(String req) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/SaveStoreReceipt';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return MRResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Save SO
  Future<SaveSalesResp> salesOrder(String soReq) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SalesOrder/SaveSalesOrder';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: soReq);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveSalesResp.fromJson(body);
    } else {
      throw new Exception(CommonCRMSaveResp.fromJson(jsonDecode(
          response.body)).errorDetails[0].errorMessageDescription);
    }
  }


  /// Get SalesOrderList
  Future<GetSOListResp> getSalesOrderList() async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SalesOrder/GetSalesOrderList';
    String req = '{"CompanyId" : "${fromAddress.fromCompanyId}", '
        '"DivisionId" : "${fromAddress.fromDivisionId}", "LocationId" : "${fromAddress.fromLocationId}"}';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return GetSOListResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get SalesOrderDetails
  Future<GetSODetailsResp> getSalesOrderDetails(
      int salesOrderId) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SalesOrder/GetSalesOrderDetails';

    String req =
        '{"SalesOrderId": "$salesOrderId"}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return GetSODetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Save DN
  Future<SaveSalesResp> saveDN(String req) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/DeliveryNote/SaveDeliveryNote';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return SaveSalesResp.fromJson(body);
    } else {
      throw new Exception(CommonCRMSaveResp.fromJson(jsonDecode(
          response.body)).errorDetails[0].errorMessageDescription);
    }
  }

  /// Get PendingTransferOut
  Future<PendingSITOResp> getPendingTransferOut(String req) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetPendingTransferOut';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token),body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return PendingSITOResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }


  /// Get TransferReceiptReferenceDetails
  Future<GetTrReferenceDetailsResp> getTransferReceiptReferenceDetails(String refId) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    String req = '{"ReferenceId" : $refId}';
    final url = '$_baseAPIUrl/SCMTransaction/GetTransferReceiptReferenceDetails';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return GetTrReferenceDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Save TransferReceipt
  Future<MRResp> saveTransferReceipt(String req) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMTransaction/SaveTransferReceipt';
    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return MRResp.fromJson(body);
    } else {
      throw new Exception(MRResp.fromJson(jsonDecode(
          response.body)).validationDetails.errorDetails[0].errorMessageDescription);
    }
  }

  /// Get PendingMaterial
  Future<PendingSITOResp> getPendingMaterialRequest({TransactionType type}) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");
    final url = '$_baseAPIUrl/SCMMasters/GetPendingMaterialRequest';
    String tranName;

    switch (type){
      case TransactionType.SI:
        tranName = "SI";
        break;
      case TransactionType.II:
        tranName = "IIS";
        break;
      case TransactionType.MTO:
        tranName = "TO";
        break;
    }

    String req = '{"Type" : "$tranName","CompanyId" : "${fromAddress.fromCompanyId}", '
          '"DivisionId" : "${fromAddress.fromDivisionId}", "LocationId" : "${fromAddress.fromLocationId}"}';

    print("Req: $req");

    print("Token: $token");

    print("link: $url");

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      return PendingSITOResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }

  /// Get MaterialRequestReferenceDetails
  Future<GetMrRefDetailsResp> getMaterialRequestReferenceDetails(String refId, TransactionType type) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("Token");

    final url = '$_baseAPIUrl/SCMTransaction/GetMaterialRequestReferenceDetails';

    String tranName;

    switch (type){
      case TransactionType.SI:
        tranName = "SI";
        break;
      case TransactionType.II:
        tranName = "IIS";
        break;
      case TransactionType.MTO:
        tranName = "TO";
        break;
    }
    String req = '{"ReferenceId" : "$refId", "Type" : "$tranName"}';

    final response = await this
        .httpClient
        .post(Uri.parse(url), headers: getAuthorizedHeader(token), body: req);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print("RefDetails:");
      print(response.body);
      return GetMrRefDetailsResp.fromJson(body);
    } else {
      throw new Exception(response.reasonPhrase);
    }
  }
}
