
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/common_widget/pin_entry_text_field.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/biometricConfiguration/biometricConfiguration_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:bcore_inventory_management/screens/pinConfiguration/pinConfiguration_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ConfigurationType{ InitialLogin, ResetPin }

class PinConfiguration extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final ConfigurationType processType;

  const PinConfiguration(
      {Key key,  this.repository,  this.appLanguage, this.processType})
      : assert(repository != null),
        super(key: key);

  @override
  _PinConfigurationState createState() => _PinConfigurationState(repository,appLanguage);
}

class _PinConfigurationState extends State<PinConfiguration> {

  final BCRepository repository;
  final AppLanguage appLanguage;
  final LocalAuthentication auth = LocalAuthentication();
  bool _canCheckBiometrics;
  List<BiometricType> _availableBiometrics;

  String userName;
  SharedPreferences prefs;
  double contentPadding;

  String userPin = "";
  String confirmPin = "";

  String thisText = "";
  int pinLength = 6;

//  bool hasError = false;
  String errorMessage;
  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: Colors.deepPurpleAccent),
      borderRadius: BorderRadius.circular(15.0),
    );
  }

  _PinConfigurationState(this.repository, this.appLanguage);

  @override
  void initState() {
    super.initState();

    getPref();
    _checkBiometrics();
    _getAvailableBiometrics();
  }

  getPref() async{
    prefs = await SharedPreferences.getInstance();
    userName = prefs.getString("UName");
  }

  storeCredentials(String uPin) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("PinToStore", uPin);
    pref.setBool("hasConfiguredPin", true);
  }

  storePinStatus() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("hasConfiguredPin", true);
  }

  clearFpIfAnyCredentials() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.containsKey('FpUPin')){
      pref.remove("FpUPin");
    }
  }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometrics = availableBiometrics;
    });
  }

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;

    return BlocProvider<PinConfigurationBloc>(

        create: (context) => PinConfigurationBloc(repository: repository),
        child: BlocListener<PinConfigurationBloc, PinConfigurationState>(
            listener: (context, state) {
              if (state is PinConfigurationInProgress){
                Common().showLoader(context);
              }
              else if (state is PinConfigurationLoaded){
                Navigator.pop(context);
                clearFpIfAnyCredentials();

                if ((_availableBiometrics.isNotEmpty) && (_availableBiometrics[0] == BiometricType.fingerprint)) {
                  storeCredentials(userPin);
                  Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) => BiometricConfiguration()));
                } else{
                  storePinStatus();
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
                }
              }
              else if (state is PinConfigurationFailure) {
                Navigator.pop(context);
                Common().showAlertMessage(
                    context: context,
                    title: "Sorry",
                    message:"Please try again",
                    okFunction: (){
                      Navigator.pop(context);
                    }
                );
              }
            },
            child: BlocBuilder<PinConfigurationBloc, PinConfigurationState>(
                builder: (context, state) {
                  return  Scaffold(
                      appBar: BCAppBar.setInitialAppBar(),
                      body: Container(
                        color: Colors.white,
                        child: SafeArea(
                            child: Padding(
                                padding: EdgeInsets.only(left: contentPadding,right: contentPadding),
                                child:  Center(
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Image.asset("assets/common/bCore_logo/bCore_logo.png"),
                                        SizedBox(height: (safeDisplayHeight(context) > 568) ? 72 : 36),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                                          child: Text("Please Enter Your PIN", style:TextStyle(fontSize: 17, color: Color(hexToColor("#221F1F")))),
                                        ),
                                        SizedBox(height: (safeDisplayHeight(context) > 568) ? 24 : 12),
                                        PinEntryTextField(
                                          fields: 6,
                                          fieldWidth: 45.0,
                                          boxColor: BCAppTheme().headingTextColor,
                                          textColor: BCAppTheme().headingTextColor,
                                          isTextObscure: true,
                                          showFieldAsBox: true,
                                          onSubmit: (String pin){
                                            this.userPin = pin;
                                          }, // end onSubmit
                                        ),
                                        SizedBox(height: (safeDisplayHeight(context) > 568) ? 32 : 16),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                                          child: Text("Please Confirm Your PIN", style: TextStyle(fontSize: 17, color: Color(hexToColor("#221F1F")))),
                                        ),
                                        SizedBox(height: (safeDisplayHeight(context) > 568) ? 24 : 12),
                                        PinEntryTextField(
                                          fields: 6,
                                          fieldWidth: 45.0,
                                          boxColor: BCAppTheme().headingTextColor,
                                          textColor: BCAppTheme().headingTextColor,
                                          isTextObscure: true,
                                          showFieldAsBox: true,
                                          onSubmit: (String pin){
                                            this.confirmPin = pin;
                                          }, // end onSubmit
                                        ),
                                        SizedBox(height: 40),
                                        BCoreButton(
                                            title: 'NEXT',
                                            onPressed: (){
                                              if (validateFields()) {
                                                BlocProvider.of<PinConfigurationBloc>(context).add(ConfigurePin(
                                                    username: this.userName,
                                                    uPin: confirmPin
                                                ),
                                                );
                                              }
                                            }
                                        ),
                                        SizedBox(height: 24),
                                        FlatButton(
                                            padding: EdgeInsets.only(left: 16.0,right: 16.0),
                                            onPressed: (){
                                              if (widget.processType == ConfigurationType.InitialLogin) {
                                                Navigator.of(context)
                                                    .pushReplacement(MaterialPageRoute(
                                                    builder: (context) => Dashboard(
                                                      repository: repository,appLanguage: appLanguage,
                                                    )
                                                )
                                                );
                                              }else{
                                                Navigator.pushAndRemoveUntil(context,
                                                    MaterialPageRoute(builder: (context) => Login()), (
                                                        route) => false);
                                              }
                                            },
                                            child: Text("SKIP PIN SETUP",
                                                style:TextStyle(
                                                  fontSize: 17.0,
                                                  color: Color(hexToColor("#CEA406")),
                                                )
                                            )
                                        ),
                                        if (safeDisplayHeight(context) > 568)
                                          SizedBox(height: 40),
                                      ],
                                    ),
                                  ),
                                )
                            )
                        ),
                      )
                  );
                })
        ));
  }

  validateFields() {
    if ((userPin.isEmpty) || (userPin.length != 6)) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please enter PIN",
          okFunction: () {
            Navigator.pop(context);
          }
      );
      return false;
    } else if ((confirmPin.isEmpty) || (confirmPin.length != 6)) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please Confirm your PIN",
          okFunction: () {
            Navigator.pop(context);
          }
      );
      return false;
    } else if (userPin != confirmPin) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "PIN mismatch",
          okFunction: () {
            Navigator.pop(context);
          }
      );
      return false;
    } else {
      return true;
    }
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}