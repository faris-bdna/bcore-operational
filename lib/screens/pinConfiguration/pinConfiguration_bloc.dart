import 'dart:async';

import 'package:bcore_inventory_management/models/response/updateUserPinResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'pinConfiguration_event.dart';
part 'pinConfiguration_state.dart';


class PinConfigurationBloc extends Bloc<PinConfigurationEvent, PinConfigurationState> {
  final BCRepository repository;

  PinConfigurationBloc({@required this.repository})
      : assert(repository != null),
        super(PinConfigurationInitial());

  @override
  Stream<PinConfigurationState> mapEventToState(PinConfigurationEvent event) async* {
    if (event is ConfigurePin) {
      yield PinConfigurationInProgress();

      try {
        final UpdateUserPinResponse resp = await repository.updateUserPin(username: event.username,uPin: event.uPin);
          yield PinConfigurationLoaded(updateUserPinResponse: resp);
      } catch (error) {
        yield PinConfigurationFailure(error: error.toString());
      }
    }
  }
}