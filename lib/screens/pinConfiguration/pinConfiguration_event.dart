part of 'pinConfiguration_bloc.dart';

abstract class PinConfigurationEvent extends Equatable {
  const PinConfigurationEvent();
}

class ConfigurePin extends PinConfigurationEvent {
  final String username;
  final String uPin;

  const ConfigurePin({
    @required this.username,
    @required this.uPin,
  });

  @override
  List<Object> get props => [username,uPin];

  @override
  String toString() =>
      'ConfigurePinSelected {"Username" : "$username", "UPin" : "$uPin"}';
}
