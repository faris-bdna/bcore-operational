part of 'pinConfiguration_bloc.dart';

abstract class PinConfigurationState extends Equatable {
  const PinConfigurationState();

  @override
  List<Object> get props => [];
}

class PinConfigurationInitial extends PinConfigurationState {}

class PinConfigurationInProgress extends PinConfigurationState {

}

class PinConfigurationLoaded extends PinConfigurationState {
  final UpdateUserPinResponse updateUserPinResponse;

  const PinConfigurationLoaded({@required this.updateUserPinResponse}) : assert(updateUserPinResponse != null);

  @override
  List<Object> get props => [updateUserPinResponse];
}

//class RegistrationUserExists extends PinConfigurationState {
//  final CreateAccountResp createAccountResp;
//
//  const RegistrationUserExists({@required this.createAccountResp}) : assert(createAccountResp != null);
//
//  @override
//  List<Object> get props => [createAccountResp];
//}

class PinConfigurationFailure extends PinConfigurationState {
  final String error;

  const PinConfigurationFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'PinConfigurationFailure { error: $error }';
}
