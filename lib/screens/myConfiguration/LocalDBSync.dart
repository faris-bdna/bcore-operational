
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/master_util/master_bloc.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';


class LocalDBSync extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const LocalDBSync({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _LocalDBSyncState createState() =>
      _LocalDBSyncState(repository, appLanguage,database);
}

class _LocalDBSyncState extends State<LocalDBSync> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  MasterBloc _masterBloc;
  bool isSyncBarcode = false, isSyncPAcking = false, isSyncBatch = false,
      isSyncCountry = false,isSyncBrand = false;

  String ldBarcode = "", ldPAcking = "", ldBatch = "",
      ldCountry = "", ldBrand = "";

  DraftTransactionData draftTransactionData;

  _LocalDBSyncState(this.repository, this.appLanguage, this.database);

  SharedPreferences pref;
  ScmMasters masterToFetch;

  GetItemBarcodeMasterResp getItemBarcodeMasterResp =
  new GetItemBarcodeMasterResp();
  GetItemPackingMasterResp getItemPackingMasterResp =
  new GetItemPackingMasterResp();
  GetItemBatchMasterResp getItemBatchMasterResp = new GetItemBatchMasterResp();
  GetCommonResp getCommonResp = new GetCommonResp();
  String barRecordCount="0";
  String packRecordCount="0";
  String batchRecordCount="0";
  EditMode editMode;

  ShapeDecoration dropDownShapeDecoration = ShapeDecoration(
    shape: RoundedRectangleBorder(
      side: BorderSide(
          width: 0.5,
          style: BorderStyle.solid,
          color: BCAppTheme().headingTextColor),
      borderRadius:
      BorderRadius.all(Radius.circular(5.0)),
    ),
  );

  @override
  void initState()  {
    super.initState();
    intlPref();
  }


  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    ScmMasters loadedMaster;

    return BlocProvider<MasterBloc>(
        create: (context) => MasterBloc(bcRepository: repository),
        child: BlocListener<MasterBloc, MasterState>
          (listener: (context, state) async {
          if (state is GetItemBarcodeMasterInProgress ||
              state is GetItemBatchMasterInProgress ||
              state is GetCommonMasterInProgress ||
              state is GetItemPackingMasterInProgress) {
            Common().showDownloadStatus(context,"Master Data Downloading...");
          }

          else if (state is GetItemBarcodeMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getItemBarcodeMasterResp = state.getItemBarcodeResp;
            if(getItemBarcodeMasterResp.masterDataList.isNotEmpty)
              await saveItemBarcodeMaster(getItemBarcodeMasterResp, db);
            pref.setString(Constants.prfIsIBDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIBDSync, true);
            intlPref();
            Navigator.pop(context);
            if (state.getItemBarcodeResp.masterDataList.length % 10000 == 0) {
              barRecordCount = (int.parse(barRecordCount)
                  + getItemBarcodeMasterResp.masterDataList.length).toString() ;
                syncBarcode();


            }

          }

          else if (state is GetItemPackingMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getItemPackingMasterResp = state.getItemPackingResp;
            if(getItemPackingMasterResp.masterDataList.isNotEmpty)
              await savePackingMaster(getItemPackingMasterResp, db);
            pref.setString(Constants.prfIsIPDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIPDSync, true);
            intlPref();
            Navigator.pop(context);
            if (state.getItemPackingResp.masterDataList.length % 10000 == 0) {
              packRecordCount = (int.parse(packRecordCount)
                  + getItemPackingMasterResp.masterDataList.length).toString();
              syncPacking();
            }

          }

          else if (state is GetItemBatchMasterLoaded) {

            var db = Provider.of<Database>(context, listen: false);
            getItemBatchMasterResp = state.getItemBatchResp;
            if(getItemBatchMasterResp.masterDataList.isNotEmpty)
              await saveBatchMaster(getItemBatchMasterResp, db);
            pref.setString(Constants.prfIsIBHDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIBHDSync, true);
            intlPref();
            Navigator.pop(context);
            if ((state.getItemBatchResp.masterDataList.length % 10000 == 0) && (batchRecordCount != "0")) {
              batchRecordCount = (int.parse(batchRecordCount)
                  + getItemBatchMasterResp.masterDataList.length).toString();
            syncBatch();
            }

          }

          /// Country Of Origin master loaded state
          else if (state is GetCountryOfOriginMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getCommonResp = state.getCommonResp;
            loadedMaster = state.scmMaster;
            if(getCommonResp.masterDataList.isNotEmpty)
              await saveCountryOfOriginMaster(getCommonResp, db);
            pref.setString(Constants.prfIsCOODDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsCOODSync, true);
            intlPref();
            Navigator.pop(context);

          }

          /// Product brand master loaded state
          else if (state is GetProductBrandMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getCommonResp = state.getCommonResp;
            loadedMaster = state.scmMaster;
            if(getCommonResp.masterDataList.isNotEmpty)
              await saveProductBrandMaster(getCommonResp, db);
            if (loadedMaster == ScmMasters.ProductBrand) {
              pref.setString(Constants.prfIsBRDDDate, DateTime.now().toString());
              pref.setBool(Constants.prfIsBRDDSync, true);
              intlPref();
            }
            Navigator.pop(context);

          }

          else if (state is GetItemBarcodeMasterFailure) {
            pref.setString(Constants.prfIsIBDDate, "");
            pref.setBool(Constants.prfIsIBDSync, false);
            Navigator.pop(context);
          }

          else if (state is GetItemPackingMasterFailure) {
            pref.setString(Constants.prfIsIPDDate, ldPAcking);
            pref.setBool(Constants.prfIsIPDSync, false);
            Navigator.pop(context);
          }

          else if (state is GetItemBatchMasterFailure) {
            pref.setString(Constants.prfIsIBHDDate,"");
            pref.setBool(Constants.prfIsIBHDSync, false);
            Navigator.pop(context);

          }

          else if (state is GetCommonMasterFailure) {
            if (loadedMaster == ScmMasters.ProductBrand) {
              pref.setString(Constants.prfIsBRDDDate, "");
              pref.setBool(Constants.prfIsBRDDSync, false);
            }else{
              pref.setString(Constants.prfIsCOODDate, "");
              pref.setBool(Constants.prfIsCOODSync, false);
            }
            Navigator.pop(context);
          }


        },
            child: BlocBuilder<MasterBloc, MasterState>(builder: (context, state) {
              _masterBloc =  BlocProvider.of<MasterBloc>(context);
              // intlPref();
              return Scaffold(
                  key: scaffoldKey,
                  appBar: BCAppBar(
                    scaffoldKey:scaffoldKey,
                    title: 'Data Sync',
                    isBack: false,
                    appBar: AppBar(),
                    database: database,
                    appLanguage: appLanguage,
                    repository: repository,
                    shouldHideActions: true,
                  ),
                  body: Scaffold(
                    drawer: NavigationDrawer(
                      context: context,
                      repository: repository,
                      appLanguage: appLanguage,
                      database: database,
                    ),
                    body: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Item Barcode Data", style: TextStyle(fontWeight: FontWeight.w500),),
                                      Text("Last Sync On: $ldBarcode",style: TextStyle(fontWeight: FontWeight.w300,
                                          fontSize:10.0, color: BCAppTheme().grayColor),)                                    ],
                                  ),
                                  IconButton(icon:Icon(isSyncBarcode?
                                  Icons.sync
                                      :Icons.sync_problem), onPressed: (){
                                    syncBarcode();
                                  })
                                ],
                              ),
                              SizedBox(height: 4.0,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Item Packing Data", style: TextStyle(fontWeight: FontWeight.w500),),
                                      Text("Last Sync On: $ldPAcking",style: TextStyle(fontWeight: FontWeight.w300,
                                          fontSize:10.0, color: BCAppTheme().grayColor),)                                    ],
                                  ),
                                  IconButton(icon:Icon(isSyncPAcking?
                                  Icons.sync:Icons.sync_problem), onPressed: (){
                                    syncPacking();
                                  })
                                ],
                              ),
                              SizedBox(height: 4.0,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Item Batch Data", style: TextStyle(fontWeight: FontWeight.w500),),
                                      Text("Last Sync On: $ldBatch",style: TextStyle(fontWeight: FontWeight.w300,
                                          fontSize:10.0, color: BCAppTheme().grayColor),)                                    ],
                                  ),
                                  IconButton(icon:Icon(isSyncBatch?
                                  Icons.sync:
                                  Icons.sync_problem), onPressed: (){
                                    syncBatch();
                                  })
                                ],
                              ),
                              SizedBox(height: 4.0,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Country Of Origin Data", style: TextStyle(fontWeight: FontWeight.w500),),
                                      Text("Last Sync On: $ldCountry",style: TextStyle(fontWeight: FontWeight.w300,
                                          fontSize:10.0, color: BCAppTheme().grayColor),)                                    ],
                                  ),
                                  IconButton(icon:Icon(isSyncCountry?
                                  Icons.sync
                                      :Icons.sync_problem), onPressed: (){
                                    syncCountry();
                                  })
                                ],
                              ),
                              SizedBox(height: 4.0,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Brand Data", style: TextStyle(fontWeight: FontWeight.w500),),
                                      Text("Last Sync On: $ldBrand",style: TextStyle(fontWeight: FontWeight.w300,
                                          fontSize:10.0, color: BCAppTheme().grayColor),)
                                    ],
                                  ),
                                  IconButton(icon:Icon(isSyncBrand?
                                  Icons.sync:Icons.sync_problem), onPressed: (){
                                    syncBrand();
                                  })
                                ],
                              ),
                              SizedBox(height: 4.0,),
                              TextButton(onPressed: ()async{
                                await syncBarcode();
                                await syncPacking();
                                await syncBatch();
                                await syncCountry();
                                await syncBrand();
                              }, child: Text("Sync All")),


                            ],
                          ),
                        ),
                      ),
                    )
                  ));
            })));
  }

  void setDraftTransaction(String type) {

    draftTransactionData = DraftTransactionData(
        id: editMode == EditMode.On ? 1 : null,  /// When the user config got saved in db the id will be autogenerated & will be "1". So we use this id ("1") always, while updating the config
        trans_id: type + '_101',
        trans_type: type,
        trans_details: json.encode(FromAddress.shared.toJson()));
  }
  Future<Null> intlPref()async {
    Future.delayed(const Duration(milliseconds: 300), () async {
      pref = await SharedPreferences.getInstance();
      setState(() {
        isSyncBarcode = pref.getBool(Constants.prfIsIBDSync)?? false;
        isSyncPAcking = pref.getBool(Constants.prfIsIPDSync)?? false;
        isSyncBatch = pref.getBool(Constants.prfIsIBHDSync)?? false;
        isSyncCountry = pref.getBool(Constants.prfIsCOODSync)?? false;
        isSyncBrand = pref.getBool(Constants.prfIsBRDDSync)?? false;
        ldBarcode = pref.getString(Constants.prfIsIBDDate)?? "";
        ldPAcking = pref.getString(Constants.prfIsIPDDate)?? "";
        ldBatch = pref.getString(Constants.prfIsIBHDDate)?? "";
        ldCountry = pref.getString(Constants.prfIsCOODDate)?? "";
        ldBrand = pref.getString(Constants.prfIsBRDDDate)?? "";
      });

    });
  }
  syncBarcode() {
    intlPref();
    _masterBloc.add(GetItemBarcodeMaster(
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        supplierId: '',
        recordCount: barRecordCount,
        lastFetchDate: pref.get(Constants.prfIsIBDDate)??""));


  }

  syncPacking(){
    intlPref();
    _masterBloc.add(GetItemPackingMaster(
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        supplierId: '',
        recordCount: packRecordCount,
        lastFetchDate: pref.get(Constants.prfIsIPDDate)??"" ));
  }

  syncBatch(){
    intlPref();
    _masterBloc.add(GetItemBatchMaster(
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        supplierId: '0',
        recordCount: batchRecordCount,
        lastFetchDate: pref.get(Constants.prfIsIBHDDate)??""));
  }
  syncCountry(){
    intlPref();
    setMasterToFetch(ScmMasters.CountryOfOrigin);
    _masterBloc.add(GetCommonMaster(scmMaster: masterToFetch));
  }
  syncBrand(){
    intlPref();
    setMasterToFetch(ScmMasters.ProductBrand);
    _masterBloc.add(GetCommonMaster(scmMaster: masterToFetch));
  }
  void setMasterToFetch(ScmMasters scmMaster) {
    masterToFetch = scmMaster;
  }

  saveItemBarcodeMaster(
      GetItemBarcodeMasterResp getItemBarcodeMasterResp, Database db) async {
    ItemBarcodeData barcodeData;
    if(barRecordCount == "0")
    db.itemBarcodeDao.deleteAllBarCode();

    await db.itemBarcodeDao.batch((b) {
      for (var item in getItemBarcodeMasterResp.masterDataList) {
        barcodeData = ItemBarcodeData(
            Text: item.text,
            ItemBarcodeId: item.value,
            ProductGroupId: item.productGroupId,
            ProductId: item.productId,
            ProductName: item.productName,
            ItemId: item.itemId,
            ItemCode: item.itemCode,
            ItemName: item.itemName,
            ItemPackingId: item.itemPackingId,
            MajorPackId: item.majorPackId,
            BasePackId: item.basePackId,
            PkgQty: item.pkgQty.toString(),
            PackingDesc: item.packingDesc,
            ScaleGroupId: item.scaleGroupId,
            BrandId: item.brandId,
            OriginCountryId: item.originCountryId,
            ProductStockTypeId: item.productStockTypeId,
            MinShelfLife: item.minShelfLife,
            TotalShelfLife: item.totalShelfLife,
            IsSalesAllowed: item.isSalesAllowed,
            StockAllocTypeId: item.stockAllocTypeId,
            IsTaxApplicable: item.isTaxApplicable,
            ItemStatus: item.itemStatus,
            pur_restriction: item.purchaseRestriction,
            IsPurchaseAllowed: item.isPurchaseAllowed,
            ExcessQty: item.excessQty.toString(),
            IsMinOrderQtyApp: item.isMinOrderQtyApp,
            MinOrderQty: item.minOrderQty.toString(),
            SuppItemCode: item.suppItemCode,
            SuppTypeId: 0, //item.suppTypeId,
            TaxPercent: item.taxPercent.toString(),
            IsBatchEnabled: item.isBatchEnabled,
            HasPurchaseGroupAccess: item.hasPurchaseGroupAccess,
            StockBasedItem: item.stockBasedItem,
            ConsignmentItem: item.consignmentItem,
            LastPurchaseQty: item.lastPurchaseQty.toString(),
            LastPurchaseDate: item.lastPurchaseDate,
            LastReceivedAmount: item.lastReceivedAmount.toString(),
            LastReceivedSupplier: item.lastReceivedSupplier,
            AverageCost: item.averageCost.toString(),
            AvailableQty: item.availableQty.toString(),
            NetCost: item.netCost.toString());
        try {
          b.insert(db.itemBarcode, barcodeData);
        } catch (e) {
          print(e);
        }
      }
    });
  }

  savePackingMaster(


      GetItemPackingMasterResp getItemPackingMasterResp, Database db) async {
  ItemPackingData packingData;

  if(packRecordCount == "0")
    db.itemPackingDao.deleteAllPkg();

    await db.itemPackingDao.batch((b) {
      for (var item in getItemPackingMasterResp.masterDataList) {
        packingData = ItemPackingData(
            packing_text: item.text,
            packing_value: item.batch_value,
            item_id: item.itemId);
        // db.itemPackingDao.updateItemPacking(packingData);
        b.insert(db.itemPacking, packingData);
      }
    });
  }

  saveBatchMaster(
      GetItemBatchMasterResp getItemBatchMasterResp, Database db) async {
    ItemBatchData batchData;

    if(batchRecordCount == "0")
    db.itemBatchDao.deleteAllBatch();
    await db.itemBatchDao.batch((b) {
      for (var item in getItemBatchMasterResp.masterDataList) {
        batchData = ItemBatchData(
            batch_text: item.text,
            batch_value: item.value,
            item_id: item.itemId,
            prod_date: item.prodDate,
            expiry_date: item.expiryDate);
        b.insert(db.itemBatch, batchData);
      }
    });
  }

  saveProductBrandMaster(GetCommonResp getCommonResp, Database db) async {
    ProductBrandData productBrandData;
    db.productBrandDao.deleteAllBrand();
    await db.productBrandDao.batch((b) {
      for (var item in getCommonResp.masterDataList) {
        productBrandData =
            ProductBrandData(brand_name: item.text, brand_id: item.value);
        b.update(db.productBrand, productBrandData);
      }
    });
  }

  saveCountryOfOriginMaster(GetCommonResp getCommonResp, Database db) async {
    CountryOriginData countryOriginData;
    db.countryOriginDao.deleteAllCountry();
    await db.countryOriginDao.batch((b) {
      for (var item in getCommonResp.masterDataList) {
        countryOriginData =
            CountryOriginData(country_name: item.text, country_id: item.value);
        b.update(db.countryOrigin, countryOriginData);
      }
    });
  }

  // storeLastFetchDate(String fetchDate) async {
  //   pref.setBool("${Constants.prfIsBRDDSync}", fetchDate);
  // }
}
