
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/address_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:moor/moor.dart' hide Column;
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'dart:convert';


class FromAddressForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const FromAddressForm({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _FromAddressFormState createState() =>
      _FromAddressFormState(repository, appLanguage,database);
}

class _FromAddressFormState extends State<FromAddressForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  AddressBloc _addressBloc;

  DraftTransactionData draftTransactionData;

  _FromAddressFormState(this.repository, this.appLanguage, this.database);
  // FromAddress fromAddress = FromAddress();

  List<CompanyDataList> companyList;
  List<CommonDataList> divisionList;
  List<CommonDataList> locationList;
  List<CommonDataList> projectsList;
  List<CommonDataList> departmentList;

  EditMode editMode;

  ShapeDecoration dropDownShapeDecoration = ShapeDecoration(
    shape: RoundedRectangleBorder(
      side: BorderSide(
          width: 0.5,
          style: BorderStyle.solid,
          color: BCAppTheme().headingTextColor),
      borderRadius:
      BorderRadius.all(Radius.circular(5.0)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

    bool _validateFields() {
      if (fromAddress.fromCompanyId == null){
        // showMessage(context: context,message: "Please select your Company");
        Common().showStepperValidation(scaffoldKey: scaffoldKey,message: "Please select your Company");
        return false;
      } else if (fromAddress.fromDivisionId == null) {
        // showMessage(context: context,message: "Please select your Division");
        Common().showStepperValidation(scaffoldKey: scaffoldKey,message: "Please select your Division");
        return false;
      } else if (fromAddress.fromLocationId == null) {
        // showMessage(context: context,message: "Please select your Location");
        Common().showStepperValidation(scaffoldKey: scaffoldKey,message: "Please select your Location");
        return false;
      }
      // else if (fromAddress.fromDepartmentId == null) {
      //   // showMessage(context: context,message: "Please select your Department");
      //   Common().showStepperValidation(scaffoldKey: scaffoldKey,message: "Please select your Department");
      //   return false;
      // }
      else {
        return true;
      }
    }

    return BlocProvider<AddressBloc>(
        create: (context) => AddressBloc(repository: repository,database: database ),
        child: BlocListener<AddressBloc, AddressState>
          (listener:
            (context, state) async {
          if (state is GetCompanyInProgress || state is GetDivisionInProgress ||
              state is GetLocationInProgress || state is GetDepartmentInProgress ||
              state is GetProjectInProgress || state is GetDraftTransactionInProgress
          || state is SaveDraftTransactionInProgress || state is  UpdateDraftTransactionInProgress) {
            Common().showLoader(context);
          }

          else if (state is GetCompanyFailure || state is GetDivisionFailure
              || state is GetLocationFailure || state is GetDepartmentFailure
              || state is GetProjectFailure ) {
            Navigator.pop(context);
          }
          else if (state is GetDraftTransactionFailure ) {
            Navigator.pop(context);
            editMode = EditMode.Off;
            _addressBloc.add(GetCompany(mode: AccessType.UserBased.index));
          }
          // else if (state is UpdateDraftTransactionFailure){
          //   Navigator.pop(context);
          //
          //   Common().showAlertMessage(
          //       context: context,
          //       title: Constants.alertMsgTitle,
          //       message: 'Sorry, Please try again ? ',
          //       okFunction: () {
          //         Navigator.pop(context);
          //       });
          // }
          else if (state is SaveDraftTransactionFailure ||
              state is SaveDraftTransactionComplete ||
              state is UpdateDraftTransactionFailure ||
              state is UpdateDraftTransactionComplete
          ) {
            Navigator.pop(context);

            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => Dashboard(
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                )));
          }
          else if (state is GetCompanyComplete) {
            Navigator.pop(context);
            companyList = [];
            setState(() {

              companyList = state.getCompanyResp.masterDataList;

              if(fromAddress.fromCompanyId != null){
                // FromAddress.shared.fromCompanyId =  fromAddress.fromCompanyId;
                _addressBloc.add(LoadDivision(
                    companyId: fromAddress.fromCompanyId,
                    mode: AccessType.UserBased.index));
              }
            });
          }

          else if (state is GetDivisionComplete) {
            Navigator.pop(context);

            if (state.getDivisionResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Division data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else {
              divisionList = [];
              setState(() {
                divisionList = state.getDivisionResponse.masterDataList;

                if(fromAddress.fromCompanyId != null && fromAddress.fromDivisionId != null){
                  // FromAddress.shared.fromDivisionId =  fromAddress.fromDivisionId;
                  _addressBloc.add(LoadLocation(
                      companyId: fromAddress.fromCompanyId,
                      divisionId: fromAddress.fromDivisionId,
                      mode: AccessType.UserBased.index));
                }
              });
              if (fromAddress.fromCompanyId != null) {
                BlocProvider.of<AddressBloc>(context).add(LoadDepartment(
                    companyId: fromAddress.fromCompanyId,
                    mode: AccessType.UserBased.index));
              }
            }
          }

          else if (state is GetLocationComplete) {
            Navigator.pop(context);
            if (state.getLocationResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Location data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else {
              locationList = [];
              setState(() {
                locationList = state.getLocationResponse.masterDataList;

                if(fromAddress.fromLocationId != null){
                  // FromAddress.shared.fromLocationId =  fromAddress.fromLocationId;
                  // FromAddress.shared.fromDeliveryLocationId =  fromAddress.fromDeliveryLocationId;
                  _addressBloc.add(
                      GetProject(locationId: fromAddress.fromLocationId));
                }
              });
            }
          }

          else if (state is GetDepartmentComplete) {
            Navigator.pop(context);

            if (state.getDepartmentResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: 'Alert',
                  message: 'Department data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else {
              departmentList = [];
              setState(() {
                departmentList = state.getDepartmentResponse.masterDataList;
                // if(fromAddress.fromDepartmentId != null){
                //   FromAddress.shared.fromDepartmentId =  fromAddress.fromDepartmentId;
                // }
              });
            }
          }

          else if (state is GetProjectComplete) {
            Navigator.pop(context);
            if (state.getProjectResp.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: 'Alert',
                  message: 'Project data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else {
              projectsList = [];
              setState(() {
                projectsList = state.getProjectResp.masterDataList;
                // if(fromAddress.fromProjectId != null){
                //   FromAddress.shared.fromProjectId =  fromAddress.fromProjectId;
                // }
              });
            }
          }

          else if(state is GetDraftTransactionComplete){
            Navigator.pop(context);

            editMode = EditMode.On;

            fromAddress.fromJson(state.fromAddress.toJson());
            print(state.fromAddress.toJson());

            _addressBloc.add(GetCompany(mode: AccessType.UserBased.index));
          }
        },
            child: BlocBuilder<AddressBloc, AddressState>(builder: (context, state) {
              _addressBloc =  BlocProvider.of<AddressBloc>(context);

              if (state is GetCompanyInitial) {

                _addressBloc.add(GetDraftTransaction(draftTransId: "CONFIG_101",
                    draftTransType:"CONFIG"));
              }
              return Scaffold(
                  key: scaffoldKey,
                  appBar: BCAppBar(
                    scaffoldKey:scaffoldKey,
                    title: 'My Address',
                    isBack: false,
                    appBar: AppBar(),
                    database: database,
                    appLanguage: appLanguage,
                    repository: repository,
                    shouldHideActions: true,
                  ),
                  body: Scaffold(
                    drawer: NavigationDrawer(
                      context: context,
                      repository: repository,
                      appLanguage: appLanguage,
                      database: database,
                    ),
                    body: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (companyList != null && companyList.isNotEmpty)
                                      ? companyList.map((CompanyDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromCompanyName,
                                  hint: Constants.companyValidationMsg,
                                  searchHint: Constants.companyValidationMsg,
                                  onChanged: (value) {
                                    if (value != null) {
                                      setState(() {

                                        /// Search for the selected company
                                        var selectedCompanyDetails =
                                        companyList.where((element) =>
                                        element.text ==
                                            value)
                                            .toList()[0];
                                        fromAddress.fromCompanyId = selectedCompanyDetails.value;
                                        fromAddress.fromCompanyName = selectedCompanyDetails.text;

                                        if (selectedCompanyDetails != null) {
                                          fromAddress.fromCompanyName =
                                              selectedCompanyDetails.text;
                                          fromAddress.fromCompanyDateFormat =
                                              selectedCompanyDetails.dateFormat;
                                          fromAddress.curDecimPlace =
                                              selectedCompanyDetails
                                                  .decimalPlace;
                                        }
                                        //getToAddressJson();
                                        // writeToFile(toAddress);

                                        if (fromAddress.fromCompanyId != null) {
                                          _addressBloc.add(LoadDivision(
                                              companyId: fromAddress.fromCompanyId, mode: AccessType.UserBased.index));
                                        }
                                      });
                                    }
                                  },
                                  displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (divisionList != null && divisionList.isNotEmpty)
                                      ? divisionList.map((CommonDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromDivisionName,
                                  hint: Constants.divisionValidationMsg,
                                  searchHint: Constants.divisionValidationMsg,
                                  onChanged: (value) {
                                    if (value != null) {
                                      setState(() {
                                        var selectedDivisionDetails =
                                        divisionList.where((element) =>
                                        element.text ==
                                            value)
                                            .toList()[0];
                                        fromAddress.fromDivisionId =
                                            selectedDivisionDetails.value;

                                        if (selectedDivisionDetails != null) {
                                          fromAddress.fromDivisionName =
                                              selectedDivisionDetails.text;
                                        }
                                        _addressBloc.add(LoadLocation(
                                            companyId: fromAddress.fromCompanyId,
                                            divisionId: fromAddress.fromDivisionId,
                                            mode: AccessType.UserBased.index));
                                      });
                                    }
                                  },
                                  displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (locationList != null && locationList.isNotEmpty)
                                      ? locationList.map((CommonDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromLocationName,
                                  hint: Constants.locationValidationMsg,
                                  searchHint: Constants.locationValidationMsg,
                                  onChanged: (value) {
                                    if (value != null) {
                                      setState(() {
                                        var selectedLocationDetails =
                                        locationList
                                            .where((element) =>
                                        element.text == value)
                                            .toList()[0];
                                        fromAddress.fromLocationId =
                                            selectedLocationDetails.value;

                                        if (selectedLocationDetails != null) {
                                          fromAddress.fromLocationName =
                                              selectedLocationDetails.text;
                                        }
                                        _addressBloc.add(GetProject(
                                            locationId:
                                            fromAddress.fromLocationId));
                                      });
                                    }
                                  },
                                  displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (locationList != null && locationList.isNotEmpty)
                                      ? locationList.map((CommonDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromDeliveryLocationName,
                                  hint: "Select Delivery Location",
                                  searchHint: "Select Delivery Location",
                                  onChanged: (value) {
                                    if (value != null) {
                                      setState(() {
                                        var selectedDeliveryLocation = locationList
                                            .where((location) =>
                                        location.text ==
                                            value)
                                            .toList()[0];
                                        fromAddress.fromDeliveryLocationId =
                                            selectedDeliveryLocation.value;
                                        fromAddress.fromDeliveryLocationName =
                                            selectedDeliveryLocation.text;
                                      });
                                    }
                                  },
                                  displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox( height: 10 ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (projectsList != null && projectsList.isNotEmpty)
                                      ? projectsList.map((CommonDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromProjectName,
                                  hint: "Select Project",
                                  searchHint: "Select Project",
                                  onChanged: (value) {
                                    if (value != null) {
                                      setState(() {
                                        var selectedProject = projectsList.where((project) =>
                                        project.text == value).toList()[0];
                                        print(selectedProject);

                                        fromAddress.fromProjectId = selectedProject.value;
                                        fromAddress.fromProjectName = selectedProject.text;
                                      });
                                    }
                                  },
                                  displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: dropDownShapeDecoration,
                                child: SearchableDropdown.single(
                                  items: (departmentList != null && departmentList.isNotEmpty)
                                      ? departmentList.map((CommonDataList item) {
                                    return DropdownMenuItem(
                                        child: Text(item.text,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: BCAppTheme().subTextColor,
                                            )),
                                        value: item.text);
                                  }).toList()
                                      : [],
                                  value: fromAddress.fromDepartmentName,
                                  hint: "Select a Department",
                                  searchHint: "Select a Department",
                              onChanged: (value) {
                                if (value != null) {
                                  setState(() {
                                    var selectedDepartmentDetails =
                                        departmentList
                                            .where((element) =>
                                                element.text == value)
                                            .toList()[0];
                                    fromAddress.fromDepartmentId =
                                        selectedDepartmentDetails.value;
                                    if (selectedDepartmentDetails != null) {
                                      fromAddress.fromDepartmentName =
                                          selectedDepartmentDetails.text;
                                    }
                                  });
                                }
                              },
                              displayClearIcon: false,
                                  underline: Container(),
                                  isExpanded: true,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              // Expanded(
                              //   child:
                              //   Container(
                              //     height: 35,
                              //     child: FlatButton(
                              //         color: BCAppTheme().primaryColor,
                              //         child: Text("Save",
                              //             style: TextStyle(
                              //                 color: BCAppTheme().secondaryColor,
                              //                 fontSize: 11)),
                              //         onPressed: () {
                              //
                              //         }),
                              //   ),
                              // ),
                              FlatButton(
                                color: BCAppTheme().primaryColor,
                                onPressed: () {
                                  if (_validateFields()){
                                    setDraftTransaction('CONFIG');

                                    if (editMode == EditMode.Off){
                                      BlocProvider.of<AddressBloc>(context).add(SaveDraftTransaction(
                                          draftTransactionData: draftTransactionData));
                                    }else{
                                      BlocProvider.of<AddressBloc>(context).add(UpdateDraftTransaction(
                                          draftTransactionData: draftTransactionData));
                                    }
                                  }
                                },
                                child: Text("Save Details",
                                    style: TextStyle(
                                        color: BCAppTheme().secondaryColor,
                                        fontSize: 11)),

                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ));
            })));
  }

  void setDraftTransaction(String type) {

    draftTransactionData = DraftTransactionData(
        id: editMode == EditMode.On ? 1 : null,  /// When the user config got saved in db the id will be autogenerated & will be "1". So we use this id ("1") always, while updating the config
        trans_id: type + '_101',
        trans_type: type,
        trans_details: json.encode(FromAddress.shared.toJson()));
  }
}
