
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:flutter/material.dart';

class ConfigurationComplete extends StatefulWidget {
  @override
  _ConfigurationCompleteState createState() => _ConfigurationCompleteState();
}

class _ConfigurationCompleteState extends State<ConfigurationComplete> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        body: SafeArea(
            child: Padding(
                padding: EdgeInsets.only(left: 32.0,right: 32.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Image.asset("assets/common/bCore_logo/bCore_logo.png"),
                  Image.asset( "assets/configurationComplete/configurationComplete_complete_icon.png", height: 210.0),
                  Text("SETUP IS COMPLETED !", style: TextStyle(fontSize: 17,color: Color(hexToColor("#221F1F"))),textAlign: TextAlign.center),
                  SizedBox(height:96),
                  BCoreButton(
                      title: 'SIGN IN',
                      onPressed: (){
                        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
                      }
                  ),
                  SizedBox(height: 40),
                ],
              ),
            )
        ),

      ),
    );
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}
