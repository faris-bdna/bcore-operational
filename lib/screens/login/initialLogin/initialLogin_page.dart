import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_api_client.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/forgotPassword/forgotPassword_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_bloc.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/MyConfiguration.dart';
import 'package:bcore_inventory_management/screens/pinConfiguration/pinConfiguration_page.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InitialLogin extends StatefulWidget {
  @override
  _InitialLoginState createState() => _InitialLoginState();
}

class _InitialLoginState extends State<InitialLogin> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  double contentPadding;

  AppLanguage appLanguage = AppLanguage();
  final BCRepository repository = BCRepository(
    bcApiClient: BCApiClient(
      httpClient: http.Client(),
    ),
  );
  Database database = Database();

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0;
    return BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(repository: repository),
        child: BlocListener<LoginBloc, LoginState>(listener: (context, state) {
          if (state is LoginInProgress) {
            Common().showLoader(context);
          } else if (state is LoginLoaded) {
            Navigator.pop(context);
            storeCredentials(_usernameController.text.trim(),
                state.loginResponse.accessToken);
            // confirmConfigurationAlert();
            navigateToNextPage();
          } else if (state is LoginFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: "Invalid username or password",
                okFunction: () {
                  Navigator.pop(context);
                });
          }
        }, child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
          return Scaffold(
            body: Container(
              color: Colors.white,
              child: SafeArea(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: contentPadding, right: contentPadding),
                    child: Center(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Image.asset(
                                "assets/common/bCore_logo/bCore_logo.png"),
                            SizedBox(height: 80),
                            Container(
                                width: 320,
                                padding: EdgeInsets.all(0.0),
                                child: TextField(
                                  textInputAction: TextInputAction.next,
                                  controller: _usernameController,
                                  autocorrect: false,
//                                              style: GoogleFonts.poppins(),
                                  decoration: InputDecoration(
                                    hintText: 'User Name',
                                    prefixIcon: Icon(Icons.mail_outline,
                                        color: isUsernameEmpty()
                                            ? BCAppTheme().boarderColor
                                            : BCAppTheme().headingTextColor),
                                    hintStyle: TextStyle(color: Colors.grey),
                                    filled: true,
                                    fillColor: Colors.white70,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      borderSide: BorderSide(
                                          color: isUsernameEmpty()
                                              ? BCAppTheme().boarderColor
                                              : BCAppTheme().headingTextColor,
                                          width: 0.5),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      borderSide: BorderSide(
                                          color: BCAppTheme().headingTextColor,
                                          width: 0.5),
                                    ),
                                  ),
                                  keyboardType: TextInputType.emailAddress,
                                  onEditingComplete: () =>
                                      FocusScope.of(context).nextFocus(),
                                )),
                            SizedBox(height: 16),
                            Container(
                                width: 320,
                                padding: EdgeInsets.all(0.0),
                                child: TextField(
                                  controller: _passwordController,
                                  enableInteractiveSelection: false,
                                  autocorrect: false,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    hintText: 'Password',
                                    prefixIcon: Icon(Icons.lock_outline,
                                        color: isPasswordEmpty()
                                            ? BCAppTheme().boarderColor
                                            : BCAppTheme().headingTextColor),
                                    hintStyle: TextStyle(color: Colors.grey),
                                    filled: true,
                                    fillColor: Colors.white70,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      borderSide: BorderSide(
                                          color: isPasswordEmpty()
                                              ? BCAppTheme().boarderColor
                                              : BCAppTheme().headingTextColor,
                                          width: 0.5),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      borderSide: BorderSide(
                                          color: Color(hexToColor("#CEA406")),
                                          width: 0.5),
                                    ),
                                  ),
//                                          onEditingComplete: (){
//                                            setState(() {
//                                            }
//                                            );
//                                          },
                                )),
                            Container(
                              padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: FlatButton(
                                    padding:
                                        EdgeInsets.only(left: 16.0, right: 0.0),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ForgotPassword(
                                                      repository: repository,
                                                      appLanguage: appLanguage,
                                                      processType: ResetType
                                                          .ResetPassword)));
                                    },
                                    child: Text("Forgot Password?",
                                        style: TextStyle(
                                            color:
                                                Color(hexToColor("#CEA406"))))),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: BCoreButton(
                                  title: 'CONFIGURE SIGN IN',
                                  onPressed: () {
                                    if (validateFields()) {
                                      BlocProvider.of<LoginBloc>(context).add(
                                          SignIn(
                                              username: _usernameController.text
                                                  .trim(),
                                              password: _passwordController.text
                                                  .trim()));
                                    }
                                  }),
                            ),
                            SizedBox(height: 40)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        })));
  }

  isUsernameEmpty() => _usernameController.text.trim().isEmpty;
  isPasswordEmpty() => _passwordController.text.trim().isEmpty;

  validateFields() {
    if (isUsernameEmpty()) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please enter your user name",
          okFunction: () {
            Navigator.pop(context);
          });
      return false;
    } else if (isPasswordEmpty()) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please enter your password",
          okFunction: () {
            Navigator.pop(context);
          });
      return false;
    } else {
      return true;
    }
  }

  storeCredentials(String userName, String token) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("UName", userName);
    pref.setString("Token", token);
  }

  navigateToNextPage() async{
    bool isInitialNavigationToDashboard = await Common().hasInitialNavigationToDashboardCompleted();
    if (isInitialNavigationToDashboard) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Dashboard(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
          )));
    }else{
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) =>
              FromAddressForm(
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              )));
    }
  }

  confirmConfigurationAlert() async {

    Dialog saveMobID = Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Padding(
        padding: EdgeInsets.all(contentPadding / 2),
        child: Container(
          height: 250,
          child: Column(
            children: [
              SizedBox(height: 32),
              Text("Configure your identity",
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color(hexToColor("#221F1F")))),
              SizedBox(height: 8.0),
              Text("Use your PIN and Fingerprint verification",
                  style: TextStyle(
                      fontSize: 10, color: Color(hexToColor("#221F1F")))),
              SizedBox(height: 32.0),
              Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: Color(hexToColor("#A88D2A")).withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 10,
                          offset: Offset(-1, 15), // changes position of shadow
                        ),
                      ],
                    ),
                    child: RaisedButton(
                        padding: EdgeInsets.only(
                            right: 4, left: 4, top: 4, bottom: 4),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PinConfiguration(
                                        repository: repository,
                                        appLanguage: appLanguage,
                                        processType:
                                            ConfigurationType.InitialLogin,
                                      )));
                        },
                        textColor: Colors.white,
                        color: Color(hexToColor("#CEA406")),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                height: 42,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 48.0, right: 48.0),
                                      child: AutoSizeText(
                                        'CONFIGURE SIGN IN',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                        maxLines: 1,
                                        minFontSize: 12.0,
                                        stepGranularity: 1.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Image.asset(
                                    "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                          ],
                        )),
                  )),
              SizedBox(height: 24.0),
              FlatButton(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  onPressed: () {
                    Navigator.pop(context);
                    navigateToNextPage();
//                    Navigator.of(context).pushReplacement(MaterialPageRoute(
//                        builder: (context) => Dashboard(
//                              repository: repository,
//                              appLanguage: appLanguage,
//                              database: database,
//                            )));
                  },
                  child: Text("SKIP NOW",
                      style: TextStyle(
                        fontSize:
                            (safeDisplayWidth(context) <= 320) ? 12.0 : 17.0,
                        color: Color(hexToColor("#CEA406")),
                      )))
            ],
          ),
        ),
      ),
    );

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => saveMobID);
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}
