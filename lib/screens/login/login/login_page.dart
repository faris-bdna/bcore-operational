import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/common_widget/pin_entry_text_field.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_api_client.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/forgotPassword/forgotPassword_page.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/MyConfiguration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'login_bloc.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  AppLanguage appLanguage = AppLanguage();
  String userPin = "";
  String userName = "";
  String fpUserPin = "";
  bool shouldEnableFPLogin = false;
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  double contentPadding;
  bool hasFPAuthPopupDisplayed = false;

  final BCRepository repository = BCRepository(
    bcApiClient: BCApiClient(
      httpClient: http.Client(),
    ),
  );

  final Database database = Database();

  @override
  void initState() {
    super.initState();
    getPref();
  }

  getPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userName = prefs.getString("UName");
    if (prefs.containsKey("FpUPin")) {
      fpUserPin = prefs.getString("FpUPin");
      setState(() {
        shouldEnableFPLogin = true;
      });
    }
  }

  Future<void> _authenticateUser(BuildContext context) async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Please authenticate to Login",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
    }
    hasFPAuthPopupDisplayed = true;

    if (!mounted) return;

    isAuthenticated
        ? print('User is authenticated!')
        : print('User is not authenticated.');
    if (isAuthenticated) {
      BlocProvider.of<LoginBloc>(context).add(
        SignIn(username: userName, password: fpUserPin),
      );
    } else {

    }
  }

  storeCredentials(String token) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
   /* json.decode(
        ascii.decode(
        base64.decode(base64.normalize(token.split(".")[1]))));*/
    pref.setString("Token", token);
  }

  checkToInitiateFPAuthentication(BuildContext context) {
    if (shouldEnableFPLogin && !hasFPAuthPopupDisplayed) {
      Future.delayed(Duration(seconds: 1), () {
        _authenticateUser(context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0;

    navigateToNextPage() async{
      bool isInitialNavigationToDashboard = await Common().hasInitialNavigationToDashboardCompleted();
      if (isInitialNavigationToDashboard) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => Dashboard(
              repository: repository,
              appLanguage: appLanguage,
              database: database,
            )));
      }else{
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) =>
                FromAddressForm(
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                )));
      }
    }
    return BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(repository: repository),
        child: BlocListener<LoginBloc, LoginState>(listener: (context, state) {
          if (state is LoginInProgress) {
            Common().showLoader(context);
          } else if (state is LoginLoaded) {
            Navigator.pop(context);
            storeCredentials(state.loginResponse.accessToken);


            navigateToNextPage();
          } else if (state is LoginFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: "Please enter valid PIN",
                okFunction: () {
                  Navigator.pop(context);
//                      if (shouldEnableFPLogin){
//                        _authenticateUser(context);
//                      }
                });
          }
        }, child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
//          if (state is LoginInitial) {
          checkToInitiateFPAuthentication(context);
//          }
          return Scaffold(
              appBar: BCAppBar.setInitialAppBar(),
//              resizeToAvoidBottomPadding: false,
              body: Container(
                color: Colors.white,
                child: SafeArea(
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: contentPadding, right: contentPadding),
                      child: Center(
                          child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
//                                crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Image.asset(
                                "assets/common/bCore_logo/bCore_logo.png"),
                            SizedBox(height: 32),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Enter Your Pin"),
                              ],
                            ),
                            SizedBox(height: 16),
                            PinEntryTextField(
                              fields: 6,
                              fieldWidth: 45.0,
                              boxColor: BCAppTheme().headingTextColor,
                              textColor: BCAppTheme().headingTextColor,
                              isTextObscure: true,
                              showFieldAsBox: true,
                              onSubmit: (String pin) {
                                userPin = pin;
                              }, // end onSubmit
                            ),
                            SizedBox(
                                height: (safeDisplayHeight(context) > 568)
                                    ? 40
                                    : 32),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: BCoreButton(
                                  title: 'SIGN IN',
                                  onPressed: () {
                                    if (validateFields()) {
                                      BlocProvider.of<LoginBloc>(context).add(
                                          SignIn(
                                              username: userName,
                                              password: userPin));
                                    }
                                  }),
                            ),
                            SizedBox(
                                height: (safeDisplayHeight(context) > 568)
                                    ? 40
                                    : 32),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Forgot your PIN?",
                                    style: TextStyle(fontSize: 17)),
                                SizedBox(width: 8),
                                ButtonTheme(
                                    child: FlatButton(
                                        padding: EdgeInsets.all(0.0),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ForgotPassword(
                                                          repository:
                                                              repository,
                                                          appLanguage:
                                                              appLanguage,
                                                          processType: ResetType
                                                              .ResetPIN)));
                                        },
                                        child: Text("RESET NOW",
                                            style: TextStyle(
                                              fontSize: 17.0,
                                              color:
                                                  BCAppTheme().headingTextColor,
                                            ))))
                              ],
                            ),
                            SizedBox(
                                height: (safeDisplayHeight(context) > 568)
                                    ? 40
                                    : 32),
                            if (shouldEnableFPLogin)
                              Image.asset(
                                "assets/login/login_fp_icon/login_fp_icon.png",
                              )
                          ],
                        ),
                      ))),
                ),
              ));
        })));
  }

  validateFields() {
    if ((userPin.isEmpty) || (userPin.length != 6)) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please enter valid PIN",
          okFunction: () {
            Navigator.pop(context);
          });
      return false;
    } else {
      return true;
    }
  }
}
