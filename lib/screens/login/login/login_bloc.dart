import 'dart:async';

import 'package:bcore_inventory_management/models/response/login_resp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part '../login/login_event.dart';
part '../login/login_state.dart';


class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final BCRepository repository;

  LoginBloc({@required this.repository})
      : assert(repository != null),
        super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is SignIn) {
      yield LoginInProgress();

      try {
        final LoginResponse resp = await repository.login(username: event.username,password: event.password);
        yield LoginLoaded(loginResponse: resp);
        print(resp.accessToken);
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}