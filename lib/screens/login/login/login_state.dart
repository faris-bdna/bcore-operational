part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginInProgress extends LoginState {

}

class LoginLoaded extends LoginState {
  final LoginResponse loginResponse;

  const LoginLoaded({@required this.loginResponse}) : assert(loginResponse != null);

  @override
  List<Object> get props => [loginResponse];
}

class LoginFailure extends LoginState {
  final String error;

  const LoginFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}
