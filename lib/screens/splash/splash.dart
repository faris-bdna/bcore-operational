import 'dart:async';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/login/initialLogin/initialLogin_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  final BCRepository bcRepository;
  final AppLanguage appLanguage;

  const Splash({Key key, this.bcRepository, this.appLanguage})
      : super(key: key);
  @override
  SplashScreenState createState() =>
      new SplashScreenState(bcRepository, appLanguage);
}


class SplashScreenState extends State<Splash>
    with SingleTickerProviderStateMixin {
  final BCRepository repository;
  final AppLanguage appLanguage;
//  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  AnimationController _controller;
  Animation<double> _animation;
  CurvedAnimation _curve;

  SplashScreenState(this.repository, this.appLanguage);

  navigationPage(bool notification) async {
    await Future.delayed(Duration(seconds: 5), () {
      checkForInitialScreen();
    });
/*    await Future.delayed(Duration(seconds: 5), () {


    });*/
  }

  checkForInitialScreen() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    // if (pref.containsKey("hasConfiguredPin")) {
    //   if (pref.getBool("hasConfiguredPin")) {
    //     Navigator.pushReplacement(
    //         context, MaterialPageRoute(builder: (context) => Login()));
    //   } else{
    //     Navigator.pushReplacement(
    //         context, MaterialPageRoute(builder: (context) => InitialLogin()));
    //   }
    // } else{
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => InitialLogin()));
   // }

  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    _animation = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(_curve);
/*
    _firebaseMessaging.subscribeToTopic("topic");

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        navigationPage(true);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        navigationPage(true);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        print("Push Messaging token: $token");
      });
    });*/

    navigationPage(false);
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _controller
          .animateTo(0.0)
          .then<TickerFuture>((value) => _controller.animateBack(1.0));
    });
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          FadeTransition(
            opacity: _animation,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  'assets/common/common_logo/common_logo.png',
                  fit: BoxFit.scaleDown,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
