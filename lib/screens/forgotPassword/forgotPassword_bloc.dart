import 'dart:async';

import 'package:bcore_inventory_management/models/response/forgotUserPin_resp.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';

part 'forgotPassword_event.dart';
part 'forgotPassword_state.dart';


class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  final BCRepository repository;

  ForgotPasswordBloc({@required this.repository})
      : assert(repository != null),
        super(ForgotPasswordInitial());

  @override
  Stream<ForgotPasswordState> mapEventToState(ForgotPasswordEvent event) async* {
    if (event is ForgotUserPin) {
      yield ForgotPasswordInProgress();

      try {
        final ForgotUserPinResponse resp = await repository.forgotUserPin(username: event.username);
        yield ForgotPasswordLoaded(forgotUserPinResponse: resp);
      } catch (error) {
        yield ForgotPasswordFailure(error: error.toString());
      }
    }
  }
}