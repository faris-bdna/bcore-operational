part of 'forgotPassword_bloc.dart';

abstract class ForgotPasswordState extends Equatable {
  const ForgotPasswordState();

  @override
  List<Object> get props => [];
}

class ForgotPasswordInitial extends ForgotPasswordState {}

class ForgotPasswordInProgress extends ForgotPasswordState {

}

class ForgotPasswordLoaded extends ForgotPasswordState {
  final ForgotUserPinResponse forgotUserPinResponse;

  const ForgotPasswordLoaded({@required this.forgotUserPinResponse}) : assert(forgotUserPinResponse != null);

  @override
  List<Object> get props => [forgotUserPinResponse];
}

class ForgotPasswordFailure extends ForgotPasswordState {
  final String error;

  const ForgotPasswordFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ForgotPasswordFailure { error: $error }';
}
