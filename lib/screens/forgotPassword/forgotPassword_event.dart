part of 'forgotPassword_bloc.dart';

abstract class ForgotPasswordEvent extends Equatable {
  const ForgotPasswordEvent();
}

class ForgotUserPin extends ForgotPasswordEvent {
  final String username;

  const ForgotUserPin({
    @required this.username,
  });

  @override
  List<Object> get props => [username];

  @override
  String toString() =>
      'ForgotUserPinSelected {"Username" : "$username"}';
}
