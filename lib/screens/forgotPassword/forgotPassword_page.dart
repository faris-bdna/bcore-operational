
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'forgotPassword_bloc.dart';
import 'otpGeneration/otpGeneration_page.dart';

enum ResetType{ ResetPIN, ResetPassword }

class ForgotPassword extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final ResetType processType;

  ForgotPassword(
      {Key key,  this.repository, this.appLanguage, this.processType})
      : assert(repository != null),
        super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState(repository,appLanguage);
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  double contentPadding;

  TextEditingController _usernameController = TextEditingController();

  _ForgotPasswordState(this.repository, this.appLanguage);

  @override
  Widget build(BuildContext context) {

    return BlocProvider<ForgotPasswordBloc>(
        create: (context) => ForgotPasswordBloc(repository: repository),
        child: BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
            listener: (context, state) {
              if (state is ForgotPasswordInProgress){
                Common().showLoader(context);
              }
              else if (state is ForgotPasswordLoaded){
                Navigator.pop(context);
                if (state.forgotUserPinResponse.statusCode == 404){
                  Common().showAlertMessage(
                      context: context,
                      title: "Sorry",
                      message: state.forgotUserPinResponse.statusMessage,
                      okFunction: (){
                        Navigator.pop(context);
                      }
                  );
                } else {
                  print(state.forgotUserPinResponse.oTP);
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>
                          OTPGeneration(appLanguage: appLanguage,
                              repository: repository,
                              userPinResponse: state.forgotUserPinResponse,
                              userName: _usernameController.text.trim(),
                              processType: widget.processType)));
                }
              }
              else if (state is ForgotPasswordFailure) {
                Navigator.pop(context);
                Common().showAlertMessage(
                    context: context,
                    title: "Sorry",
                    message:"Please try again",
                    okFunction: (){
                      Navigator.pop(context);
                    }
                );
              }
            },
            child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
                builder: (context, state) {
                  contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;

                  return Scaffold(
                      appBar: BCAppBar.setInitialAppBar(),
//                      resizeToAvoidBottomPadding: false,
                      body: Container(
                        color: Colors.white,
                        child: SafeArea(
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                                padding: EdgeInsets.only(left: contentPadding,right: contentPadding),
                                child: Center(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Image.asset("assets/common/bCore_logo/bCore_logo.png"),
                                          SizedBox(height: 80),
                                          Container(
                                              width: 320,
                                              padding: EdgeInsets.all(0.0),
                                              child: TextField(
                                                controller: _usernameController,
                                                autocorrect: false,
                                                decoration: InputDecoration(
                                                  hintText: 'User Name',
                                                  prefixIcon: Icon(Icons.mail_outline,color: isUsernameEmpty() ? BCAppTheme().boarderColor : BCAppTheme().headingTextColor ),
                                                  hintStyle: TextStyle(color: Colors.grey),
                                                  filled: true,
                                                  fillColor: Colors.white70,
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                                    borderSide: BorderSide(color: isUsernameEmpty() ? BCAppTheme().boarderColor : BCAppTheme().headingTextColor, width: 0.5),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                                    borderSide: BorderSide(color: Color(hexToColor("#CEA406")), width: 0.5),
                                                  ),
                                                ),
                                                keyboardType: TextInputType.emailAddress,
//                                                onEditingComplete: (){
//                                                  setState(() {
//                                                  });
//                                                },
                                              )
                                          ),
                                          SizedBox(height: 40),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                                            child: BCoreButton(
                                                title: 'RESET NOW',
                                                onPressed: (){
                                                  if (_usernameController.text.trim().length != 0 ) {
                                                    BlocProvider.of<
                                                        ForgotPasswordBloc>(context)
                                                        .add(ForgotUserPin(
                                                      username: _usernameController
                                                          .text.trim(),
                                                    ),
                                                    );
                                                  }else{
                                                    Common().showAlertMessage(
                                                        context: context,
                                                        title: "Sorry",
                                                        message:"Please enter your user name",
                                                        okFunction: (){
                                                          Navigator.pop(context);
                                                        }
                                                    );
                                                  }
                                                }
                                            ),
                                          ),
                                          SizedBox(height: 24),
                                          FittedBox(
                                            fit: BoxFit.fitWidth,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("Don't need password reset? or ", style: TextStyle(fontSize: 16)),
                                                ButtonTheme(
                                                  minWidth: 10,
                                                  child: FlatButton(
                                                      padding: EdgeInsets.all(0.0),
                                                      onPressed: (){
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text("SIGN IN",
                                                          style: TextStyle(
                                                            fontSize: 16.0,
                                                            color: Color(hexToColor("#CEA406")),
                                                          )
                                                      )
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 40),
                                        ],
                                      ),
                                    )
                                )
                            ),
                          ),
                        ),
                      )
                  );
                })
        ));
  }

  isUsernameEmpty() => _usernameController.text.trim().isEmpty;

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}
