import 'dart:async';

import 'package:bcore_inventory_management/models/response/resetPassword_resp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'resetPassword_event.dart';
part 'resetPassword_state.dart';


class ResetPasswordBloc extends Bloc<ResetPasswordEvent, ResetPasswordState> {
  final BCRepository repository;

  ResetPasswordBloc({@required this.repository})
      : assert(repository != null),
        super(ResetPasswordInitial());

  @override
  Stream<ResetPasswordState> mapEventToState(ResetPasswordEvent event) async* {
    if (event is ChangePassword) {
      yield ResetPasswordInProgress();

      try {
        final ResetPasswordResponse resp = await repository.resetPassword(username: event.uname, password: event.password);
        yield ResetPasswordLoaded(resetPasswordResponse: resp);
      } catch (error) {
        yield ResetPasswordFailure(error: error.toString());
      }
    }
  }
}