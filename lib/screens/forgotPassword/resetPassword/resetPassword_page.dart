
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/screens/forgotPassword/resetPassword/resetPassword_bloc.dart';
import 'package:bcore_inventory_management/screens/login/initialLogin/initialLogin_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';

class ResetPassword extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final String userName;

  ResetPassword({Key key, this.repository, this.appLanguage, this.userName})
      : assert(repository != null),
        super(key: key);

  @override
  _ResetPasswordState createState() =>
      _ResetPasswordState(repository, appLanguage, userName);
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final BCRepository repository;
  final AppLanguage appLanguage;
  final String userName;
  double contentPadding;

  _ResetPasswordState(this.repository, this.appLanguage, this.userName);

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;
    return BlocProvider<ResetPasswordBloc>(
        create: (context) => ResetPasswordBloc(repository: repository),
        child: BlocListener<ResetPasswordBloc, ResetPasswordState>(
            listener: (context, state) {
              if (state is ResetPasswordInProgress) {
                Common().showLoader(context);
              }
              else if (state is ResetPasswordLoaded) {
                Navigator.pop(context);
                Navigator.pushAndRemoveUntil(context,
                    MaterialPageRoute(builder: (context) => InitialLogin()), (
                        route) => false);
              }
              else if (state is ResetPasswordFailure) {
                Navigator.pop(context);
                Common().showAlertMessage(
                    context: context,
                    title: "Sorry",
                    message: "Please try again",
                    okFunction: () {Navigator.pop(context);}
                );
              }
            },
            child: BlocBuilder<ResetPasswordBloc, ResetPasswordState>(
                builder: (context, state) {
                  contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;

                  return Scaffold(
//                      resizeToAvoidBottomPadding: false,
                      appBar: BCAppBar.setInitialAppBar(),
                      body: Container(
                        color: Colors.white,
                        child: SafeArea(
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                                padding: EdgeInsets.only(left: contentPadding,right: contentPadding),
                                child: Center(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child:
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .stretch,
                                        children: [
                                          Image.asset(
                                              "assets/common/bCore_logo/bCore_logo.png"),
                                          SizedBox(height: 80),
                                          Text("Please Enter new password"),
                                          SizedBox(height: 16),
                                          Container(
                                              width: 320,
                                              padding: EdgeInsets.all(0.0),
                                              child: TextField(
                                                textInputAction:  TextInputAction.next,
                                                controller: _passwordController,
                                                enableInteractiveSelection: false,
                                                autocorrect: false,
                                                obscureText: true,
                                                decoration: InputDecoration(
                                                  hintText: 'Password',
                                                  prefixIcon: Icon(Icons.lock_outline,
                                                      color: isPasswordEmpty()
                                                          ? BCAppTheme().boarderColor
                                                          : BCAppTheme().headingTextColor),
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey),
                                                  filled: true,
                                                  fillColor: Colors.white70,
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(5.0)),
                                                    borderSide: BorderSide(
                                                        color: isPasswordEmpty()
                                                            ? BCAppTheme()
                                                            .boarderColor
                                                            : BCAppTheme()
                                                            .headingTextColor,
                                                        width: 0.5),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(5.0)),
                                                    borderSide: BorderSide(
                                                        color: BCAppTheme()
                                                            .headingTextColor,
                                                        width: 0.5),
                                                  ),
                                                ),
                                                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                                              )
                                          ),
                                          SizedBox(height: 16),
                                          Container(
                                              width: 320,
                                              padding: EdgeInsets.all(0.0),
                                              child: TextField(
                                                controller: _confirmPasswordController,
                                                enableInteractiveSelection: false,
                                                autocorrect: false,
                                                obscureText: true,
                                                decoration: InputDecoration(
                                                  hintText: 'Confirm Password',
                                                  prefixIcon: Icon(Icons.lock_outline,
                                                      color: isConfirmPasswordEmpty()
                                                          ? BCAppTheme().boarderColor
                                                          : BCAppTheme()
                                                          .headingTextColor),
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey),
                                                  filled: true,
                                                  fillColor: Colors.white70,
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(5.0)),
                                                    borderSide: BorderSide(
                                                        color: isConfirmPasswordEmpty()
                                                            ? BCAppTheme()
                                                            .boarderColor
                                                            : BCAppTheme()
                                                            .headingTextColor,
                                                        width: 0.5),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(5.0)),
                                                    borderSide: BorderSide(
                                                        color: BCAppTheme()
                                                            .headingTextColor,
                                                        width: 0.5),
                                                  ),
                                                ),
//                                                onEditingComplete: () {
//                                                  setState(() {});
//                                                },
                                              )
                                          ),
                                          SizedBox(height: 40),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8.0),
                                            child: BCoreButton(
                                                title: 'CONFIRM',
                                                onPressed: () {
                                                  if (validateFields()) {
                                                    BlocProvider.of<
                                                        ResetPasswordBloc>(context)
                                                        .add(ChangePassword(
                                                        uname: this.userName,
                                                        password: _confirmPasswordController
                                                            .text.trim()));
                                                  }
                                                }
                                            ),
                                          ),
                                          if (safeDisplayHeight(context) > 568)
                                            SizedBox(height: 40)
                                        ],
                                      ),
                                    )
                                )

                            ),
                          ),
                        ),
                      )
                  );
                })
        ));
  }

  validateFields() {
    if (isPasswordEmpty()) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please enter your Password",
          okFunction: () {Navigator.pop(context);}
      );
      return false;
    } else if (isConfirmPasswordEmpty()) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Please Confirm your Password",
          okFunction: () {Navigator.pop(context);}
      );
      return false;
    } else if (_passwordController.text.trim() !=
        _confirmPasswordController.text.trim()) {
      Common().showAlertMessage(
          context: context,
          title: "Sorry",
          message: "Password mismatch",
          okFunction: () {Navigator.pop(context);}
      );
      return false;
    } else{
      return true;
    }
  }

  isConfirmPasswordEmpty() =>_confirmPasswordController.text.trim().isEmpty;
  isPasswordEmpty() => _passwordController.text.trim().isEmpty;
}
