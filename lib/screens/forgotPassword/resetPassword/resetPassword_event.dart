part of 'resetPassword_bloc.dart';

abstract class ResetPasswordEvent extends Equatable {
  const ResetPasswordEvent();
}

class ChangePassword extends ResetPasswordEvent {
  final String uname;
  final String password;

  const ChangePassword({
    @required this.uname, @required this.password,
  });

  @override
  List<Object> get props => [uname,password];

  @override
  String toString() =>
      'ResetPassword {"Username" : "$uname", "Password" : "$password"}';
}
