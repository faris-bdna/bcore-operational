part of 'resetPassword_bloc.dart';

abstract class ResetPasswordState extends Equatable {
  const ResetPasswordState();

  @override
  List<Object> get props => [];
}

class ResetPasswordInitial extends ResetPasswordState {}

class ResetPasswordInProgress extends ResetPasswordState {

}

class ResetPasswordLoaded extends ResetPasswordState {
  final ResetPasswordResponse resetPasswordResponse;

  const ResetPasswordLoaded({@required this.resetPasswordResponse}) : assert(resetPasswordResponse != null);

  @override
  List<Object> get props => [resetPasswordResponse];
}

class ResetPasswordFailure extends ResetPasswordState {
  final String error;

  const ResetPasswordFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ResetPasswordFailure { error: $error }';
}
