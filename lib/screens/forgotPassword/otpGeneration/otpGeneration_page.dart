
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/common_widget/pin_entry_text_field.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/response/forgotUserPin_resp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/forgotPassword/resetPassword/resetPassword_page.dart';
import 'package:bcore_inventory_management/screens/pinConfiguration/pinConfiguration_page.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../forgotPassword_bloc.dart';
import '../forgotPassword_page.dart';

class OTPGeneration extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final String userName;
  final ForgotUserPinResponse userPinResponse;
  final ResetType processType;

  OTPGeneration(
      {Key key,
      this.repository,
      this.appLanguage,
      this.userPinResponse,
      this.userName,
      this.processType})
      : assert(repository != null),
        super(key: key);

  @override
  _OTPGenerationState createState() =>
      _OTPGenerationState(repository, appLanguage, userPinResponse, userName);
}

class _OTPGenerationState extends State<OTPGeneration> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  ForgotUserPinResponse userPinResponse;
  final String userName;
  String otpEntered;
  num pinFieldWidth;
  bool hasOtpExpired = false;
  double contentPadding;

  _OTPGenerationState(
      this.repository, this.appLanguage, this.userPinResponse, this.userName);

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;
    return BlocProvider<ForgotPasswordBloc>(
        create: (context) => ForgotPasswordBloc(repository: repository),
        child: BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
            listener: (context, state) {
          if (state is ForgotPasswordInProgress) {
            Common().showLoader(context);
          }
          else if (state is ForgotPasswordLoaded) {
            Navigator.pop(context);
            setState(() {
              setState(() {
                hasOtpExpired = false;
              });
              userPinResponse = state.forgotUserPinResponse;
              print(userPinResponse.oTP);
            });
          }
          else if (state is ForgotPasswordFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: "Please try again",
                okFunction: () {
                  Navigator.pop(context);
                });
          }
        }, child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
                builder: (context, state) {
          return Scaffold(
              appBar: BCAppBar.setInitialAppBar(),
//              resizeToAvoidBottomPadding: false,
              body: Container(
                color: Colors.white,
                child: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(left: contentPadding,right: contentPadding),
                    child: Center(
                child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Image.asset(
                            "assets/common/bCore_logo/bCore_logo.png"),
                        SizedBox(height: 32),
                        Text(
                            "OTP has been sent to your registered email address",
                            style: TextStyle(fontSize: 13),
                            textAlign: TextAlign.center),
                        SizedBox(height: 32),
                        Visibility(
                            visible: (this.hasOtpExpired == false),
                            child: _getOTPWidget()),
                        _getTimerText(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Didn't Receive OTP?",
                                style: TextStyle(fontSize: 17)),
                            SizedBox(width: 8),
                            ButtonTheme(
                              child: FlatButton(
                                  padding: EdgeInsets.all(0.0),
                                  onPressed: () {
                                    setState(() {
                                      hasOtpExpired = true;
                                    });
                                    BlocProvider.of<ForgotPasswordBloc>(
                                            context)
                                        .add(
                                      ForgotUserPin(
                                        username: this.userName.trim(),
                                      ),
                                    );
                                  },
                                  child: Text("RESEND OTP",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(
                                            hexToColor("#CEA406"),
                                          ),
                                      ))),
                            )
                          ],
                        ),
                        if (safeDisplayHeight(context) > 568)
                          SizedBox(height: 40),
                      ],
                    ),
                )
                    )
                  ),
                ),
              ));
        })));
  }

  _getTimerText() {
    return (hasOtpExpired != true)
        ? CountdownFormatted(
            duration: Duration(seconds: int.parse(userPinResponse.expiresIn)),
            onFinish: () {
              hasOtpExpired = true;
              setState(() {});
            },
            builder: (BuildContext ctx, String remaining) {
              return Text('OTP will expire in $remaining',
                  style: TextStyle(fontSize: 13, color: Colors.red),
                  textAlign: TextAlign.center);
            },
          )
        : Text('OTP has been expired',
            style: TextStyle(fontSize: 13, color: Colors.red),
            textAlign: TextAlign.center);
  }

  _getOTPWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text("Please Enter 6 Digit OTP Code",
            style: TextStyle(fontSize: 16), textAlign: TextAlign.left),
        SizedBox(height: 32),
        PinEntryTextField(
          fields: 6,
          fieldWidth: 45.0,
          boxColor: BCAppTheme().headingTextColor,
          textColor: BCAppTheme().headingTextColor,
          isTextObscure: true,
          showFieldAsBox: true,
          onSubmit: (String pin) {
            otpEntered = pin;
          }, // end onSubmit
        ),
        SizedBox(height: 40),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: BCoreButton(
              title: 'VERIFY NOW',
              onPressed: () {
                if (userPinResponse.oTP == otpEntered) {
                  if (widget.processType == ResetType.ResetPassword) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResetPassword(
                                  repository: repository,
                                  appLanguage: appLanguage,
                                  userName: userName,
                                )));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PinConfiguration(
                                  repository: repository,
                                  appLanguage: appLanguage,
                                  processType: ConfigurationType.ResetPin,
                                )));
                  }
                } else {
                  Common().showAlertMessage(
                      context: context,
                      title: "Sorry",
                      message: "Invalid OTP",
                      okFunction: () {
                        Navigator.pop(context);
                      });
                }
              }),
        ),
        SizedBox(height: 32),
      ],
    );
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}
