import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/response/accountsResp.dart';
import 'package:bcore_inventory_management/models/response/salesType_resp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSOListResp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart'
as cmnMaster;

import 'client_bloc.dart';
import 'package:intl/intl.dart';

class ClientForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const ClientForm({Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _ClientFormState createState() => _ClientFormState(repository);
}

class _ClientFormState extends State<ClientForm> {
  final BCRepository repository;

  List<SalesTypeList> _salesType = [];

  // Should contain or not
  bool containType, containTransactionDate, containAccountType = false;
  bool containBAorClient, containSalesman, containExpDeliveryDate = false;
  bool containSalesOrder, containAddressInfo = false;

  EditMode editMode;

  _ClientFormState(this.repository);

  ClientBloc _clientBloc;

  List<cmnMaster.CommonDataList> salesmenList = [];
  List<DropdownMenuItem> salesMenDDList = [];
  List<AccountsList> accountsDDList = [];
  List<cmnMaster.CommonDataList> accType = [];
  List<SalesOrderList> salesOrderList = [];

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.Off) {
      // if (widget.type != TransactionType.DN) {
      setDefaultTransactionDate();
      // }
    }
  }

  setDefaultTransactionDate() {
    cmnSalesClientInfo.entryDate = Utils.getDateByFormat(
        passedDateTime: DateTime.now(),
        format: fromAddress.fromCompanyDateFormat);
  }

  @override
  void initState() {
    super.initState();

    _configUI();
    loadItemDetailsIfInEditMode();
  }

  _configUI() {
    if ((cmnSalesClientInfo != null) &&
        (cmnSalesClientInfo.entryDate != null)) {
      editMode = EditMode.On;
    } else {
      editMode = EditMode.Off;
    }

    switch (widget.type) {
      case TransactionType.SO:
        containTransactionDate = true;
        containType = true;
        containAccountType = true;
        containBAorClient = true;
        containSalesman = true;
        containExpDeliveryDate = true;
        containSalesOrder = false;
        containAddressInfo = false;
        break;
      case TransactionType.SAI:
        containTransactionDate = true;
        containType = false;
        containAccountType = true;
        containBAorClient = true;
        containSalesman = true;
        containExpDeliveryDate = false;
        containSalesOrder = false;
        containAddressInfo = false;
        break;
      case TransactionType.DN:
        containTransactionDate = false;
        containType = false;
        containAccountType = false;
        containBAorClient = false;
        containSalesman = false;
        containExpDeliveryDate = false;
        containSalesOrder = true;
        containAddressInfo = true;
        break;
    }
  }

  Future<DateTime> showEntryDatePicker() async {
    DateTime firstDate = await Common().getBackDateAllowed()
        ? DateTime.now().subtract(Duration(days: 365))
        : DateTime.now();

    DateTime date = await showDatePicker(
      context: context,
      helpText: (widget.type == TransactionType.SO)
          ? "Select Sales Order Date"
          : "Select Entry Date",
      initialDate: DateTime.now(),
      firstDate: firstDate,
      lastDate: DateTime.now(),
    );
    return date;
  }

  Future<DateTime> showExpDeliveryDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Expected Delivery Date",
      initialDate: _getInitialDate(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );
    return date;
  }

  /// Can be used if needed
  _getInitialDate() {
    if (cmnSalesClientInfo.expectedDelivDate != null) {
      var initialDate = DateFormat(fromAddress.fromCompanyDateFormat)
          .parse(cmnSalesClientInfo.expectedDelivDate);
      return initialDate;
    } else {
      return DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ClientBloc>(
      create: (context) => ClientBloc(repository: repository),
      child: BlocListener<ClientBloc, ClientState>(
        listener: (context, state) {
          if (state is LoadSalesTypeInProgress ||
              state is LoadAccountsInProgress ||
              state is LoadAccountTypeInProgress ||
              state is GetSalesOrderListInProgress ||
              state is LoadSalesManInProgress) {
            Common().showLoader(context);
          } else if (state is LoadSalesTypeFailure ||
              state is LoadAccountsFailure ||
              state is LoadAccountTypeFailure ||
              state is GetSalesOrderListFailure ||
              state is LoadSalesManFailure) {
            Navigator.pop(context);
          } else if (state is LoadSalesTypeComplete) {
            Navigator.pop(context);
            _salesType = state.salesTypeResponse.salesTypeList;

            if (containType) {
              if (editMode == EditMode.On) {
                _clientBloc.add(LoadAccountType());
              } else {
                Future.delayed(Duration.zero, () {
                  showTypesDialogue();
                });
              }
            }
          } else if (state is LoadAccountTypeComplete) {
            Navigator.pop(context);

            accType = state.commonResp.masterDataList;
            if (state.commonResp.masterDataList.isNotEmpty) {
              if (editMode == EditMode.Off) {
                /// Checks whether the selected sales type is Credit or not
                if (cmnSalesClientInfo.salesTypeId != null &&
                    int.tryParse(cmnSalesClientInfo.salesTypeId) == 2) {
                  /// search for Customer Bill Type
                  var billingPartyWithCustomerType = state
                      .commonResp.masterDataList
                      .where((party) => (int.tryParse(party.value) == 3))
                      .toList()[0];
                  if (billingPartyWithCustomerType != null) {
                    cmnSalesClientInfo.billingPartyType =
                        int.parse(billingPartyWithCustomerType.value);
                    cmnSalesClientInfo.billingPartyTypeName =
                        billingPartyWithCustomerType.text;
                  } else {
                    cmnSalesClientInfo.billingPartyType =
                        int.parse(state.commonResp.masterDataList[0].value);
                    cmnSalesClientInfo.billingPartyTypeName =
                        state.commonResp.masterDataList[0].text;
                  }
                } else {
                  cmnSalesClientInfo.billingPartyType =
                      int.parse(state.commonResp.masterDataList[0].value);
                  cmnSalesClientInfo.billingPartyTypeName =
                      state.commonResp.masterDataList[0].text;
                }
              }

              accountsDDList.clear();
              _clientBloc.add(LoadAccounts(
                  contractType: cmnSalesClientInfo.billingPartyType,
                  companyId: int.parse(FromAddress.shared.fromCompanyId)));
            }
          } else if (state is LoadAccountsComplete) {
            Navigator.pop(context);

            if (state.accountsResp != null &&
                state.accountsResp.accountsList.length > 0) {
              accountsDDList = [];
              setState(() {
                accountsDDList = state.accountsResp.accountsList;

                if (editMode == EditMode.Off) {
                  cmnSalesClientInfo.billingPartyID =
                      accountsDDList[0].clientTypeId;
                  cmnSalesClientInfo.accountName =
                      accountsDDList[0].accountName;
                }
                // if (editMode == EditMode.On) {
                // salesmenList.clear();
                // salesMenDDList.clear();
                _clientBloc.add(LoadSalesMan(
                    clientTypeID: cmnSalesClientInfo.billingPartyType,
                    customerId: cmnSalesClientInfo.billingPartyID,
                    companyId: int.parse(FromAddress.shared.fromCompanyId)));
                // }
              });
            }
          } else if (state is LoadSalesManComplete) {
            Navigator.pop(context);
            if (state.getSalesmanResponse != null &&
                state.getSalesmanResponse.masterDataList.length > 0) {
              salesMenDDList = [];
              salesmenList = [];

              setState(() {
                salesmenList = state.getSalesmanResponse.masterDataList;
                salesMenDDList =
                    salesmenList.map((cmnMaster.CommonDataList item) {
                  return DropdownMenuItem(
                      child: Text(item.text ?? "",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: BCAppTheme().subTextColor,
                          )),
                      value: item.text ?? "");
                }).toList();

                if (editMode == EditMode.Off) {
                  cmnSalesClientInfo.salesmanId =
                      int.tryParse(salesmenList[0].value) ?? 0;
                  cmnSalesClientInfo.salesmanText = salesmenList[0].text;
                }
              });
            }
          } else if (state is GetSalesOrderListComplete) {
            Navigator.pop(context);

            salesOrderList = state.resp.masterDataList;
          }
        },
        child: BlocBuilder<ClientBloc, ClientState>(builder: (context, state) {
          _clientBloc = BlocProvider.of<ClientBloc>(context);

          if (state is GetSalesTypeInitial) {
            if (widget.type == TransactionType.SO) {
              _clientBloc.add(LoadSalesType());
            } else if (widget.type == TransactionType.SAI) {
              _clientBloc.add(LoadAccountType());
            } else if (widget.type == TransactionType.DN) {
              _clientBloc.add(GetSalesOrderList());
            }
          }
          return Container(
            child: Column(
              children: [
                if (containType) _getTypes(),
                if (containTransactionDate) _getTransactionDate(),
                if (containAccountType) _getAccountTypes(),
                if (containBAorClient) _getBAorClient(),
                if (containSalesman) _getSalesman(),
                if (containExpDeliveryDate) _getExpDeliveryDate(),
                if (containSalesOrder) _getSalesOrder(),
                if (containAddressInfo) _getAddressInfo(),
                _getRemarksWidget()
              ],
            ),
          );
        }),
      ),
    );
  }

  _getTypes() {
    String typeText;

    switch (widget.type) {
      case TransactionType.SO:
        typeText = (cmnSalesClientInfo.selSalesType == null)
            ? ""
            : cmnSalesClientInfo.selSalesType;
        break;
    }

    return Row(children: [
      SizedBox(width: 8),
      Text("Type :"),
      FlatButton(
          child: Text(typeText,
              style: TextStyle(
                  decoration: TextDecoration.underline,
                  color: BCAppTheme().headingTextColor,
                  fontWeight: FontWeight.bold)),
          onPressed: () => showTypesDialogue())
    ]);
  }

  _getTransactionDate() {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: DateFormField(
            initialValue: cmnSalesClientInfo.entryDate,
            format: fromAddress.fromCompanyDateFormat,
            showPicker: showEntryDatePicker,
            onDateChanged: (DateTime date) {
              cmnSalesClientInfo.entryDate = Utils.getDateByFormat(
                  passedDateTime: date,
                  format: fromAddress.fromCompanyDateFormat);
            },
            decoration: Common()
                .getInputDecoration(cmnSalesClientInfo.entryDate == null, ""),
          ),
        ),
      ],
    );
  }

  clearSalesman() {
    cmnSalesClientInfo.salesmanId = null;
    cmnSalesClientInfo.salesmanText = null;
    salesmenList.clear();
    salesMenDDList.clear();
  }

  clearCustomer() {
    cmnSalesClientInfo.billingPartyID = null;
    cmnSalesClientInfo.accountName = null;
    accountsDDList.clear();
  }

  /// Get "Account Types" such as Business Accounts, Contact, Customer etc.
  _getAccountTypes() {
    return Column(
      children: [
        SizedBox(height: 8),
        Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: -4.0,
          alignment: WrapAlignment.center,
          children: List<Widget>.generate(
            accType.length,
            (int index) {
              return ChoiceChip(
                selectedColor: BCAppTheme().primaryColor,
                backgroundColor: Colors.black12,
                label: Text('${accType[index].text}',
                    style: TextStyle(fontSize: 12, color: Colors.white)),
                selected: cmnSalesClientInfo.billingPartyType ==
                    int.parse(accType[index].value),
                onSelected: (bool selected) {
                  if (selected) {
                    setState(() {
                      cmnSalesClientInfo.billingPartyType =
                          selected ? int.parse(accType[index].value) : null;
                      cmnSalesClientInfo.billingPartyTypeName =
                          selected ? accType[index].text : null;
                      // clearCustomer();
                      // clearSalesman();
                      _clientBloc.add(LoadAccounts(
                          contractType: cmnSalesClientInfo.billingPartyType,
                          companyId:
                              int.parse(FromAddress.shared.fromCompanyId)));
                    });
                  }
                },
              );
            },
          ).toList(),
        ),
      ],
    );
  }

  /// Get "Select Customer" field
  _getBAorClient() {
    return Column(
      children: [
        SizedBox(height: 8.0),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: (cmnSalesClientInfo.billingPartyID != null)),
          child: SearchableDropdown.single(
            value: cmnSalesClientInfo.accountName,
            items: (accountsDDList != null && accountsDDList.isNotEmpty)
                ? accountsDDList.map((AccountsList item) {
                    return DropdownMenuItem(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(item.accountName,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: BCAppTheme().subTextColor,
                                  )),
                              Text(item.name,
                                  style: TextStyle(
                                    fontSize: 10.0,
                                    color: BCAppTheme().grayColor,
                                  )),
                            ]),
                        value: item.accountName);
                  }).toList()
                : [],
            hint: cmnSalesClientInfo.billingPartyType == null
                ? ""
                : "Select ${cmnSalesClientInfo.billingPartyTypeName ?? ""}",
            searchHint: cmnSalesClientInfo.billingPartyType == null
                ? ""
                : "Select ${cmnSalesClientInfo.billingPartyTypeName ?? ""}",
            onChanged: (value) {
              setState(() {
                AccountsList selectedAccount = accountsDDList
                    .where((account) => account.accountName == value)
                    .toList()[0];
                cmnSalesClientInfo.billingPartyID =
                    selectedAccount.clientTypeId;
                cmnSalesClientInfo.accountName = selectedAccount.accountName;

                // clearSalesman();

                _clientBloc.add(LoadSalesMan(
                    clientTypeID: cmnSalesClientInfo.billingPartyType,
                    customerId: selectedAccount.clientTypeId,
                    companyId: int.parse(FromAddress.shared.fromCompanyId)));
              });
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getSalesman() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: (cmnSalesClientInfo.salesmanId != null)),
          child: SearchableDropdown.single(
            value: cmnSalesClientInfo.salesmanText,
            items: (salesmenList != null && salesmenList.isNotEmpty)
                ? salesmenList.map((cmnMaster.CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text ?? "",
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text ?? "");
                  }).toList()
                : [],
            // items: salesMenDDList,
            hint: "Select Salesman",
            searchHint: "Select Salesman",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  cmnMaster.CommonDataList selectedSalesman = salesmenList
                      .where((salesman) => salesman.text == value)
                      .toList()[0];
                  cmnSalesClientInfo.salesmanId =
                      int.tryParse(selectedSalesman.value) ?? -1;
                  cmnSalesClientInfo.salesmanText = selectedSalesman.text;
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getSalesOrder() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: (cmnSalesClientInfo.salesOrderNo != null)),
          child: SearchableDropdown.single(
            value: cmnSalesClientInfo.salesOrderNo == null
                ? null
                : cmnSalesClientInfo.salesOrderNo.toString(),
            items: (salesOrderList != null && salesOrderList.isNotEmpty)
                ? salesOrderList.map((SalesOrderList salesOrder) {
                    return DropdownMenuItem(
                        child: Text(salesOrder.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: salesOrder.text);
                  }).toList()
                : [],
            hint: "Select Sales Order",
            searchHint: "Select Sales Order",
            onChanged: (value) {
              setState(() {
                SalesOrderList salesOrder = salesOrderList
                    .where((order) => order.text == value)
                    .toList()[0];
                cmnSalesClientInfo.salesOrderId =
                    int.tryParse(salesOrder.value) ?? -1;
                cmnSalesClientInfo.salesOrderNo = salesOrder.text;
              });
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getExpDeliveryDate() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: DateFormField(
            format: fromAddress.fromCompanyDateFormat,
            showPicker: showExpDeliveryDatePicker,
            initialValue: cmnSalesClientInfo.expectedDelivDate,
            onDateChanged: (DateTime date) {
              cmnSalesClientInfo.expectedDelivDate = Utils.getDateByFormat(
                  passedDateTime: date,
                  format: fromAddress.fromCompanyDateFormat);
            },
            decoration:
                Common().getInputDecoration(false, 'Expected Delivery Date'),
          ),
        )
      ],
    );
  }

  _getRemarksWidget() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            // controller: _remarksController,
            controller: TextEditingController()
              ..text = cmnSalesClientInfo.orderRemarks,
            autocorrect: false,
            decoration:
                Common().getInputDecoration(false, 'Remarks', icon: null),
            keyboardType: TextInputType.emailAddress,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
            onChanged: (value) {
              cmnSalesClientInfo.orderRemarks = value;
            },
          ),
        ),
        SizedBox(height: 16),
      ],
    );
  }

  /// Gets called when user selects a transaction type from the popup
  changeType(BuildContext dialogContext, int buttonIndex) {
    Navigator.pop(dialogContext);
    _clientBloc.add(LoadAccountType());
    setState(() {
      switch (widget.type) {
        case TransactionType.SO:
          cmnSalesClientInfo.selSalesType = _salesType[buttonIndex].text;
          cmnSalesClientInfo.salesTypeId = _salesType[buttonIndex].value;
          break;
      }
    });
  }

  showTypesDialogue() {
    String dialogTitle;

    /// Add new case for adding new Type
    switch (widget.type) {
      case TransactionType.SO:
        dialogTitle = "Sales Types";
        break;
    }

    Dialog typeAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Stack(alignment: Alignment.topRight, children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            height: 65,
            child: Column(
              children: [
                Text(dialogTitle, textAlign: TextAlign.center),
                SizedBox(height: 16.0),
                getDialogContent()
              ],
            ),
          ),
        ),
        Positioned(
            child: SizedBox(
          width: 35,
          height: 35,
          child: IconButton(
              icon: Icon(Icons.close, color: BCAppTheme().redColor),
              onPressed: () {
                Navigator.pop(context);
                // changeType(context, 0);
                _clientBloc.add(LoadAccountType());
                cmnSalesClientInfo.selSalesType = _salesType[0].text;
                cmnSalesClientInfo.salesTypeId = _salesType[0].value;
              }),
        )),
      ]),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return typeAlert;
      },
    ).then((val) {
      switch (widget.type) {
        case TransactionType.SO:
          if (_salesType == null) {
            setState(() {
              // _salesType = SalesType.Credit;
            });
          }
          break;
      }
    });
  }

  getDialogContent() {
    return Expanded(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: getTypeButtons(),
      ),
    );
  }

  getTypeButtons() {
    int buttonCount;
    List<Widget> buttonList = [];

    /// Add new case for adding new Type

    switch (widget.type) {
      case TransactionType.SO:
        buttonCount = _salesType.length;
        break;
    }
    for (int index = 0; index < buttonCount; index++) {
      buttonList.add(getTypeButton(index));
      if (index + 1 != buttonCount) {
        buttonList.add(SizedBox(width: 6));
      }
    }
    return buttonList;
  }

  _getAddressInfo() {
    String companyInfo;
    if (fromAddress.fromCompanyName != null)
      companyInfo = fromAddress.fromCompanyName;
    if (fromAddress.fromDivisionName != null)
      companyInfo = companyInfo + ", " + fromAddress.fromDivisionName;
    if (fromAddress.fromLocationName != null)
      companyInfo = companyInfo + ", " + fromAddress.fromLocationName;
    if (fromAddress.fromDepartmentName != null)
      companyInfo = companyInfo + ", " + fromAddress.fromDepartmentName;

    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: const EdgeInsets.all(8.0),
          decoration:
              Common().getBCoreSD(isMandatory: false, isValidated: false),
          child: Column(
            children: [
              Text("Address",
                  style:
                      TextStyle(fontSize: 13.0, color: BCAppTheme().grayColor),
                  textAlign: TextAlign.center),
              Text(companyInfo ?? "",
                  style: TextStyle(fontSize: 12.0), textAlign: TextAlign.left),
            ],
          ),
        ),
      ],
    );
  }

  /// returns button with title for the transaction type selection pop up
  Widget getTypeButton(int buttonIndex) {
    String buttonText;

    /// Add new case for adding new Type
    switch (widget.type) {
      case TransactionType.SO:
        buttonText = _salesType[buttonIndex].text;
        break;
    }

    return Expanded(
      flex: 3,
      child: FlatButton(
          color: BCAppTheme().headingTextColor,
          child: Text(buttonText,
              style:
                  TextStyle(color: BCAppTheme().secondaryColor, fontSize: 11)),
          onPressed: () => changeType(context, buttonIndex)),
    );
  }

  bool check() {
    return true;
  }
}
