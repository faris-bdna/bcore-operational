import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/response/accountsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/models/response/salesType_resp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSOListResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'client_event.dart';

part 'client_state.dart';

class ClientBloc extends Bloc<ClientEvent, ClientState> {
  final BCRepository repository;

  ClientBloc({@required this.repository})
      : assert(repository != null),
        super(GetSalesTypeInitial());

  @override
  Stream<ClientState> mapEventToState(ClientEvent event) async* {

    if (event is LoadSalesType) {
      yield LoadSalesTypeInProgress();

      try {
        final SalesTypeResponse salesTypeResponse =
            await repository.getSalesType();
        yield LoadSalesTypeComplete(salesTypeResponse: salesTypeResponse);
      } catch (error) {
        yield LoadSalesTypeFailure(error: error.toString());
      }
    }

    else if (event is LoadAccountType) {
      yield LoadAccountTypeInProgress();

      try {
        final GetCommonResp commonResp = await repository.getAccountType();
        yield LoadAccountTypeComplete(commonResp: commonResp);
      } catch (error) {
        yield LoadAccountTypeFailure(error: error.toString());
      }
    }

    else if (event is LoadAccounts) {
      yield LoadAccountsInProgress();

      try {
        final AccountsResp accountsResp = await repository.getAccounts(
            contractType: event.contractType, companyId: event.companyId);
        yield LoadAccountsComplete(accountsResp: accountsResp);
      } catch (error) {
        yield LoadAccountsFailure(error: error.toString());
      }
    }

    else if (event is LoadSalesMan) {
      yield LoadSalesManInProgress();

      try {
        final GetCommonResp getDivisionResponse = await repository.getSalesman(
            clientTypeID: event.clientTypeID,
            customerId: event.customerId,
            companyId: event.companyId);
        yield LoadSalesManComplete(getSalesmanResponse: getDivisionResponse);
      } catch (error) {
        yield LoadSalesManFailure(error: error.toString());
      }
    }

    else if (event is LoadLocation) {
      yield GetLocationInProgress();

      try {
        final GetCommonResp getLocationResponse =
            await repository.getLocationMaster(
                companyId: event.companyId,
                divisionId: event.divisionId,
                mode: event.mode);
        yield GetLocationComplete(getLocationResponse: getLocationResponse);
      } catch (error) {
        yield GetLocationFailure(error: error.toString());
      }
    }

    else if (event is LoadDepartment) {
      yield GetDepartmentInProgress();

      try {
        final GetCommonResp getDepartmentResponse = await repository
            .getDepartmentMaster(companyId: event.companyId, mode: event.mode);
        yield GetDepartmentComplete(
            getDepartmentResponse: getDepartmentResponse);
      } catch (error) {
        yield GetDepartmentFailure(error: error.toString());
      }
    }

    else if (event is GetPriceType) {
      yield GetPriceTypeInProgress();

      try {
        final GetCommonResp getPriceTypeResp = await repository.getPriceType();
        yield GetPriceTypeComplete(getPriceTypeResp: getPriceTypeResp);
      } catch (error) {
        yield GetPriceTypeFailure(error: error.toString());
      }
    }

    else if (event is GetReason) {
      yield GetReasonInProgress();

      try {
        final GetCommonResp getReasonResp = await repository.getReason();
        yield GetReasonComplete(getReasonResp: getReasonResp);
      } catch (error) {
        yield GetReasonFailure(error: error.toString());
      }
    }

    else if (event is GetGRNEntryNo) {
      yield GetGRNEntryNoInProgress();

      try {
        final ReferencePOGrnResp resp = await repository.getGRNEntryNo();
        yield GetGRNEntryNoComplete(referencePOGrnResp: resp);
      } catch (error) {
        yield GetGRNEntryNoFailure(error: error.toString());
      }
    }

    else if (event is GetPendingPurchaseOrder) {
      yield GetPendingPurchaseOrderInProgress();

      try {
        final ReferencePOGrnResp resp =
            await repository.getPendingPurchaseOrder(grnType: event.grnType);
        yield GetPendingPurchaseOrderComplete(
            getPendingPurchaseOrderResp: resp);
      } catch (error) {
        yield GetPendingPurchaseOrderFailure(error: error.toString());
      }
    }

    else if (event is GetSalesOrderList) {
      yield GetSalesOrderListInProgress();

      try {
        final GetSOListResp resp = await repository.getSalesOrderList();
        yield GetSalesOrderListComplete(resp: resp);
      } catch (error) {
        yield GetSalesOrderListFailure(error: error.toString());
      }
    }
  }
}
