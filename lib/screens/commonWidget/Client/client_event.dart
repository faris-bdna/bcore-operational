part of 'client_bloc.dart';

abstract class ClientEvent extends Equatable {
  const ClientEvent();
}

class LoadSalesType extends ClientEvent {

  const LoadSalesType();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoadSalesType';
}

class LoadAccountType extends ClientEvent {
  const LoadAccountType();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoadAccountType';
}

class LoadAccounts extends ClientEvent {
  final int contractType;
  final int companyId;

  const LoadAccounts({@required this.contractType,this.companyId});

  @override
  List<Object> get props => [contractType,companyId];

  @override
  String toString() => 'LoadAccounts {"ContractType" : "$contractType", "CompanyId" :"$companyId" }';
}

class LoadSalesMan extends ClientEvent {
  final int clientTypeID;
  final int customerId;
  final int companyId;

  const LoadSalesMan({@required this.clientTypeID,this.customerId,this.companyId});

  @override
  List<Object> get props => [ clientTypeID, customerId,  companyId];

  @override
  String toString() =>
      'LoadSalesMan {"clientTypeID": "$clientTypeID","customerId":"$customerId",'
          '"CompanyId" : "$companyId" }';
}

class LoadLocation extends ClientEvent {
  final String companyId;
  final String divisionId;
  final int mode;

  const LoadLocation(
      {@required this.companyId, @required this.divisionId, this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() =>
      'GetLocation {"CompanyId" : "$companyId", "DivisionId" : "$divisionId", "Mode" : "$mode" }';
}

class LoadDepartment extends ClientEvent {
  final String companyId;
  final int mode;

  const LoadDepartment({@required this.companyId, this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() =>
      'GetDepartment {"Company" : "$companyId", "Mode" : "$mode" }';
}

class GetPriceType extends ClientEvent {
  const GetPriceType();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetPriceType';
}

class GetReason extends ClientEvent {

  const GetReason();

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetReason';
}

class GetGRNEntryNo extends ClientEvent {

  const GetGRNEntryNo();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetGRNEntryNo';
}

class GetPendingPurchaseOrder extends ClientEvent {
  final String grnType;

  const GetPendingPurchaseOrder({this.grnType});

  @override
  List<Object> get props => [grnType];

  @override
  String toString() => 'GetPendingPurchaseOrder {"GRNType" : "$grnType"}';
}

class GetSalesOrderList extends ClientEvent {

  const GetSalesOrderList();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetSalesOrderList {}';
}
