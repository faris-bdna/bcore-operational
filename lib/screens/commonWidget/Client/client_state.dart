part of 'client_bloc.dart';

abstract class ClientState extends Equatable {
  const ClientState();

  @override
  List<Object> get props => [];
}


/// Get Sales Type
class GetSalesTypeInitial extends ClientState{}

class LoadSalesTypeInProgress extends ClientState{}

class LoadSalesTypeComplete extends ClientState{
  final SalesTypeResponse salesTypeResponse;

  LoadSalesTypeComplete({@required this.salesTypeResponse}) : assert(salesTypeResponse != null);

  @override
  List<Object> get props => [salesTypeResponse];
}

class LoadSalesTypeFailure extends ClientState {
  final String error;

  const LoadSalesTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesTypeFailure { error : $error }';
}


/// Get Account type
class LoadAccountTypeInProgress extends ClientState{}

class LoadAccountTypeComplete extends ClientState{
  final GetCommonResp commonResp;

  LoadAccountTypeComplete({@required this.commonResp}) : assert(commonResp != null);

  @override
  List<Object> get props => [commonResp];
}

class LoadAccountTypeFailure extends ClientState {
  final String error;

  const LoadAccountTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetAccTypeFailure { error : $error }';
}


/// Get Accounts with selected account type

class LoadAccountsInProgress extends ClientState{}

class LoadAccountsComplete extends ClientState{
  final AccountsResp accountsResp;

  LoadAccountsComplete({@required this.accountsResp}) : assert(accountsResp != null);

  @override
  List<Object> get props => [accountsResp];
}

class LoadAccountsFailure extends ClientState {
  final String error;

  const LoadAccountsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetAccountsFailure { error : $error }';
}


/// Get Salesman

class LoadSalesManInProgress extends ClientState{}

class LoadSalesManComplete extends ClientState{
  final GetCommonResp getSalesmanResponse;

  LoadSalesManComplete({@required this.getSalesmanResponse}) : assert(getSalesmanResponse != null);

  @override
  List<Object> get props => [getSalesmanResponse];
}

class LoadSalesManFailure extends ClientState {
  final String error;

  const LoadSalesManFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesmanFailure { error : $error }';
}


/// Get Location

class GetLocationInitial extends ClientState{}

class GetLocationInProgress extends ClientState{}

class GetLocationComplete extends ClientState{
  final GetCommonResp getLocationResponse;

  GetLocationComplete({@required this.getLocationResponse}) : assert(getLocationResponse != null);

  @override
  List<Object> get props => [getLocationResponse];
}

class GetLocationFailure extends ClientState {
  final String error;

  const GetLocationFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetLocationFailure { error : $error }';
}


/// Get Department

class GetDepartmentInitial extends ClientState{}

class GetDepartmentInProgress extends ClientState{}

class GetDepartmentComplete extends ClientState{
  final GetCommonResp getDepartmentResponse;

  GetDepartmentComplete({@required this.getDepartmentResponse}) : assert(getDepartmentResponse != null);

  @override
  List<Object> get props => [getDepartmentResponse];
}

class GetDepartmentFailure extends ClientState {
  final String error;

  const GetDepartmentFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDepartmentFailure { error : $error }';
}



class GetPriceTypeInitial extends ClientState {}

class GetPriceTypeInProgress extends ClientState {}

class GetPriceTypeComplete extends ClientState {
  final GetCommonResp getPriceTypeResp;

  GetPriceTypeComplete({@required this.getPriceTypeResp}) : assert(getPriceTypeResp != null);

  @override
  List<Object> get props => [getPriceTypeResp];
}

class GetPriceTypeFailure extends ClientState {
  final String error;

  const GetPriceTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPriceTypeFailure { error : $error }';
}



class GetReasonInitial extends ClientState {}

class GetReasonInProgress extends ClientState {}

class GetReasonComplete extends ClientState {
  final GetCommonResp getReasonResp;

  GetReasonComplete({@required this.getReasonResp}) : assert(getReasonResp != null);

  @override
  List<Object> get props => [getReasonResp];
}

class GetReasonFailure extends ClientState {
  final String error;

  const GetReasonFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetReasonFailure { error : $error }';
}


/// GetGRNEntryNo States

class GetGRNEntryNoInitial extends ClientState{}

class GetGRNEntryNoInProgress extends ClientState{}

class GetGRNEntryNoComplete extends ClientState{
  final ReferencePOGrnResp referencePOGrnResp;

  GetGRNEntryNoComplete({@required this.referencePOGrnResp}) : assert(referencePOGrnResp != null);

  @override
  List<Object> get props => [referencePOGrnResp];
}

class GetGRNEntryNoFailure extends ClientState {
  final String error;

  const GetGRNEntryNoFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNEntryNoFailure { error : $error }';
}


/// GetPendingPurchaseOrder States

class GetPendingPurchaseOrderInitial extends ClientState{}

class GetPendingPurchaseOrderInProgress extends ClientState{}

class GetPendingPurchaseOrderComplete extends ClientState{
  final ReferencePOGrnResp getPendingPurchaseOrderResp;

  GetPendingPurchaseOrderComplete({@required this.getPendingPurchaseOrderResp}) : assert(getPendingPurchaseOrderResp != null);

  @override
  List<Object> get props => [getPendingPurchaseOrderResp];
}

class GetPendingPurchaseOrderFailure extends ClientState {
  final String error;

  const GetPendingPurchaseOrderFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPendingPurchaseOrderFailure { error : $error }';
}


/// GetSalesOrderList States

class GetSalesOrderListInitial extends ClientState{}

class GetSalesOrderListInProgress extends ClientState{}

class GetSalesOrderListComplete extends ClientState{
  final GetSOListResp resp;

  GetSalesOrderListComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetSalesOrderListFailure extends ClientState {
  final String error;

  const GetSalesOrderListFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesOrderListFailure { error : $error }';
}