import 'package:bcore_operational/data/database.dart';
import 'package:bcore_operational/models/response/SalesOrder/saveSalesOrderResp.dart';
import 'package:bcore_operational/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'draftdetails_event.dart';

part 'draftdetails_state.dart';

class DraftDetailsBloc extends Bloc<DraftDetailsEvent, DraftDetailsState> {
  final BCRepository repository;
  final Database database;

  DraftDetailsBloc({@required this.repository, @required this.database})
      : assert(repository != null),
        super(DraftDetailInitial());

  @override
  Stream<DraftDetailsState> mapEventToState(DraftDetailsEvent event) async* {
    SaveSalesResp transactionResp;

    if (event is GetDraftTransaction) {
      yield DraftDetailInProgress();

      try {
        transactionResp = await database.draftTransactionDao
            .deleteDraftTransaction(event.draftTransactionData);
        yield DraftDetailComplete(draftDetailResp: transactionResp);
      } catch (error) {
        yield DraftDetailFailure(error: error.toString());
      }
    }
  }
}
