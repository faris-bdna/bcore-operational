part of 'draftdetails_bloc.dart';

abstract class DraftDetailsEvent extends Equatable {
  const DraftDetailsEvent();
}

class GetDraftTransaction extends DraftDetailsEvent {
  final DraftTransactionData draftTransactionData;

  const GetDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() =>
      'GetDraftTransaction {"draftTransactionData" : "$draftTransactionData"}';
}
