import 'package:bcore_operational/app_config/bcAppTheme.dart';
import 'package:bcore_operational/common/common.dart';
import 'package:bcore_operational/data/database.dart';
import 'package:bcore_operational/loclization/AppLanguage.dart';
import 'package:bcore_operational/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_operational/repository/bCore_repository.dart';
import 'package:bcore_operational/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_operational/screens/commonWidget/DraftDetails/draftdetails_bloc.dart';
import 'package:bcore_operational/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DraftDetailsForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;
  final Database database;

  const DraftDetailsForm(
      {Key key, this.repository, this.appLanguage, this.type, this.database})
      : super(key: key);

  @override
  _DraftDetailsFormState createState() =>
      _DraftDetailsFormState(repository, appLanguage, database);
}

class _DraftDetailsFormState extends State<DraftDetailsForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  TransBloc _transBloc;
  DraftTransactionData draftTransactionData;
  DraftDetailsBloc _draftDetailsBloc;

  _DraftDetailsFormState(this.repository, this.appLanguage, this.database);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DraftDetailsBloc>(
        create: (context) => DraftDetailsBloc(repository: repository),
        child: BlocListener<DraftDetailsBloc, DraftDetailsState>(
            listener: (context, state) async {
              if (state is SaveDraftTransactionInProgress){
                Common().showLoader(context);
              }
              else if(state is SaveDraftTransactionComplete) {
                _draftDetailsBloc.add(GetDraftTransaction(draftTransactionData: draftTransactionData));
              }
              else if(state is SaveDraftTransactionFailure){
                Navigator.pop(context, true);
              }
            },
            child: BlocBuilder<DraftDetailsBloc, DraftDetailsState>(
                builder: (context, state) {
              _draftDetailsBloc = BlocProvider.of<DraftDetailsBloc>(context);
              return Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => ClientForm(
                          //             repository: repository,
                          //             appLanguage: appLanguage,
                          //             type: TransactionType.SO)));
                        },
                        child: ElevatedButton(
                            child: Text(
                          'Add New',
                          style:
                              TextStyle(color: BCAppTheme().headingTextColor),
                        ),onPressed: (){
                          Common().indexDraft = 1;
                        },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 20,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text("${index + 1}",
                                        textAlign: TextAlign.justify),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text('Account Name'),
                                  ],
                                ),
                            );
                          }),
                    ),
                  ],
                ),
              );
            })));
  }
}
