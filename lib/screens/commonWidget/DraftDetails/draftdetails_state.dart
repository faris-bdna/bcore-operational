part of 'draftdetails_bloc.dart';


abstract class DraftDetailsState extends Equatable {
  const DraftDetailsState();

  @override
  List<Object> get props => [];
}

class DraftDetailInitial extends DraftDetailsState {}

class  DraftDetailInProgress extends DraftDetailsState {}

class  DraftDetailComplete extends DraftDetailsState {
  final SaveSalesResp draftDetailResp;

  DraftDetailComplete({@required this.draftDetailResp})
      : assert(draftDetailResp != null);

  @override
  List<Object> get props => [draftDetailResp];
}

class  DraftDetailFailure extends DraftDetailsState {
  final String error;

  const  DraftDetailFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DraftDetailFailure { error : $error }';
}
