import 'dart:io';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart';
import 'package:bcore_inventory_management/models/response/companyResp.dart';
import 'package:bcore_inventory_management/models/response/pendingSIResp.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/address_bloc.dart';
import 'package:bcore_inventory_management/screens/transactions/MR/mr_page.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart'
    as srn;
import 'basicForm_bloc.dart';

class BasicForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const BasicForm({Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _BasicFormState createState() => _BasicFormState(repository, appLanguage);
}

class _BasicFormState extends State<BasicForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;

  _BasicFormState(this.repository, this.appLanguage);

  int selectedPriorityValue;
  List<srn.MasterDataList> stockReferenceNoList = [];

  String companyDateFormat;

  EditMode editMode;
  bool hasReferenceMatchFound = false;

  bool containReference = true;
  bool containRemarks = false;
  bool containNotes = false;
  bool containOtherInfo = false;

  BasicFormBloc _basicFormBloc;

  @override
  void initState() {
    super.initState();

    _configUI();
    loadItemDetailsIfInEditMode();
  }

  setDefaultTransactionDate() {
    cmnBasicFormInfo.entryDateTemp = Utils.getDateByFormat(
        passedDateTime: DateTime.now(),
        format: fromAddress.fromCompanyDateFormat);
  }

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.Off) {
      setDefaultTransactionDate();
    }
  }

  ///configuring UI for loading dynamic components, as per the transaction based condition.
  _configUI() {
    if (cmnBasicFormInfo != null && cmnBasicFormInfo.entryDateTemp != null) {
      editMode = EditMode.On;
    } else {
      editMode = EditMode.Off;
    }

    switch (widget.type) {
      case TransactionType.ST:
        containReference = true;
        containRemarks = true;
        containNotes = false;
        containOtherInfo = true;
        break;

      case TransactionType.OB:
        containReference = false;
        containRemarks = false;
        containNotes = true;
        containOtherInfo = false;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BasicFormBloc>(
        create: (context) => BasicFormBloc(bcRepository: repository),
        child: BlocListener<BasicFormBloc, BasicFormState>(
            listener: (context, state) async {
          if (state is GetStockReferenceNoInProgress) {
            Common().showLoader(context);
          } else if (state is GetStockReferenceNoComplete) {
            Navigator.pop(context);

            if (state.getStockReferenceNoResp.masterDataList.isNotEmpty) {
              stockReferenceNoList = [];

              setState(() {
                stockReferenceNoList =
                    state.getStockReferenceNoResp.masterDataList;

                if (editMode == EditMode.On) {}

                // for (int i = 0; i < getStockReferenceNoResp.masterDataList.length; i++) {
                //   stockReferenceNoList.add(DropdownMenuItem(
                //     child: Text(getStockReferenceNoResp.masterDataList[i].text),
                //     value: getStockReferenceNoResp.masterDataList[i].text,
                //   ));
                // }

                // if (editMode == EditMode.On) {
                // selectedPriceTypeValue = cmnSupplierInfo.priceTypeId.toString();
                // }
              });
            }
          } else if (state is GetStockReferenceNoFailure) {
            Navigator.pop(context);
          }
        }, child: BlocBuilder<BasicFormBloc, BasicFormState>(
                builder: (context, state) {
          _basicFormBloc = BlocProvider.of<BasicFormBloc>(context);
          if (widget.type == TransactionType.ST) {
            if (state is GetStockReferenceNoInitial) {
              _basicFormBloc.add(GetStockReferenceNo());
            }
          }
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _getDates(),
                if (containReference) _getReferences(),
                if (containRemarks) _getRemarks(),
                if (containOtherInfo) _getOtherInfoFields(),
                if (containNotes) _getNotes(),
              ],
            ),
          );
        })));
  }

  _getDates() {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: DateFormField(
              initialValue: cmnBasicFormInfo.entryDateTemp,
              format: fromAddress.fromCompanyDateFormat,
              showPicker: showEntryDatePicker,
              onDateChanged: (DateTime date) {
                setState(() {
                  print("Date : $date");
                  cmnBasicFormInfo.entryDateTemp = Utils.getDateByFormat(
                      passedDateTime: date,
                      format: fromAddress.fromCompanyDateFormat);
                });
              },
              decoration: Common().getBCoreMandatoryID(
                  isMandatory: true,
                  isValidated: cmnBasicFormInfo.entryDateTemp != null,
                  hintText: 'Transaction date'),),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getReferences() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: cmnBasicFormInfo.stockReferenceNo != null),
          child: SearchableDropdown.single(
            items: stockReferenceNoList != null
                ? stockReferenceNoList.map((srn.MasterDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnBasicFormInfo.stockReferenceNo,
            hint: "Select a Reference",
            searchHint: "Select a Reference",
            onChanged: (value) {
              if (value != null) {
              setState(() {
                cmnBasicFormInfo.stockReferenceNo = value;
                bindData();
              });
            }},
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  bindData() {
    var selectedRef = stockReferenceNoList
        .where((refItem) => (refItem.text == cmnBasicFormInfo.stockReferenceNo))
        .toList()[0];

    cmnBasicFormInfo.stockTypeName = selectedRef.stockType;
    cmnBasicFormInfo.stockWarehouseName = selectedRef.warehouseName;
    cmnBasicFormInfo.stockZoneName = selectedRef.zoneName;
    cmnBasicFormInfo.stockRackName = selectedRef.rackName;
    cmnBasicFormInfo.stockBinName = selectedRef.binName;
    cmnBasicFormInfo.stockProductGroupName = selectedRef.productGroupName;
    cmnBasicFormInfo.stockBrandName = selectedRef.brandName;
    cmnBasicFormInfo.stockItemName = selectedRef.itemName;
    // cmnBasicFormInfo.stockReferenceNo = selectedReferenceNo;
    cmnBasicFormInfo.refId = selectedRef.value;
    cmnBasicFormInfo.stockTypeId = selectedRef.stockTypeId;
  }

  _getRemarks() {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.done,
            autocorrect: false,
            controller: cmnBasicFormInfo.remarks == null
                ? null
                : TextEditingController(text: cmnBasicFormInfo.remarks),
            decoration:
                Common().getBCoreID(validityStatus: true, hintText: "Remarks"),
            keyboardType: TextInputType.multiline,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
            onChanged: (value) {
              cmnBasicFormInfo.remarks = value;
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getOtherInfoFields() {
    return Container(
      decoration: Common().getBCoreSD(isMandatory: false, isValidated: false),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(height: 12),
            Row(children: [
              Expanded(
                  child: Text(
                      " Stock Type : ${cmnBasicFormInfo.stockTypeName ?? ''}",
                      style: TextStyle(fontSize: 12.0),
                      textAlign: TextAlign.left)),
            ]),
            Divider(),
            Row(children: [
              Expanded(
                  child: Text(" WareHouse : ${_getWarehouseInfo() ?? ""}",
                      style: TextStyle(fontSize: 12.0),
                      textAlign: TextAlign.left)),
            ]),
            Divider(),
            Row(children: [
              Expanded(
                  child: Text(" Item Info : ${_getItemInfo() ?? ""}",
                      style: TextStyle(fontSize: 12.0),
                      textAlign: TextAlign.left)),
            ]),
//            getWarehouseWidget(),
//            getItemWidget()
          ],
        ),
      ),
    );
  }

  _getWarehouseInfo() {
    String warehouseInfo;
    if (cmnBasicFormInfo.stockWarehouseName != "" &&
        cmnBasicFormInfo.stockWarehouseName != null) {
      warehouseInfo = cmnBasicFormInfo.stockWarehouseName;
    }
    if (cmnBasicFormInfo.stockZoneName != "" &&
        cmnBasicFormInfo.stockZoneName != null) {
      warehouseInfo = warehouseInfo + ', ' + cmnBasicFormInfo.stockZoneName;
    }
    if (cmnBasicFormInfo.stockRackName != "" &&
        cmnBasicFormInfo.stockRackName != null) {
      warehouseInfo = warehouseInfo + ', ' + cmnBasicFormInfo.stockRackName;
    }
    if (cmnBasicFormInfo.stockBinName != "" &&
        cmnBasicFormInfo.stockBinName != null) {
      warehouseInfo = warehouseInfo + ', ' + cmnBasicFormInfo.stockBinName;
    }
    return warehouseInfo;
  }

//  getWarehouseWidget(){
//    String wareHouseInfo = _getWarehouseInfo();
//    if (wareHouseInfo == null){
//        return SizedBox();
//      }
//    else {
//      return Column(
//        children: [
//          Divider(),
//          Row(children: [
//            Expanded(child: Text(
//                " WareHouse : ${_getWarehouseInfo()}",
//                style: TextStyle(fontSize: 12.0), textAlign: TextAlign.left)),
//          ]),
//        ],
//      );
//    }
//  }

  _getItemInfo() {
    String itemInfo;
    if (cmnBasicFormInfo.stockItemName != "" &&
        cmnBasicFormInfo.stockItemName != null) {
      itemInfo = cmnBasicFormInfo.stockItemName;
    }
    if (cmnBasicFormInfo.stockProductGroupName != "" &&
        cmnBasicFormInfo.stockProductGroupName != null) {
      if (itemInfo == null){
        itemInfo = cmnBasicFormInfo.stockProductGroupName;
      } else {
        itemInfo = itemInfo + ', ' + cmnBasicFormInfo.stockProductGroupName;
      }
    }
    if (cmnBasicFormInfo.stockBrandName != "" &&
        cmnBasicFormInfo.stockBrandName != null) {
      if (itemInfo == null) {
        itemInfo = cmnBasicFormInfo.stockBrandName;
      } else {
        itemInfo = itemInfo + ', ' + cmnBasicFormInfo.stockBrandName;
      }
    }

    return itemInfo;
  }

//  getItemWidget(){
//    String itemInfo = _getItemInfo();
//    if (itemInfo == null)
//    {
//      return SizedBox();
//    }
//    else {
//      return Column(
//        children: [
//          Divider(),
//          Row(children: [
//            Expanded(child: Text(
//                " Item Info : ${_getItemInfo()}",
//                style: TextStyle(fontSize: 12.0), textAlign: TextAlign.left)),
//          ]),
//        ],
//      );
//    }
//  }

  _getNotes() {
    return Column(
      children: [
        SizedBox(
          height: 100,
          child: TextField(
            maxLines: 2,
            controller: cmnBasicFormInfo.remarks == null
                ? null
                : TextEditingController(text: cmnBasicFormInfo.remarks),
            textInputAction: TextInputAction.done,
            autocorrect: false,
            decoration:
                Common().getBCoreID(validityStatus: true, hintText: "Notes"),
            keyboardType: TextInputType.multiline,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
            onChanged: (value) {
              cmnBasicFormInfo.remarks = value;
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Future<DateTime> showEntryDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Entry Date",
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(Duration(days: 365)),
      lastDate: DateTime(2101),
    );
    return date;
  }
}

//enum TransferType { Direct, FromMR }

//class Item {
//  final int itemId;
//  final String itemName;
//
//  Item(this.itemId, this.itemName);
//}
