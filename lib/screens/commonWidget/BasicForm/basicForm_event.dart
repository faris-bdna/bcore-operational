part of 'basicForm_bloc.dart';


abstract class BasicFormEvent extends Equatable {
  const  BasicFormEvent();
}

class GetStockReferenceNo extends BasicFormEvent {
  const GetStockReferenceNo();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetStockReferenceNo {}';
}

// class GetStockReferenceDetails extends BasicFormEvent {
//   final String refId;
//   const GetStockReferenceDetails(this.refId);
//
//   @override
//   List<Object> get props => [refId];
//
//   @override
//   String toString() => 'GetStockReferenceDetails { RefId : $refId}';
// }