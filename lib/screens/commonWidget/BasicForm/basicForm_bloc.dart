import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'basicForm_event.dart';
part 'basicForm_state.dart';

class BasicFormBloc extends Bloc<BasicFormEvent, BasicFormState> {
  final BCRepository bcRepository;
  final Database database;
  final GrnReq grnReq;

  BasicFormBloc(
      {@required this.bcRepository,
        @required this.database,
        @required this.grnReq})
      : assert(bcRepository != null),
        super(GetStockReferenceNoInitial());

  @override
  Stream<BasicFormState> mapEventToState(BasicFormEvent event) async* {

    if (event is GetStockReferenceNo) {
      yield GetStockReferenceNoInProgress();

      try {
        final GetStockReferenceNoResp getStockReferenceNoResp = await bcRepository.getStockReferenceNo();
        yield GetStockReferenceNoComplete(getStockReferenceNoResp: getStockReferenceNoResp);
      } catch (error) {
        yield GetStockReferenceNoFailure(error: error.toString());
      }
    }
  }
}
