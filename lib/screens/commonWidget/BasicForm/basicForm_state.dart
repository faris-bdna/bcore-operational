part of 'basicForm_bloc.dart';

abstract class BasicFormState extends Equatable {
  const BasicFormState();

  @override
  List<Object> get props => [];
}

/// Get Stock Reference

class GetStockReferenceNoInitial extends BasicFormState{}

class GetStockReferenceNoInProgress extends BasicFormState{}

class GetStockReferenceNoComplete extends BasicFormState{
  final GetStockReferenceNoResp getStockReferenceNoResp;

  GetStockReferenceNoComplete({@required this.getStockReferenceNoResp}) : assert(getStockReferenceNoResp != null);

  @override
  List<Object> get props => [getStockReferenceNoResp];
}

class GetStockReferenceNoFailure extends BasicFormState {
  final String error;

  const GetStockReferenceNoFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetStockReferenceNoFailure { error : $error }';
}


/// Get Stock Reference Details

class GetStockReferenceDetailsInitial extends BasicFormState{}

class GetStockReferenceDetailsInProgress extends BasicFormState{}

class GetStockReferenceDetailsComplete extends BasicFormState{
  final GetStockReferenceDetailsResp getStockReferenceDetailsResp;

  GetStockReferenceDetailsComplete({@required this.getStockReferenceDetailsResp}) : assert(getStockReferenceDetailsResp != null);

  @override
  List<Object> get props => [getStockReferenceDetailsResp];
}

class GetStockReferenceDetailsFailure extends BasicFormState {
  final String error;

  const GetStockReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetStockReferenceDetailsFailure { error : $error }';
}