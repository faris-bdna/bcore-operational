import 'dart:async';

import 'package:bcore_inventory_management/models/response/utilities/getAdditionalServicesResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'addCharges_event.dart';
part 'addCharges_state.dart';

class AddChargesBloc extends Bloc<AddChargesEvent, AddChargesState> {
  final BCRepository bcRepository;

  AddChargesBloc(
      {@required this.bcRepository})
      : assert(bcRepository != null),
        super(GetAdditionalServicesInitial());

  @override
  Stream<AddChargesState> mapEventToState(AddChargesEvent event) async* {
    if (event is GetAdditionalServices) {
      yield GetAdditionalServicesInProgress();
      try {
        final GetAdditionalServicesResp resp = await bcRepository
            .getAdditionalServices();
        yield GetAdditionalServicesComplete(resp: resp);
      } catch (error) {
        yield GetAdditionalServicesFailure(error: error.toString());
      }
    }
  }
}
