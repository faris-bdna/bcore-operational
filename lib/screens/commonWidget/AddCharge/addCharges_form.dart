import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/response/utilities/getAdditionalServicesResp.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:flutter/material.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'addCharges_bloc.dart';


class AddChargesForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const AddChargesForm(
      {Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _AddChargesFormState createState() => _AddChargesFormState(repository);
}

class _AddChargesFormState extends State<AddChargesForm> {

  final BCRepository repository;

  _AddChargesFormState(this.repository);

  AddChargesBloc _addChargesBloc;
  CommonAddCharges chargeDetail;
  List<AdditionalServiceMasterDataList> additionalServiceList = [];

  EditMode _editMode;

  var _rateController = TextEditingController();
  var _quantityController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _editMode = (chargeDetail == null) ? EditMode.Off : EditMode.On;

    if (chargeDetail == null) {
      chargeDetail = CommonAddCharges();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddChargesBloc>(
        create: (context) => AddChargesBloc(bcRepository: repository),
        child: BlocListener<AddChargesBloc,AddChargesState>(
          listener: (context,state){
            if (state is GetAdditionalServicesInProgress){
              Common().showLoader(context);
            }
            else if (state is GetAdditionalServicesFailure){
              Navigator.pop(context);
            }
            else if (state is GetAdditionalServicesComplete){
              Navigator.pop(context);

              if (state.resp.masterDataList.isNotEmpty){
                setState(() {
                  additionalServiceList = state.resp.masterDataList;
                });
              }
            }
          },
          child: BlocBuilder<AddChargesBloc,AddChargesState>(builder: (context,state){
            _addChargesBloc = BlocProvider.of<AddChargesBloc>(context);

            if (state is GetAdditionalServicesInitial) {
              _addChargesBloc.add(GetAdditionalServices());
            }
            return Column(
              children: [
                _getItem(),
                SizedBox(height: 16),
                Row(children: [Padding(padding: EdgeInsets.only(left: 8.0,right: 8.0), child: Text("UOM : ${chargeDetail?.productUnit ?? ""}"))]),
                _getRateWidget(),
                _getQuantity(),
                Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 16),
                            Row(children: [Padding(padding: EdgeInsets.only(left: 8.0,right: 8.0), child: Text("Tax Amount : ${_getTaxAmount()}"))]),
                            SizedBox(height: 16),
                            Row(children: [Padding(padding: EdgeInsets.only(left: 8.0,right: 8.0), child: Text("Total Amount : ${_getTotalAmount()}"))]),
                          ],
                        ),
                      ),
                      Container(
                        width: 40.0,
                        height: 50.0,
                        child: FlatButton(
                            padding: EdgeInsets.zero,
                            color: BCAppTheme().headingTextColor,
                            onPressed: () {
                              if (_validateItemDetail()){
                                setReqDetailJson();
                              }
                            },
                            child: Icon(
                              Icons.save,
                              size: 24,
                              color: Colors.white,
                            )),
                      )
                    ]
                ),
                SizedBox(height: 8),
                Divider(),
                SizedBox(height: 8),
                _getAdditionalChargeList()
              ],
            );
          }),
        )
    );
  }

  _getItem() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(isMandatory: true, isValidated: chargeDetail.productId != null),
          child: SearchableDropdown.single(
            items: (additionalServiceList != null && additionalServiceList.isNotEmpty)
                ? additionalServiceList.map((AdditionalServiceMasterDataList item) {
              return DropdownMenuItem(
                  child: Text(item.productName,
                      style: TextStyle(
                        fontSize: 14.0,
                        color: BCAppTheme().subTextColor,
                      )),
                  value: item.productName);
            }).toList()
                : [],
            value: chargeDetail.productName,
            hint: "Select Charge",
            searchHint: "Select Charge",
            onChanged: (value) {
              if (value != null){
                setState(() {
                AdditionalServiceMasterDataList selectedItem = additionalServiceList.where((item) => item.productName == value).toList()[0];
                chargeDetail.fromJson(selectedItem.toJson());
                _rateController.text = chargeDetail.unitPrice.toString();
              });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getRateWidget() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            controller: _rateController,
            autocorrect: false,
            decoration: Common().getBCoreID(
                validityStatus: (_rateController.text != null && _rateController.text != ''),
                hintText: 'Rate'),
            keyboardType: TextInputType.number,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
            onChanged: (value){
              setState(() {
                if (value != ""){
                  chargeDetail.unitPrice = value?.toDouble(pos: getDeci());
                }else{
                  chargeDetail.unitPrice = 0.0;
                }
              });
            },
          ),
        ),
      ],
    );
  }

  _getQuantity(){
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 50,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _quantityController,
            autocorrect: false,
            decoration: Common().getBCoreID(
              validityStatus:_quantityController.text.trim().isNotEmpty,
              hintText: 'Quantity',
            ),
            onChanged: (value) {
              setState(() {
                if (value != ""){
                  chargeDetail?.quantity = value.toDouble(pos: getDeci());
                }else{
                  chargeDetail?.quantity = 0.0;
                }
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getAdditionalChargeList(){
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: CommonAddChargesInfo.shared.cmnAddChargesList.length,
      itemBuilder: (context, index) {
        return AdditionalChargeRow(
          index: index,
          didTapEdit: () {
            setState(() {
              chargeDetail = CommonAddCharges.fromJson(CommonAddChargesInfo.shared.cmnAddChargesList[index].toJson());
              _loadData();
            });
          },
          didTapDelete: () {
            setState(() {
              CommonAddChargesInfo.shared.cmnAddChargesList.removeAt(index);
            });
          },
        );
      },
    );
  }

  _getTaxAmount(){
    var unitPrice= chargeDetail.unitPrice ?? 0.0;
    var quantity = chargeDetail?.quantity ?? 1.0;
    chargeDetail.taxAmount = ((unitPrice * quantity) * ((chargeDetail.taxPercent ?? 0.0) / 100.0)).trim();
    return chargeDetail.taxAmount;
  }

  _getTotalAmount(){
    var unitPrice = chargeDetail.unitPrice ?? 0.0;
    var quantity = chargeDetail?.quantity ?? 1.0;
    var taxAmount = ((unitPrice * quantity) * ((chargeDetail.taxPercent ?? 0.0) / 100.0)).trim();
    chargeDetail.total = (unitPrice * quantity).trim();
    chargeDetail.netAmount = (unitPrice * quantity) + taxAmount;
    return chargeDetail.total;
  }

  bool _validateItemDetail() {

    if (chargeDetail.productId == null) {
      showMessage(
          context: context, message: "Please select any charge");
      return false;
    }
    if (chargeDetail.unitPrice == null || chargeDetail.unitPrice == 0) {
      showMessage(context: context, message: "Please enter Rate");
      return false;
    }
    if (chargeDetail.quantity == null || chargeDetail.quantity == 0) {
      showMessage(context: context, message: "Please enter quantity");
      return false;
    }
    return true;
  }

  void  setReqDetailJson() {
    if (_editMode == EditMode.Off) {
      CommonAddCharges newCharge = CommonAddCharges.fromJson(chargeDetail.toJson());
      newCharge.editable = true;
      CommonAddChargesInfo.shared.cmnAddChargesList.add(newCharge);
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Service Details Added!'), duration: Duration(milliseconds: 500)));
    }else{
      /// Check whether already item is there
      var indexOfItemInExistingList = CommonAddChargesInfo.shared.cmnAddChargesList
          .indexWhere((item) => item.productId == chargeDetail.productId);

      if (indexOfItemInExistingList != -1) {
        CommonAddChargesInfo.shared.cmnAddChargesList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CommonAddCharges.fromJson(chargeDetail.toJson())]);
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Service Details Updated!'), duration: Duration(milliseconds: 500)));
      }
    }
    _clearFields();
  }

  _clearFields(){
    setState(() {
      chargeDetail = CommonAddCharges();
      _quantityController.text = "";
      _rateController.text = "";
    });
  }

  _loadData(){
    _rateController.text = chargeDetail.unitPrice.toString();
    _quantityController.text = chargeDetail.quantity.toString();
    _editMode = EditMode.On;
  }
}

class AdditionalChargeRow extends StatelessWidget {
  final int index;

  final VoidCallback didTapEdit;
  final VoidCallback didTapDelete;

  const AdditionalChargeRow(
      {Key key, this.index, this.didTapEdit, this.didTapDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Card(
        child:
        ListTile(
          tileColor: Colors.white,
          contentPadding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
          onTap: () {},
          title: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Item",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 12.0,
                            color: BCAppTheme().textColor,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                    Text(CommonAddChargesInfo.shared.cmnAddChargesList[index].productName ?? "",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                  ],
                ),
              ),
              // SizedBox(width: 8.0),
              // Expanded(
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.end,
              //     children: [
              //       Text("Sub Type",
              //           maxLines: 1,
              //           style: TextStyle(
              //               fontSize: 12.0,
              //               color: BCAppTheme().textColor,
              //               fontWeight: FontWeight.bold),
              //           overflow: TextOverflow.ellipsis,
              //           softWrap: true),
              //       Text(getItemName(),
              //           maxLines: 1,
              //           style: TextStyle(
              //               fontSize: 11.0, color: BCAppTheme().textColor),
              //           overflow: TextOverflow.ellipsis,
              //           softWrap: true),
              //     ],
              //   ),
              // )
            ],
          ),
          subtitle: Column(
            children: [
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                      child:
                      Text("Total Amount: ${CommonAddChargesInfo.shared.cmnAddChargesList[index].total ?? ""}",
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true)
                  ),
                  Expanded(
                    child: Text("Quantity: ${CommonAddChargesInfo.shared.cmnAddChargesList[index].quantity ?? ""}",
                        textAlign: TextAlign.right,
                        maxLines: 1,
                        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                  )
                ],
              ),
              SizedBox(height: 8),
              if (CommonAddChargesInfo.shared.cmnAddChargesList[index].editable)
                Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(flex: 4, child: Container()),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 25,
                      child: FlatButton(
                          child: Text("DELETE",
                              style: TextStyle(
                                  color: BCAppTheme().redColor, fontSize: 11)),
                          onPressed: () {
//                          Navigator.pop(context);
                            didTapDelete();
                          }),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 25,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color(Utils.hexToColor("#A88D2A"))
                                .withOpacity(0.1),
                            spreadRadius: 0,
                            blurRadius: 10,
                            offset: Offset(-1, 15), // changes position of shadow
                          ),
                        ],
                      ),
                      child: RaisedButton(
                          padding: EdgeInsets.only(
                              right: 2, left: 2, top: 2, bottom: 2),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(50.0))),
                          onPressed: () {
                            didTapEdit();
                          },
                          textColor: Colors.white,
                          color: BCAppTheme().headingTextColor,
                          child: Container(
                            width: double.maxFinite,
                            child: Stack(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    child: AutoSizeText(
                                      'EDIT',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 11),
                                      maxLines: 1,
                                      minFontSize: 8.0,
                                      stepGranularity: 1.0,
                                    ),
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                        "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                              ],
                            ),
                          )),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      );
  }
}


