part of 'addCharges_bloc.dart';

abstract class AddChargesEvent extends Equatable {
  const AddChargesEvent();
}

class GetAdditionalServices extends AddChargesEvent {
  const GetAdditionalServices();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetAdditionalServices';
}