part of 'addCharges_bloc.dart';

abstract class AddChargesState extends Equatable {
  const AddChargesState();

  @override
  List<Object> get props => [];
}

class GetAdditionalServicesInitial extends AddChargesState {}

class GetAdditionalServicesInProgress extends AddChargesState {}

class GetAdditionalServicesComplete extends AddChargesState {
  final GetAdditionalServicesResp resp;

  const GetAdditionalServicesComplete({@required this.resp})
      : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetAdditionalServicesFailure extends AddChargesState {
  final String error;

  const GetAdditionalServicesFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetAdditionalServicesFailure { error: $error }';
}