import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/sai/saveSAIReq.dart';
import 'package:bcore_inventory_management/models/response/utilities/termsAndConditionsResp.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnTandCInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Utilities/utilities_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TermsAndConditions extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const TermsAndConditions({Key key, this.repository, this.appLanguage, this.type}) : super(key: key);

  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState(repository);
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  final BCRepository repository;

  _TermsAndConditionsState(this.repository);

  UtilitiesBloc _utilitiesBloc;
  List<TandAMasterDataList> termsAndConditionsList = [];

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero,()
    {
      _utilitiesBloc.add(GetTermsAndCondition());
    });
  }

  getTandCStatus(int index) {
    if (CommonTandCInfo.shared.termsAndConditionList != null) {
      if (CommonTandCInfo.shared.termsAndConditionList.isNotEmpty) {
        if (CommonTandCInfo.shared.termsAndConditionList
            .where(
                (term) => termsAndConditionsList[index].termsId == term.termsId)
            .toList()
            .isNotEmpty) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  changeSelection({int index, bool hasSelected}) {

    List<TermsAndCondions> terms = [];

    /// Checks whether there is any t&c added already
    if (CommonTandCInfo.shared.termsAndConditionList.isNotEmpty){

      /// search for selected tc using term id in selected list to check already exist or not
      terms = CommonTandCInfo.shared.termsAndConditionList
          .where(
              (term) => termsAndConditionsList[index].termsId == term.termsId)
          .toList() ?? [];
    }

    if (terms.isEmpty){
      /// tc is not in already selected list and need to add as new one.
      if (hasSelected){
        TermsAndCondions term = TermsAndCondions.fromJson(termsAndConditionsList[index].toJson());
        term.editable = true;
        CommonTandCInfo.shared.termsAndConditionList.add(term);
      }
    }
    else {
      /// tc already exist in the selected list.
      /// checks whether the tc is editable ( is not from sales order )
      if(terms[0].editable == null || terms[0].editable == true){
        CommonTandCInfo.shared.termsAndConditionList.removeWhere((condition) => condition.termsId == terms[0].termsId);
      }else{
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UtilitiesBloc>(
        create: (context) => UtilitiesBloc(bcRepository: repository),
        child: BlocListener<UtilitiesBloc, UtilitiesState>(
          listener: (context, state) {
            if (state is GetTermsAndConditionInProgress) {
              Common().showLoader(context);
            }
            else if (state is GetTermsAndConditionFailure) {
              Navigator.pop(context);
            }
            else if (state is GetTermsAndConditionComplete) {
              Navigator.pop(context);

              if (state.resp.masterDataList.isNotEmpty) {
                setState(() {
                  termsAndConditionsList = state.resp.masterDataList;

                });
              }
            }
          },
          child: BlocBuilder<UtilitiesBloc, UtilitiesState>(
              builder: (context, state) {
                _utilitiesBloc = BlocProvider.of<UtilitiesBloc>(context);

                return Container(
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: termsAndConditionsList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: new Container(
                          padding: new EdgeInsets.only(right: 8.0),
                          child: new Column(
                            children: <Widget>[
                              new CheckboxListTile(
                                  contentPadding: EdgeInsets.zero,
                                  value: getTandCStatus(index),
                                  title: Text(termsAndConditionsList[index]?.categoryName ?? "",
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w600,
                                          color: BCAppTheme().textColor),
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: true),
                                  subtitle: Text(
                                      termsAndConditionsList[index]?.termsAndConditionsName ?? "",
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 10.0,
                                          color: BCAppTheme().textColor),
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: true),
                                  /// Commented as there will be only one type of t & c will be there. If there comes multiple types can use it
                                  // Row(children: [
                                  //   Expanded(child: Text(
                                  //       termsAndConditionsList[index]?.termsAndConditionsName ?? "",
                                  //       textAlign: TextAlign.left,
                                  //       maxLines: 1,
                                  //       style: TextStyle(
                                  //           fontSize: 10.0,
                                  //           color: BCAppTheme().textColor),
                                  //       overflow: TextOverflow.ellipsis,
                                  //       softWrap: true)),
                                  //   SizedBox(width: 8.0),
                                  //   Expanded(child: Text( "Payment",
                                  //       // termsAndConditionsList[index]?. ?? "",
                                  //       textAlign: TextAlign.right,
                                  //       maxLines: 1,
                                  //       style: TextStyle(
                                  //           fontSize: 10.0,
                                  //           color: BCAppTheme().textColor),
                                  //       overflow: TextOverflow.ellipsis,
                                  //       softWrap: true)),
                                  // ],
                                  // ),
                                  controlAffinity: ListTileControlAffinity
                                      .leading,
                                  onChanged: (bool val) {
                                    setState(() {
                                      changeSelection(index: index, hasSelected: val);
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
              }),
        )
    );
  }
}
