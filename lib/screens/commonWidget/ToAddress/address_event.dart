part of 'address_bloc.dart';

abstract class AddressEvent extends Equatable {
  const  AddressEvent();
}

class GetCompany extends AddressEvent {
  final int mode;
  const GetCompany({@required this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'LoadCompany {"Mode" : "$mode" }';
}

class LoadDivision extends AddressEvent {
  final String companyId;
  final int mode;

  const LoadDivision({@required this.companyId ,this.mode});

  @override
  List<Object> get props => [companyId,mode];

  @override
  String toString() => 'GetDivision {"CompanyId" : "$companyId", "Mode" : "$mode" }';
}

class LoadLocation extends AddressEvent {
  final String companyId;
  final String divisionId;
  final int mode;

  const LoadLocation({@required this.companyId , @required this.divisionId, this.mode});

  @override
  List<Object> get props => [companyId,divisionId,mode];

  @override
  String toString() => 'GetLocation {"CompanyId" : "$companyId", "DivisionId" : "$divisionId", "Mode" : "$mode" }';
}

class LoadDepartment extends AddressEvent {
  final String companyId;
  final int mode;

  const LoadDepartment({@required this.companyId ,this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'GetDepartment {"Company" : "$companyId", "Mode" : "$mode" }';
}

class GetProject extends AddressEvent {
  final String locationId;
//  final int mode;

//  const GetProject({@required this.locationId ,this.mode});
  const GetProject({@required this.locationId});

  @override
  List<Object> get props => [locationId];

  @override
//  String toString() => 'GetProject {"LocationId" : "$locationId", "Mode" : "$mode" }';
  String toString() => 'GetProject {"LocationId" : "$locationId"}';
}

class LoadDeliveryLocation extends AddressEvent {
  final String companyId;
  final String divisionId;
  final int mode;

  const LoadDeliveryLocation({@required this.companyId , @required this.divisionId, this.mode});

  @override
  List<Object> get props => [companyId,divisionId,mode];

  @override
  String toString() => 'GetLocation {"CompanyId" : "$companyId", "DivisionId" : "$divisionId", "Mode" : "$mode" }';
}

class GetPendingSI extends AddressEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  const GetPendingSI({@required this.companyId,@required this.divisionId,@required this.locationId});

  @override
  List<Object> get props => [companyId,divisionId,locationId];

  @override
  String toString() => 'GetPendingSI {"CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"}';
}

class SaveDraftTransaction extends AddressEvent {
  final DraftTransactionData draftTransactionData;
  const SaveDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'SaveDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class UpdateDraftTransaction extends AddressEvent {
  final int id;
  final DraftTransactionData draftTransactionData;
  const UpdateDraftTransaction({@required this.id, this.draftTransactionData});

  @override
  List<Object> get props => [id, draftTransactionData];

  @override
  String toString() => 'SaveDraftTransaction {"Id":"$id" ,DraftTransaction" : "$draftTransactionData"}';
}

class GetDraftTransaction extends AddressEvent {
  final String draftTransId;
  final String draftTransType;
  const GetDraftTransaction({@required this.draftTransId, this.draftTransType});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetDraftTransaction {"DraftID" : "$draftTransId","DraftType" : "$draftTransType"}';
}

class GetPendingTransferOut extends AddressEvent {
  final String req;
  const GetPendingTransferOut({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'GetPendingTransferOut {"Req" : "$req"}';
}

class GetPendingMR extends AddressEvent {
  final TransactionType type;
  const GetPendingMR({@required this.type});

  @override
  List<Object> get props => [type];

  @override
  String toString() => 'GetPendingMR {"Type" : "$type"}';
}