import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/request/II/iiReq.dart';
import 'package:bcore_inventory_management/models/request/MTI/GetPendingTransferOutReq.dart';
import 'package:bcore_inventory_management/models/request/mtoReq.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/response/pendingSIResp.dart';
import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/address_bloc.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class AddressForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const AddressForm({Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _AddressFormState createState() => _AddressFormState(repository, appLanguage);
}

class _AddressFormState extends State<AddressForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;

  _AddressFormState(this.repository, this.appLanguage);

  List<CompanyDataList> companyList = [];
  List<CommonDataList> divisionList = [];
  List<CommonDataList> locationList = [];
  List<CommonDataList> deliveryLocationList = [];
  List<CommonDataList> departmentList = [];
  List<CommonDataList> projectsList = [];
  List<PendingRefList> pendingRefList = [];

  /// Pending Store Issue List

  EditMode editMode;

  // bool hasReferenceMatchFound = false;

  bool containTypes = false;
  bool containAddress = true;
  bool containProject = false;
  bool containRemarks = false;
  bool containReferences = false;
  bool containExpiryDate = false;
  bool containPriority = false;
  bool containTextAddress = false;

  bool validateDivision = false;
  bool validateLocation = false;
  bool validateDepartment = false;

  // var expectedReceiveDate;

  SIType _transferType;
  AddressBloc _addressBloc;

  List<Item> priorityMenuItems = [
    Item('1', "Low"),
    Item('2', "Medium"),
    Item('3', "High")
  ];

  @override
  void initState() {
    super.initState();

    if (toAddress.transactionDate != null) {
      editMode = EditMode.On;
    } else {
      editMode = EditMode.Off;
      setDefaultTransactionDate();
    }
    _configUI();
    _configValidation();

    if (widget.type == TransactionType.MTO ||
        widget.type == TransactionType.SI ||
        widget.type == TransactionType.II) {
      if (toAddress.transferTypeId == null) {
        Future.delayed(Duration.zero, () {
          showTransferTypesDialogue();
        });
      } else {
        _transferType = SIType.values[toAddress.transferTypeId];
        if (_transferType == SIType.From_MR) {
          setState(() {
            containTextAddress = true;
            containAddress = false;
            containProject = false;
          });
        }
      }
    }
    // else if (widget.type == TransactionType.II) {
    //   _transferType = SIType.Direct;
    // }
  }

  setDefaultTransactionDate() {
    toAddress.transactionDate = Utils.getDateByFormat(
        passedDateTime: DateTime.now(),
        format: fromAddress.fromCompanyDateFormat);
  }

  ///configuring UI for loading dynamic components, as per the transaction based condition.
  _configUI() {
    switch (widget.type) {
      case TransactionType.MR:
        containTypes = false;
        containReferences = false;
        containExpiryDate = true;
        containPriority = true;
        containAddress = true;
        containProject = false;
        containRemarks = false;
        containTextAddress = false;
        break;

      case TransactionType.SI:
        containTypes = true;
        containReferences = true;
        containExpiryDate = false;
        containPriority = false;
        containAddress = true;
        containProject = true;
        containRemarks = true;
        containTextAddress = false;
        break;

      case TransactionType.SR:
        containTypes = false;
        containReferences = true;
        containExpiryDate = false;
        containPriority = false;
        containAddress = false;
        containProject = false;
        containRemarks = true;
        containTextAddress = true;
        break;

      case TransactionType.II:
        containTypes = true;
        containReferences = true;
        containExpiryDate = false;
        containPriority = false;
        containAddress = true;
        containProject = true;
        containRemarks = true;
        containTextAddress = false;
        break;

      case TransactionType.MTO:
        containTypes = true;
        containReferences = true;
        containExpiryDate = false;
        containPriority = false;
        containAddress = true;
        containProject = true;
        containRemarks = true;
        containTextAddress = false;
        break;

      case TransactionType.MTI:
        containTypes = false;
        containReferences = true;
        containExpiryDate = false;
        containPriority = false;
        containAddress = false;
        containProject = false;
        containRemarks = true;
        containTextAddress = true;
        break;
    }

    if (toAddress.deliveryPriority == null) {
      toAddress.deliveryPriority = priorityMenuItems[1].itemId;
    }
  }

  ///Configuring mandatory filed as per the condition.
  _configValidation() {
    switch (widget.type) {
      case TransactionType.MR:
        validateDivision = true;
        validateLocation = true;
        validateDepartment = false;
        break;

      case TransactionType.SI:
        validateDivision = true;
        validateLocation = true;
        validateDepartment = false;
        break;

      case TransactionType.SR:
        validateDivision = false;
        validateLocation = false;
        validateDepartment = false;
        break;

      case TransactionType.II:
        validateDivision = true;
        validateLocation = true;
        validateDepartment = true;
        break;

      case TransactionType.MTO:
        validateDivision = true;
        validateLocation = true;
        validateDepartment = false;
        break;

      case TransactionType.MTI:
        validateDivision = true;
        validateLocation = true;
        validateDepartment = false;
        break;
    }
  }

  Future<DateTime> showItemDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Expected Receiving Date",
      initialDate: DateTime.now(),
      // firstDate: DateTime.now(),
      firstDate: DateTime.now().subtract(Duration(days: 356)),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );
    return date;
  }

  _getItemDate(String placeHolderText) {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: DateFormField(
            format: fromAddress.fromCompanyDateFormat,
            showPicker: showItemDatePicker,
            initialValue: (toAddress.expectedReceiveDate != null)
                ? toAddress.expectedReceiveDate
                : null,
            onDateChanged: (DateTime date) {
              setState(() {
                print("Date : $date");

                toAddress.expectedReceiveDate = Utils.getDateByFormat(
                    format: fromAddress.fromCompanyDateFormat,
                    passedDateTime: date);
              });
            },
            decoration: Common().getBCoreMandatoryID(
                isMandatory: true,
                isValidated: toAddress.expectedReceiveDate != null,
                hintText: placeHolderText,
                icon: Icons.calendar_today),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddressBloc>(
        create: (context) => AddressBloc(repository: repository),
        child: BlocListener<AddressBloc, AddressState>(listener:
            (context, state) async {
          if (state is GetCompanyInProgress ||
              state is GetDivisionInProgress ||
              state is GetLocationInProgress ||
              state is GetDepartmentInProgress ||
              state is GetProjectInProgress ||
              state is GetPendingSIInProgress ||
              state is GetPendingTransferOutInProgress ||
              state is GetPendingMRInProgress) {
            Common().showLoader(context);
          } else if (state is GetCompanyFailure ||
              state is GetDivisionFailure ||
              state is GetLocationFailure ||
              state is GetDepartmentFailure ||
              state is GetProjectFailure ||
              state is GetPendingSIFailure ||
              state is GetPendingTransferOutFailure ||
              state is GetPendingMRFailure) {
            Navigator.pop(context);
          }
          else if (state is GetCompanyComplete) {
            Navigator.pop(context);
            companyList = [];

            setState(() {
              companyList = state.getCompanyResp.masterDataList;

              if (widget.type == TransactionType.SI) {
                companyList.removeWhere((company) =>
                    company.value == FromAddress.shared.fromCompanyId);
              }
              if (widget.type == TransactionType.II ||
                  widget.type == TransactionType.MTO) {
                toAddress.companyName = fromAddress.fromCompanyName;
                toAddress.companyId = fromAddress.fromCompanyId;
                _addressBloc.add(LoadDivision(
                    companyId: fromAddress.fromCompanyId,
                    mode: AccessType.All.index));
              } else if (editMode == EditMode.On &&
                  toAddress.companyId != null) {
                _addressBloc.add(LoadDivision(
                    companyId: toAddress.companyId,
                    mode: AccessType.All.index));
              }
            });
          }
          else if (state is GetDivisionComplete) {
            Navigator.pop(context);

            if (state.getDivisionResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Division data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            } else {
              divisionList = [];
              setState(() {
                divisionList = state.getDivisionResponse.masterDataList;

                if (widget.type == TransactionType.II) {
                  toAddress.divisionName = fromAddress.fromDivisionName;
                  toAddress.divisionId = fromAddress.fromDivisionId;
                  _addressBloc.add(LoadLocation(
                      companyId: toAddress.companyId,
                      divisionId: toAddress.divisionId,
                      mode: AccessType.All.index));
                } else if (editMode == EditMode.On) {
                  if (toAddress.companyId != null &&
                      toAddress.divisionId != null) {
                    _addressBloc.add(LoadLocation(
                        companyId: toAddress.companyId,
                        divisionId: toAddress.divisionId,
                        mode: AccessType.All.index));
                  }
                }
              });
            }
          }
          else if (state is GetLocationComplete) {
            Navigator.pop(context);

            if (state.getLocationResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Location data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            } else {
              setState(() {
                locationList = state.getLocationResponse.masterDataList;

                if (widget.type == TransactionType.MTO) {
                  locationList.removeWhere((company) =>
                      company.value == FromAddress.shared.fromLocationId);
                }
                if (widget.type == TransactionType.II) {
                  toAddress.locationName = fromAddress.fromLocationName;
                  toAddress.locationId = fromAddress.fromLocationId;

                  if (toAddress.locationId != null) {
                    _addressBloc.add(LoadDepartment(
                        companyId: toAddress.companyId,
                        mode: AccessType.All.index));
                    _addressBloc
                        .add(GetProject(locationId: toAddress.locationId));
                  }
                }

                if (editMode == EditMode.On && toAddress.locationId != null) {
                  _addressBloc.add(LoadDepartment(
                      companyId: toAddress.companyId,
                      mode: AccessType.All.index));

                  if (containProject) {
                    _addressBloc
                        .add(GetProject(locationId: toAddress.locationId));
                  }
                }
              });
            }
          }
          else if (state is GetDeliveryLocationComplete) {
            Navigator.pop(context);

            if (state.getDelLocCompleteResp.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Location data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            } else {
              setState(() {
                deliveryLocationList =
                    state.getDelLocCompleteResp.masterDataList;
              });
            }
          }
          else if (state is GetDepartmentComplete) {
            Navigator.pop(context);

            if (state.getDepartmentResponse.masterDataList == null) {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Department data not available',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            } else {
              departmentList = [];
              setState(() {
                departmentList = state.getDepartmentResponse.masterDataList;

                // if (editMode == EditMode.On && toAddress.departmentId != null) {
                //   _addressBloc
                //       .add(GetProject(locationId: toAddress.locationId));
                // }
              });
            }
          }
          else if (state is GetProjectComplete) {
            Navigator.pop(context);

            setState(() {
              if (state.getProjectResp.masterDataList != null) {
                projectsList = state.getProjectResp.masterDataList;
              }
            });
          }
          else if (state is GetPendingSIComplete) {
            Navigator.pop(context);

            pendingRefList = [];
            setState(() {
              if (state.pendingSIResp.pendingRefList != null) {
                pendingRefList = state.pendingSIResp.pendingRefList;
              }
            });
          }
          else if (state is GetPendingTransferOutComplete) {
            Navigator.pop(context);
            if (state.resp.pendingRefList.isNotEmpty) {
              setState(() {
                pendingRefList = state.resp.pendingRefList;
              });
            }
          }
          else if (state is GetPendingMRComplete) {
            Navigator.pop(context);
            if (state.resp.pendingRefList.isNotEmpty) {
              setState(() {
                pendingRefList = state.resp.pendingRefList;
              });
            }
          }
        }, child:
            BlocBuilder<AddressBloc, AddressState>(builder: (context, state) {
          _addressBloc = BlocProvider.of<AddressBloc>(context);

          if (state is GetCompanyInitial) {
            if (widget.type == TransactionType.SR) {
              _addressBloc.add(GetPendingSI(
                  companyId: fromAddress.fromCompanyId,
                  divisionId: fromAddress.fromDivisionId,
                  locationId: fromAddress.fromLocationId));
            } else if (widget.type == TransactionType.MTI) {
              var req = GetPendingTransferOutReq(
                  companyId: fromAddress.fromCompanyId,
                  divisionId: fromAddress.fromDivisionId,
                  locationId: fromAddress.fromLocationId);

              _addressBloc
                  .add(GetPendingTransferOut(req: jsonEncode(req.toJson())));
            } else if ((widget.type == TransactionType.SI ||
                    widget.type == TransactionType.MTO ||
                    widget.type == TransactionType.II) &&
                editMode == EditMode.On) {
              if (_transferType == SIType.Direct) {
                if (companyList == null || companyList.length == 0) {
                  _addressBloc.add(GetCompany(mode: AccessType.All.index));
                }
              } else {
                _addressBloc.add(GetPendingMR(type: widget.type));
              }
            } else if (widget.type == TransactionType.MR) {
              if (companyList == null || companyList.length == 0) {
                _addressBloc.add(GetCompany(mode: AccessType.All.index));
              }
            }
          }
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (containTypes) _getTypeFields(),
                if (containReferences && (_transferType != SIType.Direct))
                  _getReferencesFields(),
                _getTransactionDateField(),
                if (containExpiryDate) _getExpReceiveDateField(),
                if (containPriority) _getPriorityField(),
                if (containAddress) _getAddressFields(),
                if (containProject) _getProjectsField(),
                if (containRemarks) _getRemarksField(),
                if (containTextAddress) _getTextAddressField()
                // if (containDeliveryLocation) _getDeliveryLocation()
              ],
            ),
          );
        })));
  }

  _getTextAddressField() {
    if (toAddress.companyName != null) {
      String address;
      if (toAddress.companyName != "" && toAddress.companyName != null) {
        address = toAddress.companyName;
      }
      if (toAddress.divisionName != "" && toAddress.divisionName != null) {
        address = address + ', ' + toAddress.divisionName;
      }
      if (toAddress.locationName != "" && toAddress.locationName != null) {
        address = address + ', ' + toAddress.locationName;
      }
      if (toAddress.departmentName != "" && toAddress.departmentName != null) {
        address = address + ', ' + toAddress.departmentName;
      }
      if (toAddress.projectName != "" && toAddress.projectName != null) {
        address = address + ', ' + toAddress.projectName;
      }

      return Container(
        padding: const EdgeInsets.all(8.0),
        decoration: Common().getBCoreSD(isMandatory: false, isValidated: false),
        child: Text(address),
      );
    } else {
      return SizedBox();
    }
  }

  _getAddressFields() {
    return Column(
      children: [
        _getCompany(),
        _getDivision(),
        _getLocation(),
        _getDepartment(),
      ],
    );
  }

  _getTypeFields({bool isDisabled = false}) {
    return Column(
      children: [
        Row(children: [
          SizedBox(width: 8),
          Text("Type : "),
          (isDisabled)
              ? Text((_transferType == null)
                  ? ""
                  : (_transferType == SIType.Direct)
                      ? "Direct"
                      : "From MR")
              : FlatButton(
                  child: Text(
                      (_transferType == null)
                          ? ""
                          : (_transferType == SIType.Direct)
                              ? "Direct"
                              : "From MR",
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: BCAppTheme().headingTextColor,
                          fontWeight: FontWeight.bold)),
                  onPressed: () => showTransferTypesDialogue())
        ]),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getReferencesFields() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true, isValidated: toAddress.storeIssueText != null),
          child: SearchableDropdown.single(
            items: (pendingRefList != null && pendingRefList.isNotEmpty)
                ? pendingRefList.map((PendingRefList pendingSI) {
                    return DropdownMenuItem(
                        child: Text(pendingSI.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: pendingSI.text);
                  }).toList()
                : [],
            value: toAddress.storeIssueText,
            hint: "Select a Reference",
            searchHint: "Select a Reference",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  PendingRefList selectedRef = pendingRefList
                      .where((siItem) => siItem.text == value)
                      .toList()[0];

                  toAddress.storeIssueId = int.parse(selectedRef.value);
                  toAddress.storeIssueText = selectedRef.text;

                  toAddress.companyId = selectedRef.toCompanyId == null
                      ? null
                      : selectedRef.toCompanyId.toString();
                  toAddress.divisionId = selectedRef.toDivisionId == null
                      ? null
                      : selectedRef.toDivisionId.toString();
                  toAddress.locationId = selectedRef.toLocationId == null
                      ? null
                      : selectedRef.toLocationId.toString();

                  toAddress.companyName = selectedRef.toCompany;
                  toAddress.divisionName = selectedRef.toDivision;
                  toAddress.locationName = selectedRef.toLocation;
                  toAddress.departmentName = selectedRef.toDepartment;
                  toAddress.projectName = selectedRef.toProject;
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getTransactionDateField() {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: DateFormField(
              initialValue: toAddress.transactionDate,
              format: fromAddress.fromCompanyDateFormat,
              showPicker: showGRNDatePicker,
              onDateChanged: (DateTime date) {
                setState(() {
                  print("Date : $date");

                  toAddress.transactionDate = Utils.getDateByFormat(
                      passedDateTime: date,
                      format: fromAddress.fromCompanyDateFormat);
                });
              },
              decoration:
                  Common().getInputDecoration(false, 'Transaction date')),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getExpReceiveDateField() {
    return Column(
      children: [
        _getItemDate("Expt. Recv. Date"),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getPriorityField() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(8.0),
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: toAddress.deliveryPriority != null),
          child: DropdownButton(
            isDense: false,
            underline: Container(),
            isExpanded: true,
            hint: Text("Delivery Priority"),
            value: toAddress.deliveryPriority,
            items: priorityMenuItems.map((Item item) {
              return DropdownMenuItem(
                  child: Text(item.itemName,
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.black,
                      )),
                  value: item.itemId);
            }).toList(),
            onChanged: (value) {
              setState(() {
                toAddress.deliveryPriority = value;
              });
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getCompany() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: (widget.type != TransactionType.II),
              isValidated: toAddress.companyId != null),
          child: SearchableDropdown.single(
            items: (companyList != null && companyList.isNotEmpty)
                ? companyList.map((CompanyDataList company) {
                    return DropdownMenuItem(
                        child: Text(company.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: company.text);
                  }).toList()
                : [],
            readOnly: (widget.type == TransactionType.II ||
                widget.type == TransactionType.MTO),
            value: toAddress.companyName,
            hint: Constants.companyValidationMsg,
            searchHint: Constants.companyValidationMsg,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  /// Search for the selected company
                  var selectedCompany = companyList
                      .where((company) => company.text == value)
                      .toList()[0];
                  if (widget.type == TransactionType.SI &&
                      selectedCompany.value == fromAddress.fromCompanyId) {
                    Common().showMessage(
                        context: context,
                        message:
                            'From Company & To Company should be different');
                  }
                  // if (widget.type == TransactionType.MTO && selectedCompany.value == fromAddress.fromCompanyId){
                  //   Common().showMessage(context: context,message: 'From Company & To Company should be different');
                  // }
                  else {
                    toAddress.companyName = selectedCompany.text;
                    toAddress.companyId = selectedCompany.value;

                    // resetAddressFields(ResetType.DivLocDeptProj);

                    if (toAddress.companyId != null) {
                      _addressBloc.add(LoadDivision(
                          companyId: toAddress.companyId,
                          mode: AccessType.All.index));
                    }
                  }
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  resetAddressFields(ResetType rType) {
    switch (rType) {
      case ResetType.DivLocDeptProj:
        toAddress.divisionName = null;
        toAddress.divisionId = null;
        // divisionList.clear();
        toAddress.locationName = null;
        toAddress.locationId = null;
        locationList.clear();

        toAddress.departmentName = null;
        toAddress.departmentId = null;
        departmentList.clear();

        toAddress.projectName = null;
        toAddress.projectId = null;
        projectsList.clear();

        break;
      case ResetType.LocDeptProj:
        toAddress.locationName = null;
        toAddress.locationId = null;
        locationList.clear();

        toAddress.departmentName = null;
        toAddress.departmentId = null;
        departmentList.clear();

        toAddress.projectName = null;
        toAddress.projectId = null;
        projectsList.clear();

        break;
      case ResetType.DeptProj:
        toAddress.departmentName = null;
        toAddress.departmentId = null;
        departmentList.clear();

        toAddress.projectName = null;
        toAddress.projectId = null;
        projectsList.clear();

        break;
      case ResetType.Proj:
        toAddress.projectName = null;
        toAddress.projectId = null;
        projectsList.clear();

        break;
    }
  }

  _getDivision() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: validateDivision,
              isValidated: toAddress.divisionId != null),
          child: SearchableDropdown.single(
            // items: divisionList,
            items: (divisionList != null && divisionList.isNotEmpty)
                ? divisionList.map((CommonDataList division) {
                    return DropdownMenuItem(
                        child: Text(division.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: division.text);
                  }).toList()
                : [],
            value: toAddress.divisionName,
            readOnly: (widget.type == TransactionType.II),
            hint: Constants.divisionValidationMsg,
            searchHint: Constants.divisionValidationMsg,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  toAddress.divisionName = value;
                  toAddress.divisionId = divisionList
                          .where((division) =>
                              division.text == toAddress.divisionName)
                          .toList()[0]
                          .value ??
                      "";
                  // resetAddressFields(ResetType.LocDeptProj);

                  _addressBloc.add(LoadLocation(
                      companyId: toAddress.companyId,
                      divisionId: toAddress.divisionId,
                      mode: AccessType.All.index));
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  _getLocation() {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.zero,
            decoration: Common().getBCoreSD(
                isMandatory: validateLocation,
                isValidated: toAddress.locationId != null),
            child: SearchableDropdown.single(
              items: (locationList != null && locationList.isNotEmpty)
                  ? locationList.map((CommonDataList location) {
                      return DropdownMenuItem(
                          child: Text(location.text,
                              style: TextStyle(
                                fontSize: 14.0,
                                color: BCAppTheme().subTextColor,
                              )),
                          value: location.text);
                    }).toList()
                  : [],
              value: toAddress.locationName,
              readOnly: (widget.type == TransactionType.II),
              hint: Constants.locationValidationMsg,
              searchHint: Constants.locationValidationMsg,
              onChanged: (value) {
                if (value != null) {
                  CommonDataList selectedLoc = locationList
                      .where((location) => location.text == value)
                      .toList()[0];

                  if (widget.type == TransactionType.MTO &&
                      selectedLoc.value == fromAddress.fromLocationId) {
                    Common().showMessage(
                        context: context,
                        message:
                            'From Location & To Location should be different');
                  } else {
                    setState(() {
                      toAddress.locationName = selectedLoc.text;
                      toAddress.locationId = selectedLoc.value;

                      // resetAddressFields(ResetType.DeptProj);

                      if (containProject) {
                        _addressBloc
                            .add(GetProject(locationId: toAddress.locationId));
                      }

                      _addressBloc.add(LoadDepartment(
                          companyId: toAddress.companyId,
                          mode: AccessType.All.index));
                    });
                  }
                }
              },
              displayClearIcon: false,
              underline: Container(),
              isExpanded: true,
            )),
        SizedBox(height: 10),
      ],
    );
  }

  _getDepartment() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: validateDepartment,
              isValidated: toAddress.departmentId != null),
          child: SearchableDropdown.single(
            // items: departmentList,
            items: (departmentList != null && departmentList.isNotEmpty)
                ? departmentList.map((CommonDataList department) {
                    return DropdownMenuItem(
                        child: Text(department.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: department.text);
                  }).toList()
                : [],
            value: toAddress.departmentName,

            hint: "Select a Department",
            searchHint: "Select a Department",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList department = departmentList
                      .where((department) => department.text == value)
                      .toList()[0];
                  toAddress.departmentName = department.text;
                  toAddress.departmentId = department.value;
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  _getProjectsField() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration:
              Common().getBCoreSD(isMandatory: false, isValidated: false),
          child: SearchableDropdown.single(
            items: (projectsList != null && projectsList.isNotEmpty)
                ? projectsList.map((CommonDataList project) {
                    return DropdownMenuItem(
                        child: Text(project.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: project.text);
                  }).toList()
                : [],
            value: toAddress.projectName,
            hint: "Select Project",
            searchHint: "Select Project",
            onChanged: (value) {
              if (value != null) {
                CommonDataList selectedProject = projectsList
                    .where((project) => project.text == value)
                    .toList()[0];
                if (widget.type == TransactionType.II &&
                    (toAddress.departmentId == fromAddress.fromDepartmentId &&
                        selectedProject.value == fromAddress.fromProjectId)) {
                  Common().showMessage(
                      context: context,
                      message:
                          'Both From & To Department and Project cann\'t be same');
                } else {
                  setState(() {
                    toAddress.projectName = selectedProject.text;
                    toAddress.projectId = selectedProject.value;
                  });
                }
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _getRemarksField() {
    return Column(
      children: [
        SizedBox(
          height: 50,
          child: TextField(
            controller: TextEditingController()..text = toAddress.remarks ?? "",
            textInputAction: TextInputAction.done,
            autocorrect: false,
            decoration:
                Common().getBCoreID(validityStatus: true, hintText: "Remarks"),
            keyboardType: TextInputType.multiline,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
            onChanged: (value) {
              toAddress.remarks = value;
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  // _getDeliveryLocation() {
  //   return Column(
  //     children: [
  //       Container(
  //         padding: EdgeInsets.zero,
  //         decoration:
  //         Common().getBCoreSD(isMandatory: false, isValidated: false),
  //         child: SearchableDropdown.single(
  //           // items: deliveryLocationList,
  //           items: (deliveryLocationList != null &&
  //               deliveryLocationList.isNotEmpty)
  //               ? deliveryLocationList.map((CommonDataList delivLocation) {
  //             return DropdownMenuItem(
  //                 child: Text(delivLocation.text,
  //                     style: TextStyle(
  //                       fontSize: 14.0,
  //                       color: BCAppTheme().subTextColor,
  //                     )),
  //                 value: delivLocation.text);
  //           }).toList()
  //               : [],
  //           value: toAddress.deliveryLocationName,
  //           hint: "Select Delivery Location",
  //           searchHint: "Select Delivery Location",
  //           onChanged: (value) {
  //             if (value != null) {
  //               setState(() {
  //                 CommonDataList loc = deliveryLocationList
  //                     .where((loc) => loc.text == value)
  //                     .toList()[0];
  //                 toAddress.deliveryLocationId = loc.value;
  //                 toAddress.deliveryLocationName = loc.text;
  //               });
  //             }
  //           },
  //           displayClearIcon: false,
  //           underline: Container(),
  //           isExpanded: true,
  //         ),
  //       ),
  //       SizedBox(height: 10)
  //     ],
  //   );
  // }

  // void getCompanyDateFormat(String companyId) {
  //   for (CompanyDataList masterData in getCompanyResponse.masterDataList) {
  //     if (masterData.value == companyId) {
  //       companyDateFormat = masterData.dateFormat;
  //       break;
  //     }
  //   }
  // }

  /// change type as per selection
  changeType(BuildContext dialogContext, SIType type) {
    Navigator.pop(dialogContext);
    setState(() {
      _transferType = type;
    });
  }

  /// This will display the type selection dialog
  showTransferTypesDialogue() {
    Dialog grnAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Stack(alignment: Alignment.topRight, children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            height: 65,
            child: Column(
              children: [
                Text(
                    (widget.type == TransactionType.SI)
                        ? "Issue Types"
                        : "Transfer Types",
                    textAlign: TextAlign.center),
                SizedBox(height: 16.0),
                Expanded(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      getTypeButton(SIType.Direct),
                      SizedBox(width: 8),
                      getTypeButton(SIType.From_MR),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
            child: SizedBox(
          width: 35,
          height: 35,
          child: IconButton(
              icon: Icon(Icons.close, color: BCAppTheme().redColor),
              onPressed: () {
                Navigator.pop(context);
                // getTypeButtonDirect(SIType.Direct);
                Text((SIType.Direct == SIType.Direct) ? "Direct" : "",
                    style: TextStyle(
                        color: BCAppTheme().secondaryColor, fontSize: 11));
                toAddress.transferTypeId = SIType.Direct.index;

                /// From enum indexing starts from 0. But for API, indexing starts from 1
                if (SIType.Direct == SIType.Direct) {
                  if (companyList == null || companyList.length == 0) {
                    _addressBloc.add(GetCompany(mode: AccessType.All.index));
                  }
                } else {
                  setState(() {
                    containTextAddress = true;
                    containAddress = false;
                    containProject = false;
                  });
                  _addressBloc.add(GetPendingMR(type: widget.type));
                }
                _transferType = SIType.Direct;
              }),
        )),
      ]),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return grnAlert;
      },
    );
  }

  Future<DateTime> showGRNDatePicker() async {
    DateTime date = await showDatePicker(
        context: context,
        helpText: (widget.type == TransactionType.MR)
            ? "Select Request Date"
            : "Select Transaction Date",
        initialDatePickerMode: DatePickerMode.year,
        initialDate: DateTime.now(),
        firstDate: DateTime.now().subtract(Duration(days: 365)),
        lastDate: DateTime(2022)

        // firstDate: DateTime.now().subtract(Duration(days: 365)),
        // lastDate: DateTime(2101),
        );
    return date;
  }

  /// This will return the widget with type buttons
  getTypeButton(SIType type) {
    return Expanded(
      flex: 3,
      child: FlatButton(
          color: BCAppTheme().headingTextColor,
          child: Text((type == SIType.Direct) ? "Direct" : "From MR",
              style:
                  TextStyle(color: BCAppTheme().secondaryColor, fontSize: 11)),
          onPressed: () {
            resetTransactionDetails();

            toAddress.transferTypeId = type.index;

            /// From enum indexing starts from 0. But for API, indexing starts from 1
            if (type == SIType.Direct) {
              if (companyList == null || companyList.length == 0) {
                _addressBloc.add(GetCompany(mode: AccessType.All.index));
              }
            } else {
              setState(() {
                containTextAddress = true;
                containAddress = false;
                containProject = false;
              });
              _addressBloc.add(GetPendingMR(type: widget.type));
            }
            changeType(context, type);
          }),
    );
  }

  resetTransactionDetails() {
    toAddress.clear();

    if (widget.type == TransactionType.SI) {
      SIReq.shared.clear();
    } else if (widget.type == TransactionType.II) {
      SaveIIReq.shared.clear();
    } else if (widget.type == TransactionType.MTO) {
      MTOReq.shared.clear();
    }
    sub_details.clear();

    CommonItemViewModel.shared.cmnItemList.clear();
    editMode = EditMode.Off;

    companyList.clear();
    divisionList.clear();
    locationList.clear();
    deliveryLocationList.clear();
    departmentList.clear();
    projectsList.clear();
    pendingRefList.clear();

    _configUI();
    _configValidation();
    setDefaultTransactionDate();
  }
}

enum ResetType { DivLocDeptProj, LocDeptProj, DeptProj, Proj }

class Item {
  final String itemId;
  final String itemName;

  Item(this.itemId, this.itemName);
}
