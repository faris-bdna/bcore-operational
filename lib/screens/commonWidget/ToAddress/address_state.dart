part of 'address_bloc.dart';

abstract class AddressState extends Equatable {
  const AddressState();

  @override
  List<Object> get props => [];
}

// Get Company
class GetCompanyInitial extends AddressState{}

class GetCompanyInProgress extends AddressState{}

class GetCompanyComplete extends AddressState{
  final GetCompanyResponse getCompanyResp;

  GetCompanyComplete({@required this.getCompanyResp}) : assert(getCompanyResp != null);

  @override
  List<Object> get props => [getCompanyResp];
}

class GetCompanyFailure extends AddressState {
  final String error;

  const GetCompanyFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCompanyFailure { error : $error }';
}


// Get Division
class GetDivisionInitial extends AddressState{}

class GetDivisionInProgress extends AddressState{}

class GetDivisionComplete extends AddressState{
  final GetCommonResp getDivisionResponse;

  GetDivisionComplete({@required this.getDivisionResponse}) : assert(getDivisionResponse != null);

  @override
  List<Object> get props => [getDivisionResponse];
}

class GetDivisionFailure extends AddressState {
  final String error;

  const GetDivisionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDivisionFailure { error : $error }';
}


// Get Location
class GetLocationInitial extends AddressState{}

class GetLocationInProgress extends AddressState{}

class GetLocationComplete extends AddressState{
  final GetCommonResp getLocationResponse;

  GetLocationComplete({@required this.getLocationResponse}) : assert(getLocationResponse != null);

  @override
  List<Object> get props => [getLocationResponse];
}

class GetDeliveryLocationComplete extends AddressState{
  final GetCommonResp getDelLocCompleteResp;

  GetDeliveryLocationComplete({@required this.getDelLocCompleteResp}) : assert(getDelLocCompleteResp != null);

  @override
  List<Object> get props => [getDelLocCompleteResp];
}

class GetLocationFailure extends AddressState {
  final String error;

  const GetLocationFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetLocationFailure { error : $error }';
}


// Get Department
class GetDepartmentInitial extends AddressState{}

class GetDepartmentInProgress extends AddressState{}

class GetDepartmentComplete extends AddressState{
  final GetCommonResp getDepartmentResponse;

  GetDepartmentComplete({@required this.getDepartmentResponse}) : assert(getDepartmentResponse != null);

  @override
  List<Object> get props => [getDepartmentResponse];
}

class GetDepartmentFailure extends AddressState {
  final String error;

  const GetDepartmentFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDepartmentFailure { error : $error }';
}


class GetProjectInitial extends AddressState{}

class GetProjectInProgress extends AddressState{}

class GetProjectComplete extends AddressState{
  final GetCommonResp getProjectResp;

  GetProjectComplete({@required this.getProjectResp}) : assert(getProjectResp != null);

  @override
  List<Object> get props => [getProjectResp];
}

class GetProjectFailure extends AddressState {
  final String error;

  const GetProjectFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetProjectFailure { error : $error }';
}


class GetPendingSIInitial extends AddressState {}

class GetPendingSIInProgress extends AddressState {}

class GetPendingSIComplete extends AddressState {
  final PendingSITOResp pendingSIResp;

  GetPendingSIComplete({@required this.pendingSIResp}) : assert(pendingSIResp != null);

  @override
  List<Object> get props => [pendingSIResp];
}

class GetPendingSIFailure extends AddressState {
  final String error;

  const GetPendingSIFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPendingSIFailure { error : $error }';
}


class SaveDraftTransactionInitial extends AddressState {}

class SaveDraftTransactionInProgress extends AddressState {}

class SaveDraftTransactionComplete extends AddressState {
  final int savedTransactionId;

  SaveDraftTransactionComplete({@required this.savedTransactionId}) : assert(savedTransactionId != null);

  @override
  List<Object> get props => [savedTransactionId];
}

class SaveDraftTransactionFailure extends AddressState {
  final String error;

  const SaveDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}


class UpdateDraftTransactionInitial extends AddressState {}

class UpdateDraftTransactionInProgress extends AddressState {}

class UpdateDraftTransactionComplete extends AddressState {
  /*final int savedTransactionId;

  UpdateDraftTransactionComplete({@required this.savedTransactionId}) : assert(savedTransactionId != null);

  @override
  List<Object> get props => [savedTransactionId];*/
}

class UpdateDraftTransactionFailure extends AddressState {
  final String error;

  const UpdateDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'UpdateDraftTransactionFailure { error : $error }';
}


class GetDraftTransactionInitial extends AddressState {}

class GetDraftTransactionInProgress extends AddressState {}

class GetDraftTransactionComplete extends AddressState {
  final FromAddress fromAddress;
  GetDraftTransactionComplete({@required this.fromAddress}) : assert(fromAddress != null);

  @override
  List<Object> get props => [fromAddress];
}

class GetDraftTransactionFailure extends AddressState {
  final String error;

  const GetDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}


class GetPendingTransferOutInitial extends AddressState {}

class GetPendingTransferOutInProgress extends AddressState {}

class GetPendingTransferOutComplete extends AddressState {
  final PendingSITOResp resp;

  GetPendingTransferOutComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [fromAddress];
}

class GetPendingTransferOutFailure extends AddressState {
  final String error;

  const GetPendingTransferOutFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}



class GetPendingMRInitial extends AddressState {}

class GetPendingMRInProgress extends AddressState {}

class GetPendingMRComplete extends AddressState {
  final PendingSITOResp resp;

  GetPendingMRComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [fromAddress];
}

class GetPendingMRFailure extends AddressState {
  final String error;

  const GetPendingMRFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPendingMRFailure { error : $error }';
}