
import 'dart:convert';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/response/pendingSIResp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'address_event.dart';

part 'address_state.dart';

class AddressBloc extends Bloc<AddressEvent, AddressState> {
  final BCRepository repository;
  final Database database;

  AddressBloc( {@required this.repository,this.database})
      : assert(repository != null),
        super(GetCompanyInitial());

  @override
  Stream<AddressState> mapEventToState(AddressEvent event) async* {
    int draftTransactionID;
    List<DraftTransactionData> draftTransactionData;

    if (event is GetCompany) {
      yield GetCompanyInProgress();

      try {
        final GetCompanyResponse getCompanyResp = await repository
            .getCompanyMaster(mode: event.mode);
        yield GetCompanyComplete(getCompanyResp: getCompanyResp);
      } catch (error) {
        yield GetCompanyFailure(error: error.toString());
      }
    }

    else if (event is LoadDivision) {
      yield GetDivisionInProgress();

      try {
        final GetCommonResp getDivisionResponse = await repository
            .getDivisionMaster(companyId: event.companyId, mode: event.mode);
        yield GetDivisionComplete(getDivisionResponse: getDivisionResponse);
      } catch (error) {
        yield GetDivisionFailure(error: error.toString());
      }
    }

    else if (event is LoadLocation) {
      yield GetLocationInProgress();

      try {
        final GetCommonResp getLocationResponse = await repository
            .getLocationMaster(companyId: event.companyId,
            divisionId: event.divisionId,
            mode: event.mode);
        yield GetLocationComplete(getLocationResponse: getLocationResponse);
      } catch (error) {
        yield GetLocationFailure(error: error.toString());
      }
    }

    else if (event is LoadDepartment) {
      yield GetDepartmentInProgress();

      try {
        final GetCommonResp getDepartmentResponse = await repository
            .getDepartmentMaster(companyId: event.companyId, mode: event.mode);
        yield GetDepartmentComplete(
            getDepartmentResponse: getDepartmentResponse);
      } catch (error) {
        yield GetDepartmentFailure(error: error.toString());
      }
    }

    else if (event is GetProject) {
      yield GetProjectInProgress();

      try {
        final GetCommonResp getProjectResp = await repository.getProjectsMaster(
            locationId: event.locationId);
        yield GetProjectComplete(getProjectResp: getProjectResp);
      } catch (error) {
        yield GetDivisionFailure(error: error.toString());
      }
    }

    else if (event is LoadDeliveryLocation) {
      yield GetLocationInProgress();

      try {
        final GetCommonResp getLocationResponse = await repository
            .getLocationMaster(companyId: event.companyId,
            divisionId: event.divisionId,
            mode: event.mode);
        yield GetDeliveryLocationComplete(
            getDelLocCompleteResp: getLocationResponse);
      } catch (error) {
        yield GetLocationFailure(error: error.toString());
      }
    }

    else if (event is GetPendingSI) {
      yield GetPendingSIInProgress();
      try {
        final PendingSITOResp pendingSIResp = await repository.getPendingSI(
            event.companyId, event.divisionId, event.locationId);
        yield GetPendingSIComplete(pendingSIResp: pendingSIResp);
      } catch (error) {
        yield GetPendingSIFailure(error: error.toString());
      }
    }

    if(event is SaveDraftTransaction) {
      yield SaveDraftTransactionInProgress();

      try {
        draftTransactionID = await database.draftTransactionDao.insertDraftTransaction(event.draftTransactionData);
        yield SaveDraftTransactionComplete(savedTransactionId: draftTransactionID);
      } catch (error) {
        yield SaveDraftTransactionFailure(error: error.toString());
      }
    }

    if(event is UpdateDraftTransaction) {
      yield UpdateDraftTransactionInProgress();

      try {
        bool status  = await database.draftTransactionDao.updateDraftTransaction(event.draftTransactionData);
        yield status ? UpdateDraftTransactionComplete() : UpdateDraftTransactionFailure(error: "Failed");
      } catch (error) {
        yield UpdateDraftTransactionFailure(error: error.toString());
      }
    }

    if (event is GetDraftTransaction) {
      yield GetDraftTransactionInProgress();

      try {

        draftTransactionData =
        await database.draftTransactionDao.getDraftTransactionById(event.draftTransId,event.draftTransType);

        FromAddress resp = FromAddress.fromJson(json.decode(draftTransactionData[0].trans_details));
        yield GetDraftTransactionComplete(fromAddress:resp);
      } catch (error) {
        yield GetDraftTransactionFailure(error: error.toString());
      }
    }

    if (event is GetPendingTransferOut) {
      yield GetPendingTransferOutInProgress();

      try {
        final PendingSITOResp resp = await repository.getPendingTransferOut(event.req);
        yield GetPendingTransferOutComplete(resp: resp);
      } catch (error) {
        yield GetPendingTransferOutFailure(error: error.toString());
      }
    }

    if (event is GetPendingMR) {
      yield GetPendingMRInProgress();

      try {
        final PendingSITOResp resp = await repository.getPendingMaterialRequest(event.type);
        yield GetPendingMRComplete(resp: resp);
      } catch (error) {
        yield GetPendingMRFailure(error: error.toString());
      }
    }
  }
}