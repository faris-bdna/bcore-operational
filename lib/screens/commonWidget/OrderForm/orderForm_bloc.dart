import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/response/getProductGroupResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'orderForm_event.dart';

part 'orderForm_state.dart';

class OrderFormBloc extends Bloc<OrderFormEvent, OrderFormState> {
  final BCRepository repository;
  final Database database;

  OrderFormBloc({@required this.repository, @required this.database})
      : assert(repository != null),
        super(GetSupplierInitial());

  @override
  Stream<OrderFormState> mapEventToState(OrderFormEvent event) async* {
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;

    if (event is GetSupplier) {
      yield GetSupplierInProgress();

      try {
        final SupplierResponse supplierResponse =
            await repository.getSupplierMaster(companyId: event.companyId);
        yield GetSupplierComplete(supplierResponse: supplierResponse);
      } catch (error) {
        yield GetSupplierFailure(error: error.toString());
      }
    }

    else if (event is GetCompany) {
      yield GetCompanyInProgress();

      try {
        final GetCompanyResponse getCompanyResp =
            await repository.getCompanyMaster(mode: event.mode);
        yield GetCompanyComplete(getCompanyResp: getCompanyResp);
      } catch (error) {
        yield GetCompanyFailure(error: error.toString());
      }
    }

    else if (event is GetDivision) {
      yield GetDivisionInProgress();

      try {
        final GetCommonResp getDivisionResponse = await repository
            .getDivisionMaster(companyId: event.companyId, mode: event.mode);
        yield GetDivisionComplete(getDivisionResponse: getDivisionResponse);
      } catch (error) {
        yield GetDivisionFailure(error: error.toString());
      }
    }

    else if (event is GetLocation) {
      yield GetLocationInProgress();

      try {
        final GetCommonResp getLocationResponse =
            await repository.getLocationMaster(
                companyId: event.companyId,
                divisionId: event.divisionId,
                mode: event.mode);
        yield GetLocationComplete(getLocationResponse: getLocationResponse);
      } catch (error) {
        yield GetLocationFailure(error: error.toString());
      }
    }

    else if (event is GetProductGroup) {
      yield GetProductGroupInProgress();

      try {
        final GetProductGroupResp productGroupResp =
        await repository.getProductGroup();
        yield GetProductGroupComplete(getProductGroupResp: productGroupResp);
      } catch (error) {
        yield GetProductGroupFailure(error: error.toString());
      }
    }

    else if (event is GetCommonMaster) {
      yield GetCountryOfOriginMasterInProgress();
      try {
        final GetCommonResp getItemPackingMasterResp =
            await repository.getCommonMaster(event.scmMaster);
        if (event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterLoaded(
              getCommonResp: getItemPackingMasterResp,
              scmMaster: event.scmMaster);
        } else if (event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterLoaded(
              getCommonResp: getItemPackingMasterResp,
              scmMaster: event.scmMaster);
        }
      } catch (error) {
        if (event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterFailure(error: error.toString());
        } else if (event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterFailure(error: error.toString());
        }
      }
    }

    /// Return data if event is LoadItem
    else if (event is LoadItem) {
      yield LoadItemInProgress();

      try {
        if (event.barcode.isNotEmpty) {
          loadItemResp =
              await database.itemBarcodeDao.getItemByBarcode(event.barcode);
        } else {
          loadItemResp =
              await database.itemBarcodeDao.getItemByItemCode(event.itemCode);
        }
        yield LoadItemComplete(loadItemResult: loadItemResp);
      } catch (error) {
        yield LoadItemFailure(error: error.toString());
      }
    }

    /// Return data if event is LoadPacking
    else if (event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp =
            await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    /// Return data if event is LoadItemByPacking
    else if (event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if (event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao
              .getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }

    /// Return data if event is LoadItemByPacking
    else if (event is LoadFilterData) {
      yield LoadItemByFilteringInProgress();

      try {
        // if (event.brandId.isNotEmpty &&
        //     event.originId.isNotEmpty &&
        //     event.productGrpId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao.getItemByFilter(
              event.brandId, event.originId, event.productGrpId);
        // }
        yield LoadItemByFilteringComplete(
            loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByFilteringFailure(error: error.toString());
      }
    }
  }
}
