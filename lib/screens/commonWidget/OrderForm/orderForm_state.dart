part of 'orderForm_bloc.dart';

abstract class OrderFormState extends Equatable {
  const OrderFormState();

  @override
  List<Object> get props => [];
}


/// Get Supplier
class GetSupplierInitial extends OrderFormState{}

class GetSupplierInProgress extends OrderFormState{}

class GetSupplierComplete extends OrderFormState{
  final SupplierResponse supplierResponse;

  GetSupplierComplete({@required this.supplierResponse}) : assert(supplierResponse != null);

  @override
  List<Object> get props => [supplierResponse];
}

class GetSupplierFailure extends OrderFormState {
  final String error;

  const GetSupplierFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSupplierFailure { error : $error }';
}


/// Get Company
class GetCompanyInitial extends OrderFormState{}

class GetCompanyInProgress extends OrderFormState{}

class GetCompanyComplete extends OrderFormState{
  final GetCompanyResponse getCompanyResp;

  GetCompanyComplete({@required this.getCompanyResp}) : assert(getCompanyResp != null);

  @override
  List<Object> get props => [getCompanyResp];
}

class GetCompanyFailure extends OrderFormState {
  final String error;

  const GetCompanyFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCompanyFailure { error : $error }';
}


/// Get Division
class GetDivisionInitial extends OrderFormState{}

class GetDivisionInProgress extends OrderFormState{}

class GetDivisionComplete extends OrderFormState{
  final GetCommonResp getDivisionResponse;

  GetDivisionComplete({@required this.getDivisionResponse}) : assert(getDivisionResponse != null);

  @override
  List<Object> get props => [getDivisionResponse];
}

class GetDivisionFailure extends OrderFormState {
  final String error;

  const GetDivisionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDivisionFailure { error : $error }';
}


/// Get Location
class GetLocationInitial extends OrderFormState{}

class GetLocationInProgress extends OrderFormState{}

class GetLocationComplete extends OrderFormState{
  final GetCommonResp getLocationResponse;

  GetLocationComplete({@required this.getLocationResponse}) : assert(getLocationResponse != null);

  @override
  List<Object> get props => [getLocationResponse];
}

class GetLocationFailure extends OrderFormState {
  final String error;

  const GetLocationFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetLocationFailure { error : $error }';
}


///Get Product Group
class GetProductGroupInProgress extends OrderFormState{}

class GetProductGroupComplete extends OrderFormState{
  final GetProductGroupResp getProductGroupResp;

  GetProductGroupComplete({@required this.getProductGroupResp}) : assert(getProductGroupResp != null);

  @override
  List<Object> get props => [getProductGroupResp];
}

class GetProductGroupFailure extends OrderFormState {
  final String error;

  const GetProductGroupFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetProductGroupFailure { error : $error }';
}

/// Get Origin
class GetCountryOfOriginMasterInProgress extends OrderFormState {}

class GetCountryOfOriginMasterLoaded extends OrderFormState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetCountryOfOriginMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetCountryOfOriginMasterFailure extends OrderFormState {
  final String error;

  const GetCountryOfOriginMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCountryOfOriginMasterFailure { error: $error }';
}

/// Get product brand master
class GetProductBrandMasterInProgress extends OrderFormState {}

class GetProductBrandMasterLoaded extends OrderFormState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetProductBrandMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetProductBrandMasterFailure extends OrderFormState {
  final String error;

  const GetProductBrandMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetProductBrandMasterFailure { error: $error }';
}

/// Load Item
class LoadItemInitial extends OrderFormState{}

class LoadItemInProgress extends OrderFormState{}

class LoadItemComplete extends OrderFormState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends OrderFormState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list
class LoadPackingInitial extends OrderFormState{}

class LoadPackingInProgress extends OrderFormState{}

class LoadPackingComplete extends OrderFormState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends OrderFormState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}

/// Load packing list

class LoadItemByPackingInitial extends OrderFormState{}

class LoadItemByPackingInProgress extends OrderFormState{}

class LoadItemByPackingComplete extends OrderFormState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends OrderFormState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}

/// Load filtering list
class LoadItemByFilteringInitial extends OrderFormState{}

class LoadItemByFilteringInProgress extends OrderFormState{}

class LoadItemByFilteringComplete extends OrderFormState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByFilteringComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByFilteringFailure extends OrderFormState {
  final String error;

  const LoadItemByFilteringFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByFilteringFailure { error : $error }';
}
