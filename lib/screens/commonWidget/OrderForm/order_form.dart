import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemDetail.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceNoResp.dart'
    as srn;
import 'package:bcore_inventory_management/models/response/getProductGroupResp.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderItemInfo.dart';
import 'package:bcore_inventory_management/models/view/orderForm/FilterItemInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderFormInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/commonWidget/OrderForm/orderForm_bloc.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_treeview/flutter_treeview.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class OrderForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const OrderForm({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _OrderFormState createState() =>
      _OrderFormState(repository, appLanguage, database);
}

class _OrderFormState extends State<OrderForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  _OrderFormState(this.repository, this.appLanguage, this.database);

  List<srn.MasterDataList> stockReferenceNoList = [];
  List<SupMasterDataList> supplierList = [];
  List<CompanyDataList> companyList;
  List<CommonDataList> divisionList, locationList, originList, brandList = [];

  List<ItemBarcodeData> listbarcodedata = [];
  List<ItemBarcodeData> data;

  String _selProductGroupId = "";
  String _selProductGroup = "Select Product Group";
  List<Node> _nodes;
  TreeViewController _treeViewController;
  var _productGroupController = TextEditingController(text: "Select Product Group");
  String companyDateFormat;
  OrderItem itemDetail;
  FilterItem filterItem;
  EditMode editMode;
  bool hasReferenceMatchFound = false;

  bool containReference = true;
  bool containRemarks = false;
  bool containNotes = false;
  bool containOtherInfo = false;
  int expDate;
  AlertDialog alert;

  bool shouldEnableSupplier = false;
  String Brandvalue="", Originvalue ="", ProductValue="";
  OrderFormBloc _orderFormBloc;
  GetCommonResp getCommonResp = new GetCommonResp();
  ItemBarcodeData itemBarcodeData;
  List<ItemPackingData> itemPackingDataList = [];
  List<ItemBarcodeData> itemByPackingDataList = [];

  List<Level> level1Temp=[];
  List<Level> level2Temp=[];
  List<Level> level3Temp=[];
  List<Level> level4Temp=[];

  /// Selected item of product group names
  String selectedProductGroup = 'Product Group';

  /// list for product group names
  List<String> productGroupNames = [
    'Product Group',
    'Product Group one',
    'Product Group two',
    'Product Group three'
  ];

  /// Selected item of brand names
  String selectedBrand;

  /// Selected origin name
  String selectedOrigin;

  /// Selected package name
  String selectedPackage = 'package';

  /// Selected pkgName name
  String pkgName;

  /// list for items to show on dialog
  List<String> itemDetails = [
    'Item 1',
    'Item 2',
    'Item 3',
    'Item 4',
    'Item 5',
    'Item 6',
    'Item 7'
  ];

  bool switch_Value = false;

  /// Controller for Search text
  var _searchController = TextEditingController();

  /// Controller for Search text in Product Group Dialog
  var _searchControllerProductGroup = TextEditingController();

  /// list to store the boolean values of items in dialog
  List<bool> _switchValues = [];

  /// list that contain the result of search
  List<String> ItemListSearch = [];

  /// InputType initialization
  InputType _itemInputType = InputType.Barcode;

  /// Controller for Item Code
  var _itemCodeController = TextEditingController();


  _didSelect(InputType type) {
    setState(() {
      _itemCodeController.text = '';
      _itemInputType = type;
      itemPackingDataList = null;
      itemBarcodeData = ItemBarcodeData();
    });
  }

  @override
  void initState() {

    super.initState();

    _configUI();
    _loadItemDetailsIfInEditMode();
    if (cmnOrderFormInfo.tranGenType == null)
      cmnOrderFormInfo.tranGenType = TransGenType.MR.index;

    /// Initialize the boolean array to false
    _switchValues = List.generate(itemDetails.length, (_) => false);
  }

  _setDefaultTransactionDate() {
    if(cmnOrderFormInfo.entryDateTemp == null)
    cmnOrderFormInfo.entryDateTemp = Utils.getDateByFormat(
        passedDateTime: DateTime.now(),
        format: fromAddress.fromCompanyDateFormat);
  }

  _loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.Off) {
      _setDefaultTransactionDate();
      itemDetail = OrderItem();
    } else {
      setState(() {
        _itemInputType = itemDetail.inputType;
        _itemCodeController.text = (_itemInputType == InputType.Barcode)
            ? itemDetail.itemBarcode
            : itemDetail.itemCode;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    editMode = EditMode.Off;
    itemDetail = null;
    _itemCodeController.dispose();
  }

  ///configuring UI for loading dynamic components, as per the transaction based condition.
  _configUI() {
    if (cmnOrderFormInfo != null &&
        cmnOrderFormInfo.entryDateTemp != null &&
        itemDetail != null) {
      editMode = EditMode.On;
    } else {
      editMode = EditMode.Off;
    }
  }

  @override
  Widget build(BuildContext context) {
    bindItemData(ItemBarcodeData item) {
      listbarcodedata.add(item);

      itemBarcodeData = item;
      //   itemBarcodeData =  item;
      if (editMode == EditMode.Off) {
        _setItemInfo();
      }

      if (_itemInputType == InputType.ItemCode) {
        print('called loaditempacking');
        _loadItemPacking(context,item.ItemId.toString());
      } else {
        _setItemName();
        // itemDetail.packingId = itemBarcodeData.item_pck_id;
        //_loadCountry(context);
      }
    }

    return BlocProvider<OrderFormBloc>(
        create: (context) =>
            OrderFormBloc(repository: repository, database: database),
        child: BlocListener<OrderFormBloc, OrderFormState>(
            listener: (context, state) async {
          if (state is GetSupplierInProgress ||
              state is GetCompanyInProgress ||
              state is GetDivisionInProgress ||
              state is GetLocationInProgress ||
              state is GetCountryOfOriginMasterInProgress ||
              state is GetProductBrandMasterInProgress ||
              state is LoadItemInProgress ||
              state is LoadPackingInProgress ||
              state is LoadItemByPackingInProgress ||
              state is LoadItemByFilteringInProgress ||
              state is GetProductGroupInProgress) {
            Common().showLoader(context);
          } else if (state is GetSupplierFailure ||
              state is GetCompanyFailure ||
              state is GetDivisionFailure ||
              state is GetLocationFailure ||
              state is GetCountryOfOriginMasterFailure ||
              state is GetProductBrandMasterFailure ||
              state is LoadItemFailure ||
              state is LoadPackingFailure ||
              state is LoadItemByPackingFailure ||
              state is GetProductGroupFailure ||
              state is LoadItemByFilteringFailure) {
            Navigator.pop(context);
          }

          else if (state is GetSupplierComplete) {
            Navigator.pop(context);

            setState(() {
              supplierList = state.supplierResponse.masterDataList;

                setSupplierInfo();
                _getCompanyList();
                _getBrandList();
                //_getOriginList();

            });
          }

          else if (state is GetCompanyComplete) {
            Navigator.pop(context);
            if (state.getCompanyResp.masterDataList.length > 0) {
              companyList = [];
              setState(() {
                companyList = state.getCompanyResp.masterDataList;
                _getDivisionList();
              });
            }
          }

          else if (state is GetDivisionComplete) {
            Navigator.pop(context);
            if (state.getDivisionResponse.masterDataList.length > 0) {
              divisionList = [];
              setState(() {

                divisionList = state.getDivisionResponse.masterDataList;
                _getLocationList();
              });
            }else{
              locationList = [];
              cmnOrderFormInfo.toLocId = null;
              cmnOrderFormInfo.toLocName = null;

              divisionList = [];
              cmnOrderFormInfo.toDivisionId = null;
              cmnOrderFormInfo.toDivisionName = null;
            }
          }

          else if (state is GetLocationComplete) {
            Navigator.pop(context);
            if (state.getLocationResponse.masterDataList.length > 0) {
              locationList = [];
              setState(() {
                locationList = state.getLocationResponse.masterDataList;

              });
            }
            else{
              locationList = [];
              cmnOrderFormInfo.toLocId = null;
              cmnOrderFormInfo.toLocName = null;
            }
          }

          else if (state is GetProductGroupComplete) {
            Navigator.pop(context);
            GetProductGroupResp productGroupResp = new GetProductGroupResp();
            productGroupResp = state.getProductGroupResp;
            if (productGroupResp.level1.isNotEmpty) {
              _nodes = List<Node>.generate(productGroupResp.level1.length,
                  (int index1) {
                return Node(
                    label: '${productGroupResp.level1[index1].subGroupName}',
                    key: '${productGroupResp.level1[index1].subGroupId}',
                    children: List<Node>.generate(
                      productGroupResp.level2
                          .where((element) =>
                              element.subGroupParentId ==
                              productGroupResp.level1[index1].subGroupId)
                          .length, (int index2) {
                      level2Temp=[];
                      productGroupResp.level2.forEach((element) {
                        if(element.subGroupParentId ==
                            productGroupResp.level1[index1].subGroupId) level2Temp.add(element);});

                        return Node(

                            label: '${level2Temp[index2].subGroupName}',
                            key: '${level2Temp[index2].subGroupId}',
                            children: List<Node>.generate(
                                productGroupResp.level3.where((element) =>
                                        element.subGroupParentId == level2Temp[index2].subGroupId)
                                    .length, (int index3) {
                              level3Temp=[];
                              productGroupResp.level3.forEach((element) {
                                if(element.subGroupParentId ==
                                    level2Temp[index2].subGroupId) level3Temp.add(element);});

                              return Node(
                                  label: '${level3Temp[index3].subGroupName}',
                                  key: '${level3Temp[index3].subGroupId}',
                                  children: List<Node>.generate(
                                      productGroupResp.level4
                                          .where((element) =>
                                              element.subGroupParentId ==
                                                  level3Temp[index3].subGroupId)
                                          .length, (int index4) {
                                    level4Temp=[];
                                    productGroupResp.level4.forEach((element) {
                                      if(element.subGroupParentId ==
                                          level3Temp[index3].subGroupId) level4Temp.add(element);});
                                    return Node(
                                      label: '${level4Temp[index4].subGroupName}',
                                      key: '${level4Temp[index4].subGroupId}',
                                    );
                                  }));
                            }));
                        },
                    ));
              });
            }

          }

          /// check the state and return the origin list
          else if (state is GetCountryOfOriginMasterLoaded) {
            getCommonResp = state.getCommonResp;
            originList = getCommonResp.masterDataList;
            Navigator.pop(context);
          }

          /// check the state and return the brand list
          else if (state is GetProductBrandMasterLoaded) {
            Navigator.pop(context);
            getCommonResp = state.getCommonResp;
            brandList = getCommonResp.masterDataList;
            for (int i = 0; i < 10; i++)
              print("brand list is ${brandList[i].text}");

            _getOriginList();
            if (editMode == EditMode.On) {
              _getOriginList();
            }
          }

          ///  check the brand, origin, product and return the item details
          else if (state is LoadItemByFilteringComplete) {

            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {
              _setDataInfo(state.loadItemByPackingResult);
            }
            Navigator.pop(context);
            showAlert(context: context, message: 'Item Filtering Done');
          }

          /// check the state and return the item details
          else if (state is LoadItemComplete) {
            Navigator.pop(context);
            print(' loadItemResult is  ${state.loadItemResult}');
            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              /// Check whether already the item is added or not
              if (_itemInputType == InputType.Barcode &&
                  OrderItemInfo.shared.orderItemList
                      .where((item) =>
                          item.itemBarcode == state.loadItemResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                _didSelect(InputType.Barcode);
                return;
              }
              bindItemData(state.loadItemResult[0]);
              //  print(OrderItemInfo.shared.orderItemList);
              //quantityFocusNode.requestFocus();
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
          }

          /// check the state and return the packing list
          else if (state is LoadPackingComplete) {
            Navigator.pop(context);
            // Load packing list in dropdown
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                print("packing list is $itemPackingDataList");
                if (editMode == EditMode.Off) {
                  // for(int i = 0; i < itemByPackingDataList.length ; i++){
                    // itemDetail.pkgName = itemPackingDataList[0].packing_text;
                    // itemDetail.packingId =
                    //     int.parse(itemPackingDataList[0].packing_value);
                    itemDetail.packingId =
                        int.parse(itemPackingDataList[0].packing_value);
                    itemDetail.pkgName = pkgName;
                  // }

                  print("packing list inside $itemPackingDataList");
                }
                print("packing list is $itemPackingDataList");
                // Set item name in barcode text field
                _setItemName();
                _findUniqueItemWithItemCodeAndPackingId();
                // FocusScope.of(context).unfocus();
              });
            }
          }

          /// check the state and return the item by packing
          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {
              itemByPackingDataList = state.loadItemByPackingResult;
              print('Load item by packing result is $itemByPackingDataList');

              /// Check whether already the item is added or not
              if (OrderItemInfo.shared.orderItemList
                  .where((item) =>
                      item.itemBarcode == state.loadItemByPackingResult[0].Text)
                  .toList()
                  .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }
              setState(() {
                itemBarcodeData = state.loadItemByPackingResult[0];
                itemDetail.itemBarcode = itemBarcodeData.Text;
                itemDetail.packingDesc =
                    state.loadItemByPackingResult[0].PackingDesc;

                _setItemName();
              });
            }
          }

        },
            child: BlocBuilder<OrderFormBloc, OrderFormState>(
                builder: (context, state) {
          _orderFormBloc = BlocProvider.of<OrderFormBloc>(context);
          if (state is GetSupplierInitial) {
            if (cmnOrderFormInfo.tranGenType == TransGenType.PO.index) {
              shouldEnableSupplier = true;
              _getSupplierList();
              _getProductGroup();
              _getBrandList();
            } else {
              _getSupplierList();
              _getCompanyList();
              _getProductGroup();
              _getBrandList();
              if (state is LoadItemInitial) {
                if (editMode == EditMode.On) {
                  _getItemDetails();
                }
              }
              //_getOriginList();
            }
          }
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _getDatesField(),
                _getTransGenerationTypeField(),
                _getToAddressContainer(),
                _getSearchContainer(),

              ],
            ),
          );
        })));
  }

  /// Method to get item details from DB
  _getItemDetails() {
    _loadItemFromDB();
  }

  /// Method to get Brand and Origin item details from DB
  _getBrandandOriginDetails() {
    _loadBrandandOriginFromDB();
  }

  /// Method to get Brand, Origin and ProductGroup item details from DB
  _loadBrandandOriginFromDB() {
    // If brand, origin and productgroup entered by user, get item details
    if (Brandvalue != null /*&& Originvalue == null && ProductValue == null*/) {
      // if (itemDetail.brandId != null && itemDetail.countryId != null ) {
      _orderFormBloc.add(LoadFilterData(
          brandId: Brandvalue, originId: Originvalue, productGrpId: _selProductGroupId));
      // }
    }
  }

  /// Method to get item details from DB
  _loadItemFromDB() {
    // If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {
      _orderFormBloc
          .add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
    } else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      _orderFormBloc
          .add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
    }
  }

  /// Method to load item by selecting packing from DB
  void _findUniqueItemWithItemCodeAndPackingId() {
    if (_itemInputType == InputType.ItemCode) {
      if (itemDetail.itemId != null && itemDetail.packingId != null) {
        _orderFormBloc.add(LoadItemByPacking(
            itemId: itemDetail.itemId.toString(),
            prodPkgId: itemDetail.packingId.toString()));
      }
    }
  }

  /// Method to load item packing DB
  void _loadItemPacking(BuildContext context,String itemId) {
    print('Called event ');
    _orderFormBloc.add(LoadPacking(itemId: itemId));
  }

  _getDatesField() {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: Column(
        children: [
          SizedBox(
            height: 50,
            child:
                DateFormField(
              initialValue: cmnOrderFormInfo.entryDateTemp,
              format: fromAddress.fromCompanyDateFormat,
                   showPicker: showEntryDatePicker,
                    onDateChanged: (DateTime date) {
                setState(() {
                  print("Date : $date");
                  if(cmnOrderFormInfo.toCompanyId != null){  alert = AlertDialog(
                      content: Text(
                        "Are you sure want to change Entry Date, It will clear all the detail Entries",
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                      actions: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                  child: Text(
                                    'Yes',
                                    style: TextStyle(fontSize: 9, color: Colors.black),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    OrderItemInfo.shared.orderItemList.clear();
                                    }
                                  ),
                            ),
                            ElevatedButton(
                                child: Text(
                                  'No',
                                  style: TextStyle(fontSize: 9, color: Colors.black),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                }),
                          ],
                        ),
                      ],
                    );showDialog(
                      context: context,
                      builder: (BuildContext myContext) {
                        return alert;
                      },
                    );}
                  cmnOrderFormInfo.entryDateTemp = Utils.getDateByFormat(
                      passedDateTime: date,
                      format: fromAddress.fromCompanyDateFormat);
                });

              },
              decoration: Common().getBCoreMandatoryID(
                  isMandatory: true,
                  isValidated: cmnOrderFormInfo.entryDateTemp != null,
                  hintText: 'Transaction date'),
            )

          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  _getTransGenerationTypeField() {
    return Column(
      children: [
        SizedBox(height: 8),
        Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: -4.0,
          alignment: WrapAlignment.center,
          children: List<Widget>.generate(
            TransGenType.values.length - 1,
            (int index) {
              return ChoiceChip(
                selectedColor: BCAppTheme().primaryColor,
                backgroundColor: Colors.black12,
                label: Text('${TransGenType.values[index + 1].value}',
                    style: TextStyle(fontSize: 12, color: Colors.white)),
                // selected: index==0,
                selected: (cmnOrderFormInfo.tranGenType != null)
                    ? cmnOrderFormInfo.tranGenType ==
                        TransGenType.values[index + 1].index
                    : false,
                onSelected: (bool selected) {

                  setState(() {
                    if (selected) {
                      cmnOrderFormInfo.tranGenType = index + 1;
                      if (TransGenType.values[index + 1] == TransGenType.PO) {
                        if(OrderItemInfo.shared.orderItemList .isNotEmpty){  alert = AlertDialog(
                          content: Text(
                            "Are you sure want to change Trn. Gen Type, It will clear all the detail Entries",
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ),
                          actions: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton(
                                      child: Text(
                                        'Yes',
                                        style: TextStyle(fontSize: 9, color: Colors.black),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        OrderItemInfo.shared.orderItemList.clear();
                                      }
                                  ),
                                ),
                                ElevatedButton(
                                    child: Text(
                                      'No',
                                      style: TextStyle(fontSize: 9, color: Colors.black),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                              ],
                            ),
                          ],
                        );showDialog(
                          context: context,
                          builder: (BuildContext myContext) {
                            return alert;
                          },
                        );}
                        shouldEnableSupplier = true;
                        _getSupplierList();
                      } else {
                        if(OrderItemInfo.shared.orderItemList .isNotEmpty){  alert = AlertDialog(
                          content: Text(
                            "Are you sure want to change Trn. Gen Type, It will clear all the detail Entries",
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ),
                          actions: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton(
                                      child: Text(
                                        'Yes',
                                        style: TextStyle(fontSize: 9, color: Colors.black),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        OrderItemInfo.shared.orderItemList.clear();
                                      }
                                  ),
                                ),
                                ElevatedButton(
                                    child: Text(
                                      'No',
                                      style: TextStyle(fontSize: 9, color: Colors.black),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                              ],
                            ),
                          ],
                        );showDialog(
                          context: context,
                          builder: (BuildContext myContext) {
                            return alert;
                          },
                        );}
                        shouldEnableSupplier = false;

                        // _getCompanyField();

                    //companyList.clear();
                      }
                      // cmnSalesClientInfo.billingPartyTypeName =
                      // selected ? accType[index].text : null;
                      // accountsDDList.clear();
                      // _clientBloc.add(LoadAccounts(
                      //     contractType: cmnSalesClientInfo.billingPartyType,
                      //     companyId: int.parse(
                      //         FromAddress.shared.fromCompanyId)));
                    }
                  });
                },
              );
            },
          ).toList(),
        ),
      ],
    );
  }

  /// Method containing To Address details
  _getToAddressContainer() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5), // if you need this
          side: BorderSide(
            color: Colors.grey.withOpacity(0.2),
            // width: 1.5,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Row(
                children: [
                  Icon(Icons.location_on_outlined),
                  Text(
                    ' TO ADDRESS',
                    style: TextStyle(
                      fontSize: 14.0,
                      color: BCAppTheme().subTextColor,
                    ),
                  ),
                ],
              ),
              if (shouldEnableSupplier) _getSupplierField(),
              _getCompanyField(),
              _getDivisionField(),
              _getLocationField(),
            ],
          ),
        ));
  }

  /// Method containing Search container details
  _getSearchContainer() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5), // if you need this
            side: BorderSide(
              color: Colors.grey.withOpacity(0.2),
              width: 1.5,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Icon(Icons.search),
                    Text(
                      ' SEARCH',
                      style: TextStyle(
                        fontSize: 14.0,
                        color: BCAppTheme().subTextColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 4.0,),
                _getProductGroupContainer(),
                _getBrandField(),
                _getOriginField(),
                _getSearchBtn(),
                const SizedBox(height: 10),
                Column(
                  children: [
                    _getItemCodeFields(),
                    if (_itemInputType == InputType.ItemCode)
                      _getPackageField(),
                    _getItemAddBtn()
                  ],
                ),
              ],
            ),
          )),
    );
  }


  /// Method returning Brand dropdown
  _getBrandField() {
    return Column(
      children: [
        const SizedBox(height: 16),
        Container(
          padding: const EdgeInsets.only(left: 15, right: 15),
          // width: MediaQuery.of(context).copyWith().size.width,
          decoration: Common().getBCoreSD(
              isMandatory: false, isValidated: selectedBrand != null),
          child: SearchableDropdown.single(
            items: (brandList != null && brandList.isNotEmpty)
                ? brandList.map((CommonDataList item) {
                    return DropdownMenuItem(
                      child: Text(item.text,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: BCAppTheme().subTextColor,
                          )),
                      // value :item.value);

                      value: item.text,
                    );
                  }).toList()
                : [],
            value: selectedBrand,
            hint: "Select Brand",
            searchHint: "Select brand",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList selectedBrand = brandList
                      .where((brand) => brand.text == value)
                      .toList()[0];
                  Brandvalue = selectedBrand.value;
                  selectedBrand.text = value;
                  // _getOriginList();
                });
              }
            },
            displayClearIcon: true,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  /// Method returning Origin dropdown
  _getOriginField() {
    return Column(
      children: [
        const SizedBox(height: 16),
        Container(
          padding: const EdgeInsets.only(left: 15, right: 15),
          // width: MediaQuery.of(context).copyWith().size.width,
          decoration: Common().getBCoreSD(
              isMandatory: false, isValidated: selectedOrigin != null),
          child: SearchableDropdown.single(
            items: (originList != null && originList.isNotEmpty)
                ? originList.map((CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: selectedOrigin,
            hint: "Select Origin",
            searchHint: "Select Origin",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList selectedOrigin = originList
                      .where((origin) => origin.text == value)
                      .toList()[0];
                  print(selectedOrigin.value);
                  cmnOrderFormInfo.toOriginId = selectedOrigin.value;
                  cmnOrderFormInfo.toOriginName = selectedOrigin.text;
                  Originvalue = selectedOrigin.value;
                  // _getBrandandOriginDetails();
                  selectedOrigin.text = value;
                });
                setState(() {

                });
              }
            },
            displayClearIcon: true,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  /// Method returning Item Code container
  _getItemCodeFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () {
                  _didSelect(InputType.Barcode);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      child: Radio(
                          activeColor: BCAppTheme().headingTextColor,
                          value: InputType.Barcode,
                          groupValue: _itemInputType,
                          onChanged: _didSelect),
                    ),
                    SizedBox(width: 8),
                    Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                  ],
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  _didSelect(InputType.ItemCode);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      child: Radio(
                          activeColor: BCAppTheme().headingTextColor,
                          value: InputType.ItemCode,
                          groupValue: _itemInputType,
                          onChanged: _didSelect),
                    ),
                    SizedBox(width: 8),
                    Text("Item Code", style: TextStyle(fontSize: 13.0))
                  ],
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextFormField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _itemCodeController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: true,
                isValidated: _itemCodeController.text.trim().isNotEmpty,
                hintText: (_itemInputType == InputType.Barcode)
                    ? 'Item Barcode'
                    : 'Item Code',
                icon: _itemInputType == InputType.Barcode
                    ? Icons.blur_linear
                    : Icons.search,
                iconPress: () async {
                  if (_itemInputType == InputType.Barcode) {
                    var result = await BarcodeScanner.scan();
                    setState(() {
                      _itemCodeController.text = result;
                      _getItemDetails();
                    });
                  } else {
                    _getItemDetails();
                  }
                }),
            keyboardType: TextInputType.name,
            // onChanged: (value) {
            //   if (_itemInputType == InputType.Barcode) {
            //     setState(() {
            //       _getItemDetails(blocContext);
            //     });
            //   }
            // },

            validator: (value) {
              _getItemDetails();
              return null;
            },
            onFieldSubmitted: (value) {
              _getItemDetails();
            },
          ),
        )
      ],
    );
  }

  /// Method returning Package dropdown
  _getPackageField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: true, isValidated: itemDetail.packingId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Container(),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.packingId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                  // itemDetail.pkgName = pkgName;
                  // pkgName = itemDetail.pkgName;
                  pkgName = item.packing_text;

                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value),);

                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    // selectedPackage = value;
                    itemDetail.packingId = value;
                        itemDetail.pkgName = itemPackingDataList
                            .where((packing) =>
                        packing.packing_value ==
                            itemDetail.packingId.toString())
                            .toList()[0]
                            .packing_text;
                    // pkgName =  itemPackingDataList
                    //     .where((packing) =>
                    // packing.packing_value ==
                    //     itemDetail.packingId.toString())
                    //     .toList()[0]
                    //     .packing_text;

                       _findUniqueItemWithItemCodeAndPackingId();
                    // If input is item code, load item info based on selected packing

                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  /// Method returning Elevated Button
  _getSearchBtn() {
    return Container(
      // padding: const EdgeInsets.only(left: 100.0, right: 100.0),
      child: ElevatedButton(
        style: ButtonStyle(),
        onPressed: () {
          _searchController.clear();
          ItemListSearch = itemDetails;

          _getBrandandOriginDetails();
          //To open the dialog popup
          //    _openDialogPopup();
        },
        child: const Text('Filter'),
      ),
    );
  }

  /// Method returning Elevated Button
  _getItemAddBtn() {
    return Container(
      // padding: const EdgeInsets.only(left: 100.0, right: 100.0),
      child: ElevatedButton(
        style: ButtonStyle(),
        onPressed: () {
          setState(() {
            if(itemDetail.itemId !=null) {
              if (_itemInputType == InputType.ItemCode &&
                  OrderItemInfo.shared.orderItemList
                      .where((item) =>
                  item.itemBarcode == itemDetail.itemBarcode)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                _didSelect(InputType.Barcode);
                return;
              }

              OrderItemInfo.shared.orderItemList.add(itemDetail);
              showAlert(context: context, message: 'Item added');
              _didSelect(InputType.ItemCode);
              itemDetail = OrderItem();
            }else{
              showAlert(context: context, message: 'Enter Proper Item Details');
            }
          });
        },
        child: const Text('Add Item'),
      ),
    );
  }

  /// method to display popup when productgroup tapped
  _getProductGroupContainer() {
    return Container(
      height: 50,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: BCAppTheme().primaryColor,width: 0.5),
          borderRadius: BorderRadius.circular(5)),
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0,right: 15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                _openDialogPopupProductGroup();
              },
              child: Container(
                width: 200.0,
                child: TextField(
                  controller: _productGroupController,
                  enabled: false,
                ),
              ),
            ),
            Icon(Icons.arrow_drop_down),
            IconButton(icon: Icon(Icons.clear,
              color: _selProductGroupId ==""?Colors.black26 : Colors.black,),
                onPressed: (){
              setState(() {
                _selProductGroupId ="";
                _selProductGroup = "Product Group";
                _productGroupController.text = "Select Product Group";
              });

                })
          ],
        ),
      ),

    );
  }

  _openDialogPopupProductGroup() {
    _treeViewController =
        TreeViewController(children: _nodes, selectedKey: _selProductGroupId);

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Column(crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: TreeView(
                        shrinkWrap: true,
                        controller: _treeViewController,
                        allowParentSelect: false,
                        supportParentDoubleTap: false,
                        onNodeTap: (key) {
                          setState(() {
                            _selProductGroupId = key;
                            _treeViewController =
                                _treeViewController.copyWith(selectedKey: key);
                            _productGroupController.text  = _treeViewController.selectedNode.label;
                            print(_productGroupController.text);
                          });
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ]);
            }),
          ));
        });
  }


  _getSupplierList() {
    if (supplierList.isEmpty)
      _orderFormBloc
          .add(GetSupplier(companyId: fromAddress.fromCompanyId ?? 0));
  }

  _getCompanyList() {
    _orderFormBloc.add(GetCompany(mode: AccessType.All.index));
  }

  /// Method to get CountryOfOrigin List
  _getOriginList() {
    _orderFormBloc.add(GetCommonMaster(scmMaster: ScmMasters.CountryOfOrigin));
  }

  /// Method to get Brand List
  _getProductGroup() {
    _orderFormBloc.add(GetProductGroup());
  }

  /// Method to get Brand List
  _getBrandList() {
    _orderFormBloc.add(GetCommonMaster(scmMaster: ScmMasters.ProductBrand));
  }

  _getSupplierField() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: true,
              isValidated: cmnOrderFormInfo.supplierId != null),
          child: SearchableDropdown.single(
            items: (supplierList != null && supplierList.isNotEmpty)
                ? supplierList.map((SupMasterDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnOrderFormInfo.supplierText,
            hint: "Select Supplier",
            searchHint: "Select Supplier",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  SupMasterDataList selectedSupplier = supplierList
                      .where((element) => element.text == value)
                      .toList()[0];
                  cmnOrderFormInfo.supplierId =
                      int.tryParse(selectedSupplier.value);
                  cmnOrderFormInfo.supplierText = selectedSupplier.text;
                  // setSupplierInfo();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getCompanyField() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
            isMandatory: true,
          ),
          //     isValidated: cmnOrderFormInfo.toCompanyId != null),
          child: SearchableDropdown.single(
            value: cmnOrderFormInfo.toCompanyName,
            items: (companyList != null && companyList.isNotEmpty)
                ? companyList.map((CompanyDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            hint: Constants.companyValidationMsg,
            searchHint: Constants.companyValidationMsg,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CompanyDataList selectedCompany = companyList
                      .where((company) => company.text == value)
                      .toList()[0];
                  cmnOrderFormInfo.toCompanyId = selectedCompany.value;
                  cmnOrderFormInfo.toCompanyName = selectedCompany.text;

                  locationList = [];
                  cmnOrderFormInfo.toLocId = null;
                  cmnOrderFormInfo.toLocName = null;

                  divisionList = [];
                  cmnOrderFormInfo.toDivisionId = null;
                  cmnOrderFormInfo.toDivisionName = null;

                  _getDivisionList();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getDivisionList() {
    _orderFormBloc.add(GetDivision(
        companyId: cmnOrderFormInfo.toCompanyId, mode: AccessType.All.index));
  }

  _getDivisionField() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
            isMandatory: true,
          ),
          //   isValidated: cmnOrderFormInfo.toDivisionName != null),
          child: SearchableDropdown.single(
            items: (divisionList != null && divisionList.isNotEmpty)
                ? divisionList.map((CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnOrderFormInfo.toDivisionName ?? "Select Division",
            hint: Constants.divisionValidationMsg,
            searchHint: Constants.divisionValidationMsg,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList selectedDiv = divisionList
                      .where((div) => div.text == value)
                      .toList()[0];
                  cmnOrderFormInfo.toDivisionId = selectedDiv.value;
                  cmnOrderFormInfo.toDivisionName = selectedDiv.text;

                  locationList = null;
                  cmnOrderFormInfo.toLocName = null;
                  cmnOrderFormInfo.toLocId = null;

                  _getLocationList();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getLocationList() {
    _orderFormBloc.add(GetLocation(
        companyId: cmnOrderFormInfo.toCompanyId,
        divisionId: cmnOrderFormInfo.toDivisionId,
        mode: AccessType.All.index));
  }

  _getLocationField() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
            isMandatory: true,
            //isValidated: cmnOrderFormInfo.toLocName != null
          ),
          child: SearchableDropdown.single(
            items: (locationList != null && locationList.isNotEmpty)
                ? locationList.map((CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnOrderFormInfo.toLocName ?? "select Location",
            hint: Constants.locationValidationMsg,
            searchHint: Constants.locationValidationMsg,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList selectedLocation = locationList
                      .where((loc) => loc.text == value)
                      .toList()[0];
                  cmnOrderFormInfo.toLocId = selectedLocation.value;
                  cmnOrderFormInfo.toLocName = selectedLocation.text;
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  /// Validates the supplier field by checking whether any supplier has been selected or not as per scenarios
  _validateSupplier() {
    // if (_grnType == GRNType.Direct || _prType == PRType.Direct) {
    //   if (selectedSupplierItem == null)
    //     return false;
    //   else
    //     return true;
    // } else
    return false;
  }

  /// Method to set Item name
  void _setItemName() {
    _itemCodeController.text = itemDetail.itemName;
  }

  void _setItemInfo() async {
    List<CountryOriginData> loadCountryResp;
    List<ProductBrandData> loadBrandResp;
    loadCountryResp =
        await database.countryOriginDao.getCountryNameById(itemBarcodeData.OriginCountryId.toString());
    loadBrandResp =
        await database.productBrandDao.getBrandNameById(itemBarcodeData.BrandId.toString());
    itemDetail.countryName = loadCountryResp.length>0 ? loadCountryResp[0].country_name : "--";
    itemDetail.brandName = loadBrandResp.length>0 ? loadBrandResp[0].brand_name : "--";

    setState(()  {
      itemDetail.inputType = _itemInputType;
      itemDetail.itemId = itemBarcodeData.ItemId;
      itemDetail.packingId = itemBarcodeData.ItemPackingId;
      itemDetail.itemBarcode = itemBarcodeData.Text;
      itemDetail.brandId = itemBarcodeData.BrandId.toString();
      itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
      itemDetail.itemCode = itemBarcodeData.ItemCode;
      itemDetail.itemName = itemBarcodeData.ItemName ?? "";
      itemDetail.packingDesc = itemBarcodeData.PackingDesc;
       _setItemName();
    });
  }

  void _setDataInfo(List<ItemBarcodeData> data) async{
    FilterItemInfo.shared.filterItemList.clear();
    for (int i = 0; i < data.length; i++) {
      filterItem = new FilterItem();
      List<ItemPackingData> loadPackingResp;
      print(data.length);
      filterItem.itemId = data[i].ItemId;
      filterItem.packingId = data[i].ItemPackingId;
      filterItem.itemBarcode = data[i].Text;
      filterItem.brandId = data[i].BrandId.toString();
      filterItem.countryId = data[i].OriginCountryId.toString();
      filterItem.itemCode = data[i].ItemCode.toString();
      filterItem.itemName = data[i].ItemName.toString();
      filterItem.isSelected = false;
      loadPackingResp = await database.itemPackingDao.getPackingById(
          data[i].ItemPackingId.toString(),data[i].ItemId.toString());
      filterItem.pkgName = loadPackingResp[0].packing_text;
      FilterItemInfo.shared.filterItemList.add(filterItem);
      print(filterItem);
    }

    // OrderItemInfo.shared.orderItemList.add(itemDetail);
  }

  setSupplierInfo() {
//     // Set supplier currency
//     supCurrency = commonResp.masterDataList
//         .where((element) =>
//     element.value == selectedSupplierItem.currencyId.toString())
//         .toList()[0]
//         .text;
//
//     // Set supplier currency rate
//     supCurrencyRate = selectedSupplierItem.currencyRate.toString();
//
//     // Set GRN header details
//     // Set supplier currency ID
//     cmnSupplierInfo.currencyId = selectedSupplierItem.currencyId;
//
//     // Set supplier currency rate
// //  cmnSupplierInfo.currencyRate = selectedSupplierItem[0].currencyRate.toString();
//
//     // Set supplier ID
//     cmnOrderFormInfo.supplierId = int.parse(selectedSupplierItem.value);
//
//     // Set supplier type ID
// //                cmnSupplierInfo.suppTypeId = selectedSupplierItem[0].supplierTypeId;
//
//     // Set tax applicable
//     cmnSupplierInfo.isTaxApp =
//     selectedSupplierItem.isTaxApplicable == 0 ? false : true;
//
//     // Set cash purchase
//     cmnSupplierInfo.isCashPurchase =
//     selectedSupplierItem.isAllowedCashPurchase == 0 ? false : true;
//
//     cmnSupplierInfo.isSupplierInternal =
//         selectedSupplierItem.isInternal.toBool();
//
//     // set tax & cash purchase status
//     hasEnabledTax = selectedSupplierItem.isTaxApplicable.toBool();
//     hasEnabledCashPurchase =
//         selectedSupplierItem.isAllowedCashPurchase.toBool();
  }

  bindData() {
    var selectedRef = stockReferenceNoList
        .where((refItem) => (refItem.text == cmnBasicFormInfo.stockReferenceNo))
        .toList()[0];

    cmnBasicFormInfo.stockTypeName = selectedRef.stockType;
    cmnBasicFormInfo.stockWarehouseName = selectedRef.warehouseName;
    cmnBasicFormInfo.stockZoneName = selectedRef.zoneName;
    cmnBasicFormInfo.stockRackName = selectedRef.rackName;
    cmnBasicFormInfo.stockBinName = selectedRef.binName;
    cmnBasicFormInfo.stockProductGroupName = selectedRef.productGroupName;
    cmnBasicFormInfo.stockBrandName = selectedRef.brandName;
    cmnBasicFormInfo.stockItemName = selectedRef.itemName;
    // cmnBasicFormInfo.stockReferenceNo = selectedReferenceNo;
    cmnBasicFormInfo.refId = selectedRef.value;
    cmnBasicFormInfo.stockTypeId = selectedRef.stockTypeId;
  }

  Future<DateTime> showEntryDatePicker() async {
    DateTime date;
      date = await showDatePicker(
        context: context,
        helpText: "Select Entry Date",
        initialDate: DateTime.now(),
        firstDate: DateTime.now().subtract(Duration(days: 365)),
        lastDate: DateTime(3000),
      );
      return date;
    }
}
