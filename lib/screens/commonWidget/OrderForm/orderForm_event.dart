part of 'orderForm_bloc.dart';

abstract class OrderFormEvent extends Equatable {
  const OrderFormEvent();
}

class GetSupplier extends OrderFormEvent {
  final String companyId;

  const GetSupplier({@required this.companyId});

  @override
  List<Object> get props => [companyId];

  @override
  String toString() => 'GetSupplier {"CompanyId" : "$companyId" }';
}

class GetCompany extends OrderFormEvent {
  final int mode;

  const GetCompany({@required this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'GetCompany {"Mode" : "$mode" }';
}

class GetDivision extends OrderFormEvent {
  final String companyId;
  final int mode;

  const GetDivision({@required this.companyId, this.mode});

  @override
  List<Object> get props => [companyId, mode];

  @override
  String toString() =>
      'GetDivision {"CompanyId" : "$companyId", "Mode" : "$mode" }';
}

class GetLocation extends OrderFormEvent {
  final String companyId;
  final String divisionId;
  final int mode;

  const GetLocation(
      {@required this.companyId, @required this.divisionId, this.mode});

  @override
  List<Object> get props => [companyId, divisionId, mode];

  @override
  String toString() =>
      'GetLocation {"CompanyId" : "$companyId", "DivisionId" : "$divisionId", "Mode" : "$mode" }';
}

class GetProductGroup extends OrderFormEvent {

  const GetProductGroup();

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetProductGroup ()';
}

/// Event to handle Origin and Brand in Order form
class GetCommonMaster extends OrderFormEvent {
  final ScmMasters scmMaster;

  const GetCommonMaster({@required this.scmMaster});

  @override
  List<Object> get props => [scmMaster];

  @override
  String toString() => 'GetCommonMaster { ' 'scmMaster: $scmMaster}';
}

/// Event to handle Item code or Item barcode
class LoadItem extends OrderFormEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

/// Event to handle package loading
class LoadPacking extends OrderFormEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

/// Event to handle item by package loading
class LoadItemByPacking extends OrderFormEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

///Event to handle item by filter data
class LoadFilterData extends OrderFormEvent {
  final String brandId;
  final String originId;
  final String productGrpId;

  const LoadFilterData({this.brandId, this.originId, this.productGrpId});

  @override
  List<Object> get props => [brandId, originId, productGrpId];

  @override
  String toString() =>
      'LoadFilterData {"BrandId" : "$brandId", "OriginId" : "$originId", "ProductGrpId" : "$productGrpId"}';
}
