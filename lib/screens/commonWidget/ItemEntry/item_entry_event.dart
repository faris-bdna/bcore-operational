part of 'item_entry_bloc.dart';


abstract class ItemEntryEvent extends Equatable {
  const ItemEntryEvent();
}

class LoadCompany extends ItemEntryEvent {
  final int mode;
  const LoadCompany({@required this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'LoadCompany {"Mode" : "$mode" }';
}

class GetMaterialHeaderId extends ItemEntryEvent {
  final String compId;
  final String divId;
  final String locId;
  final String deptId;
  const GetMaterialHeaderId(this.compId, this.divId, this.locId, this.deptId);

  @override
  List<Object> get props => [compId, divId, locId, deptId];

  @override
  String toString() => 'GetMaterialHeaderId {}';
}

class GetMaterialDetailLineNum extends ItemEntryEvent {
  final int matReqId;
  const GetMaterialDetailLineNum({@required this.matReqId});

  @override
  List<Object> get props => [matReqId];

  @override
  String toString() =>
      'GetMaterialHeaderId {"MaterialReqHeaderId" : "$matReqId"}';
}

class SaveItem extends ItemEntryEvent {
  final MaterialRequestData materialRequest;

  const SaveItem({@required this.materialRequest});

  @override
  List<Object> get props => [materialRequest];

  @override
  String toString() => 'SaveItem {"MaterialRequest" : "$materialRequest"}';
}

class SaveItemDetail extends ItemEntryEvent {
  final MaterialRequestDetail materialRequestDetail;

  const SaveItemDetail({@required this.materialRequestDetail});

  @override
  List<Object> get props => [materialRequestDetail];

  @override
  String toString() =>
      'SaveItem {"MaterialRequestDetail" : "$materialRequestDetail"}';
}

class LoadItem extends ItemEntryEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

/// Find Unique Item With ItemCode And PackingId

class LoadItemByPacking extends ItemEntryEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

class LoadPacking extends ItemEntryEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadPackingById extends ItemEntryEvent {
  final String packId;
  final String itemId;

  const LoadPackingById({this.packId, this.itemId});

  @override
  List<Object> get props => [packId, itemId];

  @override
  String toString() =>
      'LoadPacking {"PackId" : "$packId", "ItemId" : "$itemId"}';
}

class GetScmItemPriceDetails extends ItemEntryEvent {
  final TransactionType type;
  final String companyId;
  final String divisionId;
  final String locationId;
  final String toCompanyId;
  final String toDivisionId;
  final String toLocationId;
  final String supplierId;
  final String itemId;
  final String packingId;

  const GetScmItemPriceDetails(
      {this.type,
      this.companyId,
      this.divisionId,
      this.locationId,
      this.toCompanyId,
      this.toDivisionId,
      this.toLocationId,
      this.supplierId,
      this.itemId,
      this.packingId});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetPackingDetails {"Type" : "$type","CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"'
      '"ToDivisionId" : "$toDivisionId","ToLocationId" : "$toLocationId","SupplierId" : "$supplierId","ItemId" : "$itemId","PackingId" : "$packingId"}';
}

class LoadCountryById extends ItemEntryEvent {
  final String countryId;

  const LoadCountryById({this.countryId});

  @override
  List<Object> get props => [countryId];

  @override
  String toString() => 'LoadCountryById {"CountryId" : "$countryId"}';
}

class LoadBrandById extends ItemEntryEvent {
  final String brandId;

  const LoadBrandById({this.brandId});

  @override
  List<Object> get props => [brandId];

  @override
  String toString() => 'LoadBrandById {"BrandId" : "$brandId"}';
}

class GetWareHouse extends ItemEntryEvent {
  const GetWareHouse();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetWareHouse { }';
}

class GetWareHouseZone extends ItemEntryEvent {
  final String warehouseId;

  const GetWareHouseZone({this.warehouseId});

  @override
  List<Object> get props => [warehouseId];

  @override
  String toString() => 'GetWareHouseZone {"WarehouseId" : "$warehouseId"}';
}

class GetWareHouseRack extends ItemEntryEvent {
  final String zoneId;

  const GetWareHouseRack({this.zoneId});

  @override
  List<Object> get props => [zoneId];

  @override
  String toString() => 'GetWareHouseRack {"ZoneId" : "$zoneId"}';
}

class GetWareHouseBin extends ItemEntryEvent {
  final String rackId;

  const GetWareHouseBin({this.rackId});

  @override
  List<Object> get props => [rackId];

  @override
  String toString() => 'GetWareHouseBin {"RackId" : "$rackId"}';
}

class GetSRRefDetails extends ItemEntryEvent {
  final String refId;

  const GetSRRefDetails({@required this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetSRRefDetails {"GetSRRefDetailsReq" : "$refId"}';
}

class GetTransferReceiptReferenceDetails extends ItemEntryEvent {
  final String refId;

  const GetTransferReceiptReferenceDetails({@required this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetTransferReceiptReferenceDetails {"ReferenceId" : "$refId"}';
}

class GetMrRefDetails extends ItemEntryEvent {
  final String refId;
  final TransactionType type;

  const GetMrRefDetails({@required this.type, this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetMaterialRequestReferenceDetails {"ReferenceId" : "$refId","Type" : "$type"}';
}