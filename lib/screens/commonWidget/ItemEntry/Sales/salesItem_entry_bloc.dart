import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/response/SalesItem/getItemPriceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetGRNReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getGRNEntryNobyItemResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'salesItem_entry_event.dart';

part 'salesItem_entry_state.dart';

class SalesItemEntryBloc extends Bloc<SalesItemEntryEvent, SalesItemEntryState> {
  final BCRepository bcRepository;
  final Database database;
  final SOReq soReq;

  SalesItemEntryBloc(
      {@required this.bcRepository,
        @required this.database,
        @required this.soReq})
      : assert(bcRepository != null),
        super(LoadItemInitial());

  @override
  Stream<SalesItemEntryState> mapEventToState(SalesItemEntryEvent event) async* {
    TransactionResp transactionResp;

    int draftTransactionResp;
    bool updateDraftTransactionResp;
    List<DraftTransactionData> getDraftTranId;
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;
    List<CountryOriginData> loadCountryResp;
    List<ProductBrandData> loadBrandResp;

    if (event is LoadItem) {
      yield LoadItemInProgress();

      try {
        if (event.barcode.isNotEmpty) {
          loadItemResp =
          await database.itemBarcodeDao.getItemByBarcode(event.barcode);
        } else {
          loadItemResp =
          await database.itemBarcodeDao.getItemByItemCode(event.itemCode);
        }
        yield LoadItemComplete(loadItemResult: loadItemResp);
      } catch (error) {
        yield LoadItemFailure(error: error.toString());
      }
    }

    else if (event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp =
        await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if (event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao
              .getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadPackingById) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp = await database.itemPackingDao
            .getPackingById(event.packId, event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadCountryById) {
      yield LoadCountryByIdInProgress();

      try {
        loadCountryResp =
        await database.countryOriginDao.getCountryNameById(event.countryId);
        yield LoadCountryByIdComplete(loadCountryResult: loadCountryResp);
      } catch (error) {
        yield LoadCountryByIdFailure(error: error.toString());
      }
    }

    else if (event is LoadBrandById) {
      yield LoadBrandByIdInProgress();

      try {
        loadBrandResp =
        await database.productBrandDao.getBrandNameById(event.brandId);
        yield LoadBrandByIdComplete(loadBrandResult: loadBrandResp);
      } catch (error) {
        yield LoadBrandByIdFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouse) {
      yield GetWareHouseInProgress();

      try {
        final GetCommonResp getCommonResp = await bcRepository.getWareHouse();
        yield GetWareHouseComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseZone) {
      yield GetWareHouseZoneInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseZone(warehouseId: event.warehouseId);
        yield GetWareHouseZoneComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseZoneFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseRack) {
      yield GetWareHouseRackInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseRack(zoneId: event.zoneId);
        yield GetWareHouseRackComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseRackFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseBin) {
      yield GetWareHouseBinInProgress();

      try {
        final GetWarehouseBinMasterResp getWarehouseBinMasterResp =
        await bcRepository.getWareHouseBin(rackId: event.rackId);
        yield GetWareHouseBinComplete(
            getWarehouseBinMasterResp: getWarehouseBinMasterResp);
      } catch (error) {
        yield GetWareHouseBinFailure(error: error.toString());
      }
    }

    else if (event is GetPackingDetails) {
      yield GetPackingDetailsInProgress();

      try {
        final GetScmItemPriceDetailsResp getPackingDetailsResp =
        await bcRepository.getScmItemPriceDetails(
            type: event.type,
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            toCompanyId: event.toCompanyId,
            toDivisionId: event.toDivisionId,
            toLocationId: event.toLocationId,
            supplierId: event.supplierId,
            itemId: event.itemId,
            packingId: event.packingId);
        yield GetPackingDetailsComplete(
            getPackingDetailsResp: getPackingDetailsResp);
      } catch (error) {
        yield GetPackingDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetGRNEntryNobyItem) {
      yield GetGRNEntryNobyItemInProgress();

      try {
        final GetGRNEntryNobyItemResp getGRNEntryNobyItemResp = await bcRepository.getGRNEntryNobyItem(req: event.req);
        yield GetGRNEntryNobyItemComplete(getGRNEntryNobyItemResp: getGRNEntryNobyItemResp);
      } catch (error) {
        yield GetGRNEntryNobyItemFailure(error: error.toString());
      }
    }

    else if (event is LoadBatchById) {
      yield LoadBatchByIdInProgress();

      try {
        List<ItemBatchData> getItemBatchResp = await database.itemBatchDao.getItemBatchByItemId(event.itemId);
        yield LoadBatchByIdComplete(getItemBatchResp: getItemBatchResp);
      } catch (error) {
        yield LoadBatchByIdFailure(error: error.toString());
      }
    }

    else if (event is GetGRNReferenceDetails) {
      yield GetGRNReferenceDetailsInProgress();

      try {
        final GetGRNReferenceDetailsResp resp = await bcRepository.getGRNReferenceDetails(refTypeId: event.refTypeId,refId: event.refId);
        yield GetGRNReferenceDetailsComplete(getGRNReferenceDetailsResp: resp);
      } catch (error) {
        yield GetGRNReferenceDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetPRReferenceDetails) {
      yield GetPRReferenceDetailsInProgress();

      try {
        final GetPRReferenceDetailsResp resp = await bcRepository.getPurchaseReturnReferenceDetails(refId: event.refId);
        yield GetPRReferenceDetailsComplete(getPRReferenceDetailsResp: resp);
      } catch (error) {
        yield GetPRReferenceDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetItemPriceDetails) {
      yield GetItemPriceDetailsInProgress();

      try {
        final GetItemPriceDetailsResp resp =
            await bcRepository.getItemPriceDetails(
                custContactTypeId: event.custContactTypeId, refContactId:
            event.refContactId, itemId: event.itemId, packingId: event.packingId);
        yield GetItemPriceDetailsComplete(getItemPriceDetailsResp: resp);
      } catch (error) {
        yield GetItemPriceDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetSalesOrderDetails) {
      yield GetSalesOrderDetailsInProgress();

      try {
        final GetSODetailsResp resp = await bcRepository.getSalesOrderDetails(event.salesOrderId);
        yield GetSalesOrderDetailsComplete(resp: resp);
      } catch (error) {
        yield GetSalesOrderDetailsFailure(error: error.toString());
      }
    }
  }
}
