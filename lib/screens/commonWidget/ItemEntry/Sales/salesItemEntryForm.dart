import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/itemPrice.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/Sales/salesItem_entry_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SalesItemEntryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;
  final CommonSalesItem item;

  const SalesItemEntryForm(
      {Key key,
      this.repository,
      this.appLanguage,
      this.database,
      this.type,
      this.item})
      : super(key: key);

  @override
  _SalesItemEntryFormState createState() =>
      _SalesItemEntryFormState(repository, appLanguage, database, item);
}

class _SalesItemEntryFormState extends State<SalesItemEntryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  _SalesItemEntryFormState(
      this.repository, this.appLanguage, this.database, this.itemDetail);

  CommonSalesItem itemDetail;
  EditMode editMode;

  /// This property becomes true when we use item price from the api (GetItemPriceDetails). If we are using value from db or user entered it becomes false
  // bool isItemPriceFromBackend = true;

  List<ItemPackingData> itemPackingDataList;
  InputType _itemInputType = InputType.Barcode;

  var _itemCodeController = TextEditingController();

  // var _batchNoController = TextEditingController();
  var _quantityController = TextEditingController();
  var _focQuantityController = TextEditingController();
  // var _unitPriceController = TextEditingController();

  var _actualPriceController = TextEditingController();
  var _discountPercentController = TextEditingController();
  var _discountAmountController = TextEditingController();
  var _taxAmountController = TextEditingController();
  var _netAmountController = TextEditingController();
  var quantityFocusNode = FocusNode();

  ItemPrice itemPrice = ItemPrice();
  SalesItemEntryBloc salesBloc;
  bool itemHasExpiry;

  String _splitBarcode, _splitBCItemWeightPrice, _splitParsedItemPrice;
  bool _isWeightedBC = false;

  @override
  void initState() {
    super.initState();

    _configUI();

    itemPrice.reset();
//    WidgetsBinding.instance.addPostFrameCallback((_) => loadControllers());
//
    loadItemDetailsIfInEditMode();
  }

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {
      setState(() {
        _itemInputType = itemDetail.inputType;
        _itemCodeController.text = (_itemInputType == InputType.Barcode) ? itemDetail.itemBarcode.toString() : itemDetail.itemCode;

        // if (widget.type == TransactionType.GRN) {
        _quantityController.text = itemDetail.quantity.toString();
        _focQuantityController.text =
            (itemDetail.focQuantity ?? 0.0).toString();
        _discountAmountController.text =
            (itemDetail.discount ?? 0.0).toString();
        _discountPercentController.text =
            (itemDetail.discountPercentage ?? 0.0).toString();
        // }
//      qty = itemDetail.unitQty.toString();
//      if (widget.type == TransactionType.PR) {
//        selectedItemBatch = itemDetail.batchNo;
//        selectedGrnNoValue = itemDetail.gRNHeaderId;
//        _quantityController.text = itemDetail.returnedQty.toString();
//      }
//      selectedPackageId = itemDetail.packingId;
//      selectedPackageName = itemDetail.pkgName;
//      selectedExpiryDate = itemDetail.expiryDateTemp;
//      selectedWarehouse = itemDetail.;
//      selectedWarehouseZone = itemDetail.wHouseZoneId.toString();
//      selectedWarehouseRack = itemDetail.wHouseZoneBinRackId.toString();
//      selectedWarehouseBin = itemDetail.wHouseZoneBinId.toString();
        // hasOptedInstallation = itemDetail.isInstallationChecked;
      });
    } else {
      _clearItemInfo();
    }
  }

//
//  loadControllers() {

//    _netAmountController.addListener(_updateNetAmount);
//  }

  @override
  void dispose() {
    super.dispose();

    editMode = EditMode.Off;
    itemDetail = null;

    _actualPriceController.dispose();
    // _batchNoController.dispose();
    _discountPercentController.dispose();
    _discountAmountController.dispose();
    _itemCodeController.dispose();
    // _minShelfLifeController.dispose();
    _netAmountController.dispose();
    _quantityController.dispose();
    _focQuantityController.dispose();
    // _searchController.dispose();
    _taxAmountController.dispose();
    // _unitPriceController.dispose();
  }

  _didSelect(InputType type) {
    setState(() {
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';
      _focQuantityController.text = '';
      _netAmountController.text = '';
      _actualPriceController.text = '';
      _discountAmountController.text = '';
      _taxAmountController.text = '';
      _discountPercentController.text = '';

      _clearItemInfo();

      itemPackingDataList = null;
      itemPrice.reset();
    });
  }

  void _setItemInfo(ItemBarcodeData itemBarcodeData) {
    setState(() {
      if (editMode == EditMode.Off) {
        itemDetail.inputType = _itemInputType;
        itemDetail.itemBarcode = int.tryParse(itemBarcodeData.Text) ?? 0;
        itemDetail.itemCode = itemBarcodeData.ItemCode;
        itemDetail.itemId = itemBarcodeData.ItemId;
        itemDetail.itemId = itemBarcodeData.ItemId;
        itemDetail.unitId = itemBarcodeData.ItemPackingId;
        itemDetail.itemName = itemBarcodeData.ItemName;
        itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
        itemDetail.brandId = itemBarcodeData.BrandId.toString();
        itemPrice.unitPrice = _isWeightedBC ? _splitParsedItemPrice.toDouble() : itemBarcodeData.AverageCost.toDouble();
        itemPrice.netAmount = (itemBarcodeData.NetCost ?? '0.0').toDouble();
        itemDetail.averageCost = itemBarcodeData.AverageCost ?? '0.0';
        itemDetail.netCost = itemBarcodeData.NetCost.toDouble().toString();
        itemDetail.minShelfLife = itemBarcodeData.MinShelfLife;
        itemDetail.productStockTypeId = itemBarcodeData.ProductStockTypeId;
        itemDetail.packingDesc = itemBarcodeData.PackingDesc;
        itemDetail.rateFromApi = 0.0;
      } else {
        itemPrice.unitPrice = itemDetail.rate;
        // isItemPriceFromBackend = itemDetail.isFromBackEnd;
      }
      setValues(itemBarcodeData);
      _updatePrices();
    });
  }

  _clearItemInfo() {
    itemDetail = new CommonSalesItem();
  }

  _configUI() {
    editMode = (itemDetail == null) ? EditMode.Off : EditMode.On;
  }

  bindItemData(ItemBarcodeData itemBarcodeData) {
    itemPrice.reset();

    _setItemInfo(itemBarcodeData);

    if (_itemInputType == InputType.ItemCode) {
      _loadItemPacking();
    } else {
      // itemDetail.unitId = itemBarcodeData.ItemPackingId;
      if (editMode == EditMode.Off) {
        _getItemPriceDetails();
        _loadCountry();
      }
      _setItemName();
    }
    quantityFocusNode.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SalesItemEntryBloc>(
      create: (context) => SalesItemEntryBloc(
          bcRepository: repository, database: database, soReq: SOReq.shared),
      child: BlocListener<SalesItemEntryBloc, SalesItemEntryState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingInProgress ||
              state is GetItemPriceDetailsInProgress ||
              state is LoadCountryByIdInProgress ||
              state is LoadBrandByIdInProgress ||
              state is LoadItemByPackingInProgress) {
            Common().showLoader(context);
          }

          else if (state is LoadItemFailure ||
              state is LoadPackingFailure ||
              state is GetItemPriceDetailsFailure ||
              state is LoadCountryByIdFailure ||
              state is LoadBrandByIdFailure ||
              state is LoadItemByPackingFailure) {
            Navigator.pop(context);
          }

          else if (state is LoadItemComplete) {
            Navigator.pop(context);

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {

              /// Check whether already the item is added or not
              if ( _itemInputType == InputType.Barcode &&
                  editMode == EditMode.Off &&
                  CommonSalesItemInfo.shared.cmnSalesItemList
                      .where((item) =>
                  item.itemBarcode.toString() == state.loadItemResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                _didSelect(InputType.Barcode);
                return;
              }

              if(state.loadItemResult[0].IsSalesAllowed.toBool() == false) {
                showAlert(context: context, message: 'Allow sales is not enabled for this pack');
                return;
              }

              bindItemData(state.loadItemResult[0]);

            } else {
              // _itemInputType = InputType.Barcode;
              // _itemCodeController.text = '';

              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
          }

          else if (state is LoadPackingComplete) {
            Navigator.pop(context);
            // Load packing list in dropdown
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                if (editMode == EditMode.Off) {
                  itemDetail.unitId =
                      int.parse(itemPackingDataList[0].packing_value);
                  itemDetail.pkgName = itemPackingDataList[0].packing_text;
                }
                // Set item name in barcode text field
                _setItemName();
                FocusScope.of(context).unfocus();
                if (editMode == EditMode.Off) {
                  _findUniqueItemWithItemCodeAndPackingId();
                  _loadCountry();
                }
              });
            }
          }

          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {

              /// Check whether already the item is added or not
              if (CommonSalesItemInfo.shared.cmnSalesItemList
                      .where((item) =>
                  item.itemBarcode.toString() == state.loadItemByPackingResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }

              if(state.loadItemByPackingResult[0].IsSalesAllowed.toBool() == false) {
                showAlert(context: context, message: 'Allow sales is not enabled for this pack');
                return;
              }

              setState(() {
                itemDetail.itemBarcode =
                    int.tryParse(state.loadItemByPackingResult[0].Text) ?? 0;
                itemPrice.unitPrice = _isWeightedBC? _splitParsedItemPrice :
                    (state.loadItemByPackingResult[0].AverageCost ?? '0.0')
                        .toDouble();
                itemDetail.packingDesc = state.loadItemByPackingResult[0].PackingDesc;

                _updatePrices();
                _getItemPriceDetails();
              });
            }
          }

          else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.countryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }
            _loadProductBrand(context);
          }

          else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.brandName = productBrandList[0].brand_name.trim();
              });
            }
          }

          else if (state is GetItemPriceDetailsComplete) {
            Navigator.pop(context);

            if (state.getItemPriceDetailsResp.masterDataList != null &&
                state.getItemPriceDetailsResp.masterDataList.length > 0) {
              setState(() {
                if (state.getItemPriceDetailsResp.masterDataList[0]
                        .availableQty !=
                    0.0) {
                  itemPrice.unitPrice = _isWeightedBC ? _splitParsedItemPrice : state
                      .getItemPriceDetailsResp.masterDataList[0].unitPrice
                      .trim();
                  itemDetail.rateFromApi = itemPrice.unitPrice;
                  // isItemPriceFromBackend = false;
                  _updatePrices();
                } else {}
              });
            }
          }
        },
        child: BlocBuilder<SalesItemEntryBloc, SalesItemEntryState>(
            builder: (blocContext, state) {
          salesBloc = BlocProvider.of<SalesItemEntryBloc>(blocContext);

          if (state is LoadItemInitial) {
            if (editMode == EditMode.On) {
              _loadItemFromDB();
            }
          }
          return Column(
            children: [
              _getAccountTypes(),
              SizedBox(height: 10),
              _getItemCodeFields(),
              if (_itemInputType == InputType.ItemCode) _getPackage(),
              if (itemDetail.packingDesc != null) _getPackingDescrField(),
              _getQuantity(),
              _getFocQuantity(),
              SizedBox(height: 10),
              IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      flex: 8,
                      child: Container(
                        decoration: Common().getBCoreSD(isMandatory: false),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text("Item details of ${itemDetail.itemCode ?? "-"} ",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: BCAppTheme().headingTextColor)),
                              _getItemBasicInfo(),
                              _getItemPricing(),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 4),
                    Expanded(
                      flex: 1,
                      child: FlatButton(
                          padding: EdgeInsets.zero,
                          color: BCAppTheme().headingTextColor,
                          onPressed: () {
                            print(itemDetail.toJson());
                            // Validate and save item details
                            if (_validateItemDetail()) {
                              saveItemDetails();
                            }
                          },
                          child: Icon(
                            Icons.save,
                            size: 18,
                            color: Colors.white,
                          )),
                    )
                  ],
                ),
              ),
              SizedBox(height: 10)
            ],
          );
        }),
      ),
    );
  }

  _getAccountTypes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 20,
                height: 20,
                child: Radio(
                    activeColor: BCAppTheme().headingTextColor,
                    value: InputType.Barcode,
                    groupValue: _itemInputType,
                    onChanged: _didSelect),
              ),
              SizedBox(width: 8),
              Text("Item Barcode", style: TextStyle(fontSize: 13.0))
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 20,
                height: 20,
                child: Radio(
                    activeColor: BCAppTheme().headingTextColor,
                    value: InputType.ItemCode,
                    groupValue: _itemInputType,
                    onChanged: _didSelect),
              ),
              SizedBox(width: 8),
              Text("Item Code", style: TextStyle(fontSize: 13.0))
            ],
          ),
        ),
      ],
    );
  }

  _getItemCodeFields() {
    return SizedBox(
      height: 55,
      child: TextFormField(
        enableSuggestions: false,
        textInputAction: TextInputAction.next,
        controller: _itemCodeController,
        autocorrect: false,
        decoration: Common().getInputDecoration(
            _itemCodeController.text.trim().isEmpty,
            (_itemInputType == InputType.Barcode)
                ? 'Item Barcode'
                : 'Item Code',
          icon: _itemInputType == InputType.Barcode ? Icons.blur_linear : Icons.search,
            iconPress: ()async{
              if (_itemInputType == InputType.Barcode) {
                var result = await BarcodeScanner.scan();
                setState(() {
                  _itemCodeController.text = result;
                  _loadItemFromDB();
                });

              }else{
                _loadItemFromDB();
              }
            }
        ),
        keyboardType: TextInputType.name,
        validator: (value){
          _loadItemFromDB();
          return null;
        },
        // onChanged: (value) {
        //   if (_itemInputType == InputType.Barcode) {
        //     _loadItemFromDB();
        //   }
        // },
        onFieldSubmitted: (value) {
          _loadItemFromDB();
        },
      ),
    );
  }

  _getPackage() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: (_itemInputType == InputType.ItemCode),
                isValidated: itemDetail.unitId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Container(),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.unitId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value));
                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.unitId = value;
                    itemDetail.pkgName = itemPackingDataList
                        .where((packing) =>
                            packing.packing_value == value.toString())
                        .toList()[0]
                        .packing_text;
                    _findUniqueItemWithItemCodeAndPackingId();
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getPackingDescrField(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Text(itemDetail.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getQuantity() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreID(
              validityStatus: _quantityController.text.trim().isNotEmpty,
              hintText: 'Quantity',
            ),
            onChanged: (value) {
              setState(() {
                _updateQuantity();
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getFocQuantity() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _focQuantityController,
            autocorrect: false,
            decoration: Common().getBCoreID(
              validityStatus: true,
              hintText: 'FOC Quantity',
            ),
            onChanged: (value) {
              itemDetail.focQuantity = double.tryParse(value);
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  setValues(ItemBarcodeData itemBarcodeData) {
    itemPrice.needToAddTax = true;

    if (editMode == EditMode.Off) {
      _quantityController.text = "";
      _discountPercentController.text = "0";
      _discountAmountController.text = "0";
    }
    if (_quantityController.text.trim().isNotEmpty) {
      itemPrice.quantity =
          _quantityController.text.trim().toDouble(pos: getDeci());
    }
    itemPrice.taxPercentage = (itemBarcodeData != null)
        ? (itemBarcodeData.TaxPercent ?? "0.0").toDouble(pos: getDeci())
        : 0.0;
    // _unitPriceController.text = itemPrice.unitPrice.toString();
  }

  _updatePrices({UpdatingField type}) {
    switch (type ?? UpdatingField.All) {
      case UpdatingField.All:
        _actualPriceController.text = itemPrice.getActualPrice().trim().toString();
        _discountPercentController.text = itemPrice.getDiscPercentage().trim().toString();
        _discountAmountController.text = itemPrice.getDiscount().trim().toString();
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;
      case UpdatingField.UP:
        _actualPriceController.text = itemPrice.getActualPrice().trim().toString();
        _discountPercentController.text = itemPrice.getDiscPercentage().trim().toString();
        _discountAmountController.text = itemPrice.getDiscount().trim().toString();
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;
      case UpdatingField.DA:
        _discountPercentController.text = itemPrice.getDiscPercentage().trim().toString();
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;
      case UpdatingField.DP:
        _discountAmountController.text = itemPrice.getDiscount().trim().toString();
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;

      case UpdatingField.TA:
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;

      default:
        _actualPriceController.text = itemPrice.getActualPrice().trim().toString();
        _discountPercentController.text = itemPrice.getDiscPercentage().trim().toString();
        _discountAmountController.text = itemPrice.getDiscount().trim().toString();
        _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
        _netAmountController.text = itemPrice.getNetAmount().trim().toString();
        break;
    }

    // _actualPriceController.text = itemPrice.getActualPrice().trim().toString();
    // _discountPercentController.text = itemPrice.getDiscPercentage().trim().toString();
    // _discountAmountController.text = itemPrice.getDiscount().trim().toString();
    // _taxAmountController.text = itemPrice.getTaxAmount().trim().toString();
    // _netAmountController.text = itemPrice.getNetAmount().trim().toString();
  }

  _loadItemFromDB() {

    /// If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {
      salesBloc.add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
    }
    else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      if(isWeightedBarcode(_itemCodeController.text)) {
        getSplitBarcodeInfo(_itemCodeController.text);
        salesBloc.add(
            LoadItem(barcode: _splitBarcode, itemCode: ''));
      }
      else{
        salesBloc.add(
            LoadItem(barcode: _itemCodeController.text, itemCode: ''));
      }
    }
  }

  bool isWeightedBarcode(String scannedBC){

    /// Validate barcode length
    if (scannedBC.length == 13) {
      /// Weighted barcode format ( 21 562147 00100 7 )
      /// First two characters ( 21 - 29 ) in the above defined barcode format denotes weighted item
      String partOne = scannedBC.substring(0, 2);
      /// Pattern to match with the barcode
      RegExp regExp = new RegExp(r"^2{1,9}",
        caseSensitive: false,
        multiLine: false,
      );
      return regExp.hasMatch(partOne);
    }
    _isWeightedBC = false;
    return false;
  }

  getSplitBarcodeInfo(String scannedBC){
    _isWeightedBC = true;
    _splitBarcode = scannedBC.substring(2, 7);
    _splitBCItemWeightPrice = scannedBC.substring(7, 12);
    _splitParsedItemPrice = _splitBCItemWeightPrice.substring(0, 3) + "." + _splitBCItemWeightPrice.substring(3, 5);
  }

  // Get Widget Methods

  _getItemBasicInfo() {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.scatter_plot, size: 10),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(getBrandName(),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 13.0),
                        overflow: TextOverflow.ellipsis,
                        softWrap: false),
                  ),
                ],
              )),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.public, size: 12),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(
                      getCountryName(),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 13.0),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ))
            ],
          ),
        ),
      ],
    );
  }

  getCountryName() {
    return itemDetail == null
        ? "--"
        : (itemDetail.countryName != null ? itemDetail.countryName : "--");
  }

  getBrandName() {
    return itemDetail == null
        ? "--"
        : (itemDetail.brandName != null ? itemDetail.brandName : "--");
  }

  _getItemPricing({isEditable = true}) {
    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            print("Price Tapped");

            if(itemDetail.itemId != null) {
              itemPrice.enableEditMode();
              showItemPriceDialogue(isEditable: isEditable);
            } else {
              Common().showMessage(
                  context: context,
                  message: "Please enter Item Barcode/ Item Code");
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: BCAppTheme().headingTextColor),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(widget.type == TransactionType.SO ? "Price" : "Unit",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                    ),
                    _getUnitPriceWidget()
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text( widget.type == TransactionType.SO ? "Total" : "Net",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                    ),
                    _getNetPriceWidget()
                  ],
                )),
                Icon(isEditable ? Icons.edit : Icons.attach_money,
                    size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        ),
      ],
    );
  }

  _getUnitPriceWidget() {
    if (itemPrice.unitPrice != null) {
      return Text(itemPrice.unitPrice.toString(),
          style: TextStyle(fontSize: 13.0));
    } else {
      return Text(
          (itemDetail.averageCost == null) ? '0.0' : itemDetail.averageCost,
          style: TextStyle(fontSize: 13.0));
    }
  }

  _getNetPriceWidget() {
    if (itemPrice.getNetAmount() != null) {
      return Text(itemPrice.getNetAmount().trim().toString(),
          style: TextStyle(fontSize: 13.0));
    } else {
      return Text((itemDetail.netCost == null) ? '0.0' : itemDetail.netCost,
          style: TextStyle(fontSize: 13.0));
    }
  }

  clearFields(){
    _actualPriceController.clear();
    _discountAmountController.clear();
    _discountPercentController.clear();
    _taxAmountController.clear();
    _netAmountController.clear();
  }
  // Pop ups

  showItemPriceDialogue({isEditable = true}) {
    bool
        shouldEnableUnitPrice = /*(widget.type == TransactionType.GRN && cmnSupplierInfo.gRNTypeId == GRNType.Direct.index+1) ?? */ false;

    Dialog itemPriceAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            children: [
              Text("Item Pricing", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: TextEditingController()
                            ..text = itemPrice.unitPrice.toString(),
                          autocorrect: false,
                          enabled:true,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: 'Unit Price'),
                          onChanged: _updateUnitPrice,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      SizedBox(height: 8),
                      Container(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _actualPriceController,
                          autocorrect: false,
                          enabled: false,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: 'Actual Price'),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _discountPercentController,
                          autocorrect: false,
                          enabled: isEditable,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: "Discount %"),
                          keyboardType: TextInputType.number,
                          onChanged: _updateDiscountPercent,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _discountAmountController,
                          autocorrect: false,
                          enabled: isEditable,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: 'Discount Amount'),
                          keyboardType: TextInputType.number,
                          onChanged: _updateDiscountAmount,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _taxAmountController,
                          autocorrect: false,
                          enabled: false,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: 'Tax Amount'),
                          keyboardType: TextInputType.number,
                          onChanged: _updateTaxAmount,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.done,
                          controller: _netAmountController,
                          autocorrect: false,
                          enabled: false,
                          decoration: Common().getBCoreInputDecorationForPopup(
                              labelText: 'Net Amount'),
                          keyboardType: TextInputType.number,
                          onChanged: _updateNetAmount,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 35,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 0.5,
                              style: BorderStyle.solid,
                              color: BCAppTheme().primaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: FlatButton(
//                          color: BCAppTheme().secondaryColor,
                          child: Text("Cancel",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                          onPressed: () {
                            itemPrice.disableEditMode();
                            setState(() {
                              _updatePrices();
                            });
                            Navigator.pop(context);
                          }),
                    ),
                  ),
                  if (isEditable) SizedBox(width: 16.0),
                  if (isEditable)
                    Expanded(
                      child: Container(
                        height: 35,
                        child: FlatButton(
                            color: BCAppTheme().primaryColor,
                            child: Text("Save",
                                style: TextStyle(
                                    color: BCAppTheme().secondaryColor,
                                    fontSize: 11)),
                            onPressed: () {
                              Navigator.pop(context);
                              if (_validateItemPriceDetails()) {
                                setState(() {
                                  itemPrice.updatePrices();
                                  itemPrice.disableEditMode();
                                });
                              } else {
                                // Show item price fields selection message
                              }
                            }),
                      ),
                    )
                ],
              )
            ],
          ),
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return itemPriceAlert;
      },
    );
  }

  Future<DateTime> showItemDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Expiry Date",
      initialDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: itemDetail.minShelfLife))
          : DateTime.now(),
      firstDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: itemDetail.minShelfLife))
          : DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 36500)),
    );
    return date;
  }

  void _updateQuantity() {
    itemDetail.quantity = _quantityController.text.isEmpty
        ? 1.0
        : double.parse(_quantityController.text.trim());
    itemPrice.quantity = _quantityController.text.trim().length > 0
        ? _quantityController.text.trim().toDouble(pos: getDeci())
        : 1.0;
    _updatePrices();
  }

  void _updateUnitPrice(String value) {
    setState(() {
      // itemPrice.tempUnitPrice = _unitPriceController.text.isEmpty
      //     ? 0.0
      //     : _unitPriceController.text.trim().toDouble(pos: getDeci());
      double tempUnitPrice =
          (value == null || value == '') ? 0.0 : value.trim().toDouble();
      itemPrice.tempUnitPrice = tempUnitPrice;
      _updatePrices();
    });
  }

  void _updateDiscountPercent(String value) {
    setState(() {
       itemPrice.tempDiscPercentage =
          _discountPercentController.text.trim().length > 0
              ? _discountPercentController.text.trim().toDouble(pos: getDeci())
              : 0.0;
      _discountAmountController.text = itemPrice.getDiscount().trim().toString();
      _updatePrices(type: UpdatingField.DP);
    });
  }

  void _updateDiscountAmount(String value) {
    setState(() {
      itemPrice.tempDiscount = _discountAmountController.text.trim().length > 0
          ? _discountAmountController.text.trim().toDouble(pos: getDeci())
          : 0.0;
      _discountPercentController.text =
          itemPrice.getDiscPercentage().trim().toString();
      _updatePrices(type: UpdatingField.DA);
    });
  }

  void _updateTaxAmount(String value) {
    setState(() {
      itemPrice.tempTaxAmount = _taxAmountController.text.trim().length > 0
          ? _taxAmountController.text.trim().toDouble(pos: getDeci())
          : 0.0;
      _updatePrices(type: UpdatingField.TA);
    });
  }

  void _updateNetAmount(String value) {
    itemPrice.netAmount = _netAmountController.text.trim().length > 0
        ? _netAmountController.text.trim().toDouble(pos: getDeci())
        : 0.0;
  }

  bool _validateItemDetail() {
    if (_itemCodeController.text.isEmpty) {
      showMessage(
          context: context, message: "Please enter Item code/ Item Barcode");
      return false;
    }

    /// Check whether already the item is added or not
    if (editMode == EditMode.Off &&
        CommonSalesItemInfo.shared.cmnSalesItemList
        .where((item) => item.itemBarcode == itemDetail.itemBarcode)
        .toList()
        .isNotEmpty) {
      showAlert(context: context, message: 'Item already added');
      return false;
    }

    if (itemDetail.unitId != null && itemDetail.unitId == 0) {
      showMessage(context: context, message: "Please select item package");
      return false;
    }
    if (_quantityController.text.trim().isEmpty ||
        double.tryParse(_quantityController.text.trim()) == 0.0 ||
        int.tryParse(_quantityController.text.trim()) == 0) {
      showMessage(context: context, message: "Please enter quantity");
      quantityFocusNode.requestFocus();
      return false;
    }
    return true;
  }

  void saveItemDetails() {
    itemDetail.rate = itemPrice.unitPrice.trim() ?? 0.0;

    /// This flag becomes true when the price is from local db or user has edited the amount. Turns in to false when the price is same as the api resp
    itemDetail.isFromBackEnd = ((itemDetail.rateFromApi ?? 0.0) == (itemDetail.rate ?? 0.0)) ? false : true;
    itemDetail.discount = itemPrice.discount.trim() ?? 0.0;
    itemDetail.discountPercentage = itemPrice.discPercentage.trim() ?? 0.0;
    itemDetail.totalCost = itemPrice.getGrossPrice().trim().toString();
    itemDetail.netCost = itemPrice.getNetAmount().trim().toString();

    if (editMode == EditMode.On) {
      var indexOfItemInExistingList = CommonSalesItemInfo
          .shared.cmnSalesItemList
          .indexWhere((item) => item.lineNum == itemDetail.lineNum);
      if (indexOfItemInExistingList != -1) {
        CommonSalesItemInfo.shared.cmnSalesItemList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CommonSalesItem.fromJson(itemDetail.toJson())]);
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Updated!'), duration: Duration(milliseconds: 500)));
        editMode = EditMode.Off;
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Please try again'),
            duration: Duration(milliseconds: 500)));
      }
    } else {
      CommonSalesItemInfo.shared.cmnSalesItemList
          .add(CommonSalesItem.fromJson(itemDetail.toJson()));
      Common().showMessage(context: context, message: 'Saved!');
    }
    _didSelect(InputType.Barcode);
  }

  void _setItemName() {
    _itemCodeController.text = itemDetail.itemName;
  }

  bool _validateItemPriceDetails() {
    // if (this._unitPriceController.text.isEmpty) {} else
    if (_actualPriceController.text.isEmpty) {
    } else if (_discountPercentController.text.isEmpty) {
    } else if (_discountAmountController.text.isEmpty) {
//    } else if (_taxTypeController.text.isEmpty) {
    } else if (_taxAmountController.text.isEmpty) {
    } else if (_netAmountController.text.isEmpty) {
    } else {
      return true;
    }
    return false;
  }

  void _findUniqueItemWithItemCodeAndPackingId() {
    if (_itemInputType == InputType.ItemCode) {
      if (itemDetail.itemId != null && itemDetail.unitId != null) {
        salesBloc.add(LoadItemByPacking(
            itemId: itemDetail.itemId.toString(),
            prodPkgId: itemDetail.unitId.toString()));
      }
    }
  }

  void _loadItemPacking() {
    salesBloc.add(LoadPacking(itemId: itemDetail.itemId.toString()));
  }

  void _getItemPriceDetails() {
    salesBloc.add(GetItemPriceDetails(
        itemId: itemDetail.itemId,
        packingId: itemDetail.unitId,
        refContactId: cmnSalesClientInfo.billingPartyID,
        custContactTypeId: cmnSalesClientInfo
            .billingPartyType));
  }

  void _loadCountry() {
    salesBloc.add(LoadCountryById(countryId: itemDetail.countryId));
  }

  void _loadProductBrand(BuildContext context) {
    salesBloc.add(LoadBrandById(brandId: itemDetail.brandId));
  }
}
