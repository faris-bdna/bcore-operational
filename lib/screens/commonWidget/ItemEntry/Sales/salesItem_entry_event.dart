part of 'salesItem_entry_bloc.dart';

abstract class SalesItemEntryEvent extends Equatable {
  const SalesItemEntryEvent();
}

class LoadItem extends SalesItemEntryEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

class LoadPacking extends SalesItemEntryEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadPackingById extends SalesItemEntryEvent {
  final String packId;
  final String itemId;

  const LoadPackingById({this.packId, this.itemId});

  @override
  List<Object> get props => [packId, itemId];

  @override
  String toString() =>
      'LoadPacking {"PackId" : "$packId", "ItemId" : "$itemId"}';
}

class LoadCountryById extends SalesItemEntryEvent {
  final String countryId;

  const LoadCountryById({this.countryId});

  @override
  List<Object> get props => [countryId];

  @override
  String toString() => 'LoadCountryById {"CountryId" : "$countryId"}';
}

class LoadBrandById extends SalesItemEntryEvent {
  final String brandId;

  const LoadBrandById({this.brandId});

  @override
  List<Object> get props => [brandId];

  @override
  String toString() => 'LoadBrandById {"BrandId" : "$brandId"}';
}

class LoadItemByPacking extends SalesItemEntryEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

class GetWareHouse extends SalesItemEntryEvent {
  const GetWareHouse();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetWareHouse { }';
}

class GetWareHouseZone extends SalesItemEntryEvent {
  final String warehouseId;

  const GetWareHouseZone({this.warehouseId});

  @override
  List<Object> get props => [warehouseId];

  @override
  String toString() => 'GetWareHouseZone {"WarehouseId" : "$warehouseId"}';
}

class GetWareHouseRack extends SalesItemEntryEvent {
  final String zoneId;

  const GetWareHouseRack({this.zoneId});

  @override
  List<Object> get props => [zoneId];

  @override
  String toString() => 'GetWareHouseRack {"ZoneId" : "$zoneId"}';
}

class GetWareHouseBin extends SalesItemEntryEvent {
  final String rackId;

  const GetWareHouseBin({this.rackId});

  @override
  List<Object> get props => [rackId];

  @override
  String toString() => 'GetWareHouseBin {"RackId" : "$rackId"}';
}

class GetPackingDetails extends SalesItemEntryEvent {
  final TransactionType type;
  final String companyId;
  final String divisionId;
  final String locationId;
  final String toCompanyId;
  final String toDivisionId;
  final String toLocationId;
  final String supplierId;
  final String itemId;
  final String packingId;

  const GetPackingDetails(
      {this.type,
        this.companyId,
        this.divisionId,
        this.locationId,
        this.toCompanyId,
        this.toDivisionId,
        this.toLocationId,
        this.supplierId,
        this.itemId,
        this.packingId});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetPackingDetails {"Type" : "$type","CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"'
          '"ToDivisionId" : "$toDivisionId","ToLocationId" : "$toLocationId","SupplierId" : "$supplierId","ItemId" : "$itemId","PackingId" : "$packingId"}';
}

class GetGRNEntryNobyItem extends SalesItemEntryEvent {
  final String req;

  const GetGRNEntryNobyItem(
      {this.req});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetGRNEntryNobyItem $req';
}

class LoadBatchById extends SalesItemEntryEvent {
  final int itemId;

  const LoadBatchById({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadBatchById {"ItemId" : "$itemId"}';
}

/// Used for getting the Items list as per the GRN No in LPO or IPO
class GetGRNReferenceDetails extends SalesItemEntryEvent {
  final String refTypeId;
  final String refId;

  const GetGRNReferenceDetails({this.refTypeId, this.refId});

  @override
  List<Object> get props => [refTypeId,refId];

  @override
  String toString() => 'GetGRNReferenceDetails {"ReferenceTypeId" : "$refTypeId","ReferenceId" : "$refId"}';
}

class GetPRReferenceDetails extends SalesItemEntryEvent {
  final String refId;

  const GetPRReferenceDetails({this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetPRReferenceDetails {"ReferenceId" : "$refId"}';
}

class GetItemPriceDetails extends SalesItemEntryEvent {
  final int custContactTypeId;
  final int refContactId;
  final int itemId;
  final int packingId;

  const GetItemPriceDetails({ this.custContactTypeId, this.refContactId, this.itemId, this.packingId});

  @override
  List<Object> get props => [custContactTypeId,refContactId,itemId,packingId];

  @override
  String toString() =>
      'GetItemPriceDetails {"custContactTypeId" : "$custContactTypeId",'
          ' "refContactId" : "$refContactId","itemId" : "$itemId",'
          ' "packingId" : "$packingId"}';
}

/// Used for getting the sales order details
class GetSalesOrderDetails extends SalesItemEntryEvent {
  final int salesOrderId;

  const GetSalesOrderDetails({this.salesOrderId});

  @override
  List<Object> get props => [salesOrderId];

  @override
  String toString() => 'GetSalesOrderDetails {"SalesOrderId" : "$salesOrderId"}';
}
