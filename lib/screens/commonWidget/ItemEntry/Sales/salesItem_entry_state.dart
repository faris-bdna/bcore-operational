part of 'salesItem_entry_bloc.dart';

abstract class SalesItemEntryState extends Equatable {
  const SalesItemEntryState();

  @override
  List<Object> get props => [];
}

class LoadItemInitial extends SalesItemEntryState{}

class LoadItemInProgress extends SalesItemEntryState{}

class LoadItemComplete extends SalesItemEntryState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends SalesItemEntryState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends SalesItemEntryState{}

class LoadPackingInProgress extends SalesItemEntryState{}

class LoadPackingComplete extends SalesItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends SalesItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing by packing id

class LoadPackingByIdInitial extends SalesItemEntryState{}

class LoadPackingByIdInProgress extends SalesItemEntryState{}

class LoadPackingByIdComplete extends SalesItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends SalesItemEntryState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends SalesItemEntryState{}

class LoadItemByPackingInProgress extends SalesItemEntryState{}

class LoadItemByPackingComplete extends SalesItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends SalesItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends SalesItemEntryState{}

class LoadCountryByIdInProgress extends SalesItemEntryState{}

class LoadCountryByIdComplete extends SalesItemEntryState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends SalesItemEntryState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends SalesItemEntryState{}

class LoadBrandByIdInProgress extends SalesItemEntryState{}

class LoadBrandByIdComplete extends SalesItemEntryState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends SalesItemEntryState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get WareHouse

class GetWareHouseInitial extends SalesItemEntryState{}

class GetWareHouseInProgress extends SalesItemEntryState{}

class GetWareHouseComplete extends SalesItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends SalesItemEntryState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends SalesItemEntryState{}

class GetWareHouseZoneInProgress extends SalesItemEntryState{}

class GetWareHouseZoneComplete extends SalesItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends SalesItemEntryState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends SalesItemEntryState{}

class GetWareHouseRackInProgress extends SalesItemEntryState{}

class GetWareHouseRackComplete extends SalesItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends SalesItemEntryState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends SalesItemEntryState{}

class GetWareHouseBinInProgress extends SalesItemEntryState{}

class GetWareHouseBinComplete extends SalesItemEntryState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends SalesItemEntryState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get Packing Details

class GetPackingDetailsInitial extends SalesItemEntryState{}

class GetPackingDetailsInProgress extends SalesItemEntryState{}

class GetPackingDetailsComplete extends SalesItemEntryState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetPackingDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetPackingDetailsFailure extends SalesItemEntryState {
  final String error;

  const GetPackingDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPackingDetailsFailure { error : $error }';
}


/// Get GRN EntryNobyItem

class GetGRNEntryNobyItemInitial extends SalesItemEntryState{}

class GetGRNEntryNobyItemInProgress extends SalesItemEntryState{}

class GetGRNEntryNobyItemComplete extends SalesItemEntryState{
  final GetGRNEntryNobyItemResp getGRNEntryNobyItemResp;

  GetGRNEntryNobyItemComplete({@required this.getGRNEntryNobyItemResp}) : assert(getGRNEntryNobyItemResp != null);

  @override
  List<Object> get props => [getGRNEntryNobyItemResp];
}

class GetGRNEntryNobyItemFailure extends SalesItemEntryState {
  final String error;

  const GetGRNEntryNobyItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNEntryNobyItemFailure { error : $error }';
}


/// LoadBatchById States

class LoadBatchByIdInitial extends SalesItemEntryState{}

class LoadBatchByIdInProgress extends SalesItemEntryState{}

class LoadBatchByIdComplete extends SalesItemEntryState{
  final List<ItemBatchData> getItemBatchResp;

  LoadBatchByIdComplete({@required this.getItemBatchResp}) : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class LoadBatchByIdFailure extends SalesItemEntryState {
  final String error;

  const LoadBatchByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBatchByIdFailure { error : $error }';
}


/// GetGRNReferenceDetails States

class GetGRNReferenceDetailsInitial extends SalesItemEntryState{}

class GetGRNReferenceDetailsInProgress extends SalesItemEntryState{}

class GetGRNReferenceDetailsComplete extends SalesItemEntryState{
  final GetGRNReferenceDetailsResp getGRNReferenceDetailsResp;

  GetGRNReferenceDetailsComplete({@required this.getGRNReferenceDetailsResp}) : assert(getGRNReferenceDetailsResp != null);

  @override
  List<Object> get props => [getGRNReferenceDetailsResp];
}

class GetGRNReferenceDetailsFailure extends SalesItemEntryState {
  final String error;

  const GetGRNReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNReferenceDetailsFailure { error : $error }';
}


/// GetPRReferenceDetails States

class GetPRReferenceDetailsInitial extends SalesItemEntryState{}

class GetPRReferenceDetailsInProgress extends SalesItemEntryState{}

class GetPRReferenceDetailsComplete extends SalesItemEntryState{
  final GetPRReferenceDetailsResp getPRReferenceDetailsResp;

  GetPRReferenceDetailsComplete({@required this.getPRReferenceDetailsResp}) : assert(getPRReferenceDetailsResp != null);

  @override
  List<Object> get props => [getPRReferenceDetailsResp];
}

class GetPRReferenceDetailsFailure extends SalesItemEntryState {
  final String error;

  const GetPRReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPRReferenceDetailsFailure { error : $error }';
}


/// GetItemPriceDetails States

class GetItemPriceDetailsInitial extends SalesItemEntryState{}

class GetItemPriceDetailsInProgress extends SalesItemEntryState{}

class GetItemPriceDetailsComplete extends SalesItemEntryState{
  final GetItemPriceDetailsResp getItemPriceDetailsResp;

  GetItemPriceDetailsComplete({@required this.getItemPriceDetailsResp}) : assert(getItemPriceDetailsResp != null);

  @override
  List<Object> get props => [getItemPriceDetailsResp];
}

class GetItemPriceDetailsFailure extends SalesItemEntryState {
  final String error;

  const GetItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemPriceDetailsFailure { error : $error }';
}



/// Get SalesOrderDetails States

class GetSalesOrderDetailsInitial extends SalesItemEntryState{}

class GetSalesOrderDetailsInProgress extends SalesItemEntryState{}

class GetSalesOrderDetailsComplete extends SalesItemEntryState{
  final GetSODetailsResp resp;

  GetSalesOrderDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetSalesOrderDetailsFailure extends SalesItemEntryState {
  final String error;

  const GetSalesOrderDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesOrderDetails { error : $error }';
}