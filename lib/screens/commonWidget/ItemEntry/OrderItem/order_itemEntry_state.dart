part of 'order_itemEntry_bloc.dart';

abstract class OrderItemEntryState extends Equatable {
  const OrderItemEntryState();

  @override
  List<Object> get props => [];
}

/// Load Item

class LoadItemInitial extends OrderItemEntryState{}

class LoadItemInProgress extends OrderItemEntryState{}

class LoadItemComplete extends OrderItemEntryState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends OrderItemEntryState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends OrderItemEntryState{}

class LoadPackingInProgress extends OrderItemEntryState{}

class LoadPackingComplete extends OrderItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends OrderItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends OrderItemEntryState{}

class LoadItemByPackingInProgress extends OrderItemEntryState{}

class LoadItemByPackingComplete extends OrderItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends OrderItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends OrderItemEntryState{}

class LoadCountryByIdInProgress extends OrderItemEntryState{}

class LoadCountryByIdComplete extends OrderItemEntryState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends OrderItemEntryState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends OrderItemEntryState{}

class LoadBrandByIdInProgress extends OrderItemEntryState{}

class LoadBrandByIdComplete extends OrderItemEntryState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends OrderItemEntryState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get SalesStock Details

class GetSalesStockDetailsInitial extends OrderItemEntryState{}

class GetSalesStockDetailsInProgress extends OrderItemEntryState{}

class GetSalesStockDetailsComplete extends OrderItemEntryState{
  final GetSalesStockDetailsResp resp;

  GetSalesStockDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetSalesStockDetailsFailure extends OrderItemEntryState {
  final String error;

  const GetSalesStockDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesStockDetailsFailure { error : $error }';
}


/// Get SortedItem Details

class SortedItemInitial extends OrderItemEntryState{}

class SortedItemInProgress extends OrderItemEntryState{}

class SortedItemComplete extends OrderItemEntryState{
  GetCommonResp sortedList;

  SortedItemComplete({@required this.sortedList}) : assert(sortedList != null);

  @override
  List<Object> get props => [sortedList];
}

class SortedItemFailure extends OrderItemEntryState {
  final String error;

  const SortedItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SortItemFailure { error : $error }';
}
