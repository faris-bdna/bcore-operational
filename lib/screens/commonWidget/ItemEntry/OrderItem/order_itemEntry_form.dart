import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/stockDetailReq.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/GetSalesStockDetailsResp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderFormInfo.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderItemInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/OrderItem/order_itemEntry_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bcore_inventory_management/models/view/orderForm/FilterItemInfo.dart';

class OrderItemEntryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final OrderItem item;

  const OrderItemEntryForm(
      {Key key, this.repository, this.appLanguage, this.database, this.item})
      : super(key: key);

  @override
  _OrderItemEntryFormState createState() =>
      _OrderItemEntryFormState(repository, appLanguage, database, item);
}

class _OrderItemEntryFormState extends State<OrderItemEntryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  OrderItem itemDetail;
  EditMode editMode;
  FilterItemInfo filterItemInfo;
  FilterItem filterItemDetail;
  ItemBarcodeData itemBarcodeData;
  List<ItemPackingData> itemPackingDataList;
  InputType _itemInputType = InputType.Barcode;
  bool isselected = false;
  List<OrderItem> tempOrderList = [];
  List<OrderItemInfo> orderitemInfo;
  List<FilterItem> tempfilterItemList = [];
  List<FilterItem> searchTempfilterItemList = [];
  List<FilterItem> selectedlist = [];
  int selIndex = 0;
  bool tapped = false;


  var _itemCodeController = TextEditingController();
  var _quantityController = TextEditingController();
  var _searchController = TextEditingController();
  var quantityFocusNode = FocusNode();

  OrderItemInfo _orderItemInfo;

  OrderItemEntryBloc _orderItemEntryBloc;
  BuildContext buildContext;

  SalesStockDetailsList salesStockDetails;
  int newindex;

  /// Will contain API response based type stock details
  List<StcDtItem> stcDtItemsList = [];
  List<FilterItemInfo> filteriteminfo;
  List<OrderItem> temporderItemList = [];



  /// list to store the boolean values of items in dialog
  List<bool> _switchValues = [];

  /// Will contain view model type list of stock details items list

  _OrderItemEntryFormState(
      this.repository, this.appLanguage, this.database, this.itemDetail);

  @override
  void initState() {
    super.initState();

    _configUI();
    loadItemDetailsIfInEditMode();

  }

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {
      setState(() {
        _itemInputType = itemDetail.inputType;
        _itemCodeController.text = (_itemInputType == InputType.Barcode)
            ? itemDetail.itemBarcode
            : itemDetail.itemCode;

        _quantityController.text = itemDetail.orderQty.toString();
      });
    } else {
      itemDetail =
          OrderItem(); // TODO Confirm this doesn't make any issue in the flow
    }
  }



  @override
  void dispose() {
    super.dispose();

    editMode = EditMode.Off;
    itemDetail = null;

    _itemCodeController.dispose();
    _quantityController.dispose();
  }

  _didSelect(InputType type) {
    setState(() {
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';

      _clearItemInfo();
//      resetItemParameters();
      itemDetail.brandName = null;
      itemDetail.countryName = null;
      itemBarcodeData = null;
      itemPackingDataList = null;
      itemDetail.packingId = null;
      itemDetail.pkgName = "";
      itemPackingDataList = null;
    });
  }

  void _setItemInfo() {
    setState(() {
      itemDetail.inputType = _itemInputType;
      itemDetail.itemBarcode = itemBarcodeData.Text;
      itemDetail.itemCode = itemBarcodeData.ItemCode;
      itemDetail.itemId = itemBarcodeData.ItemId;
      itemDetail.packingId = itemBarcodeData.ItemPackingId;
      itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
      itemDetail.brandId = itemBarcodeData.BrandId.toString();
      itemDetail.itemName = itemBarcodeData.ItemName ?? "";
      itemDetail.packingDesc = itemBarcodeData.PackingDesc;
      // _setItemName();
      setValues();

      OrderItemInfo.shared.orderItemList.add(itemDetail);
    });
  }

  _clearItemInfo() {
    itemDetail.itemBarcode = null;
    itemDetail.itemCode = null;
    itemDetail.itemId = null;
    // selectedPackageId = -1;
    itemDetail.pkgName = "";
    itemDetail.itemName = null;
    itemDetail.countryId = null;
    itemDetail.brandId = null;
    itemDetail = new OrderItem();
  }

  _configUI() {
    editMode = (itemDetail == null) ? EditMode.Off : EditMode.On;
  }

  @override
  Widget build(BuildContext context) {
    buildContext = context;

    bindItemData(ItemBarcodeData item) {
      itemBarcodeData = item;
      if (editMode == EditMode.Off) {
        _setItemInfo();
      }

      if (_itemInputType == InputType.ItemCode) {
        _loadItemPacking(context);
      } else {
        _setItemName();
        // itemDetail.packingId = itemBarcodeData.item_pck_id;
        // _loadCountry(context);
      }
    }
    int getDeci() {
        return int.parse(fromAddress.curDecimPlace);
    }

    _validateAndBindItemData(ItemBarcodeData item) {}

    return BlocProvider<OrderItemEntryBloc>(
      create: (context) =>
          OrderItemEntryBloc(bcRepository: repository, database: database),
      child: BlocListener<OrderItemEntryBloc, OrderItemEntryState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingInProgress ||
              state is LoadItemByPackingInProgress ||
              state is LoadCountryByIdInProgress ||
              state is LoadBrandByIdInProgress ||
              state is GetSalesStockDetailsInProgress) {
            Common().showLoader(context);
          } else if (state is LoadItemFailure ||
              state is LoadPackingFailure ||
              state is LoadItemByPackingFailure ||
              state is LoadCountryByIdFailure ||
              state is LoadBrandByIdFailure ||
              state is GetSalesStockDetailsFailure) {
            Navigator.pop(context);
          }
          else if (state is GetSalesStockDetailsComplete) {
            Navigator.pop(context);

            if (state.resp.masterDataList.isNotEmpty) {
                salesStockDetails = state.resp.masterDataList[0];
                stcDtItemsList = [];
                if(tapped) {
                  //.toDouble(pos: getDeci())
                  stcDtItemsList.add(StcDtItem(
                      name: "Currently Available Stock",
                      count: salesStockDetails.currentAvailableStock
                          .toString()));
                  stcDtItemsList.add(StcDtItem(
                      name: "Last Year Current Day Sales",
                      count: salesStockDetails.currentDaySaleQtyLY.toString()));
                  stcDtItemsList.add(StcDtItem(
                      name: "Last Day Wastage",
                      count: salesStockDetails.lastDayWastageQty.toString()));
                  stcDtItemsList.add(StcDtItem(
                      name: "Last Week Current Day Sales",
                      count: salesStockDetails.currentDaySaleQtyLW.toString()));
                  stcDtItemsList.add(StcDtItem(
                      name: "Stock Holding/Days",
                      count: salesStockDetails.stockHolding.toString()));
                  stcDtItemsList.add(StcDtItem(
                      name: "MTD Sales Average",
                      count: salesStockDetails.avgMTDSaleQty.toString()));

                  _showStockDetails(stcDtItemsList);
                }
                else{
                  setState(() {
                    if (tempOrderList.length > 0){
                      for (int i = 0; i <
                          state.resp.masterDataList.length; i++) {
                        tempOrderList.where((element) =>
                        element.itemId == state.resp.masterDataList[i].itemId)
                            .first.orderQty
                        = state.resp.masterDataList[i].estimatedOrderQty;
                      }
                    if (OrderItemInfo.shared.orderItemList.isNotEmpty) {
                      for (OrderItem x in tempOrderList) {
                        if (!OrderItemInfo.shared.orderItemList.any(
                                (element) => element.itemId == x.itemId))
                          OrderItemInfo.shared.orderItemList.add(x);
                      }
                    }
                    else {
                      OrderItemInfo.shared.orderItemList
                        ..addAll(tempOrderList);
                    }
                  }
                    else{
                      for (int i = 0; i < OrderItemInfo.shared.orderItemList.length; i++) {
                        OrderItemInfo.shared.orderItemList.where((element) =>
                        element.itemId == state.resp.masterDataList[i].itemId)
                            .first.orderQty
                        = state.resp.masterDataList[i].estimatedOrderQty;
                      }
                    }
                  });
                }
            }

          }
          else if (state is LoadItemComplete) {
            Navigator.pop(context);

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              /// Check whether already the item is added or not
              if (_itemInputType == InputType.Barcode &&
                  editMode == EditMode.Off &&
                  OrderItemInfo.shared.orderItemList
                      .where((item) =>
                          item.itemBarcode == state.loadItemResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                _didSelect(InputType.Barcode);
                return;
              }

              bindItemData(state.loadItemResult[0]);
              quantityFocusNode.requestFocus();
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
          }
          else if (state is LoadPackingComplete) {
            Navigator.pop(context);
            // Load packing list in dropdown
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                if (editMode == EditMode.Off) {
                  itemDetail.packingId =
                      int.parse(itemPackingDataList[0].packing_value);
                  itemDetail.pkgName = itemPackingDataList[0].packing_text;
                }
                // Set item name in barcode text field
                _setItemName();
                _findUniqueItemWithItemCodeAndPackingId(context);
                FocusScope.of(context).unfocus();
                // _loadCountry(context);
              });
            }
          }
          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {
              /// Check whether already the item is added or not
              if (OrderItemInfo.shared.orderItemList
                  .where((item) =>
                      item.itemBarcode == state.loadItemByPackingResult[0].Text)
                  .toList()
                  .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }

              setState(() {
                itemBarcodeData = state.loadItemByPackingResult[0];
                itemDetail.itemBarcode = itemBarcodeData.Text;
                itemDetail.packingDesc =
                    state.loadItemByPackingResult[0].PackingDesc;

                _setItemName();
              });
            }
          }
          else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.countryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }
            // _loadProductBrand(context);
          }
          else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.brandName = productBrandList[0].brand_name.trim();
              });
            }

            StockDetailReq stockDetailReq = new StockDetailReq();
            List<ItemDetails> item=[];
            ItemDetails itemRow=new ItemDetails();
            stockDetailReq.companyId=int.parse(fromAddress.fromCompanyId);
            stockDetailReq.divisionId =int.parse(fromAddress.fromDivisionId);
            stockDetailReq.locationId=int.parse(fromAddress.fromDeliveryLocationId);
            stockDetailReq.entryDateTemp =cmnOrderFormInfo.entryDateTemp;
            itemRow.itemId = itemDetail.itemId ?? 0;
            itemRow.packingId = itemDetail.packingId;
            item.add(itemRow);
            stockDetailReq.itemDetails =item;

            _orderItemEntryBloc.add(GetSalesStockDetails(stockDetailReq: stockDetailReq));
          }
          else if (state is SortedItemComplete) {
            Navigator.pop(context);

            // if (state.sortedList != null &&
            //     state.sortedList > 0) {
            //   setState(() {
            //     List<ProductBrandData> productBrandList = state.sortedList;
            //     itemDetail.brandName = productBrandList[0].brand_name.trim();
            //   });
            // }
          }
        },
        child: BlocBuilder<OrderItemEntryBloc, OrderItemEntryState>(
          builder: (blocContext, state) {
            _orderItemEntryBloc =
                BlocProvider.of<OrderItemEntryBloc>(blocContext);
            if (state is LoadItemInitial) {
              // if (editMode == EditMode.On) {
              //   _getItemDetails();
              // }

              if(FilterItemInfo.shared.filterItemList.isNotEmpty) {
                Future.delayed(const Duration(milliseconds: 300), () async {
                  tempfilterItemList.addAll(
                      FilterItemInfo.shared.filterItemList);
                  searchTempfilterItemList = tempfilterItemList;
                  FilterItemInfo.shared.filterItemList.clear();
                  Common().showLoader(context);
                  for (int i = 0; i < tempfilterItemList.length; i++) {
                    List<CountryOriginData> loadCountryResp;
                    List<ProductBrandData> loadBrandResp;
                    loadCountryResp =
                    await database.countryOriginDao.getCountryNameById(
                        tempfilterItemList[i].countryId);
                    loadBrandResp =
                    await database.productBrandDao.getBrandNameById(
                        tempfilterItemList[i].brandId);
                    tempfilterItemList[i].countryName =
                    loadCountryResp.length > 0
                        ? loadCountryResp[0].country_name
                        : "--";
                    tempfilterItemList[i].brandName = loadBrandResp.length > 0
                        ? loadBrandResp[0].brand_name
                        : "--";
                  }
                  Navigator.pop(context);

                  _openDialogPopup();
                });
              }
              else /*if(OrderItemInfo
                  .shared
                  .orderItemList.isEmpty)*/{
                _itemDetails();
              }
            }
            return Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: [
                  _getItemCodeFieldsNew(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Get Widget Methods

  _getItemCodeFields(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.Barcode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.ItemCode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Code", style: TextStyle(fontSize: 13.0))
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextFormField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _itemCodeController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: true,
                isValidated: _itemCodeController.text.trim().isNotEmpty,
                hintText: (_itemInputType == InputType.Barcode)
                    ? 'Item Barcode'
                    : 'Item Code',
                icon: _itemInputType == InputType.Barcode
                    ? Icons.blur_linear
                    : Icons.search,
                iconPress: () async {
                  if (_itemInputType == InputType.Barcode) {
                    var result = await BarcodeScanner.scan();
                    setState(() {
                      _itemCodeController.text = result;
                      _getItemDetails();
                    });
                  } else {
                    _getItemDetails();
                  }
                }),
            keyboardType: TextInputType.name,
            // onChanged: (value) {
            //   if (_itemInputType == InputType.Barcode) {
            //     setState(() {
            //       _getItemDetails(blocContext);
            //     });
            //   }
            // },

            validator: (value) {
              _getItemDetails();
              return null;
            },
            onFieldSubmitted: (value) {
              _getItemDetails();
            },
          ),
        )
      ],
    );
  }

  _getPackageField(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: true, isValidated: itemDetail.packingId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Container(),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.packingId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value));
                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.packingId = value;
                    itemDetail.pkgName = itemPackingDataList
                        .where((packing) =>
                            packing.packing_value ==
                            itemDetail.packingId.toString())
                        .toList()[0]
                        .packing_text;
                    // If input is item code, load item info based on selected packing
                    _findUniqueItemWithItemCodeAndPackingId(blocContext);
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getPackingDescrField() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child:
                  Text(itemDetail.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getQuantityFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
              enableSuggestions: false,
              textInputAction: TextInputAction.done,
              controller: _quantityController,
              focusNode: quantityFocusNode,
              autocorrect: false,
              decoration: Common().getBCoreMandatoryID(
                  isMandatory: true,
                  isValidated: itemDetail.orderQty != null,
                  hintText: 'Quantity'),
              keyboardType: TextInputType.number,
              onChanged: _updateQuantity),
        )
      ],
    );
  }

  _updateQuantity(String qty) {
    setState(() {
      itemDetail.orderQty = (int.tryParse(qty) == null)
          ? double.tryParse(qty)
          : int.parse(qty).toDouble();
    });
  }

  _getItemDetailsFields(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  decoration:
                      Common().getBCoreSD(isMandatory: true, isValidated: true),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text("Item details",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().headingTextColor)),
                        _getItemBasicInfoFields(),
                        _getTransGenerationType()
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: FlatButton(
                    padding: EdgeInsets.zero,
                    color: BCAppTheme().headingTextColor,
                    onPressed: () {
                      // Validate and save item details
                      if (_validateItemDetail()) {
                        saveItemDetails();
                      } else {
                        // Show alert message for required fields
                      }
                    },
                    child: Icon(
                      Icons.save,
                      size: 18,
                      color: Colors.white,
                    )),
              )
            ],
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }

  _getTransGenerationType() {
    return Column(
      children: [
        SizedBox(height: 8),
        Wrap(
          spacing: 0.0, // gap between adjacent chips
          runSpacing: -4.0,
          alignment: WrapAlignment.start,
          children: List<Widget>.generate(
            stcDtItemsList.length,
            (int index) {
              return Chip(
                padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                shape: StadiumBorder(
                    side: BorderSide(
                        width: 0.5, color: BCAppTheme().primaryColor)),
                backgroundColor: Colors.white,
                label: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: BCAppTheme().primaryColor, width: 0.5),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text('${stcDtItemsList[index].count}',
                            style: TextStyle(
                                fontSize: 10,
                                color: BCAppTheme().primaryColor,
                                fontWeight: FontWeight.w600)),
                      ),
                      // color: BCAppTheme().primaryColor,
                    ),
                    SizedBox(width: 8.0),
                    Text('${stcDtItemsList[index].name}',
                        style: TextStyle(fontSize: 12, color: Colors.black54)),
                  ],
                ),
              );
            },
          ).toList(),
        ),
      ],
    );
  }

  _getItemBasicInfoFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.scatter_plot, size: 10),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(
                        itemDetail.brandName != null
                            ? itemDetail.brandName
                            : "--",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 13.0),
                        overflow: TextOverflow.ellipsis,
                        softWrap: false),
                  ),
                ],
              )),
              SizedBox(width: 10),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.public, size: 12),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(
                      itemDetail.countryName != null
                          ? itemDetail.countryName
                          : "--",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 13.0),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ))
            ],
          ),
        ),
      ],
    );
  }

  // Pop ups

  bool _validateItemDetail() {
    if (_itemCodeController.text.isEmpty) {
      showMessage(
          context: context, message: "Please enter Item code/ Item Barcode");
      return false;
    }

    /// Check whether already the item is added or not
    if (editMode == EditMode.Off &&
        OrderItemInfo.shared.orderItemList
            .where((item) => item.itemBarcode == itemDetail.itemBarcode)
            .toList()
            .isNotEmpty) {
      showAlert(context: context, message: 'Item already added');
      return false;
    }

    if (itemDetail.packingId == null) {
      showMessage(context: context, message: "Please select item package");
      return false;
    }

    if (itemDetail.orderQty == null) {
      showMessage(context: context, message: "Please enter quantity");
      quantityFocusNode.requestFocus();
      return false;
    }
    return true;
  }

  void saveItemDetails() {
    /// Told to set quality check to true always
    // itemDetail.pkgName = itemDetail.pkgName ?? "";
    itemDetail.index = Utils.getUniqueId();
    // itemDetail.itemBarcode = itemBarcode;
    // itemDetail.itemName = itemName;
    // itemDetail.itemCode = itemCode;
    // itemDetail.itemId = int.tryParse(itemId);

    // itemDetail.packingId = selectedPackageId;

    if (editMode == EditMode.On) {
      /// Check whether already item is there
      var indexOfItemInExistingList = OrderItemInfo.shared.orderItemList
          .indexWhere((item) => item.itemBarcode == itemDetail.itemBarcode);

      if (indexOfItemInExistingList != -1) {
        OrderItemInfo.shared.orderItemList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [OrderItem.fromJson(itemDetail.toJson())]);
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Updated!'), duration: Duration(milliseconds: 500)));
        _didSelect(InputType.Barcode);
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Please try again'),
            duration: Duration(milliseconds: 500)));
      }
    } else {
      OrderItemInfo.shared.orderItemList
          .add(OrderItem.fromJson(itemDetail.toJson()));

      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Saved!'), duration: Duration(milliseconds: 500)));

      _didSelect(InputType.Barcode);
    }
//    itemCodeFocusNode.requestFocus();
  }

  void _setItemName() {
    _itemCodeController.text = itemDetail.itemName;
  }

  int getDeci() {
    return int.parse(fromAddress.curDecimPlace);
  }

  setValues() {
    /// when suppTypeId is 1, the supplier is 'local'. When suppTypeId is 2, the supplier is 'import'
    if (editMode == EditMode.Off) {
      _quantityController.text = "";
    }
  }

  /// Gets the item details

  _getItemDetails() {
    _loadItemFromDB();
  }

  _loadItemFromDB() {
    // If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {
      _orderItemEntryBloc
          .add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
    } else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      _orderItemEntryBloc
          .add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
    }
  }

  void _findUniqueItemWithItemCodeAndPackingId(BuildContext context) {
    if (_itemInputType == InputType.ItemCode) {
      if (itemDetail.itemId != null && itemDetail.packingId != null) {
        _orderItemEntryBloc.add(LoadItemByPacking(
            itemId: itemDetail.itemId.toString(),
            prodPkgId: itemDetail.packingId.toString()));
      }
    }
  }

  void _loadItemPacking(BuildContext context) {
    _orderItemEntryBloc
        .add(LoadPacking(itemId: itemBarcodeData.ItemId.toString()));
  }

  void _loadCountry(BuildContext context,String countryId ) {
    _orderItemEntryBloc.add(LoadCountryById(countryId: countryId));
  }

  void _loadProductBrand(BuildContext context, String brandId) {
    _orderItemEntryBloc.add(LoadBrandById(brandId: brandId));
  }

  ///design for orderform step 2 and dialog for showing list
  _getItemCodeFieldsNew() {
    return Expanded(
      child: OrderItemInfo
          .shared
          .orderItemList.length > 0 ? ListView.builder(
          itemCount: getCount(),
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: ListTile(

                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            selIndex = index;
                            tapped = true;
                            StockDetailReq stockDetailReq = new StockDetailReq();
                            List<ItemDetails> item=[];
                            ItemDetails itemRow=new ItemDetails();
                            stockDetailReq.companyId=int.parse(fromAddress.fromCompanyId);
                            stockDetailReq.divisionId =int.parse(fromAddress.fromDivisionId);
                            stockDetailReq.locationId=int.parse(fromAddress.fromLocationId);
                            stockDetailReq.entryDateTemp =cmnOrderFormInfo.entryDateTemp;
                            itemRow.itemId = OrderItemInfo.shared.orderItemList[index].itemId ?? 0;
                            itemRow.packingId = OrderItemInfo.shared.orderItemList[index].packingId;
                            item.add(itemRow);
                            stockDetailReq.itemDetails =item;

                            _orderItemEntryBloc.add(GetSalesStockDetails(stockDetailReq: stockDetailReq));

                          },
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: 15.0,
                                backgroundColor: BCAppTheme().primaryColor,
                                child: Text('${index +1}',
                                    style:
                                        TextStyle(color: Colors.white,fontSize: 10.0)),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("${"${OrderItemInfo.shared.orderItemList[index].itemCode}"
                                            " (""${OrderItemInfo
                                                .shared
                                                .orderItemList[index]
                                                .pkgName ?? ""}"")"}" ?? " ",
                                            style: TextStyle(fontSize: 10.0, color: BCAppTheme().subTextColor),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left),

                                        SizedBox(width: 2.0,),

                                      ],
                                    ),
                                    Text(
                                      OrderItemInfo.shared.orderItemList[index]
                                              .itemName ?? " ",
                                      style: TextStyle(
                                          fontSize: 11.0, color: Colors.grey),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                    Container(
                        margin: const EdgeInsets.only(right: 8.0),
                        // padding: const EdgeInsets.all(3.0),
                        width: 55,
                        height: 25,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blueGrey[100]),
                        ),
                        child: TextField(
                          style: TextStyle(fontSize: 12.0),
                          textAlign: TextAlign.center,
                          keyboardType:TextInputType.number ,
                          autofocus: true,
                          onChanged: (value){
                            OrderItemInfo.shared.orderItemList[index].orderQty = value.toDouble();
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: '${
                                OrderItemInfo
                                    .shared.orderItemList[index].orderQty ?? "0.0"
                              }',
                              hintStyle: TextStyle(fontSize: 10,color: Colors.black)),
                        )),
                    IconButton(

                      icon:Icon(Icons.delete_outlined),
                      color: Colors.red[300],
                      iconSize: 20.0,
                      onPressed: (){
                        OrderItemInfo
                            .shared
                            .orderItemList.removeAt(index);
                        setState(() {

                        });
                      },
                    ),
                  ],
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        OrderItemInfo
                            .shared
                            .orderItemList[index]
                            .countryName,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 8)),
                    SizedBox(width: 2.0,),
                    Text(
                        OrderItemInfo
                            .shared
                            .orderItemList[index]
                            .brandName,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 8)),
                  ],
                ),
              )
            );
          })
          : Center(
        child: Text("No Items Selected"),
      ),
    );
  }



  String getId() {
    String itemBarcodeid =
        OrderItemInfo.shared.orderItemList[0].itemBarcode ?? " ";
    return itemBarcodeid;
  }

  getCount() {
    return OrderItemInfo.shared.orderItemList.length;
  }

  String getText() {
    String itemBarcodename =
        OrderItemInfo.shared.orderItemList[0].itemName ?? " ";
    return itemBarcodename;
  }
  void _itemSelected(setState) {
    setState(() {
      tapped =false;
      StockDetailReq stockDetailReq = new StockDetailReq();
      List<ItemDetails> item=[];
      stockDetailReq.companyId=int.parse(fromAddress.fromCompanyId);
      stockDetailReq.divisionId =int.parse(fromAddress.fromDivisionId);
      stockDetailReq.locationId=int.parse(fromAddress.fromLocationId);
      stockDetailReq.entryDateTemp =cmnOrderFormInfo.entryDateTemp;
      for(int i =0; i < tempOrderList.length;i++){
        ItemDetails itemRow=new ItemDetails();
        itemRow.itemId = tempOrderList[i].itemId ?? 0;
        itemRow.packingId = tempOrderList[i]
            .packingId;
        item.add(itemRow);
      }

      stockDetailReq.itemDetails =item;

      _orderItemEntryBloc.add(GetSalesStockDetails(stockDetailReq: stockDetailReq));
    });
  }
  void _itemDetails() {
      StockDetailReq stockDetailReq = new StockDetailReq();
      List<ItemDetails> item=[];
      stockDetailReq.companyId=int.parse(fromAddress.fromCompanyId);
      stockDetailReq.divisionId =int.parse(fromAddress.fromDivisionId);
      stockDetailReq.locationId=int.parse(fromAddress.fromLocationId);
      stockDetailReq.entryDateTemp =cmnOrderFormInfo.entryDateTemp;
      for(int i =0; i < OrderItemInfo
          .shared
          .orderItemList.length;i++){
        if(OrderItemInfo
            .shared
            .orderItemList[i].orderQty == null) {
          ItemDetails itemRow = new ItemDetails();
          itemRow.itemId = OrderItemInfo
              .shared
              .orderItemList[i].itemId ?? 0;
          itemRow.packingId = OrderItemInfo
              .shared
              .orderItemList[i]
              .packingId;
          item.add(itemRow);
        }
      }

      stockDetailReq.itemDetails =item;
        if(stockDetailReq.itemDetails.length > 0)
      _orderItemEntryBloc.add(GetSalesStockDetails(stockDetailReq: stockDetailReq));

  }

    _openDialogPopup() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, StateSetter setState) {
              return Dialog(
              insetPadding: EdgeInsets.all(5.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return Column(crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisSize: MainAxisSize.min,
                        children: [
                          Text("Item Details",
                              //textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: BCAppTheme().primaryColor,
                                  fontSize: 15)),
                          SizedBox(height: 16.0),
                          Padding(
                            padding: const EdgeInsets.only(
                                bottom: 10, left: 2, right: 2),
                            child: Container(
                              //width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(10)),
                              child: TextField(
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                  if (_searchController.text.isNotEmpty &&
                                      tempfilterItemList.length != 0) {
                                    searchTempfilterItemList= tempfilterItemList
                                        .where((element) => element.itemCode
                                        .toLowerCase()
                                        .contains(value.toLowerCase()))
                                        .toList();
                                  } else{
                                    searchTempfilterItemList = tempfilterItemList;
                                  }

                                  });
                                },
                                controller: _searchController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  prefixIcon:
                                  new Icon(Icons.search, color: Colors.black),
                                  hintText: 'Search Item Code here',
                                  //contentPadding: EdgeInsets.all(8)
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              //physics: NeverScrollableScrollPhysics(),
                              // shrinkWrap: true,
                              itemCount:searchTempfilterItemList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Card(
                                  elevation: 3,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(children: [
                                      CircleAvatar(
                                        backgroundColor: Colors.amber[200],
                                        child: Text('${index + 1}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: BCAppTheme().primaryColor,
                                                fontSize: 15)),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(
                                                    searchTempfilterItemList[index]
                                                        .itemCode,
                                                    style: TextStyle(
                                                      fontSize: 10,
                                                      fontWeight: FontWeight.bold,
                                                    )),
                                                SizedBox(width: 2.0,),
                                                Text(
                                                    searchTempfilterItemList[index]
                                                        .pkgName,
                                                    style: TextStyle(
                                                      fontSize: 10,
                                                    )),
                                              ],
                                            ),
                                            Text(
                                                searchTempfilterItemList[index]
                                                    .itemName,
                                                style: TextStyle(fontSize: 10)),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(
                                                    searchTempfilterItemList[index]
                                                        .countryName,
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(fontSize: 8)),
                                                SizedBox(width: 2.0,),
                                                Text(
                                                    searchTempfilterItemList[index]
                                                        .brandName,
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(fontSize: 8)),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Switch(
                                        value: searchTempfilterItemList[index].isSelected,
                                        onChanged: (value) {
                                          setState(() {

                                            searchTempfilterItemList[index].isSelected = value;

                                          });
                                        },
                                        activeColor: BCAppTheme().headingTextColor,
                                      ),
                                    ]),
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(height: 8),
                          Center(
                            child: ElevatedButton(
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                )),
                                padding: MaterialStateProperty.all<EdgeInsets>(
                                    EdgeInsets.only(left: 50, right: 50)),
                              ),
                              onPressed: () {
                                searchTempfilterItemList = tempfilterItemList;
                                for(int i =0; i < searchTempfilterItemList.length; i++){

                                  if(searchTempfilterItemList[i].isSelected == true){
                                    itemDetail = new OrderItem();
                                    itemDetail.itemId = searchTempfilterItemList[i].itemId;
                                    itemDetail.packingId = searchTempfilterItemList[i].packingId;
                                    itemDetail.inputType = searchTempfilterItemList[i].inputType;
                                    itemDetail.itemBarcode = searchTempfilterItemList[i].itemBarcode;
                                    itemDetail.itemCode = searchTempfilterItemList[i].itemCode;
                                    itemDetail.itemId = searchTempfilterItemList[i].itemId;
                                    itemDetail.packingId = searchTempfilterItemList[i].packingId;
                                    itemDetail.itemName = searchTempfilterItemList[i].itemName ?? "";
                                    itemDetail.countryName = searchTempfilterItemList[i].countryName;
                                    itemDetail.brandName = searchTempfilterItemList[i].brandName;
                                    itemDetail.pkgName = searchTempfilterItemList[i].pkgName;
                                    tempOrderList.add(itemDetail);
                                  }
                                }
                                if(tempOrderList.length > 0) {
                                  tempfilterItemList.clear();
                                  searchTempfilterItemList.clear();
                                  FilterItemInfo.shared.filterItemList.clear();
                                  _itemSelected(setState);
                                  Navigator.pop(context);
                                }
                                else {
                                  showAlert(context: context, message: "No Items Selected");
                                }
                              },
                              child: const Text('ADD'),


                            ),
                          ),
                        ]);
                  }),
                ));
        } );
        });
  }

  _showStockDetails(  List<StcDtItem> stcDtItemsList){
    Dialog alert = Dialog(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Column(
            crossAxisAlignment:
            CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(Icons.close_rounded,
                          color: Colors.red),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20),
                    child: Text(OrderItemInfo.shared.orderItemList[selIndex].itemName,
                      style: TextStyle(
                          color: Colors.amber,
                          fontSize: 17.0),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              Padding(
                padding:
                const EdgeInsets.only(left: 20.0),
                child: Text('${cmnOrderFormInfo.entryDateTemp}'??"",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 10.0),
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 10.0,
                left: 20.0,
                bottom: 20,
                right: 20),
            child: Column(
              mainAxisAlignment:
              MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.home,
                                size: 16.0,
                                color: Colors.grey,
                              ),
                              Text('Brand',
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: 10.5,
                                      color: Colors
                                          .black),
                                  overflow:
                                  TextOverflow
                                      .ellipsis),
                            ],
                          ),
                          Text('${OrderItemInfo.shared.orderItemList[selIndex].brandName} '?? "",
                            maxLines: 2,
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 11.0),
                            overflow:
                            TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: 20,
                    // ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.home,
                                size: 16.0,
                                color: Colors.grey,
                              ),
                              Text(
                                'ORIGIN',
                                maxLines: 2,
                                style: TextStyle(
                                    fontSize: 10.5,
                                    color:
                                    Colors.black),
                              ),
                            ],
                          ),
                          Text('${OrderItemInfo.shared.orderItemList[selIndex].countryName} '?? "",
                            maxLines: 2,
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 11.0),
                            overflow:
                            TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Divider(
                    height: 0.5,
                  ),
                ),
                Row(
                  // mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment:
                  //     MainAxisAlignment
                  //         .spaceBetween,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[0].name,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 10.5,
                                  color:
                                  Colors.black),
                              overflow: TextOverflow
                                  .ellipsis),
                          Text(stcDtItemsList[0].count.toDouble().toString(),
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 14.0,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: 20,
                    // ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[1].name,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 10.5,
                                  color:
                                  Colors.black),
                              overflow: TextOverflow
                                  .ellipsis),
                          Text(stcDtItemsList[1].count.toDouble().toString(),
                              style: TextStyle(
                                  color: Colors.amber,
                                  fontSize: 14.0,
                                  fontWeight:
                                  FontWeight
                                      .bold),
                              overflow: TextOverflow
                                  .ellipsis),
                        ],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Divider(
                    height: 0.5,
                  ),
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[2].name,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 10.5,
                                  color:
                                  Colors.black),
                              overflow: TextOverflow
                                  .ellipsis),
                          Text(stcDtItemsList[2].count.toDouble().toString(),
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 14.0,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: 20,
                    // ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[3].name,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 10.5,
                                  color:
                                  Colors.black),
                              overflow: TextOverflow
                                  .ellipsis),
                          Text(stcDtItemsList[3].count.toDouble().toString(),
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 14.0,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Divider(
                    height: 0.5,
                  ),
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  crossAxisAlignment:
                  CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[4].name,
                            maxLines: 2,
                            style: TextStyle(
                                fontSize: 10.5,
                                color: Colors.black),
                            overflow:
                            TextOverflow.ellipsis,
                          ),
                          Text(stcDtItemsList[4].count.toDouble().toString(),
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 14.0,
                                fontWeight:
                                FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: 20,
                    // ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(stcDtItemsList[5].name,
                            maxLines: 2,
                            style: TextStyle(
                                fontSize: 10.5,
                                color: Colors.black),
                          ),
                          Text(stcDtItemsList[5].count.toDouble().toString(),
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 14.0,
                                fontWeight:
                                FontWeight.bold),
                          ),
                          // Text(stcDtItemsList[5].count,
                          //   style: TextStyle(
                          //       color: Colors.amber,
                          //       fontSize: 14.0,
                          //       fontWeight:
                          //       FontWeight.bold),
                          // ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class StcDtItem {
  String name;
  String count;

  StcDtItem({this.name, this.count});
}
