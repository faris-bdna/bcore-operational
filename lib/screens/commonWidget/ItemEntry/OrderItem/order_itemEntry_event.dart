part of 'order_itemEntry_bloc.dart';

abstract class OrderItemEntryEvent extends Equatable {
  const OrderItemEntryEvent();
}

class LoadItem extends OrderItemEntryEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

class LoadPacking extends OrderItemEntryEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadItemByPacking extends OrderItemEntryEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

class LoadCountryById extends OrderItemEntryEvent {
  final String countryId;

  const LoadCountryById({this.countryId});

  @override
  List<Object> get props => [countryId];

  @override
  String toString() => 'LoadCountryById {"CountryId" : "$countryId"}';
}

class LoadBrandById extends OrderItemEntryEvent {
  final String brandId;

  const LoadBrandById({this.brandId});

  @override
  List<Object> get props => [brandId];

  @override
  String toString() => 'LoadBrandById {"BrandId" : "$brandId"}';
}

class GetSalesStockDetails extends OrderItemEntryEvent {
  final StockDetailReq stockDetailReq;

  const GetSalesStockDetails(
      {this.stockDetailReq});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetSalesStockDetails {"StockDetailReq" : "$stockDetailReq"}';
}

/// Event to handle sorted item
class SortedItem extends OrderItemEntryEvent {
  final String productGroup;
  final String brand;
  final String origin;

  SortedItem(this.productGroup, this.brand, this.origin);

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'SortedItem {"ProductGroup" : "$productGroup","Brand" : "$brand", "Origin" : "$origin"}';
}

