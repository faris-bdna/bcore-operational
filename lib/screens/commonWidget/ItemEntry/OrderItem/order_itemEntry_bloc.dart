import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/request/stockDetailReq.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/GetSalesStockDetailsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'order_itemEntry_event.dart';

part 'order_itemEntry_state.dart';

class OrderItemEntryBloc
    extends Bloc<OrderItemEntryEvent, OrderItemEntryState> {
  final BCRepository bcRepository;
  final Database database;

  OrderItemEntryBloc({@required this.bcRepository, @required this.database})
      : assert(bcRepository != null),
        super(LoadItemInitial());

  @override
  Stream<OrderItemEntryState> mapEventToState(
      OrderItemEntryEvent event) async* {
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;
    List<CountryOriginData> loadCountryResp;
    List<ProductBrandData> loadBrandResp;
     GetCommonResp  sortedResp;

    if (event is LoadItem) {
      yield LoadItemInProgress();

      try {
        if (event.barcode.isNotEmpty) {
          loadItemResp =
              await database.itemBarcodeDao.getItemByBarcode(event.barcode);
        } else {
          loadItemResp =
              await database.itemBarcodeDao.getItemByItemCode(event.itemCode);
        }
        yield LoadItemComplete(loadItemResult: loadItemResp);
      } catch (error) {
        yield LoadItemFailure(error: error.toString());
      }
    }

    else if (event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp =
            await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if (event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao
              .getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadCountryById) {
      yield LoadCountryByIdInProgress();

      try {
        loadCountryResp =
            await database.countryOriginDao.getCountryNameById(event.countryId);
        yield LoadCountryByIdComplete(loadCountryResult: loadCountryResp);
      } catch (error) {
        yield LoadCountryByIdFailure(error: error.toString());
      }
    }

    else if (event is LoadBrandById) {
      yield LoadBrandByIdInProgress();

      try {
        loadBrandResp =
            await database.productBrandDao.getBrandNameById(event.brandId);
        yield LoadBrandByIdComplete(loadBrandResult: loadBrandResp);
      } catch (error) {
        yield LoadBrandByIdFailure(error: error.toString());
      }
    }

    else if (event is GetSalesStockDetails) {
      yield GetSalesStockDetailsInProgress();

      try {
        final GetSalesStockDetailsResp resp =
            await bcRepository.getSalesStockDetails(
              stockDetailReq: event.stockDetailReq
            );

        yield GetSalesStockDetailsComplete(resp: resp);
      } catch (error) {
        yield GetSalesStockDetailsFailure(error: error.toString());
      }
    }

    else if (event is SortedItem) {
      yield SortedItemInProgress();

      try {
         sortedResp = await bcRepository.getSortedItem(
            brand: event.brand, origin: event.origin);
        yield SortedItemComplete(sortedList: sortedResp);
      } catch (error) {
        yield SortedItemFailure(error: error.toString());
      }
    }
  }
}
