part of 'dnItemEntry_bloc.dart';

abstract class DnItemEntryState extends Equatable {
  const DnItemEntryState();

  @override
  List<Object> get props => [];
}


/// Get SalesOrderDetails States

class GetSalesOrderDetailsInitial extends DnItemEntryState{}

class GetSalesOrderDetailsInProgress extends DnItemEntryState{}

class GetSalesOrderDetailsComplete extends DnItemEntryState{
  final GetSODetailsResp resp;

  GetSalesOrderDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetSalesOrderDetailsFailure extends DnItemEntryState {
  final String error;

  const GetSalesOrderDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSalesOrderDetails { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends DnItemEntryState{}

class LoadPackingInProgress extends DnItemEntryState{}

class LoadPackingComplete extends DnItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends DnItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}



/// Load packing list

class LoadItemByPackingInitial extends DnItemEntryState{}

class LoadItemByPackingInProgress extends DnItemEntryState{}

class LoadItemByPackingComplete extends DnItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends DnItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}

