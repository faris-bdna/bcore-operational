part of 'dnItemEntry_bloc.dart';

abstract class DnItemEntryEvent extends Equatable {
  const DnItemEntryEvent();
}

/// Used for getting the sales order details
class GetSalesOrderDetails extends DnItemEntryEvent {
  final int salesOrderId;

  const GetSalesOrderDetails({this.salesOrderId});

  @override
  List<Object> get props => [salesOrderId];

  @override
  String toString() => 'GetSalesOrderDetails {"SalesOrderId" : "$salesOrderId"}';
}

class LoadPacking extends DnItemEntryEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadItemByPacking extends DnItemEntryEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}
