import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'dnItemEntry_event.dart';

part 'dnItemEntry_state.dart';

class DnItemEntryBloc extends Bloc<DnItemEntryEvent, DnItemEntryState> {
  final BCRepository bcRepository;
  final Database database;
  final SOReq soReq;

  DnItemEntryBloc(
      {@required this.bcRepository,
        @required this.database,
        @required this.soReq})
      : assert(bcRepository != null),
        super(GetSalesOrderDetailsInitial());

  @override
  Stream<DnItemEntryState> mapEventToState(DnItemEntryEvent event) async* {
    // TransactionResp transactionResp;
    //
    // int draftTransactionResp;
    // bool updateDraftTransactionResp;
    // List<DraftTransactionData> getDraftTranId;
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;
    // List<CountryOriginData> loadCountryResp;
    // List<ProductBrandData> loadBrandResp;

    if (event is GetSalesOrderDetails) {
      yield GetSalesOrderDetailsInProgress();

      try {
        final GetSODetailsResp resp = await bcRepository.getSalesOrderDetails(event.salesOrderId);
        yield GetSalesOrderDetailsComplete(resp: resp);
      } catch (error) {
        yield GetSalesOrderDetailsFailure(error: error.toString());
      }
    }

    else if (event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp =
        await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if (event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao
              .getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }
  }
}
