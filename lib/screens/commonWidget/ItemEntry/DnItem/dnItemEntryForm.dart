import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnTandCInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dnItemEntry_bloc.dart';

class DnItemEntryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final CommonSalesItem item;

  const DnItemEntryForm(
      {Key key,
        this.repository,
        this.appLanguage,
        this.database,
        this.item})
      : super(key: key);

  @override
  _DnItemEntryFormState createState() =>
      _DnItemEntryFormState(repository, appLanguage, database, item);
}

class _DnItemEntryFormState extends State<DnItemEntryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  _DnItemEntryFormState(
      this.repository, this.appLanguage, this.database, this.itemDetail);

  CommonSalesItem itemDetail;
  EditMode editMode;

  List<ItemPackingData> itemPackingDataList;
  InputType _itemInputType = InputType.Barcode;

  var _itemCodeController = TextEditingController();
  var _quantityController = TextEditingController();
  var _searchController = TextEditingController();

  var quantityFocusNode = FocusNode();

  String _splitBarcode;
  bool _isWeightedBC = false;

  DnItemEntryBloc dnItemBloc;
  BuildContext dnItemEntryBlocContext;

  SODetailsList salesOrderDetails;

  @override
  void initState() {
    super.initState();

    _configUI();
    loadItemDetailsIfInEditMode();
  }

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {
      setState(() {

        _itemInputType = itemDetail.inputType;
        _itemCodeController.text = (_itemInputType == InputType.Barcode) ? itemDetail.itemBarcode.toString() : itemDetail.itemCode;
        _quantityController.text = itemDetail.quantity.toString();
      });
    } else {
      itemDetail =
          CommonSalesItem(); // TODO Confirm this doesn't make any issue in the flow
    }
  }

  @override
  void dispose() {
    super.dispose();

    editMode = EditMode.Off;
    itemDetail = null;

    _itemCodeController.dispose();
    _quantityController.dispose();
    _searchController.dispose();

  }

  _didSelect(InputType type) {
    setState(() {
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';
      _searchController.text = '';

      _clearItemInfo();

      itemPackingDataList = null;
    });
  }

  void _setItemInfo(ItemBarcodeData itemBarcodeData) {
    setState(() {
      if (editMode == EditMode.Off) {
        itemDetail.itemBarcode = int.tryParse(itemBarcodeData.Text) ?? 0;
        itemDetail.itemCode = itemBarcodeData.ItemCode;
        itemDetail.itemId = itemBarcodeData.ItemId;
        itemDetail.unitId = itemBarcodeData.ItemPackingId;
        itemDetail.itemName = itemBarcodeData.ItemName;
        itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
        itemDetail.brandId = itemBarcodeData.BrandId.toString();
        itemDetail.averageCost = itemBarcodeData.AverageCost;
        itemDetail.netCost = itemBarcodeData.NetCost.toDouble().toString();
        itemDetail.minShelfLife = itemBarcodeData.MinShelfLife;
        itemDetail.productStockTypeId = itemBarcodeData.ProductStockTypeId;
      } else {
      }
      // setValues(itemBarcodeData);
    });
  }

  _clearItemInfo() {
    itemDetail =  CommonSalesItem();
  }

  _configUI() {
    if (Constants.isTransEditMode) {
      editMode = (itemDetail == null || itemDetail.itemBarcode == null)
          ? EditMode.Off
          : EditMode.On;
    }else{
      editMode = EditMode.Off;
    }
  }

  // bindItemData(ItemBarcodeData itemBarcodeData) {
  //
  //   _setItemInfo(itemBarcodeData);
  //
  //   if (_itemInputType == InputType.ItemCode) {
  //     _loadItemPacking();
  //   } else {
  //     if (editMode == EditMode.Off) {
  //     }
  //     _setItemName();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DnItemEntryBloc>(
      create: (context) => DnItemEntryBloc(
          bcRepository: repository, database: database, soReq: SOReq.shared),
      child: BlocListener<DnItemEntryBloc, DnItemEntryState>(
        listener: (context, state) {
          if (
              state is LoadPackingInProgress ||
              state is GetSalesOrderDetailsInProgress ||
              state is LoadItemByPackingInProgress) {
            Common().showLoader(context);
          }
          else if (
              state is LoadPackingFailure ||
              state is GetSalesOrderDetailsFailure ||
              state is LoadItemByPackingFailure) {
            Navigator.pop(context);
          }
          else if (state is LoadPackingComplete) {
            Navigator.pop(context);
            // Load packing list in dropdown
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                FocusScope.of(context).unfocus();
                _findUniqueItemWithItemCodeAndPackingId();
              });
            }
            quantityFocusNode.requestFocus();
          }
          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);

            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {

              /// Check whether already the item is added or not
              if ( CommonSalesItemInfo.shared.cmnSalesItemList
                  .where((item) =>
              item.itemBarcode.toString() == state.loadItemByPackingResult[0].Text)
                  .toList()
                  .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }

              setState(() {
                itemDetail.itemBarcode =
                    int.tryParse(state.loadItemByPackingResult[0].Text) ?? 0;
                if (itemDetail.totalBaseQty == null){
                  itemDetail.totalBaseQty = (state.loadItemByPackingResult[0].PkgQty.toDouble() ?? 1.0) * itemDetail.totalReqQty;
                } else{
                  itemDetail.totalReqQty = itemDetail.totalBaseQty / (state.loadItemByPackingResult[0].PkgQty.toDouble() ?? 1.0);
                }
                itemDetail.quantity = itemDetail.totalReqQty;
                itemDetail.packingDesc = state.loadItemByPackingResult[0].PackingDesc;

                _quantityController.text = itemDetail.totalReqQty.toString();
              });
            }
          }
          else if (state is GetSalesOrderDetailsComplete) {
            Navigator.pop(context);

            if (state.resp.masterDataList.isNotEmpty) {

              salesOrderDetails = state.resp.masterDataList[0];
              print(salesOrderDetails.toJson());
              if (CommonAddChargesInfo.shared.orderId !=
                  salesOrderDetails.orderId) {
                CommonAddChargesInfo.shared.cmnAddChargesList = [];

                CommonAddChargesInfo.shared.orderId = salesOrderDetails.orderId;
                for (AdditionalChargesDetailsList charge in salesOrderDetails
                    .additionalChargesDetailsList) {
                  CommonAddCharges cmnCharge = CommonAddCharges.fromSO(
                      charge.toJson());
                  cmnCharge.editable = false;
                  CommonAddChargesInfo.shared.cmnAddChargesList.add(cmnCharge);
                }
              }

              if (CommonTandCInfo.shared.orderId != salesOrderDetails.orderId) {
                if (CommonTandCInfo.shared.orderId !=
                    salesOrderDetails.orderId) {
                  CommonTandCInfo.shared.termsAndConditionList = [];

                  CommonTandCInfo.shared.orderId = salesOrderDetails.orderId;
                  for (TermsAndCondionList term in salesOrderDetails
                      .termsAndCondionList) {
                    TermsAndCondions cmnTerm = TermsAndCondions.fromSO(
                        term.toJson());
                    cmnTerm.editable = false;
                    CommonTandCInfo.shared.termsAndConditionList
                        .add(cmnTerm);
                  }
                }
              }
            } else{
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }
        },
        child: BlocBuilder<DnItemEntryBloc, DnItemEntryState>(
            builder: (blocContext, state) {
              dnItemBloc = BlocProvider.of<DnItemEntryBloc>(blocContext);
              dnItemEntryBlocContext = blocContext;

              if (state is GetSalesOrderDetailsInitial) {
                  _loadSalesOrderDetails();
              }
              return Column(
                children: [
                  _getAccountTypes(),
                  SizedBox(height: 10),
                  _getItemCodeFields(),
                  if (_itemInputType == InputType.ItemCode) _getPackage(),
                  if (itemDetail.packingDesc != null) _getPackingDescrField(),
                  _getAdvancedQuantityFields(),
                  _getQuantityField(),
                  _getItemDetailsField()
                ],
              );
            }),
      ),
    );
  }

  _getAccountTypes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 20,
                height: 20,
                child: Radio(
                    activeColor: BCAppTheme().headingTextColor,
                    value: InputType.Barcode,
                    groupValue: _itemInputType,
                    onChanged: _didSelect),
              ),
              SizedBox(width: 8),
              Text("Item Barcode", style: TextStyle(fontSize: 13.0))
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 20,
                height: 20,
                child: Radio(
                    activeColor: BCAppTheme().headingTextColor,
                    value: InputType.ItemCode,
                    groupValue: _itemInputType,
                    onChanged: _didSelect),
              ),
              SizedBox(width: 8),
              Text("Item Code", style: TextStyle(fontSize: 13.0))
            ],
          ),
        ),
      ],
    );
  }
  // _getItemCodeFields() {
  //
  //   return  editMode == EditMode.Off ? Column(
  //     children: [
  //       Row(
  //         mainAxisAlignment: MainAxisAlignment.start,
  //         children: [
  //           Expanded(
  //             child: SizedBox(
  //               height: 55,
  //               child: TextFormField(
  //                 enableSuggestions: false,
  //                 textInputAction: TextInputAction.next,
  //                 controller: _itemCodeController,
  //                 autocorrect: false,
  //                 decoration: Common().getInputDecoration(
  //                     _itemCodeController.text.trim().isEmpty,
  //                     (_itemInputType == InputType.Barcode)
  //                         ? 'Item Barcode'
  //                         : 'Item Code',
  //                     icon: _itemInputType == InputType.Barcode ? Icons.blur_linear : Icons.search,
  //                     iconPress: ()async{
  //                       if (_itemInputType == InputType.Barcode) {
  //                         var result = await BarcodeScanner.scan();
  //                         setState(() {
  //                           _itemCodeController.text = result.rawContent;
  //                           _getItemDetailsFromRef(context);
  //                         });
  //
  //                       }else{
  //                         _getItemDetailsFromRef(context);
  //                       }
  //                     }
  //                 ),
  //                 keyboardType: TextInputType.name,
  //                 // onChanged: (value) {
  //                 //   if (_itemInputType == InputType.Barcode) {
  //                 //     _getItemDetailsFromRef(context);
  //                 //   }
  //                 //
  //                 // },
  //                 validator: (value){
  //                   _getItemDetailsFromRef(context);
  //
  //                   return null;
  //                 },
  //                 onFieldSubmitted: (value) {
  //                   _getItemDetailsFromRef(context);
  //                 },
  //               ),
  //             ),
  //           ),
  //           SizedBox(width: 10),
  //           Container(
  //             decoration: Common().getBCoreSD(
  //                 isMandatory: true, isValidated: true),
  //             width: 55,
  //             height: 55,
  //             child: IconButton(icon: Icon(Icons.list_alt, color: BCAppTheme().primaryColor,),
  //                 iconSize: 20,
  //                 onPressed: (){
  //                   showRefItemsList();
  //                 }),
  //           )
  //         ],
  //       ),
  //     ],
  //   ) :Column(
  //     children: [
  //       ElevatedButton(onPressed: (){
  //         editMode = EditMode.Off;
  //         _didSelect(InputType.Barcode);
  //       }, child: Text("Clear & Add New"))
  //     ],
  //   );
  // }
  _getItemCodeFields() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: SizedBox(
            height: 55,
            child: TextFormField(
              enableSuggestions: false,
              textInputAction: TextInputAction.next,
              controller: _itemCodeController,
              autocorrect: false,
              decoration: Common().getInputDecoration(
                  _itemCodeController.text.trim().isEmpty,
                  (_itemInputType == InputType.Barcode)
                      ? 'Item Barcode'
                      : 'Item Code',
                  icon: _itemInputType == InputType.Barcode ? Icons.blur_linear : Icons.search,
                iconPress: ()async{
                  if (_itemInputType == InputType.Barcode) {
                    var result = await BarcodeScanner.scan();
                    setState(() {
                      _itemCodeController.text = result;
                      _getItemDetailsFromRef(context);
                    });

                  }else{
                    _getItemDetailsFromRef(context);
                  }
                }
              ),
              keyboardType: TextInputType.name,
              // onChanged: (value) {
              //   if (_itemInputType == InputType.Barcode) {
              //     _getItemDetailsFromRef(context);
              //   }
              //
              // },
              validator: (value){
                _getItemDetailsFromRef(context);

                return null;
              },
              onFieldSubmitted: (value) {
                  _getItemDetailsFromRef(context);
              },
            ),
          ),
        ),
        SizedBox(width: 10),
        Container(
          decoration: Common().getBCoreSD(
              isMandatory: true, isValidated: true),
          width: 55,
          height: 55,
          child: IconButton(icon: Icon(Icons.list_alt, color: BCAppTheme().primaryColor,),
              iconSize: 20,
              onPressed: (){
                showRefItemsList();
              }),
        )
      ],
    );
  }


  showRefItemsList() {
    Dialog itemListDialogue = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter dropDownState) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Items List", textAlign: TextAlign.center),
                      SizedBox(height: 16.0),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 45,
                          child: TextField(
                            onChanged: (value) {
                              dropDownState(() {});
                            },
                            controller: _searchController,
                            decoration: Common().getBCoreMandatoryID(
                                isMandatory: false,
                                isValidated: true,
                                hintText: 'Search',
                                icon: Icons.search),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: salesOrderDetails.materialDetailsList.length,
                            itemBuilder: (context, index) {
                              if (_searchController.text.isEmpty) {
                                return RefItemRow(
                                  hasAdded: CommonSalesItemInfo.shared.cmnSalesItemList.where((item) => item.orderDetId == salesOrderDetails.materialDetailsList[index].orderDetId).toList().isNotEmpty,
                                  item: salesOrderDetails.materialDetailsList[index],
                                );}
                              else if (salesOrderDetails.materialDetailsList[index]
                                  .itemName
                                  .toLowerCase()
                                  .contains(_searchController.text.toLowerCase())) {
                                return RefItemRow(
                                  hasAdded: CommonSalesItemInfo.shared.cmnSalesItemList.where((item) => item.orderDetId == salesOrderDetails.materialDetailsList[index].orderDetId).toList().isNotEmpty,
                                  item: salesOrderDetails.materialDetailsList[index],
                                );
                              }
                              else {
                                return Container();
                              }
                            }),
                      ),
                      SizedBox(height: 8),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Ok",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);

                                    setState(() {

                                    });
                                  }),
                            ),
                          )
                        ],
                      )
                    ],
                  );
                })),
      ),
    );

    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext myContext) {
        return itemListDialogue;
      },
    );
  }

  _getPackage() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: (_itemInputType == InputType.ItemCode),
                isValidated: itemDetail.unitId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Container(),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.unitId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                  return DropdownMenuItem(
                      child: Text(item.packing_text,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: BCAppTheme().subTextColor,
                          )),
                      value: int.parse(item.packing_value));
                }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.unitId = value;
                    itemDetail.pkgName = itemPackingDataList
                        .where((packing) => packing.packing_value == value.toString())
                        .toList()[0]
                        .packing_text;
                    _findUniqueItemWithItemCodeAndPackingId();
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getPackingDescrField(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Text(itemDetail.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getAdvancedQuantityFields() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.only(left: 4.0, right: 4.0),
          child: Row(children: [
            Expanded(child: Text("Balance Qty : ${(itemDetail.totalReqQty ?? 0.0) - _getQuantity()}"))
          ]),
        ),
      ],
    );
  }

  _getQuantity(){
    if (_quantityController.text.trim().isNotEmpty){
      return (_quantityController.text.trim().toDouble() ?? 0.0);
    }else{
      return 0.0;
    }
  }
  
  _getQuantityField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreID(
              validityStatus: _quantityController.text.trim().isNotEmpty,
              hintText: 'Quantity',
            ),
            onChanged: (value) {
              setState(() {
                if (value != ''){
                  itemDetail.quantity = value.toDouble();
                }
                else{
                  itemDetail.quantity = 1.0;
                }
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getItemDetailsField() {
    return Column(
      children: [
        SizedBox(height: 10),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  decoration: Common().getBCoreSD(isMandatory: false),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text("Item details of ${itemDetail.itemCode?? "-"}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().headingTextColor)),
                        _getItemBasicInfo(),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: FlatButton(
                    padding: EdgeInsets.zero,
                    color: BCAppTheme().headingTextColor,
                    onPressed: () {
                      print(itemDetail.toJson());
                      // Validate and save item details
                      if (_validateItemDetail()) {
                        saveItemDetails();
                      }
                    },
                    child: Icon(
                      Icons.save,
                      size: 18,
                      color: Colors.white,
                    )),
              )
            ],
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }
  // setValues(ItemBarcodeData itemBarcodeData) {
  //
  //   if (editMode == EditMode.Off) {
  //     _quantityController.text = "";
  //   }
  // }

  bool isWeightedBarcode(String scannedBC){

    /// Validate barcode length
    if (scannedBC.length == 13) {
      /// Weighted barcode format ( 21 562147 00100 7 )
      /// First two characters ( 21 - 29 ) in the above defined barcode format denotes weighted item
      String partOne = scannedBC.substring(0, 2);
      /// Pattern to match with the barcode
      RegExp regExp = new RegExp(r"^2{1,9}",
        caseSensitive: false,
        multiLine: false,
      );
      return regExp.hasMatch(partOne);
    }
    _isWeightedBC = false;
    return false;
  }

  getSplitBarcodeInfo(String scannedBC){
    _isWeightedBC = true;
    _splitBarcode = scannedBC.substring(2, 7);
  }

  _getItemDetailsFromRef(BuildContext context) {
    if (salesOrderDetails != null) {

      MaterialDetailsList salesOrderItem;

      /// Find item in Sales Order Ref Details Items' list

      if (_itemCodeController.text.isNotEmpty) {
        if(isWeightedBarcode(_itemCodeController.text))
        getSplitBarcodeInfo(_itemCodeController.text);
        // if (_itemInputType == InputType.ItemCode) {
          List<MaterialDetailsList> salesOrderItemsList = salesOrderDetails
              .materialDetailsList
              .where((item) => (((_itemInputType == InputType.ItemCode) ? item.itemCode
              : isWeightedBarcode(_itemCodeController.text)? _splitBarcode
              : item.barCode) == _itemCodeController.text))
              .toList();

          if (salesOrderItemsList != null && salesOrderItemsList.isNotEmpty) {
            salesOrderItem = salesOrderItemsList[0];

            /// Check whether already the item is added or not
            if ( _itemInputType == InputType.Barcode &&
                CommonSalesItemInfo.shared.cmnSalesItemList
                .where((item) =>
            item.itemBarcode.toString() == salesOrderItem.barCode)
                .toList()
                .isNotEmpty) {
              showAlert(context: context, message: 'Item already added');
              _didSelect(InputType.Barcode);
              return;
            }
          }
          else{
            Scaffold.of(context).showSnackBar(SnackBar(
                content: Text('Item not found'),
                duration: Duration(milliseconds: 500)));
            return;
            // _didSelect(InputType.Barcode);
          }
        // }
        // else if (_itemInputType == InputType.Barcode) {
        //   List<MaterialDetailsList> salesOrderItemsList = salesOrderDetails
        //       .materialDetailsList
        //       .where((item) => item.barCode == _itemCodeController.text)
        //       .toList();
        //   if (salesOrderItemsList != null && salesOrderItemsList.isNotEmpty) {
        //     salesOrderItem = salesOrderItemsList[0];
        //   }else{
        //     Scaffold.of(context).showSnackBar(SnackBar(
        //         content: Text('Item not found'),
        //         duration: Duration(milliseconds: 500)));
        //     // _didSelect(InputType.Barcode);
        //   }
        // }

        /// If match has been found, bind data. Otherwise check item in local db
        if (salesOrderItem != null) {
          itemDetail.inputType = _itemInputType;

          itemDetail.orderDetId = salesOrderItem.orderDetId;
          itemDetail.itemId = salesOrderItem.itemId;
          itemDetail.itemBarcode = int.parse(salesOrderItem.barCode);
          itemDetail.itemCode = salesOrderItem.itemCode;
          itemDetail.unitId = salesOrderItem.unitId;
          itemDetail.pkgName = salesOrderItem.unitName;
          itemDetail.itemName = salesOrderItem.itemName;
          itemDetail.productStockTypeId = salesOrderItem.productStockTypeId;
          itemDetail.focQuantity = salesOrderItem.fOCQuantity;
          itemDetail.quantity = salesOrderItem.deliveryQuantity;
          itemDetail.totalReqQty = salesOrderItem.deliveryQuantity;
          itemDetail.brandName = salesOrderItem.brandName ?? "";
          itemDetail.countryName = salesOrderItem.orginCountryName ?? "";
          itemDetail.packingDesc = salesOrderItem.packingDetails;

          _quantityController.text = itemDetail.totalReqQty.toString();

          setState(() {});
          _setItemName();
          if (_itemInputType == InputType.ItemCode) {
            _loadItemPacking();
          }
        }
      }
    }
  }

  // Get Widget Methods

  _getItemBasicInfo() {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.scatter_plot, size: 10),
                      SizedBox(width: 4.0),
                      Flexible(
                        child: Text(getBrandName(),
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 13.0),
                            overflow: TextOverflow.ellipsis,
                            softWrap: false),
                      ),
                    ],
                  )),
              SizedBox(width: 10),
              Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.public, size: 12),
                      SizedBox(width: 4.0),
                      Flexible(
                        child: Text(
                          getCountryName(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 13.0),
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                        ),
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ],
    );
  }

  getCountryName() {
    return itemDetail == null
        ? "--"
        : ((itemDetail.countryName != null && itemDetail.countryName != '') ? itemDetail.countryName : "--");
  }

  getBrandName() {
    return itemDetail == null
        ? "--"
        : ((itemDetail.brandName != null && itemDetail.brandName != '') ? itemDetail.brandName : "--");
  }

  // Pop ups

  bool _validateItemDetail() {
    if (_itemCodeController.text.isEmpty) {
      showMessage(
          context: context, message: "Please enter Item code/ Item Barcode");
      return false;
    }

    /// Check whether already the item is added or not
    if (editMode == EditMode.Off &&
        CommonSalesItemInfo.shared.cmnSalesItemList
        .where((item) => item.itemBarcode == itemDetail.itemBarcode)
        .toList()
        .isNotEmpty) {
      showAlert(context: context, message: 'Item already added');
      return false;
    }

    if (itemDetail.unitId != null && itemDetail.unitId == 0) {
      showMessage(context: context, message: "Please select item package");
      return false;
    }
    if (_quantityController.text.trim().isEmpty ||
        double.tryParse(_quantityController.text.trim()) == 0.0 ||
        int.tryParse(_quantityController.text.trim()) == 0) {
      showMessage(context: context, message: "Please enter quantity");
      quantityFocusNode.requestFocus();
      return false;
    }
    if(itemDetail.quantity > itemDetail.totalReqQty){
      showMessage(context: context, message: "Please enter quantity less than balance quantity");
      return false;
    }
    return true;
  }

  void saveItemDetails() {
    // if (widget.type == TransactionType.SAI || widget.type == TransactionType.SO) {
    //   itemDetail.isFromBackEnd = isItemPriceFromBackend;
    //   itemDetail.rate = itemPrice.unitPrice.trim() ?? 0.0;
    //   itemDetail.discount = itemPrice.discount.trim() ?? 0.0;
    //   itemDetail.discountPercentage = itemPrice.discPercentage.trim() ?? 0.0;
    //   itemDetail.netCost = itemPrice.getNetAmount().toString();
    // }
    if (editMode == EditMode.On) {
      var indexOfItemInExistingList = CommonSalesItemInfo
          .shared.cmnSalesItemList
          .indexWhere((item) => item.lineNum == itemDetail.lineNum);

      if (indexOfItemInExistingList != -1) {
        CommonSalesItemInfo.shared.cmnSalesItemList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CommonSalesItem.fromJson(itemDetail.toJson())]);
        showMessage(context: context, message: 'Updated!');

        editMode = EditMode.Off;
      }
      else {
        showMessage(context: context, message: 'Please try again');

      }
    }
    else {
      CommonSalesItemInfo.shared.cmnSalesItemList
          .add(CommonSalesItem.fromJson(itemDetail.toJson()));
      showMessage(context: context, message: 'Saved!');
    }

    _didSelect(InputType.Barcode);
  }

  void _setItemName() {
    _itemCodeController.text = itemDetail.itemName;
  }

  void _findUniqueItemWithItemCodeAndPackingId() {
    if (_itemInputType == InputType.ItemCode) {
      if (itemDetail.itemId != null && itemDetail.unitId != null) {
        dnItemBloc.add(LoadItemByPacking(
            itemId: itemDetail.itemId.toString(),
            prodPkgId: itemDetail.unitId.toString()));
      }
    }
  }

  void _loadItemPacking() {
    dnItemBloc.add(LoadPacking(itemId: itemDetail.itemId.toString()));
  }

  void _loadSalesOrderDetails() {
    dnItemBloc.add(
        GetSalesOrderDetails(salesOrderId: cmnSalesClientInfo.salesOrderId));
  }
}

class RefItemRow extends StatelessWidget {
  final bool hasAdded;
  final MaterialDetailsList item;
  const RefItemRow(
      {Key key, this.hasAdded, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      child: ListTile(
        // tileColor: hasAdded ? BCAppTheme().greenColor : Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
            children: <Widget>[
              Expanded(
                  child:
                  Column(
                        children: [
                          if (hasAdded) Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child:  CircleAvatar(
                                  backgroundColor: BCAppTheme().primaryColor,
                                  radius: 10,
                                  child: Icon(Icons.done,size: 12, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                        children: [
                                          Icon(Icons.radio_button_on,size: 10.0,),
                                          SizedBox(width: 2.0,),
                                          Text(item.itemCode,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 12.0,
                                                  color: BCAppTheme().textColor,
                                                  fontWeight: FontWeight.bold),
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: true),]
                                    ),
                                    getBarcodeTextField(),
                                  ],
                                ),
                              ),
                              SizedBox(width: 8.0),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text("Item Name",
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: BCAppTheme().textColor,
                                            fontWeight: FontWeight.bold),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true),
                                    getItemNameField(),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ]
                    ),
              )
            ]
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child:  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getBrandNameField(),
                      getPackageNameField(),
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      getOrginNameFiled(),
                      getPkgDesc(),
                      getQuantityField(),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }



  getBarcodeTextField() {
    String itemBarcode;
    itemBarcode = item.barCode ?? "";
    return Row(
      children: [
        Icon(Icons.blur_linear,size: 10.0,),
        SizedBox(width: 2.0,),
        Text(itemBarcode,
            maxLines: 1,
            style: TextStyle(
                fontSize: 11.0, color: BCAppTheme().textColor),
            overflow: TextOverflow.ellipsis,
            softWrap: true),
      ],
    );
  }

  getItemNameField() {
    String itemName;

    itemName = item.itemName ?? "";
    return Text(itemName,
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
  getBrandNameField() {
    String brandName;
    brandName = item.brandName ?? "";

    return Text(" $brandName",
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getPackageNameField() {
    String pkgName;
    pkgName = item.unitName ?? "";

    return Text("Package: $pkgName",
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getQuantityField() {
    String itemQuantity;
    itemQuantity = "Req Qty: ${item.deliveryQuantity}";

    return Text(itemQuantity, maxLines: 1, style: TextStyle(
        fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getOrginNameFiled() {
    String orginName;

    orginName = item.orginCountryName ?? "--";

    return Text(orginName,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getPkgDesc() {
    String totalCostText;

    totalCostText =
    "Pkg Desc: ${item.packingDetails}";

    return Text(totalCostText,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
}
