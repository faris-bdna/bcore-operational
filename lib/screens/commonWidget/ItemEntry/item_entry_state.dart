part of 'item_entry_bloc.dart';

abstract class ItemEntryState extends Equatable {
  const ItemEntryState();

  @override
  List<Object> get props => [];
}

class ItemEntryInitial extends ItemEntryState{}

// Get Material Request Header ID from database
class GetMaterialHeaderIdInitial extends ItemEntryState{}

class GetMaterialHeaderIdInProgress extends ItemEntryState{}

class GetMaterialHeaderIdComplete extends ItemEntryState{
  final List<MaterialRequestData> matReqResp;

  GetMaterialHeaderIdComplete({@required this.matReqResp}) : assert(matReqResp != null);

  @override
  List<Object> get props => [matReqResp];
}

class GetMaterialHeaderIdFailure extends ItemEntryState {
  final String error;

  const GetMaterialHeaderIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetMaterialHeaderIdFailure { error : $error }';
}

// Get Material Request Header ID from database
class GetMaterialDetailLineNumInitial extends ItemEntryState{}

class GetMaterialDetailLineNumInProgress extends ItemEntryState{}

class GetMaterialDetailLineNumComplete extends ItemEntryState{
  final MaterialRequestDetail matReqDetailResp;

  GetMaterialDetailLineNumComplete({@required this.matReqDetailResp}) : assert(matReqDetailResp != null);

  @override
  List<Object> get props => [matReqDetailResp];
}

class GetMaterialDetailLineNumFailure extends ItemEntryState {
  final String error;

  const GetMaterialDetailLineNumFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetMaterialDetailLineNumFailure { error : $error }';
}

class LoadItemInitial extends ItemEntryState{}

class LoadItemInProgress extends ItemEntryState{}

class LoadItemComplete extends ItemEntryState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends ItemEntryState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}

// Load packing list
class LoadPackingInitial extends ItemEntryState{}

class LoadPackingInProgress extends ItemEntryState{}

class LoadPackingComplete extends ItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends ItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}

// Load packing by packing id
class LoadPackingByIdInitial extends ItemEntryState{}

class LoadPackingByIdInProgress extends ItemEntryState{}

class LoadPackingByIdComplete extends ItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends ItemEntryState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}

// Load packing list
class LoadItemByPackingInitial extends ItemEntryState{}

class LoadItemByPackingInProgress extends ItemEntryState{}

class LoadItemByPackingComplete extends ItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends ItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}

/// GetItemPriceDetails States
class GetScmItemPriceDetailsInitial extends ItemEntryState{}

class GetScmItemPriceDetailsInProgress extends ItemEntryState{}

class GetScmItemPriceDetailsComplete extends ItemEntryState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetScmItemPriceDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetScmItemPriceDetailsFailure extends ItemEntryState {
  final String error;

  const GetScmItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetScmItemPriceDetailsFailure { error : $error }';
}

// Load country by id
class LoadCountryByIdInitial extends ItemEntryState{}

class LoadCountryByIdInProgress extends ItemEntryState{}

class LoadCountryByIdComplete extends ItemEntryState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends ItemEntryState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}

// Load brand by id
class LoadBrandByIdInitial extends ItemEntryState{}

class LoadBrandByIdInProgress extends ItemEntryState{}

class LoadBrandByIdComplete extends ItemEntryState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends ItemEntryState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}

// Save item
class SaveItemInitial extends ItemEntryState{}

class SaveItemInProgress extends ItemEntryState{}

class SaveItemComplete extends ItemEntryState{
  final int saveItemResult;

  SaveItemComplete({@required this.saveItemResult}) : assert(saveItemResult != null);

  @override
  List<Object> get props => [saveItemResult];
}

class SaveItemFailure extends ItemEntryState {
  final String error;

  const SaveItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveItemFailure { error : $error }';
}

class SaveItemDetailInitial extends ItemEntryState{}

class SaveItemDetailInProgress extends ItemEntryState{}

class SaveItemDetailComplete extends ItemEntryState{
  final int saveItemDetailResult;

  SaveItemDetailComplete({@required this.saveItemDetailResult}) : assert(saveItemDetailResult != null);

  @override
  List<Object> get props => [saveItemDetailResult];
}

class SaveItemDetailFailure extends ItemEntryState {
  final String error;

  const SaveItemDetailFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveItemDetailFailure { error : $error }';
}

/// Get WareHouse

class GetWareHouseInitial extends ItemEntryState{}

class GetWareHouseInProgress extends ItemEntryState{}

class GetWareHouseComplete extends ItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends ItemEntryState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends ItemEntryState{}

class GetWareHouseZoneInProgress extends ItemEntryState{}

class GetWareHouseZoneComplete extends ItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends ItemEntryState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends ItemEntryState{}

class GetWareHouseRackInProgress extends ItemEntryState{}

class GetWareHouseRackComplete extends ItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends ItemEntryState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends ItemEntryState{}

class GetWareHouseBinInProgress extends ItemEntryState{}

class GetWareHouseBinComplete extends ItemEntryState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends ItemEntryState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get SRRefDetails

class GetSRRefDetailsInitial extends ItemEntryState{}

class GetSRRefDetailsInProgress extends ItemEntryState{}

class GetSRRefDetailsComplete extends ItemEntryState{
  final GetSRRefDetailsResp resp;

  GetSRRefDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetSRRefDetailsFailure extends ItemEntryState {
  final String error;

  const GetSRRefDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSRRefDetailsFailure { error : $error }';
}


/// Get SRRefDetails

class GetTransferReceiptReferenceDetailsInitial extends ItemEntryState{}

class GetTransferReceiptReferenceDetailsInProgress extends ItemEntryState{}

class GetTransferReceiptReferenceDetailsComplete extends ItemEntryState{
  final GetTrReferenceDetailsResp resp;

  GetTransferReceiptReferenceDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetTransferReceiptReferenceDetailsFailure extends ItemEntryState {
  final String error;

  const GetTransferReceiptReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetTransferReceiptReferenceDetailsFailure { error : $error }';
}


/// Get MrRefDetails

class GetMrRefDetailsInitial extends ItemEntryState{}

class GetMrRefDetailsInProgress extends ItemEntryState{}

class GetMrRefDetailsComplete extends ItemEntryState{
  final GetMrRefDetailsResp resp;

  GetMrRefDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetMrRefDetailsFailure extends ItemEntryState {
  final String error;

  const GetMrRefDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetMrRefDetailsFailure { error : $error }';
}