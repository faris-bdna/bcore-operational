part of 'highItem_entry_bloc.dart';

abstract class HighItemEntryState extends Equatable {
  const HighItemEntryState();

  @override
  List<Object> get props => [];
}

class LoadItemInitial extends HighItemEntryState{}

class LoadItemInProgress extends HighItemEntryState{}

class LoadItemComplete extends HighItemEntryState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends HighItemEntryState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends HighItemEntryState{}

class LoadPackingInProgress extends HighItemEntryState{}

class LoadPackingComplete extends HighItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends HighItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing by packing id

class LoadPackingByIdInitial extends HighItemEntryState{}

class LoadPackingByIdInProgress extends HighItemEntryState{}

class LoadPackingByIdComplete extends HighItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends HighItemEntryState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends HighItemEntryState{}

class LoadItemByPackingInProgress extends HighItemEntryState{}

class LoadItemByPackingComplete extends HighItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends HighItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends HighItemEntryState{}

class LoadCountryByIdInProgress extends HighItemEntryState{}

class LoadCountryByIdComplete extends HighItemEntryState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends HighItemEntryState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends HighItemEntryState{}

class LoadBrandByIdInProgress extends HighItemEntryState{}

class LoadBrandByIdComplete extends HighItemEntryState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends HighItemEntryState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get WareHouse

class GetWareHouseInitial extends HighItemEntryState{}

class GetWareHouseInProgress extends HighItemEntryState{}

class GetWareHouseComplete extends HighItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends HighItemEntryState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends HighItemEntryState{}

class GetWareHouseZoneInProgress extends HighItemEntryState{}

class GetWareHouseZoneComplete extends HighItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends HighItemEntryState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends HighItemEntryState{}

class GetWareHouseRackInProgress extends HighItemEntryState{}

class GetWareHouseRackComplete extends HighItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends HighItemEntryState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends HighItemEntryState{}

class GetWareHouseBinInProgress extends HighItemEntryState{}

class GetWareHouseBinComplete extends HighItemEntryState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends HighItemEntryState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get Packing Details

class GetScmItemPriceDetailsInitial extends HighItemEntryState{}

class GetScmItemPriceDetailsInProgress extends HighItemEntryState{}

class GetScmItemPriceDetailsComplete extends HighItemEntryState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetScmItemPriceDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetScmItemPriceDetailsFailure extends HighItemEntryState {
  final String error;

  const GetScmItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetScmItemPriceDetailsFailure { error : $error }';
}


/// Get GRN EntryNobyItem

class GetGRNEntryNobyItemInitial extends HighItemEntryState{}

class GetGRNEntryNobyItemInProgress extends HighItemEntryState{}

class GetGRNEntryNobyItemComplete extends HighItemEntryState{
  final GetGRNEntryNobyItemResp getGRNEntryNobyItemResp;

  GetGRNEntryNobyItemComplete({@required this.getGRNEntryNobyItemResp}) : assert(getGRNEntryNobyItemResp != null);

  @override
  List<Object> get props => [getGRNEntryNobyItemResp];
}

class GetGRNEntryNobyItemFailure extends HighItemEntryState {
  final String error;

  const GetGRNEntryNobyItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNEntryNobyItemFailure { error : $error }';
}


/// LoadBatchById States

class LoadBatchByIdInitial extends HighItemEntryState{}

class LoadBatchByIdInProgress extends HighItemEntryState{}

class LoadBatchByIdComplete extends HighItemEntryState{
  final List<ItemBatchData> getItemBatchResp;

  LoadBatchByIdComplete({@required this.getItemBatchResp}) : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class LoadBatchByIdFailure extends HighItemEntryState {
  final String error;

  const LoadBatchByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBatchByIdFailure { error : $error }';
}



/// GetGRNReferenceDetails States

class GetGRNReferenceDetailsInitial extends HighItemEntryState{}

class GetGRNReferenceDetailsInProgress extends HighItemEntryState{}

class GetGRNReferenceDetailsComplete extends HighItemEntryState{
  final GetGRNReferenceDetailsResp getGRNReferenceDetailsResp;

  GetGRNReferenceDetailsComplete({@required this.getGRNReferenceDetailsResp}) : assert(getGRNReferenceDetailsResp != null);

  @override
  List<Object> get props => [getGRNReferenceDetailsResp];
}

class GetGRNReferenceDetailsFailure extends HighItemEntryState {
  final String error;

  const GetGRNReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNReferenceDetailsFailure { error : $error }';
}



class GetPRReferenceDetailsInitial extends HighItemEntryState{}

class GetPRReferenceDetailsInProgress extends HighItemEntryState{}

class GetPRReferenceDetailsComplete extends HighItemEntryState{
  final GetPRReferenceDetailsResp getPRReferenceDetailsResp;

  GetPRReferenceDetailsComplete({@required this.getPRReferenceDetailsResp}) : assert(getPRReferenceDetailsResp != null);

  @override
  List<Object> get props => [getPRReferenceDetailsResp];
}

class GetPRReferenceDetailsFailure extends HighItemEntryState {
  final String error;

  const GetPRReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPRReferenceDetailsFailure { error : $error }';
}
