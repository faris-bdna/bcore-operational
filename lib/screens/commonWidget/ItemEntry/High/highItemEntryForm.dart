import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/itemPrice.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart'
as cmn;
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemFocDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemSerialNoDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemTaxDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/request/highItemEntry/getGRNEntryNobyItemReq.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetGRNReferenceDetailsResp.dart'
as grnRef;
import 'package:bcore_inventory_management/models/response/highItem/getGRNEntryNobyItemResp.dart'
as grnNo;
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart'
as prRefDet;
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/highItem/common_highItem_details.dart';
import 'package:bcore_inventory_management/models/view/supplierDetails.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'highItem_entry_bloc.dart';

class HighItemEntryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;
  final CommonHighItem item;

  const HighItemEntryForm(
      {Key key,
      this.repository,
      this.appLanguage,
      this.database,
      this.type,
      this.item})
      : super(key: key);

  @override
  _HighItemEntryFormState createState() =>
      _HighItemEntryFormState(repository, appLanguage, database, type, item);
}

class _HighItemEntryFormState extends State<HighItemEntryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;

  CommonHighItem itemDetail;
  EditMode editMode;

  List<grnRef.ItemDetails> grnRefItemList;
  List<prRefDet.ItemDetails> prRefItemList;

  List<ItemPackingData> itemPackingDataList;
  List<cmn.CommonDataList> wareHouseList = [];
  List<cmn.CommonDataList> wareHouseZoneList = [];
  List<cmn.CommonDataList> wareHouseRackList = [];
  List<WarehouseBinList> wareHouseBinList = [];
  List<grnNo.MasterDataList> grnNoItemsList;
  List<ItemBatchData> itemBatchDataList;

  List<FOCDetails> focDetailList = [];
  List<SerialNoDetails> serialNoDetailList = [];
  List<OtherCostTaxDetails> taxDetailList = [];

  InputType _itemInputType = InputType.Barcode;

  var _itemCodeController = TextEditingController();
  var _batchNoController = TextEditingController();
  var _quantityController = TextEditingController();
  var _unitPriceController = TextEditingController();
  var _exciseDutyController = TextEditingController();
  var _actualPriceController = TextEditingController();
  var _discountPercentController = TextEditingController();
  var _discountAmountController = TextEditingController();

  var _taxAmountController = TextEditingController();
  var _netAmountController = TextEditingController();
  var _minShelfLifeController = TextEditingController();
  var _searchController = TextEditingController();
  var _serialNoController = TextEditingController();

  var quantityFocusNode = FocusNode();

  // Should contain or not
  bool containItemBasicInfo = false;
  bool containItemPricing = false;
  bool containItemExpiryDate = false;
  bool containBatchNoDropdown = false;

  bool containItemLocation = false;
  bool containSubstituteCheck = false;
  bool containFocAndQualityCheck = false;
  bool containInstallationRequired = false;
  bool containGrnNo = false;
  bool containBatchNoField = false;
  bool containSerialNo = false;

  bool shouldValidateItemCode = false;
  bool shouldValidatePackage = false;
  bool shouldValidateQuantity = false;
  bool isQuantityValid = false;
  bool shouldValidateBatchNo = false;
  bool shouldValidateGrnNo = false;

  double tempTotalUOMQty = 0.0;
  double tempUOMQty = 0.0;

  // String selectedGrnNoValue;
  // grnNo.MasterDataList selectedGRN;

  List<CommonHighItem> focParentItems = [];
  List<CommonHighItem> selectedFocParentItems = [];

  ItemPrice itemPrice = ItemPrice();

  HighItemEntryBloc _hteBloc;
  BuildContext buildContext;

  // bool suppContractPriceStatus;
  bool itemHasExpiry = false;
  bool shouldDisableBatchNo = false;
  AlertDialog alert;

  OutlineInputBorder border = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
    borderSide: BorderSide(color: BCAppTheme().headingTextColor, width: 0.5),
  );

  _HighItemEntryFormState(this.repository, this.appLanguage, this.database,
      this.type, this.itemDetail);

  @override
  void initState() {
    super.initState();

    _configUI();
    _configValidation();

    itemPrice.reset();
    // WidgetsBinding.instance.addPostFrameCallback((_) => loadControllers());

    loadItemDetailsIfInEditMode();

    if (widget.type == TransactionType.GRN) {
      // if (cmnSupplierInfo.gRNTypeId == GRNType.IPO.index + 1 ||
      //     cmnSupplierInfo.gRNTypeId == GRNType.LPO.index + 1) {
      //   loadItemListForGRNReference();
      // } else {
      if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1) {
        prepareFOCItems();
      }
    } else if (widget.type == TransactionType.PR) {
      if (cmnSupplierInfo.pRetTypeId == PRType.GRN.index + 1) {
        containGrnNo = false;
        // loadItemListForPRReference();
      }
    }
  }

  loadItemListForGRNReference() {
    // Future.delayed(Duration.zero, () {
    _hteBloc.add(GetGRNReferenceDetails(
        refTypeId: cmnSupplierInfo.gRNTypeId.toString(),
        refId: cmnSupplierInfo.gRNTypeRefNo.toString()));
    // });
  }

  loadItemListForPRReference() {
    // Future.delayed(Duration.zero, () {
    _hteBloc.add(
        GetPRReferenceDetails(refId: cmnSupplierInfo.pRetTypeRefNo.toString()));
    // });
  }

  loadItemDetailsIfInEditMode() {
    setState(() {
      if (editMode == EditMode.On) {
        _itemInputType = itemDetail.inputType;
        _itemCodeController.text = (_itemInputType == InputType.Barcode)
            ? itemDetail.itemBarcode
            : itemDetail.itemCode;

        if (widget.type == TransactionType.GRN) {
          serialNList = itemDetail.serialNoList;
          _quantityController.text = itemDetail.unitQty.toString();
        }
        if (widget.type == TransactionType.PR) {
          _quantityController.text = itemDetail.returnedQty.toString();
        }

        _batchNoController.text = itemDetail.batchNo;

        if (_quantityController.text.trim() != null &&
            _quantityController.text.trim() != '') {
          isQuantityValid = true;
        }
        itemPrice.unitPrice = itemDetail.unitPrice;
        itemPrice.quantity = itemDetail.unitQty;
        itemPrice.discPercentage = itemDetail.discPercent;
        itemPrice.exciseDuty = itemDetail.exciseDuty;
        itemPrice.unitPrice = itemDetail.unitPrice;
        // itemPrice.discount = itemDetail.totalCost;
        // _discountAmountController.text = itemPrice.discount.toString();
        // _taxAmountController.text = itemPrice.taxAmount.toString();
        // double discAmount = _discountAmountController.text.trim() as double;
        // print(discAmount);
        // double taxAmount = _taxAmountController.text.trim().toString().toDouble(pos: getDeci());
        // print(taxAmount);
        // _unitPriceController.text.trim().toDouble(pos: getDeci());
        updatePrices();
      } else {
        itemDetail =
            CommonHighItem(); // TODO Confirm this doesn't make any issue in the flow
        itemDetail.isFoc = false;
        itemDetail.isInstallationChecked = false;
        itemDetail.expiryDateTemp = '';
        itemDetail.hasParent = false;
        itemDetail.isItemQualityChecked = false;
      }
    });
  }

  prepareFOCItems() {
    if (CommonHighItemInfo.shared.cmnHighItemList != null &&
        CommonHighItemInfo.shared.cmnHighItemList.isNotEmpty) {
      focParentItems = CommonHighItemInfo.shared.cmnHighItemList
          .where((element) => element.isFoc != true)
          .toList();
    }
  }

  @override
  void dispose() {
    super.dispose();

    editMode = EditMode.Off;
    itemDetail = null;

    _actualPriceController.dispose();
    _batchNoController.dispose();
    _discountPercentController.dispose();
    _discountAmountController.dispose();
    _exciseDutyController.dispose();
    _itemCodeController.dispose();
    _minShelfLifeController.dispose();
    _netAmountController.dispose();
    _quantityController.dispose();
    _searchController.dispose();
    _taxAmountController.dispose();
    _unitPriceController.dispose();
  }

  _didSelect(InputType type) {
    setState(() {
      _itemInputType = type;
      _itemCodeController.text = '';
      _batchNoController.text = '';
      _quantityController.text = '';
      _unitPriceController.text = '';
      _minShelfLifeController.text = '';
      _netAmountController.text = '';
      _actualPriceController.text = '';
      _discountAmountController.text = '';
      _taxAmountController.text = '';
      _discountPercentController.text = '';
      _exciseDutyController.text = '';
      _searchController.text = '';

      _clearItemInfo();
      itemPackingDataList = null;
      itemBatchDataList = [];
      itemPrice.reset();
      itemPackingDataList = null;
    });
  }

  void _setItemInfo(
      {ItemBarcodeData itemBarcodeData,
      grnRef.ItemDetails itemRefData,
      prRefDet.ItemDetails prItemRefData}) {
    setState(() {
      if (widget.type == TransactionType.GRN) {
        if (cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) {
          itemDetail.itemBarcode = itemBarcodeData.Text;
          itemDetail.itemCode = itemBarcodeData.ItemCode;
          itemDetail.itemId = itemBarcodeData.ItemId;
          itemDetail.packingId = itemBarcodeData.ItemPackingId;
          itemDetail.packingDesc = itemBarcodeData.PackingDesc;
          itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
          itemDetail.brandId = itemBarcodeData.BrandId.toString();
          itemHasExpiry = ProductStockType
                  .values[itemBarcodeData.ProductStockTypeId ?? 0] ==
              ProductStockType.Expiry;
          containSerialNo = (itemBarcodeData.ProductStockTypeId ==
              ProductStockType.Serialized.index);
          containItemExpiryDate = itemHasExpiry;
          itemDetail.itemName = itemBarcodeData.ItemName ?? "";
          itemDetail.taxPercent = itemBarcodeData.TaxPercent;
          itemDetail.netCost = itemBarcodeData.NetCost;
          itemDetail.minShelfLife = itemBarcodeData.MinShelfLife;
          itemDetail.unitPrice = double.tryParse(itemBarcodeData.AverageCost);
          itemDetail.isBatchEnabled = itemBarcodeData.IsBatchEnabled;
          containBatchNoField = itemDetail.isBatchEnabled.toBool();
        } else {
          itemDetail.itemBarcode = itemRefData.itemBarcode;
          itemDetail.itemCode = itemRefData.itemCode;
          itemDetail.itemId = itemRefData.itemId;
          itemDetail.itemName = itemRefData.itemName;
          itemDetail.packingId = itemRefData.packingId;
          itemDetail.pkgName = itemRefData.pkgName;
          itemDetail.packingDesc = itemRefData.packingDesc ?? "";
          itemPrice.unitPrice = itemRefData.unitPrice;
          itemPrice.refUnitPrice = itemRefData.unitPrice;
          itemPrice.discPercentage = itemRefData.discPercent;
          itemPrice.discount = itemRefData.discAmount;
          itemPrice.actualPrice = itemRefData.actualPrice;
          itemPrice.taxAmount = itemRefData.taxAmount;
          itemPrice.taxPercentage = itemRefData.taxPerc;
          itemPrice.netAmount = itemRefData.netAmount;
          itemDetail.suppContractPriceStatus =
              itemRefData.isSuppContractPrice.toBool();
          itemDetail.brandName = itemRefData.brandName;
          itemDetail.countryName = itemRefData.countryName;
          itemDetail.isFoc = itemRefData.isFoc;
          itemDetail.taxPercent = itemRefData.taxPerc.toString();
          itemDetail.netCost = itemRefData.netAmount.toString();
          itemDetail.minShelfLife = itemRefData.minShelfLife;
          itemDetail.isBatchEnabled = itemRefData.batchNoEnableItem.toInt();
          containBatchNoField = itemDetail.isBatchEnabled.toBool();
          itemDetail.pendingQty = itemRefData.pendingQty;
          itemDetail.uOMQty = itemRefData.uOMQty;
          tempUOMQty = itemRefData.uOMQty;
          itemDetail.totalUOMQty = itemRefData.totalUOMQty;
          tempTotalUOMQty = itemRefData.totalUOMQty;
          if (itemRefData.productStockTypeId != null) {
            itemHasExpiry =
                ProductStockType.values[itemRefData.productStockTypeId ?? 0] ==
                    ProductStockType.Expiry;
          }
          containItemExpiryDate = itemHasExpiry;
          containSerialNo = (itemRefData.productStockTypeId ==
              ProductStockType.Serialized.index);

          if (cmnSupplierInfo.deliveryTypeId == DeliveryType.Full.index + 1) {
            itemDetail.unitQty = itemRefData.pendingQty;
            itemPrice.quantity = itemRefData.pendingQty;
            _quantityController.text = itemRefData.pendingQty.toString();
            isQuantityValid = true;
          }

          _setItemName();
          setPriceValues(cmnUOMQTY: itemDetail.uOMQty);
          updatePrices();
        }
      } else if (widget.type == TransactionType.PR) {
        if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1) {
          itemDetail.itemBarcode = itemBarcodeData.Text;
          itemDetail.itemCode = itemBarcodeData.ItemCode;
          itemDetail.itemId = itemBarcodeData.ItemId;
          itemDetail.packingId = itemBarcodeData.ItemPackingId;
          itemDetail.itemName = itemBarcodeData.ItemName;
          itemDetail.countryId = itemBarcodeData.OriginCountryId.toString();
          itemDetail.brandId = itemBarcodeData.BrandId.toString();
          if (itemBarcodeData.ProductStockTypeId != null) {
            itemHasExpiry =
                ProductStockType.values[itemBarcodeData.ProductStockTypeId] ==
                    ProductStockType.Expiry;
          }
          itemDetail.itemName = itemBarcodeData.ItemName ?? "";
          itemDetail.taxPercent = itemBarcodeData.TaxPercent;
          itemDetail.netCost = itemBarcodeData.NetCost;
          itemDetail.minShelfLife = itemBarcodeData.MinShelfLife;
        } else if (cmnSupplierInfo.pRetTypeId == PRType.GRN.index + 1) {
          itemDetail.itemBarcode = prItemRefData.itemBarcode ?? "";
          itemDetail.itemCode = prItemRefData.itemCode;
          itemDetail.itemId = prItemRefData.itemId;
          itemDetail.itemName = prItemRefData.itemName;
          itemDetail.packingId = prItemRefData.packingId ?? 0;
          itemDetail.pkgName = prItemRefData.pkgName;
          itemDetail.packingDesc = prItemRefData.packingDesc;
          itemDetail.packingDesc = prItemRefData.packingDesc ?? '';
          itemPrice.unitPrice = prItemRefData.unitPrice;
          itemPrice.refUnitPrice = prItemRefData.unitPrice;
          itemDetail.unitPrice = prItemRefData.unitPrice;
          itemDetail.batchNo = prItemRefData.batchNo;
          itemPrice.actualPrice = prItemRefData.totalPrice;
          itemPrice.taxAmount = prItemRefData.taxAmount;
          itemPrice.taxPercentage = prItemRefData.itemTaxPer;
          itemPrice.netAmount = prItemRefData.netAmount;
          itemDetail.netCost = prItemRefData.netAmount.toString();
          itemDetail.gRNQty = prItemRefData.gRNQty;
          itemDetail.countryId = prItemRefData.orginId.toString();
          itemDetail.brandId = prItemRefData.brandId.toString();
          itemDetail.uOMQty = prItemRefData.uOMQty;

          _setItemName();
        }
      }
    });
  }

  _clearItemInfo() {
    itemDetail = new CommonHighItem();
    itemDetail.isFoc = false;
    itemDetail.isInstallationChecked = false;
    itemDetail.expiryDateTemp = '';
    itemDetail.hasParent = false;
    itemDetail.isItemQualityChecked = false;
    serialNList = [];
  }

  _configUI() {
    editMode = (itemDetail == null) ? EditMode.Off : EditMode.On;

    switch (widget.type) {
      case TransactionType.GRN:
        containBatchNoDropdown = false;
        containFocAndQualityCheck = true;
        containItemBasicInfo = true;
        containItemPricing = true;
        containItemExpiryDate = true;
        containItemLocation = true;
        containInstallationRequired = true;
        containBatchNoField = true;
        break;
      case TransactionType.PR:
        containBatchNoDropdown = true;
        containFocAndQualityCheck = false;
        containItemBasicInfo = true;
        containItemPricing = true;
        containItemExpiryDate = false;
        containItemLocation = true;
        containInstallationRequired = false;
        containGrnNo = true;
        containBatchNoField = false;
        break;
    }
  }

  _configValidation() {
    switch (widget.type) {
      case TransactionType.GRN:
        shouldValidateItemCode = true;
        shouldValidatePackage = true;
        shouldValidateQuantity = true;
        shouldValidateBatchNo = true;
        isQuantityValid = false;

        shouldValidateGrnNo = false;
        break;

      case TransactionType.PR:
        shouldValidateItemCode = true;
        shouldValidatePackage = true;
        shouldValidateQuantity = true;
        shouldValidateBatchNo = true;
        isQuantityValid = false;

        shouldValidateGrnNo = true;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    buildContext = context;

    _bindItemData(ItemBarcodeData item) {
      itemDetail.inputType = _itemInputType;

      if (editMode == EditMode.Off) {
        _setItemInfo(itemBarcodeData: item);
      }

      if (containBatchNoDropdown) {
        _loadItemBatch();
      }

      if (_itemInputType == InputType.ItemCode) {
        _loadItemPacking();
      } else {
        _setItemName();
        if (editMode == EditMode.Off) {
          _loadItemPackingById();
          _loadItemPriceDetails();
          _loadCountry();
        }
      }
    }

    _validateAndBindItemData(ItemBarcodeData selItem) {
      /// Check whether already the item is added or not
      if (_itemInputType == InputType.Barcode &&
          editMode == EditMode.Off &&
          CommonHighItemInfo.shared.cmnHighItemList
              .where((item) => item.itemBarcode == selItem.Text)
              .toList()
              .isNotEmpty) {
        showAlert(context: context, message: 'Item already added');
        _didSelect(InputType.Barcode);
        return;
      }

      setState(() {
        if (editMode == EditMode.Off) {
          itemPrice.reset();
        }
        if (widget.type == TransactionType.GRN) {
          /// when item stock allocation is warehouse && supplier is external, GRN can't be created.
          /// If item doesn't have group purchase access then also GRN can't be created
          if ((selItem.StockAllocTypeId == 1 &&
                  !cmnSupplierInfo.isSupplierInternal) ||
              !selItem.HasPurchaseGroupAccess.toBool()) {
            showAlert(context: context, message: "Item not found");
            return;
          }
          if (ItemStatus.values[selItem.ItemStatus] == ItemStatus.Active ||
              (ItemStatus.values[selItem.ItemStatus] != ItemStatus.Active &&
                  cmnSupplierInfo.isSupplierInternal)) {
            _bindItemData(selItem);
          } else {
            showAlert(context: context, message: "Item not found");
          }
        } else if (widget.type == TransactionType.PR) {
          _bindItemData(selItem);
        }
        quantityFocusNode.requestFocus();
      });
    }

    return BlocProvider<HighItemEntryBloc>(
      create: (context) => HighItemEntryBloc(
          bcRepository: repository, database: database, grnReq: GrnReq.shared),
      child: BlocListener<HighItemEntryBloc, HighItemEntryState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingByIdInProgress ||
              state is LoadPackingInProgress ||
              state is LoadItemByPackingInProgress ||
              state is LoadCountryByIdInProgress ||
              state is LoadBrandByIdInProgress ||
              state is GetWareHouseInProgress ||
              state is GetWareHouseZoneInProgress ||
              state is GetWareHouseRackInProgress ||
              state is GetWareHouseBinInProgress ||
              state is GetScmItemPriceDetailsInProgress ||
              state is GetGRNEntryNobyItemInProgress ||
              state is LoadBatchByIdInProgress ||
              state is GetGRNReferenceDetailsInProgress ||
              state is GetPRReferenceDetailsInProgress) {
            Common().showLoader(context);
          } else if (state is LoadItemFailure ||
              state is LoadPackingByIdFailure ||
              state is LoadPackingFailure ||
              state is LoadItemByPackingFailure ||
              state is LoadCountryByIdFailure ||
              state is LoadBrandByIdFailure ||
              state is GetWareHouseFailure ||
              state is GetWareHouseZoneFailure ||
              state is GetWareHouseRackFailure ||
              state is GetWareHouseBinFailure ||
              state is GetScmItemPriceDetailsFailure ||
              state is GetGRNEntryNobyItemFailure ||
              state is LoadBatchByIdFailure ||
              state is GetGRNReferenceDetailsFailure ||
              state is GetPRReferenceDetailsFailure) {
            Navigator.pop(context);
          } else if (state is LoadItemComplete) {
            Navigator.pop(context);

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              _validateAndBindItemData(state.loadItemResult[0]);
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
          } else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);
            itemBatchDataList = [];
            if (state.getItemBatchResp != null &&
                state.getItemBatchResp.length > 0) {
              setState(() {
                itemBatchDataList = state.getItemBatchResp;
                if (editMode == EditMode.On) {
                  _loadGrnNos();
                }
//                selectedItemBatch = int.parse(itemBatchDataList[0].batch_text);
              });
            }
          } else if (state is LoadPackingComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                if (editMode == EditMode.Off) {
                  itemDetail.packingId =
                      int.parse(itemPackingDataList[0].packing_value);
                  itemDetail.pkgName = itemPackingDataList[0].packing_text;

                  /**
                   * Load unique item info with barcode based on selected packing, using the item id
                   * */
                  _findUniqueItemWithItemCodeAndPackingId();
                  _loadCountry();
                }
                // Set item name in barcode text field
                _setItemName();
                FocusScope.of(context).unfocus();
              });
            }
          } else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.length > 0) {
              /// Check whether already the item is added or not
              if (CommonHighItemInfo.shared.cmnHighItemList
                  .where((item) =>
                      item.itemBarcode == state.loadItemByPackingResult[0].Text)
                  .toList()
                  .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }

              setState(() {
                itemDetail.itemBarcode =
                    state.loadItemByPackingResult[0].Text ?? "0";

                itemDetail.packingDesc =
                    state.loadItemByPackingResult[0].PackingDesc;
                if (type == TransactionType.GRN) {
                  if (cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) {
                    itemPrice.unitPrice = 0.0;
                    _loadItemPriceDetails();
                  } else {
                    itemDetail.uOMQty =
                        double.parse(state.loadItemByPackingResult[0].PkgQty);
                    _quantityController.text =
                        (tempTotalUOMQty / itemDetail.uOMQty).trim().toString();
                  }
                }

                setPriceValues(
                    cmnUOMQTY:
                        double.parse(state.loadItemByPackingResult[0].PkgQty));
                updatePrices();
              });
            }
          } else if (state is LoadPackingByIdComplete) {
            Navigator.pop(context);
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                // itemPackingDataList = state.loadPackingResult;
                // selectedPackageId = itemBarcodeData.ItemPackingId;
                if (state.loadPackingResult.isNotEmpty) {
                  itemDetail.pkgName = state.loadPackingResult
                      .where((packing) =>
                          packing.packing_value ==
                          itemDetail.packingId.toString())
                      .toList()[0]
                      .packing_text;
                }
              });
            }
          } else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.countryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }
            _loadProductBrand();
          } else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.brandName = productBrandList[0].brand_name.trim();
              });
            }
          } else if (state is GetWareHouseComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.Off) {
                  showItemLocationDialogue(context);
                } else {
                  _loadZone();
                }
              });
            }
          } else if (state is GetWareHouseZoneComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseZoneList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On) {
                  _loadRack();
                }
              });
            }
          } else if (state is GetWareHouseRackComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseRackList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On) {
                  _loadBin();
                }
              });
            }
          } else if (state is GetWareHouseBinComplete) {
            Navigator.pop(context);

            if (state.getWarehouseBinMasterResp != null &&
                state.getWarehouseBinMasterResp.masterDataList.length > 0) {
              setState(() {
                wareHouseBinList =
                    state.getWarehouseBinMasterResp.masterDataList;
                if (editMode == EditMode.On) {
                  showItemLocationDialogue(context);
                }
              });
            }
          } else if (state is GetScmItemPriceDetailsComplete) {
            Navigator.pop(context);
            FocusScope.of(context).requestFocus(new FocusNode());

            if (state.getPackingDetailsResp != null &&
                state.getPackingDetailsResp.masterDataList.isNotEmpty) {
              setState(() {
                itemDetail.suppContractPriceStatus = state
                    .getPackingDetailsResp.masterDataList[0].isSuppContractPrice
                    .toBool();

                itemPrice.unitPrice =
                    state.getPackingDetailsResp.masterDataList[0].unitPrice;
                setPriceValues(cmnUOMQTY: itemDetail.uOMQty);
                updatePrices();
              });
            }
          } else if (state is GetGRNEntryNobyItemComplete) {
            Navigator.pop(context);

            if (state.getGRNEntryNobyItemResp != null &&
                state.getGRNEntryNobyItemResp.masterDataList.length > 0) {
              setState(() {
                grnNoItemsList = state.getGRNEntryNobyItemResp.masterDataList;

                // Populate dropdown
                // for (int i = 0; i < grnNoItemsList.length; i++) {
                //   grnNoDropdownList.add(DropdownMenuItem(
                //     child: Text(grnNoItemsList[i].text,
                //         style: TextStyle(
                //           fontSize: 14.0,
                //           color: BCAppTheme().subTextColor,
                //         )),
                //     value: int.parse(grnNoItemsList[i].value),
                //   ));
                // }
                if (editMode == EditMode.On) {
                  _bindGrnNos();
                }
              });
            } else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'No GRN number found',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          } else if (state is GetGRNReferenceDetailsComplete) {
            Navigator.pop(context);
            if (state.getGRNReferenceDetailsResp.data.itemDetails.isNotEmpty) {
              grnRefItemList =
                  state.getGRNReferenceDetailsResp.data.itemDetails;
              print(state.getGRNReferenceDetailsResp.toJson());
              if (editMode == EditMode.On) {
                _getItemDetails(context);
              }
            } else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          } else if (state is GetPRReferenceDetailsComplete) {
            Navigator.pop(context);

            if (state.getPRReferenceDetailsResp.data.itemDetails.isNotEmpty) {
              SavePRReq.shared.dateFormat =
                  state.getPRReferenceDetailsResp.data.dateFormat;
              prRefItemList = state.getPRReferenceDetailsResp.data.itemDetails;
              if (editMode == EditMode.On) {
                _getItemDetails(context);
              }
            } else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected GRN No doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }
        },
        child: BlocBuilder<HighItemEntryBloc, HighItemEntryState>(
          builder: (blocContext, state) {
            _hteBloc = BlocProvider.of<HighItemEntryBloc>(blocContext);

            if (state is LoadItemInitial) {
              if (type == TransactionType.GRN) {
                if (cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) {
                  /// Works only if the form is in editing mode (update).
                  /// Item code / item barcode and other item details will be set to common itemDetail object in initState().
                  /// So here just loads the item from db by calling the common item binding method.
                  if (editMode == EditMode.On) {
                    _getItemDetails(blocContext);
                  }
                } else {
                  loadItemListForGRNReference();
                }
              } else if (type == TransactionType.PR) {
                if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1) {
                  /// Works only if the form is in editing mode (update).
                  /// Item code / item barcode and other item details will be set to common itemDetail object in initState().
                  /// So here just loads the item from db by calling the common item binding method.
                  if (editMode == EditMode.On) {
                    _getItemDetails(blocContext);
                  }
                } else {
                  loadItemListForPRReference();
                }
              }
            }
            return Container(
              child: Column(
                children: [
                  _getItemCodeFields(blocContext),
                  if (_itemInputType == InputType.ItemCode)
                    _getPackageField(blocContext),
                  if (itemDetail.packingDesc != null) _getPackingDescrField(),
                  if (containFocAndQualityCheck) _getFocAndQualityFields(),
                  if (containBatchNoDropdown) _getBatchNoField(context),
                  if (containGrnNo) _getGrnNoField(),
                  _getQuantityFields(),
                  _getItemDetailsFields(blocContext)
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Get Widget Methods

  _getItemCodeFields(BuildContext blocContext) {
    return editMode == EditMode.Off
        ? Column(
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 20,
                          height: 20,
                          child: Radio(
                              activeColor: BCAppTheme().headingTextColor,
                              value: InputType.Barcode,
                              groupValue: _itemInputType,
                              onChanged: _didSelect),
                        ),
                        SizedBox(width: 8),
                        Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 20,
                          height: 20,
                          child: Radio(
                              activeColor: BCAppTheme().headingTextColor,
                              value: InputType.ItemCode,
                              groupValue: _itemInputType,
                              onChanged: _didSelect),
                        ),
                        SizedBox(width: 8),
                        Text("Item Code", style: TextStyle(fontSize: 13.0))
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              SizedBox(
                height: 55,
                child: TextFormField(
                  enableSuggestions: false,
                  textInputAction: TextInputAction.next,
                  controller: _itemCodeController,
                  autocorrect: false,
                  decoration: Common().getBCoreMandatoryID(
                      isMandatory: shouldValidateItemCode,
                      isValidated: _itemCodeController.text.trim().isNotEmpty,
                      hintText: (_itemInputType == InputType.Barcode)
                          ? 'Item Barcode'
                          : 'Item Code',
                      icon: _itemInputType == InputType.Barcode
                          ? Icons.blur_linear
                          : Icons.search,
                      iconPress: () async {
                        if (_itemInputType == InputType.Barcode) {
                          var result = await BarcodeScanner.scan();
                          setState(() {
                            _itemCodeController.text = result;
                            _getItemDetails(blocContext);
                          });
                        } else {
                          _getItemDetails(blocContext);
                        }
                      }),
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    _getItemDetails(blocContext);
                    return null;
                  },
                  // onChanged: (value) {
                  //   if (_itemInputType == InputType.Barcode) {
                  //     setState(() {
                  //       _getItemDetails(blocContext);
                  //     });
                  //   }
                  // },
                  onFieldSubmitted: (value) {
                    _getItemDetails(blocContext);
                  },
                ),
              )
            ],
          )
        : Column(
            children: [
              ElevatedButton(
                  onPressed: () {
                    editMode = EditMode.Off;
                    _didSelect(InputType.Barcode);
                  },
                  child: Text("Clear & Add New"))
            ],
          );
  }

  _getPackageField(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: shouldValidatePackage,
                isValidated: itemDetail.packingId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Container(),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.packingId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value));
                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.packingId = value;
                    itemDetail.pkgName = itemPackingDataList
                        .where((packing) =>
                            packing.packing_value ==
                            itemDetail.packingId.toString())
                        .toList()[0]
                        .packing_text;

                    /**
                     * Load unique item info with barcode based on selected packing, using the item id
                     * */
                    _findUniqueItemWithItemCodeAndPackingId();
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getPackingDescrField() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child:
                  Text(itemDetail.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getFocAndQualityFields() {
    return Column(
      children: [
        SizedBox(height: 5),
        IntrinsicHeight(
          child: Row(
            children: [
              Checkbox(
                value: itemDetail.isFoc,
                onChanged: (bool value) {
                  if (_itemCodeController.text.trim().isNotEmpty) {
                    /// Checks whether user has already selected the check box or not ( the item has been already added as a foc item)
                    if (itemDetail.isFoc) {
                      if (itemDetail.hasParent) {
                        /// show foc parent item list pop up to edit
                        showFOCItemDialogue();
                      } else {
                        setState(() {
                          itemDetail.isFoc = !itemDetail.isFoc;
                          focDetailList.clear();
                        });
                      }
                    } else {
                      /// show foc parent item list pop up to select
                      showFOCItemDialogue();
                    }
                  } else {
                    showMessage(
                        context: context,
                        message: "Please enter Item Code/ Item Barcode");
                  }
                },
              ),
              Text("Is FOC Item?"),
              if ((widget.type == TransactionType.GRN))
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: VerticalDivider(
                      width: 1.0, color: BCAppTheme().boarderColor),
                ),
              Switch(
                value: itemDetail.isItemQualityChecked ?? false,
                activeColor: BCAppTheme().headingTextColor,
                onChanged: (value) {
                  setState(() {
                    itemDetail.isItemQualityChecked = value;
                  });
                },
              ),
              Text("Quality Check"),
            ],
          ),
        ),
      ],
    );
  }

  _getBatchNoField(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: _shouldValidateBatchNo(),
              isValidated: _isBatchNoValid()),
          child: SearchableDropdown.single(
            value: itemDetail.batchNo,
            items: (itemBatchDataList != null && itemBatchDataList.isNotEmpty)
                ? itemBatchDataList.map((ItemBatchData item) {
                    return DropdownMenuItem(
                        child: Text(item.batch_text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.batch_text);
                  }).toList()
                : [],
            hint: "Select Batch No",
            searchHint: "Select Batch No",
            readOnly: shouldDisableBatchNo,
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  itemDetail.batchNo = value;
                  _loadGrnNos();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        )
      ],
    );
  }

  _getQuantityFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateQuantity,
                isValidated: _isQuantityValid(),
                hintText: 'Quantity'),
            keyboardType: TextInputType.number,
            onChanged: _updateQuantity,
          ),
        )
      ],
    );
  }

  _getItemDetailsFields(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  decoration:
                      Common().getBCoreSD(isMandatory: true, isValidated: true),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text("Item details of ${itemDetail.itemCode ?? "-"}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().headingTextColor)),
                        if (containItemBasicInfo) _getItemBasicInfoFields(),
                        if (containItemPricing &&
                            widget.type == TransactionType.GRN)
                          _getItemPricingFields(),
                        if (containItemPricing &&
                            widget.type == TransactionType.PR)
                          _getItemPricingFields(isEditable: false),
                        if (containItemExpiryDate) _getItemExpiryDateFields(),
                        if (containBatchNoField) _getBachNoField(),
                        if (containItemLocation)
                          _getItemLocationField(blocContext),
                        if (containInstallationRequired)
                          _getInstallationRequiredField(),
                        if (containSerialNo) _getSerialNoListField()
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: FlatButton(
                    padding: EdgeInsets.zero,
                    color: BCAppTheme().headingTextColor,
                    onPressed: () {
                      /// Validate and save item details
                      if (_validateItemDetail()) {
                        saveItemDetails();
                      }
                    },
                    child: Icon(
                      Icons.save,
                      size: 18,
                      color: Colors.white,
                    )),
              )
            ],
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }

  _getGrnNoField() {
    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: shouldValidateGrnNo,
              isValidated: (itemDetail.gRNHeaderId != null)),
          child: SearchableDropdown.single(
            value: _getSelectedGrnValue(),
            items: (grnNoItemsList != null && grnNoItemsList.isNotEmpty)
                ? grnNoItemsList.map((grnNo.MasterDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            hint: "Select GRN No",
            searchHint: "Select GRN No",
            onChanged: (value) {
              if (value != null) {
                var selectedGrn = grnNoItemsList
                    .where((item) => item.text == value)
                    .toList()[0];
                setState(() {
                  itemDetail.gRNHeaderId = int.parse(selectedGrn.value);
                  _bindGrnNos();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getSelectedGrnValue() {
    if (itemDetail.gRNHeaderId != null) {
      var selectedGrn = grnNoItemsList
          .where((item) => item.value == itemDetail.gRNHeaderId.toString())
          .toList()[0];
      return selectedGrn.text;
    } else {
      return null;
    }
  }

  _getItemBasicInfoFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.scatter_plot, size: 10),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(
                        itemDetail.brandName != null
                            ? itemDetail.brandName
                            : "--",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 13.0),
                        overflow: TextOverflow.ellipsis,
                        softWrap: false),
                  ),
                ],
              )),
              SizedBox(width: 10),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.public, size: 12),
                  SizedBox(width: 4.0),
                  Flexible(
                    child: Text(
                      itemDetail.countryName != null
                          ? itemDetail.countryName
                          : "--",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 13.0),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ))
            ],
          ),
        ),
      ],
    );
  }

  _getItemPricingFields({isEditable = true}) {
    Widget netIcon;

    if (isEditable) {
      netIcon = Icon(Icons.edit, size: 15, color: BCAppTheme().primaryColor);
    } else {
      netIcon = ImageIcon(
        AssetImage('assets/common/money_icon/money_icon.png'),
        size: 15.0,
      );
    }

    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            print("Price Tapped");

            if (widget.type == TransactionType.PR &&
                cmnSupplierInfo.pRetTypeId == PRType.GRN.index + 1) {
              return;
            } else {
              if (itemDetail.itemId != null) {
                itemPrice.enableEditMode();
                _unitPriceController.text = itemPrice.unitPrice.toString();
                _exciseDutyController.text = itemPrice.exciseDuty.toString();
                _discountPercentController.text =
                    itemPrice.discPercentage.toString();
                _discountAmountController.text = itemPrice.discount.toString();
                _taxAmountController.text = itemPrice.taxAmount
                    .toString()
                    .trim()
                    .toDouble(pos: getDeci())
                    .toString();
                showItemPriceDialogue(isEditable: isEditable);
              } else {
                Common().showMessage(
                    context: context,
                    message: "Please enter Item Barcode/ Item Code");
              }
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration:
                Common().getBCoreSD(isMandatory: true, isValidated: true),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Unit",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                    ),
                    _getUnitPriceWidget() //TODO Bind item unit cos
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Net",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                    ),
                    _getNetPriceWidget()
                  ],
                )),
                netIcon
                // Icon(isEditable ? Icons.edit : Icons.attach_money,
                //     size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        ),
      ],
    );
  }

  _getUnitPriceWidget() {
//    if (_unitPriceController.text.trim().isNotEmpty){
//      return Text(_unitPriceController.text.trim(),
//          style: TextStyle(fontSize: 13.0));
//    }
    if (itemDetail.isFoc) {
      return Text("0.0", style: TextStyle(fontSize: 13.0));
    } else if (itemPrice.unitPrice != null) {
      return Text(_unitPriceController.text, style: TextStyle(fontSize: 13.0));
    }
    // else {
    //   return Text(
    //       (itemBarcodeData.AverageCost == null) ? '0.0' : itemBarcodeData.AverageCost,
    //       style: TextStyle(fontSize: 13.0));
    // }
  }

  _getNetPriceWidget() {
//    if (_netAmountController.text.trim().isNotEmpty){
//      return Text(_netAmountController.text.trim(),
//          style: TextStyle(fontSize: 13.0));
//    } else {

    if (itemDetail.isFoc) {
      return Text("0.0", style: TextStyle(fontSize: 13.0));
    } else if (itemPrice.getNetAmount() != null) {
      return Text(itemPrice.getNetAmount().trim().toString(),
          style: TextStyle(fontSize: 13.0));
    } else {
      return Text((itemDetail.netCost == null) ? '0.0' : itemDetail.netCost,
          style: TextStyle(fontSize: 13.0));
    }
  }

  _getItemExpiryDateFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: DateFormField(
            format: fromAddress.fromCompanyDateFormat,
            showPicker: showItemDatePicker,
            initialValue: itemDetail.expiryDateTemp,
            onDateChanged: (DateTime date) {
              setState(() {
                itemDetail.expiryDateTemp = Utils.getDateByFormat(
                    format: fromAddress.fromCompanyDateFormat,
                    passedDateTime: date);
                print("Date : ${itemDetail.expiryDateTemp}");
              });
            },
            decoration: Common().getBCoreMandatoryID(
                hintText: 'Expiry Date',
                icon: Icons.calendar_today,
                isMandatory: _shouldValidateExpDate(),
                isValidated: _validateExpDate()),
          ),
        )
      ],
    );
  }

  _getBachNoField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _batchNoController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                hintText: 'Batch No',
                isMandatory: _shouldValidateBatchNo(),
                isValidated: _isBatchNoValid()),
            keyboardType: TextInputType.text,
            onChanged: _updatedBatchNo,
          ),
        ),
      ],
    );
  }

  _getItemLocationField(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            if (wareHouseList.isEmpty) {
              _loadWareHouse();
            } else {
              showItemLocationDialogue(context);
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration:
                Common().getBCoreSD(isMandatory: true, isValidated: true),
            child: Row(
              children: [
                Expanded(
                    child: Text("Item Location",
                        style: TextStyle(color: BCAppTheme().grayColor))),
                SizedBox(width: 8),
                Icon(Icons.edit, size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        )
      ],
    );
  }

  _getInstallationRequiredField() {
    return Row(
      children: [
        Checkbox(
          value: itemDetail.isInstallationChecked,
          onChanged: (bool value) {
            setState(() {
              itemDetail.isInstallationChecked = value;
              // _getOptedInstallation();
            });
          },
        ),
        Text("Installation Required?"),
      ],
    );
  }

  _validateQuantityField() {
    if (_quantityController.text != null && _quantityController.text != '') {
      if (int.tryParse(_quantityController.text) != null) {
        return true;
      } else {
        Common()
            .showMessage(context: buildContext, message: 'Invalid Quantity');
        return false;
      }
    } else {
      Common().showMessage(
          context: buildContext, message: 'Please enter Item Quantity First');
      return false;
    }
  }

  _getSerialNoListField() {
    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            if (_validateQuantityField()) {
              showSerialNoDialogue();
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration: Common().getBCoreSD(
                isMandatory: true,
                isValidated: (int.tryParse(_quantityController.text) ?? 0) ==
                    serialNList.length),
            child: Row(
              children: [
                Expanded(
                    child: Text("Serial No Details",
                        style: TextStyle(color: BCAppTheme().grayColor))),
                SizedBox(width: 8),
                Icon(Icons.edit, size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        )
      ],
    );
  }

  OutlineInputBorder ob = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
    borderSide: BorderSide(color: BCAppTheme().headingTextColor),
  );
  List<String> serialNList = [];

  showSerialNoDialogue() {
    String serialNo;
    List<String> serialNListTemp = [...serialNList]; // for edit purpose

    Dialog chequeDetailsAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: StatefulBuilder(
        builder: (BuildContext context, StateSetter dialogState) {
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Serial No Details", textAlign: TextAlign.center),
                SizedBox(height: 16.0),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 45,
                      child: TextFormField(
                        enableSuggestions: false,
                        textInputAction: TextInputAction.done,
                        controller: TextEditingController(text: serialNo),
                        autocorrect: false,
                        decoration: InputDecoration(
                          isDense: true,
                          border: OutlineInputBorder(),
                          hintText: 'Enter Serial No',
                          hintStyle: TextStyle(
                              color: BCAppTheme().grayColor, fontSize: 14),
                          filled: true,
                          fillColor: Colors.white70,
                          suffixIcon: IconButton(
                              icon: Icon(Icons.add,
                                  color: BCAppTheme().headingTextColor),
                              onPressed: () {
                                if (serialNo != null && serialNo != '') {
                                  dialogState(() {
                                    serialNListTemp.add(serialNo);
                                    serialNo = '';
                                  });
                                } else {
                                  Common().showMessage(
                                      context: context,
                                      message: 'Please enter Serial No');
                                }
                              }),
                          enabledBorder: ob,
                          focusedBorder: ob,
                        ),
                        keyboardType: TextInputType.name,
                        onChanged: (value) {
                          serialNo = value;
                        },
                        validator: (value) {
                          if (serialNo != null && serialNo != '') {
                            dialogState(() {
                              serialNListTemp.add(serialNo);
                              serialNo = '';
                            });
                          } else {
                            Common().showMessage(
                                context: buildContext,
                                message: 'Please enter Serial No');
                          }
                          return null;
                        },
                        onFieldSubmitted: (value) {
                          if (serialNo != null && serialNo != '') {
                            dialogState(() {
                              serialNListTemp.add(serialNo);
                              serialNo = '';
                            });
                          } else {
                            Common().showMessage(
                                context: buildContext,
                                message: 'Please enter Serial No');
                          }
                        },
                      ),
                    ),
                    SizedBox(height: 8),
                    Container(
                      height: 150.0,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: serialNListTemp.length,
                        itemBuilder: (context, index) {
                          return Card(
                            child: ListTile(
                                title: Text(serialNListTemp[index]),
                                trailing: CloseButton(
                                    color: Colors.red,
                                    onPressed: () {
                                      dialogState(() {
                                        serialNListTemp.removeAt(index);
                                      });
                                    })),
                          );
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 35,
                        decoration: Common()
                            .getBCoreSD(isMandatory: false, isValidated: false),
                        child: FlatButton(
                            child: Text("Cancel",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 11)),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                      ),
                    ),
                    SizedBox(width: 16.0),
                    Expanded(
                      child: Container(
                        height: 35,
                        child: FlatButton(
                            color: BCAppTheme().primaryColor,
                            child: Text("Save",
                                style: TextStyle(
                                    color: BCAppTheme().secondaryColor,
                                    fontSize: 11)),
                            onPressed: () {
                              if (serialNListTemp.length ==
                                  int.parse(_quantityController.text)) {
                                Navigator.pop(context);
                                setState(() {
                                  serialNList = [...serialNListTemp];
                                });
                              } else {
                                Common().showMessage(
                                    context: buildContext,
                                    message:
                                        'Item Quantity & Serial No Count dosn\'t match');
                              }
                            }),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        },
      ),
    );

    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext myContext) {
        return chequeDetailsAlert;
      },
    );
  }

  /// Validation methods

  _isQuantityValid() {
    if (_quantityController.text.isNotEmpty && isQuantityValid) {
      return true;
    } else {
      return false;
    }
  }

  _shouldValidateBatchNo() {
    if (itemDetail.isBatchEnabled != null) {
      if (itemDetail.isBatchEnabled.toBool() &&
          (itemDetail.batchNo == null || itemDetail.batchNo.isEmpty)) {
        return true;
      }
      return false;
    } else {
      return false;
    }
    // } else {
    //   if (itemDetail.isBatchEnabled != null || itemDetail.isBatchEnabled != null) {
    //     if (itemDetail.isBatchEnabled.toBool() &&
    //         (itemDetail.batchNo == null || itemDetail.batchNo.isEmpty)) {
    //       return true;
    //     }
    //     return false;
    //   } else {
    //     return false;
    //   }
    // }
  }

  _isBatchNoValid() {
    if (itemDetail.isBatchEnabled != null) {
      if (itemDetail.isBatchEnabled.toBool() &&
          (itemDetail.batchNo == null || itemDetail.batchNo.isEmpty)) {
        return false;
      }
      return true;
    } else {
      return true;
    }
  }

  _bindGrnNos() {
    grnNo.MasterDataList selectedGRN = grnNoItemsList
        .where(
            (grnNoItem) => grnNoItem.value == itemDetail.gRNHeaderId.toString())
        .toList()[0];
    itemPrice.unitPrice = selectedGRN.unitPrice;
    itemDetail.gRNQty = selectedGRN.gRNQty;
    setPriceValues(cmnUOMQTY: itemDetail.uOMQty);
    updatePrices();
  }

  _shouldValidateExpDate() {
    if (widget.type == TransactionType.GRN) {
      if (containItemExpiryDate) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  _validateExpDate() {
    if (widget.type == TransactionType.GRN) {
      if (containItemExpiryDate && itemDetail.expiryDateTemp.isEmpty) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  // Pop ups

  showFOCItemDialogue() {
    Dialog focAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(child: StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Select FOC Details", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 45,
                  child: TextField(
                    onChanged: (value) {
                      dropDownState(() {});
                    },
                    controller: _searchController,
                    decoration: Common().getBCoreMandatoryID(
                        isMandatory: false,
                        isValidated: true,
                        hintText: 'Search',
                        icon: Icons.search),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: focParentItems.length,
                    itemBuilder: (context, index) {
                      if (_searchController.text.isEmpty) {
                        return CheckboxListTile(
                          contentPadding: EdgeInsets.zero,
                          title: Text(focParentItems[index].itemName ?? ""),
                          value: selectedFocParentItems
                              .where((element) =>
                                  element.itemId ==
                                  focParentItems[index].itemId)
                              .isNotEmpty,

                          /// Checks whether the parent item is in selected parent item list
                          controlAffinity: ListTileControlAffinity.leading,
                          onChanged: (bool val) {
                            dropDownState(() {
                              CommonHighItem selectedParentItem =
                                  focParentItems[index];

                              if (val) {
                                /// adds the parent item to the selected parent item list
                                selectedFocParentItems.add(selectedParentItem);

                                /// adds the current item to the foc details list
                                focDetailList.add(FOCDetails(
                                  fOCItemLineNo: index,
                                  fOCItemId: itemDetail.itemId,
                                  parentItemCode: selectedParentItem.itemCode,
                                  gRNItemLineNo: 0,
                                  parentItemId: selectedParentItem.itemId,
                                  parentIndex: selectedParentItem.index,
                                ));
                              } else {
                                /// Removes the parent item from the selected parent item list
                                selectedFocParentItems.removeWhere((element) =>
                                    element.itemId ==
                                    selectedParentItem.itemId);

                                /// Removes the current item from the foc details list
                                focDetailList.removeWhere((element) =>
                                    element.parentItemId ==
                                    selectedParentItem.itemId);
                              }
                            });
                          },
                        );
                      } else if (focParentItems[index]
                          .itemName
                          .toLowerCase()
                          .contains(_searchController.text.toLowerCase())) {
                        return CheckboxListTile(
                            contentPadding: EdgeInsets.zero,
                            title: Text(focParentItems[index].itemName ?? ""),
                            value: selectedFocParentItems
                                .where((element) =>
                                    element.itemId ==
                                    focParentItems[index].itemId)
                                .isNotEmpty,
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (bool val) {
                              dropDownState(() {
                                CommonHighItem selectedParentItem =
                                    focParentItems[index];

                                if (val) {
                                  /// adds the parent item to the selected parent item list
                                  selectedFocParentItems
                                      .add(selectedParentItem);

                                  /// adds the current item to the foc details list
                                  focDetailList.add(FOCDetails(
                                    fOCItemLineNo: index,
                                    fOCItemId: itemDetail.itemId,
                                    parentItemCode: selectedParentItem.itemCode,
                                    gRNItemLineNo: 0,
                                    parentItemId: selectedParentItem.itemId,
                                    parentIndex: selectedParentItem.index,
                                  ));
                                } else {
                                  /// Removes the parent item from the selected parent item list
                                  selectedFocParentItems.removeWhere(
                                      (element) =>
                                          element.itemId ==
                                          selectedParentItem.itemId);

                                  /// Removes the current item from the foc details list
                                  focDetailList.removeWhere((element) =>
                                      element.parentItemId ==
                                      selectedParentItem.itemId);
                                }
                              });
                            });
                      } else {
                        return Container();
                      }
                    }),
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 35,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 0.5,
                              style: BorderStyle.solid,
                              color: BCAppTheme().primaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: FlatButton(
                          child: Text("Cancel",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Container(
                      height: 35,
                      child: FlatButton(
                          color: BCAppTheme().primaryColor,
                          child: Text("Save",
                              style: TextStyle(
                                  color: BCAppTheme().secondaryColor,
                                  fontSize: 11)),
                          onPressed: () {
                            Navigator.pop(context);

                            setState(() {
                              /// Checks whether the item has selected a parent item or not
                              if (focDetailList.isEmpty) {
                                if (itemDetail.hasParent) {
                                  itemDetail.hasParent = false;

                                  /// Change selection
                                  itemDetail.isFoc = !itemDetail.isFoc;
                                } else {
                                  /// for foc items without parent items
                                  focDetailList.add(FOCDetails(
                                    fOCItemLineNo: 0,
                                    fOCItemId: itemDetail.itemId,
                                    parentItemCode: itemDetail.itemCode,
                                    gRNItemLineNo: 0,
                                    parentItemId: itemDetail.itemId,
                                    parentIndex: null,
                                  ));

                                  /// Change selection
                                  itemDetail.isFoc = !itemDetail.isFoc;
                                }
                              } else {
                                itemDetail.hasParent = true;

                                /// Change selection
                                itemDetail.isFoc = !itemDetail.isFoc;
                              }
                            });
                          }),
                    ),
                  )
                ],
              )
            ],
          );
        })),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return focAlert;
      },
    );
  }

  showItemPriceDialogue({isEditable = true}) {
    bool shouldEnableUnitPrice = (widget.type == TransactionType.GRN &&
            cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) ??
        false;

    Dialog itemPriceAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            children: [
              Text("Item Pricing", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _unitPriceController,
                          autocorrect: false,
                          enabled: (GrnReq.shared.gRNTypeId ==
                                      GRNType.Direct.index + 1 &&
                                  itemDetail.suppContractPriceStatus)
                              ? false
                              : shouldEnableUnitPrice,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Unit Price',
//                            hintText: 'Unit Price',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: _updateUnitPrice,
                        ),
                      ),
                      // SizedBox(height: 8),
                      // SizedBox(
                      //   height: 45,
                      //   child: TextField(
                      //     enableSuggestions: false,
                      //     textInputAction: TextInputAction.next,
                      //     controller: _exciseDutyController,
                      //     autocorrect: false,
                      //     enabled: isEditable,
                      //     decoration: InputDecoration(
                      //       contentPadding: EdgeInsets.only(
                      //           left: 12, right: 8, bottom: 8, top: 0),
                      //       labelText: 'Excise Duty',
                      //       hintStyle: TextStyle(color: Colors.grey),
                      //       filled: true,
                      //       fillColor: Colors.white70,
                      //       enabledBorder: border,
                      //       focusedBorder: border,
                      //     ),
                      //     keyboardType: TextInputType.number,
                      //     onChanged: _updateExciseDuty,
                      //   ),
                      // ),
                      SizedBox(height: 8),
                      Container(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _actualPriceController,
                          autocorrect: false,
                          enabled: false,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Actual Price',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _discountPercentController,
                          autocorrect: false,
                          enabled: isEditable,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Discount %',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: _updateDiscountPercent,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _discountAmountController,
                          autocorrect: false,
                          enabled: isEditable,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Discount Amount',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: _updateDiscountAmount,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _taxAmountController,
                          autocorrect: false,
                          enabled: false,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Tax Amount',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: _updateTaxAmount,
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 45,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.done,
                          controller: _netAmountController,
                          autocorrect: false,
                          enabled: false,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 12, right: 8, bottom: 8, top: 0),
                            labelText: 'Net Amount',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: border,
                            focusedBorder: border,
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: _updateNetAmount,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 35,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 0.5,
                              style: BorderStyle.solid,
                              color: BCAppTheme().primaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: FlatButton(
//                          color: BCAppTheme().secondaryColor,
                          child: Text("Cancel",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                          onPressed: () {
                            itemPrice.disableEditMode();
                            Navigator.pop(context);
                          }),
                    ),
                  ),
                  if (isEditable) SizedBox(width: 16.0),
                  if (isEditable)
                    Expanded(
                      child: Container(
                        height: 35,
                        child: FlatButton(
                            color: BCAppTheme().primaryColor,
                            child: Text("Save",
                                style: TextStyle(
                                    color: BCAppTheme().secondaryColor,
                                    fontSize: 11)),
                            onPressed: () {
                              Navigator.pop(context);
                              if (_validateItemPriceDetails()) {
                                setState(() {
                                  itemPrice.updatePrices();
                                  itemPrice.disableEditMode();
                                });
                              } else {
                                // Show item price fields selection message
                              }
                            }),
                      ),
                    )
                ],
              )
            ],
          ),
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return itemPriceAlert;
      },
    );
  }

  Future<DateTime> showItemDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Expiry Date",
      initialDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: itemDetail.minShelfLife))
          : DateTime.now(),
      firstDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: itemDetail.minShelfLife))
          : DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 36500)),
    );
    return date;
  }

  showItemLocationDialogue(BuildContext blocContext) {
    Dialog locationAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Item Location", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              // Expanded(
              //   child:
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child:
                    // Expanded(
                    //   child:
                    StatefulBuilder(builder:
                        (BuildContext context, StateSetter dropDownState) {
                  return Column(
                    children: [
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common()
                              .getBCoreSD(isMandatory: true, isValidated: true),
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Warehouse"),
                                value: itemDetail.warehouseId == null
                                    ? null
                                    : itemDetail.warehouseId.toString(),
                                items: wareHouseList
                                    .map((cmn.CommonDataList wareHouse) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouse.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouse.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    if (value != null) {
                                      itemDetail.warehouseId = int.parse(value);
                                      _loadZone();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common()
                              .getBCoreSD(isMandatory: true, isValidated: true),
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Zone"),
                                value: itemDetail.wHouseZoneId == null
                                    ? null
                                    : itemDetail.wHouseZoneId.toString(),
                                items: wareHouseZoneList
                                    .map((cmn.CommonDataList wareHouseZone) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseZone.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseZone.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    if (value != null) {
                                      itemDetail.wHouseZoneId =
                                          int.parse(value);
                                      _loadRack();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common()
                              .getBCoreSD(isMandatory: true, isValidated: true),
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Rack"),
                                value: itemDetail.wHouseZoneBinRackId == null
                                    ? null
                                    : itemDetail.wHouseZoneBinRackId.toString(),
                                items: wareHouseRackList
                                    .map((cmn.CommonDataList wareHouseRack) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseRack.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseRack.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    if (value != null) {
                                      itemDetail.wHouseZoneBinRackId =
                                          int.parse(value);
                                      _loadBin();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common()
                              .getBCoreSD(isMandatory: true, isValidated: true),
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Bin"),
                                value: itemDetail.wHouseZoneBinId == null
                                    ? null
                                    : itemDetail.wHouseZoneBinId.toString(),
                                items: wareHouseBinList
                                    .map((WarehouseBinList wareHouseBin) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseBin.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseBin.value);
                                }).toList(),
                                onChanged: (value) {
                                  if (value != null) {
                                    dropDownState(() {
                                      itemDetail.wHouseZoneBinId =
                                          int.parse(value);
                                    });
                                  }
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 35,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      width: 0.5,
                                      style: BorderStyle.solid,
                                      color: BCAppTheme().primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                ),
                              ),
                              child: FlatButton(
                                  child: Text("Cancel",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                            ),
                          ),
                          SizedBox(width: 16.0),
                          Expanded(
                            child: Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Save",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    if (_validateItemLocation()) {
                                      setState(() {
//                                            Navigator.pop(context);
                                      });
                                    } else {}
                                  }),
                            ),
                          )
                        ],
                      )
                    ],
                  );
                }),
                // ),
              ),
              // )
            ],
          ),
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return locationAlert;
      },
    );
  }

  _updatedBatchNo(String val) {
    setState(() {
      itemDetail.batchNo = _batchNoController.text.trim().isEmpty
          ? ""
          : _batchNoController.text.trim();
    });
  }

  void _updateQuantity(String val) {
    setState(() {
      if (editMode == EditMode.On) {
        if (widget.type == TransactionType.GRN) {
          if (itemDetail.isFoc) {
            alert = AlertDialog(
              content: Text(
                "Do you want to remove the FOC Item?",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
              actions: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          child: Text(
                            'Delete',
                            style: TextStyle(fontSize: 9, color: Colors.black),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            GrnReq.shared.itemDetails.removeAt(itemIndex);
                          }),
                    ),
                    ElevatedButton(
                        child: Text(
                          'Cancel',
                          style: TextStyle(fontSize: 9, color: Colors.black),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                          itemDetail.isFoc = true;
                          if (itemDetail.isFoc == true &&
                              editMode == EditMode.On &&
                              CommonHighItemInfo.shared.cmnHighItemList
                                  .where((item) =>
                                      item.itemBarcode ==
                                      itemDetail.itemBarcode)
                                  .toList()
                                  .isNotEmpty) {
                            showAlert(
                                context: context,
                                message: 'Item already added');
                            return false;
                          }
                          itemDetail.unitQty = _quantityController.text.isEmpty
                              ? 1.0
                              : double.parse(_quantityController.text.trim());
                          itemPrice.quantity =
                              int.parse(_quantityController.text).toDouble();
                          itemPrice.quantity =
                              _quantityController.text.trim().length > 0
                                  ? _quantityController.text
                                      .trim()
                                      .toDouble(pos: getDeci())
                                  : 1.0;
                        }),
                  ],
                ),
              ],
            );
            showDialog(
              context: context,
              builder: (BuildContext myContext) {
                return alert;
              },
            );
          } else {
            isQuantityValid = true;
            itemDetail.unitQty = _quantityController.text.isEmpty
                ? 1.0
                : double.parse(_quantityController.text.trim());
            itemPrice.quantity = _quantityController.text.trim().length > 0
                ? _quantityController.text.trim().toDouble(pos: getDeci())
                : 1.0;
            updatePrices();
          }
        }
      }
      isQuantityValid = true;
      itemDetail.unitQty = _quantityController.text.isEmpty
          ? 1.0
          : double.parse(_quantityController.text.trim());
      itemPrice.quantity = _quantityController.text.trim().length > 0
          ? _quantityController.text.trim().toDouble(pos: getDeci())
          : 1.0;
      updatePrices();

//    if (_quantityController.text.isEmpty) {
//      if (_itemCodeController.text.trim().length > 0) {
//        _quantityController.text = "1";
//      } else {
//        _quantityController.text = "";
//      }
//    }
    });
  }

  void _updateUnitPrice(String value) {
    setState(() {
      itemPrice.tempUnitPrice = _unitPriceController.text.isEmpty
          ? 0.0
          : _unitPriceController.text.trim().toDouble(pos: getDeci());
      updatePrices();
    });
  }

  void _updateExciseDuty(String val) {
    setState(() {
      itemPrice.tempExciseDuty = _exciseDutyController.text.isEmpty
          ? 0.0
          : _exciseDutyController.text.trim().toDouble(pos: getDeci());
      updatePrices();
    });
  }

  void _updateDiscountPercent(String value) {
    setState(() {
      itemPrice.tempDiscPercentage =
          _discountPercentController.text.trim().length > 0
              ? _discountPercentController.text.trim().toDouble(pos: getDeci())
              : 0.0;
      _discountAmountController.text = itemPrice.getDiscount().toString();
      updatePrices();
    });
  }

  void _updateDiscountAmount(String value) {
    setState(() {
      itemPrice.tempDiscount = _discountAmountController.text.trim().length > 0
          ? _discountAmountController.text.trim().toDouble(pos: getDeci())
          : 0.0;
      _discountPercentController.text =
          itemPrice.getDiscPercentage().toString();
      updatePrices();
    });
  }

  void _updateTaxAmount(String value) {
    setState(() {
      itemPrice.tempTaxAmount = _taxAmountController.text.trim().length > 0
          ? _taxAmountController.text.trim().toDouble(pos: getDeci())
          : 0.0;
      updatePrices();
    });
  }

  void _updateNetAmount(String value) {
    setState(() {
      itemPrice.netAmount = _netAmountController.text.trim().length > 0
          ? _netAmountController.text.trim().toDouble(pos: getDeci())
          : 0.0;
    });
  }

  bool _validateItemDetail() {
    if (widget.type != TransactionType.PR && itemDetail.batchNo == null) {
      itemDetail.batchNo = "";
      // In GRN we haven't given a batch no field. But in web app there
      // is a batch no field and on a specific scenario it is mandatory.
      // That is why we have set static value// TODO
    }

    if (_itemCodeController.text.isEmpty) {
      showMessage(
          context: context, message: "Please enter Item code/ Item Barcode");
      return false;
    }

    /// Check whether already the item is added or not
    if (itemDetail.isFoc == false &&
        editMode == EditMode.Off &&
        CommonHighItemInfo.shared.cmnHighItemList
            .where((item) => item.itemBarcode == itemDetail.itemBarcode)
            .toList()
            .isNotEmpty) {
      showAlert(context: context, message: 'Item already added');
      return false;
    }

    if (itemDetail.packingId != null && itemDetail.packingId == 0) {
      showMessage(context: context, message: "Please select item package");
      return false;
    }

    if (_quantityController.text.trim().isEmpty ||
        double.tryParse(_quantityController.text.trim()) == 0.0 ||
        int.tryParse(_quantityController.text.trim()) == 0) {
      showMessage(context: context, message: "Please enter quantity");
      quantityFocusNode.requestFocus();
      return false;
    }

    if (widget.type == TransactionType.GRN) {
      if (cmnSupplierInfo.gRNTypeId != GRNType.Direct.index + 1 &&
          cmnSupplierInfo.deliveryTypeId == DeliveryType.Full.index + 1 &&
          (tempTotalUOMQty / itemDetail.uOMQty !=
              double.tryParse(_quantityController.text.trim()))) {
        showMessage(
            context: context,
            message:
                "Not matching with ${GRNType.values[cmnSupplierInfo.gRNTypeId - 1].value} quantity");
        isQuantityValid = false;
        return false;
      }
      if (cmnSupplierInfo.gRNTypeId != GRNType.Direct.index + 1 &&
          cmnSupplierInfo.deliveryTypeId == DeliveryType.Partial.index + 1 &&
          (tempTotalUOMQty / itemDetail.uOMQty <
              double.tryParse(_quantityController.text.trim()))) {
        showMessage(
            context: context,
            message:
                "Entered quantity is greater than ${GRNType.values[cmnSupplierInfo.gRNTypeId - 1].value} quantity");
        isQuantityValid = false;
        return false;
      }
      // if (isFoc &&
      //     selectedFocParentItems.length == 0 &&
      //     widget.type == TransactionType.GRN) {
      //   showMessage(context: context, message: "Please select FOC details");
      //   return false;
      // }
      if (containItemExpiryDate && itemDetail.expiryDateTemp.isEmpty) {
        showMessage(context: context, message: "Please enter expiry date");
        return false;
      }

      /// For GRN
      if (containSerialNo &&
          (int.tryParse(_quantityController.text.trim()) == null)) {
        showMessage(context: context, message: "Invalid quantity");
        return false;
      }

      /// For GRN
      if (containSerialNo &&
          (int.tryParse(_quantityController.text.trim()) !=
              serialNList.length)) {
        showMessage(context: context, message: "Please enter Serial number(s)");
        return false;
      }
    }
    if (widget.type == TransactionType.PR) {
      if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1 &&
          itemDetail.gRNHeaderId == null) {
        showMessage(context: context, message: "Please select GRN No");
        return false;
      }
      if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1) {
        if (itemDetail.unitPrice == 0.0) {
          showMessage(
              context: context,
              message: "Can't save an item with Unit price 0");
          return false;
        }
        // if (itemDetail.gRNQty <
        //     (int.tryParse(_quantityController.text.trim()) ??
        //         double.tryParse(_quantityController.text.trim()))) {
        //   showAlert(
        //       context: context,
        //       message:
        //       "Quantity can't be greater than GRN quantity(${itemDetail.gRNQty})");
        //   setState(() {
        //     isQuantityValid = false;
        //   });
        //   return false;
        // }
      } else {
        /// as per the discussion with api team we are not editing the price in PR, so no need of this validation
        // if (itemDetail.unitPrice == 0.0) {
        //   showMessage(
        //       context: context,
        //       message: "Can't save an item with Unit price 0");
        //   return false;
        // }
        if (itemDetail.gRNQty <
            (int.tryParse(_quantityController.text.trim()) ??
                double.tryParse(_quantityController.text.trim()))) {
          showAlert(
              context: context,
              message:
                  "Quantity can't be greater than GRN quantity(${itemDetail.gRNQty})");
          setState(() {
            isQuantityValid = false;
          });
          return false;
        }
      }
    }

    if (itemDetail.isBatchEnabled.toBool() &&
        (itemDetail.batchNo == null || itemDetail.batchNo.isEmpty)) {
      showMessage(context: context, message: "Please enter Batch No");
      return false;
    }
    return true;
  }

  void saveItemDetails() {
    if (itemDetail.itemId != null) {
      if (widget.type == TransactionType.GRN) {
        itemDetail.discPercent =
            (itemDetail.isFoc) ? 0.0 : itemPrice.getDiscPercentage();
        itemDetail.exciseDuty = (itemDetail.isFoc) ? 0.0 : itemPrice.exciseDuty;
        itemDetail.productionDateTemp = "";
        itemDetail.unitQty = itemPrice.quantity ?? 1.0;
        if (containSerialNo) {
          itemDetail.serialNoList = [...serialNList];
        }
      }
      itemDetail.index = Utils.getUniqueId();
      itemDetail.totalCost = itemPrice.getNetAmount();
      itemDetail.projectId = int.tryParse(fromAddress.fromProjectId ?? '0');
      itemDetail.unitPrice = (itemDetail.isFoc) ? 0.0 : itemPrice.unitPrice;

      if (widget.type == TransactionType.PR) {
        if (cmnSupplierInfo.pRetTypeId != PRType.Direct.index + 1) {
          itemDetail.gRNHeaderId = cmnSupplierInfo.pRetTypeRefNo;
          // itemDetail.gRNHeaderId = itemDetail.gRNHeaderId ?? 0;
          // itemDetail.gRNQty = selectedGRN.gRNQty;
          // itemDetail.pkgName = selectedPackageName ?? "";
        } else {
          // itemDetail.gRNHeaderId = cmnSupplierInfo.pRetTypeRefNo;
          // itemDetail.gRNQty = itemDetail.gRNQty;
          // itemDetail.pkgName = itemDetail.pkgName;
        }
        itemDetail.reasonId = SavePRReq.shared.reasonId;
        itemDetail.returnedQty = itemPrice.quantity ?? 1.0;
      }

      if (editMode == EditMode.On) {
        if (widget.type == TransactionType.GRN && itemDetail.isFoc) {
          for (FOCDetails focItem in focDetailList) {
            focItem.focIndex = itemDetail.index;
            var indexOfFOCItemInExistingList = grn_foc_details.indexWhere(
                (item) =>
                    item.parentItemCode == focItem.parentItemCode &&
                    (item.focIndex == focItem.focIndex));
            if (indexOfFOCItemInExistingList != -1) {
              grn_foc_details.replaceRange(indexOfFOCItemInExistingList,
                  indexOfFOCItemInExistingList + 1, [focItem]);
            }
          }
        }

        /// Check whether already item is there
        var indexOfItemInExistingList = CommonHighItemInfo
            .shared.cmnHighItemList
            .indexWhere((item) => item.itemBarcode == itemDetail.itemBarcode);

        if (indexOfItemInExistingList != -1) {
          CommonHighItemInfo.shared.cmnHighItemList.replaceRange(
              indexOfItemInExistingList,
              indexOfItemInExistingList + 1,
              [CommonHighItem.fromJson(itemDetail.toJson())]);
          showAlert(context: context, message: 'Updated!');
          _didSelect(InputType.Barcode);
        } else {
          showAlert(context: context, message: 'Please try again');
        }

        if (widget.type == TransactionType.GRN && !itemDetail.isFoc) {
          var indexOfFocParentItemInExistingList = focParentItems
              .indexWhere((item) => item.index == itemDetail.index);
          if (indexOfFocParentItemInExistingList != -1) {
            focParentItems.replaceRange(indexOfFocParentItemInExistingList,
                indexOfFocParentItemInExistingList + 1, [itemDetail]);
          }
        }
      } else {
        if (widget.type == TransactionType.GRN && itemDetail.isFoc) {
          /// set index for foc items
          for (FOCDetails focDetail in focDetailList) {
            focDetail.focIndex = itemDetail.index;
            if (focDetail.parentIndex == null) {
              /// for foc items without parent items
              focDetail.parentIndex = itemDetail.index;
            }
          }
          grn_foc_details.addAll([...focDetailList]);
          focDetailList = [];
        }

        /// Add item in common item list to save
        CommonHighItemInfo.shared.cmnHighItemList
            .add(CommonHighItem.fromJson(itemDetail.toJson()));

        /// Add item in the parent itemList for foc selection
        if (widget.type == TransactionType.GRN && !itemDetail.isFoc) {
          focParentItems.add(itemDetail);
        }

        showAlert(context: context, message: 'Saved!');

        _didSelect(InputType.Barcode);
      }
//    itemCodeFocusNode.requestFocus();
    } else {
      showAlert(context: context, message: 'Enter proper item details');
      _didSelect(InputType.Barcode);
    }
  }

  void _setItemName() {
    _itemCodeController.text = itemDetail.itemName;
  }

  bool _validateItemPriceDetails() {
    if (this._unitPriceController.text.isEmpty) {
    } else if (_exciseDutyController.text.isEmpty) {
    } else if (_actualPriceController.text.isEmpty) {
    } else if (_discountPercentController.text.isEmpty) {
    } else if (_discountAmountController.text.isEmpty) {
//    } else if (_taxTypeController.text.isEmpty) {
    } else if (_taxAmountController.text.isEmpty) {
    } else if (_netAmountController.text.isEmpty) {
    } else {
      return true;
    }
    return false;
  }

  // bool _validateMinShelfLife() {
  //   return this._minShelfLifeController.text.isNotEmpty;
  // }

  bool _validateItemLocation() {
    return (itemDetail.warehouseId != null) ||
        (itemDetail.wHouseZoneId != null) ||
        (itemDetail.wHouseZoneBinId != null) ||
        (itemDetail.wHouseZoneBinRackId != null);
  }

  int getDeci() {
    if (type == TransactionType.GRN) {
      return int.parse(fromAddress.curDecimPlace);
    } else {
      return 2;
    }
  }

  setPriceValues({double cmnUOMQTY}) {
    /// when suppTypeId is 1, the supplier is 'local'. When suppTypeId is 2, the supplier is 'import'
    itemPrice.needToAddTax =
        (cmnSupplierInfo.suppTypeId == 1) ? cmnSupplierInfo.isTaxApp : false;
    itemPrice.needToCalculateTaxToDisplay =
        (cmnSupplierInfo.suppTypeId == 1) ? cmnSupplierInfo.isTaxApp : true;
    if (editMode == EditMode.Off) {
      if (widget.type == TransactionType.GRN &&
          cmnSupplierInfo.gRNTypeId != GRNType.Direct.index + 1 &&
          cmnSupplierInfo.deliveryTypeId == DeliveryType.Full.index + 1) {
      } else {
        _quantityController.text = "";
      }
    }
//    itemPrice.quantity = _quantityController.text.trim().toDouble(pos: getDeci());
    itemPrice.taxPercentage =
        (itemDetail.taxPercent ?? "0.0").toDouble(pos: getDeci());
    _exciseDutyController.text = "0";
    _discountPercentController.text = "0";
    _discountAmountController.text = "0";
    if (itemDetail.uOMQty == null) {
      _unitPriceController.text = itemPrice.unitPrice.trim().toString();
    } else {
      double a = itemPrice.refUnitPrice / tempUOMQty;
      setState(() {
        itemPrice.unitPrice = a * cmnUOMQTY;
        _unitPriceController.text = itemPrice.unitPrice.trim().toString();
      });
    }
  }

  updatePrices() {
    // itemPrice.needToAddTax = true;
    _actualPriceController.text = itemPrice.getActualPrice().toString();
    _taxAmountController.text = itemPrice
        .getTaxAmount()
        .toString()
        .trim()
        .toDouble(pos: getDeci())
        .toString();
    _netAmountController.text = itemPrice
        .getNetAmount()
        .toString()
        .trim()
        .toDouble(pos: getDeci())
        .toString();
    _unitPriceController.text = itemPrice.unitPrice.toString();
    // _unitPriceController.text =  itemPrice.unitPrice.toString();
    // _exciseDutyController.text = itemPrice.exciseDuty.toString();
    // _discountPercentController.text = itemPrice.discPercentage.toString();
    // _discountAmountController.text = itemPrice.discount.toString();
  }

  /// Gets the item details

  _getItemDetails(BuildContext context) {
    if (widget.type == TransactionType.GRN) {
      /// If the GRN type is Direct
      if (cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) {
        _loadItem();
      }

      /// If the GRN type is LPO or IPO
      else {
        var grnRefItem;

        /// If item barcode or item code entered by user, get item details
        if (_itemInputType == InputType.ItemCode &&
            _itemCodeController.text.isNotEmpty) {
          grnRefItem = grnRefItemList
              .where((item) => item.itemCode == _itemCodeController.text.trim())
              .toList();
        } else if (_itemInputType == InputType.Barcode &&
            _itemCodeController.text.isNotEmpty) {
          grnRefItem = grnRefItemList
              .where(
                  (item) => item.itemBarcode == _itemCodeController.text.trim())
              .toList();
        }
        if (grnRefItem.isNotEmpty) {
          /// Check whether already the item is added or not
          if (_itemInputType == InputType.Barcode &&
              editMode == EditMode.Off &&
              CommonHighItemInfo.shared.cmnHighItemList
                  .where(
                      (item) => item.itemBarcode == grnRefItem[0].itemBarcode)
                  .toList()
                  .isNotEmpty) {
            showAlert(context: context, message: 'Item already added');
            _didSelect(InputType.Barcode);
            return;
          }

          itemDetail.inputType = _itemInputType;
          _setItemInfo(itemRefData: grnRefItem[0]);

          if (_itemInputType == InputType.ItemCode) {
            _loadItemPacking();
          } else {
            _loadCountry();
          }
        } else {
          showAlert(
              context: context,
              message: 'Selected reference doesn\'t contain the item');
        }
      }
    } else if (widget.type == TransactionType.PR) {
      if (cmnSupplierInfo.pRetTypeId == PRType.Direct.index + 1) {
        _loadItem();
      } else {
        var prRefItem;

        /// If item barcode or item code entered by user, get item details
        if (_itemInputType == InputType.ItemCode &&
            _itemCodeController.text.isNotEmpty) {
          prRefItem = prRefItemList
              .where((item) => item.itemCode == _itemCodeController.text.trim())
              .toList();
        } else if (_itemInputType == InputType.Barcode &&
            _itemCodeController.text.isNotEmpty) {
          prRefItem = prRefItemList
              .where(
                  (item) => item.itemBarcode == _itemCodeController.text.trim())
              .toList();
        }
        if (prRefItem.isNotEmpty) {
          /// Check whether already the item is added or not
          if (_itemInputType == InputType.Barcode &&
              editMode == EditMode.Off &&
              CommonHighItemInfo.shared.cmnHighItemList
                  .where((item) => item.itemBarcode == prRefItem[0].itemBarcode)
                  .toList()
                  .isNotEmpty) {
            showAlert(context: context, message: 'Item already added');
            _didSelect(InputType.Barcode);
            return;
          }

          itemDetail.inputType = _itemInputType;
          _setItemInfo(prItemRefData: prRefItem[0]);

          if (_itemInputType == InputType.ItemCode) {
            _loadItemPacking();
          } else {
            _loadCountry();
          }

          if (cmnSupplierInfo.pRetTypeId == PRType.GRN.index + 1) {
            shouldDisableBatchNo = true;
            itemBatchDataList = [ItemBatchData(batch_text: itemDetail.batchNo)];
          }
        } else {
          showAlert(
              context: context,
              message: 'Selected reference doesn\'t contain the item');
        }
      }
    }
  }

  // void _getOptedInstallation() {
  //   // itemDetail.isInstallationChecked = hasOptedInstallation;
  // }

  _loadItem() {
    // If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {
      _hteBloc.add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
    } else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      _hteBloc.add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
    }
  }

  _loadGrnNos() {
    if (itemDetail.batchNo.isNotEmpty) {
      var req = GetGRNEntryNobyItemReq();
      req.batchNo = itemDetail.batchNo;
      req.itemId = itemDetail.itemId.toString();
      req.packingId = itemDetail.packingId.toString();
      req.supplierId = cmnSupplierInfo.suppId.toString();
      req.priceType = cmnSupplierInfo.priceTypeId.toString();
      req.companyId = fromAddress.fromCompanyId;
      req.divisionId = fromAddress.fromDivisionId;
      req.locationId = fromAddress.fromLocationId;
      req.departmentId = fromAddress.fromDepartmentId;

      _hteBloc.add(GetGRNEntryNobyItem(req: json.encode(req.toJson())));
    } else {
      showMessage(context: buildContext, message: "Enter Batch No");
    }
  }

  /// Load unique item info with barcode based on selected packing, using the item id
  void _findUniqueItemWithItemCodeAndPackingId() {
    if (_itemInputType == InputType.ItemCode) {
      if (itemDetail.itemId != null && itemDetail.packingId != null) {
        _hteBloc.add(LoadItemByPacking(
            itemId: itemDetail.itemId.toString(),
            prodPkgId: itemDetail.packingId.toString()));
      }
    }
  }

  void _loadItemPacking() {
    _hteBloc.add(LoadPacking(itemId: itemDetail.itemId.toString()));
  }

  void _loadItemPackingById() {
    _hteBloc.add(LoadPackingById(
        packId: itemDetail.packingId.toString(),
        itemId: itemDetail.itemId.toString()));
  }

  void _loadItemBatch() {
    if (itemDetail.itemId != null) {
      _hteBloc.add(LoadBatchById(itemId: itemDetail.itemId));
    }
  }

  void _loadItemPriceDetails() {
    // if (type == TransactionType.GRN) {
    _hteBloc.add(GetScmItemPriceDetails(
        type: type,
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        toCompanyId: cmnSupplierInfo.companyId.toString(),
        toDivisionId: cmnSupplierInfo.divisionId.toString(),
        toLocationId: cmnSupplierInfo.locationId.toString(),
        supplierId: cmnSupplierInfo.suppId.toString(),
        itemId: itemDetail.itemId.toString(),
        packingId: "${itemDetail.packingId}"));
    // }
  }

  void _loadCountry() {
    _hteBloc.add(LoadCountryById(countryId: itemDetail.countryId));
  }

  void _loadProductBrand() {
    _hteBloc.add(LoadBrandById(brandId: itemDetail.brandId));
  }

  void _loadWareHouse() {
    _hteBloc.add(GetWareHouse());
  }

  void _loadZone() {
    _hteBloc
        .add(GetWareHouseZone(warehouseId: itemDetail.warehouseId.toString()));
  }

  void _loadRack() {
    _hteBloc.add(GetWareHouseRack(zoneId: itemDetail.wHouseZoneId.toString()));
  }

  void _loadBin() {
    _hteBloc.add(
        GetWareHouseBin(rackId: itemDetail.wHouseZoneBinRackId.toString()));
  }
}
