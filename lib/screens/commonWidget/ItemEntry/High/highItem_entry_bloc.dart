import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetGRNReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getGRNEntryNobyItemResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'highItem_entry_event.dart';

part 'highItem_entry_state.dart';

class HighItemEntryBloc extends Bloc<HighItemEntryEvent, HighItemEntryState> {
  final BCRepository bcRepository;
  final Database database;
  final GrnReq grnReq;

  HighItemEntryBloc(
      {@required this.bcRepository,
        @required this.database,
        @required this.grnReq})
      : assert(bcRepository != null),
        super(LoadItemInitial());

  @override
  Stream<HighItemEntryState> mapEventToState(HighItemEntryEvent event) async* {
    TransactionResp transactionResp;

    int draftTransactionResp;
    bool updateDraftTransactionResp;
    List<DraftTransactionData> getDraftTranId;
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;
    List<CountryOriginData> loadCountryResp;
    List<ProductBrandData> loadBrandResp;

    if (event is LoadItem) {
      yield LoadItemInProgress();

      try {
        if (event.barcode.isNotEmpty) {
          loadItemResp =
          await database.itemBarcodeDao.getItemByBarcode(event.barcode);
        } else {
          loadItemResp =
          await database.itemBarcodeDao.getItemByItemCode(event.itemCode);
        }
        yield LoadItemComplete(loadItemResult: loadItemResp);
        // yield LoadItemComplete(loadItemResult: [
        //   ItemBarcodeData(id: 1,
        //       prod_stk_type_id: 1,
        //       barcode_text: "1111000028",
        //       barcode_value: "1111000028",
        //       prod_id: 20,
        //       prod_name: "Almarai Cheese 100gm",
        //       item_id: 2,
        //       item_code: "00000001",
        //       item_name: "Almarai Cheese 100gm",
        //       item_pck_id: 2,
        //       mjr_pck_id: 9,
        //       base_pck_id: 2,
        //       pkg_qty: "1.0",
        //       scale_grp_id: 0,
        //       brand_id: 11,
        //       min_shelf_life: 10,
        //       total_shelf_life: 100,
        //       is_sales_allowed: 1,
        //       is_tax_applicable: 1,
        //       item_status: 1,
        //       is_pur_allowed: 1,
        //       excess_qty: "5",
        //       is_min_order_qty_app: null,
        //       min_order_qty: null,
        //       supp_type_id: 123,
        //       tax_percent: null,
        //       is_batch_enabled: 1,
        //       has_pur_grp_access: 1,
        //       stk_based_item: 1,
        //       consign_item: 0,
        //       last_pur_qty: "1000",
        //       last_rcvd_amt: "0.42415")
        // ]);
      } catch (error) {
        yield LoadItemFailure(error: error.toString());
      }
    }

    else if (event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp =
        await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if (event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao
              .getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadPackingById) {
      yield LoadPackingByIdInProgress();

      try {
        loadPackingResp = await database.itemPackingDao
            .getPackingById(event.packId, event.itemId);
        yield LoadPackingByIdComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingByIdFailure(error: error.toString());
      }
    }

    else if (event is LoadCountryById) {
      yield LoadCountryByIdInProgress();

      try {
        loadCountryResp =
        await database.countryOriginDao.getCountryNameById(event.countryId);
        yield LoadCountryByIdComplete(loadCountryResult: loadCountryResp);
      } catch (error) {
        yield LoadCountryByIdFailure(error: error.toString());
      }
    }

    else if (event is LoadBrandById) {
      yield LoadBrandByIdInProgress();

      try {
        loadBrandResp =
        await database.productBrandDao.getBrandNameById(event.brandId);
        yield LoadBrandByIdComplete(loadBrandResult: loadBrandResp);
      } catch (error) {
        yield LoadBrandByIdFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouse) {
      yield GetWareHouseInProgress();

      try {
        final GetCommonResp getCommonResp = await bcRepository.getWareHouse();
        yield GetWareHouseComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseZone) {
      yield GetWareHouseZoneInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseZone(warehouseId: event.warehouseId);
        yield GetWareHouseZoneComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseZoneFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseRack) {
      yield GetWareHouseRackInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseRack(zoneId: event.zoneId);
        yield GetWareHouseRackComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseRackFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseBin) {
      yield GetWareHouseBinInProgress();

      try {
        final GetWarehouseBinMasterResp getWarehouseBinMasterResp =
        await bcRepository.getWareHouseBin(rackId: event.rackId);
        yield GetWareHouseBinComplete(
            getWarehouseBinMasterResp: getWarehouseBinMasterResp);
      } catch (error) {
        yield GetWareHouseBinFailure(error: error.toString());
      }
    }

    else if (event is GetScmItemPriceDetails) {
      yield GetScmItemPriceDetailsInProgress();

      try {
        final GetScmItemPriceDetailsResp getPackingDetailsResp =
        await bcRepository.getScmItemPriceDetails(
            type: event.type,
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            toCompanyId: event.toCompanyId,
            toDivisionId: event.toDivisionId,
            toLocationId: event.toLocationId,
            supplierId: event.supplierId,
            itemId: event.itemId,
            packingId: event.packingId);
        yield GetScmItemPriceDetailsComplete(
            getPackingDetailsResp: getPackingDetailsResp);
      } catch (error) {
        yield GetScmItemPriceDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetGRNEntryNobyItem) {
      yield GetGRNEntryNobyItemInProgress();

      try {
        final GetGRNEntryNobyItemResp getGRNEntryNobyItemResp = await bcRepository.getGRNEntryNobyItem(req: event.req);
        yield GetGRNEntryNobyItemComplete(getGRNEntryNobyItemResp: getGRNEntryNobyItemResp);
      } catch (error) {
        yield GetGRNEntryNobyItemFailure(error: error.toString());
      }
    }

    else if (event is LoadBatchById) {
      yield LoadBatchByIdInProgress();

      try {
        List<ItemBatchData> getItemBatchResp = await database.itemBatchDao.getItemBatchByItemId(event.itemId);
        yield LoadBatchByIdComplete(getItemBatchResp: getItemBatchResp);
      } catch (error) {
        yield LoadBatchByIdFailure(error: error.toString());
      }
    }

    else if (event is GetGRNReferenceDetails) {
      yield GetGRNReferenceDetailsInProgress();

      try {
        final GetGRNReferenceDetailsResp resp = await bcRepository.getGRNReferenceDetails(refTypeId: event.refTypeId,refId: event.refId);
        yield GetGRNReferenceDetailsComplete(getGRNReferenceDetailsResp: resp);
      } catch (error) {
        yield GetGRNReferenceDetailsFailure(error: error.toString());
      }
    }

    else if (event is GetPRReferenceDetails) {
      yield GetPRReferenceDetailsInProgress();

      try {
        final GetPRReferenceDetailsResp resp = await bcRepository.getPurchaseReturnReferenceDetails(refId: event.refId);
        yield GetPRReferenceDetailsComplete(getPRReferenceDetailsResp: resp);
      } catch (error) {
        yield GetPRReferenceDetailsFailure(error: error.toString());
      }
    }
  }
}
