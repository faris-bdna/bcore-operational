part of 'basic_item_entry_bloc.dart';

abstract class BasicItemEntryEvent extends Equatable {
  const  BasicItemEntryEvent();
}

class GetStockReferenceDetails extends BasicItemEntryEvent {
  final String refId;
  const GetStockReferenceDetails({this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetStockReferenceDetails { RefId : $refId}';
}

class LoadItem extends BasicItemEntryEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

class LoadPacking extends BasicItemEntryEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadPackingById extends BasicItemEntryEvent {
  final String packId;
  final String itemId;

  const LoadPackingById({this.packId, this.itemId});

  @override
  List<Object> get props => [packId, itemId];

  @override
  String toString() =>
      'LoadPacking {"PackId" : "$packId", "ItemId" : "$itemId"}';
}

class LoadCountryById extends BasicItemEntryEvent {
  final String countryId;

  const LoadCountryById({this.countryId});

  @override
  List<Object> get props => [countryId];

  @override
  String toString() => 'LoadCountryById {"CountryId" : "$countryId"}';
}

class LoadBrandById extends BasicItemEntryEvent {
  final String brandId;

  const LoadBrandById({this.brandId});

  @override
  List<Object> get props => [brandId];

  @override
  String toString() => 'LoadBrandById {"BrandId" : "$brandId"}';
}

class LoadItemByPacking extends BasicItemEntryEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

class GetWareHouse extends BasicItemEntryEvent {
  const GetWareHouse();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetWareHouse { }';
}

class GetWareHouseZone extends BasicItemEntryEvent {
  final String warehouseId;

  const GetWareHouseZone({this.warehouseId});

  @override
  List<Object> get props => [warehouseId];

  @override
  String toString() => 'GetWareHouseZone {"WarehouseId" : "$warehouseId"}';
}

class GetWareHouseRack extends BasicItemEntryEvent {
  final String zoneId;

  const GetWareHouseRack({this.zoneId});

  @override
  List<Object> get props => [zoneId];

  @override
  String toString() => 'GetWareHouseRack {"ZoneId" : "$zoneId"}';
}

class GetWareHouseBin extends BasicItemEntryEvent {
  final String rackId;

  const GetWareHouseBin({this.rackId});

  @override
  List<Object> get props => [rackId];

  @override
  String toString() => 'GetWareHouseBin {"RackId" : "$rackId"}';
}

class GetPackingDetails extends BasicItemEntryEvent {
  final TransactionType type;
  final String companyId;
  final String divisionId;
  final String locationId;
  final String toCompanyId;
  final String toDivisionId;
  final String toLocationId;
  final String supplierId;
  final String itemId;
  final String packingId;

  const GetPackingDetails(
      {this.type,
        this.companyId,
        this.divisionId,
        this.locationId,
        this.toCompanyId,
        this.toDivisionId,
        this.toLocationId,
        this.supplierId,
        this.itemId,
        this.packingId});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetPackingDetails {"Type" : "$type","CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"'
          '"ToDivisionId" : "$toDivisionId","ToLocationId" : "$toLocationId","SupplierId" : "$supplierId","ItemId" : "$itemId","PackingId" : "$packingId"}';
}

class LoadBatchById extends BasicItemEntryEvent {
  final int itemId;

  const LoadBatchById({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadBatchById {"ItemId" : "$itemId"}';
}

class GetItemBatchMaster extends BasicItemEntryEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String lastFetchDate;

  const GetItemBatchMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate];

  @override
  String toString() => 'GetItemBatchMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'lastFetchDate: $lastFetchDate}';
}

class GetReason extends BasicItemEntryEvent {

  const GetReason();

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetReason';
}