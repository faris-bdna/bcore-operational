import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart' as cmn;
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/response/BasicForm/getStockReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicItemInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/OB/ob_page.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart'
    as cmn;
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart'
    as whb;

import 'basic_item_entry_bloc.dart';

class BasicItemEntryForm extends StatefulWidget {
  final BCRepository repository;
  final Database database;
  final AppLanguage appLanguage;
  final TransactionType type;
  final CommonBasicItem item;

  const BasicItemEntryForm(
      {Key key,
      this.database,
      this.appLanguage,
      this.item,
      this.type,
      this.repository})
      : super(key: key);

  @override
  _BasicItemEntryFormState createState() =>
      _BasicItemEntryFormState(database, appLanguage, item, repository);
}

class _BasicItemEntryFormState extends State<BasicItemEntryForm> {
  final Database database;
  final AppLanguage appLanguage;
  CommonBasicItem basicItem;
  final BCRepository repository;

  EditMode editMode;

  _BasicItemEntryFormState(
      this.database, this.appLanguage, this.basicItem, this.repository);

  final List<DropdownMenuItem> items = [];

  var _itemCodeController = TextEditingController();
  var _quantityController = TextEditingController();
  var _averageCostController = TextEditingController();
  var _batchNoController = TextEditingController();
  var _batchCostController = TextEditingController();
  var _netCostController = TextEditingController();

  InputType _itemInputType = InputType.Barcode;

  bool itemHasExpiry = true;

  var itemCodeFocusNode = FocusNode();
  var quantityFocusNode = FocusNode();
  var averageCostFocusNode = FocusNode();
  var netCostFocusNode = FocusNode();

  List<MaterialRequestData> materialRequestDataList;
  MaterialRequestDetail materialRequestDetail;
  ItemPackingData itemPackingData;
  List<ItemPackingData> itemPackingDataList;
  List<MasterDataList> stockRefItemsList = [];

  List<cmn.CommonDataList> wareHouseList = [];
  List<cmn.CommonDataList> wareHouseZoneList = [];
  List<cmn.CommonDataList> wareHouseRackList = [];
  List<whb.WarehouseBinList> wareHouseBinList = [];

  List<ItemBarcodeData> itemBarcodeDataList;
  MaterialRequest materialRequest;
  MaterialRequestData materialRequestData;

  List<ItemBatchData> itemBatchDataList;
  List<cmn.CommonDataList> reasonsList;

  // Should contain or not
  bool containBatchNoDropDown = false;

  /// For batch no drop down
  bool containReason = false;
  bool containExpiryDate = false;
  bool containExpiryDatePicker = false;
  bool containItemLocation = false;
  bool containEditablePrice = false;
  bool containBatchNoTextField = false;

  bool shouldValidateItemCode = false;
  bool shouldValidatePackage = false;
  bool shouldValidateQuantity = false;
  bool isQuantityValid = false;

  BasicItemEntryBloc _basicItemEntryBloc;

  @override
  void initState() {
    super.initState();

    _configUI();
    _configValidation();
    loadItemDetailsIfInEditMode();
  }



  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {

      _itemInputType = basicItem.inputType;
      _itemCodeController.text = (_itemInputType == InputType.Barcode)
          ? basicItem.itemBarcode : basicItem.itemCode;

      if (widget.type == TransactionType.OB) {
        _averageCostController.text = basicItem.averageCost ?? "";
        _netCostController.text = basicItem.netCost ?? "";
        _batchNoController.text = basicItem.batchNo ?? "";
        _batchCostController.text = basicItem.batchCost ?? "";
      }
      else if (widget.type == TransactionType.ST) {
        if (_itemInputType == InputType.ItemCode) {
          Future.delayed(Duration.zero, () {
            _loadItemPacking(context);
          });
        }
        else {
          if (editMode == EditMode.Off) {
            _setItemName(); // TODO
          }
          Future.delayed(Duration.zero, () {
            getBatchNoAndReason(context);
          });
        }
      }
      setState(() {
        if(basicItem.phyQty != null)
        _quantityController.text = basicItem.phyQty.toString();
      });
    }
    else {
      itemCodeFocusNode.requestFocus();
    }
  }

  _configUI() {
    if (basicItem == null || basicItem.itemBarcode==null) {
      editMode = EditMode.Off;
      basicItem = CommonBasicItem();
    }
    else {
      editMode = EditMode.On;
    }

    switch (widget.type) {
      case TransactionType.ST:

        /// Show batch no drop down only if the Stock Type is Batch wise
        if (cmnBasicFormInfo.stockTypeId == StockType.BatchWise.index + 1) {
          containBatchNoDropDown = true;
          containExpiryDate = true;
        }
        containReason = true;
        containItemLocation = false;
        containEditablePrice = false;
        containBatchNoTextField = false;
        break;

      case TransactionType.OB:
        containBatchNoDropDown = false;
        containReason = false;
        containItemLocation = true;
        containEditablePrice = true;
        containBatchNoTextField = true;
        break;
    }
  }

  _configValidation() {
    switch (widget.type) {
      case TransactionType.ST:
        shouldValidateItemCode = true;
        shouldValidatePackage = true;
        shouldValidateQuantity = true;
        isQuantityValid = false;
        break;

      case TransactionType.OB:
        shouldValidateItemCode = true;
        shouldValidatePackage = true;
        shouldValidateQuantity = true;
        isQuantityValid = false;
        break;
    }
  }

  @override
  void dispose() {
    super.dispose();

    editMode = EditMode.Off;
    basicItem = CommonBasicItem();
    _batchCostController.dispose();
    _batchNoController.dispose();
    _netCostController.dispose();
    _itemCodeController.dispose();
    _quantityController.dispose();
    _averageCostController.dispose();
  }

  void _didSelect(InputType type) {
    itemCodeFocusNode.requestFocus();

    setState(() {
      itemPackingDataList= [];
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';
      _averageCostController.text = '';
      _batchCostController.text = '';
      _batchNoController.text = '';
      _netCostController.text = '';
      // itemBatchDataList = [];
      basicItem.clear();
      // reasonsList = [];
      _clearItemInfo();
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BasicItemEntryBloc>(
      create: (context) =>
          BasicItemEntryBloc(database: database, bcRepository: repository),
      child: BlocListener<BasicItemEntryBloc, BasicItemEntryState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingByIdInProgress ||
              state is LoadPackingInProgress ||
              state is LoadCountryByIdInProgress ||
              state is LoadBrandByIdInProgress ||
              state is GetStockReferenceDetailsInProgress ||
              state is GetReasonInProgress ||
              state is LoadBatchByIdInProgress ||
              state is GetWareHouseInProgress ||
              state is GetWareHouseZoneInProgress ||
              state is GetWareHouseRackInProgress ||
              state is GetWareHouseBinInProgress ||
              state is LoadItemByPackingInProgress
          ) {
            Common().showLoader(context);
          } else if (state is LoadItemFailure ||
              state is LoadPackingByIdFailure ||
              state is LoadPackingFailure ||
              state is LoadCountryByIdFailure ||
              state is LoadBrandByIdFailure ||
              state is GetStockReferenceDetailsFailure ||
              state is GetReasonFailure ||
              state is LoadBatchByIdFailure ||
              state is GetWareHouseFailure ||
              state is GetWareHouseZoneFailure ||
              state is GetWareHouseRackFailure ||
              state is GetWareHouseBinFailure ||
              state is LoadItemByPackingFailure
          ) {
            Navigator.pop(context);
          }
          else if (state is GetStockReferenceDetailsComplete) {
            Navigator.pop(context);
            if (state.resp.data.masterDataList.isNotEmpty) {
              stockRefItemsList = state.resp.data.masterDataList;
              SaveSTReq.shared.dateFormat = state.resp.data.dateFormat;

              if (editMode == EditMode.On) {
                _getItemDetailsFromRef(context);
              }
            }else{
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          } else if (state is LoadItemComplete) {
            Navigator.pop(context);
            print("LoadItemComplete");

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              ItemBarcodeData itemData = state.loadItemResult[0];

              /// Check whether already the item is added or not
              if ( _itemInputType == InputType.Barcode &&
                  editMode == EditMode.Off &&
                  CommonBasicItemInfo.shared.cmnBasicItemList
                      .where((item) =>
                  item.itemBarcode == state.loadItemResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                _didSelect(InputType.Barcode);
                return;
              }

              if (widget.type == TransactionType.OB) {
                /// Here Checks whether the item is already added in stock.
                /// If so, shows alert and resets the fields.

                /// Checks if item has expiry
                if (itemData.ProductStockTypeId ==
                    ProductStockType.Expiry.index) {
                  containExpiryDatePicker = true;
                }

                /// Checks if item has a batch
                if (itemData.IsBatchEnabled.toBool()) {
                  containBatchNoTextField = true;
                }

                if (editMode == EditMode.Off) {
                  /// checks whether the item is already in the stock
                  if ( itemData.StockBasedItem.toBool() == true) {
                    Common().showStepperValidation(
                        scaffoldKey: scaffoldOBKey,
                        message: 'Item already available');

                    /// Reset Fields
                    _didSelect(InputType.Barcode);
                    return;
                  }
                  _setItemInfo(itemData);
                }
              }

              if (widget.type == TransactionType.ST) {
                if (editMode == EditMode.Off) {
                  _setItemInfo(itemData);
                }
              }

              //   setState(() {
              //
              //   if (editMode == EditMode.Off) {
              //   }
              // });

              if (_itemInputType == InputType.ItemCode) {
                // basicItem.itemCode = _itemCodeController.text.trim();
                _loadItemPacking(context);
              }
              else {
                // basicItem.itemBarcode = _itemCodeController.text.trim();
                /// Set item name in barcode text field
                _setItemName();

                if (widget.type == TransactionType.ST) {
                  getBatchNoAndReason(context);
                }
              }
            }
            else {
              if (widget.type == TransactionType.ST) {

                /// hide keyboard code for different versions
                FocusScope.of(context).unfocus();
                FocusScope.of(context).requestFocus(FocusNode());

                /// Check whether already the item is added or not
                if (_itemInputType == InputType.Barcode &&
                    CommonBasicItemInfo.shared.cmnBasicItemList
                    .where((item) => item.itemBarcode == _itemCodeController.text.trim())
                    .toList()
                    .isNotEmpty) {
                  showAlert(context: context, message: 'Item already added');
                  return;
                }
                showAlert(context: context, message: 'Item Not Found');
                _didSelect(_itemInputType);
               return;
                // Common().showAlertMessage(
                //     context: context,
                //     title: Constants.alertMsgTitle,
                //     message:
                //         'Item not available in master. Still you can add item\'s Stock Taking',
                //     okFunction: () {
                //       Navigator.pop(context);
                //     });

               /* if (_itemInputType == InputType.ItemCode) {
                  basicItem.itemCode = _itemCodeController.text;
                } else {
                  basicItem.itemBarcode = _itemCodeController.text;
                }
                _getReasons(context);*/
              }
              else {
                if (_itemInputType == InputType.ItemCode) {
                  basicItem.itemCode = _itemCodeController.text;
                } else {
                  basicItem.itemBarcode = _itemCodeController.text;
                }
                Common().showAlertMessage(
                    context: context,
                    title: 'Alert',
                    message:
                        'Item not available in master. Still you can add item\'s Opening Balance',
                    okFunction: () {
                      Navigator.pop(context);
//                      quantityFocusNode.requestFocus();
                    });
              }
            }
            quantityFocusNode.requestFocus();
          }
          else if (state is LoadPackingComplete) {
            Navigator.pop(context);

            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                if (editMode == EditMode.Off) {
                  basicItem.packingId =
                      int.parse(itemPackingDataList[0].packing_value);
                  basicItem.pkgName = itemPackingDataList[0].packing_text;
                }

                /// Set item name in barcode text field
                _setItemName();
                FocusScope.of(context).unfocus();
              });

              /**
               * Load unique item info with barcode based on selected packing, using the item id
               * */
              _findUniqueItemWithItemCodeAndPackingId();

              if (widget.type == TransactionType.ST) {
                getBatchNoAndReason(context);
              }
            }
          }
          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.isNotEmpty) {

              /// Check whether already the item is added or not
              if (CommonBasicItemInfo.shared.cmnBasicItemList
                      .where((item) =>
                  item.itemBarcode == state.loadItemByPackingResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
                return;
              }

              setState(() {
                basicItem.itemBarcode =
                    state.loadItemByPackingResult[0].Text ?? "0";
                // basicItem.averageCost = state.loadItemByPackingResult[0].AverageCost;
                basicItem.packingDesc = state.loadItemByPackingResult[0].PackingDesc;
              });
            }
          }
          else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);

            if (state.getItemBatchResp != null &&
                state.getItemBatchResp.length > 0) {
              setState(() {
                itemBatchDataList = state.getItemBatchResp;
              });
            }

            _getReasons(context);
          } else if (state is GetReasonComplete) {
            Navigator.pop(context);
            cmn.GetCommonResp getReasonTypeResp = state.getReasonResp;
            reasonsList = [];

            setState(() {
              reasonsList = getReasonTypeResp.masterDataList;
            });
          } else if (state is GetWareHouseComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.Off) {
                  showItemLocationDialogue(context);
                } else {
                  _loadZone();
                }
              });
            }
          } else if (state is GetWareHouseZoneComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseZoneList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On) {
                  _loadRack();
                }
              });
            }
          } else if (state is GetWareHouseRackComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseRackList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On) {
                  _loadBin();
                }
              });
            }
          } else if (state is GetWareHouseBinComplete) {
            Navigator.pop(context);

            if (state.getWarehouseBinMasterResp != null &&
                state.getWarehouseBinMasterResp.masterDataList.length > 0) {
              setState(() {
                wareHouseBinList =
                    state.getWarehouseBinMasterResp.masterDataList;
                if (editMode == EditMode.On) {
                  showItemLocationDialogue(context);
                }
              });
            }
          }
        },
        child: BlocBuilder<BasicItemEntryBloc, BasicItemEntryState>(
            builder: (context, state) {
          _basicItemEntryBloc = BlocProvider.of<BasicItemEntryBloc>(context);

          if (state is GetStockReferenceDetailsInitial) {
            if (widget.type == TransactionType.ST && editMode == EditMode.Off) {
              _getItemListForRef(context);
            }
            if (widget.type == TransactionType.OB && editMode == EditMode.On) {
              _getItemDetailsFromDB(context);
            }
          }
          return Container(
            child: Column(
              children: [
                _getBarcodeField(context),
                if (_itemInputType == InputType.ItemCode) _getPackageField(),
                if (basicItem.packingDesc != null) _getPackingDescrField(),
                _getQuantityField(),
                if (containEditablePrice) _getEditablePriceField(),
                _getItemDetailsField(context)
              ],
            ),
          );
        }),
      ),
    );
  }

  _getBarcodeField(BuildContext context) {
    return  editMode == EditMode.Off ? Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.Barcode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.ItemCode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Code", style: TextStyle(fontSize: 13.0))
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextFormField(
            enableSuggestions: false,
            inputFormatters: [
              FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s"))
            ],
            textInputAction: (_itemInputType == InputType.Barcode)
                ? TextInputAction.next
                : TextInputAction.done,
            controller: _itemCodeController,
            focusNode: itemCodeFocusNode,
            maxLines: 1,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateItemCode,
                isValidated: _itemCodeController.text.trim().isNotEmpty,
                hintText: (_itemInputType == InputType.Barcode)
                    ? 'Item Barcode'
                    : 'Item Code',
                icon:_itemInputType == InputType.Barcode ? Icons.blur_linear : Icons.search,
              iconPress: ()async{
                if (_itemInputType == InputType.Barcode) {
                  var result = await BarcodeScanner.scan();
                  setState(() {
                    _itemCodeController.text = result;

                    if (widget.type == TransactionType.ST) {
                      _getItemDetailsFromRef(context);
                    }
                    else {
                      _getItemDetailsFromDB(context);
                    }
                  });
                }
                else{
                  if (widget.type == TransactionType.ST) {
                    _getItemDetailsFromRef(context);
                  }
                  else {
                    _getItemDetailsFromDB(context);
                  }
                }
              }),
            keyboardType: TextInputType.name,
            validator: (value){
              if (widget.type == TransactionType.ST) {
                _getItemDetailsFromRef(context);
              } else {
                _getItemDetailsFromDB(context);
              }
              return null;
            },
            onFieldSubmitted: (value) {
              setState(() {
                if (widget.type == TransactionType.ST) {
                  _getItemDetailsFromRef(context);
                } else {
                  _getItemDetailsFromDB(context);
                }
              });
            },
          ),
        ),
      ],
    ): Column(
    children: [
    ElevatedButton(onPressed: (){
    editMode = EditMode.Off;
    _didSelect(InputType.Barcode);
    }, child: Text("Clear & Add New"))
    ],
    );
  }

  _getPackageField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: shouldValidatePackage,
                isValidated: basicItem.packingId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Text(""),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: basicItem.packingId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value));
                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    basicItem.packingId = value;
                    basicItem.pkgName = itemPackingDataList
                        .where((packing) =>
                            packing.packing_value == value.toString())
                        .toList()[0]
                        .packing_text;
                    /**
                     * Load unique item info with barcode based on selected packing, using the item id
                     * */
                    _findUniqueItemWithItemCodeAndPackingId();
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getPackingDescrField(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Text(basicItem.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getQuantityField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.done,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateQuantity,
                isValidated: _quantityController.text.isNotEmpty,
                hintText: 'Quantity'),
            onChanged: (value) {
              setState(() {
                basicItem.phyQty = double.parse(value);
              });
            },
            onSubmitted: (value) {
              setState(() {
                basicItem.phyQty = double.parse(value);
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getEditablePriceField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _averageCostController,
            focusNode: averageCostFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: false, hintText: 'Average Cost'),
            onChanged: (value) {
              setState(() {
                basicItem.averageCost = value;
              });
            },
            onSubmitted: (value) {
              setState(() {
                basicItem.averageCost = value;
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.done,
            controller: _netCostController,
            focusNode: netCostFocusNode,
            autocorrect: false,
            decoration: Common()
                .getBCoreMandatoryID(isMandatory: false, hintText: 'Net Cost'),
            onChanged: (value) {
              setState(() {
                basicItem.netCost = value;
              });
            },
            onSubmitted: (value) {
              setState(() {
                basicItem.netCost = value;
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getItemDetailsField(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  decoration: Common()
                      .getBCoreSD(isMandatory: false, isValidated: false),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text("Item details of ${basicItem.itemCode?? "-"}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().headingTextColor)),
                        if (containBatchNoTextField) _getBatchNoTextField(),
                        if (containExpiryDatePicker) _getItemExpiryDatePicker(),
                        if (containItemLocation) _getItemLocationField(context),
                        if (containBatchNoDropDown)
                          _getBatchNoDropDown(context),
                        if (containExpiryDate) _getItemExpiryDate(),
                        if (containReason) _getReason()
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: FlatButton(
                    padding: EdgeInsets.zero,
                    color: BCAppTheme().headingTextColor,
                    onPressed: () {
                      // Validate input
                      if (_validateItemData()) {
                        // Save material request
                        saveItemDetails();
                      }
                    },
                    child: Icon(
                      Icons.save,
                      size: 18,
                      color: Colors.white,
                    )),
              )
            ],
          ),
        ),
      ],
    );
  }

  _getBatchNoDropDown(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.zero,
          decoration:
              Common().getBCoreSD(isMandatory: false, isValidated: false),
          child: SearchableDropdown.single(
            items: (itemBatchDataList != null && itemBatchDataList.isNotEmpty)
                ? itemBatchDataList.map((ItemBatchData item) {
                    return DropdownMenuItem(
                        child: Text(item.batch_text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.batch_text);
                  }).toList()
                : [],
            value: basicItem.batchNo,
            hint: "Select Batch No",
            searchHint: "Select Batch No",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  basicItem.batchNo = value;
                  ItemBatchData batchNo = itemBatchDataList
                      .where((item) => item.batch_text == value)
                      .toList()[0];
                  if (batchNo.expiry_date != null) {
                    basicItem.expiryDate = Utils.getDateByFormat(
                        passedDateTime: DateTime.parse(batchNo.expiry_date),
                        format: fromAddress.fromCompanyDateFormat);
                  }
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        )
      ],
    );
  }

  _getItemExpiryDate() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 4.0, right: 4.0),
              child: Text("Expiry Date: ${basicItem.expiryDate ?? ""}",
                  textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getBatchNoTextField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.next,
            controller: _batchNoController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: _shouldValidateBatchNo(),
                isValidated: _isBatchNoValid(),
                hintText: 'Batch No'),
            onChanged: (value) {
              setState(() {
                basicItem.batchNo = value;
              });
            },
            onSubmitted: (value) {
              setState(() {
                basicItem.batchNo = value;
              });
            },
            keyboardType: TextInputType.text,
          ),
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 50,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.done,
            controller: _batchCostController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: _shouldValidateBatchCost(),
                isValidated: _isBatchCostValid(),
                hintText: 'Batch Cost'),
            onChanged: (value) {
              setState(() {
                basicItem.batchCost = value;
              });
            },
            onSubmitted: (value) {
              setState(() {
                basicItem.batchCost = value;
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _shouldValidateBatchCost() {
    if (basicItem != null) {
      if (basicItem.isBatchEnabledItem.toBool()) {
        return true;
      } else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  _isBatchCostValid(){
    if (_shouldValidateBatchCost() && (basicItem?.batchCost == null || basicItem.batchCost.isEmpty)){
      return false;
    }else{
      return true;
    }
  }

  _shouldValidateBatchNo() {
    if (basicItem != null) {
      if (basicItem.isBatchEnabledItem.toBool()) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  _isBatchNoValid(){
    if (_shouldValidateBatchNo() && (basicItem?.batchNo == null || basicItem.batchNo.isEmpty)){
      return false;
    }else{
      return true;
    }
  }

  _getItemExpiryDatePicker() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 40,
          child: DateFormField(
            format: fromAddress.fromCompanyDateFormat,
            showPicker: showItemDatePicker,
            initialValue: basicItem.expiryDate,
            onDateChanged: (DateTime date) {
              setState(() {
                basicItem.expiryDate = Utils.getDateByFormat(
                    format: fromAddress.fromCompanyDateFormat,
                    passedDateTime: date);
              });
            },
            decoration: Common().getBCoreMandatoryID(
                hintText: 'Expiry Date',
                icon: Icons.calendar_today,
                isMandatory: _shouldValidateExpiryDate(),
                isValidated: _isExpiryValid()),
          ),
        )
      ],
    );
  }

  _shouldValidateExpiryDate() {
    // basicItem.prodStkTypeId = ProductStockType.Expiry.index;
    if (basicItem != null) {
      if (basicItem.prodStkTypeId == ProductStockType.Expiry.index) {
        return true;
      } else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  _isExpiryValid(){
    if (_shouldValidateExpiryDate() && basicItem?.expiryDate == null){
      return false;
    }else{
      return true;
    }
  }

  Future<DateTime> showItemDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Expiry Date",
      initialDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: basicItem.minShelfLife))
          : DateTime.now(),
      firstDate: (itemHasExpiry)
          ? DateTime.now().add(Duration(days: basicItem.minShelfLife))
          : DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 36500)),
    );
    return date;
  }

  showItemLocationDialogue(BuildContext blocContext) {
    Dialog locationAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Item Location", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: StatefulBuilder(builder:
                    (BuildContext context, StateSetter dropDownState) {
                  return Column(
                    children: [
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common().getBCoreSD(
                              isMandatory: false, isValidated: true),
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Warehouse"),
                                value: basicItem.warehouseId == null
                                    ? basicItem.warehouseId
                                    : basicItem.warehouseId.toString(),
                                items: wareHouseList
                                    .map((cmn.CommonDataList wareHouse) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouse.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouse.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    basicItem.warehouseId = int.parse(value);
                                    if (basicItem.warehouseId != null) {
                                      _loadZone();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common().getBCoreSD(
                              isMandatory: false, isValidated: true),
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Zone"),
                                value: basicItem.zoneId == null
                                    ? basicItem.zoneId
                                    : basicItem.zoneId.toString(),
                                items: wareHouseZoneList
                                    .map((cmn.CommonDataList wareHouseZone) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseZone.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseZone.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    basicItem.zoneId = int.parse(value);
                                    if (basicItem.zoneId != null) {
                                      _loadRack();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common().getBCoreSD(
                              isMandatory: false, isValidated: true),
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Rack"),
                                value: basicItem.rackId == null
                                    ? basicItem.rackId
                                    : basicItem.rackId.toString(),
                                items: wareHouseRackList
                                    .map((cmn.CommonDataList wareHouseRack) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseRack.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseRack.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    basicItem.rackId = int.parse(value);
                                    if (basicItem.rackId != null) {
                                      _loadBin();
                                    }
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 50,
                        child: Container(
                          decoration: Common().getBCoreSD(
                              isMandatory: false, isValidated: true),
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8.0),
                              child: DropdownButton(
                                underline: Container(),
                                isDense: false,
                                isExpanded: true,
                                hint: Text("Bin"),
                                value: basicItem.binId == null
                                    ? basicItem.binId
                                    : basicItem.binId.toString(),
                                items: wareHouseBinList
                                    .map((whb.WarehouseBinList wareHouseBin) {
                                  return DropdownMenuItem(
                                      child: Text(wareHouseBin.text,
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                      value: wareHouseBin.value);
                                }).toList(),
                                onChanged: (value) {
                                  dropDownState(() {
                                    basicItem.binId = int.parse(value);
                                  });
                                },
                              )),
                        ),
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 35,
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      width: 0.5,
                                      style: BorderStyle.solid,
                                      color: BCAppTheme().primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                ),
                              ),
                              child: FlatButton(
                                  child: Text("Cancel",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                            ),
                          ),
                          SizedBox(width: 16.0),
                          Expanded(
                            child: Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Save",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);
//                                      if (_validateItemLocation()) {
//                                        setState(() {
////                                            Navigator.pop(context);
//                                        });
//                                      } else {}
                                  }),
                            ),
                          )
                        ],
                      )
                    ],
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return locationAlert;
      },
    );
  }

  bool _validateItemLocation() {
    return (basicItem.warehouseId != null ||
        basicItem.zoneId != null ||
        basicItem.binId != null ||
        basicItem.rackId != null);
  }

  _getReason() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: false, isValidated: true),
          child: SearchableDropdown.single(
            items: (reasonsList != null && reasonsList.isNotEmpty)
                ? reasonsList.map((cmn.CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: basicItem.reasonText,
            hint: "Select Reason",
            searchHint: "Select Reason",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  cmn.CommonDataList selectedReason = reasonsList
                      .where((reason) => reason.text == value)
                      .toList()[0];
                  basicItem.reasonId = int.tryParse(selectedReason.value);
                  basicItem.reasonText = selectedReason.text;
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  void _setItemInfo(ItemBarcodeData itemBarcodeData) {
    basicItem.inputType = _itemInputType;
    basicItem.itemBarcode = itemBarcodeData.Text;
    basicItem.itemCode = itemBarcodeData.ItemCode;
    basicItem.itemId = itemBarcodeData.ItemId;
    basicItem.packingId = itemBarcodeData.ItemPackingId;
    basicItem.itemName = itemBarcodeData.ItemName;
    basicItem.minShelfLife = itemBarcodeData.MinShelfLife;
    basicItem.prodStkTypeId = itemBarcodeData.ProductStockTypeId;
    basicItem.isBatchEnabledItem = itemBarcodeData.IsBatchEnabled;
    basicItem.packingDesc = itemBarcodeData.PackingDesc;
//    basicItem.averageCost = itemBarcodeData.avg_cost.toString();
//    basicItem.netCost = itemBarcodeData.net_cost.toString();
  }

  _clearItemInfo() {
    basicItem = CommonBasicItem();
  }

  _getItemListForRef(BuildContext context) {
    _basicItemEntryBloc
        .add(GetStockReferenceDetails(refId: cmnBasicFormInfo.refId));
  }

  /// Checks for the item in the reference details
  _getItemDetailsFromRef(BuildContext context) {
    // if (widget.type == TransactionType.ST) {
    if (stockRefItemsList.isNotEmpty) {
      List<MasterDataList> enteredItems;

      /// Find item in Store Ref Details Items' list

      if (_itemCodeController.text.isNotEmpty) {
        if (_itemInputType == InputType.ItemCode) {
          enteredItems = stockRefItemsList
              .where((item) => item.itemCode == _itemCodeController.text)
              .toList();
        }
        else if (_itemInputType == InputType.Barcode) {
          enteredItems = stockRefItemsList
              .where((item) => item.itemBarcode == _itemCodeController.text)
              .toList();
        }

        /// If match has been found, bind data. Otherwise check item in local db
        if (enteredItems.isNotEmpty) {

          /// Check whether already the item is added or not
          if ( _itemInputType == InputType.Barcode &&
              CommonBasicItemInfo.shared.cmnBasicItemList
              .where((item) =>
          item.itemBarcode == enteredItems[0].itemBarcode)
              .toList()
              .isNotEmpty) {
            showAlert(context: context, message: 'Item already added');
            _didSelect(InputType.Barcode);
            return;
          }

          var enteredItem = enteredItems[0];
          basicItem.itemId = enteredItem.itemId;
          basicItem.itemBarcode = enteredItem.itemBarcode;
          basicItem.itemCode = enteredItem.itemCode;
          basicItem.itemId = enteredItem.itemId;
          basicItem.packingId = enteredItem.packingId;
          basicItem.pkgName = enteredItem.pkgName;
          basicItem.itemName = enteredItem.itemName;
          basicItem.averageCost = enteredItem.unitCost.toString();
          basicItem.netCost = enteredItem.totalCost.toString();
          basicItem.batchNo = enteredItem.batchNo;
          basicItem.packingDesc = enteredItem.packingDesc;

          /// Here checks whether the item product stock type is Expiry item or not
          if (enteredItem.expiryDate != null) {
            basicItem.expiryDate = Utils.getDateByFormat(
                passedDateTime: DateTime.parse(enteredItem.expiryDate),
                format: fromAddress.fromCompanyDateFormat);
          }
          _setItemName();
          if (_itemInputType == InputType.ItemCode) {
            _loadItemPacking(context);
          }
          else {
            getBatchNoAndReason(context);
          }
        } else {
          _getItemDetailsFromDB(context);
        }
      }
    }
    else {
      _getItemDetailsFromDB(context);
    }
    // else{
    //   showAlert(context: context, message: 'This item not friezed for stock taking');
    //   _didSelect(_itemInputType);
    // }
    // }
    // else if (widget.type == TransactionType.OB) {
    //   /// Get item using barcode/item code from DB
    //   _getItemFromDB(context);
    // }
  }

  getBatchNoAndReason(BuildContext context) {
    /// if the stock type is  Batch wise type get Batch list for the item. Otherwise call next API call, ie. GetReason
    if (cmnBasicFormInfo.stockTypeId == StockType.BatchWise.index + 1 &&
        basicItem.itemId != null) {
      _getBatchNo(context);
    } else {
      _getReasons(context);
    }
  }

  /// Load unique item info with barcode based on selected packing, using the item id
  void _findUniqueItemWithItemCodeAndPackingId() {
    if (basicItem.itemId != null && basicItem.packingId != null) {
      _basicItemEntryBloc.add(LoadItemByPacking(
          itemId: basicItem.itemId.toString(),
          prodPkgId: basicItem.packingId.toString()));
    }
  }

  _getBatchNo(BuildContext context) =>
      _basicItemEntryBloc.add(LoadBatchById(itemId: basicItem.itemId));

  _getReasons(BuildContext context) => _basicItemEntryBloc.add(GetReason());

  _getItemDetailsFromDB(BuildContext context) {
    if (_itemCodeController.text.isNotEmpty) {
      /// If item barcode or item code entered by user, get item details
      if (_itemInputType == InputType.ItemCode) {
        _basicItemEntryBloc
            .add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
      } else if (_itemInputType == InputType.Barcode) {
        _basicItemEntryBloc
            .add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
      }
    }
  }

  _getItemLocationField(BuildContext blocContext) {
    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            if (wareHouseList.isEmpty) {
              _loadWareHouse();
            } else {
              showItemLocationDialogue(context);
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration:
                Common().getBCoreSD(isMandatory: false, isValidated: true),
            child: Row(
              children: [
                Expanded(
                    child: Text("Item Location",
                        style: TextStyle(color: BCAppTheme().grayColor))),
                SizedBox(width: 8),
                Icon(Icons.edit, size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        )
      ],
    );
  }

  void _loadWareHouse() {
    _basicItemEntryBloc.add(GetWareHouse());
  }

  void _loadZone() {
    _basicItemEntryBloc
        .add(GetWareHouseZone(warehouseId: basicItem.warehouseId.toString()));
  }

  void _loadRack() {
    _basicItemEntryBloc
        .add(GetWareHouseRack(zoneId: basicItem.zoneId.toString()));
  }

  void _loadBin() {
    _basicItemEntryBloc
        .add(GetWareHouseBin(rackId: basicItem.rackId.toString()));
  }

  void saveItemDetails() {
    if (editMode == EditMode.On) {
      var indexOfItemInExistingList = CommonBasicItemInfo
          .shared.cmnBasicItemList
          .indexWhere((item) => item.itemBarcode == basicItem.itemBarcode);
      if (indexOfItemInExistingList != -1) {
        CommonBasicItemInfo.shared.cmnBasicItemList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CommonBasicItem.fromJson(basicItem.toJson())]);
        showMessage(context: context, message: 'Updated!');

        editMode = EditMode.Off;
      } else {
        showMessage(context: context, message: 'Please try again');
      }
    } else {
      basicItem.index = Utils.getUniqueId();
      CommonBasicItemInfo.shared.cmnBasicItemList.insert(
          CommonItemViewModel.shared.cmnItemList.length,
          CommonBasicItem.fromJson(basicItem.toJson()));
      showMessage(context: context, message: 'Saved!');

    }

    setState(() {
      _didSelect(InputType.Barcode);
    });
    itemCodeFocusNode.requestFocus();
  }

  bool _validateItemData() {
    if (_itemCodeController.text.trim().isEmpty) {
      Common().showAlertMessage(
          context: context,
          title: 'Alert',
          message: 'Enter barcode or item code',
          okFunction: () {
            Navigator.pop(context);
            itemCodeFocusNode.requestFocus();
          });
      return false;
    }

    /// Check whether already the item is added or not
    if ( editMode == EditMode.Off &&
        CommonBasicItemInfo.shared.cmnBasicItemList
        .where((item) => item.itemBarcode == basicItem.itemBarcode)
        .toList()
        .isNotEmpty) {
      showAlert(context: context, message: 'Item already added');
      return false;
    }
    // if (basicItem.packingId == null) {
    //   showMessage(context: context, message: "Please select item package");
    //   return false;
    // }
    if ((basicItem.phyQty == null) ||
        (basicItem.phyQty != null && basicItem.phyQty == 0.0)) {
      Common().showAlertMessage(
          context: context,
          title: 'Alert',
          message: 'Enter quantity',
          okFunction: () {
            Navigator.pop(context);
            quantityFocusNode.requestFocus();
          });
      return false;
    }
    if (widget.type == TransactionType.OB) {

      if (basicItem.isBatchEnabledItem != null && basicItem.isBatchEnabledItem.toBool()) {
        if (basicItem.batchNo == null) {
          Common().showAlertMessage(
              context: context,
              title: 'Alert',
              message: 'Please enter Batch No');
          return false;
        }
        else if (basicItem.batchCost == null) {
          Common().showAlertMessage(
              context: context,
              title: 'Alert',
              message: 'Please enter Batch Cost');
          return false;
        }
        else if (basicItem.prodStkTypeId == ProductStockType.Expiry.index &&
            basicItem.expiryDate == null) {
          Common().showAlertMessage(
              context: context, title: 'Alert', message: 'Select Expiry Date');
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }
  }

  void _setItemName() {
    if (basicItem.itemName != null && basicItem.itemName != "") {
      _itemCodeController.text = basicItem.itemName;
    }
  }

  void _loadItemPacking(BuildContext context) {
    _basicItemEntryBloc.add(LoadPacking(itemId: basicItem.itemId.toString()));
  }
}
