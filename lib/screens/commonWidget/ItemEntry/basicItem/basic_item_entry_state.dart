part of 'basic_item_entry_bloc.dart';

abstract class BasicItemEntryState extends Equatable {
  const BasicItemEntryState();

  @override
  List<Object> get props => [];
}

/// Get Stock Reference Details

class GetStockReferenceDetailsInitial extends BasicItemEntryState{}

class GetStockReferenceDetailsInProgress extends BasicItemEntryState{}

class GetStockReferenceDetailsComplete extends BasicItemEntryState{
  final GetStockReferenceDetailsResp resp;

  GetStockReferenceDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetStockReferenceDetailsFailure extends BasicItemEntryState {
  final String error;

  const GetStockReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetStockReferenceDetailsFailure { error : $error }';
}


/// Load Items

class LoadItemInitial extends BasicItemEntryState{}

class LoadItemInProgress extends BasicItemEntryState{}

class LoadItemComplete extends BasicItemEntryState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends BasicItemEntryState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends BasicItemEntryState{}

class LoadPackingInProgress extends BasicItemEntryState{}

class LoadPackingComplete extends BasicItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends BasicItemEntryState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing by packing id

class LoadPackingByIdInitial extends BasicItemEntryState{}

class LoadPackingByIdInProgress extends BasicItemEntryState{}

class LoadPackingByIdComplete extends BasicItemEntryState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends BasicItemEntryState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends BasicItemEntryState{}

class LoadItemByPackingInProgress extends BasicItemEntryState{}

class LoadItemByPackingComplete extends BasicItemEntryState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends BasicItemEntryState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends BasicItemEntryState{}

class LoadCountryByIdInProgress extends BasicItemEntryState{}

class LoadCountryByIdComplete extends BasicItemEntryState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends BasicItemEntryState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends BasicItemEntryState{}

class LoadBrandByIdInProgress extends BasicItemEntryState{}

class LoadBrandByIdComplete extends BasicItemEntryState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends BasicItemEntryState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get WareHouse

class GetWareHouseInitial extends BasicItemEntryState{}

class GetWareHouseInProgress extends BasicItemEntryState{}

class GetWareHouseComplete extends BasicItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends BasicItemEntryState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends BasicItemEntryState{}

class GetWareHouseZoneInProgress extends BasicItemEntryState{}

class GetWareHouseZoneComplete extends BasicItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends BasicItemEntryState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends BasicItemEntryState{}

class GetWareHouseRackInProgress extends BasicItemEntryState{}

class GetWareHouseRackComplete extends BasicItemEntryState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends BasicItemEntryState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends BasicItemEntryState{}

class GetWareHouseBinInProgress extends BasicItemEntryState{}

class GetWareHouseBinComplete extends BasicItemEntryState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends BasicItemEntryState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get Packing Details

class GetPackingDetailsInitial extends BasicItemEntryState{}

class GetPackingDetailsInProgress extends BasicItemEntryState{}

class GetPackingDetailsComplete extends BasicItemEntryState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetPackingDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetPackingDetailsFailure extends BasicItemEntryState {
  final String error;

  const GetPackingDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPackingDetailsFailure { error : $error }';
}


/// LoadBatchById States

class LoadBatchByIdInitial extends BasicItemEntryState{}

class LoadBatchByIdInProgress extends BasicItemEntryState{}

class LoadBatchByIdComplete extends BasicItemEntryState{
  final List<ItemBatchData> getItemBatchResp;

  LoadBatchByIdComplete({@required this.getItemBatchResp}) : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class LoadBatchByIdFailure extends BasicItemEntryState {
  final String error;

  const LoadBatchByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBatchByIdFailure { error : $error }';
}



class GetReasonInitial extends BasicItemEntryState {}

class GetReasonInProgress extends BasicItemEntryState {}

class GetReasonComplete extends BasicItemEntryState {
  final GetCommonResp getReasonResp;

  GetReasonComplete({@required this.getReasonResp}) : assert(getReasonResp != null);

  @override
  List<Object> get props => [getReasonResp];
}

class GetReasonFailure extends BasicItemEntryState {
  final String error;

  const GetReasonFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetReasonFailure { error : $error }';
}