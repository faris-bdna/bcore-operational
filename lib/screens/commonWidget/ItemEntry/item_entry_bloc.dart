
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetMrRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetSRRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetTrReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'item_entry_event.dart';

part 'item_entry_state.dart';

class ItemEntryBloc extends Bloc<ItemEntryEvent, ItemEntryState> {
  final Database database;
  final BCRepository bcRepository;

  ItemEntryBloc({@required this.bcRepository, @required this.database}) : assert(database != null), super(GetMaterialHeaderIdInitial());

  @override
  Stream<ItemEntryState> mapEventToState(ItemEntryEvent event) async* {
    List<ItemBarcodeData> loadItemResp;
    List<ItemPackingData> loadPackingResp;
    List<CountryOriginData> loadCountryResp;
    List<ProductBrandData> loadBrandResp;
    List<MaterialRequestData> materialRequestData;
    MaterialRequestDetail materialRequestDetail;

    if(event is GetMaterialHeaderId) {
      try {
        materialRequestData = await database.materialRequestDao.getId(event.compId, event.divId, event.locId, event.deptId);
        yield GetMaterialHeaderIdComplete(matReqResp: materialRequestData);
      } catch (error) {
        yield GetMaterialHeaderIdFailure(error: error.toString());
      }
    }

    if(event is GetMaterialDetailLineNum) {
      try {
        materialRequestDetail = await database.materialRequestDetailsDao.getLastLineNumber(event.matReqId);
        yield GetMaterialDetailLineNumComplete(matReqDetailResp: materialRequestDetail);
      } catch (error) {
        yield GetMaterialDetailLineNumFailure(error: error.toString());
      }
    }

    if(event is LoadItem) {
      yield LoadItemInProgress();

      try {
        if(event.barcode.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao.getItemByBarcode(event.barcode);
        } else {
          loadItemResp = await database.itemBarcodeDao.getItemByItemCode(event.itemCode);
        }
        yield LoadItemComplete(loadItemResult: loadItemResp);
      } catch (error) {
        yield LoadItemFailure(error: error.toString());
      }
    }

    if(event is LoadItemByPacking) {
      yield LoadItemByPackingInProgress();

      try {
        if(event.itemId.isNotEmpty && event.prodPkgId.isNotEmpty) {
          loadItemResp = await database.itemBarcodeDao.getItemByPacking(event.itemId, event.prodPkgId);
        }
        yield LoadItemByPackingComplete(loadItemByPackingResult: loadItemResp);
      } catch (error) {
        yield LoadItemByPackingFailure(error: error.toString());
      }
    }

    if(event is LoadPacking) {
      yield LoadPackingInProgress();

      try {
        loadPackingResp = await database.itemPackingDao.getPackingByItemId(event.itemId);
        yield LoadPackingComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingFailure(error: error.toString());
      }
    }

    else if (event is LoadPackingById) {
      yield LoadPackingByIdInProgress();

      try {
        loadPackingResp = await database.itemPackingDao
            .getPackingById(event.packId, event.itemId);
        yield LoadPackingByIdComplete(loadPackingResult: loadPackingResp);
      } catch (error) {
        yield LoadPackingByIdFailure(error: error.toString());
      }
    }

    else if (event is GetScmItemPriceDetails) {
      yield GetScmItemPriceDetailsInProgress();

      try {
        final GetScmItemPriceDetailsResp getPackingDetailsResp =
        await bcRepository.getScmItemPriceDetails(
            type: event.type,
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            toCompanyId: event.toCompanyId,
            toDivisionId: event.toDivisionId,
            toLocationId: event.toLocationId,
            supplierId: event.supplierId,
            itemId: event.itemId,
            packingId: event.packingId);
        yield GetScmItemPriceDetailsComplete(
            getPackingDetailsResp: getPackingDetailsResp);
      } catch (error) {
        yield GetScmItemPriceDetailsFailure(error: error.toString());
      }
    }

    if(event is LoadCountryById) {
      yield LoadCountryByIdInProgress();

      try {
        loadCountryResp = await database.countryOriginDao.getCountryNameById(event.countryId);
        yield LoadCountryByIdComplete(loadCountryResult: loadCountryResp);
      } catch (error) {
        yield LoadCountryByIdFailure(error: error.toString());
      }
    }

    if(event is LoadBrandById) {
      yield LoadBrandByIdInProgress();

      try {
        loadBrandResp = await database.productBrandDao.getBrandNameById(event.brandId);
        yield LoadBrandByIdComplete(loadBrandResult: loadBrandResp);
      } catch (error) {
        yield LoadBrandByIdFailure(error: error.toString());
      }
    }

    if(event is SaveItem) {
      yield SaveItemInProgress();

      try {
        final int saveItemResult = await database.materialRequestDao.insertMaterialRequest(event.materialRequest);
        yield SaveItemComplete(saveItemResult: saveItemResult);
      } catch (error) {
        yield SaveItemFailure(error: error.toString());
      }
    }

    else if(event is SaveItemDetail) {
      yield SaveItemInProgress();

      try {
        final int saveItemDetailResult = await database.materialRequestDetailsDao.insertMaterialRequestDetails(event.materialRequestDetail);
        yield SaveItemDetailComplete(saveItemDetailResult: saveItemDetailResult);
      } catch (error) {
        yield SaveItemDetailFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouse) {
      yield GetWareHouseInProgress();

      try {
        final GetCommonResp getCommonResp = await bcRepository.getWareHouse();
        yield GetWareHouseComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseZone) {
      yield GetWareHouseZoneInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseZone(warehouseId: event.warehouseId);
        yield GetWareHouseZoneComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseZoneFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseRack) {
      yield GetWareHouseRackInProgress();

      try {
        final GetCommonResp getCommonResp =
        await bcRepository.getWareHouseRack(zoneId: event.zoneId);
        yield GetWareHouseRackComplete(getCommonResp: getCommonResp);
      } catch (error) {
        yield GetWareHouseRackFailure(error: error.toString());
      }
    }

    else if (event is GetWareHouseBin) {
      yield GetWareHouseBinInProgress();

      try {
        final GetWarehouseBinMasterResp getWarehouseBinMasterResp =
        await bcRepository.getWareHouseBin(rackId: event.rackId);
        yield GetWareHouseBinComplete(
            getWarehouseBinMasterResp: getWarehouseBinMasterResp);
      } catch (error) {
        yield GetWareHouseBinFailure(error: error.toString());
      }
    }

    else if(event is GetSRRefDetails) {
      yield GetSRRefDetailsInProgress();

      try {
        final GetSRRefDetailsResp resp = await bcRepository.getStoreReceiptReferenceDetails(refId: event.refId);
        yield GetSRRefDetailsComplete(resp: resp);
      } catch (error) {
        yield GetSRRefDetailsFailure(error: error.toString());
      }
    }

    else if(event is GetTransferReceiptReferenceDetails) {
      yield GetTransferReceiptReferenceDetailsInProgress();

      try {
        final GetTrReferenceDetailsResp resp = await bcRepository.getTransferReceiptReferenceDetails(refId: event.refId);
        yield GetTransferReceiptReferenceDetailsComplete(resp: resp);
      } catch (error) {
        yield GetTransferReceiptReferenceDetailsFailure(error: error.toString());
      }
    }

    else if(event is GetMrRefDetails) {
      yield GetMrRefDetailsInProgress();

      try {
        final GetMrRefDetailsResp resp = await bcRepository.getMrRefDetails(refId: event.refId, type: event.type);
        yield GetMrRefDetailsComplete(resp: resp);
      } catch (error) {
        yield GetMrRefDetailsFailure(error: error.toString());
      }
    }
  }
}