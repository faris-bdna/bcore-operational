import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getWarehouseBin_resp.dart';
import 'package:bcore_inventory_management/models/request/MTI/SaveTRReq.dart';
import 'package:bcore_inventory_management/models/request/SR/srReq.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetMrRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetSRRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/item_entry_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart'
as cmn;


class ItemEntryForm extends StatefulWidget {
  final Database database;
  final BCRepository repository;
  final EditMode editMode;
  final TransactionType type;
  final AppLanguage appLanguage;

  final CommonItem item;

  const ItemEntryForm(
      {Key key,
        this.database,
        this.appLanguage,
        this.item,
        this.repository,
        this.editMode,
        this.type})
      : super(key: key);

  @override
  _ItemEntryFormState createState() =>
      _ItemEntryFormState(database, appLanguage, item, repository, type);
}

class _ItemEntryFormState extends State<ItemEntryForm> {
  final Database database;
  final AppLanguage appLanguage;
  CommonItem itemDetail;
  final BCRepository repository;
  final TransactionType type;

  EditMode editMode;

  int lineNum = 1;

  _ItemEntryFormState(this.database, this.appLanguage, this.itemDetail,
      this.repository, this.type);

  DraftTransactionData draftTransactionData;

  var _itemCodeController = TextEditingController();
  var _quantityController = TextEditingController();
  var _searchController = TextEditingController();

  InputType _itemInputType = InputType.Barcode;

  /// Becomes true, when the item in the reference is out of stock and the item has substitute option enabled
  bool hasEnabledSubstituteMode = false;
  bool itemLoaded = false;

  File toAddressJson;
  File matReqJson;
  Directory dir;
  String toAddressFileName = "to_address.json";
  String matReqFileName = "mat_req.json";
  bool fileExists = false;
  Map<String, dynamic> fileContent;
  ToAddress _toAddress;

  bool containPrice = false;
  bool containAvailableQuantity = false;
  bool containIsSubstitute = false;
  bool containLocation = false;
  bool containGetAvailableQtyField = false;
  bool containInstallationRequired = false;

  // bool hasOptedInstallation = false;

  var itemCodeFocusNode = FocusNode();
  var quantityFocusNode = FocusNode();

  List<ItemPackingData> itemPackingDataList;
  List<cmn.CommonDataList> wareHouseList = [];
  List<cmn.CommonDataList> wareHouseZoneList = [];
  List<cmn.CommonDataList> wareHouseRackList = [];
  List<WarehouseBinList> wareHouseBinList = [];
  ItemEntryBloc _itemEntryBloc;

  String selectedWarehouse;
  String selectedWarehouseZone;
  String selectedWarehouseRack;
  String selectedWarehouseBin;

  List<SRRefItemDetails> srRefItemList = [];
  List<MrRefItemsList> mrRefItemList = [];

  MrRefItemsList refItemForSubstitute;

  @override
  void initState() {
    super.initState();

    itemCodeFocusNode.requestFocus();

    _configUI();
    _loadItemDetailsIfInEditMode();
  }

  _loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {
      setState(() {
        _itemInputType = InputType.values[itemDetail.itIndex];
        _itemCodeController.text = (_itemInputType == InputType.Barcode) ? itemDetail.ItemBarcode : itemDetail.ItemCode;
        itemLoaded = true;
        _quantityController.text = itemDetail.ReqQty.toString();
        if (type == TransactionType.SR || type == TransactionType.MTI) {
          selectedWarehouse = itemDetail.warehouseId.toString();
          selectedWarehouseZone = itemDetail.wHouseZoneId.toString();
          selectedWarehouseRack = itemDetail.wHouseZoneBinRackId.toString();
          selectedWarehouseBin = itemDetail.wHouseZoneBinId.toString();
          containInstallationRequired = itemDetail.isAssetItem.toBool();
          itemLoaded = true;
        }
      });
    }
  }

  _configUI() {
    editMode = (itemDetail == null) ? EditMode.Off : EditMode.On;

    if (editMode == EditMode.Off) {
      itemDetail = CommonItem();
    }

    switch (widget.type) {
      case TransactionType.II:
      case TransactionType.SI:
        containPrice = true;
        containLocation = false;
        containAvailableQuantity = false;
        containGetAvailableQtyField = true;
        break;
      case TransactionType.MR:
        containPrice = false;
        containLocation = false;
        containAvailableQuantity = false;
        containIsSubstitute = true;
        break;
      case TransactionType.MTO:
        containPrice = true;
        containLocation = false;
        containAvailableQuantity = true;
        containGetAvailableQtyField = true;
        break;
      case TransactionType.SR:
        containPrice = true;
        containLocation = true;
        containAvailableQuantity = false;
        containGetAvailableQtyField = true;
        containInstallationRequired = true;
        break;
      case TransactionType.MTI:
        containPrice = true;
        containLocation = true;
        containAvailableQuantity = false;
        containGetAvailableQtyField = true;
        break;
    }
  }

  void readToAddressJson() {
    getApplicationDocumentsDirectory().then((Directory directory) {
      dir = directory;
      toAddressJson = new File(dir.path + "/" + toAddressFileName);
      fileExists = toAddressJson.existsSync();
      // if (fileExists) this.setState(() => fileContent = json.decode(toAddressJson.readAsStringSync()));
      if (fileExists)
        this.setState(
                () => _toAddress = json.decode(toAddressJson.readAsStringSync()));
      print(_toAddress);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _itemCodeController.dispose();
    _quantityController.dispose();
    _searchController.dispose();

  }

  void _didSelect(InputType type) {
    itemCodeFocusNode.requestFocus();
    setState(() {
      itemPackingDataList = [];
      itemLoaded = false;
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';
      itemDetail = CommonItem();
      selectedWarehouse = selectedWarehouseBin = null;
      selectedWarehouseRack = selectedWarehouseZone = null;
      // hasOptedSubstitute = false;
      wareHouseList.clear();
      wareHouseRackList.clear();
      wareHouseBinList.clear();
      wareHouseZoneList.clear();
      _searchController.text = '';
      itemDetail.IsSubstituteAllowed = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ItemEntryBloc>(
      create: (context) =>
          ItemEntryBloc(database: database, bcRepository: repository),
      child: BlocListener<ItemEntryBloc, ItemEntryState>(
        listener: (context, state) {
          if ((state is LoadItemInProgress) ||
              (state is LoadPackingInProgress) ||
              state is LoadPackingByIdInProgress ||
              (state is LoadItemByPackingInProgress) ||
              (state is LoadCountryByIdInProgress) ||
              state is GetScmItemPriceDetailsInProgress ||
              state is GetSRRefDetailsInProgress ||
              state is GetWareHouseInProgress ||
              state is GetWareHouseZoneInProgress ||
              state is GetWareHouseRackInProgress ||
              state is GetWareHouseBinInProgress ||
              state is LoadBrandByIdInProgress ||
              state is GetTransferReceiptReferenceDetailsInProgress ||
              state is GetMrRefDetailsInProgress
          ) {
            Common().showLoader(context);
          } else if ((state is LoadItemFailure) ||
              (state is LoadPackingFailure) ||
              state is LoadPackingByIdFailure ||
              (state is LoadItemByPackingFailure) ||
              (state is LoadCountryByIdFailure) ||
              state is GetScmItemPriceDetailsFailure ||
              state is GetSRRefDetailsFailure ||
              state is GetWareHouseFailure ||
              state is GetWareHouseZoneFailure ||
              state is GetWareHouseRackFailure ||
              state is GetWareHouseBinFailure ||
              state is LoadBrandByIdFailure ||
              state is  GetTransferReceiptReferenceDetailsFailure ||
              state is GetMrRefDetailsFailure
          ) {
            Navigator.pop(context);
          }
          else if (state is LoadItemComplete) {
            Navigator.pop(context);
            print(state.loadItemResult.toString());

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              if (state.loadItemResult[0].HasPurchaseGroupAccess.toBool() == false)
              {
                showAlert(context: context, message: "Item not found");
                return;
              }

              /// Check whether already the item is added or not
              if ( _itemInputType == InputType.Barcode &&
                  editMode == EditMode.Off &&
                  (hasEnabledSubstituteMode == false) &&
                  CommonItemViewModel.shared.cmnItemList
                      .where((item) =>
                  item.ItemBarcode == state.loadItemResult[0].Text)
                      .toList()
                      .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }
                setState(() {
                _setItemInfo(item: state.loadItemResult[0]);
                itemLoaded = true;
              });

              if (_itemInputType == InputType.ItemCode) {
                _loadPackingList();
              }
              else {
                _setItemName();
                _loadItemPackingById();
                _getItemPriceDetails();
                _loadCountry();
              }
              quantityFocusNode.requestFocus();
            }
            else {
              showAlert(context: context, message: 'Item not found');

              // Scaffold.of(context).showSnackBar(SnackBar(
              //     content: Text('Item not found'),
              //     duration: Duration(milliseconds: 500)));
            }
          }
          else if (state is GetSRRefDetailsComplete) {
            Navigator.pop(context);
            print(state.resp.toJson());

            if (state.resp.data != null &&
                state.resp.data.itemDetails.isNotEmpty) {
              setState(() {
                SRReq.shared.dateFormat = state.resp.data.dateFormat;
                srRefItemList = state.resp.data.itemDetails;
                print(state.resp.data.toJson());
              });
            }
            else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }
          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);

            if (state.loadItemByPackingResult != null &&
                state.loadItemByPackingResult.isNotEmpty) {

              /// Check whether already the item is added or not
              if ((hasEnabledSubstituteMode == false) &&
                  editMode == EditMode.Off &&
                  CommonItemViewModel.shared.cmnItemList
                  .where((item) =>
              item.ItemBarcode == state.loadItemByPackingResult[0].Text)
                  .toList()
                  .isNotEmpty) {
                showAlert(context: context, message: 'Item already added');
              }

              setState(() {
                itemDetail.ItemBarcode = state.loadItemByPackingResult[0].Text;
                itemDetail.pkgQty = state.loadItemByPackingResult[0].PkgQty.toDouble();
                itemDetail.packingDesc = state.loadItemByPackingResult[0].PackingDesc;
                itemLoaded = true;

                if(hasEnabledSubstituteMode){

                  double approvedQtyInBasePacking = (refItemForSubstitute.approvedQty * refItemForSubstitute.pkgQty).trim();
                  double issueTransferQtyInBasePacking = (refItemForSubstitute.issueTransferQty * refItemForSubstitute.pkgQty).trim();

                  itemDetail.approvedQty = approvedQtyInBasePacking/ itemDetail.pkgQty;
                  itemDetail.issueTransferQty = issueTransferQtyInBasePacking/ itemDetail.pkgQty;
                }

                /// Set item name in barcode text field
                _setItemName();
              });
            }
            _getItemPriceDetails();
          }
          else if (state is LoadPackingComplete) {
            Navigator.pop(context);
            itemPackingDataList = [];
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                if (editMode == EditMode.Off) {
                  itemDetail.ProdpkgId =
                      int.parse(itemPackingDataList[0].packing_value);
                  itemDetail.PackageName = itemPackingDataList[0].packing_text;
                }

                /**
                 * Load unique item info with barcode based on selected packing, using the item id
                 * */
                _findUniqueItemWithItemCodeAndPackingId();
                FocusScope.of(context).unfocus();
                _loadCountry();
              });
            }
          }
          else if (state is LoadPackingByIdComplete) {
            Navigator.pop(context);
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;

                itemDetail.PackageName = itemPackingDataList
                    .where((packing) =>
                        packing.packing_value ==
                        itemDetail.ProdpkgId.toString())
                    .toList()[0]
                    .packing_text;
              });
            }
          }
          else if (state is GetScmItemPriceDetailsComplete) {
            Navigator.pop(context);

            if (state.getPackingDetailsResp != null &&
                state.getPackingDetailsResp.masterDataList.length > 0) {
              setState(() {
                List<GetScmItemPriceList> packingDetails =
                    state.getPackingDetailsResp.masterDataList;
                if (packingDetails.isNotEmpty) {
                  itemDetail.UnitCost =
                      state.getPackingDetailsResp.masterDataList[0].unitPrice ??
                          (itemDetail.UnitCost ?? 0.0);
                  itemDetail.AvailableQty = state.getPackingDetailsResp
                      .masterDataList[0].availableQty ??
                      0.0;
                }
              });
            }
          }
          else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.CountryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }

            _loadProductBrand();
          } else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.BrandName = productBrandList[0].brand_name.trim();
              });
            }
          } else if (state is GetWareHouseComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On || type == TransactionType.SR) {
                  _loadZone();
                } else {
                  showItemLocationDialogue(context);
                }
              });
            }
          } else if (state is GetWareHouseZoneComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseZoneList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On || type == TransactionType.SR) {
                  _loadRack();
                }
              });
            }
          } else if (state is GetWareHouseRackComplete) {
            Navigator.pop(context);

            if (state.getCommonResp != null &&
                state.getCommonResp.masterDataList.length > 0) {
              setState(() {
                wareHouseRackList = state.getCommonResp.masterDataList;
                if (editMode == EditMode.On || type == TransactionType.SR) {
                  _loadBin();
                }
              });
            }
          } else if (state is GetWareHouseBinComplete) {
            Navigator.pop(context);

            if (state.getWarehouseBinMasterResp != null &&
                state.getWarehouseBinMasterResp.masterDataList.length > 0) {
              setState(() {
                wareHouseBinList =
                    state.getWarehouseBinMasterResp.masterDataList;
                if (editMode == EditMode.On || type == TransactionType.SR) {
                  showItemLocationDialogue(context);
                }
              });
            }
          }
          else if (state is GetTransferReceiptReferenceDetailsComplete){
            Navigator.pop(context);

            if (state.resp.data != null &&
                state.resp.data.itemDetails.isNotEmpty) {
              setState(() {
                SaveTRReq.shared.dateFormat = state.resp.data.dateFormat;
                srRefItemList = state.resp.data.itemDetails;
                print(state.resp.data.toJson());
                // if (editMode == EditMode.On) {
                //   _getItemDetails();
                // }
              });
            } else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }
          else if (state is GetMrRefDetailsComplete){
            Navigator.pop(context);
            if (state.resp.data.itemDetails.isNotEmpty) {
              SIReq.shared.dateFormat = state.resp.data.dateFormat;
              mrRefItemList = state.resp.data.itemDetails;
              print(state.resp.toJson());

              if (editMode == EditMode.On) {

                /// Checks for item using item referenceDtId in the list
                List<MrRefItemsList> items = mrRefItemList.where((item) => (item.mtlReqDtId == itemDetail.referenceDtId)).toList();

                if (items.isNotEmpty) {
                  setState(() {
                    refItemForSubstitute = items[0];
                    hasEnabledSubstituteMode = itemDetail.isSubstitute;
                  });

                  if (_itemInputType == InputType.ItemCode) {
                    _loadPackingList();
                  }
                  else{
                    _loadCountry();
                  }
                }
              }
            }
            else {
              Common().showAlertMessage(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: 'Selected reference doesn\'t contain any items',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }
        },
        child: BlocBuilder<ItemEntryBloc, ItemEntryState>(
            builder: (context, state) {
              _itemEntryBloc = BlocProvider.of<ItemEntryBloc>(context);

              if (state is GetMaterialHeaderIdInitial) {
                if ((type == TransactionType.SI ||
                    type == TransactionType.II ||
                    type == TransactionType.MTO) && (toAddress.transferTypeId == SIType.From_MR.index))  {
                  _itemEntryBloc.add(GetMrRefDetails(
                      refId: toAddress.storeIssueId.toString(),
                      type: widget.type));
                }
                else if (editMode == EditMode.On) {
                  if (type != TransactionType.SR && type != TransactionType.MTI) {
                    _getItemDetails();
                  }
                }
                else {
                  if (type == TransactionType.SR) {
                    _itemEntryBloc.add(
                        GetSRRefDetails(refId: toAddress.storeIssueId.toString()));
                  } else if (type == TransactionType.MTI) {
                    _itemEntryBloc.add(
                        GetTransferReceiptReferenceDetails(
                            refId: toAddress.storeIssueId.toString()));
                  }
                }
              }

              return Container(
                child: Column(
                  children: [
                    if (hasEnabledSubstituteMode) Row(
                      children: [
                        Expanded(child: Text("Add substitute item for ${refItemForSubstitute.itemName}")),
                        IconButton(icon: Icon(Icons.clear, color: Colors.red), onPressed: (){
                          _didSelect(InputType.Barcode);
                          setState(() {
                            refItemForSubstitute = null;
                            hasEnabledSubstituteMode = true;
                          });
                        }),
                        SizedBox(height: 10),
                      ],
                    ),
                    _getItemCodeFields(),
                    if (_itemInputType == InputType.ItemCode) _getPackageField(),
                    if (itemDetail.packingDesc != null) _getPackingDescrField(),
                    if (containIsSubstitute) _getSubstituteField(),
                    _getQuantityField(),
                    _getItemDetailsField()
                  ],
                ),
              );
            }),
      ),
    );
  }

  _getItemCodeFields(){
    return editMode == EditMode.Off ? Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.Barcode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.ItemCode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Code", style: TextStyle(fontSize: 13.0))
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 55,
                child: TextFormField(
                  enableSuggestions: false,
                  textInputAction: (_itemInputType == InputType.Barcode)
                      ? TextInputAction.next
                      : TextInputAction.search,
                  controller: _itemCodeController,
                  focusNode: itemCodeFocusNode,

                  autocorrect: false,
                  decoration: Common().getBCoreMandatoryID(
                      isMandatory: true,
                      isValidated: _itemCodeController.text.trim().isNotEmpty,
                      hintText: (_itemInputType == InputType.Barcode)
                          ? 'Item Barcode'
                          : 'Item Code',
                      icon: _itemInputType == InputType.Barcode ? Icons.blur_linear : Icons.search,
                      iconPress: () async{
                        if (_itemInputType == InputType.Barcode) {
                          var result = await BarcodeScanner.scan();
                          setState(() {
                            _itemCodeController.text = result;
                            _getItemDetails();
                          });
                        }
                        else{
                          _getItemDetails();
                        }
                      }),
                  keyboardType: TextInputType.name,
                  validator: (value){
                    _getItemDetails();
                    return null;
                  },
                  // onChanged: (value) {
                  //   // Load item details from DB
                  //   if (_itemInputType == InputType.Barcode) {
                  //     _getItemDetails();
                  //   }
                  //
                  // },
                  onFieldSubmitted:  (value) {
                    _getItemDetails();
                  },
                ),
              ),
            ),
            if ((type == TransactionType.SI ||
                type == TransactionType.II ||
                type == TransactionType.MTO) && (toAddress.transferTypeId == SIType.From_MR.index))
              SizedBox(width: 10),
            if ((type == TransactionType.SI ||
                type == TransactionType.II ||
                type == TransactionType.MTO) && (toAddress.transferTypeId == SIType.From_MR.index))
              Container(
                decoration: Common().getBCoreSD(
                    isMandatory: true, isValidated: true),
                width: 55,
                height: 55,
                child: IconButton(icon: Icon(Icons.list_alt, color: BCAppTheme().primaryColor,),
                    iconSize: 20,
                    onPressed: (){
                      showRefItemsList();
                    }),
              )
          ],
        ),
      ],
    ):
        Column(
          children: [
            ElevatedButton(onPressed: (){
              editMode = EditMode.Off;
              hasEnabledSubstituteMode = false;

              _didSelect(InputType.Barcode);
              itemCodeFocusNode.requestFocus();
            }, child: Text("Clear & Add New"))
          ],
        )
    ;
  }

  showRefItemsList() {
    Dialog itemListDialogue = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter dropDownState) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Items List", textAlign: TextAlign.center),
                      SizedBox(height: 16.0),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 45,
                          child: TextField(
                            onChanged: (value) {
                              dropDownState(() {});
                            },
                            controller: _searchController,
                            decoration: Common().getBCoreMandatoryID(
                                isMandatory: false,
                                isValidated: true,
                                hintText: 'Search',
                                icon: Icons.search),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: mrRefItemList.length,
                            itemBuilder: (context, index) {
                              if (_searchController.text.isEmpty) {
                                return RefItemRow(
                                  hasAdded: CommonItemViewModel.shared.cmnItemList.where((item) => item.referenceDtId == mrRefItemList[index].mtlReqDtId).toList().isNotEmpty,
                                  item: mrRefItemList[index],
                                );}
                              else if (mrRefItemList[index]
                                  .itemName
                                  .toLowerCase()
                                  .contains(_searchController.text.toLowerCase())) {
                                return RefItemRow(
                                  hasAdded: CommonItemViewModel.shared.cmnItemList.where((item) => item.referenceDtId == mrRefItemList[index].mtlReqDtId).toList().isNotEmpty,
                                  item: mrRefItemList[index],
                                );
                              }
                              else {
                                return Container();
                              }
                            }),
                      ),
                      SizedBox(height: 8),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Ok",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);

                                    setState(() {

                                    });
                                  }),
                            ),
                          )
                        ],
                      )
                    ],
                  );
                })),
      ),
    );

    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext myContext) {
        return itemListDialogue;
      },
    );
  }

  _getPackageField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: true, isValidated: itemDetail.ProdpkgId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Text(""),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.ProdpkgId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                  return DropdownMenuItem(
                      child: Text(item.packing_text,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: BCAppTheme().subTextColor,
                          )),
                      value: int.parse(item.packing_value));
                }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.ProdpkgId = value;
                    itemDetail.PackageName = itemPackingDataList
                        .where((package) =>
                    package.packing_value == value.toString())
                        .toList()[0]
                        .packing_text;

                    /**
                     * Load unique item info with barcode based on selected packing, using the item id
                     * */
                    _findUniqueItemWithItemCodeAndPackingId();
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  _getPackingDescrField(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Text(itemDetail.packingDesc ?? "", textAlign: TextAlign.left),
            ),
          ],
        ),
      ],
    );
  }

  _getSubstituteField() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Checkbox(
              checkColor: BCAppTheme().secondaryColor,
              value: itemDetail.IsSubstituteAllowed ?? false,
              onChanged: (bool value) {
                setState(() {
                  itemDetail.IsSubstituteAllowed = value;
                });
              },
            ),
            Text("Is Substitute?"),
          ],
        ),
      ],
    );
  }

  _getItemDetailsField() {
    return Column(
      children: [
        SizedBox(height: 10),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 8,
                child: Container(
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 0.5,
                          style: BorderStyle.solid,
                          color: BCAppTheme().headingTextColor),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text("Item details of  ${itemDetail.ItemCode?? "-"}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().headingTextColor)),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              // flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.scatter_plot, size: 10),
                                    SizedBox(width: 4.0),
                                    Flexible(
                                      child: Text(
                                        itemDetail.BrandName != null
                                            ? itemDetail.BrandName
                                            : "--",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 13.0),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                      ),
                                    ),
                                  ],
                                )),
                            SizedBox(width: 10),
                            Expanded(
                              // flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.public, size: 12),
                                    SizedBox(width: 4.0),
                                    Flexible(
                                      child: Text(
                                        itemDetail.CountryName != null
                                            ? itemDetail.CountryName
                                            : "--",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 13.0),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                      ),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                        if (containPrice) _getPriceFields(),
                        if (containGetAvailableQtyField)_getAvailableQtyField(),
                        if (containLocation) _getItemLocation(),
                        if (containInstallationRequired)
                          _getInstallationRequiredField()
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: FlatButton(
                    padding: EdgeInsets.zero,
                    color: BCAppTheme().headingTextColor,
                    onPressed: () {
                      // Validate input
                      if (_validateData()) {
                        saveItemDetails(lineNum != 1 ? lineNum++ : lineNum);
                        // writeToFile(materialRequest);
                        // resetItemParameters();
                        // }
                      }
                    },
                    child: Icon(
                      Icons.save,
                      size: 18,
                      color: Colors.white,
                    )),
              )
            ],
          ),
        ),
      ],
    );
  }

  _getPriceFields() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text("Unit",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    Text((itemDetail.UnitCost ?? 0.0).toString(),
                        style: TextStyle(fontSize: 13.0))
                  ],
                )),
            Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Net",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: BCAppTheme().grayColor, fontSize: 10.0)),
                        ImageIcon(
                          AssetImage('assets/common/money_icon/money_icon.png'),
                          size: 15.0,
                        )
                        // Icon(Icons.attach_money,
                        //     size: 12, color: BCAppTheme().grayColor)
                      ],
                    ),
                    Text(_getTotalCost(), style: TextStyle(fontSize: 13.0))
                  ],
                ))
          ],
        ),
      ],
    );
  }

  _getTotalCost() {
    return ((itemDetail.UnitCost ?? 0.0) * (itemDetail.ReqQty ?? 1.0))
        .trim()
        .toString();
  }

  _getAvailableQtyField() {
    if (itemDetail.AvailableQty != null) {
      return Column(
        children: [
          SizedBox(height: 10),
          Row(
            children: [
              Text(" Available Quantity: ${itemDetail.AvailableQty}",
                  textAlign: TextAlign.left),
            ],
          )
        ],
      );
    } else {
      return SizedBox(height: 0.0);
    }
  }

  _getQuantityField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 55,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.done,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: true,
                isValidated: itemDetail.ReqQty != null,
                hintText: 'Quantity'),
            onChanged: (value) {
              setState(() {
                itemDetail.ReqQty = (value != null && value != '')
                    ? value.toDouble()
                    : null;
              });
            },
            onSubmitted: (value) {
              setState(() {
                itemDetail.ReqQty = (value != null && value != '')
                    ? value.toDouble()
                    : null;
                if (_validateData()) {
                  saveItemDetails(lineNum != 1 ? lineNum++ : lineNum);
                }
              });
            },
            keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
          ),
        ),
      ],
    );
  }

  _setItemInfo({ItemBarcodeData item, SRRefItemDetails refItem}) {
    itemDetail.itIndex = _itemInputType.index;

    if (type == TransactionType.SR || type == TransactionType.MTI) {
      itemDetail.ItemCode = refItem.itemCode;
      itemDetail.ItemId = refItem.itemId;
      itemDetail.ItemBarcode = refItem.itemBarcode;
      itemDetail.ItemName = refItem.itemName;
      itemDetail.ProdpkgId = refItem.packingId;
      itemDetail.PackageName = refItem.pkgName;
      itemDetail.packingDesc = refItem.packingDesc;
      itemDetail.UnitCost = refItem.unitPrice;
      itemDetail.unitQty = refItem.unitQty;

      itemDetail.NetCost = refItem.netAmount;
      itemDetail.CountryId = refItem.countryId;
      itemDetail.BrandId = refItem.brandId;
      itemDetail.BrandName = refItem.brandName;
      itemDetail.CountryName = refItem.countryName;
      itemDetail.storeIssueId = toAddress.storeIssueId;
      itemDetail.storeIssueDetailId = refItem.referenceDtId;
      itemDetail.batchNo = refItem.batchNo;

      if (refItem.productStockTypeId == ProductStockType.Expiry.index) {
        itemDetail.expiryDate = Utils.getDateByFormat(
            format: fromAddress.fromCompanyDateFormat,
            passedDateTime: DateTime.tryParse(refItem.expiryDate));
      }

      containInstallationRequired = refItem.isAssetItem.toBool();
      itemDetail.isAssetItem = refItem.isAssetItem;
      itemDetail.isInstallationChecked = false;
      selectedWarehouse = refItem.warehouseId.toString();
      selectedWarehouseZone = refItem.wHouseZoneId.toString();
      selectedWarehouseRack = refItem.wHouseZoneBinRackId.toString();
      selectedWarehouseBin = refItem.wHouseZoneBinId.toString();
      itemDetail.warehouseId = refItem.warehouseId;
      itemDetail.wHouseZoneBinId = refItem.wHouseZoneBinId;
      itemDetail.wHouseZoneId = refItem.wHouseZoneId;
      itemDetail.wHouseZoneBinRackId = refItem.wHouseZoneBinRackId;

      itemLoaded = true;
      _setItemName();
    }
    else
    {
      itemDetail.ItemCode = item.ItemCode;
      itemDetail.ItemId = item.ItemId;
      itemDetail.ItemName = item.ItemName;
      itemDetail.CountryId = item.OriginCountryId;
      itemDetail.BrandId = item.BrandId;
      itemDetail.pkgQty = item.PkgQty.toDouble();
      itemDetail.packingDesc = item.PackingDesc;

      if (_itemInputType == InputType.Barcode){
        itemDetail.ItemBarcode = item.Text;
      }

      if (editMode == EditMode.Off) {
        itemDetail.ProdpkgId = item.ItemPackingId;
        itemDetail.UnitCost = item.AverageCost.toDouble();
        itemDetail.NetCost = item.NetCost.toDouble();
      }
      if(hasEnabledSubstituteMode){

        double approvedQtyInBasePacking = (refItemForSubstitute.approvedQty * refItemForSubstitute.pkgQty).trim();
        double issueTransferQtyInBasePacking = (refItemForSubstitute.issueTransferQty * refItemForSubstitute.pkgQty).trim();

        itemDetail.approvedQty = approvedQtyInBasePacking/ itemDetail.pkgQty;
        itemDetail.issueTransferQty = issueTransferQtyInBasePacking/ itemDetail.pkgQty;
      }
    }
  }

  _setItemInfoFromRef({MrRefItemsList refItem}) {
    itemDetail.ItemCode = refItem.itemCode;
    itemDetail.ItemId = refItem.itemId;
    itemDetail.ItemBarcode = refItem.itemBarcode;
    itemDetail.ItemName = refItem.itemName;
    itemDetail.ProdpkgId = refItem.packingId;
    itemDetail.PackageName = refItem.pkgName;
    itemDetail.packingDesc = refItem.packingDesc;
    itemDetail.UnitCost = refItem.unitPrice;

    itemDetail.approvedQty = refItem.approvedQty;
    itemDetail.issueTransferQty = refItem.issueTransferQty;
    itemDetail.AvailableQty = refItem.availableQty;
    itemDetail.pkgQty = refItem.pkgQty;

    itemDetail.IsSubstituteAllowed = refItem.isSubstituteAllowed.toBool();

    // itemDetail.NetCost = refItem.netAmount;
    itemDetail.CountryId = refItem.countryId;
    itemDetail.BrandId = refItem.brandId;
    itemDetail.BrandName = refItem.brandName;
    itemDetail.CountryName = refItem.countryName;

    itemDetail.referenceDtId = refItem.mtlReqDtId;
    itemDetail.mtlReqHdId = refItem.mtlReqHdId;
    itemDetail.mtlReqDtId = refItem.mtlReqDtId;

    itemLoaded = true;
    _setItemName();

    if(itemDetail.AvailableQty <= 0.0 && itemDetail.IsSubstituteAllowed) {
      Common().showAlertMessageWithAction(
          context: context,
          title: Constants.alertMsgTitle,
          message: 'Item seems Out of Stock. Do you want to add Substitute',
          okButtonTitle: "Cancel",
          okFunction: (){
            Navigator.pop(context);
            _didSelect(InputType.Barcode);
          },
          actionButtonTitle: "Ok",
          actionFunction: (){
            Navigator.pop(context);
            _didSelect(InputType.Barcode);

            refItemForSubstitute = refItem;
            itemCodeFocusNode.requestFocus();
            setState(() {
              hasEnabledSubstituteMode = true;
            });
          }
      );
    }
  }

  _getItemDetails() {

    if (type == TransactionType.SR || type == TransactionType.MTI) {
      _loadItemFromRef();
    }
    else if ((type == TransactionType.SI ||
        type == TransactionType.II ||
        type == TransactionType.MTO) &&
        (toAddress.transferTypeId == SIType.From_MR.index))
    {
      if (hasEnabledSubstituteMode){
        _loadItemFromDB();
      }
      else {
        _loadItemFromMRRef();
      }
    }
    else {
      /// loads item from DB when type is MR
      _loadItemFromDB();
    }
  }

  _loadItemFromRef(){
    List<SRRefItemDetails> srRefItem;
    /// If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {

      srRefItem = srRefItemList
          .where((item) => item.itemCode == _itemCodeController.text.trim())
          .toList();
    }
    else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {

      srRefItem = srRefItemList
          .where(
              (item) => item.itemBarcode == _itemCodeController.text.trim())
          .toList();
    }
    if (srRefItem.isNotEmpty) {

      /// Check whether already the item is added or not
      if ( CommonItemViewModel.shared.cmnItemList
          .where((item) =>
      item.ItemBarcode == srRefItem[0].itemBarcode)
          .toList()
          .isNotEmpty) {
        showAlert(context: context, message: 'Item already added');
        _didSelect(InputType.Barcode);
        return;
      }

      setState(() {
        _setItemInfo(refItem: srRefItem[0]);
      });
    }
    else{
      showAlert(context: context, message: 'Item not found');
    }
  }

  _loadItemFromMRRef(){
    List<MrRefItemsList> srRefItems;

    /// If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {

      srRefItems = mrRefItemList
          .where((item) => item.itemCode == _itemCodeController.text.trim())
          .toList();
    }
    else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      srRefItems = mrRefItemList
          .where(
              (item) => item.itemBarcode == _itemCodeController.text.trim())
          .toList();
    }

    if ( srRefItems.isNotEmpty) {

      /// Check whether already the item is added or not
      if ( _itemInputType == InputType.Barcode &&
          (hasEnabledSubstituteMode == false) &&
          CommonItemViewModel.shared.cmnItemList
          .where((item) =>
      item.ItemBarcode == srRefItems[0].itemBarcode)
          .toList()
          .isNotEmpty) {
        showAlert(context: context, message: 'Item already added');
        _didSelect(InputType.Barcode);
        return;
      }

      setState(() {
        itemDetail.itIndex = _itemInputType.index;
        _setItemInfoFromRef(refItem: srRefItems[0]);
      });

      if (_itemInputType == InputType.ItemCode) {
        _loadPackingList();
      }
      else{
        _loadCountry();
      }
    }
    else{
      showAlert(context: context, message: 'Selected reference doesn\'t contain the item');
    }
  }

  _loadItemFromDB() {
    // If item barcode or item code entered by user, get item details
    if (_itemInputType == InputType.ItemCode &&
        _itemCodeController.text.isNotEmpty) {
      _itemEntryBloc
          .add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
    } else if (_itemInputType == InputType.Barcode &&
        _itemCodeController.text.isNotEmpty) {
      _itemEntryBloc
          .add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
    }
  }

  void saveItemDetails(int lineNum) {
    itemDetail.AppStatus = 1;
    itemDetail.Comments = '';
    itemDetail.IsNewItem = true;
    // itemDetail.IsSubstituteAllowed = hasOptedSubstitute ?? false;
    itemDetail.ItemDesc = itemDetail.ItemName ?? "";
    itemDetail.LineNum = lineNum ?? 0;
    itemDetail.mtlReqHdId = itemDetail.mtlReqHdId ?? 0;
    itemDetail.ProdClassId = 1;
    itemDetail.ProductId = 1;
    itemDetail.TotalCost = (itemDetail.UnitCost * itemDetail.ReqQty).trim();

    if ((type == TransactionType.SI ||
        type == TransactionType.II ||
        type == TransactionType.MTO) &&
        (toAddress.transferTypeId == SIType.From_MR.index))
    {
      if(hasEnabledSubstituteMode) {
        itemDetail.referenceDtId = refItemForSubstitute.mtlReqDtId;
        itemDetail.mtlReqHdId = refItemForSubstitute.mtlReqHdId;
        itemDetail.mtlReqDtId = refItemForSubstitute.mtlReqDtId;
        itemDetail.isSubstitute = true;

        if (editMode == EditMode.Off) {
          SubstituteItemDetails subItemDetail = SubstituteItemDetails(
              mtlReqHdId: itemDetail.mtlReqHdId,
              mtlReqDtId: itemDetail.mtlReqDtId,
              packingId: itemDetail.ProdpkgId,
              substituteId: itemDetail.ItemId,
              unitQty: itemDetail.ReqQty);
          sub_details.add(subItemDetail);
        }
        else{
          SubstituteItemDetails subItemDetail = sub_details.where((subItem) => (subItem.mtlReqDtId == itemDetail.referenceDtId)).toList()[0];
          subItemDetail.mtlReqHdId = itemDetail.mtlReqHdId;
          subItemDetail.mtlReqDtId = itemDetail.mtlReqDtId;
          subItemDetail.packingId = itemDetail.ProdpkgId;
          subItemDetail.substituteId = itemDetail.ItemId;
          subItemDetail.unitQty = itemDetail.ReqQty;
        }
      }else{
        itemDetail.isSubstitute = false;
      }
    }

    if (editMode == EditMode.On) {
      var indexOfItemInExistingList = CommonItemViewModel.shared.cmnItemList
          .indexWhere((item) => item.ItemBarcode == itemDetail.ItemBarcode);
      if (indexOfItemInExistingList != -1) {
        CommonItemViewModel.shared.cmnItemList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CommonItem.fromJson(itemDetail.toJson())]);
        showAlert(context: context, message: 'Updated!');
      } else {
        showAlert(context: context, message: 'Please try again');
      }
    }
    else {
      CommonItemViewModel.shared.cmnItemList.insert(
          CommonItemViewModel.shared.cmnItemList.length,
          CommonItem.fromJson(itemDetail.toJson()));
      showAlert(context: context, message: 'Saved!');
    }

    editMode = EditMode.Off;
    hasEnabledSubstituteMode = false;

    _didSelect(InputType.Barcode);
    itemCodeFocusNode.requestFocus();
  }

  void getMatReqDetailLineNum(int id) {
    _itemEntryBloc.add(GetMaterialDetailLineNum(matReqId: id));
  }

  bool _validateData() {
    if (itemLoaded) {
      /// Check whether already the item is added or not
      if ( editMode == EditMode.Off &&
          CommonItemViewModel.shared.cmnItemList
          .where((item) => item.ItemBarcode == itemDetail.ItemBarcode)
          .toList()
          .isNotEmpty) {
        showAlert(context: context, message: 'Item already added');
        return false;
      }

      if (type == TransactionType.SR || type == TransactionType.MTI) {
        if (itemDetail.ReqQty == null) {
          Common().showAlertMessage(
              context: context,
              title: Constants.alertMsgTitle,
              message: 'Enter quantity',
              okFunction: () {
                Navigator.pop(context);
                quantityFocusNode.requestFocus();
              });
        }
        else if (itemDetail.ReqQty != null && itemDetail.unitQty != null) {
          if (itemDetail.ReqQty > itemDetail.unitQty) {
            Common().showAlertMessage(
                context: context,
                title: Constants.alertMsgTitle,
                message: 'Quantity should be less than or equal to available quantity',
                okFunction: () {
                  Navigator.pop(context);
                  quantityFocusNode.requestFocus();
                });
          }
          else {
            return true;
          }
        }
      }
      else {
        if (itemDetail.ProdpkgId == null) {
          Common().showAlertMessage(
              context: context,
              title: Constants.alertMsgTitle,
              message: 'Select packing',
              okFunction: () {
                Navigator.pop(context);
              });
        }
        else if (itemDetail.ReqQty == null) {
          Common().showAlertMessage(
              context: context,
              title: Constants.alertMsgTitle,
              message: 'Enter quantity',
              okFunction: () {
                Navigator.pop(context);
                quantityFocusNode.requestFocus();
              });
        }
        else if ((type == TransactionType.SI ||
            type == TransactionType.II ||
            type == TransactionType.MTO) &&
            (toAddress.transferTypeId == SIType.From_MR.index) &&
            (itemDetail.ReqQty > (itemDetail.approvedQty - itemDetail.issueTransferQty))
        ){
          Common().showAlertMessage(
              context: context,
              title: Constants.alertMsgTitle,
              message: 'Enter quantity less than or equal to Required quantity',
              okFunction: () {
                Navigator.pop(context);
                quantityFocusNode.requestFocus();
              });
        }
        else if (type != TransactionType.MR &&
            itemDetail.ReqQty != null &&
            itemDetail.AvailableQty != null) {
          if (itemDetail.ReqQty > itemDetail.AvailableQty) {
            Common().showAlertMessage(
                context: context,
                title: Constants.alertMsgTitle,
                message : 'Enter the Quantity less than or equal to available quantity',
                // message: 'Enter quantity less than Available Quantity',
                okFunction: () {
                  Navigator.pop(context);
                  quantityFocusNode.requestFocus();
                });
          } else {
            return true;
          }
        } else {
          return true;
        }
      }
    } else {
      Common().showAlertMessage(
          context: context,
          title: 'Alert',
          message: 'Enter barcode or item code',
          okFunction: () {
            Navigator.pop(context);
            itemCodeFocusNode.requestFocus();
          });
    }
    return false;
  }

  void _setItemName() {
    _itemCodeController.text = itemDetail.ItemName;
  }

  _getItemLocation() {
    return Column(
      children: [
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            print("Location Tapped");
            if (itemLoaded) {
              if (wareHouseList.isEmpty) {
                _loadWareHouse();
              } else {
                showItemLocationDialogue(context);
              }
            } else {
              Common().showMessage(
                  context: context,
                  message: "Please enter Item Barcode/ Item Code");
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 16),
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: BCAppTheme().headingTextColor),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
            child: Row(
              children: [
                Expanded(child: Text("Item Location")),
                SizedBox(width: 8),
                Icon(Icons.edit, size: 15, color: BCAppTheme().primaryColor)
              ],
            ),
          ),
        )
      ],
    );
  }

  showItemLocationDialogue(BuildContext blocContext) {
    Dialog locationAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Flexible(
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Item Location", textAlign: TextAlign.center),
                SizedBox(height: 16.0),
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Expanded(
                    child: StatefulBuilder(builder:
                        (BuildContext context, StateSetter dropDownState) {
                      return Column(
                        children: [
                          SizedBox(
                            height: 50,
                            child: Container(
                              decoration: Common().getBCoreSD(
                                  isMandatory: true, isValidated: true),
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: DropdownButton(
                                    underline: Container(),
                                    isDense: false,
                                    isExpanded: true,
                                    hint: Text("Warehouse"),
                                    value: selectedWarehouse,
                                    items: wareHouseList
                                        .map((cmn.CommonDataList wareHouse) {
                                      return DropdownMenuItem(
                                          child: Text(wareHouse.text,
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: BCAppTheme().subTextColor,
                                              )),
                                          value: wareHouse.value);
                                    }).toList(),
                                    onChanged: (value) {
                                      dropDownState(() {
                                        selectedWarehouse = value;
                                        if (selectedWarehouse != null) {
                                          _loadZone();
                                        }
                                      });
                                    },
                                  )),
                            ),
                          ),
                          SizedBox(height: 8),
                          SizedBox(
                            height: 50,
                            child: Container(
                              decoration: Common().getBCoreSD(
                                  isMandatory: true, isValidated: true),
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: DropdownButton(
                                    underline: Container(),
                                    isDense: false,
                                    isExpanded: true,
                                    hint: Text("Zone"),
                                    value: selectedWarehouseZone,
                                    items: wareHouseZoneList
                                        .map((cmn.CommonDataList wareHouseZone) {
                                      return DropdownMenuItem(
                                          child: Text(wareHouseZone.text,
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: BCAppTheme().subTextColor,
                                              )),
                                          value: wareHouseZone.value);
                                    }).toList(),
                                    onChanged: (value) {
                                      dropDownState(() {
                                        selectedWarehouseZone = value;
                                        if (selectedWarehouseZone != null) {
                                          _loadRack();
                                        }
                                      });
                                    },
                                  )),
                            ),
                          ),
                          SizedBox(height: 8),
                          SizedBox(
                            height: 50,
                            child: Container(
                              decoration: Common().getBCoreSD(
                                  isMandatory: true, isValidated: true),
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: DropdownButton(
                                    underline: Container(),
                                    isDense: false,
                                    isExpanded: true,
                                    hint: Text("Rack"),
                                    value: selectedWarehouseRack,
                                    items: wareHouseRackList
                                        .map((cmn.CommonDataList wareHouseRack) {
                                      return DropdownMenuItem(
                                          child: Text(wareHouseRack.text,
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: BCAppTheme().subTextColor,
                                              )),
                                          value: wareHouseRack.value);
                                    }).toList(),
                                    onChanged: (value) {
                                      dropDownState(() {
                                        selectedWarehouseRack = value;
                                        if (selectedWarehouseRack != null) {
                                          _loadBin();
                                        }
                                      });
                                    },
                                  )),
                            ),
                          ),
                          SizedBox(height: 8),
                          SizedBox(
                            height: 50,
                            child: Container(
                              decoration: Common().getBCoreSD(
                                  isMandatory: true, isValidated: true),
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: DropdownButton(
                                    underline: Container(),
                                    isDense: false,
                                    isExpanded: true,
                                    hint: Text("Bin"),
                                    value: selectedWarehouseBin,
                                    items: wareHouseBinList
                                        .map((WarehouseBinList wareHouseBin) {
                                      return DropdownMenuItem(
                                          child: Text(wareHouseBin.text,
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: BCAppTheme().subTextColor,
                                              )),
                                          value: wareHouseBin.value);
                                    }).toList(),
                                    onChanged: (value) {
                                      dropDownState(() {
                                        selectedWarehouseBin = value;
                                      });
                                    },
                                  )),
                            ),
                          ),
                          SizedBox(height: 16),
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 35,
                                  decoration: ShapeDecoration(
                                    shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          width: 0.5,
                                          style: BorderStyle.solid,
                                          color: BCAppTheme().primaryColor),
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                    ),
                                  ),
                                  child: FlatButton(
                                      child: Text("Cancel",
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 11)),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      }),
                                ),
                              ),
                              SizedBox(width: 16.0),
                              Expanded(
                                child: Container(
                                  height: 35,
                                  child: FlatButton(
                                      color: BCAppTheme().primaryColor,
                                      child: Text("Save",
                                          style: TextStyle(
                                              color: BCAppTheme().secondaryColor,
                                              fontSize: 11)),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        if (_validateItemLocation()) {
                                          setState(() {
//                                            Navigator.pop(context);
                                          });
                                        } else {}
                                      }),
                                ),
                              )
                            ],
                          )
                        ],
                      );
                    }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return locationAlert;
      },
    );
  }

  bool _validateItemLocation() {
    return (selectedWarehouse != null && selectedWarehouse.length > 0) ||
        (selectedWarehouseZone != null && selectedWarehouseZone.length > 0) ||
        (selectedWarehouseBin != null && selectedWarehouseBin.length > 0) ||
        (selectedWarehouseRack != null && selectedWarehouseRack.length > 0);
  }

  _getInstallationRequiredField() {
    return Row(
      children: [
        Checkbox(
          value: itemDetail.isInstallationChecked ?? false,
          onChanged: (bool value) {
            setState(() {
              itemDetail.isInstallationChecked = value;
            });
          },
        ),
        Text("Installation Required?"),
      ],
    );
  }

  /// Load unique item info with barcode based on selected packing, using the item id
  void _findUniqueItemWithItemCodeAndPackingId() {
    if (itemDetail.ItemId != null && itemDetail.ProdpkgId != null) {
      _itemEntryBloc.add(LoadItemByPacking(
          itemId: itemDetail.ItemId.toString(),
          prodPkgId: itemDetail.ProdpkgId.toString()));
    }
  }

  void _getItemPriceDetails() {
    _itemEntryBloc.add(GetScmItemPriceDetails(
        type: type,
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        toCompanyId: toAddress.companyId,
        toDivisionId: toAddress.divisionId,
        toLocationId: toAddress.locationId,
        itemId: itemDetail.ItemId.toString(),
        packingId: itemDetail.ProdpkgId.toString()));
  }

  void _loadPackingList() {
    _itemEntryBloc.add(LoadPacking(itemId: itemDetail.ItemId.toString()));
  }

  void _loadItemPackingById() {
    _itemEntryBloc.add(
        LoadPackingById(packId: itemDetail.ProdpkgId.toString(), itemId:  itemDetail.ItemId.toString()));
  }

  void _loadCountry() {
    _itemEntryBloc
        .add(LoadCountryById(countryId: itemDetail.CountryId.toString()));
  }

  void _loadProductBrand() {
    _itemEntryBloc
        .add(LoadBrandById(brandId: itemDetail.BrandId.toString()));
  }

  void _loadWareHouse() {
    _itemEntryBloc.add(GetWareHouse());
  }

  void _loadZone() {
    _itemEntryBloc.add(GetWareHouseZone(warehouseId: selectedWarehouse));
  }

  void _loadRack() {
    _itemEntryBloc.add(GetWareHouseRack(zoneId: selectedWarehouseZone));
  }

  void _loadBin() {
    _itemEntryBloc.add(GetWareHouseBin(rackId: selectedWarehouseRack));
  }




  // left scan keyCode
  static final int LEFT_SCAN_KEY = 132;
// right scan keyCode
  static final int RIGHT_SCAN_KEY = 133;
// scan keyCode
  static final int SCAN_KEY = 165;
// bluetooth scan keyCode (i'm not test)
  static final int BLUETOOTH_SCAN_KEY = 113;

  static bool isOnScan(int keyCode) {
    return keyCode == LEFT_SCAN_KEY ||
        keyCode == RIGHT_SCAN_KEY ||
        keyCode == SCAN_KEY ||
        keyCode == BLUETOOTH_SCAN_KEY;
  }

}

class Item {
  final int itemId;
  final String itemName;

  Item(this.itemId, this.itemName);
}

class RefItemRow extends StatelessWidget {
  final bool hasAdded;
  final MrRefItemsList item;
  const RefItemRow(
      {Key key, this.hasAdded, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      child: ListTile(
        // tileColor: hasAdded ? BCAppTheme().greenColor : Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
            children: <Widget>[
              Expanded(
                child:
                Column(
                    children: [
                      if (hasAdded) Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child:  CircleAvatar(
                              backgroundColor: BCAppTheme().primaryColor,
                              radius: 10,
                              child: Icon(Icons.done,size: 12, color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                    children: [
                                      Icon(Icons.radio_button_on,size: 10.0,),
                                      SizedBox(width: 2.0,),
                                      Text(item.itemCode,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: BCAppTheme().textColor,
                                          fontWeight: FontWeight.bold),
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: true),]
                                ),
                                getBarcodeTextField(),
                              ],
                            ),
                          ),
                          SizedBox(width: 8.0),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text("Item Name",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        color: BCAppTheme().textColor,
                                        fontWeight: FontWeight.bold),
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: true),
                                getItemNameField(),
                              ],
                            ),
                          )
                        ],
                      ),
                    ]
                ),
              )
            ]
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getBrandNameField(),
                      getPackageNameField(),
                      getSubstField(),
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      getOrginNameFiled(),
                      getPkgDesc(),
                      getQuantityField(),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }

  getBarcodeTextField() {
    String itemBarcode;
    itemBarcode = item.itemBarcode ?? "";
    return Row(
      children: [
        Icon(Icons.blur_linear,size: 10.0,),
        SizedBox(width: 2.0,),
        Text(itemBarcode,
            maxLines: 1,
            style: TextStyle(
                fontSize: 11.0, color: BCAppTheme().textColor),
            overflow: TextOverflow.ellipsis,
            softWrap: true),
      ],
    );
  }

  getItemNameField() {
    String itemName;

    itemName = item.itemName ?? "";
    return Text(itemName,
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
  getBrandNameField() {
    String brandName;
    brandName = item.brandName ?? "";

    return Text(" $brandName",
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getPackageNameField() {
    String pkgName;
    pkgName = item.pkgName ?? "";

    return Text("Package: $pkgName",
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getSubstField() {
    String substText;

    substText = "Is Subst: ${item.isSubstituteAllowed.toBool()}" ?? "";

    return Text(substText,
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getQuantityField() {
    String itemQuantity;
    itemQuantity = "Req Qty: ${item.approvedQty}";

    return Text(itemQuantity, maxLines: 1, style: TextStyle(
        fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
  getOrginNameFiled() {
    String orginName;

    orginName = item.countryName ?? "--";

    return Text(orginName,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
  getPkgDesc() {
    String totalCostText;

    totalCostText =
    "Pkg Desc: ${item.packingDesc}";

    return Text(totalCostText,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
}
