part of 'supplier_bloc.dart';

abstract class SupplierEvent extends Equatable {
  const SupplierEvent();
}

class LoadSupplier extends SupplierEvent {
  final String companyId;

  const LoadSupplier({@required this.companyId});

  @override
  List<Object> get props => [companyId];

  @override
  String toString() => 'LoadSupplier {"CompanyId" : "$companyId" }';
}

class GetMrRefDetails extends SupplierEvent {
  final String refId;
  final TransactionType type;

  const GetMrRefDetails({@required this.type, this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetMaterialRequestReferenceDetails {"ReferenceId" : "$refId","Type" : "$type"}';
}

class GetCurrency extends SupplierEvent {
  const GetCurrency();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoadCurrency';
}

class LoadCompany extends SupplierEvent {
  final int mode;

  const LoadCompany({@required this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'LoadCompany {"Mode" : "$mode" }';
}

class LoadDivision extends SupplierEvent {
  final String companyId;
  final int mode;

  const LoadDivision({@required this.companyId, this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() =>
      'GetDivision {"CompanyId" : "$companyId", "Mode" : "$mode" }';
}

class LoadLocation extends SupplierEvent {
  final String companyId;
  final String divisionId;
  final int mode;

  const LoadLocation(
      {@required this.companyId, @required this.divisionId, this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() =>
      'GetLocation {"CompanyId" : "$companyId", "DivisionId" : "$divisionId", "Mode" : "$mode" }';
}

class LoadDepartment extends SupplierEvent {
  final String companyId;
  final int mode;

  const LoadDepartment({@required this.companyId, this.mode});

  @override
  List<Object> get props => [mode];

  @override
  String toString() =>
      'GetDepartment {"Company" : "$companyId", "Mode" : "$mode" }';
}

class GetPriceType extends SupplierEvent {
  const GetPriceType();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetPriceType';
}

class GetReason extends SupplierEvent {

  const GetReason();

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetReason';
}

class GetGRNEntryNo extends SupplierEvent {

  const GetGRNEntryNo();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetGRNEntryNo';
}

class GetPendingPurchaseOrder extends SupplierEvent {
  final String grnType;

  const GetPendingPurchaseOrder({this.grnType});

  @override
  List<Object> get props => [grnType];

  @override
  String toString() => 'GetPendingPurchaseOrder {"GRNType" : "$grnType"}';
}
