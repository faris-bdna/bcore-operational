import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/response/ItemEntry/GetMrRefDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'supplier_event.dart';

part 'supplier_state.dart';

class SupplierBloc extends Bloc<SupplierEvent, SupplierState> {
  final BCRepository repository;

  SupplierBloc({@required this.repository}) : assert(repository != null), super(GetSupplierInitial());

  @override
  Stream<SupplierState> mapEventToState(SupplierEvent event) async* {

    if(event is LoadSupplier) {
      yield GetSupplierInProgress();

      try {
        final SupplierResponse supplierResponse = await repository.getSupplierMaster(companyId: event.companyId);
        yield GetSupplierComplete(supplierResponse:supplierResponse);
      } catch (error) {
        yield GetSupplierFailure(error: error.toString());
      }
    }

    else if(event is GetCurrency) {
      yield GetCurrencyInProgress();

      try {
        final GetCommonResp commonResp = await repository.getCurrencyMaster();
        yield GetCurrencyComplete(commonResp: commonResp);
      } catch (error) {
        yield GetCurrencyFailure(error: error.toString());
      }
    }

    else if(event is LoadCompany) {
      yield GetCompanyInProgress();

      try {
        final GetCompanyResponse getCompanyResp = await repository.getCompanyMaster(mode: event.mode);
        yield GetCompanyComplete(getCompanyResp: getCompanyResp);
      } catch (error) {
        yield GetCompanyFailure(error: error.toString());
      }
    }

    else if(event is LoadDivision) {
      yield GetDivisionInProgress();

      try {
        final GetCommonResp getDivisionResponse = await repository.getDivisionMaster(companyId: event.companyId, mode: event.mode);
        yield GetDivisionComplete(getDivisionResponse: getDivisionResponse);
      } catch (error) {
        yield GetDivisionFailure(error: error.toString());
      }
    }

    else if(event is LoadLocation) {
      yield GetLocationInProgress();

      try {
        final GetCommonResp getLocationResponse = await repository.getLocationMaster(companyId: event.companyId, divisionId: event.divisionId, mode: event.mode);
        yield GetLocationComplete(getLocationResponse: getLocationResponse);
      } catch (error) {
        yield GetLocationFailure(error: error.toString());
      }
    }

    else if(event is LoadDepartment) {
      yield GetDepartmentInProgress();

      try {
        final GetCommonResp getDepartmentResponse = await repository.getDepartmentMaster(companyId: event.companyId, mode: event.mode);
        yield GetDepartmentComplete(getDepartmentResponse: getDepartmentResponse);
      } catch (error) {
        yield GetDepartmentFailure(error: error.toString());
      }
    }

    else if(event is GetPriceType) {
      yield GetPriceTypeInProgress();

      try {
        final GetCommonResp getPriceTypeResp = await repository.getPriceType();
        yield GetPriceTypeComplete(getPriceTypeResp: getPriceTypeResp);
      } catch (error) {
        yield GetPriceTypeFailure(error: error.toString());
      }
    }

    else if(event is GetReason) {
      yield GetReasonInProgress();

      try {
        final GetCommonResp getReasonResp = await repository.getReason();
        yield GetReasonComplete(getReasonResp: getReasonResp);
      } catch (error) {
        yield GetReasonFailure(error: error.toString());
      }
    }

    else if (event is GetGRNEntryNo) {
      yield GetGRNEntryNoInProgress();

      try {
        final ReferencePOGrnResp resp = await repository.getGRNEntryNo();
        yield GetGRNEntryNoComplete(referencePOGrnResp: resp);
      } catch (error) {
        yield GetGRNEntryNoFailure(error: error.toString());
      }
    }

    else if (event is GetPendingPurchaseOrder) {
      yield GetPendingPurchaseOrderInProgress();

      try {
        final ReferencePOGrnResp resp = await repository.getPendingPurchaseOrder(grnType: event.grnType);
        yield GetPendingPurchaseOrderComplete(getPendingPurchaseOrderResp: resp);
      } catch (error) {
        yield GetPendingPurchaseOrderFailure(error: error.toString());
      }
    }

    else if(event is GetMrRefDetails) {
      yield GetMrRefDetailsInProgress();

      try {
        final GetMrRefDetailsResp resp = await repository.getMrRefDetails(refId: event.refId, type: event.type);
        yield GetMrRefDetailsComplete(resp: resp);
      } catch (error) {
        yield GetMrRefDetailsFailure(error: error.toString());
      }
    }
  }
}