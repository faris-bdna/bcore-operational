import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemFocDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/highItem/common_highItem_details.dart';
import 'package:bcore_inventory_management/models/view/supplierDetails.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Supplier/supplier_bloc.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart'
    as cmnMaster;

class SupplierForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const SupplierForm({Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _SupplierFormState createState() => _SupplierFormState(repository);
}

class _SupplierFormState extends State<SupplierForm> {
  final BCRepository repository;
  GRNType _grnType;
  PRType _prType;
  SIType _siType;

  // bool hasReferenceMatchFound = false;
  // bool hasEnabledTax = false;
  // bool hasEnabledQualityCheck = false;
  // bool hasEnabledCashPurchase = false;
  // bool hasEnabledWithInvoice = false;

  // Should contain or not
  bool containType = false;
  bool containReferences = false;
  bool containTransactionDate = false;
  bool containSupplier = false;
  bool containCurrencyRate = false;
  bool containTaxAndQualityCheck = false;
  bool containReason = false;
  bool containTaxOnly = false;
  bool containCashPurchaseAndInvoice = false;
  bool containInvoiceDate = false;
  bool containInvoiceNo = false;
  bool containDeliveryNote = false;
  bool containRemarks = false;
  bool containPriceType = false;
  bool containSalesMan = false;

  bool shouldValidateReference = false;
  bool shouldValidateTransactionDate = false;
  bool shouldValidateSupplier = false;
  bool shouldValidatePriceType = false;
  bool shouldValidateReason = false;
  bool shouldValidateInVoice = false;
  bool shouldValidateDeliveryNote = false;
  bool shouldValidateSalesMan = false;
  bool shouldValidateRemarks = false;
  int priceTypeID;

  EditMode editMode;

  var _referenceNoController = TextEditingController();
  var _invoiceNoController = TextEditingController();
  var _deliveryNoteController = TextEditingController();
  var _salesManController = TextEditingController();
  var _remarksController = TextEditingController();

  _SupplierFormState(this.repository);

  SupplierBloc _supplierBloc;

  // String /*selectedPriceTypeValue,*/
  //     /*selectedReasonValue,*/
  //     // supCurrency = "",
  //     supCurrencyRate = "";

  // List<DropdownMenuItem> currencyDropDownButtons = [];
  List<SupMasterDataList> supplierList = [];
  List<CommonDataList> priceTypeList = [];
  List<CommonDataList> reasonList = [];
  List<ReferencePOGrn> poRefList = [];

  SupMasterDataList selectedSupplierItem;
  ReferencePOGrn selectedReferenceItem;

  cmnMaster.CommonDataList selectedReasonItem;
  bool shouldDisableSupplierSelection = false;

  /// This will contain the value that will enable/ disable the Price type Dropdown selection.
  /// It's value will be true when the PR type is GRN and will be false when PR type is Direct.
  bool shouldDisablePriceTypeSelection = false;

  // GetCommonResp commonResp;
  List<CommonDataList> currencyList;

  String selectedTransactionDate;

  /// This gets called when the supplier form is in edit mode and will load the default API calls as per the transaction type
  setInitialValuesAndLoadValues() {
    /// Checks and sets Transaction Sub Type
    switch (widget.type) {
      case TransactionType.GRN:
        _grnType = GRNType.values[cmnSupplierInfo.gRNTypeId - 1];
        break;
      case TransactionType.PR:
        _prType = PRType.values[cmnSupplierInfo.pRetTypeId - 1];
        break;
    }

    if (widget.type == TransactionType.GRN) {
      if (_grnType == GRNType.Direct &&
          (supplierList == null || supplierList.length == 0)) {
        Future.delayed(Duration.zero, () {
          _supplierBloc.add(LoadSupplier(companyId: fromAddress.fromCompanyId));
        });
      } else {
        Future.delayed(Duration.zero, () {
          _supplierBloc.add(GetPendingPurchaseOrder(
              grnType: (_grnType.index + 1).toString()));
        });
      }
    } else if (widget.type == TransactionType.PR) {
      if (_prType == PRType.Direct &&
          (priceTypeList == null || priceTypeList.length == 0)) {
        Future.delayed(Duration.zero, () {
          _supplierBloc.add(GetPriceType());
        });
      } else {
        Future.delayed(Duration.zero, () {
          _supplierBloc.add(GetGRNEntryNo());
        });
      }
    }
  }

  loadItemDetailsIfInEditMode() {
    if (editMode == EditMode.On) {
      if (containType) {
        setInitialValuesAndLoadValues();
      }
      if (containTransactionDate) {
        selectedTransactionDate = (widget.type == TransactionType.GRN)
            ? cmnSupplierInfo.gRNDateTemp
            : cmnSupplierInfo.entryDateTemp;
      }
      if (containTaxAndQualityCheck) {
        cmnSupplierInfo.isQualityChecked =
            cmnSupplierInfo.isQualityChecked ?? false;
      }
      cmnSupplierInfo.isTaxApp = cmnSupplierInfo.isTaxApp ?? false;

      if (containCashPurchaseAndInvoice) {
        cmnSupplierInfo.isCashPurchase =
            cmnSupplierInfo.isCashPurchase ?? false;
        cmnSupplierInfo.isInvoiceRcvd = cmnSupplierInfo.isInvoiceRcvd ?? false;
      }
      if (containInvoiceDate) {
        // selectedInvoiceDate = cmnSupplierInfo.docDateTemp ?? null;
      }
      if (containInvoiceNo) {
        _invoiceNoController.text = cmnSupplierInfo.invoiceNo ?? "";
      }
      if (containDeliveryNote) {
        _deliveryNoteController.text = cmnSupplierInfo.deliveryNoteNo ?? "";
      }
      if (containSalesMan) {
        _salesManController.text = cmnSupplierInfo.salesMan ?? "";
      }
      if (containRemarks) {
        _remarksController.text = cmnSupplierInfo.remarks ?? "";
      }
    } else {
      cmnSupplierInfo.isInvoiceRcvd = false;
      if (containTransactionDate) {
        setDefaultTransactionDate();
      }
    }
  }

  setDefaultTransactionDate() {
    selectedTransactionDate = Utils.getDateByFormat(
        passedDateTime: DateTime.now(),
        format: fromAddress.fromCompanyDateFormat);

    switch (widget.type) {
      case TransactionType.GRN:
        cmnSupplierInfo.gRNDateTemp = Utils.getDateByFormat(
            passedDateTime: DateTime.now(),
            format: fromAddress.fromCompanyDateFormat);
        break;
      case TransactionType.PR:
        cmnSupplierInfo.entryDateTemp = Utils.getDateByFormat(
            passedDateTime: DateTime.now(),
            format: fromAddress.fromCompanyDateFormat);
        break;
    }
  }

  @override
  void initState() {
    super.initState();

    if ((cmnSupplierInfo != null) && (cmnSupplierInfo.suppId != null)) {
      editMode = EditMode.On;
    } else {
      editMode = EditMode.Off;
    }

    _configUI();
    _configValidation();
    loadItemDetailsIfInEditMode();

    if (_grnType == null && _prType == null) {
      Future.delayed(Duration.zero, () {
        /// For adding new dialog type, add new Type enum in Common & follow the comments
        showTypesDialogue();
      });
    }
    _referenceNoController.addListener(_getGrnReferenceNo);
    _invoiceNoController.addListener(_getInvoiceNo);
    _deliveryNoteController.addListener(_getDeliveryNoteNo);
    _remarksController.addListener(_getRemarks);
    _salesManController.addListener(_getSalesMan);
  }

  _configUI() {
    switch (widget.type) {
      case TransactionType.GRN:
        containType = true;
        containReferences = true;
        containTransactionDate = true;
        containSupplier = true;
        containCurrencyRate = true;
        containTaxAndQualityCheck = true;
        containTaxOnly = false;
        containCashPurchaseAndInvoice = true;
        containInvoiceDate = true;
        containInvoiceNo = true;
        containDeliveryNote = true;
        containRemarks = true;
        containPriceType = false;
        containReason = false;
        break;

      case TransactionType.PR:
        containType = true;
        containReferences = true;
        containTransactionDate = true;
        containSupplier = true;
        containCurrencyRate = true;
        containTaxAndQualityCheck = false;
        containTaxOnly = true;
        containReason = true;
        containCashPurchaseAndInvoice = false;
        containInvoiceDate = false;
        containInvoiceNo = false;
        containDeliveryNote = false;
        containRemarks = true;
        containPriceType = true;
        containSalesMan = true;
        break;
    }
  }

  _configValidation() {
    switch (widget.type) {
      case TransactionType.GRN:
        shouldValidateReference = true;
        shouldValidateTransactionDate = true;
        shouldValidateSupplier = true;
        shouldValidatePriceType = false;
        shouldValidateReason = false;
        shouldValidateInVoice = true;
        shouldValidateDeliveryNote = true;
        shouldValidateSalesMan = false;
        shouldValidateRemarks = false;
        break;

      case TransactionType.PR:
        shouldValidateReference = true;
        shouldValidateTransactionDate = true;
        shouldValidatePriceType = true;
        shouldValidateSupplier = true;
        shouldValidateReason = false;
        shouldValidateInVoice = false;
        shouldValidateDeliveryNote = false;
        shouldValidateSalesMan = false;
        shouldValidateRemarks = false;
        break;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _referenceNoController.dispose();
    _invoiceNoController.dispose();
    _deliveryNoteController.dispose();
    _salesManController.dispose();
    _remarksController.dispose();
  }

  Future<DateTime> showGRNDatePicker() async {
    DateTime firstDate = await Common().getBackDateAllowed()
        ? DateTime.now().subtract(Duration(days: 365))
        : DateTime.now();

    DateTime date = await showDatePicker(
      context: context,
      helpText: (widget.type == TransactionType.GRN)
          ? "Select GRN Date"
          : "Purchase Return Date",
      initialDate: DateTime.now(),
      firstDate: firstDate,
      lastDate: DateTime.now(),
    );
    return date;
  }

  Future<DateTime> showInvoiceDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Invoice Date",
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(Duration(days: 365)),
      lastDate: DateTime.now(),
    );
    return date;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SupplierBloc>(
      create: (context) => SupplierBloc(repository: repository),
      child: BlocListener<SupplierBloc, SupplierState>(
        listener: (context, state) {
          if (state is GetSupplierInProgress ||
              state is GetCurrencyInProgress ||
              state is GetPriceTypeInProgress ||
              state is GetReasonInProgress ||
              state is GetPendingPurchaseOrderInProgress ||
              state is GetGRNEntryNoInProgress) {
            Common().showLoader(context);
          } else if (state is GetPriceTypeComplete) {
            Navigator.pop(context);

            priceTypeList = [];

            priceTypeList = state.getPriceTypeResp.masterDataList;
            _supplierBloc
                .add(LoadSupplier(companyId: fromAddress.fromCompanyId));

            // Populate supplier dropdown
            setState(() {
              // if (editMode == EditMode.On) {
              //   selectedPriceTypeValue = cmnSupplierInfo.priceTypeId.toString();
              // }
            });
          } else if (state is GetSupplierComplete) {
            Navigator.pop(context);

            supplierList = [];

            supplierList = state.supplierResponse.masterDataList;
            _supplierBloc.add(GetCurrency());

            setState(() {
              if (editMode == EditMode.On) {
                selectedSupplierItem = supplierList
                    .where((element) =>
                        element.value == cmnSupplierInfo.suppId.toString())
                    .toList()[0];
                setSupplierInfo();
              }
            });
          } else if (state is GetCurrencyComplete) {
            Navigator.pop(context);

            currencyList = [];
            if (state.commonResp.masterDataList.isNotEmpty) {
              currencyList = state.commonResp.masterDataList;

              setState(() {
                if (selectedSupplierItem != null) {
                  String supplierCurrencyId = supplierList
                      .where((element) =>
                          element.value == selectedSupplierItem.value)
                      .toList()[0]
                      .currencyId
                      .toString();

                  // Set currency of selected supplier
                  if (editMode == EditMode.On) {
                    cmnSupplierInfo.currencyName = currencyList
                        .where((element) =>
                            element.value ==
                            cmnSupplierInfo.currencyId.toString())
                        .toList()[0]
                        .text;
                    cmnSupplierInfo.currencyRate =
                        selectedSupplierItem.currencyRate;
                  } else {
                    cmnSupplierInfo.currencyName = currencyList
                        .where((element) => element.value == supplierCurrencyId)
                        .toList()[0]
                        .text;
                  }
                }
                // Populate currency dropdown
                //   for (int i = 0; i < state.commonResp.masterDataList.length; i++) {
                //     currencyDropDownButtons.add(DropdownMenuItem(
                //       child: Text(state.commonResp.masterDataList[i].text),
                //       value: state.commonResp.masterDataList[i].value,
                //     ));
                //   }
              });

              if (widget.type == TransactionType.PR) {
                if (reasonList == null || reasonList.length == 0) {
                  _supplierBloc.add(GetReason());
                }
              }
            }
          } else if (state is GetReasonComplete) {
            Navigator.pop(context);
            reasonList = [];
            if (state.getReasonResp.masterDataList.isNotEmpty) {
              setState(() {
                reasonList = state.getReasonResp.masterDataList;
              });
            }
          } else if (state is GetPendingPurchaseOrderComplete) {
            Navigator.pop(context);

            poRefList = state.getPendingPurchaseOrderResp.masterDataList;

            setState(() {
              if (editMode == EditMode.On) {
                List<ReferencePOGrn> ppOrder = poRefList
                    .where((order) =>
                        order.value == cmnSupplierInfo.gRNTypeRefNo.toString())
                    .toList();
                if (ppOrder.isNotEmpty) {
                  selectedReferenceItem = ppOrder.first;

                  shouldDisableSupplierSelection = true;
                  supplierList = [
                    SupMasterDataList.fromRefJson(
                        selectedReferenceItem.toJson())
                  ];
                  selectedSupplierItem = SupMasterDataList.fromRefJson(
                      selectedReferenceItem.toJson());
                }
              }
            });
          } else if (state is GetGRNEntryNoComplete) {
            Navigator.pop(context);

            poRefList = [];

            setState(() {
              poRefList = state.referencePOGrnResp.masterDataList;

              if (editMode == EditMode.On) {
                List<ReferencePOGrn> grnRef = poRefList
                    .where((grnRef) =>
                        grnRef.value ==
                        cmnSupplierInfo.pRetTypeRefNo.toString())
                    .toList(); //cmnSupplierInfo..toString();
                if (grnRef.isNotEmpty) {
                  selectedReferenceItem = grnRef.first;

                  shouldDisablePriceTypeSelection = true;
                  priceTypeList = [
                    CommonDataList.fromRefJson(selectedReferenceItem.toJson())
                  ];

                  shouldDisableSupplierSelection = true;
                  supplierList = [
                    SupMasterDataList.fromRefJson(
                        selectedReferenceItem.toJson())
                  ];
                  selectedSupplierItem = SupMasterDataList.fromRefJson(
                      selectedReferenceItem.toJson());
                }
              }
            });
            _supplierBloc.add(GetReason());
          } else if (state is GetSupplierFailure ||
              state is GetCurrencyFailure ||
              state is GetPriceTypeFailure ||
              state is GetReasonFailure ||
              state is GetPendingPurchaseOrderFailure ||
              state is GetGRNEntryNoFailure) {
            Navigator.pop(context);
          }
        },
        child:
            BlocBuilder<SupplierBloc, SupplierState>(builder: (context, state) {
          _supplierBloc = BlocProvider.of<SupplierBloc>(context);

          if (state is GetSupplierInitial) {}

          return Container(
            child: Column(
              children: [
                if (containType) _getTypes(),
                if (containReferences) _getReferences(),
                if (containTransactionDate) _getTransactionDate(),
                if (containPriceType) _getPriceType(),
                if (containSupplier) _getSupplier(),
                if (containCurrencyRate && cmnSupplierInfo.currencyName != null)
                  _getCurrencyRate(),
                if (containTaxOnly) _getTax(),
                if (containTaxAndQualityCheck) _getTaxAndQualityCheck(),
                if (containReason) _getReason(),
                if (containCashPurchaseAndInvoice) _getCashPurchaseAndInvoice(),
                if (containInvoiceDate) _getInvoiceDate(),
                if (containInvoiceNo) _getInvoiceNoWidget(),
                if (containDeliveryNote) _getDeliveryNote(),
                if (containSalesMan) _getSalesManWidget(),
                if (containRemarks) _getRemarksWidget()
              ],
            ),
          );
        }),
      ),
    );
  }

  /// This will return the type selection widget
  _getTypes() {
    String typeText;

    switch (widget.type) {
      case TransactionType.GRN:
        typeText = (_grnType == null) ? "" : _grnType.value;
        break;
      case TransactionType.PR:
        typeText = (_prType == null) ? "" : _prType.value;
        break;
    }

    return Row(children: [
      Text("Type :"),
      FlatButton(
          child: Text(typeText,
              style: TextStyle(
                  decoration: TextDecoration.underline,
                  color: BCAppTheme().headingTextColor,
                  fontWeight: FontWeight.bold)),
          onPressed: () => showTypesDialogue())
    ]);
  }

  _getReferences() {
    if ((widget.type == TransactionType.GRN && _grnType != GRNType.Direct) ||
        (widget.type == TransactionType.PR && _prType != PRType.Direct)) {
      return Column(
        children: [
          Container(
            padding: EdgeInsets.zero,
            decoration: Common().getBCoreSD(
                isMandatory: shouldValidateReference,
                isValidated: selectedReferenceItem != null),
            child: SearchableDropdown.single(
              items: (poRefList != null && poRefList.isNotEmpty)
                  ? poRefList.map((ReferencePOGrn item) {
                      return DropdownMenuItem(
                          child: Text(item.text,
                              style: TextStyle(
                                fontSize: 14.0,
                                color: BCAppTheme().subTextColor,
                              )),
                          value: item.text);
                    }).toList()
                  : [],
              value: selectedReferenceItem?.text,
              hint: Constants.refPlaceHolderText,
              searchHint: Constants.refPlaceHolderText,
              onChanged: (value) {
                if (value != null) {
                  setState(() {
                    print(selectedReferenceItem);

                    selectedReferenceItem = poRefList
                        .where((element) => element.text == value)
                        .toList()[0];
                    if (widget.type == TransactionType.GRN) {
                      cmnSupplierInfo.gRNTypeId = _grnType.index + 1;
                      cmnSupplierInfo.gRNTypeRefNo =
                          int.tryParse(selectedReferenceItem.value);
                      if (selectedReferenceItem.supplierId != null &&
                          selectedReferenceItem.supplierName != null) {
                        supplierList = [
                          SupMasterDataList.fromRefJson(
                              selectedReferenceItem.toJson())
                        ];
                        selectedSupplierItem = SupMasterDataList.fromRefJson(
                            selectedReferenceItem.toJson());

                        cmnSupplierInfo.currencyName =
                            selectedSupplierItem.currencyName;
                        cmnSupplierInfo.currencyRate =
                            selectedSupplierItem.currencyRate;
                        if (_grnType == GRNType.LPO)
                          cmnSupplierInfo.suppTypeId = 1;

                        cmnSupplierInfo.isTaxApp =
                            selectedSupplierItem.isTaxApplicable == 0
                                ? false
                                : true;
                        shouldDisableSupplierSelection = true;
                      }
                    } else if (widget.type == TransactionType.PR) {
                      cmnSupplierInfo.pRetTypeRefNo =
                          int.tryParse(selectedReferenceItem.value);
                      cmnSupplierInfo.priceTypeId =
                          selectedReferenceItem.priceTypeId;
                      if (selectedReferenceItem.supplierId != null &&
                          selectedReferenceItem.supplierName != null) {
                        shouldDisableSupplierSelection = true;
                        supplierList = [
                          SupMasterDataList.fromRefJson(
                              selectedReferenceItem.toJson())
                        ];
                        selectedSupplierItem = SupMasterDataList.fromRefJson(
                            selectedReferenceItem.toJson());

                        shouldDisablePriceTypeSelection = true;
                        priceTypeList = [
                          CommonDataList.fromRefJson(
                              selectedReferenceItem.toJson())
                        ];

                        cmnSupplierInfo.priceTypeText =
                            selectedReferenceItem.priceTypeName;
                        cmnSupplierInfo.currencyName =
                            selectedSupplierItem.currencyName;
                        cmnSupplierInfo.currencyRate =
                            selectedSupplierItem.currencyRate;

                        cmnSupplierInfo.isTaxApp =
                            selectedSupplierItem.isTaxApplicable == 0
                                ? false
                                : true;
                      }
                    }
                    cmnSupplierInfo.suppId = selectedReferenceItem.supplierId;
                    cmnSupplierInfo.deliveryTypeId =
                        selectedReferenceItem.deliveryTypeId;
                    cmnSupplierInfo.companyId = selectedReferenceItem.companyId;
                    cmnSupplierInfo.divisionId =
                        selectedReferenceItem.divisionId;
                    cmnSupplierInfo.locationId =
                        selectedReferenceItem.locationId;
                    cmnSupplierInfo.departmentId =
                        selectedReferenceItem.departmentId;
                    cmnSupplierInfo.currencyId =
                        selectedReferenceItem.currencyId;
                    cmnSupplierInfo.projectId = selectedReferenceItem.projectId;
                  });
                }
              },
              displayClearIcon: false,
              underline: Container(),
              isExpanded: true,
            ),
          ),
          SizedBox(height: 16),
        ],
      );
    } else {
      return SizedBox();
    }
  }

  _getTransactionDate() {
    return Column(
      children: [
//        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: DateFormField(
            initialValue: selectedTransactionDate,
            format: fromAddress.fromCompanyDateFormat,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateTransactionDate,
                isValidated: selectedTransactionDate != null),
            showPicker: showGRNDatePicker,
            onDateChanged: (DateTime date) {
              // Set GRN date
              if (widget.type == TransactionType.GRN) {
                cmnSupplierInfo.gRNDateTemp = Utils.getDateByFormat(
                    passedDateTime: date,
                    format: fromAddress.fromCompanyDateFormat);
              } else if (widget.type == TransactionType.PR) {
                cmnSupplierInfo.entryDateTemp = Utils.getDateByFormat(
                    passedDateTime: date,
                    format: fromAddress.fromCompanyDateFormat);
              }
            },
          ),
        ),
      ],
    );
  }

  _getSupplier() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: shouldValidateSupplier,
              isValidated: _validateSupplier()),
          child: SearchableDropdown.single(
            items: (supplierList != null && supplierList.isNotEmpty)
                ? supplierList.map((SupMasterDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: selectedSupplierItem?.text,
            readOnly: shouldDisableSupplierSelection,
            hint: "Select Supplier",
            searchHint: "Select Supplier",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  selectedSupplierItem = supplierList
                      .where((element) => element.text == value)
                      .toList()[0];
                  setSupplierInfo();
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  setSupplierInfo() {
    // Set supplier currency
    if (currencyList != null && selectedSupplierItem.currencyId != null) {
      cmnSupplierInfo.currencyName = currencyList
          .where((element) =>
              element.value == selectedSupplierItem.currencyId.toString())
          .toList()[0]
          .text;
    }
    // Set supplier currency rate
    cmnSupplierInfo.currencyRate = selectedSupplierItem.currencyRate;

    // Set GRN header details
    // Set supplier currency ID
    cmnSupplierInfo.currencyId = selectedSupplierItem.currencyId;

    // Set supplier currency rate
//  cmnSupplierInfo.currencyRate = selectedSupplierItem[0].currencyRate.toString();

    // Set supplier ID
    cmnSupplierInfo.suppId = int.parse(selectedSupplierItem.value);

    // Set supplier type ID
    cmnSupplierInfo.suppTypeId = selectedSupplierItem.supplierTypeId;

    // Set tax applicable
    cmnSupplierInfo.isTaxApp =
        selectedSupplierItem.isTaxApplicable == 0 ? false : true;

    // Set cash purchase
    cmnSupplierInfo.isCashPurchase =
        selectedSupplierItem.isAllowedCashPurchase == 0 ? false : true;

    cmnSupplierInfo.isSupplierInternal =
        selectedSupplierItem.isInternal.toBool();

    // set tax & cash purchase status
    cmnSupplierInfo.isTaxApp = selectedSupplierItem.isTaxApplicable.toBool();
    cmnSupplierInfo.isCashPurchase =
        selectedSupplierItem.isAllowedCashPurchase.toBool();
  }

  _getCurrencyRate() {
    return Column(
      children: [
        SizedBox(height: 16),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              child: Text((cmnSupplierInfo.currencyName ?? '') + "   :"),
            ),
            SizedBox(width: 16),
            Flexible(
                child: Text((cmnSupplierInfo.currencyRate ?? 0.0).toString()))
          ],
        ),
      ],
    );
  }

  _getTaxAndQualityCheck() {
    return Column(
      children: [
        SizedBox(height: 16),
        Row(
          children: [
            Text(
              "Tax",
              style: TextStyle(fontSize: 11),
            ),
            Switch(
              value: cmnSupplierInfo.isTaxApp ?? false,
              activeColor: BCAppTheme().headingTextColor,
              onChanged: (value) {
                setState(() {
//                  if (widget.type != TransactionType.GRN) {
//                    hasEnabledTax = value;
//                  }
                });
              },
            ),
            Container(
              width: 1,
              height: 15,
              color: BCAppTheme().headingTextColor,
            ),
            SizedBox(width: 8),
            Text("Quality Check", style: TextStyle(fontSize: 11)),
            Switch(
              value: cmnSupplierInfo.isQualityChecked ?? false,
              activeColor: BCAppTheme().headingTextColor,
              onChanged: (value) {
                setState(() {
                  // Set quality check value in GRN header
                  cmnSupplierInfo.isQualityChecked = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  _getTax() {
    return Column(
      children: [
        SizedBox(height: 16),
        Row(
          children: [
            Text(
              "Tax",
              style: TextStyle(fontSize: 11),
            ),
            Switch(
              value: cmnSupplierInfo.isTaxApp ?? false,
              activeColor: BCAppTheme().headingTextColor,
              onChanged: (value) {
//                setState(() {
//                  hasEnabledTax = value;
//                });
              },
            ),
          ],
        ),
      ],
    );
  }

  _getCashPurchaseAndInvoice() {
    return Row(
      children: [
        Text("Cash Purchase", style: TextStyle(fontSize: 11)),
        Switch(
          value: cmnSupplierInfo.isCashPurchase ?? false,
          activeColor: BCAppTheme().headingTextColor,
          onChanged: (value) {
//            setState(() {
//              hasEnabledCashPurchase = value;
//
//              // Set cash purchase value in GRN header
//                cmnSupplierInfo.isCashPurchase = value;
//            });
          },
        ),
        Container(
          width: 1,
          height: 15,
          color: BCAppTheme().headingTextColor,
        ),
        SizedBox(width: 8),
        Text("With Invoice", style: TextStyle(fontSize: 11)),
        Switch(
          value: cmnSupplierInfo.isInvoiceRcvd ?? false,
          activeColor: BCAppTheme().headingTextColor,
          onChanged: (value) {
            setState(() {
              // Set invoice received value in GRN header
              cmnSupplierInfo.isInvoiceRcvd = value;
              if (cmnSupplierInfo.isInvoiceRcvd == false) {
                cmnSupplierInfo.docDateTemp = null;
                _invoiceNoController.text = "";
              }
            });
          },
        ),
      ],
    );
  }

  _getInvoiceDate() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: DateFormField(
            enabled: cmnSupplierInfo.isInvoiceRcvd ?? false,
            format: fromAddress.fromCompanyDateFormat,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateInVoice,
                isValidated: _validateInvoiceDate(),
                hintText: 'Invoice Date',
                icon: Icons.calendar_today),
            showPicker: showInvoiceDatePicker,
            initialValue: cmnSupplierInfo.docDateTemp,
            onDateChanged: (DateTime date) {
              setState(() {
                cmnSupplierInfo.docDateTemp = Utils.getDateByFormat(
                    passedDateTime: date,
                    format: fromAddress.fromCompanyDateFormat);
              });
            },
          ),
        )
      ],
    );
  }

  _getInvoiceNoWidget() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            controller: _invoiceNoController,
            enabled: cmnSupplierInfo.isInvoiceRcvd ?? false,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateInVoice,
                isValidated: _validateInvoiceNo(),
                hintText: 'Invoice No'),
            keyboardType: TextInputType.text,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
          ),
        ),
      ],
    );
  }

  _getDeliveryNote() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            controller: _deliveryNoteController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateDeliveryNote,
                isValidated: _validateDeliveryNoteField(),
                hintText: 'Delivery Note'),
            keyboardType: TextInputType.text,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
          ),
        )
      ],
    );
  }

  _getRemarksWidget() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            controller: _remarksController,
            autocorrect: false,
            decoration:
                Common().getBCoreID(hintText: 'Remarks', validityStatus: true),
            keyboardType: TextInputType.text,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
          ),
        ),
        SizedBox(height: 16),
      ],
    );
  }

  _getPriceType() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(
              isMandatory: shouldValidatePriceType,
              isValidated: cmnSupplierInfo.priceTypeId != null),
          child: SearchableDropdown.single(
            items: (priceTypeList != null && priceTypeList.isNotEmpty)
                ? priceTypeList.map((CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnSupplierInfo.priceTypeText,
            readOnly: shouldDisablePriceTypeSelection,
            hint: "Select Price Type",
            searchHint: "Select Price Type",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList selectedSupplierItem = priceTypeList
                      .where((element) => element.text == value)
                      .toList()[0];
                  cmnSupplierInfo.priceTypeText = selectedSupplierItem.text;
                  cmnSupplierInfo.priceTypeId =
                      int.parse(selectedSupplierItem.value);
                  priceTypeID = int.parse(selectedSupplierItem.value);
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getReason() {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.zero,
          decoration: Common()
              .getBCoreSD(isMandatory: shouldValidateReason, isValidated: true),
          child: SearchableDropdown.single(
            items: (reasonList != null && reasonList.isNotEmpty)
                ? reasonList.map((CommonDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                : [],
            value: cmnSupplierInfo.reasonText,
            hint: "Select Reason",
            searchHint: "Select Reason",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  CommonDataList reason = reasonList
                      .where((element) => element.text == value)
                      .toList()[0];
                  cmnSupplierInfo.reasonText = reason.text;
                  cmnSupplierInfo.reasonId = int.parse(reason.value);
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getSalesManWidget() {
    return Column(
      children: [
        SizedBox(height: 16),
        SizedBox(
          height: 50,
          child: TextField(
            textInputAction: TextInputAction.next,
            controller: _salesManController,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: false,
                isValidated: true,
                hintText: 'Enter Sales Man'),
            keyboardType: TextInputType.name,
            onEditingComplete: () => FocusScope.of(context).nextFocus(),
          ),
        )
      ],
    );
  }

  /// Gets called when user selects a transaction type from the popup
  changeType(BuildContext dialogContext, int buttonIndex) {
    Navigator.pop(dialogContext);

    setState(() {
      resetTransaction();

      if (containTransactionDate) {
        setDefaultTransactionDate();
      }

      switch (widget.type) {
        case TransactionType.GRN:
          _grnType = GRNType.values[buttonIndex];
          cmnSupplierInfo.gRNTypeId = buttonIndex + 1;

          /// Enum indexing starts from 0 and the API indexing starts from 1. We have added 1 constantly in order to match this
          break;
        case TransactionType.PR:
          _prType = PRType.values[buttonIndex];
          cmnSupplierInfo.pRetTypeId = buttonIndex + 1;

          /// Enum indexing starts from 0 and the API indexing starts from 1. We have added 1 constantly in order to match this
          break;
      }
      poRefList.clear();
      selectedReferenceItem = null;
    });

    setState(() {});
    if (widget.type == TransactionType.GRN) {
      if (buttonIndex == GRNType.Direct.index) {
        /// User has selected GRN Type Direct
        if (supplierList == null || supplierList.length == 0) {
          _supplierBloc.add(LoadSupplier(companyId: fromAddress.fromCompanyId));
        }
      } else {
        /// User has selected GRN Type LPO or IPO
        _supplierBloc.add(
            GetPendingPurchaseOrder(grnType: (buttonIndex + 1).toString()));
      }
    } else if (widget.type == TransactionType.PR) {
      if (
          // (priceTypeList == null || priceTypeList.length == 0) &&
          (buttonIndex == PRType.Direct.index)) {
        Future.delayed(Duration.zero, () {
          _supplierBloc.add(GetPriceType());
        });
      } else {
        _supplierBloc.add(GetGRNEntryNo());
      }
    }
  }

  /// This will display the types dialog
  showTypesDialogue() {
    String dialogTitle;

    /// Add new case for adding new Type
    switch (widget.type) {
      case TransactionType.GRN:
        dialogTitle = "GRN Types";
        break;
      case TransactionType.PR:
        dialogTitle = "Purchase Return Types";
        break;
    }

    Dialog typeAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Stack(alignment: Alignment.topRight, children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            height: 65,
            child: Column(
              children: [
                Text(dialogTitle, textAlign: TextAlign.center),
                SizedBox(height: 16.0),
                getDialogContent()
              ],
            ),
          ),
        ),
        Positioned(
            child: SizedBox(
          width: 35,
          height: 35,
          child: IconButton(
              icon: Icon(Icons.close, color: BCAppTheme().redColor),
              onPressed: () {
                Navigator.pop(context);
              }),
        )),
      ]),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return typeAlert;
      },
    ).then((val) {
      switch (widget.type) {
        case TransactionType.GRN:
          if (_grnType == null) {
            setState(() {
              _grnType = GRNType.Direct;
              cmnSupplierInfo.gRNTypeId = _grnType.index + 1;

              /// Enum indexing starts from 0 and the API indexing starts from 1. We have added 1 constantly in order to match this
              _supplierBloc
                  .add(LoadSupplier(companyId: fromAddress.fromCompanyId));
            });
          }
          break;
        case TransactionType.PR:
          if (_prType == null) {
            setState(() {
              _prType = PRType.Direct;
              cmnSupplierInfo.pRetTypeId = _prType.index + 1;

              /// Enum indexing starts from 0 and the API indexing starts from 1. We have added 1 constantly in order to match this
              _supplierBloc.add(GetPriceType());
            });
          }
          break;
      }
    });
  }

  /// Will provide the content with types as widget with buttons
  getDialogContent() {
    return Expanded(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: getTypeButtons(),
      ),
    );
  }

  /// This will return the type buttons list
  List<Widget> getTypeButtons() {
    int buttonCount;
    List<Widget> buttonList = [];

    /// Add new case for adding new Type

    switch (widget.type) {
      case TransactionType.GRN:
        buttonCount = GRNType.values.length;
        break;
      case TransactionType.PR:
        buttonCount = PRType.values.length;
        break;
    }
    for (int index = 0; index < buttonCount; index++) {
      buttonList.add(getTypeButton(index));
      if (index + 1 != buttonCount) {
        buttonList.add(SizedBox(width: 6));
      }
    }
    return buttonList;
  }

  /// returns button with title for the transaction type selection pop up
  Widget getTypeButton(int buttonIndex) {
    String buttonText;

    /// Add new case for adding new Type
    switch (widget.type) {
      case TransactionType.GRN:
        buttonText = GRNType.values[buttonIndex].value;
        break;
      case TransactionType.PR:
        buttonText = PRType.values[buttonIndex].value;
        break;
    }

    return Expanded(
      flex: 3,
      child: FlatButton(
          color: BCAppTheme().headingTextColor,
          child: Text(buttonText,
              style:
                  TextStyle(color: BCAppTheme().secondaryColor, fontSize: 11)),
          onPressed: () => changeType(context, buttonIndex)),
    );
  }

  resetTransaction() {
    cmnSupplierInfo.clear();
    cmnSupplierInfo.isInvoiceRcvd = false;
    CommonHighItemInfo.shared.cmnHighItemList.clear();

    if (widget.type == TransactionType.GRN) {
      GrnReq.shared.clear();
      grn_foc_details.clear();

      if (_grnType == GRNType.Direct) {
        poRefList = [];
      }
    } else {
      SavePRReq.shared.clear();

      if (_prType == PRType.Direct) {
        poRefList = [];
      }
    }
    Utils.resetUniqueId();

    // priceTypeList = [];
    // reasonList = [];
    // poRefList = [];
    currencyList = [];

    selectedSupplierItem = null;
    selectedReferenceItem = null;
    selectedReasonItem = null;
    selectedTransactionDate = null;

    shouldDisableSupplierSelection = false;
    shouldDisablePriceTypeSelection = false;
    editMode = EditMode.Off;

    _deliveryNoteController.clear();
    _invoiceNoController.clear();
    _referenceNoController.clear();
    _remarksController.clear();
    _salesManController.clear();

    _configUI();
    _configValidation();
  }

  _getGrnReferenceNo() {
    /// Set entry no value in GRN header
    cmnSupplierInfo.gRNTypeRefNo = _referenceNoController.text.isEmpty
        ? null
        : int.parse(_referenceNoController.text.trim());
  }

  _getInvoiceNo() {
    setState(() {
      // Set invoice no value in GRN header
      cmnSupplierInfo.invoiceNo = _invoiceNoController.text.isEmpty
          ? ""
          : _invoiceNoController.text.trim();
    });
  }

  _getDeliveryNoteNo() {
    /// Set delivery note no value in GRN header
    setState(() {
      cmnSupplierInfo.deliveryNoteNo = _deliveryNoteController.text.isEmpty
          ? ""
          : _deliveryNoteController.text.trim();
    });
  }

  _getSalesMan() {
    /// Set remarks value in GRN header
    cmnSupplierInfo.salesMan =
        _salesManController.text.isEmpty ? "" : _salesManController.text.trim();
  }

  _getRemarks() {
    /// Set remarks value in GRN header
    cmnSupplierInfo.remarks =
        _remarksController.text.isEmpty ? "" : _remarksController.text.trim();
  }

  _validateDeliveryNoteField() {
    return (cmnSupplierInfo.isInvoiceRcvd ||
        (!cmnSupplierInfo.isInvoiceRcvd &&
            (cmnSupplierInfo.deliveryNoteNo != null &&
                cmnSupplierInfo.deliveryNoteNo.isNotEmpty)));
  }

  _validateInvoiceDate() {
    return (cmnSupplierInfo.isInvoiceRcvd &&
        (cmnSupplierInfo.docDateTemp != null &&
            cmnSupplierInfo.docDateTemp.isNotEmpty));
  }

  _validateInvoiceNo() {
    return (cmnSupplierInfo.isInvoiceRcvd &&
        (cmnSupplierInfo.invoiceNo != null &&
            cmnSupplierInfo.invoiceNo.isNotEmpty));
  }

  /// Validates the supplier field by checking whether any supplier has been selected or not as per scenarios
  _validateSupplier() {
    if (_grnType == GRNType.Direct || _prType == PRType.Direct) {
      if (selectedSupplierItem == null)
        return false;
      else
        return true;
    } else
      return true;
  }

  bool check() {
    return true;
  }
}
