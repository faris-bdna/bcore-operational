part of 'supplier_bloc.dart';

abstract class SupplierState extends Equatable {
  const SupplierState();

  @override
  List<Object> get props => [];
}


/// Get Supplier

class GetSupplierInitial extends SupplierState{}

class GetSupplierInProgress extends SupplierState{}

class GetSupplierComplete extends SupplierState{
  final SupplierResponse supplierResponse;

  GetSupplierComplete({@required this.supplierResponse}) : assert(supplierResponse != null);

  @override
  List<Object> get props => [supplierResponse];
}

class GetSupplierFailure extends SupplierState {
  final String error;

  const GetSupplierFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSupplierFailure { error : $error }';
}

/// Get MrRefDetails

class GetMrRefDetailsInitial extends SupplierState{}

class GetMrRefDetailsInProgress extends SupplierState{}

class GetMrRefDetailsComplete extends SupplierState{
  final GetMrRefDetailsResp resp;

  GetMrRefDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetMrRefDetailsFailure extends SupplierState {
  final String error;

  const GetMrRefDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetMrRefDetailsFailure { error : $error }';
}
/// Get Currency

class GetCurrencyInProgress extends SupplierState{}

class GetCurrencyComplete extends SupplierState{
  final GetCommonResp commonResp;

  GetCurrencyComplete({@required this.commonResp}) : assert(commonResp != null);

  @override
  List<Object> get props => [commonResp];
}

class GetCurrencyFailure extends SupplierState {
  final String error;

  const GetCurrencyFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetSupplierFailure { error : $error }';
}


/// Get Company

class GetCompanyInitial extends SupplierState{}

class GetCompanyInProgress extends SupplierState{}

class GetCompanyComplete extends SupplierState{
  final GetCompanyResponse getCompanyResp;

  GetCompanyComplete({@required this.getCompanyResp}) : assert(getCompanyResp != null);

  @override
  List<Object> get props => [getCompanyResp];
}

class GetCompanyFailure extends SupplierState {
  final String error;

  const GetCompanyFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCompanyFailure { error : $error }';
}


/// Get Division

class GetDivisionInitial extends SupplierState{}

class GetDivisionInProgress extends SupplierState{}

class GetDivisionComplete extends SupplierState{
  final GetCommonResp getDivisionResponse;

  GetDivisionComplete({@required this.getDivisionResponse}) : assert(getDivisionResponse != null);

  @override
  List<Object> get props => [getDivisionResponse];
}

class GetDivisionFailure extends SupplierState {
  final String error;

  const GetDivisionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDivisionFailure { error : $error }';
}


/// Get Location

class GetLocationInitial extends SupplierState{}

class GetLocationInProgress extends SupplierState{}

class GetLocationComplete extends SupplierState{
  final GetCommonResp getLocationResponse;

  GetLocationComplete({@required this.getLocationResponse}) : assert(getLocationResponse != null);

  @override
  List<Object> get props => [getLocationResponse];
}

class GetLocationFailure extends SupplierState {
  final String error;

  const GetLocationFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetLocationFailure { error : $error }';
}


/// Get Department

class GetDepartmentInitial extends SupplierState{}

class GetDepartmentInProgress extends SupplierState{}

class GetDepartmentComplete extends SupplierState{
  final GetCommonResp getDepartmentResponse;

  GetDepartmentComplete({@required this.getDepartmentResponse}) : assert(getDepartmentResponse != null);

  @override
  List<Object> get props => [getDepartmentResponse];
}

class GetDepartmentFailure extends SupplierState {
  final String error;

  const GetDepartmentFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDepartmentFailure { error : $error }';
}



class GetPriceTypeInitial extends SupplierState {}

class GetPriceTypeInProgress extends SupplierState {}

class GetPriceTypeComplete extends SupplierState {
  final GetCommonResp getPriceTypeResp;

  GetPriceTypeComplete({@required this.getPriceTypeResp}) : assert(getPriceTypeResp != null);

  @override
  List<Object> get props => [getPriceTypeResp];
}

class GetPriceTypeFailure extends SupplierState {
  final String error;

  const GetPriceTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPriceTypeFailure { error : $error }';
}



class GetReasonInitial extends SupplierState {}

class GetReasonInProgress extends SupplierState {}

class GetReasonComplete extends SupplierState {
  final GetCommonResp getReasonResp;

  GetReasonComplete({@required this.getReasonResp}) : assert(getReasonResp != null);

  @override
  List<Object> get props => [getReasonResp];
}

class GetReasonFailure extends SupplierState {
  final String error;

  const GetReasonFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetReasonFailure { error : $error }';
}


/// GetGRNEntryNo States

class GetGRNEntryNoInitial extends SupplierState{}

class GetGRNEntryNoInProgress extends SupplierState{}

class GetGRNEntryNoComplete extends SupplierState{
  final ReferencePOGrnResp referencePOGrnResp;

  GetGRNEntryNoComplete({@required this.referencePOGrnResp}) : assert(referencePOGrnResp != null);

  @override
  List<Object> get props => [referencePOGrnResp];
}

class GetGRNEntryNoFailure extends SupplierState {
  final String error;

  const GetGRNEntryNoFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNEntryNoFailure { error : $error }';
}


/// GetPendingPurchaseOrder States

class GetPendingPurchaseOrderInitial extends SupplierState{}

class GetPendingPurchaseOrderInProgress extends SupplierState{}

class GetPendingPurchaseOrderComplete extends SupplierState{
  final ReferencePOGrnResp getPendingPurchaseOrderResp;

  GetPendingPurchaseOrderComplete({@required this.getPendingPurchaseOrderResp}) : assert(getPendingPurchaseOrderResp != null);

  @override
  List<Object> get props => [getPendingPurchaseOrderResp];
}

class GetPendingPurchaseOrderFailure extends SupplierState {
  final String error;

  const GetPendingPurchaseOrderFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPendingPurchaseOrderFailure { error : $error }';
}
