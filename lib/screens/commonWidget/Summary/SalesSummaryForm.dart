import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/DN/saveDNReq.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/request/sai/saveSAIReq.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

class SalesSummaryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Function(int) navigateBackWithValueIndex;
  final Database database;
  final TransactionType type;

  const SalesSummaryForm(
      {Key key,
        this.repository,
        this.appLanguage,
        this.navigateBackWithValueIndex,
        this.database,
        this.type})
      : super(key: key);

  @override
  _SalesSummaryFormState createState() =>
      _SalesSummaryFormState(repository, appLanguage, database, type);
}

class _SalesSummaryFormState extends State<SalesSummaryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;

  TextEditingController discountPercController = TextEditingController();
  TextEditingController discountAmountController = TextEditingController();

  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;

  bool shouldContainTotalInfoField = false;

  _SalesSummaryFormState(
      this.repository, this.appLanguage, this.database, this.type);

  @override
  void initState() {
    super.initState();

    switch (type) {
      case TransactionType.SO:
        SOReq.shared.discountOnGross = 0.0;
        shouldContainTotalInfoField = true;
        break;
      case TransactionType.SAI:
        SaveSAIReq.shared.discount = 0.0;
        SaveSAIReq.shared.appStatus = SaveSAIReq.shared.appStatus ?? 1;
        shouldContainTotalInfoField = true;
        break;
    }
  }

  double _getDiscountInterest() {
    double discount;

    switch(type){
      case TransactionType.SO:
        discount = SOReq.shared.discountOnGross;
        break;
      case TransactionType.SAI:
        discount = SaveSAIReq.shared.discount;;
        break;
      case TransactionType.DN:
        discount = 0.0;
        break;
    }

    double totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList
        .fold(0.0, (prevVal, item) => prevVal + item.totalCost.toDouble());
    return ((discount / totalAmount) * 100).trim();
  }

  double _getDiscountAmount(double discPerc) {
    double totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList
        .fold(0.0, (prevVal, item) => prevVal + item.totalCost.toDouble());
    return (totalAmount * (discPerc / 100)).trim();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is UpdateDraftTransactionInProgress) {
            Common().showLoader(context);
          } else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
            print("${state.updateDraftTransactionResp}");

            if (state.updateDraftTransactionResp) {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item has been deleted'),
                  duration: Duration(milliseconds: 500)));
            }
          } else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _getDiscountOnGrossFields(),
                if (shouldContainTotalInfoField) _getTotalInfoField(),
                SizedBox(height: 10),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: getCount(),
                  itemBuilder: (context, index) {
                    return ItemRow(
                      type: type,
                      index: index,
                      didTapEdit: () {
                        widget.navigateBackWithValueIndex(index);
                      },
                      grossDiscount: _getDiscountInterest(),
                      didTapDelete: () {
                        setState(() {
                          removeItem(index);
                          removeItemFromDB();
                        });
                      },
                    );
                  },
                ),
                SizedBox(height: 16),
              ],
            ),
          );
        }),
      ),
    );
  }

  _getDiscountOnGrossFields() {
    if (widget.type == TransactionType.SO) {
      return Column(
        children: [
          SizedBox(
            height: 50,
            child: TextField(
              enableSuggestions: false,
              textInputAction: TextInputAction.next,
              autocorrect: false,
              decoration: Common().getBCoreID(
                validityStatus: true,
                hintText: 'Discount on Gross Value',
              ),
              onChanged: (value) {
                setState(() {
                  SOReq.shared.discountOnGross =
                  (value != null && value != '') ? value.toDouble() : 0.0;
                });
              },
              keyboardType: TextInputType.number,
            ),
          )
        ],
      );
    }
    else if (widget.type == TransactionType.SAI) {
      return Column(
        children: [
          Text('Discount on Gross Value'),
          SizedBox(height: 8.0),
          Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: 50,
                  child: TextField(
                    controller: discountPercController,
                    enableSuggestions: false,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                    decoration: Common().getBCoreID(
                      validityStatus: true,
                      hintText: '%',
                    ),
                    onChanged: (value) {
                      print("called onChanged of %");
                      double discountPercentage = (value != null && value != '') ? value.toDouble() : 0.0;
                      double discountAmount = _getDiscountAmount(discountPercentage);

                      setState(() {
                        if (widget.type == TransactionType.SAI){
                          SaveSAIReq.shared.discount = discountAmount;
                        }
                        else if (widget.type == TransactionType.SO){
                          SOReq.shared.discountOnGross = discountAmount;
                        }

                        discountAmountController.text = discountAmount.toString();
                      });
                    },
                    keyboardType: TextInputType.number,
                  ),
                ),
              ),
              SizedBox(width: 8.0),
              Expanded(
                child: SizedBox(
                  height: 50,
                  child: TextField(
                    controller: discountAmountController,
                    enableSuggestions: false,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                    decoration: Common().getBCoreID(
                      validityStatus: true,
                      hintText: 'Amount',
                    ),
                    onChanged: (value) {
                      print("called onChanged of Amount");
                      double discountAmount = (value != null && value != '') ? value.toDouble() : 0.0;

                      setState(() {
                        if (widget.type == TransactionType.SAI){
                          SaveSAIReq.shared.discount = discountAmount;
                        }
                        else if (widget.type == TransactionType.SO){
                          SOReq.shared.discountOnGross = discountAmount;
                        }
                        discountPercController.text = _getDiscountInterest().toString();
                      });
                      },
                    keyboardType: TextInputType.number,
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text('Draft'),
              Switch(
                value: SaveSAIReq.shared.appStatus.toBool(),
                activeColor: BCAppTheme().headingTextColor,
                onChanged: (value) {
                  setState(() {
                    SaveSAIReq.shared.appStatus = value.toInt();
                  });
                },
              ),
            ],
          ),
        ],
      );
    } else {
      return SizedBox();
    }
  }

  getCount() {
    return CommonSalesItemInfo.shared.cmnSalesItemList.length;
  }

  removeItem(int index) {
    CommonSalesItemInfo.shared.cmnSalesItemList.removeAt(index);
    if (type == TransactionType.SO) {
      SOReq.shared.salesOrderMaterialsListTemp.removeAt(index);
    } else if (type == TransactionType.SAI) {
      SaveSAIReq.shared.materialDetailsListTemp.removeAt(index);
    } else if (type == TransactionType.DN) {
      SaveDNReq.shared.materialsList.removeAt(index);
    }
  }

  removeItemFromDB() async {
    Future abc = await setDraftTransaction(type: type);
    _transBloc.add(
        UpdateDraftTransaction(draftTransactionData: draftTransactionData));
  }

  setDraftTransaction({TransactionType type}) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    String transDetails;

    switch (type) {
      case TransactionType.SO:
        transDetails = SOReq.shared.toJson().toString();
        break;
      case TransactionType.SAI:
        transDetails = SaveSAIReq.shared.toJson().toString();
        break;
      case TransactionType.DN:
        transDetails = SaveDNReq.shared.toJson().toString();
        break;
    }

    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: transDetails);
    }
  }

  _getTotalInfoField(){
    return Column(
      children: [
        SizedBox(height: 12.0),
        Card(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right: 8.0, top: 8.0,bottom: 4.0),
                    child: Text('Total :',
                      style: TextStyle(color: BCAppTheme().primaryColor),
                      textAlign: TextAlign.center,
                      /*TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)*/),
                  ),
                ],
              ),
              Divider(height: 1.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Text("Items", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(getCount().toString(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Gross Amount", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(_getGrossTotalAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Total Amount", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(_getTotalNetAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  _getGrossTotalAmount() {
    double totalItemsAmount = 0.0;
    double totalAddChargesAmount = 0.0;
    totalItemsAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(
        0, (prevVal, item) => prevVal + ((item.rate ?? 0.0) * item.quantity));
    if (type == TransactionType.SAI) {
      totalAddChargesAmount = CommonAddChargesInfo.shared.cmnAddChargesList.fold(
          0, (prevVal, item) => prevVal + ((item.unitPrice ?? 0.0) * item.quantity));
    }
    return (totalItemsAmount + totalAddChargesAmount).trim().toString();
  }

  _getTotalNetAmount() {
    double totalNetAmount = 0.0;
    double totalAddChargesNetAmount = 0.0;
    totalNetAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + (item.netCost ?? '0.0').toDouble());

    if (type == TransactionType.SAI) {
      totalAddChargesNetAmount = CommonAddChargesInfo.shared.cmnAddChargesList.fold(
          0, (prevVal, item) => prevVal + (item.netAmount ?? 0.0));
    }
    return (totalNetAmount + totalAddChargesNetAmount).trim().toString();
  }
}

/// Item Row that will be used to display the summary of items in the list
class ItemRow extends StatelessWidget {
  /// This will contain the item index
  final int index;

  /// This call back will contain the block of code that will get executed when user selects the Edit button
  final VoidCallback didTapEdit;

  /// This call back will contain the block of code that will get executed when user selects the Delete button
  final VoidCallback didTapDelete;

  /// This will contain the Transaction Type
  final TransactionType type;

  /// This will contain the gross discount value
  final double grossDiscount;

  const ItemRow(
      {Key key,
        this.index,
        this.didTapEdit,
        this.didTapDelete,
        this.type,
        this.grossDiscount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
//    print('notification $notification');
    return Card(
      child: ListTile(
        tileColor: Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  getItemCodeText(),
                  getBarcodeText(),
                  Row(
                    children: [
                      Icon(Icons.scatter_plot, size: 10),
                      SizedBox(width: 4.0),
                      Expanded(
                        child: Text(_getBrandName(),
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0, color: BCAppTheme().textColor),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _getTitleTextField("Item Name"),
                  getItemName(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.public, size: 12),
                      SizedBox(width: 4.0),
                      Flexible(
                        child: Text(_getCountryName(),
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0, color: BCAppTheme().textColor),
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.end,
                            softWrap: true
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              children: <Widget>[ 
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getPackageName(),
                      if (type != TransactionType.DN) getUnitPrice()
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      getQuantityField(),
                      // if (type == TransactionType.SO && grossDiscount != 0.0)
                      //   getGrossDiscountAmountField(),
                      if (type != TransactionType.DN)
                        getTotalCostField()
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(flex: 4, child: Container()),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    child: FlatButton(
                        child: Text("DELETE",
                            style: TextStyle(
                                color: BCAppTheme().redColor, fontSize: 11)),
                        onPressed: () {
//                          Navigator.pop(context);
                          didTapDelete();
                        }),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(Utils.hexToColor("#A88D2A"))
                              .withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 10,
                          offset: Offset(-1, 15), // changes position of shadow
                        ),
                      ],
                    ),
                    child: RaisedButton(
                        padding: EdgeInsets.only(
                            right: 2, left: 2, top: 2, bottom: 2),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(50.0))),
                        onPressed: () {
                          didTapEdit();
                        },
                        textColor: Colors.white,
                        color: BCAppTheme().headingTextColor,
                        child: Container(
                          width: double.maxFinite,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: AutoSizeText(
                                    'EDIT',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11),
                                    maxLines: 1,
                                    minFontSize: 8.0,
                                    stepGranularity: 1.0,
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Image.asset(
                                      "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                            ],
                          ),
                        )),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getItemCodeText() {
    String itemBarcode = CommonSalesItemInfo
        .shared.cmnSalesItemList[index].itemCode
        .toString();
    return _getTextField(itemBarcode);
  }

  getBarcodeText() {
    String itemBarcode = CommonSalesItemInfo
        .shared.cmnSalesItemList[index].itemBarcode
        .toString();
    return _getTextField(itemBarcode);
  }

  getItemName() {
    String itemName =
        CommonSalesItemInfo.shared.cmnSalesItemList[index].itemName;
    return _getTextField(itemName);
  }

  _getBrandName() {
    String brandName = CommonSalesItemInfo.shared.cmnSalesItemList[index].brandName ?? "--";
    return brandName;
  }

  getPackageName() {
    String pkgName =
        "Package: ${CommonSalesItemInfo.shared.cmnSalesItemList[index].pkgName ?? ""}";
    return _getTextField(pkgName);
  }

  getUnitPrice() {
    String itemUnitPrice =
        "Unit Cost: ${CommonSalesItemInfo.shared.cmnSalesItemList[index].rate.toString()}";
    return _getTextField(itemUnitPrice);
  }

  getQuantityField() {
    String itemQuantity =
        "Quantity: ${CommonSalesItemInfo.shared.cmnSalesItemList[index].quantity.toString()}";
    return _getTextField(itemQuantity);
  }

  _getCountryName() {
    String countryName = CommonSalesItemInfo.shared.cmnSalesItemList[index].countryName ?? "--";
    return countryName;
  }

  // getGrossDiscountAmountField() {
  //   String totalCostText = "Gross Discount: ${_getGrossDiscount()}";
  //   return _getTextField(totalCostText);
  // }

  _getGrossDiscount() {
    // double discount;
    // if (grossDiscount != 0.0 &&
    //     CommonSalesItemInfo.shared.cmnSalesItemList[index].totalCost != null) {
    //   discount = CommonSalesItemInfo.shared.cmnSalesItemList[index].totalCost
    //       .toDouble() *
    //       ((grossDiscount ?? 0.0) / 100);
    // }
    // return discount.trim();
  }

  // _getGrossDiscountAsPercentage() {
  //   double discount;
  //   if (grossDiscount != 0.0 &&
  //       CommonSalesItemInfo.shared.cmnSalesItemList[index].netCost != null) {
  //     discount = CommonSalesItemInfo.shared.cmnSalesItemList[index].netCost
  //         .toDouble() *
  //         ((grossDiscount ?? 0.0) / 100);
  //   }
  //   return discount.trim();
  // }

  getTotalCostField() {
    String totalCostText;
    //
    ///// currently we are displaying net cost (with tax ) - gross discount as total cost.
    ///// Actually it should be total cost (without tax) - gross discount + tax amount (totalcost * taxperc)
    // if (grossDiscount != 0.0 &&
    //     CommonSalesItemInfo.shared.cmnSalesItemList[index].netCost != null) {
    //   totalCostText =
    //   "Total Cost: ${(CommonSalesItemInfo.shared.cmnSalesItemList[index].totalCost.toDouble() - _getGrossDiscount()) + ((CommonSalesItemInfo.shared.cmnSalesItemList[index].totalCost.toDouble() - _getGrossDiscount()) * 0.05).trim()}";
    //   /// here the calculation will be
    //   /// ( CommonSalesItemInfo.shared.cmnSalesItemList[index].totalCost - _getGrossDiscount() ) + Tax Amount
    // } else {
      totalCostText =
      "Total Cost: ${CommonSalesItemInfo.shared.cmnSalesItemList[index].netCost}";
    // }
    return _getTextField(totalCostText);
  }

  _getTextField(String text) {
    return Text(text,
        maxLines: 1,
        textAlign: TextAlign.end,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  _getTitleTextField(String text) {
    return Text(text,
        maxLines: 1,
        style: TextStyle(
            fontSize: 12.0,
            color: BCAppTheme().textColor,
            fontWeight: FontWeight.bold),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
}
