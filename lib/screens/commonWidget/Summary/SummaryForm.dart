import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/II/iiReq.dart';
import 'package:bcore_inventory_management/models/request/MTI/SaveTRReq.dart';
import 'package:bcore_inventory_management/models/request/SR/srReq.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemFocDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/request/mrReq.dart';
import 'package:bcore_inventory_management/models/request/mtoReq.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/view/highItem/common_highItem_details.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

class SummaryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Function(int) navigateBackWithValueIndex;
  final Database database;
  final TransactionType type;

  const SummaryForm(
      {Key key,
      this.repository,
      this.appLanguage,
      this.navigateBackWithValueIndex,
      this.database,
      this.type})
      : super(key: key);

  @override
  _SummaryFormState createState() =>
      _SummaryFormState(repository, appLanguage, database, type);
}

class _SummaryFormState extends State<SummaryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;
  var deletingItem,focDetailsOfDeletingItem;

  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;

  _SummaryFormState(
      this.repository, this.appLanguage, this.database, this.type);

  @override
  void initState() {
    super.initState();
    print("items ${CommonHighItemInfo.shared.toJson()}");
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is UpdateDraftTransactionInProgress) {
            Common().showLoader(context);
          } else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
            print("${state.updateDraftTransactionResp}");

            if (state.updateDraftTransactionResp) {
              // Navigator.pop(context);
              // Scaffold.of(context).showSnackBar(SnackBar(
              //     content: Text('Item has been deleted')));
              Common().showMessage(
                  context: context, message: 'Item has been deleted');
                  // duration: Duration(milliseconds: 500)));
              // if (CommonHighItemInfo.shared.cmnHighItemList.isEmpty) {
              //   widget.navigateBackWithValueIndex(null);
              // }
            }
          } else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);

          return Container(
            child: Column(
              children: [
                _getTotalInfoField(),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: getCount(),
                  itemBuilder: (context, index) {
                    return ItemRow(
                      type: type,
                      index: index,
                      didTapEdit: () {
                        widget.navigateBackWithValueIndex(index);
                      },
                      didTapDelete: () {
                        setState(() {
                          removeItem(index);
                          removeItemFromDB();
                        });
                      },
                    );
                  },
                ),
                SizedBox(height: 16),
              ],
            ),
          );
        }),
      ),
    );
  }

  getCount() {
    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        return CommonHighItemInfo.shared.cmnHighItemList.length;
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        return CommonItemViewModel.shared.cmnItemList.length;
        break;
    }
  }

  removeItem(int index) {
    if (type == TransactionType.MR) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      MRReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.GRN) {
       deletingItem = CommonHighItemInfo.shared.cmnHighItemList[index];
       focDetailsOfDeletingItem = GrnReq.shared.fOCDetails
          .where((element) => element.parentIndex == deletingItem.index)
          .toList();
      if (focDetailsOfDeletingItem.isNotEmpty || deletingItem == null || focDetailsOfDeletingItem == null) {
        Common().showAlertMessageWithAction(
            context: context,
            title: Constants.alertMsgTitle,
            message: 'The corresponding FOC item will also be deleted',
            okButtonTitle: "Ok",
            okFunction: () {
              Navigator.pop(context);
              CommonHighItemInfo.shared.cmnHighItemList.removeAt(index);
              GrnReq.shared.itemDetails.removeAt(index);
              for (FOCDetails focItem in focDetailsOfDeletingItem) {
                GrnReq.shared.itemDetails.removeWhere((element) =>
                    element.isFoc && element.index == focItem.focIndex);
                GrnReq.shared.fOCDetails.removeWhere(
                    (element) => element.focIndex == focItem.focIndex);
              }
            },
            actionButtonTitle: "Cancel",
            actionFunction: () {
              Navigator.pop(context);
            });
      } else {
        if (deletingItem != null) {
          CommonHighItemInfo.shared.cmnHighItemList.removeAt(index);
          GrnReq.shared.itemDetails.removeAt(index);
        }
      }
    } else if (type == TransactionType.PR) {
      CommonHighItemInfo.shared.cmnHighItemList.removeAt(index);
      SavePRReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.SI) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      SIReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.II) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      SaveIIReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.MTO) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      MTOReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.SR) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      SRReq.shared.itemDetails.removeAt(index);
    } else if (type == TransactionType.MTI) {
      CommonItemViewModel.shared.cmnItemList.removeAt(index);
      SaveTRReq.shared.itemDetails.removeAt(index);
    }
  }

  removeItemFromDB() async {
    Future abc = await setDraftTransaction(type: type);
    _transBloc.add(
        UpdateDraftTransaction(draftTransactionData: draftTransactionData));
  }

  setDraftTransaction({TransactionType type}) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    String transDetails;

    switch (widget.type) {
      case TransactionType.MR:
        transDetails = MRReq.shared.toJson().toString();
        break;
      case TransactionType.GRN:
        transDetails = GrnReq.shared.toJson().toString();
        break;
      case TransactionType.PR:
        transDetails = SavePRReq.shared.toJson().toString();
        break;
      case TransactionType.SI:
        transDetails = SIReq.shared.toJson().toString();
        break;
      case TransactionType.II:
        transDetails = SaveIIReq.shared.toJson().toString();
        break;
      case TransactionType.MTO:
        transDetails = MTOReq.shared.toJson().toString();
        break;
      case TransactionType.MTI:
        transDetails = SaveTRReq.shared.toJson().toString();
        break;
    }

    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: transDetails);
    }
  }

  _getTotalInfoField() {
    return Column(
      children: [
        SizedBox(height: 12.0),
        Card(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 8.0, bottom: 4.0),
                    child: Text(
                      'Total :',
                      style: TextStyle(color: BCAppTheme().primaryColor),
                      textAlign: TextAlign.center,
                      /*TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)*/
                    ),
                  ),
                ],
              ),
              Divider(height: 1.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Text("Items",
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold)),
                      Text(getCount().toString(),
                          style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Gross Amount",
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold)),
                      Text(_getGrossTotalAmount(),
                          style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Total Amount",
                          style: TextStyle(
                              fontSize: 12.0, fontWeight: FontWeight.bold)),
                      Text(_getTotalNetAmount(),
                          style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  _getGrossTotalAmount() {
    double totalAmount;
    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        totalAmount = CommonHighItemInfo.shared.cmnHighItemList.fold(
            0,
            (prevVal, item) =>
                prevVal +
                (item.isFoc
                    ? 0.0
                    : ((item.unitPrice ?? 0.0 * item.unitQty ?? 0.0) +
                        // : ((item.unitPrice * item.unitQty ) +
                        (item.exciseDuty ?? 0.0))));
        return totalAmount.trim().toString();
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        totalAmount = CommonItemViewModel.shared.cmnItemList.fold(
            0, (prevVal, item) => prevVal + (item.UnitCost * item.ReqQty));
        return totalAmount.trim().toString();
        break;
    }
  }

  _getTotalNetAmount() {
    double totalAmount;
    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        totalAmount = CommonHighItemInfo.shared.cmnHighItemList.fold(0,
            (prevVal, item) => prevVal + (item.isFoc ? 0.0 : item.totalCost));
        return totalAmount.trim().toString();
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        totalAmount = CommonItemViewModel.shared.cmnItemList
            .fold(0, (prevVal, item) => prevVal + item.TotalCost);
        return totalAmount.trim().toString();
        break;
    }
  }
}

/// Item Row that will be used to display the summary of items in the list
class ItemRow extends StatelessWidget {
  /// This will contain the item index
  final int index;

  /// This call back will contain the block of code that will get executed when user selects the Edit button
  final VoidCallback didTapEdit;

  /// This call back will contain the block of code that will get executed when user selects the Delete button
  final VoidCallback didTapDelete;

  /// This will contain the Transaction Type
  final TransactionType type;

  const ItemRow(
      {Key key, this.index, this.didTapEdit, this.didTapDelete, this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        tileColor: Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text( _getItemCodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(_getBarcodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Row(
                    children: [
                      Icon(Icons.scatter_plot, size: 10),
                      SizedBox(width: 4.0),
                      Expanded(
                        child: Text(_getBrandName(),
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("Item Name",
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(_getItemName(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.public, size: 12),
                      SizedBox(width: 4.0),
                      Flexible(
                        child: Text(_getCountryName(),
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0, color: BCAppTheme().textColor),
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.end,
                            softWrap: true
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Package: ${_getPackageName()}",
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                      Text(_getUnitPrice(),
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(_getQuantity(),
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                      _getTotalCost()
                    ],
                  ),
                )
              ],
            ),
            if (_shouldShowSubstituteField()) SizedBox(height: 8),
            if (_shouldShowSubstituteField())
              Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Substitute",
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0,
                                color: BCAppTheme().primaryColor),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true),
                      ],
                    ),
                  )
                ],
              ),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(flex: 4, child: Container()),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    child: FlatButton(
                        child: Text("DELETE",
                            style: TextStyle(
                                color: BCAppTheme().redColor, fontSize: 11)),
                        onPressed: () {
                          didTapDelete();
                        }),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(Utils.hexToColor("#A88D2A"))
                              .withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 10,
                          offset: Offset(-1, 15), // changes position of shadow
                        ),
                      ],
                    ),
                    child: RaisedButton(
                        padding: EdgeInsets.only(
                            right: 2, left: 2, top: 2, bottom: 2),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        onPressed: () {
                          didTapEdit();
                        },
                        textColor: Colors.white,
                        color: BCAppTheme().headingTextColor,
                        child: Container(
                          width: double.maxFinite,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: AutoSizeText(
                                    'EDIT',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11),
                                    maxLines: 1,
                                    minFontSize: 8.0,
                                    stepGranularity: 1.0,
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Image.asset(
                                      "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                            ],
                          ),
                        )),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
  _getItemCodeText() {
    String itemBarcode;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        itemBarcode =
            CommonHighItemInfo.shared.cmnHighItemList[index].itemCode ?? "";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        itemBarcode =
            CommonItemViewModel.shared.cmnItemList[index].ItemCode ?? "";
        break;
    }
    return itemBarcode;
  }

  _getBarcodeText() {
    String itemBarcode;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        itemBarcode =
            CommonHighItemInfo.shared.cmnHighItemList[index].itemBarcode ?? "";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        itemBarcode =
            CommonItemViewModel.shared.cmnItemList[index].ItemBarcode ?? "";
        break;
    }
    return itemBarcode;
  }

  _getBrandName() {
    String brandName;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        brandName =
            CommonHighItemInfo.shared.cmnHighItemList[index].brandName ?? "--";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        brandName =
            CommonItemViewModel.shared.cmnItemList[index].BrandName ?? "--";
        break;
    }
    return brandName;
  }

  _shouldShowSubstituteField() {
    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
      case TransactionType.MTI:
      case TransactionType.SR:
      case TransactionType.MR:
        return false;
        break;
      case TransactionType.SI:
      case TransactionType.MTO:
      case TransactionType.II:
        return CommonItemViewModel.shared.cmnItemList[index].isSubstitute ??
            false;
        break;
    }
  }

  _getItemName() {
    String itemName;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        itemName =
            CommonHighItemInfo.shared.cmnHighItemList[index].itemName ?? "";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        itemName = CommonItemViewModel.shared.cmnItemList[index].ItemName ?? "";
        break;
    }
    return itemName;
  }

  _getCountryName() {
    String countryName;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        countryName =
            CommonHighItemInfo.shared.cmnHighItemList[index].countryName ?? "--";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        countryName =
            CommonItemViewModel.shared.cmnItemList[index].CountryName ?? "--";
        break;
    }
    return countryName;
  }

  _getPackageName() {
    String pkgName;
    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        pkgName =
            CommonHighItemInfo.shared.cmnHighItemList[index].pkgName ?? "";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        pkgName = CommonItemViewModel.shared.cmnItemList[index].PackageName;
        break;
    }
    return pkgName;
  }

  _getUnitPrice() {
    String itemUnitPrice;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        itemUnitPrice =
            "Unit Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].unitPrice.trim()}" ??
                "";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        itemUnitPrice =
            "Unit Cost: ${CommonItemViewModel.shared.cmnItemList[index].UnitCost.trim()}" ??
                "";
        break;
    }
    return itemUnitPrice;
  }

  _getQuantity() {
    String itemQuantity;

    switch (type) {
      case TransactionType.GRN:
      case TransactionType.PR:
        itemQuantity =
            // "Quantity: ${CommonHighItemInfo.shared.cmnHighItemList[index].unitQty != null ? CommonHighItemInfo.shared.cmnHighItemList[index].unitQty : 0.0}";
        "Quantity: ${CommonHighItemInfo.shared.cmnHighItemList[index].unitQty}";

        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        itemQuantity =
            "Quantity: ${CommonItemViewModel.shared.cmnItemList[index].ReqQty}" ??
                "";
        break;
    }
    return itemQuantity;
  }

  _getTotalCost() {
    String totalCostText;

    switch (type) {
      case TransactionType.GRN:
        if (GrnReq.shared.itemDetails[index].isFoc) {
          totalCostText = "FOC";
        } else {
          totalCostText =
              "Total Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].totalCost.trim()}";
        }
        break;
      case TransactionType.PR:
        totalCostText =
            "Total Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].totalCost.trim()}";
        break;
      case TransactionType.MR:
      case TransactionType.SI:
      case TransactionType.SR:
      case TransactionType.MTI:
      case TransactionType.MTO:
      case TransactionType.II:
        totalCostText =
            "Total Cost: ${CommonItemViewModel.shared.cmnItemList[index].TotalCost.trim()}";
        break;
    }
    return Text(totalCostText,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
}
