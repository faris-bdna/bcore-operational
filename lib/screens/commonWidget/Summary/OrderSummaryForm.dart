import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/OF/saveOrderFormReq.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderItemInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

class OrderSummaryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Function(int) navigateBackWithValueIndex;
  final Database database;

  const OrderSummaryForm(
      {Key key,
        this.repository,
        this.appLanguage,
        this.navigateBackWithValueIndex,
        this.database,
      })
      : super(key: key);

  @override
  _OrderSummaryFormState createState() =>
      _OrderSummaryFormState(repository, appLanguage, database);
}

class _OrderSummaryFormState extends State<OrderSummaryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  // int deletingItemIndex;
  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;

  _OrderSummaryFormState(
      this.repository, this.appLanguage, this.database);

  @override
  void initState() {
    super.initState();
    print("items ${OrderItemInfo.shared.toJson()}");
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider<TransBloc>(
      create: (context) => TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {

          if (state is UpdateDraftTransactionInProgress) {
            Common().showLoader(context);
          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
//            print("${state.updateDraftTransactionResp}");

            if (state.updateDraftTransactionResp) {
              // removeItem(deletingItemIndex);
              // deletingItemIndex = null;
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item has been deleted'),
                  duration: Duration(milliseconds: 500)));
            }
          }
          else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);
          return Container(
            child: Column(
              children: [
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: getCount(),
                  itemBuilder: (context, index) {
                    return ItemRow(
                      index: index,
                      didTapEdit: () {
                        widget.navigateBackWithValueIndex(index);
                      },
                      didTapDelete: () {
                        setState(() {
                          // deletingItemIndex = index;
                          removeItem(index);
                          removeItemFromDB();
                        });
                      },
                    );
                  },
                ),
                SizedBox(height: 16),
              ],
            ),
          );
        }),
      ),
    );
  }

  getCount(){
    return OrderItemInfo.shared.orderItemList.length;
  }

  removeItem(int index){
    SaveOFReq.shared.itemDetails.removeAt(index);
    OrderItemInfo.shared.orderItemList.removeAt(index);
  }

  removeItemFromDB() async {
    Future abc = await setDraftTransaction(type: TransactionType.OF);
    _transBloc.add(
        UpdateDraftTransaction(draftTransactionData: draftTransactionData));
  }

  setDraftTransaction({TransactionType type}) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    String transDetails = SaveOFReq.shared.toJson().toString();

    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: transDetails);
    }
  }
}

/// Item Row that will be used to display the summary of items in the list
class ItemRow extends StatelessWidget {
  /// This will contain the item index
  final int index;

  /// This call back will contain the block of code that will get executed when user selects the Edit button
  final VoidCallback didTapEdit;

  /// This call back will contain the block of code that will get executed when user selects the Delete button
  final VoidCallback didTapDelete;

  const ItemRow(
      {Key key, this.index, this.didTapEdit, this.didTapDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      child: ListTile(
        tileColor: Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(getItemCodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(getBarcodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Row(
                    children: [
                      Icon(Icons.scatter_plot, size: 10),
                      SizedBox(width: 4.0),
                      Expanded(
                        child: Text(_getBrandName(),
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0, color: BCAppTheme().textColor),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("Item Name",
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(getItemName(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.public, size: 12),
                      SizedBox(width: 4.0),
                      Flexible(
                        child: Text(_getCountryName(),
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 11.0, color: BCAppTheme().textColor),
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.end,
                            softWrap: true
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Package: ${getPackageName()}",
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                      // SizedBox()
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(getQuantity(),
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(flex: 4, child: Container()),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    child: FlatButton(
                        child: Text("DELETE",
                            style: TextStyle(
                                color: BCAppTheme().redColor, fontSize: 11)),
                        onPressed: () {
                          didTapDelete();
                        }),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(Utils.hexToColor("#A88D2A"))
                              .withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 10,
                          offset: Offset(-1, 15), // changes position of shadow
                        ),
                      ],
                    ),
                    child: RaisedButton(
                        padding: EdgeInsets.only(
                            right: 2, left: 2, top: 2, bottom: 2),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(50.0))),
                        onPressed: () {
                          didTapEdit();
                        },
                        textColor: Colors.white,
                        color: BCAppTheme().headingTextColor,
                        child: Container(
                          width: double.maxFinite,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: AutoSizeText(
                                    'EDIT',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11),
                                    maxLines: 1,
                                    minFontSize: 8.0,
                                    stepGranularity: 1.0,
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Image.asset(
                                      "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                            ],
                          ),
                        )),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
  getItemCodeText(){
    String itemBarcode =  OrderItemInfo.shared.orderItemList[index].itemCode ?? " ";
    return itemBarcode;
  }
  getBarcodeText(){
    String itemBarcode =  OrderItemInfo.shared.orderItemList[index].itemBarcode ?? " ";
    return itemBarcode;
  }

  getItemName(){
    String itemName =  OrderItemInfo.shared.orderItemList[index].itemName ?? "";
    return itemName;
  }

  _getBrandName() {
    String brandName = OrderItemInfo.shared.orderItemList[index].brandName ?? "--";
    return brandName;
  }

  _getCountryName() {
    String countryName = OrderItemInfo.shared.orderItemList[index].countryName ?? "--";
    return countryName;
  }

  getPackageName(){
    String pkgName = OrderItemInfo.shared.orderItemList[index].pkgName ?? "";
    return pkgName;
  }

  getQuantity(){
    String itemQuantity = "Quantity: ${OrderItemInfo.shared.orderItemList[index].orderQty}";
    return itemQuantity;
  }
}
