import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicItemInfo.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

class BasicSummaryForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Function(int) navigateBackWithValueIndex;
  final Database database;
  final TransactionType type;

  const BasicSummaryForm(
      {Key key,
        this.repository,
        this.appLanguage,
        this.navigateBackWithValueIndex,
        this.database,
        this.type})
      : super(key: key);

  @override
  _BasicSummaryFormState createState() =>
      _BasicSummaryFormState(repository, appLanguage, database, type);
}

class _BasicSummaryFormState extends State<BasicSummaryForm> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final TransactionType type;

  int deletingItemIndex;
  bool shouldContainTopBar = false;
  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;

  _BasicSummaryFormState(
      this.repository, this.appLanguage, this.database, this.type);

  @override
  void initState() {
    super.initState();

    if (type == TransactionType.OB){
      shouldContainTopBar = true;
    }

    print("items ${CommonBasicItemInfo.shared.toJson()}");
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider<TransBloc>(
      create: (context) => TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {

          if (state is UpdateDraftTransactionInProgress) {
            Common().showLoader(context);
          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
//            print("${state.updateDraftTransactionResp}");

            if (state.updateDraftTransactionResp) {
              removeItem(deletingItemIndex);
              deletingItemIndex = null;
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item has been deleted'),
                  duration: Duration(milliseconds: 500)));
            }
          }
          else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);

          return Container(
            child: Column(
              children: [
                if (shouldContainTopBar) _getTotalInfoField(),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: getCount(),
                  itemBuilder: (context, index) {
                    return ItemRow(
                      database: database,
                      type: type,
                      index: index,
                      didTapEdit: () {
                        widget.navigateBackWithValueIndex(index);
                      },
                      didTapDelete: () {
                        setState(() {
                          deletingItemIndex = index;
                          removeItemFromDB();
                        });
                      },
                    );
                  },
                ),
                SizedBox(height: 16),
              ],
            ),
          );
        }),
      ),
    );
  }

  getCount(){
    return CommonBasicItemInfo.shared.cmnBasicItemList.length;
  }

  removeItem(int index){
    if (type == TransactionType.ST)
      SaveSTReq.shared.itemDetails.removeAt(index);
    else if (type == TransactionType.OB)
      SaveOBReq.shared.itemDetails.removeAt(index);

    CommonBasicItemInfo.shared.cmnBasicItemList.removeAt(index);
  }

  removeItemFromDB() async {
    Future abc = await setDraftTransaction(type: type);
    _transBloc.add(
        UpdateDraftTransaction(draftTransactionData: draftTransactionData));
  }

  setDraftTransaction({TransactionType type}) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    String transDetails = (type == TransactionType.ST) ? SaveSTReq.shared.toJson().toString() : SaveSTReq.shared.toJson().toString();

    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: transDetails);
    }
  }

  _getTotalInfoField(){
    return Column(
      children: [
        SizedBox(height: 12.0),
        Card(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right: 8.0, top: 8.0,bottom: 4.0),
                    child: Text('Total :',
                      style: TextStyle(color: BCAppTheme().primaryColor),
                      textAlign: TextAlign.center,
                  ),
                  )
                ],
              ),
              Divider(height: 1.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Text("Items", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(getCount().toString(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Gross Amount", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(_getGrossTotalAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  ),
                  Column(
                    children: [
                      Text("Total Amount", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                      Text(_getTotalNetAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  _getGrossTotalAmount(){
    double totalAmount;
    if (CommonBasicItemInfo.shared.cmnBasicItemList.length != null) {
      for (int i = 0; i <
          CommonBasicItemInfo.shared.cmnBasicItemList.length; i++) {
        if (CommonBasicItemInfo.shared.cmnBasicItemList[i].averageCost !=
            null) {
          totalAmount = CommonBasicItemInfo.shared.cmnBasicItemList.fold(
              0,
                  (prevVal, item) =>
              prevVal +
                  double.tryParse(
                      double.tryParse(item.averageCost).toString() ?? '0.0') *
                      item.phyQty);
          return totalAmount.trim().toString();
        }
        else {
          return  totalAmount.trim().toString();
        }
      }
    }
  }

  _getTotalNetAmount(){
    double totalAmount =  CommonBasicItemInfo.shared.cmnBasicItemList.fold(0,
            (prevVal, item) => prevVal + double.tryParse(item.netCost ?? '0.0'));
    return totalAmount.trim().toString();
  }
}

/// Item Row that will be used to display the summary of items in the list
class ItemRow extends StatelessWidget {
  /// This will contain the item index
  final int index;

  /// This call back will contain the block of code that will get executed when user selects the Edit button
  final VoidCallback didTapEdit;

  /// This call back will contain the block of code that will get executed when user selects the Delete button
  final VoidCallback didTapDelete;

  /// This will contain the Transaction Type
  final TransactionType type;

  final Database database;

  const ItemRow(
      {Key key, this.index, this.didTapEdit, this.didTapDelete, this.type, this.database})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      child: ListTile(
        tileColor: Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(getItemCodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(getBarcodeText(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("Item Name",
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: BCAppTheme().textColor,
                          fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                  Text(getItemName(),
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 11.0, color: BCAppTheme().textColor),
                      overflow: TextOverflow.ellipsis,
                      softWrap: true),
                ],
              ),
            )
          ],
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Package : ${getPackageName()}",
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                      (type == TransactionType.OB)
                          ? Text(getUnitPrice(),
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 11.0,
                                  color: BCAppTheme().textColor),
                              overflow: TextOverflow.ellipsis,
                              softWrap: true)
                          : SizedBox()
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(getQuantity(),
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 11.0, color: BCAppTheme().textColor),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true),
                      (type == TransactionType.OB)
                          ? getTotalCost()
                          : SizedBox()
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(flex: 4, child: Container()),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    child: FlatButton(
                        child: Text("DELETE",
                            style: TextStyle(
                                color: BCAppTheme().redColor, fontSize: 11)),
                        onPressed: () {
                          didTapDelete();
                        }),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 25,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(Utils.hexToColor("#A88D2A"))
                              .withOpacity(0.1),
                          spreadRadius: 0,
                          blurRadius: 10,
                          offset: Offset(-1, 15), // changes position of shadow
                        ),
                      ],
                    ),
                    child: RaisedButton(
                        padding: EdgeInsets.only(
                            right: 2, left: 2, top: 2, bottom: 2),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(50.0))),
                        onPressed: () {
                          didTapEdit();
                        },
                        textColor: Colors.white,
                        color: BCAppTheme().headingTextColor,
                        child: Container(
                          width: double.maxFinite,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: AutoSizeText(
                                    'EDIT',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11),
                                    maxLines: 1,
                                    minFontSize: 8.0,
                                    stepGranularity: 1.0,
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Image.asset(
                                      "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                            ],
                          ),
                        )),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
  getItemCodeText(){
    String itemBarcode =  CommonBasicItemInfo.shared.cmnBasicItemList[index].itemCode ?? " ";
    return itemBarcode;
  }

  getBarcodeText(){
    String itemBarcode =  CommonBasicItemInfo.shared.cmnBasicItemList[index].itemBarcode ?? " ";
    return itemBarcode;
  }

  getItemName(){
    String itemName =  CommonBasicItemInfo.shared.cmnBasicItemList[index].itemName ?? "";
    return itemName;
  }

  getPackageName(){
    // String pkgName = CommonBasicItemInfo.shared.cmnBasicItemList[index].packingDesc ?? "";
    // return pkgName;
    String pkgName = CommonBasicItemInfo.shared.cmnBasicItemList[index].pkgName ?? "";
    return pkgName;
  }

  getBrandName(){
    List<ProductBrandData> loadBrandResp;

  }
  getCountryName()async{
    // List<CountryOriginData> loadCountryResp;
    // loadCountryResp = await database.countryOriginDao.getCountryNameById(event.countryId);
  }

  getUnitPrice(){
    String itemUnitPrice = "Unit Cost : ${(CommonBasicItemInfo.shared.cmnBasicItemList[index].averageCost ?? "0.0").toDouble()}";
    return itemUnitPrice;
  }

  getQuantity(){
    String itemQuantity = "Quantity: ${CommonBasicItemInfo.shared.cmnBasicItemList[index].phyQty}";
    return itemQuantity;
  }

  getTotalCost() {
    String totalCostText =
          "Total Cost : ${(CommonBasicItemInfo.shared.cmnBasicItemList[index].netCost ?? "0.0").toDouble()}";
    return
      Text(totalCostText,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);

  }
}
