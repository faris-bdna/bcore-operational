part of 'utilities_bloc.dart';

abstract class UtilitiesEvent extends Equatable {
  const UtilitiesEvent();
}

class GetAdditionalServices extends UtilitiesEvent {
  const GetAdditionalServices();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetAdditionalServices';
}

class GetTermsAndCondition extends UtilitiesEvent {
  const GetTermsAndCondition();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetTermsAndCondition';
}

class GetPaymentType extends UtilitiesEvent {
  const GetPaymentType();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetPaymentType';
}

class GetPaymentSubType extends UtilitiesEvent {
  final String id;

  const GetPaymentSubType(this.id);

  @override
  List<Object> get props => [id];

  @override
  String toString() =>
      'GetPaymentSubType {"Id" : "$id"}';
}
