part of 'utilities_bloc.dart';

abstract class UtilitiesState extends Equatable {
  const UtilitiesState();

  @override
  List<Object> get props => [];
}


class GetPaymentTypeInitial extends UtilitiesState {}

class GetPaymentTypeInProgress extends UtilitiesState {}

class GetPaymentTypeComplete extends UtilitiesState {
  final GetPaymentTypeResp resp;

  const GetPaymentTypeComplete({@required this.resp})
      : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetPaymentTypeFailure extends UtilitiesState {
  final String error;

  const GetPaymentTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPaymentTypeFailure { error: $error }';
}


class GetAdditionalServicesInitial extends UtilitiesState {}

class GetAdditionalServicesInProgress extends UtilitiesState {}

class GetAdditionalServicesComplete extends UtilitiesState {
  final GetAdditionalServicesResp resp;

  const GetAdditionalServicesComplete({@required this.resp})
      : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetAdditionalServicesFailure extends UtilitiesState {
  final String error;

  const GetAdditionalServicesFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetAdditionalServicesFailure { error: $error }';
}


class GetTermsAndConditionInitial extends UtilitiesState {}

class GetTermsAndConditionInProgress extends UtilitiesState {}

class GetTermsAndConditionComplete extends UtilitiesState {
  final TermsAndConditionsResp resp;

  const GetTermsAndConditionComplete({@required this.resp})
      : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetTermsAndConditionFailure extends UtilitiesState {
  final String error;

  const GetTermsAndConditionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetTermsAndConditionFailure { error: $error }';
}


class GetPaymentSubTypeInitial extends UtilitiesState {}

class GetPaymentSubTypeInProgress extends UtilitiesState {}

class GetPaymentSubTypeComplete extends UtilitiesState {
  final GetPaymentTypeResp resp;

  const GetPaymentSubTypeComplete({@required this.resp})
      : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetPaymentSubTypeFailure extends UtilitiesState {
  final String error;

  const GetPaymentSubTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPaymentSubTypeFailure { error: $error }';
}