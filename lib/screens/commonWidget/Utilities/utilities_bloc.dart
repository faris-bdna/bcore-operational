import 'dart:async';

import 'package:bcore_inventory_management/models/response/utilities/getAdditionalServicesResp.dart';
import 'package:bcore_inventory_management/models/response/utilities/termsAndConditionsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/getPaymentTypeResp.dart';

part 'utilities_event.dart';
part 'utilities_state.dart';

class UtilitiesBloc extends Bloc<UtilitiesEvent, UtilitiesState> {
  final BCRepository bcRepository;
  // final Database database;

  UtilitiesBloc(
      {@required this.bcRepository})
      : assert(bcRepository != null),
        super(GetPaymentTypeInitial());

  @override
  Stream<UtilitiesState> mapEventToState(UtilitiesEvent event) async* {

    if (event is GetAdditionalServices) {
      yield GetAdditionalServicesInProgress();
      try {
        final GetAdditionalServicesResp resp = await bcRepository.getAdditionalServices();
        yield GetAdditionalServicesComplete(resp: resp);
      } catch (error) {
        yield GetAdditionalServicesFailure(error: error.toString());
      }
    }
    else if (event is GetTermsAndCondition) {
      yield GetTermsAndConditionInProgress();
      try {
        final TermsAndConditionsResp resp = await bcRepository.getTermsAndConditionList();
        yield GetTermsAndConditionComplete(resp: resp);
      } catch (error) {
        yield GetTermsAndConditionFailure(error: error.toString());
      }
    }
    else if (event is GetPaymentType) {
      yield GetPaymentTypeInProgress();
      try {
        final GetPaymentTypeResp resp = await bcRepository.getPaymentType();
        yield GetPaymentTypeComplete(resp: resp);
      } catch (error) {
        yield GetPaymentTypeFailure(error: error.toString());
      }
    }
    else if (event is GetPaymentSubType) {
      yield GetPaymentSubTypeInProgress();
      try {
        final GetPaymentTypeResp resp = await bcRepository.getPaymentSubType(id: event.id);
        yield GetPaymentSubTypeComplete(resp: resp);
      } catch (error) {
        yield GetPaymentSubTypeFailure(error: error.toString());
      }
    }
  }
}
