import 'package:auto_size_text/auto_size_text.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/getPaymentTypeResp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnPaymentMethods.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Utilities/utilities_bloc.dart';
import 'package:date_form_field/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';


class PaymentMethodForm extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final TransactionType type;

  const PaymentMethodForm(
      {Key key, this.repository, this.appLanguage, this.type})
      : super(key: key);

  @override
  _PaymentMethodFormState createState() => _PaymentMethodFormState(repository);
}

class _PaymentMethodFormState extends State<PaymentMethodForm> {

  final BCRepository repository;

  _PaymentMethodFormState(this.repository);

  List<DropdownMenuItem> typeDropDownButtons = [];
  List<DropdownMenuItem> subTypeDropDownButtons = [];
  List<PaymentTypeMasterDataList> paymentTypeList = [];
  List<PaymentTypeMasterDataList> paymentSubTypeList = [];

  UtilitiesBloc _utilitiesBloc;

  // PaymentTypeMasterDataList /*selectedPaymentType, */selectedPaymentSubType;

  EditMode _editMode;

  CmnPaymentMethod paymentMethod;
  var _amountController = TextEditingController();
  // var _chequeNameController = TextEditingController();
  // var _chequeNoController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _editMode = (paymentMethod == null) ? EditMode.Off : EditMode.On;

    if (paymentMethod == null){
      paymentMethod = CmnPaymentMethod();
    }
    Future.delayed(Duration.zero,()
    {
      _utilitiesBloc.add(GetPaymentType());
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UtilitiesBloc>(
        create: (context) => UtilitiesBloc(bcRepository: repository),
        child: BlocListener<UtilitiesBloc, UtilitiesState>(
          listener: (context, state) {
            if (state is GetPaymentTypeInProgress ||
                state is GetPaymentSubTypeInProgress
            ) {
              Common().showLoader(context);
            }
            else if (state is GetPaymentTypeFailure ||
            state is GetPaymentSubTypeFailure
            ) {
              Navigator.pop(context);
            }
            else if (state is GetPaymentTypeComplete) {
              Navigator.pop(context);

              if (state.resp.masterDataList.isNotEmpty) {
                setState(() {
                  paymentTypeList = state.resp.masterDataList;
                });
              }
            }
            else if (state is GetPaymentSubTypeComplete) {
              Navigator.pop(context);

              if (state.resp.masterDataList.isNotEmpty) {
                setState(() {
                  paymentSubTypeList = state.resp.masterDataList;
                });
              }
            }
          },
          child: BlocBuilder<UtilitiesBloc, UtilitiesState>(
              builder: (context, state) {
                _utilitiesBloc = BlocProvider.of<UtilitiesBloc>(context);
                return Column(
                  children: [
                    _getPaymentType(),
                    _getSubType(),
                    _getAmount(),
                    _getTotalPaymentInfoField(),
                    SizedBox(height: 8.0),
                    _getPaymentMethodList()
                  ],
                );
              }),
        )
    );
  }

  _getPaymentType() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.zero,
          decoration: Common().getBCoreSD(isMandatory: true,isValidated: paymentMethod.paymentTypeValue != null),
          child: SearchableDropdown.single(
            items: (paymentTypeList != null && paymentTypeList.isNotEmpty)
                ? paymentTypeList.map((PaymentTypeMasterDataList item) {
              return DropdownMenuItem(
                  child: Text(item.text,
                      style: TextStyle(
                        fontSize: 14.0,
                        color: BCAppTheme().subTextColor,
                      )),
                  value: item.text);
            }).toList()
                : [],
            value: paymentMethod.paymentTypeValueName,//selectedPaymentType?.value,
            hint: "Payment Type",
            searchHint: "Payment Type",
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  paymentMethod.clear();
                  PaymentTypeMasterDataList selectedPaymentType =
                      paymentTypeList
                          .where((item) => item.text == value)
                          .toList()[0];
                  paymentMethod.paymentTypeValue = selectedPaymentType.value;
                  paymentMethod.paymentTypeValueName = selectedPaymentType.text;

                  paymentMethod.paymentSubTypeValue = null;
                  paymentMethod.paymentSubTypeValueName = null;
                  paymentSubTypeList.clear();

                  _utilitiesBloc
                      .add(GetPaymentSubType(selectedPaymentType.value));
                });
              }
            },
            displayClearIcon: false,
            underline: Container(),
            isExpanded: true,
          ),
        ),
      ],
    );
  }

  _getSubType() {
    return Column(
      children: [
        SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.zero,
                decoration: Common().getBCoreSD(isMandatory: true,isValidated: paymentMethod.paymentSubTypeValue != null),
                child: SearchableDropdown.single(
                  items: (paymentSubTypeList != null && paymentSubTypeList.isNotEmpty)
                      ? paymentSubTypeList.map((PaymentTypeMasterDataList item) {
                    return DropdownMenuItem(
                        child: Text(item.text,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: BCAppTheme().subTextColor,
                            )),
                        value: item.text);
                  }).toList()
                      : [],
                  value: paymentMethod.paymentSubTypeValueName,
                  hint: "Sub Type",
                  searchHint: "Sub Type",
                  onChanged: (value) {
                    if (value != null) {
                      setState(() {
                        PaymentTypeMasterDataList selectedPaymentSubType =
                            paymentSubTypeList
                                .where((item) => item.text == value)
                                .toList()[0];
                        paymentMethod.paymentSubTypeValue =
                            selectedPaymentSubType.value;
                        paymentMethod.paymentSubTypeValueName =
                            selectedPaymentSubType.text;
                      });
                    }
                  },
                  displayClearIcon: false,
                  underline: Container(),
                  isExpanded: true,
                ),
              ),
            ),
            if (paymentMethod.paymentSubTypeValueName == "Cheque")
              IconButton(icon: Icon(Icons.info_outline, color: BCAppTheme().grayColor), onPressed: (){
              showChequeDetailsDialogue();
            })
          ],
        ),
      ],
    );
  }

  _getAmount(){
    return Column(
      children: [
        SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 50,
                child: TextField(
                  enableSuggestions: false,
                  textInputAction: TextInputAction.next,
                  controller: _amountController,
                  autocorrect: false,
                  decoration: Common().getBCoreID(
                    validityStatus: true,
                    hintText: 'Amount',
                  ),
                  onChanged: (value){
                    if (value != null && value != '') {
                      paymentMethod.amount = value.toDouble(pos: getDeci());
                    }else{
                      paymentMethod.amount = 0.0;
                    }
                  },
                  keyboardType: TextInputType.number,
                ),
              ),
            ),
            SizedBox(width: 8.0),
            Container(
              width: 40.0,
              height: 50.0,
              child: FlatButton(
                  padding: EdgeInsets.zero,
                  color: BCAppTheme().headingTextColor,
                  onPressed: () {
                    if(_validateFields()){
                      setReqDetailJson();
                    }
                  },
                  child: Icon(
                    Icons.save,
                    size: 24,
                    color: Colors.white,
                  )),
            ),
          ],
        ),
      ],
    );
  }

  _validateFields() {
    if (paymentMethod.paymentTypeValue == null) {
      Common()
          .showMessage(context: context, message: "Please select Payment Type");
      return false;
    }
    if (paymentMethod.paymentSubTypeValue == null) {
      Common().showMessage(
          context: context, message: "Please select Payment Sub Type");
      return false;
    }
    if ((paymentMethod.paymentSubTypeValueName == "Cheque")) {

      if ((paymentMethod.chequeDate == null)) {
        Common().showMessage(context: context, message: "Please enter Cheque Date");
        return false;
      }
      if ((paymentMethod.chequeName == null)) {
        Common().showMessage(context: context, message: "Please enter Cheque Name");
        return false;
      }
      if ((paymentMethod.chequeNo == null)) {
        Common().showMessage(context: context, message: "Please enter Cheque No");
        return false;
      }
      if (paymentMethod.amount == null) {
        Common().showMessage(context: context, message: "Please enter Amount");
        return false;
      }
      return true;
    }
    if (paymentMethod.amount == null) {
      Common().showMessage(context: context, message: "Please enter Amount");
      return false;
    }
    return true;
  }

  void setReqDetailJson() {
    if (_editMode == EditMode.Off) {
      CmnPaymentMethods.shared.cmnPaymentMethodList.add(
          CmnPaymentMethod.fromJson(paymentMethod.toJson()));
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Payment Details Saved!'), duration: Duration(milliseconds: 500)));
    }else{
      /// Check whether already item is there
      var indexOfItemInExistingList = CmnPaymentMethods.shared.cmnPaymentMethodList
          .indexWhere((item) => item.paymentTypeValue == paymentMethod.paymentTypeValue);

      if (indexOfItemInExistingList != -1) {
        CmnPaymentMethods.shared.cmnPaymentMethodList.replaceRange(
            indexOfItemInExistingList,
            indexOfItemInExistingList + 1,
            [CmnPaymentMethod.fromJson(paymentMethod.toJson())]);
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Payment Details Updated!'), duration: Duration(milliseconds: 500)));
      }
    }
    _clearFields();
  }

  _clearFields(){
    setState(() {
      paymentMethod.clear();
      _amountController.text = '';
    });
  }

  _getTotalPaymentInfoField(){
    return Column(
      children: [
        SizedBox(height: 12.0),
        Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text("Invoice", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                  Text(_getTotalInvoiceAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                ],
              ),
              Column(
                children: [
                  Text("Paid", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                  Text(_getTotalPaidAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                ],
              ),
              Column(
                children: [
                  Text("Balance", style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.bold)),
                  Text(_getBalanceAmount(), style: TextStyle(color: BCAppTheme().primaryColor))
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  _getTotalInvoiceAmount(){
    double totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + double.parse(item.netCost ?? 0.0));
    double totalAddChargeAmount = CommonAddChargesInfo.shared.cmnAddChargesList.fold(0, (prevVal, item) => prevVal + (item.netAmount ?? 0.0));
    return (totalAmount + totalAddChargeAmount).trim().toString();
  }

  _getTotalPaidAmount(){
    double totalPaidAmount = CmnPaymentMethods.shared.cmnPaymentMethodList.fold(0, (prevVal, item) => prevVal + item.amount ?? 0.0);
    return totalPaidAmount.trim().toString() ?? '0.0';
  }

  _getBalanceAmount(){
    double totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + double.parse(item.netCost ?? 0.0));
    double totalPaidAmount = CmnPaymentMethods.shared.cmnPaymentMethodList.fold(0, (prevVal, item) => prevVal + item.amount ?? 0.0);
    double totalAddChargeAmount = CommonAddChargesInfo.shared.cmnAddChargesList.fold(0, (prevVal, item) => prevVal + (item.netAmount ?? 0.0));
    return ((totalAmount + totalAddChargeAmount) - (totalPaidAmount ?? 0.0) ).trim().toString();
  }

  _getPaymentMethodList(){
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: CmnPaymentMethods.shared.cmnPaymentMethodList.length,
      itemBuilder: (context, index) {
        return MethodItemRow(
          index: index,
          didTapEdit: () {
            setState(() {
              _loadData(index);
            });
          },
          didTapDelete: () {
            setState(() {
              setState(() {
                CmnPaymentMethods.shared.cmnPaymentMethodList.removeAt(index);
              });
            });
          },
        );
      },
    );
  }

  _loadData(int index){
    paymentMethod = CmnPaymentMethod.fromJson(CmnPaymentMethods.shared.cmnPaymentMethodList[index].toJson());
    _amountController.text = paymentMethod.amount.toString();
    _editMode = EditMode.On;
  }

  showChequeDetailsDialogue() {
    ChequeInfo chequeInfo = ChequeInfo();
    if(paymentMethod.paymentSubTypeValueName == "Cheque"){
      chequeInfo.fromJson(paymentMethod?.toJson());
    }

    _validateChequeDetails(){
      // if (chequeInfo.chequeDate == null || chequeInfo.chequeName == null || chequeInfo.chequeNo == null) {
      //   return false;
      // }
        if (chequeInfo.chequeDate == null){
        Common().showMessage(context: context,message: "Please enter Cheque date");
        return false;
      }
      if (chequeInfo.chequeName == null){
        Common().showMessage(context: context,message: "Please enter Cheque name");
        return false;
      }
      if (chequeInfo.chequeNo == null){
        Common().showMessage(context: context,message: "Please enter Cheque no");
        return false;
      }
      return true;
    }

    Dialog chequeDetailsAlert = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Cheque Details", textAlign: TextAlign.center),
            SizedBox(height: 16.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 45,
                  child: DateFormField(
                    format: fromAddress.fromCompanyDateFormat,
                    showPicker: showItemDatePicker,
                    initialValue: chequeInfo.chequeDate,
                    onDateChanged: (DateTime date) {
                      setState(() {
                        chequeInfo.chequeDate = Utils.getDateByFormat(
                            format: fromAddress.fromCompanyDateFormat,
                            passedDateTime: date);
                        print("Date : $paymentMethod.chequeDate");
                      });
                    },
                    decoration: Common().getBCoreID(validityStatus: true,hintText: 'Cheque Date'),
                  ),
                ),
                SizedBox(height: 8),
                SizedBox(
                  height: 45,
                  child: TextField(
                    enableSuggestions: false,
                    textInputAction: TextInputAction.next,
                    controller: TextEditingController(text: chequeInfo?.chequeName?.toString()),
                    autocorrect: false,
                    decoration: Common().getBCoreID(validityStatus: true,hintText: 'Cheque Name'),
                    keyboardType: TextInputType.name,
                    onChanged: (value){
                      setState(() {
                        chequeInfo.chequeName = value;
                      });
                    },
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  height: 45,
                  child: TextField(
                    enableSuggestions: false,
                    textInputAction: TextInputAction.next,
                    controller: TextEditingController(text: chequeInfo?.chequeNo?.toString()),
                    autocorrect: false,
                    decoration: Common().getBCoreID(validityStatus: true,hintText: 'Cheque No'),
                    keyboardType: TextInputType.number,
                    onChanged: (value){
                      setState(() {
                        chequeInfo.chequeNo = value;
                      });
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 16.0),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 35,
                    decoration: Common().getBCoreSD(isMandatory: false, isValidated: false),
                    child: FlatButton(
                        child: Text("Cancel",
                            style:
                            TextStyle(color: Colors.black, fontSize: 11)),
                        onPressed: () {
                          Navigator.pop(context);
                          chequeInfo.clear();
                        }),
                  ),
                ),
                SizedBox(width: 16.0),
                Expanded(
                  child: Container(
                    height: 35,
                    child: FlatButton(
                        color: BCAppTheme().primaryColor,
                        child: Text("Save",
                            style: TextStyle(
                                color: BCAppTheme().secondaryColor,
                                fontSize: 11)),
                        onPressed: () {
                          if (_validateChequeDetails()) {
                            paymentMethod.fromJson(chequeInfo.toJson());
                            Navigator.pop(context);
                          }
                        }),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return chequeDetailsAlert;
      },
    );
  }

  Future<DateTime> showItemDatePicker() async {
    DateTime date = await showDatePicker(
      context: context,
      helpText: "Select Cheque Date",
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 36500)),
    );
    return date;
  }

  // int getDeci() {
  //   if (widget.type == TransactionType.GRN) {
  //     return int.parse(fromAddress.curDecimPlace);
  //   } else {
  //     return 2;
  //   }
  // }
}

class MethodItemRow extends StatelessWidget {
  final int index;
  final VoidCallback didTapEdit;
  final VoidCallback didTapDelete;

  const MethodItemRow(
      {Key key, this.index, this.didTapEdit, this.didTapDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Card(
        child:
        ListTile(
          tileColor: Colors.white,
          contentPadding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
          onTap: () {},
          title: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Payment Type",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 12.0,
                            color: BCAppTheme().textColor,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                    Text(CmnPaymentMethods.shared.cmnPaymentMethodList[index].paymentTypeValueName ?? "",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                  ],
                ),
              ),
              SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text("Sub Type",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 12.0,
                            color: BCAppTheme().textColor,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                    // CmnPaymentMethods.shared.cmnPaymentMethodList
                    Text(CmnPaymentMethods.shared.cmnPaymentMethodList[index].paymentSubTypeValueName ?? "",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                  ],
                ),
              )
            ],
          ),
          subtitle: Column(
            children: [
              SizedBox(height: 8),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text("Total Amount: ${CmnPaymentMethods.shared.cmnPaymentMethodList[index].amount ?? ""}",
                        textAlign: TextAlign.left,
                        maxLines: 1,
                        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
                        overflow: TextOverflow.ellipsis,
                        softWrap: true),
                  )
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(flex: 4, child: Container()),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 25,
                      child: FlatButton(
                          child: Text("DELETE",
                              style: TextStyle(
                                  color: BCAppTheme().redColor, fontSize: 11)),
                          onPressed: () {
//                          Navigator.pop(context);
                            didTapDelete();
                          }),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 25,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color(Utils.hexToColor("#A88D2A"))
                                .withOpacity(0.1),
                            spreadRadius: 0,
                            blurRadius: 10,
                            offset: Offset(-1, 15), // changes position of shadow
                          ),
                        ],
                      ),
                      child: RaisedButton(
                          padding: EdgeInsets.only(
                              right: 2, left: 2, top: 2, bottom: 2),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(50.0))),
                          onPressed: () {
                            didTapEdit();
                          },
                          textColor: Colors.white,
                          color: BCAppTheme().headingTextColor,
                          child: Container(
                            width: double.maxFinite,
                            child: Stack(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    child: AutoSizeText(
                                      'EDIT',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 11),
                                      maxLines: 1,
                                      minFontSize: 8.0,
                                      stepGranularity: 1.0,
                                    ),
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                        "assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")),
                              ],
                            ),
                          )),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      );
  }
//
//   getBarcodeText(){
//     String itemBarcode = "Credit Card";
//
// //    switch (type){
// //      case TransactionType.GRN:
// //      case TransactionType.PR:
// //        itemBarcode =  CommonHighItemInfo.shared.cmnHighItemList[index].itemBarcode ?? "";
// //        break;
// //      case TransactionType.MR:
// //      case TransactionType.SI:
// //      case TransactionType.SR:
// //      case TransactionType.MTI:
// //      case TransactionType.MTO:
// //      case TransactionType.II:
// //        itemBarcode = CommonItemViewModel.shared.cmnItemList[index].ItemBarcode?? "";
// //        break;
// //    }
//     return itemBarcode;
//   }
//
//   getItemName(){
//     String itemName = "Visa/ Master";
//
// //    switch (type){
// //      case TransactionType.GRN:
// //      case TransactionType.PR:
// //        itemName =  CommonHighItemInfo.shared.cmnHighItemList[index].itemName ?? "";
// //        break;
// //      case TransactionType.MR:
// //      case TransactionType.SI:
// //      case TransactionType.SR:
// //      case TransactionType.MTI:
// //      case TransactionType.MTO:
// //      case TransactionType.II:
// //        itemName = CommonItemViewModel.shared.cmnItemList[index].ItemName?? "";
// //        break;
// //    }
//     return itemName;
//   }
//
//   getPackageName(){
// //    String pkgName = CommonHighItemInfo.shared.cmnHighItemList[index].pkgName ?? "";
//     String pkgName = "Carton";
//     return pkgName;
//   }
//
//   getUnitPrice(){
//     String itemUnitPrice = "100.0";
//
// //    switch (type){
// //      case TransactionType.GRN:
// //      case TransactionType.PR:
// //        itemUnitPrice =  "Unit Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].unitPrice}" ?? "";
// //        break;
// //      case TransactionType.MR:
// //      case TransactionType.SI:
// //      case TransactionType.SR:
// //      case TransactionType.MTI:
// //      case TransactionType.MTO:
// //      case TransactionType.II:
// //        itemUnitPrice =  "Unit Cost: ${CommonItemViewModel.shared.cmnItemList[index].UnitCost}"??"";
// //        break;
// //    }
//     return itemUnitPrice;
//   }
//
//   _getTotalCost() {
//     String totalCostText = " Amount : 100.0";
//
// //    switch (type) {
// //      case TransactionType.GRN:
// //        if (GrnReq.shared.itemDetails[index].isFoc) {
// //          totalCostText = "FOC";
// //        }
// //        else {
// //          totalCostText =
// //          "Total Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].totalCost}";
// //        }
// //        break;
// //      case TransactionType.PR:
// //        totalCostText =
// //        "Total Cost: ${CommonHighItemInfo.shared.cmnHighItemList[index].totalCost}";
// //        break;
// //      case TransactionType.MR:
// //      case TransactionType.SI:
// //      case TransactionType.SR:
// //      case TransactionType.MTI:
// //      case TransactionType.MTO:
// //      case TransactionType.II:
// //        totalCostText = "Total Cost: ${CommonItemViewModel.shared.cmnItemList[index].TotalCost}";
// //        break;
// //    }
//     return Text(totalCostText,
//         textAlign: TextAlign.left,
//         maxLines: 1,
//         style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
//         overflow: TextOverflow.ellipsis,
//         softWrap: true);
//
//   }
}