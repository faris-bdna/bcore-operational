part of 'dashboard_bloc.dart';

abstract class DashboardEvent extends Equatable {
  const DashboardEvent();
}

class GetItemBarcodeMaster extends DashboardEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String lastFetchDate;
  final String recordCount;

  const GetItemBarcodeMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate,recordCount];

  @override
  String toString() => 'GetItemBarcodeMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'lastFetchDate: $lastFetchDate,'
      'recordCount: $recordCount}';
}

class GetItemPackingMaster extends DashboardEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String lastFetchDate;
  final String recordCount;

  const GetItemPackingMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, recordCount,lastFetchDate];

  @override
  String toString() => 'GetItemPackingMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'lastFetchDate: $lastFetchDate,'
      'recordCount: $recordCount}';
}

class GetItemBatchMaster extends DashboardEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String lastFetchDate;
  final String recordCount;

  const GetItemBatchMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId,recordCount, lastFetchDate];

  @override
  String toString() => 'GetItemBatchMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'lastFetchDate: $lastFetchDate,'
      'recordCount: $recordCount}';
}

class GetCommonMaster extends DashboardEvent {
  final ScmMasters scmMaster;
  const GetCommonMaster({@required this.scmMaster});

  @override
  List<Object> get props => [scmMaster];

  @override
  String toString() => 'GetCommonMaster { ''scmMaster: $scmMaster}';
}


class GetDraftTransaction extends DashboardEvent {
  final String draftTransId;
  final String draftTransType;
  const GetDraftTransaction({@required this.draftTransId, this.draftTransType});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetDraftTransaction {"DraftID" : "$draftTransId","DraftType" : "$draftTransType"}';
}

// class GetCountryOfOriginMaster extends DashboardEvent {
//   final ScmMasters scmMaster;
//   const GetCountryOfOriginMaster({@required this.scmMaster});
//
//   @override
//   List<Object> get props => [scmMaster];
//
//   @override
//   String toString() => 'GetCountryOfOriginMaster { ''scmMaster: $scmMaster}';
// }
//
// class GetBrandMaster extends DashboardEvent {
//   final ScmMasters scmMaster;
//   const GetCountryOfOriginMaster({@required this.scmMaster});
//
//   @override
//   List<Object> get props => [scmMaster];
//
//   @override
//   String toString() => 'GetCountryOfOriginMaster { ''scmMaster: $scmMaster}';
// }
