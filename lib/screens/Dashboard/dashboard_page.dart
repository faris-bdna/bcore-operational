import 'dart:convert';

import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/network_util/bloc.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_bloc.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/MyConfiguration.dart';
import 'package:bcore_inventory_management/screens/transactions/DN/dn_page.dart';
import 'package:bcore_inventory_management/screens/transactions/GRN/grn_page.dart';
import 'package:bcore_inventory_management/screens/transactions/II/ii_page.dart';
import 'package:bcore_inventory_management/screens/transactions/MTI/mti_page.dart';
import 'package:bcore_inventory_management/screens/transactions/MTO/mto_page.dart';
import 'package:bcore_inventory_management/screens/transactions/OB/ob_page.dart';
import 'package:bcore_inventory_management/screens/transactions/PR/pr_page.dart';
import 'package:bcore_inventory_management/screens/transactions/PrintPreview/printPreview.dart';
import 'package:bcore_inventory_management/screens/transactions/SAI/sai_page.dart';
import 'package:bcore_inventory_management/screens/transactions/SI/si_page.dart';
import 'package:bcore_inventory_management/screens/transactions/SO/so_page.dart';
import 'package:bcore_inventory_management/screens/transactions/SP/shelf_printing_page.dart';
import 'package:bcore_inventory_management/screens/transactions/SR/sr_page.dart';
import 'package:bcore_inventory_management/screens/transactions/ST/st_page.dart';
import 'package:bcore_inventory_management/screens/transactions/OF/of_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_version/new_version.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../transactions/MR/mr_page.dart';

class Dashboard extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const Dashboard({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _DashboardState createState() =>
      _DashboardState(repository, appLanguage, database);
}

class _DashboardState extends State<Dashboard> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  String lastFetchDate;
  ScmMasters masterToFetch;
  bool masterSaved;
  GetItemBarcodeMasterResp getItemBarcodeMasterResp =
      new GetItemBarcodeMasterResp();
  GetItemPackingMasterResp getItemPackingMasterResp =
      new GetItemPackingMasterResp();
  GetItemBatchMasterResp getItemBatchMasterResp = new GetItemBatchMasterResp();
  GetCommonResp getCommonResp = new GetCommonResp();

  _DashboardState(this.repository, this.appLanguage, this.database);

  List<Item> itemList = [];

  DashboardBloc dashboardBlocProvider;
  String barRecordCount="0";
  String packRecordCount="0";
  String batchRecordCount="0";

  bool shouldValidateCompany,
      shouldValidateDivision,
      shouldValidateLocation,
      shouldValidateDeliveryLocation,
      shouldValidateProject,
      shouldValidateDepartment;

  resetValidations() {
    shouldValidateCompany = false;
    shouldValidateDivision = false;
    shouldValidateLocation = false;
    shouldValidateDeliveryLocation = false;
    shouldValidateProject = false;
    shouldValidateDepartment = false;
  }

  @override
  void initState() {
    super.initState();

    resetValidations();
    checkLastFetchDate();
    resetTransactionData();
    setStaticValuesForClientSettings();
    setStaticValuesForGetModuleSettings();

    getList();

    Common().hasInitialNavigationToDashboardCompleted().then((value) {
      /// checks user didn't complete initial dashboard or not
      if (value == false) {
        Common().setInitialNavigationToDashboardStatus(true);
        fetchData();
      }
      setState(() {});
    });
  }

  _fetchConfigDetailsFromDB() {
    dashboardBlocProvider.add(GetDraftTransaction(
        draftTransId: "CONFIG_101", draftTransType: "CONFIG"));
  }

  @override
  void didUpdateWidget(Widget oldWidget) {}

  getList() {
    List<String> itemNames = [
      "Material Request",
      "Goods Receiving Note",
      "Store Issue",
      "Store Receipt",
      "Material Transfer Out",
      "Material Transfer In",
      "Internal Issue",
      "Purchase Return",
      "Stock Taking",
      "Opening Balance",
      "Sales Order",
      "Sales Invoice",
      "Delivery Note",
      "Order Form",
      "Shelf Printing",
      "Label Printing"
    ];
    for (int i = 0; i < itemNames.length; i++) {
      itemList.add(Item(
          itemNames[i],
          "https://businesscore.ae/media/1001/configure.png",
          TransactionType.values[i].value));
    }
  }

  setStaticValuesForClientSettings() {
    Common().setDepartmentRequired(true);
    Common().setDivisionRequired(true);
  }

  setStaticValuesForGetModuleSettings() {
    Common().setBackDateAllowed(true);
  }

  resetTransactionData() {
    toAddress.clear();
    CommonItemViewModel.shared.cmnItemList.clear();
    all_items_List.clear();
  }

  checkLastFetchDate() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.containsKey("lastFetchDate")) {
      if (pref.getString("lastFetchDate") != null) {
        lastFetchDate = Utils.getDateByFormat(
            format: fromAddress.fromCompanyDateFormat,
            passedDateTime: DateTime.parse(pref.getString("lastFetchDate")));
      } else {
        lastFetchDate = '';
      }
    } else {
      lastFetchDate = '';
    }
  }

  storeLastFetchDate(String fetchDate) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("lastFetchDate", fetchDate);
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    GlobalKey<ScaffoldState> scaffKey = new GlobalKey<ScaffoldState>();
    ScmMasters loadedMaster;

    BlocProvider(
      create: (context) => NetworkBloc()..add(ListenConnection()),
    );
    BlocBuilder<NetworkBloc, NetworkState>(
      builder: (context, state) {
        if (state is ConnectionFailure) return Text("No Internet Connection");
        if (state is ConnectionSuccess)
          return Text("You're Connected to Internet");
        else
          return Text("");
      },
    );

    _showAlertAndNavigateToConfig(String message) {
      resetValidations();
      Common().showAlertMessage(
          context: context,
          title: Constants.alertMsgTitle,
          message: message,
          okFunction: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => FromAddressForm(
                      repository: repository,
                      appLanguage: appLanguage,
                      database: database,
                    )));
          });
    }

    validate(TransactionType type) {
      String content, message;
      if (shouldValidateCompany && FromAddress.shared.fromCompanyId == null) {
        content = "Company";
      }
      if (shouldValidateDivision && FromAddress.shared.fromDivisionId == null) {
        content = (content != null) ? content + " & Division" : "Division";
      }
      if (shouldValidateLocation && FromAddress.shared.fromLocationId == null) {
        content = (content != null) ? content + " & Location" : "Location";
      }
      if (shouldValidateDeliveryLocation &&
          FromAddress.shared.fromDeliveryLocationId == null) {
        content = (content != null)
            ? content + " & Delivery Location"
            : "Delivery Location";
      }
      if (shouldValidateProject && FromAddress.shared.fromProjectId == null) {
        content = (content != null) ? content + " & Project" : "Project";
      }
      if (shouldValidateDepartment &&
          FromAddress.shared.fromDepartmentId == null) {
        content = (content != null) ? content + " & Department" : "Department";
      }

      if (content != null) {
        message = "Please select " + content + " for " + type.value;
        _showAlertAndNavigateToConfig(message);
        return false;
      } else {
        return true;
      }
    }

    bool _hasMandatoryInfo(TransactionType type) {
      switch (type) {
        case TransactionType.GRN:
        case TransactionType.SI:
        case TransactionType.MTO:
        case TransactionType.SO:
        case TransactionType.SAI:
        case TransactionType.DN:
        case TransactionType.SR:
        case TransactionType.OB:
        case TransactionType.ST:
        case TransactionType.MTI:
        case TransactionType.SP:
        case TransactionType.PR:
          shouldValidateCompany = true;
          shouldValidateDivision = true;
          shouldValidateLocation = true;
          return validate(type);
          break;
        case TransactionType.MR:
        case TransactionType.OF:
          shouldValidateCompany = true;
          shouldValidateDivision = true;
          shouldValidateLocation = true;
          shouldValidateDeliveryLocation = true;
          return validate(type);
          break;
        case TransactionType.II:
          shouldValidateCompany = true;
          shouldValidateDivision = true;
          shouldValidateLocation = true;
          // shouldValidateProject = true;
          shouldValidateDepartment = true;
          return validate(type);
          break;
      }
    }

    navigateToPage(TransactionType type) {
      switch (type) {
        case TransactionType.MR:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MRPage(
                        repository: repository,
                        appLanguage: appLanguage,
                        database: database)));
          }
          break;
        case TransactionType.GRN:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => GRNPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.SI:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SIPage(
                        repository: repository,
                        appLanguage: appLanguage,
                        database: database)));
          }
          break;
        case TransactionType.SR:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SRPage(
                          repository: repository,
                          appLanguage: appLanguage,
                          database: database,
                        )));
          }
          break;
        case TransactionType.MTO:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MTOPage(
                        repository: repository,
                        appLanguage: appLanguage,
                        database: database)));
          }
          break;
        case TransactionType.MTI:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MTIPage(
                        repository: repository,
                        appLanguage: appLanguage,
                        database: database)));
          }
          break;
        case TransactionType.II:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => IIPage(
                          repository: repository,
                          appLanguage: appLanguage,
                          database: database,
                        )));
          }
          break;
        case TransactionType.PR:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PRPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.ST:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => STPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.SO:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SOPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.SAI:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SAIPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.OB:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OBPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.DN:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DNPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.OF:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OFPage(
                        repository: repository,
                        database: database,
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.SP:
          if (_hasMandatoryInfo(type)) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ShelfPrintingPage(
                        repository: repository,
                        database: Database(),
                        appLanguage: appLanguage)));
          }
          break;
        case TransactionType.PP:
          // if (_hasMandatoryInfo(type)) {
          //   Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //           builder: (context) => OFPage(
          //               repository: repository,
          //               database: Database(),
          //               appLanguage: appLanguage)));
          // }
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PrintPreview(
                      repository: repository,
                      database: Database(),
                      appLanguage: appLanguage)));
          break;
      }
    }

    return BlocProvider<DashboardBloc>(
      create: (context) =>
          DashboardBloc(bcRepository: repository, database: database),
      child: BlocListener<DashboardBloc, DashboardState>(
        listener: (context, state) async {
          SharedPreferences pref = await SharedPreferences.getInstance();
          if (state is GetItemBarcodeMasterInProgress ||
              state is GetItemPackingMasterInProgress ||
              state is GetItemBatchMasterInProgress ||
              state is GetCommonMasterInProgress ||
              state is GetDraftTransactionInProgress) {
            Common().showDownloadStatus(context, "Master Data Downloading...");
          }

          if (state is GetItemBarcodeMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getItemBarcodeMasterResp = state.getItemBarcodeResp;
            await saveItemBarcodeMaster(getItemBarcodeMasterResp, db);
            Navigator.pop(context);
            pref.setString(Constants.prfIsIBDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIBDSync, true);

            if (state.getItemBarcodeResp.masterDataList.length % 10000 == 0) {
              barRecordCount = (int.parse(barRecordCount)
                  + getItemBarcodeMasterResp.masterDataList.length).toString() ;
              fetchData();

            }else {
              BlocProvider.of<DashboardBloc>(context).add(GetItemPackingMaster(
                  companyId: FromAddress.shared.fromCompanyId,
                  divisionId: FromAddress.shared.fromDivisionId,
                  locationId: FromAddress.shared.fromLocationId,
                  supplierId: '',
                  recordCount: packRecordCount,
                  lastFetchDate: ''
              ));
            }
          }

          if (state is GetItemPackingMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getItemPackingMasterResp = state.getItemPackingResp;
            await savePackingMaster(getItemPackingMasterResp, db);
            Navigator.pop(context);
            pref.setString(Constants.prfIsIPDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIPDSync, true);

            if (state.getItemPackingResp.masterDataList.length % 10000 == 0) {
              packRecordCount = (int.parse(packRecordCount)
                  + getItemPackingMasterResp.masterDataList.length).toString() ;
              BlocProvider.of<DashboardBloc>(context).add(GetItemPackingMaster(
                  companyId: FromAddress.shared.fromCompanyId,
                  divisionId: FromAddress.shared.fromDivisionId,
                  locationId: FromAddress.shared.fromLocationId,
                  supplierId: '',
                  recordCount: packRecordCount,
                  lastFetchDate: '' ));

            }else {
              BlocProvider.of<DashboardBloc>(context).add(GetItemBatchMaster(
                  companyId: FromAddress.shared.fromCompanyId,
                  divisionId: FromAddress.shared.fromDivisionId,
                  locationId: FromAddress.shared.fromLocationId,
                  supplierId: '0',
                  recordCount: batchRecordCount,
                  lastFetchDate: '""'
              ));
            }
          }

          if (state is GetItemBatchMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getItemBatchMasterResp = state.getItemBatchResp;
            await saveBatchMaster(getItemBatchMasterResp, db);
            Navigator.pop(context);
            // Store date after saving main masters(Item Barcode, Item Packing and Item Batch)
            storeLastFetchDate(DateTime.now().toString());
            pref.setString(Constants.prfIsIBHDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsIBHDSync, true);
            // Set next master to fetch as country of origin master
            setMasterToFetch(ScmMasters.CountryOfOrigin);

            if ((state.getItemBatchResp.masterDataList.length % 10000 == 0) && (batchRecordCount != "0"))
            {
              batchRecordCount = (int.parse(batchRecordCount)
                  + getItemBatchMasterResp.masterDataList.length).toString() ;
              BlocProvider.of<DashboardBloc>(context).add(GetItemBatchMaster(
                  companyId: FromAddress.shared.fromCompanyId,
                  divisionId: FromAddress.shared.fromDivisionId,
                  locationId: FromAddress.shared.fromLocationId,
                  supplierId: '0',
                  recordCount: batchRecordCount,
                  lastFetchDate: '""'));

            }
            else {
              BlocProvider.of<DashboardBloc>(context)
                  .add(GetCommonMaster(scmMaster: masterToFetch));
            }
          }

          // Country Of Origin master loaded state
          if (state is GetCountryOfOriginMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getCommonResp = state.getCommonResp;
            loadedMaster = state.scmMaster;
            await saveCountryOfOriginMaster(getCommonResp, db);
            pref.setString(Constants.prfIsCOODDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsCOODSync, true);
            Navigator.pop(context);
            if (loadedMaster == ScmMasters.CountryOfOrigin) {
              setMasterToFetch(ScmMasters.ProductBrand);
              BlocProvider.of<DashboardBloc>(context)
                  .add(GetCommonMaster(scmMaster: masterToFetch));
            }
          }

          // Product brand master loaded state
          if (state is GetProductBrandMasterLoaded) {
            var db = Provider.of<Database>(context, listen: false);
            getCommonResp = state.getCommonResp;
            loadedMaster = state.scmMaster;
            await saveProductBrandMaster(getCommonResp, db);
            pref.setString(Constants.prfIsBRDDDate, DateTime.now().toString());
            pref.setBool(Constants.prfIsBRDDSync, true);
            Navigator.pop(context);
            // saveCommonMaster(getCommonResp, db, masterToFetch);
            // Save common master to database
            // Set next master to fetch as country of origin master
            if (loadedMaster == ScmMasters.ProductBrand) {
              Common().showAlertMessage(
                  context: context,
                  title: 'Success',
                  message: 'Data download success.',
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
          }

          if (state is GetItemBarcodeMasterFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: 'Failed',
                message: 'Barcode Data download failed.',
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          if (state is GetItemPackingMasterFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: 'Failed',
                message: 'Packing Data download failed.',
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          if (state is GetItemBatchMasterFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: 'Failed',
                message: 'Batch Data download failed.',
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          if (state is GetCommonMasterFailure) {
            Navigator.pop(context);
            Common().showAlertMessage(
                context: context,
                title: 'Failed',
                message: 'Master data download failed.',
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          else if (state is GetDraftTransactionComplete) {
            Navigator.pop(context);

            fromAddress.fromJson(state.fromAddress.toJson());
            print(state.fromAddress.toJson());
          }

          if (state is GetDraftTransactionFailure) {
            Navigator.pop(context);
          }
        },
        child: BlocBuilder<DashboardBloc, DashboardState>(
            builder: (context, state) {
          if (dashboardBlocProvider == null) {
            dashboardBlocProvider = BlocProvider.of<DashboardBloc>(context);
          }

          if (state is GetItemBarcodeMasterInitial &&
              FromAddress.shared.fromCompanyId == null) {
            final newVersion = NewVersion(
              iOSId: 'com.bdna.bcore_inventory_management',
              androidId: 'com.bdna.bcore_inventory_management',
            );
            newVersion.showAlertIfNecessary(context: context);
            _fetchConfigDetailsFromDB();
          }

          return Scaffold(
            key: scaffKey,
            appBar: BCAppBar(
              title: 'DASHBOARD',
              scaffoldKey: scaffoldKey,
              isBack: true,
              appBar: AppBar(),
              database: database,
              appLanguage: appLanguage,
              repository: repository,
              shouldHideActions: false,
            ),
            body: Scaffold(
              key: scaffoldKey,
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 175,
                          childAspectRatio: 15 / 13,
                          crossAxisSpacing: 4,
                          mainAxisSpacing: 4),
                      itemCount: itemList.length,
                      itemBuilder: (BuildContext ctx, index) {
                        return InkWell(
                          onTap: () {
                            print("tapped");
                            navigateToPage(TransactionType.values[index]);
                          },
                          child: Card(
                            child: Container(
                              padding: EdgeInsets.all(6.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.network(itemList[index].image,
                                      width: 50, height: 50),
                                  SizedBox(height: 4.0),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          itemList[index].name.toUpperCase(),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 11.0,
                                              //width / 45.0,
                                              fontWeight: FontWeight.w600,
                                              height: 1.0),
                                          // overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setMasterToFetch(ScmMasters scmMaster) {
    masterToFetch = scmMaster;
  }

  fetchData() {
    dashboardBlocProvider.add(GetItemBarcodeMaster(
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        supplierId: '',
        lastFetchDate: '' /*lastFetchDate*/,
    recordCount: barRecordCount));
  }
}

saveItemBarcodeMaster(
    GetItemBarcodeMasterResp getItemBarcodeMasterResp, Database db) async {
  ItemBarcodeData barcodeData;

  await db.itemBarcodeDao.batch((b) {
    for (var item in getItemBarcodeMasterResp.masterDataList) {
      barcodeData = ItemBarcodeData(
          Text: item.text,
          ItemBarcodeId: item.value,
          ProductGroupId: item.productGroupId,
          ProductId: item.productId,
          ProductName: item.productName,
          ItemId: item.itemId,
          ItemCode: item.itemCode,
          ItemName: item.itemName,
          ItemPackingId: item.itemPackingId,
          MajorPackId: item.majorPackId,
          BasePackId: item.basePackId,
          PkgQty: item.pkgQty.toString(),
          PackingDesc: item.packingDesc,
          ScaleGroupId: item.scaleGroupId,
          BrandId: item.brandId,
          OriginCountryId: item.originCountryId,
          ProductStockTypeId: item.productStockTypeId,
          MinShelfLife: item.minShelfLife,
          TotalShelfLife: item.totalShelfLife,
          IsSalesAllowed: item.isSalesAllowed,
          StockAllocTypeId: item.stockAllocTypeId,
          IsTaxApplicable: item.isTaxApplicable,
          ItemStatus: item.itemStatus,
          pur_restriction: item.purchaseRestriction,
          IsPurchaseAllowed: item.isPurchaseAllowed,
          ExcessQty: item.excessQty.toString(),
          IsMinOrderQtyApp: item.isMinOrderQtyApp,
          MinOrderQty: item.minOrderQty.toString(),
          SuppItemCode: item.suppItemCode,
          SuppTypeId: 0, //item.suppTypeId,
          TaxPercent: item.taxPercent.toString(),
          IsBatchEnabled: item.isBatchEnabled,
          HasPurchaseGroupAccess: item.hasPurchaseGroupAccess,
          StockBasedItem: item.stockBasedItem,
          ConsignmentItem: item.consignmentItem,
          LastPurchaseQty: item.lastPurchaseQty.toString(),
          LastPurchaseDate: item.lastPurchaseDate,
          LastReceivedAmount: item.lastReceivedAmount.toString(),
          LastReceivedSupplier: item.lastReceivedSupplier,
          AverageCost: item.averageCost.toString(),
          AvailableQty: item.availableQty.toString(),
          NetCost: item.netCost.toString());
      b.insert(db.itemBarcode, barcodeData);
    }
  });
}

savePackingMaster(
    GetItemPackingMasterResp getItemPackingMasterResp, Database db) async {
  ItemPackingData packingData;

  await db.itemPackingDao.batch((b) {
    for (var item in getItemPackingMasterResp.masterDataList) {
      packingData = ItemPackingData(
          packing_text: item.text,
          packing_value: item.batch_value,
          item_id: item.itemId);
      // db.itemPackingDao.updateItemPacking(packingData);
      b.insert(db.itemPacking, packingData);
    }
  });
}

saveBatchMaster(
    GetItemBatchMasterResp getItemBatchMasterResp, Database db) async {
  ItemBatchData batchData;
  await db.itemBatchDao.batch((b) {
    for (var item in getItemBatchMasterResp.masterDataList) {
      batchData = ItemBatchData(
          batch_text: item.text,
          batch_value: item.value,
          item_id: item.itemId,
          prod_date: item.prodDate,
          expiry_date: item.expiryDate);
      b.insert(db.itemBatch, batchData);
    }
  });
}

saveProductBrandMaster(GetCommonResp getCommonResp, Database db) async {
  ProductBrandData productBrandData;
  await db.productBrandDao.batch((b) {
    for (var item in getCommonResp.masterDataList) {
      productBrandData =
          ProductBrandData(brand_name: item.text, brand_id: item.value);
      b.insert(db.productBrand, productBrandData);
    }
  });
}

saveCountryOfOriginMaster(GetCommonResp getCommonResp, Database db) async {
  CountryOriginData countryOriginData;
  await db.countryOriginDao.batch((b) {
    for (var item in getCommonResp.masterDataList) {
      countryOriginData =
          CountryOriginData(country_name: item.text, country_id: item.value);
      b.insert(db.countryOrigin, countryOriginData);
    }
  });
}

enum TransactionType {
  MR,
  GRN,
  SI,
  SR,
  MTO,
  MTI,
  II,
  PR,
  ST,
  OB,
  SO,
  SAI,
  DN,
  OF,
  SP,
  PP
}

extension TransactionTypeVal on TransactionType {
  String get value => this.toString().split('.').last;
}
