import 'dart:async';
import 'dart:convert';

import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  final BCRepository bcRepository;
  final Database database;

  DashboardBloc({@required this.database, this.bcRepository})
      : assert(bcRepository != null),
        super(GetItemBarcodeMasterInitial());

  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    if (event is GetItemBarcodeMaster) {
      yield GetItemBarcodeMasterInProgress();
      try {
        final GetItemBarcodeMasterResp getItemBarcodeMasterResp =
            await bcRepository.getItemBarcodeMaster(
                companyId: event.companyId,
                divisionId: event.divisionId,
                locationId: event.locationId,
                supplierId: event.supplierId,
                lastFetchDate: event.lastFetchDate,
            recordCount: event.recordCount);
        yield GetItemBarcodeMasterLoaded(
            getItemBarcodeResp: getItemBarcodeMasterResp);
      } catch (error) {
        yield GetItemBarcodeMasterFailure(error: error.toString());
      }
    }

    if (event is GetItemBatchMaster) {
      yield GetItemBatchMasterInProgress();
      try {
        final GetItemBatchMasterResp getItemBatchMasterResp =
            await bcRepository.getItemBatchMaster(
                companyId: event.companyId,
                divisionId: event.divisionId,
                locationId: event.locationId,
                supplierId: event.supplierId,
                recordCount: event.recordCount,
                lastFetchDate: event.lastFetchDate);
        yield GetItemBatchMasterLoaded(
            getItemBatchResp: getItemBatchMasterResp);
      } catch (error) {
        yield GetItemBatchMasterFailure(error: error.toString());
      }
    }

    if (event is GetItemPackingMaster) {
      yield GetItemPackingMasterInProgress();
      try {
        final GetItemPackingMasterResp getItemPackingMasterResp =
            await bcRepository.getItemPackingMaster(
                companyId: event.companyId,
                divisionId: event.divisionId,
                locationId: event.locationId,
                supplierId: event.supplierId,
                recordCount: event.recordCount,
                lastFetchDate: event.lastFetchDate);
        yield GetItemPackingMasterLoaded(
            getItemPackingResp: getItemPackingMasterResp);
      } catch (error) {
        yield GetItemPackingMasterFailure(error: error.toString());
      }
    }

    if (event is GetCommonMaster) {
      yield GetCommonMasterInProgress();
      try {
        final GetCommonResp getItemPackingMasterResp =
        await bcRepository.getCommonMaster(event.scmMaster);
        if(event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterLoaded(
              getCommonResp: getItemPackingMasterResp, scmMaster: event.scmMaster);
        } else if(event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterLoaded(
              getCommonResp: getItemPackingMasterResp, scmMaster: event.scmMaster);
        }
      } catch (error) {
        if(event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterFailure(error: error.toString());
        } else if(event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterFailure(error: error.toString());
        }
      }
    }

    if (event is GetDraftTransaction) {
      yield GetDraftTransactionInProgress();

      try {

        List<DraftTransactionData> draftTransactionData =
        await database.draftTransactionDao.getDraftTransactionById(event.draftTransId,event.draftTransType);

        FromAddress resp = FromAddress.fromJson(json.decode(draftTransactionData[0].trans_details));
        yield GetDraftTransactionComplete(
            fromAddress:resp);
      } catch (error) {
        yield GetDraftTransactionFailure(error: error.toString());
      }
    }
  }
}
