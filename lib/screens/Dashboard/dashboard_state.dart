part of 'dashboard_bloc.dart';

abstract class DashboardState extends Equatable {
  const DashboardState();

  @override
  List<Object> get props => [];
}

class GetItemBarcodeMasterInitial extends DashboardState {}

class GetItemBarcodeMasterInProgress extends DashboardState {}

class GetItemBarcodeMasterLoaded extends DashboardState {
  final GetItemBarcodeMasterResp getItemBarcodeResp;

  const GetItemBarcodeMasterLoaded({@required this.getItemBarcodeResp})
      : assert(getItemBarcodeResp != null);

  @override
  List<Object> get props => [getItemBarcodeResp];
}

class GetItemBarcodeMasterFailure extends DashboardState {
  final String error;

  const GetItemBarcodeMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemBarcodeMasterFailure { error: $error }';
}

class GetItemPackingMasterInitial extends DashboardState {}

class GetItemPackingMasterInProgress extends DashboardState {}

class GetItemPackingMasterLoaded extends DashboardState {
  final GetItemPackingMasterResp getItemPackingResp;

  const GetItemPackingMasterLoaded({@required this.getItemPackingResp})
      : assert(getItemPackingResp != null);

  @override
  List<Object> get props => [getItemPackingResp];
}

class GetItemPackingMasterFailure extends DashboardState {
  final String error;

  const GetItemPackingMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemPackingMasterFailure { error: $error }';
}

class GetItemBatchMasterInitial extends DashboardState {}

class GetItemBatchMasterInProgress extends DashboardState {}

class GetItemBatchMasterLoaded extends DashboardState {
  final GetItemBatchMasterResp getItemBatchResp;

  const GetItemBatchMasterLoaded({@required this.getItemBatchResp})
      : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class GetItemBatchMasterFailure extends DashboardState {
  final String error;

  const GetItemBatchMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemBatchMasterFailure { error: $error }';
}

// Get country of origin master
class GetCommonMasterInitial extends DashboardState {}

class GetCommonMasterInProgress extends DashboardState {}

class GetCommonMasterLoaded extends DashboardState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetCommonMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetCommonMasterFailure extends DashboardState {
  final String error;

  const GetCommonMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCommonMasterFailure { error: $error }';
}

// Get country of origin master
class GetCountryOfOriginMasterInitial extends DashboardState {}

class GetCountryOfOriginMasterInProgress extends DashboardState {}

class GetCountryOfOriginMasterLoaded extends DashboardState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetCountryOfOriginMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetCountryOfOriginMasterFailure extends DashboardState {
  final String error;

  const GetCountryOfOriginMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCountryOfOriginMasterFailure { error: $error }';
}

// Get product brand master
class GetProductBrandMasterInitial extends DashboardState {}

class GetProductBrandMasterInProgress extends DashboardState {}

class GetProductBrandMasterLoaded extends DashboardState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetProductBrandMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetProductBrandMasterFailure extends DashboardState {
  final String error;

  const GetProductBrandMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetProductBrandMasterFailure { error: $error }';
}


class GetDraftTransactionInitial extends DashboardState {}

class GetDraftTransactionInProgress extends DashboardState {}

class GetDraftTransactionComplete extends DashboardState {
  final FromAddress fromAddress;

  GetDraftTransactionComplete({@required this.fromAddress}) : assert(fromAddress != null);

  @override
  List<Object> get props => [fromAddress];
}

class GetDraftTransactionFailure extends DashboardState {
  final String error;

  const GetDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}