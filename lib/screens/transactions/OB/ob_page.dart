import 'dart:io';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicItemInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/BasicForm/basic_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/basicItem/basic_item_entry_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/basic_summary_form.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';


final GlobalKey<CoolStepperState> coolStepperOBState = GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldOBKey = new GlobalKey<ScaffoldState>();
class OBPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  OBPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _OBPageState createState() => _OBPageState(repository, appLanguage, database);
}

class _OBPageState extends State<OBPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  TransBloc _transBloc;
  DraftTransactionData draftTransactionData;

  bool needToUpdateStepper = false;
  CommonBasicItem itemForStepperUpdate;
  _OBPageState(
      this.repository,
      this.appLanguage,
      this.database,
      );

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData(){
    cmnBasicFormInfo.clear();
    CommonBasicItemInfo.shared.cmnBasicItemList.clear();
    SaveOBReq.shared.clear();
    Utils.resetUniqueId();
  }

  getStepWithItemEntryForm() {
    return  CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? BasicItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.OB,item: itemForStepperUpdate)
          : BasicItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.OB,),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonBasicItemInfo.shared.cmnBasicItemList.isNotEmpty) {
          SaveOBReq.shared.fromJson(CommonBasicItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.OB);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(scaffoldKey: scaffoldOBKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  @override
  Widget build(BuildContext cont) {

    final formKey = GlobalKey<FormState>();
    List<CoolStep> steps = [
      CoolStep(
        title: "",
        content:
        BasicForm(key:formKey,repository: repository, appLanguage: appLanguage,
            type: TransactionType.OB),
        validation: () {
            SaveOBReq.shared.fromJson(cmnBasicFormInfo.toJson());
            setDraftTransaction(TransactionType.OB);
            _transBloc.add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
        }
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if(toAddress.companyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${toAddress.companyName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.divisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${toAddress.divisionName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.locationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${toAddress.locationName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.departmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${toAddress.departmentName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: BasicSummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.OB,
            navigateBackWithValueIndex: (int indexOfEditingItem){
              if (CommonBasicItemInfo.shared.cmnBasicItemList[indexOfEditingItem] != null) {
                setState(() {
                  itemForStepperUpdate = CommonBasicItemInfo.shared.cmnBasicItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperOBState.currentState.onStepBack();
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    void _bindAddressToGRN() {
      SaveOBReq.shared.companyId = int.parse(fromAddress.fromCompanyId);
      SaveOBReq.shared.divisionId = int.parse(fromAddress.fromDivisionId);
      SaveOBReq.shared.locationId = int.parse(fromAddress.fromLocationId);
    }

    CoolStepper stepper = CoolStepper(
      key: coolStepperOBState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveOBReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldOBKey, message: Constants.noItemAddedMessage);
          return;
        }

        _bindAddressToGRN();
        _transBloc.add(SaveOB(obReq: SaveOBReq.shared));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle: TextStyle(color: BCAppTheme().primaryColor,fontSize: 15),

      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0,right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is SaveOBInProgress ||
              state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress)
          {
            Common().showLoader(context);
          }
          else if (state is SaveOBComplete)
          {
            Navigator.pop(context,true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();

                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));

                  Navigator.pop(context,true);
                });
          }

          else if (state is SaveOBFailure)
          {
            Navigator.pop(context,true);
            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          else if (state is DeleteDraftTransactionComplete)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
          }

          else if (state is SaveDraftTransactionComplete)
          {
            Navigator.pop(context,true);

            print("${state.savedTransactionId}");
            if (state.savedTransactionId != null) {
              Common().setTransactionId(state.savedTransactionId);
            }
          }

          else if (state is DeleteDraftTransactionFailure)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }

          else if (state is SaveDraftTransactionFailure ||
              state is UpdateDraftTransactionFailure){
            Navigator.pop(context,true);
          }

        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldOBKey,
              appBar: BCAppBar(scaffoldKey:scaffoldOBKey,title: 'Opening Balance', isBack: false,appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(TransactionType type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type.value + '_' + uuid.v1(),
        trans_type: type.value,
        trans_details: SaveOBReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async{
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SaveOBReq.shared.toJson().toString());
    }
  }
}

