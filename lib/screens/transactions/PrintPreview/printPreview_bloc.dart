import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getCompany_resp.dart';
import 'package:bcore_inventory_management/models/masters/supplier_resp.dart';
import 'package:bcore_inventory_management/models/response/grn_details_print.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'printPreview_event.dart';
part 'printPreview_state.dart';

class PrintPreviewBloc extends Bloc<PrintPreviewEvent, PrintPreviewState> {
  final BCRepository repository;

  PrintPreviewBloc({@required this.repository}) : assert(repository != null), super(GetGRNEntryNoInitial());

  @override
  Stream<PrintPreviewState> mapEventToState(PrintPreviewEvent event) async* {

    if (event is GetGRNEntryNo) {
      yield GetGRNEntryNoInProgress();

      try {
        final GRNDetails resp = await repository.getGRNEntryNoDetails(event.grnEntryNo);
        yield GetGRNEntryNoComplete(referencePOGrnResp: resp);
      } catch (error) {
        yield GetGRNEntryNoFailure(error: error.toString());
      }
    }

    else if (event is GetPRReferenceDetails) {
      yield GetPRReferenceDetailsInProgress();

      try {
        final GetPRReferenceDetailsResp resp = await repository.getPurchaseReturnReferenceDetails(refId: event.refId);
        yield GetPRReferenceDetailsComplete(getPRReferenceDetailsResp: resp);
      } catch (error) {
        yield GetPRReferenceDetailsFailure(error: error.toString());
      }
    }
  }
}