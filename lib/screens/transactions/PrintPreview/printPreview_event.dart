part of 'printPreview_bloc.dart';

abstract class PrintPreviewEvent extends Equatable {
  const PrintPreviewEvent();
}

class GetGRNEntryNo extends PrintPreviewEvent {
  final String grnEntryNo;

  const GetGRNEntryNo(this.grnEntryNo);

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetGRNEntryNo';
}

class GetPRReferenceDetails extends PrintPreviewEvent {
  final String refId;

  const GetPRReferenceDetails({this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetPRReferenceDetails {"ReferenceId" : "$refId"}';
}
