
import 'dart:io';
import 'dart:ui';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/response/grn_details_print.dart';
import 'package:bcore_inventory_management/models/response/highItem/GetPendingPurchaseOrderResp.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/transactions/PrintPreview/printPreview_bloc.dart';
import 'package:bcore_inventory_management/screens/transactions/SP/shelf_printing_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:bcore_inventory_management/models/response/highItem/getPRReferenceDetailsResp.dart'
as prRefDet;
import 'package:pdf/widgets.dart' as pw;
import 'package:wifi/wifi.dart';
import 'package:zsdk/zsdk.dart' as ZLPrinter;

class PrintPreview extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final String grnRefNo;

  const PrintPreview({Key key, this.repository, this.appLanguage, this.database, this.grnRefNo}) : super(key: key);

  @override
  _PrintPreviewState createState() => _PrintPreviewState(repository,appLanguage,database);
}

class _PrintPreviewState extends State<PrintPreview> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  bool shouldValidateReference = false;
  List<ItemDetails> poRefList = [];
  // List<prRefDet.ItemDetails> prRefItemList = [];

  ReferencePOGrn selectedReferenceItem;

  PrintPreviewBloc _ppBloc;

  var _searchController = TextEditingController();
  var _grnRefNoController = TextEditingController();

  String localIp = '';
  List<String> network_devices = [];
  String network_device;

  bool isDiscovering = false;
  int found = -1;
  TextEditingController portController = TextEditingController(text: '9100');
  TextEditingController ipTextController = TextEditingController();


  String message;
  String statusMessage;
  CheckingStatus checkingStatus = CheckingStatus.NONE;
  PrintStatus printStatus = PrintStatus.NONE;
  ZLPrinter.Orientation printerOrientation = ZLPrinter.Orientation.PORTRAIT;

  _PrintPreviewState(this.repository, this.appLanguage, this.database);

  @override
  Widget build(BuildContext context) {


    return BlocProvider(
        create: (context) => PrintPreviewBloc(repository: repository),
      child: BlocListener<PrintPreviewBloc, PrintPreviewState>(
        listener: (context,state) {
          if (state is GetGRNEntryNoInProgress ||
              state is GetPRReferenceDetailsInProgress){
            Common().showLoader(context);
          }
          else if (state is GetGRNEntryNoFailure ||
              state is GetPRReferenceDetailsFailure){
            Navigator.pop(context);
          }
          else if (state is GetGRNEntryNoComplete) {
            Navigator.pop(context);

            // Populate GRN EntryNo dropdown
            setState(() {
              poRefList = state.referencePOGrnResp.data.itemDetails;
              if(poRefList.isEmpty){
                showMessage(context: context,message:"Enter Proper GRN Number");
              }
            });
          }
          // else if (state is GetPRReferenceDetailsComplete) {
          //   Navigator.pop(context);
          //   if(state.getPRReferenceDetailsResp.data.itemDetails.isNotEmpty){
          //     prRefItemList = state.getPRReferenceDetailsResp.data.itemDetails;
          //     for (var item in prRefItemList){
          //       item.printQty = item.gRNQty;
          //     }
          //   }
          //
          // }
        },
        child:
        BlocBuilder<PrintPreviewBloc, PrintPreviewState>(builder: (context, state) {
          _ppBloc = BlocProvider.of<PrintPreviewBloc>(context);
          if(state is GetGRNEntryNoInitial){
            if(widget.grnRefNo != null){
              _grnRefNoController.text= widget.grnRefNo;
              _ppBloc.add(GetGRNEntryNo(widget.grnRefNo));
            }
          }

          return Scaffold(
            // key: scaffoldDNKey,
            appBar: BCAppBar(
                // scaffoldKey: scaffoldDNKey,
                title: 'Item Label Print',
                isBack: false,
                appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: true),
            drawer: NavigationDrawer(
              context: context,
              repository: repository,
              appLanguage: appLanguage,
              database: database,
            ),      body: SafeArea(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 8.0),
                      // _getReferences(),
                      SizedBox(
                        height: 55,
                        child: TextFormField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: _grnRefNoController,
                          autocorrect: false,
                          decoration: Common().getBCoreMandatoryID(
                              isMandatory: true,
                              isValidated: _grnRefNoController.text.trim().isNotEmpty,
                              hintText: 'Enter GRN Number',
                              icon: Icons.search,
                              iconPress: () async {
                                _ppBloc.add(GetGRNEntryNo(_grnRefNoController.text));
                              }),
                          keyboardType: TextInputType.name,

                          validator: (value) {
                            _ppBloc.add(GetGRNEntryNo(value));
                            return null;
                          },
                          onFieldSubmitted: (value) {
                            _ppBloc.add(GetGRNEntryNo(_grnRefNoController.text));
                          },
                        ),
                      ),

                      Expanded(
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: poRefList.length,
                            itemBuilder: (context, index) {
                              if (_searchController.text.isEmpty) {
                                return RefItemRow(
                                  item: poRefList[index],
                                );}
                              else if (poRefList[index]
                                  .itemName
                                  .toLowerCase()
                                  .contains(_searchController.text.toLowerCase())) {
                                return RefItemRow(
                                  item: poRefList[index],
                                );
                              }
                              else {
                                return Container();
                              }
                            }),
                      ),
                      _getNetworkPrinterField(),
                      SizedBox(height: 8.0,),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Print",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    Navigator.pop(context);

                                    setState(() async{
                                        for(int i =0; i < poRefList.length;i++ ){
                                          await Future.delayed(Duration(milliseconds: 500), () {
                                          genLblPDF(
                                              printerIp: network_device,
                                              ctx: context,
                                              copy: int.parse(poRefList[i].gRNQty.toString()),
                                              brand: poRefList[i].brandName,
                                              package: poRefList[i].pkgName,
                                              barcode: poRefList[i].itemBarcode,
                                              itemName: poRefList[i].itemName,
                                              itemCode: poRefList[i].itemCode,
                                              itemPrice:poRefList[i].unitPrice.toString(),
                                              page: PdfPageFormat.roll80);
                                          });
                                        }
                                    });
                                  }),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),

    );
  }
  // _getReferences() {
  //   return Column(
  //       children: [
  //         Container(
  //           padding: EdgeInsets.zero,
  //           decoration: Common().getBCoreSD(
  //               isMandatory: shouldValidateReference,
  //               isValidated: selectedReferenceItem != null),
  //           child: SearchableDropdown.single(
  //             items: (poRefList != null && poRefList.isNotEmpty)
  //                 ? poRefList.map((ReferencePOGrn item) {
  //               return DropdownMenuItem(
  //                   child: Text(item.text,
  //                       style: TextStyle(
  //                         fontSize: 14.0,
  //                         color: BCAppTheme().subTextColor,
  //                       )),
  //                   value: item.text);
  //             }).toList()
  //                 : [],
  //             value: selectedReferenceItem?.text,
  //             hint: 'Select GRN No' /*Constants.refPlaceHolderText*/,
  //             searchHint: 'Select GRN No' /*Constants.refPlaceHolderText*/,
  //             onChanged: (value) {
  //               if (value != null) {
  //                 setState(() {
  //                   print(selectedReferenceItem);
  //
  //                   selectedReferenceItem = poRefList
  //                       .where((element) => element.text == value)
  //                       .toList()[0];
  //                   _ppBloc.add(GetPRReferenceDetails(refId: selectedReferenceItem.value));
  //                 });
  //               }
  //             },
  //             displayClearIcon: false,
  //             underline: Container(),
  //             isExpanded: true,
  //           ),
  //         ),
  //         SizedBox(height: 16),
  //       ],
  //     );
  // }

  void genLblPDF(
      {String printerIp,
        BuildContext ctx,
        int copy,
        String brand,
        String package,
        String barcode,
        String itemName,
        String itemCode,
        String itemPrice,
        PdfPageFormat page}) async {
    final pdf = pw.Document();

    var data = await rootBundle.load("fonts/open-sans.ttf");
    final ttf = pw.Font.ttf(data);
    pdf.addPage(pw.Page(
        pageFormat: page,
        build: (pw.Context context) {
          return pw.Row(children: [

            pw.Container(
              width: 60,
              child: pw.Padding(
                padding: const pw.EdgeInsets.only(left: 4.0),
                child: pw.Column(
                  children: [
                    pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.start,
                      children: [
                        pw.Text(itemName ?? "Item name",
                            style: pw.TextStyle(font: ttf, fontSize: 10.0)),
                      ],
                    ),
                    pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.start,
                      children: [
                        pw.Text(brand ?? "Brand",
                            style: pw.TextStyle(font: ttf, fontSize: 9.0))
                      ],
                    ),
                    pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.start,
                      children: [
                        pw.Text(package ?? "Pkg name",
                            style: pw.TextStyle(font: ttf, fontSize: 10.0)),
                      ],
                    ),
                    pw.Row(
                      // mainAxisAlignment: pw.MainAxisAlignment.center,
                      // crossAxisAlignment: pw.CrossAxisAlignment.stretch,
                      children: [
                        pw.Column(
                          children: [
                            pw.BarcodeWidget(
                              barcode: pw.Barcode.code128(),
                              data: barcode,
                              width: 110,
                              height: 60,
                              drawText: false,
                            ),
                          ],
                        ),
                        pw.SizedBox(width: 2.0),
                        pw.Padding(
                          padding: const pw.EdgeInsets.only(left: 4.0),
                          child: pw.Column(
                            mainAxisSize: pw.MainAxisSize.min,
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                            children: [
                              //${itemDetail.expiryDate}
                              pw.Text("E:16/10/2021" ?? "E:",
                                  style:
                                  pw.TextStyle(font: ttf,
                                      fontSize: 9.0,
                                      lineSpacing: 0.0)),
                              pw.SizedBox(height: 12.0),
                              pw.Text("BN:",
                                  style:
                                  pw.TextStyle(font: ttf, fontSize: 9.0)),
                              pw.SizedBox(height: 12.0),
                              pw.Text("RF:$itemCode" ?? "RF:",
                                  style:
                                  pw.TextStyle(font: ttf, fontSize: 9.0)),
                            ],
                          ),
                        ),
                      ],
                    ),
                    pw.SizedBox(height: 2,),
                    pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.start,
                      children: [
                        pw.Padding(
                          padding: const pw.EdgeInsets.only(left: 8.0),
                          child: pw.Text(barcode ?? "-",
                              textAlign: pw.TextAlign.justify,
                              style: pw.TextStyle(font: ttf, fontSize: 12.0)),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ]);
          //   pw.Center(
          //   child: pw.Text(""),
          // ); // Center
        }));
// }

    final appDocDir =
    await getExternalStorageDirectory(); //getApplicationDocumentsDirectory();
    final appDocPath = appDocDir.absolute.path;
    final file = File(appDocPath + '/' + 'document.pdf');
    print('Save as file ${file.path} ...');
    await file.writeAsBytes(await pdf.save());
    for(int i = 0 ; i < copy;i++){
      await Future.delayed(Duration(milliseconds: 500), () {
        bdna_zdk_print(printerIp: printerIp, path: file.path);
      });
    }

  }

  void bdna_zdk_print({String printerIp, String path}) {
    final ZLPrinter.ZSDK zsdk = ZLPrinter.ZSDK();
    zsdk
        .printPdfFileOverTCPIP(
        filePath: path,
        address: printerIp,
        port: int.tryParse(''),
        printerConf: ZLPrinter.PrinterConf(
          cmWidth: double.tryParse('6'),
          cmHeight: double.tryParse('2.54'),
          dpi: double.tryParse('180'),
          orientation: printerOrientation,
        ))
        .then((value) {
      setState(() {
        printStatus = PrintStatus.SUCCESS;
        message = "$value";
      });
    }, onError: (error, stacktrace) {
      try {
        throw error;
      } on PlatformException catch (e) {
        ZLPrinter.PrinterResponse printerResponse;
        try {
          printerResponse = ZLPrinter.PrinterResponse.fromMap(e.details);
          message =
          "${printerResponse.message} ${printerResponse.errorCode} ${printerResponse.statusInfo.status} ${printerResponse.statusInfo.cause}";
        } catch (e) {
          print(e);
          message = "${e.toString()}";
        }
      } on MissingPluginException catch (e) {
        message = "${e.message}";
      } catch (e) {
        message = "${e.toString()}";
      }
      setState(() {
        printStatus = PrintStatus.ERROR;
      });
    });
  }


  _getNetworkPrinterField() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: true,
                      isValidated: true),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Network Printer"),
                      // value: "Printer",
                      items: network_devices != null
                          ? network_devices.map((String item) {
                        return DropdownMenuItem(
                            child: Text(item,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: item);
                      }).toList()
                          : [],
                      onChanged: (value) {
                        setState(() {
                          // _device= null;
                          network_device = value;
                        });
                      },
                      value: network_device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
              Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: IconButton(
                  icon: Icon(
                    Icons.search,
                    color: BCAppTheme().primaryColor,
                  ),
                  iconSize: 20,
                  onPressed: () {
                    showPrinterDialogue();
                  }),
            )
          ],
        )
      ],
    );
  }

  showPrinterDialogue() {
    Dialog printerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(child: StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Printer IP", textAlign: TextAlign.center),
                  SizedBox(height: 16.0),
                  Container(
                    height: 45,
                    child: TextField(
                      controller: ipTextController,
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        dropDownState(() {});
                      },
                      decoration: Common().getBCoreMandatoryID(
                          isMandatory: false,
                          isValidated: true,
                          hintText: 'Search',
                          icon: Icons.search),
                    ),
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 35,
                          decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 0.5,
                                  style: BorderStyle.solid,
                                  color: BCAppTheme().primaryColor),
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            ),
                          ),
                          child: FlatButton(
                              child: Text("Cancel",
                                  style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                        ),
                      ),
                      SizedBox(width: 16.0),
                      Expanded(
                        child: Container(
                          height: 35,
                          child: FlatButton(
                              color: BCAppTheme().primaryColor,
                              child: Text("Ok",
                                  style: TextStyle(
                                      color: BCAppTheme().secondaryColor,
                                      fontSize: 11)),
                              onPressed: () {
                                discover(context);
                                Navigator.pop(context);
                                // setState(() {
                                // });
                              }),
                        ),
                      )
                    ],
                  )
                ],
              );
            })),
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return printerDialog;
      },
    );
  }

  void discover(BuildContext ctx) async {
    setState(() {
      isDiscovering = true;
      network_devices.clear();
      found = -1;
    });

    String ip;
    try {
      ip = await Wifi.ip;
      print('local ip:\t$ip');
    } catch (e) {
      final snackBar = SnackBar(
          content: Text('WiFi is not connected', textAlign: TextAlign.center));
      Scaffold.of(ctx).showSnackBar(snackBar);
      return;
    }
    setState(() {
      localIp = ipTextController.text;
    });

    final String subnet = localIp.substring(0, ip.lastIndexOf('.'));
    int port = 9100;
    try {
      port = int.parse(portController.text);
    } catch (e) {
      portController.text = port.toString();
    }
    print('subnet:\t$subnet, port:\t$port');

    final stream = NetworkAnalyzer.discover2(subnet, port);

    stream.listen((NetworkAddress addr) {
      if (addr.exists) {
        print('Found device: ${addr.ip}');
        setState(() {
          network_devices.add(addr.ip);
          found = network_devices.length;
        });
      }
    })
      ..onDone(() {
        setState(() {
          isDiscovering = false;
          found = network_devices.length;
        });
      })
      ..onError((dynamic e) {
        final snackBar = SnackBar(
            content: Text('Unexpected exception', textAlign: TextAlign.center));
        Scaffold.of(ctx).showSnackBar(snackBar);
      });
  }

}

class RefItemRow extends StatelessWidget {
  final ItemDetails item;
  const RefItemRow(
      {Key key, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      child: ListTile(
        // tileColor: hasAdded ? BCAppTheme().greenColor : Colors.white,
        contentPadding: EdgeInsets.all(8.0),
        onTap: () {},
        title: Row(
            children: <Widget>[
              Expanded(
                child:
                Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            height: 25.0,
                            width: 25.0,
                            child: Checkbox(
                              value: true,
                              onChanged: (bool value) {
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4.0),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Item Barcode",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        color: BCAppTheme().textColor,
                                        fontWeight: FontWeight.bold),
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: true),
                                getBarcodeTextField(),
                              ],
                            ),
                          ),
                          SizedBox(width: 8.0),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text("Item Name",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        color: BCAppTheme().textColor,
                                        fontWeight: FontWeight.bold),
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: true),
                                getItemNameField(),
                              ],
                            ),
                          )
                        ],
                      ),
                    ]
                ),
              )
            ]
        ),
        subtitle: Column(
          children: [
            SizedBox(height: 8),
            Divider(height: 1.0),
            SizedBox(height: 8),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child:  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getPackageNameField(),
                      getQuantityField(),
                    ],
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      getPkgDesc(),
                      SizedBox(height: 8),
                      SizedBox(
                        height: 35,
                        width: 35,
                        child: TextField(
                          enableSuggestions: false,
                          textInputAction: TextInputAction.next,
                          controller: TextEditingController()..text = item.gRNQty.toString(),
                          autocorrect: false,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                                fontSize: 12,
                                height: 1
                            ),
                          decoration: InputDecoration(
                            isDense: true,
                            border: OutlineInputBorder(),
                          hintText: 'Qty',
                            hintStyle: TextStyle(color: BCAppTheme().grayColor, fontSize: 14),
                            filled: true,
                            fillColor: Colors.white70,
                            contentPadding: EdgeInsets.fromLTRB(4.0, 12.0, 4.0, 0.0),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              borderSide:
                              BorderSide(color: BCAppTheme().headingTextColor, width: 0.5)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              borderSide:
                              BorderSide(color: BCAppTheme().headingTextColor, width: 0.5),
                            ),
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: (value){
                            item.gRNQty = value ?? 1.0;
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }


  getBarcodeTextField() {
    String itemBarcode;
    itemBarcode = item.itemBarcode ?? "";
    return Text(itemBarcode,
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getItemNameField() {
    String itemName;

    itemName = item.itemName ?? "";
    return Text(itemName,
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.right,
        softWrap: true);
  }

  getPackageNameField() {
    String pkgName;
    pkgName = item.pkgName ?? "";

    return Text("Package: $pkgName",
        maxLines: 1,
        style: TextStyle(
            fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getQuantityField() {
    String itemQuantity;
    itemQuantity = "GRN Qty: ${item.gRNQty}";

    return Text(itemQuantity, maxLines: 1, style: TextStyle(
        fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }

  getPkgDesc() {
    String totalCostText;

    totalCostText =
    "Pkg Desc: ${item.packingDesc}";

    return Text(totalCostText,
        maxLines: 1,
        style: TextStyle(fontSize: 11.0, color: BCAppTheme().textColor),
        overflow: TextOverflow.ellipsis,
        softWrap: true);
  }
}
