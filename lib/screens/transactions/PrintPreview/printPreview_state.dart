part of 'printPreview_bloc.dart';

abstract class PrintPreviewState extends Equatable {
  const PrintPreviewState();

  @override
  List<Object> get props => [];
}

/// GetGRNEntryNo States

class GetGRNEntryNoInitial extends PrintPreviewState{}

class GetGRNEntryNoInProgress extends PrintPreviewState{}

class GetGRNEntryNoComplete extends PrintPreviewState{
  final GRNDetails referencePOGrnResp;

  GetGRNEntryNoComplete({@required this.referencePOGrnResp}) : assert(referencePOGrnResp != null);

  @override
  List<Object> get props => [referencePOGrnResp];
}

class GetGRNEntryNoFailure extends PrintPreviewState {
  final String error;

  const GetGRNEntryNoFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetGRNEntryNoFailure { error : $error }';
}


class GetPRReferenceDetailsInitial extends PrintPreviewState{}

class GetPRReferenceDetailsInProgress extends PrintPreviewState{}

class GetPRReferenceDetailsComplete extends PrintPreviewState{
  final GetPRReferenceDetailsResp getPRReferenceDetailsResp;

  GetPRReferenceDetailsComplete({@required this.getPRReferenceDetailsResp}) : assert(getPRReferenceDetailsResp != null);

  @override
  List<Object> get props => [getPRReferenceDetailsResp];
}

class GetPRReferenceDetailsFailure extends PrintPreviewState {
  final String error;

  const GetPRReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetPRReferenceDetailsFailure { error : $error }';
}

