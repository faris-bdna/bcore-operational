import 'dart:io';
import 'dart:typed_data';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/printUtil.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicItemInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/transactions/SP/shelf_printing_bloc.dart';
import 'package:bdna_esc_pos_printer/esc_pos_printer.dart';
import 'package:bdna_esc_pos_utils/esc_pos_utils.dart';
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:wifi/wifi.dart';
import 'package:bdna_esc_pos_printer/esc_pos_printer.dart' as enums;
import 'package:pdf/widgets.dart' as pw;
import 'package:zsdk/zsdk.dart' as ZLPrinter;
// import 'package:zebrautility/ZebraPrinter.dart';
// import 'package:zebrautility/zebrautility.dart';
// import 'package:zsdk/zsdk.dart';

final GlobalKey<CoolStepperState> coolStepperOBState =
    GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldOBKey = new GlobalKey<ScaffoldState>();

class ShelfPrintingPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  ShelfPrintingPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _ShelfPrintingPageState createState() =>
      _ShelfPrintingPageState(repository, appLanguage, database);
}

class _ShelfPrintingPageState extends State<ShelfPrintingPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  DraftTransactionData draftTransactionData;

  bool needToUpdateStepper = false;
  CommonBasicItem itemForStepperUpdate;
  String _devicesMsg;
  InputType _itemInputType = InputType.Barcode;
  CommonItem itemDetail;
  List<ItemPackingData> itemPackingDataList;
  List<ItemBatchData> itemBatchDataList;
  ShelfPrintingBloc _shelfPrintingBloc;
  bool _connected = false;

  String message;
  String statusMessage;
  CheckingStatus checkingStatus = CheckingStatus.NONE;
  PrintStatus printStatus = PrintStatus.NONE;
  ZLPrinter.Orientation printerOrientation = ZLPrinter.Orientation.PORTRAIT;

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();

  List<PrinterBluetooth> _devices = [];
  PrinterBluetooth _device;
  PrintUtil printUtil;

  String localIp = '';
  List<String> network_devices = [];
  String network_device;

  bool isDiscovering = false;
  int found = -1;
  TextEditingController portController = TextEditingController(text: '9100');
  TextEditingController ipTextController = TextEditingController();

  var _itemCodeController = TextEditingController();
  var _quantityController = TextEditingController();
  var _averageCostController = TextEditingController();
  var _batchNoController = TextEditingController();
  var _batchCostController = TextEditingController();
  var _netCostController = TextEditingController();

  var itemCodeFocusNode = FocusNode();
  var quantityFocusNode = FocusNode();
  var averageCostFocusNode = FocusNode();
  var netCostFocusNode = FocusNode();

  bool shouldValidateItemCode = false;
  bool shouldValidatePackage = false;
  bool shouldValidateQuantity = false;

  _ShelfPrintingPageState(
    this.repository,
    this.appLanguage,
    this.database,
  );

  @override
  void initState() {
    super.initState();
    initPlatformState();
    itemDetail = CommonItem();
    Utils.resetUniqueId();
    printUtil = PrintUtil();
  }

  void _didSelect(InputType type) {
    itemCodeFocusNode.requestFocus();

    setState(() {
      _itemInputType = type;
      _itemCodeController.text = '';
      _quantityController.text = '';
      _averageCostController.text = '';
      _batchCostController.text = '';
      _batchNoController.text = '';
      _netCostController.text = '';
      itemDetail = CommonItem();
    });
  }

  resetTransactionData() {
    cmnBasicFormInfo.clear();
    // CommonBasicItemInfo.shared.cmnBasicItemList.clear();
    SaveOBReq.shared.clear();
    Utils.resetUniqueId();
  }

  Future<void> initPlatformState() async {
    printerManager.scanResults.listen((devices) async {
      // print('UI: Devices found ${devices.length}');
      setState(() {
        _devices = devices;
      });
    });
  }

  @override
  Widget build(BuildContext cont) {
    void _bindAddressToGRN() {
      SaveOBReq.shared.companyId = int.parse(fromAddress.fromCompanyId);
      SaveOBReq.shared.divisionId = int.parse(fromAddress.fromDivisionId);
      SaveOBReq.shared.locationId = int.parse(fromAddress.fromLocationId);
    }

    return BlocProvider<ShelfPrintingBloc>(
      create: (context) =>
          ShelfPrintingBloc(bcRepository: repository, database: database),
      child: BlocListener<ShelfPrintingBloc, ShelfPrintingState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingByIdInProgress ||
              state is LoadPackingInProgress ||
              state is LoadCountryByIdInProgress ||
              state is LoadBrandByIdInProgress ||
              state is GetStockReferenceDetailsInProgress ||
              state is GetScmItemPriceDetailsInProgress ||
              state is GetReasonInProgress ||
              state is LoadBatchByIdInProgress ||
              state is GetWareHouseInProgress ||
              state is GetWareHouseZoneInProgress ||
              state is GetWareHouseRackInProgress ||
              state is GetWareHouseBinInProgress) {
            Common().showLoader(context);
          } else if (state is LoadItemFailure ||
              state is LoadPackingByIdFailure ||
              state is LoadPackingFailure ||
              state is LoadCountryByIdFailure ||
              state is LoadBrandByIdFailure ||
              state is GetStockReferenceDetailsFailure ||
              state is GetScmItemPriceDetailsFailure ||
              state is GetReasonFailure ||
              state is LoadBatchByIdFailure ||
              state is GetWareHouseFailure ||
              state is GetWareHouseZoneFailure ||
              state is GetWareHouseRackFailure ||
              state is GetWareHouseBinFailure) {
            Navigator.pop(context);
          }


          else if (state is LoadItemComplete) {
            Navigator.pop(context);
            print("LoadItemComplete");

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
              ItemBarcodeData itemData = state.loadItemResult[0];
              _setItemInfo(itemData);
              _shelfPrintingBloc.add(LoadBatchById(itemId:itemData.ItemId));

              if (_itemInputType == InputType.ItemCode) {
                _loadItemPacking(context);
              } else {
                /// Set item name in barcode text field
                _setItemName();

                _loadItemPackingById();
                _getItemPriceDetails();
                _loadCountry();
              }
            }


            else {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
            quantityFocusNode.requestFocus();
          }
          else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);
            itemBatchDataList = [];
            if (state.getItemBatchResp != null &&
                state.getItemBatchResp.length > 0) {
              setState(() {
                itemBatchDataList = state.getItemBatchResp;
//                selectedItemBatch = int.parse(itemBatchDataList[0].batch_text);
              });
            }
          }

          else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);

            /// Save material request header in database
            if (state.loadItemByPackingResult != null) {
              setState(() {
                itemDetail.ItemBarcode = state.loadItemByPackingResult[0].Text;

                /// Set item name in barcode text field
                _setItemName();
              });
            }
            _getItemPriceDetails();
          } else if (state is LoadPackingComplete) {
            Navigator.pop(context);

            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                itemDetail.ProdpkgId =
                    int.parse(itemPackingDataList[0].packing_value);
                itemDetail.PackageName = itemPackingDataList[0].packing_text;

                /// Set item name in barcode text field
                _setItemName();
                FocusScope.of(context).unfocus();
              });
            }
          } else if (state is GetScmItemPriceDetailsComplete) {
            Navigator.pop(context);

            if (state.getPackingDetailsResp != null &&
                state.getPackingDetailsResp.masterDataList.length > 0) {
              setState(() {
                List<GetScmItemPriceList> packingDetails =
                    state.getPackingDetailsResp.masterDataList;
                if (packingDetails.isNotEmpty) {
                  itemDetail.UnitCost =
                      state.getPackingDetailsResp.masterDataList[0].unitPrice ??
                          (itemDetail.UnitCost ?? 0.0);
                  itemDetail.AvailableQty = state.getPackingDetailsResp
                          .masterDataList[0].availableQty ??
                      0.0;
                }
              });
            }
          } else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.CountryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }

            _loadProductBrand();
          } else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.BrandName = productBrandList[0].brand_name.trim();
              });
            }
          } else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);

            if (state.getItemBatchResp != null &&
                state.getItemBatchResp.length > 0) {
              setState(() {
                itemBatchDataList = state.getItemBatchResp;
              });
            }
          } else if (state is LoadPackingByIdComplete) {
            Navigator.pop(context);
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;

                itemDetail.PackageName = itemPackingDataList
                    .where((packing) =>
                        packing.packing_value ==
                        itemDetail.ProdpkgId.toString())
                    .toList()[0]
                    .packing_text;
              });
            }
          }
        },
        child: BlocBuilder<ShelfPrintingBloc, ShelfPrintingState>(
            builder: (context, state) {
          _shelfPrintingBloc = BlocProvider.of<ShelfPrintingBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(
                  context: context,
                  title: "Alert",
                  message:
                      "Your transaction details will be cleared. Do you want to continue ?",
                  okButtonTitle: 'Ok',
                  okFunction: () {
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: () {
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldOBKey,
              appBar: BCAppBar(
                scaffoldKey: scaffoldOBKey,
                title: 'Shelf Label Printing',
                isBack: false,
                appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        _getBarcodeField(context),
                        if (_itemInputType == InputType.ItemCode)
                          _getPackageField(),
                        _getQuantityField(),
                        _getItemBarcodeSticker(),
                        _getBlueToothPrinterField(),
                        _getNetworkPrinterField(),
                        SizedBox(height: 16.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              height: 35,
                              child: FlatButton(
                                  color: BCAppTheme().primaryColor,
                                  child: Text("Print",
                                      style: TextStyle(
                                          color: BCAppTheme().secondaryColor,
                                          fontSize: 11)),
                                  onPressed: () {
                                    // if (network_device.isNotEmpty) {
                                      genLblPDF(
                                          printerIp: network_device,
                                          ctx: context,
                                          copy: int.parse(_quantityController.text),
                                          brand: itemDetail.BrandName,
                                          package: itemDetail.PackageName,
                                          barcode: itemDetail.ItemBarcode,
                                          itemName: itemDetail.ItemName,
                                          itemCode: itemDetail.ItemCode,
                                          itemPrice:
                                              itemDetail.UnitCost.toString(),
                                          page: PdfPageFormat.roll80);
                                    // }
                                  }),
                            ),

                          ],
                        ),
                        SizedBox(height: 5),
                        // if (containEditablePrice) _getEditablePriceField(),
                        // _getItemDetailsField(context)
                        // _getPreint(),
                        // _getNetoworkPrinter(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  _getBarcodeField(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.Barcode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Barcode", style: TextStyle(fontSize: 13.0))
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    child: Radio(
                        activeColor: BCAppTheme().headingTextColor,
                        value: InputType.ItemCode,
                        groupValue: _itemInputType,
                        onChanged: _didSelect),
                  ),
                  SizedBox(width: 8),
                  Text("Item Code", style: TextStyle(fontSize: 13.0))
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 50,
          child: TextFormField(
            enableSuggestions: false,
            inputFormatters: [
              FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s"))
            ],
            textInputAction: (_itemInputType == InputType.Barcode)
                ? TextInputAction.next
                : TextInputAction.done,
            controller: _itemCodeController,
            focusNode: itemCodeFocusNode,
            maxLines: 1,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateItemCode,
                isValidated: _itemCodeController.text.trim().isNotEmpty,
                hintText: (_itemInputType == InputType.Barcode)
                    ? 'Item Barcode'
                    : 'Item Code',
                icon: _itemInputType == InputType.Barcode
                    ? Icons.blur_linear
                    : Icons.search,
                iconPress: () async {
                  if (_itemInputType == InputType.Barcode) {
                    var result = await BarcodeScanner.scan();
                    setState(() {
                      _itemCodeController.text = result;
                      _getItemDetailsFromDB(context);
                    });
                  } else {
                    _getItemDetailsFromDB(context);
                  }
                }),
            keyboardType: TextInputType.name,
            onChanged: (value) {
              if (_itemInputType == InputType.Barcode) {
                setState(() {
                  _getItemDetailsFromDB(context);
                });
              }
            },
            validator: (value) {
              _getItemDetailsFromDB(context);
              return null;
            },
            onFieldSubmitted: (value) {
              setState(() {
                _getItemDetailsFromDB(context);
              });
            },
          ),
        ),
      ],
    );
  }

  _getPackageField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 50,
          child: Container(
            decoration: Common().getBCoreSD(
                isMandatory: shouldValidatePackage,
                isValidated: itemDetail.ProdpkgId != null),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: DropdownButton(
                underline: Text(""),
                isDense: false,
                isExpanded: true,
                hint: Text("Select Package"),
                value: itemDetail.ProdpkgId,
                items: itemPackingDataList != null
                    ? itemPackingDataList.map((ItemPackingData item) {
                        return DropdownMenuItem(
                            child: Text(item.packing_text,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: BCAppTheme().subTextColor,
                                )),
                            value: int.parse(item.packing_value));
                      }).toList()
                    : [],
                onChanged: (value) {
                  setState(() {
                    itemDetail.ProdpkgId = value;
                    itemDetail.PackageName = itemPackingDataList
                        .where((packing) =>
                            packing.packing_value == value.toString())
                        .toList()[0]
                        .packing_text;

                    _findUniqueItemWithItemCodeAndPackingId(context);
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  _getQuantityField() {
    return Column(
      children: [
        SizedBox(height: 10),
        SizedBox(
          height: 50,
          child: TextField(
            enableSuggestions: false,
            textInputAction: TextInputAction.done,
            controller: _quantityController,
            focusNode: quantityFocusNode,
            autocorrect: false,
            decoration: Common().getBCoreMandatoryID(
                isMandatory: shouldValidateQuantity,
                isValidated: _quantityController.text.isNotEmpty,
                hintText: 'Quantity'),
            onChanged: (value) {
              setState(() {
                itemDetail.ReqQty = double.parse(value);
              });
            },
            onSubmitted: (value) {
              setState(() {
                itemDetail.ReqQty = double.parse(value);
              });
            },
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }

  _getItemDetailsFromDB(BuildContext context) {
    if (_itemCodeController.text.isNotEmpty) {
      /// If item barcode or item code entered by user, get item details
      if (_itemInputType == InputType.ItemCode) {
        _shelfPrintingBloc
            .add(LoadItem(barcode: '', itemCode: _itemCodeController.text));
      } else if (_itemInputType == InputType.Barcode) {
        _shelfPrintingBloc
            .add(LoadItem(barcode: _itemCodeController.text, itemCode: ''));
      }
    }
  }

  _getItemBarcodeSticker() {
    return Column(
      children: [
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 275,
              color: BCAppTheme().drawerBg,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(4.0, 8.0, 4.0, 8.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                              child: Text(itemDetail.BrandName ?? "brand",
                                  textAlign: TextAlign.start)),
                          Expanded(
                              child: Text(itemDetail.PackageName ?? "package",
                                  textAlign: TextAlign.end))
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Image.network(
                                'https://static.vecteezy.com/system/resources/previews/001/199/360/non_2x/barcode-png.png',
                                height: 50)),
                      ],
                    ),

                    //  Row(
                    //     mainAxisSize: MainAxisSize.min,
                    //   // mainAxisAlignment: pw.MainAxisAlignment.center,
                    //   // crossAxisAlignment: pw.CrossAxisAlignment.stretch,
                    //   children: [
                    //     Column(
                    //       children: [
                    //         Expanded(
                    //             child: Image.network(
                    //                 'https://static.vecteezy.com/system/resources/previews/001/199/360/non_2x/barcode-png.png',
                    //                 height: 50)),
                    //
                    //       ],
                    //     ),
                    //     SizedBox(width: 2.0),
                    //     // Padding(
                    //     //   padding: const EdgeInsets.only(left: 4.0),
                    //     //   child: Column(
                    //     //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     //     mainAxisAlignment: MainAxisAlignment.end,
                    //     //     children: [
                    //     //       //${itemDetail.expiryDate}
                    //     //       Text("EXP:16/10/2021" ?? "EXP:",
                    //     //           style: TextStyle(fontSize: 7.0)),
                    //     //       SizedBox(height: 2.0),
                    //     //       Text("BN:${itemDetail.batchNo}" ?? "BN:",
                    //     //           style: TextStyle( fontSize: 7.0)),
                    //     //       SizedBox(height: 2.0),
                    //     //       Text("RF:${itemDetail.ItemCode}" ?? "RF:",
                    //     //           style: TextStyle( fontSize: 7.0)),
                    //     //     ],
                    //     //   ),
                    //     // ),
                    //   ],
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(itemDetail.ItemBarcode ?? "-",
                            textAlign: TextAlign.justify),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(itemDetail.ItemName ?? "item name"),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                              child: Text(itemDetail.ItemCode ?? "item code",
                                  textAlign: TextAlign.start)),
                          Expanded(
                              child: Text(
                                  (itemDetail.UnitCost ?? 0.0)
                                          .trim()
                                          .toString() ??
                                      "price",
                                  textAlign: TextAlign.end))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        // Row(children: [
        //   Container(
        //     width: 60,
        //     child: Padding(
        //       padding: const EdgeInsets.only(left: 4.0),
        //       child: Column(
        //         children: [
        //           Row(
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             children: [
        //               Text(itemDetail.ItemName ?? "item name",
        //                   style: TextStyle( fontSize: 8.0)),
        //             ],
        //           ),
        //           Row(
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             children: [
        //               Text(itemDetail.BrandName ?? "brand",
        //                   style: TextStyle(fontSize: 5.0))
        //             ],
        //           ),
        //           Row(
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             children: [
        //               Text(itemDetail.PackageName ?? "item name",
        //                   style: TextStyle( fontSize: 6.0)),
        //             ],
        //           ),
        //           Row(
        //             // mainAxisAlignment: pw.MainAxisAlignment.center,
        //             // crossAxisAlignment: pw.CrossAxisAlignment.stretch,
        //             children: [
        //               Column(
        //                 children: [
        //                   Expanded(
        //                       child: Image.network(
        //                           'https://static.vecteezy.com/system/resources/previews/001/199/360/non_2x/barcode-png.png',
        //                           height: 50)),
        //                   // Barcode(
        //                   //   barcode: Barcode.code128(),
        //                   //   data: barcode,
        //                   //   width: 110,
        //                   //   height: 50,
        //                   //   textStyle: TextStyle( fontSize: 8.0, lineSpacing: 2.0),
        //                   //   textPadding: 2,
        //                   //   drawText: true,
        //                   // ),
        //                 ],
        //               ),
        //               SizedBox(width: 2.0),
        //               Padding(
        //                 padding: const EdgeInsets.only(left: 4.0),
        //                 child: Column(
        //                   crossAxisAlignment: CrossAxisAlignment.start,
        //                   mainAxisAlignment: MainAxisAlignment.end,
        //                   children: [
        //                     //${itemDetail.expiryDate}
        //                     Text("EXP:16/10/2021" ?? "EXP:",
        //                         style: TextStyle(fontSize: 7.0)),
        //                     SizedBox(height: 2.0),
        //                     Text("BN:${itemDetail.batchNo}" ?? "BN:",
        //                         style: TextStyle( fontSize: 7.0)),
        //                     SizedBox(height: 2.0),
        //                     Text("RF:${itemDetail.ItemCode}" ?? "RF:",
        //                         style: TextStyle( fontSize: 7.0)),
        //                   ],
        //                 ),
        //               ),
        //             ],
        //           ),
        //           // pw.Row(
        //           //   mainAxisAlignment: pw.MainAxisAlignment.center,
        //           //   children: [
        //           //     pw.Text(itemDetail.ItemBarcode ?? "-",
        //           //         textAlign: pw.TextAlign.justify,
        //           //         style: pw.TextStyle(font: ttf, fontSize: 5.0)),
        //           //   ],
        //           // ),
        //         ],
        //       ),
        //     ),
        //   )
        // ])
      ],
    );
  }

  _getBlueToothPrinterField() {
    return Column(
      children: [
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemDetail.ProdpkgId != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Bluetooth Printer"),
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() {
                        // network_device = "";
                        _device = value;
                      }),
                      value: _device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
                  Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: StreamBuilder<bool>(
                stream: printerManager.isScanningStream,
                initialData: false,
                builder: (c, snapshot) {
                  if (snapshot.data) {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().blueColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          _stopScanDevices();
                        });
                  } else {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().primaryColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          // Common().showLoader(context);
                          _startScanDevices();
                        });
                  }
                },
              ),
            )
          ],
        )
      ],
    );
  }

  _getNetworkPrinterField() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemDetail.ProdpkgId != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Network Printer"),
                      // value: "Printer",
                      items: network_devices != null
                          ? network_devices.map((String item) {
                              return DropdownMenuItem(
                                  child: Text(item,
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        color: BCAppTheme().subTextColor,
                                      )),
                                  value: item);
                            }).toList()
                          : [],
                      onChanged: (value) {
                        setState(() {
                          // _device= null;
                          network_device = value;
                        });
                      },
                      value: network_device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
                  Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: IconButton(
                  icon: Icon(
                    Icons.search,
                    color: BCAppTheme().primaryColor,
                  ),
                  iconSize: 20,
                  onPressed: () {
                    showPrinterDialogue();
                  }),
            )
          ],
        )
      ],
    );
  }

  showPrinterDialogue() {
    Dialog printerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(child: StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Printer IP", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              Container(
                height: 45,
                child: TextField(
                  controller: ipTextController,
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    dropDownState(() {});
                  },
                  decoration: Common().getBCoreMandatoryID(
                      isMandatory: false,
                      isValidated: true,
                      hintText: 'Search',
                      icon: Icons.search),
                ),
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 35,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 0.5,
                              style: BorderStyle.solid,
                              color: BCAppTheme().primaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: FlatButton(
                          child: Text("Cancel",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Container(
                      height: 35,
                      child: FlatButton(
                          color: BCAppTheme().primaryColor,
                          child: Text("Ok",
                              style: TextStyle(
                                  color: BCAppTheme().secondaryColor,
                                  fontSize: 11)),
                          onPressed: () {
                            discover(context);
                            Navigator.pop(context);
                            // setState(() {
                            // });
                          }),
                    ),
                  )
                ],
              )
            ],
          );
        })),
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return printerDialog;
      },
    );
  }

  _getNetoworkPrinter() {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text("Network Printer"),
        SizedBox(height: 5),
        Row(
          children: [
            Text('Local ip: $localIp', style: TextStyle(fontSize: 20)),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: TextField(
                controller: portController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Port',
                  hintText: 'Port',
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 5),
        RaisedButton(
            child: Text('${isDiscovering ? 'Discovering...' : 'Discover'}'),
            onPressed: isDiscovering ? null : () => discover(context)),
        SizedBox(height: 5),
        found >= 0
            ? Text('Found: $found device(s)', style: TextStyle(fontSize: 16))
            : Container(),
        Container(
          height: 100,
          child: ListView.builder(
            itemCount: network_devices.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                // onTap: () => testPrint(network_devices[index], context),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.print),
                          SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  '${network_devices[index]}:${portController.text}',
                                  style: TextStyle(fontSize: 16),
                                ),
                                Text(
                                  'Click to print a test receipt',
                                  style: TextStyle(color: Colors.grey[700]),
                                ),
                              ],
                            ),
                          ),
                          Icon(Icons.chevron_right),
                        ],
                      ),
                    ),
                    Divider(),
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }

  List<DropdownMenuItem<PrinterBluetooth>> _getDeviceItems() {
    List<DropdownMenuItem<PrinterBluetooth>> items = [];
    if (_devices.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }

  void _setItemName() {
    if (itemDetail.ItemName != null && itemDetail.ItemName != "") {
      _itemCodeController.text = itemDetail.ItemName;
    }
  }

  void _loadItemPacking(BuildContext context) {
    _shelfPrintingBloc.add(LoadPacking(itemId: itemDetail.ItemId.toString()));
  }

  void _setItemInfo(ItemBarcodeData itemBarcodeData) {
    itemDetail.ItemBarcode = itemBarcodeData.Text;
    itemDetail.ItemCode = itemBarcodeData.ItemCode;
    itemDetail.ItemId = itemBarcodeData.ItemId;
    itemDetail.ProdpkgId = itemBarcodeData.ItemPackingId;
    itemDetail.ItemName = itemBarcodeData.ItemName;
    itemDetail.CountryId = itemBarcodeData.OriginCountryId;
    itemDetail.BrandId = itemBarcodeData.BrandId;
    itemDetail.UnitCost = itemBarcodeData.AverageCost.toDouble();
    itemDetail.NetCost = itemBarcodeData.NetCost.toDouble();

    // itemDetail. = itemBarcodeData.MinShelfLife;
    // itemDetail.prodStkTypeId = itemBarcodeData.ProductStockTypeId;
    // itemDetail.isBatchEnabledItem = itemBarcodeData.IsBatchEnabled;
//    basicItem.averageCost = itemBarcodeData.avg_cost.toString();
//    basicItem.netCost = itemBarcodeData.net_cost.toString();
  }

  void _startScanDevices() {
    setState(() {
      _devices = [];
    });
    printerManager.startScan(Duration(seconds: 4));
  }

  _stopScanDevices() {
    printerManager.stopScan();
  }

//write to app path
  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future show(
    String message, {
    Duration duration: const Duration(seconds: 3),
  }) async {
    await new Future.delayed(new Duration(milliseconds: 100));
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text(
          message,
          style: new TextStyle(
            color: Colors.white,
          ),
        ),
        duration: duration,
      ),
    );
  }

  void discover(BuildContext ctx) async {
    setState(() {
      isDiscovering = true;
      network_devices.clear();
      found = -1;
    });

    String ip;
    try {
      ip = await Wifi.ip;
      print('local ip:\t$ip');
    } catch (e) {
      final snackBar = SnackBar(
          content: Text('WiFi is not connected', textAlign: TextAlign.center));
      Scaffold.of(ctx).showSnackBar(snackBar);
      return;
    }
    setState(() {
      localIp = ipTextController.text;
    });

    final String subnet = localIp.substring(0, ip.lastIndexOf('.'));
    int port = 9100;
    try {
      port = int.parse(portController.text);
    } catch (e) {
      portController.text = port.toString();
    }
    print('subnet:\t$subnet, port:\t$port');

    final stream = NetworkAnalyzer.discover2(subnet, port);

    stream.listen((NetworkAddress addr) {
      if (addr.exists) {
        print('Found device: ${addr.ip}');
        setState(() {
          network_devices.add(addr.ip);
          found = network_devices.length;
        });
      }
    })
      ..onDone(() {
        setState(() {
          isDiscovering = false;
          found = network_devices.length;
        });
      })
      ..onError((dynamic e) {
        final snackBar = SnackBar(
            content: Text('Unexpected exception', textAlign: TextAlign.center));
        Scaffold.of(ctx).showSnackBar(snackBar);
      });
  }


  void bdna_zdk_print({String printerIp, String path}) {
    final ZLPrinter.ZSDK zsdk = ZLPrinter.ZSDK();

    zsdk
        .printPdfFileOverTCPIP(
            filePath: path,
            address: printerIp,
            port: int.tryParse(''),
            printerConf: ZLPrinter.PrinterConf(
              cmWidth: double.tryParse('6'),
              cmHeight: double.tryParse('2.54'),
              dpi: double.tryParse('180'),
              orientation: printerOrientation,
            ))
        .then((value) {
      setState(() {
        printStatus = PrintStatus.SUCCESS;
        message = "$value";
      });
    }, onError: (error, stacktrace) {
      try {
        throw error;
      } on PlatformException catch (e) {
        ZLPrinter.PrinterResponse printerResponse;
        try {
          printerResponse = ZLPrinter.PrinterResponse.fromMap(e.details);
          message =
              "${printerResponse.message} ${printerResponse.errorCode} ${printerResponse.statusInfo.status} ${printerResponse.statusInfo.cause}";
        } catch (e) {
          print(e);
          message = "${e.toString()}";
        }
      } on MissingPluginException catch (e) {
        message = "${e.message}";
      } catch (e) {
        message = "${e.toString()}";
      }
      setState(() {
        printStatus = PrintStatus.ERROR;
      });
    });
  }

  void genLblPDF(
      {String printerIp,
      BuildContext ctx,
        int copy,
      String brand,
      String package,
      String barcode,
      String itemName,
      String itemCode,
      String itemPrice,
      PdfPageFormat page}) async {
    final pdf = pw.Document();

    var data = await rootBundle.load("fonts/open-sans.ttf");
    final ttf = pw.Font.ttf(data);
  pdf.addPage(pw.Page(
      pageFormat: page,
      build: (pw.Context context) {
        return pw.Row(children: [

          pw.Container(
            width: 60,
            child: pw.Padding(
              padding: const pw.EdgeInsets.only(left: 4.0),
              child: pw.Column(
                children: [
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      pw.Text(itemDetail.ItemName ?? "Item name",
                          style: pw.TextStyle(font: ttf, fontSize: 10.0)),
                    ],
                  ),
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      pw.Text(itemDetail.BrandName ?? "Brand",
                          style: pw.TextStyle(font: ttf, fontSize: 9.0))
                    ],
                  ),
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      pw.Text(itemDetail.PackageName ?? "Pkg name",
                          style: pw.TextStyle(font: ttf, fontSize: 10.0)),
                    ],
                  ),
                  pw.Row(
                    // mainAxisAlignment: pw.MainAxisAlignment.center,
                    // crossAxisAlignment: pw.CrossAxisAlignment.stretch,
                    children: [
                      pw.Column(
                        children: [
                          pw.BarcodeWidget(
                            barcode: pw.Barcode.code128(),
                            data: barcode,
                            width: 110,
                            height: 60,
                            drawText: false,
                          ),
                        ],
                      ),
                      pw.SizedBox(width: 2.0),
                      pw.Padding(
                        padding: const pw.EdgeInsets.only(left: 4.0),
                        child: pw.Column(
                          mainAxisSize: pw.MainAxisSize.min,
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                          children: [
                            //${itemDetail.expiryDate}
                            pw.Text("E:16/10/2021" ?? "E:",
                                style:
                                pw.TextStyle(font: ttf,
                                    fontSize: 9.0,
                                    lineSpacing: 0.0)),
                            pw.SizedBox(height: 12.0),
                            pw.Text(itemBatchDataList.length > 0
                                ? "BN:${itemBatchDataList[0].batch_text}"
                                : "BN:",
                                style:
                                pw.TextStyle(font: ttf, fontSize: 9.0)),
                            pw.SizedBox(height: 12.0),
                            pw.Text("RF:${itemDetail.ItemCode}" ?? "RF:",
                                style:
                                pw.TextStyle(font: ttf, fontSize: 9.0)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  pw.SizedBox(height: 2,),
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      pw.Padding(
                        padding: const pw.EdgeInsets.only(left: 8.0),
                        child: pw.Text(itemDetail.ItemBarcode ?? "-",
                            textAlign: pw.TextAlign.justify,
                            style: pw.TextStyle(font: ttf, fontSize: 12.0)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ]);
        //   pw.Center(
        //   child: pw.Text(""),
        // ); // Center
      }));
// }

    final appDocDir =
        await getExternalStorageDirectory(); //getApplicationDocumentsDirectory();
    final appDocPath = appDocDir.absolute.path;
    final file = File(appDocPath + '/' + 'document.pdf');
    print('Save as file ${file.path} ...');
    await file.writeAsBytes(await pdf.save());
    for(int i = 0; i < copy; i++){
      await Future.delayed(Duration(milliseconds: 500), () {
        bdna_zdk_print(printerIp: printerIp, path: file.path);
      });

    }

  }

  void _findUniqueItemWithItemCodeAndPackingId(BuildContext context) {
    if (itemDetail.ItemId != null && itemDetail.ProdpkgId != null) {
      _shelfPrintingBloc.add(LoadItemByPacking(
          itemId: itemDetail.ItemId.toString(),
          prodPkgId: itemDetail.ProdpkgId.toString()));
    }

  }

  void _getItemPriceDetails() {
    _shelfPrintingBloc.add(GetScmItemPriceDetails(
        type: TransactionType.MR,
        companyId: fromAddress.fromCompanyId,
        divisionId: fromAddress.fromDivisionId,
        locationId: fromAddress.fromLocationId,
        toCompanyId: fromAddress.fromCompanyId,
        toDivisionId: fromAddress.fromDivisionId,
        toLocationId: fromAddress.fromLocationId,
        // toCompanyId: toAddress.companyId,
        // toDivisionId: toAddress.divisionId,
        // toLocationId: toAddress.locationId,
        itemId: itemDetail.ItemId.toString(),
        packingId: itemDetail.ProdpkgId.toString()));
  }

  _loadItemPackingById() {
    _shelfPrintingBloc.add(LoadPackingById(
        packId: itemDetail.ProdpkgId.toString(),
        itemId: itemDetail.ItemId.toString()));
  }

  _loadCountry() {
    _shelfPrintingBloc
        .add(LoadCountryById(countryId: itemDetail.CountryId.toString()));
  }

  _loadProductBrand() {
    _shelfPrintingBloc
        .add(LoadBrandById(brandId: itemDetail.BrandId.toString()));
  }
}

enum PrintStatus {
  PRINTING,
  SUCCESS,
  ERROR,
  NONE,
}

enum CheckingStatus {
  CHECKING,
  SUCCESS,
  ERROR,
  NONE,
}
