part of 'shelf_printing_bloc.dart';


abstract class ShelfPrintingEvent extends Equatable {
  const  ShelfPrintingEvent();
}

class GetStockReferenceDetails extends ShelfPrintingEvent {
  final String refId;
  const GetStockReferenceDetails({this.refId});

  @override
  List<Object> get props => [refId];

  @override
  String toString() => 'GetStockReferenceDetails { RefId : $refId}';
}

class LoadItem extends ShelfPrintingEvent {
  final String barcode;
  final String itemCode;

  const LoadItem({this.barcode, this.itemCode});

  @override
  List<Object> get props => [barcode, itemCode];

  @override
  String toString() =>
      'LoadItem {"Barcode" : "$barcode", "ItemCode" : "$itemCode"}';
}

class LoadPacking extends ShelfPrintingEvent {
  final String itemId;

  const LoadPacking({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadPacking {"ItemId" : "$itemId"}';
}

class LoadPackingById extends ShelfPrintingEvent {
  final String packId;
  final String itemId;

  const LoadPackingById({this.packId, this.itemId});

  @override
  List<Object> get props => [packId, itemId];

  @override
  String toString() =>
      'LoadPacking {"PackId" : "$packId", "ItemId" : "$itemId"}';
}

class LoadCountryById extends ShelfPrintingEvent {
  final String countryId;

  const LoadCountryById({this.countryId});

  @override
  List<Object> get props => [countryId];

  @override
  String toString() => 'LoadCountryById {"CountryId" : "$countryId"}';
}

class LoadBrandById extends ShelfPrintingEvent {
  final String brandId;

  const LoadBrandById({this.brandId});

  @override
  List<Object> get props => [brandId];

  @override
  String toString() => 'LoadBrandById {"BrandId" : "$brandId"}';
}

class LoadItemByPacking extends ShelfPrintingEvent {
  final String itemId;
  final String prodPkgId;

  const LoadItemByPacking({this.itemId, this.prodPkgId});

  @override
  List<Object> get props => [itemId, prodPkgId];

  @override
  String toString() =>
      'LoadItemByPacking {"ItemId" : "$itemId", "ProdPkgId" : "$prodPkgId"}';
}

class GetWareHouse extends ShelfPrintingEvent {
  const GetWareHouse();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetWareHouse { }';
}

class GetWareHouseZone extends ShelfPrintingEvent {
  final String warehouseId;

  const GetWareHouseZone({this.warehouseId});

  @override
  List<Object> get props => [warehouseId];

  @override
  String toString() => 'GetWareHouseZone {"WarehouseId" : "$warehouseId"}';
}

class GetWareHouseRack extends ShelfPrintingEvent {
  final String zoneId;

  const GetWareHouseRack({this.zoneId});

  @override
  List<Object> get props => [zoneId];

  @override
  String toString() => 'GetWareHouseRack {"ZoneId" : "$zoneId"}';
}

class GetWareHouseBin extends ShelfPrintingEvent {
  final String rackId;

  const GetWareHouseBin({this.rackId});

  @override
  List<Object> get props => [rackId];

  @override
  String toString() => 'GetWareHouseBin {"RackId" : "$rackId"}';
}

class GetPackingDetails extends ShelfPrintingEvent {
  final TransactionType type;
  final String companyId;
  final String divisionId;
  final String locationId;
  final String toCompanyId;
  final String toDivisionId;
  final String toLocationId;
  final String supplierId;
  final String itemId;
  final String packingId;

  const GetPackingDetails(
      {this.type,
        this.companyId,
        this.divisionId,
        this.locationId,
        this.toCompanyId,
        this.toDivisionId,
        this.toLocationId,
        this.supplierId,
        this.itemId,
        this.packingId});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetPackingDetails {"Type" : "$type","CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"'
          '"ToDivisionId" : "$toDivisionId","ToLocationId" : "$toLocationId","SupplierId" : "$supplierId","ItemId" : "$itemId","PackingId" : "$packingId"}';
}

class LoadBatchById extends ShelfPrintingEvent {
  final int itemId;

  const LoadBatchById({this.itemId});

  @override
  List<Object> get props => [itemId];

  @override
  String toString() => 'LoadBatchById {"ItemId" : "$itemId"}';
}

class GetItemBatchMaster extends ShelfPrintingEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String lastFetchDate;

  const GetItemBatchMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate];

  @override
  String toString() => 'GetItemBatchMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'lastFetchDate: $lastFetchDate}';
}

class GetReason extends ShelfPrintingEvent {

  const GetReason();

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetReason';
}

class GetScmItemPriceDetails extends ShelfPrintingEvent {
  final TransactionType type;
  final String companyId;
  final String divisionId;
  final String locationId;
  final String toCompanyId;
  final String toDivisionId;
  final String toLocationId;
  final String supplierId;
  final String itemId;
  final String packingId;

  const GetScmItemPriceDetails(
      {this.type,
        this.companyId,
        this.divisionId,
        this.locationId,
        this.toCompanyId,
        this.toDivisionId,
        this.toLocationId,
        this.supplierId,
        this.itemId,
        this.packingId});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'GetPackingDetails {"Type" : "$type","CompanyId" : "$companyId","DivisionId" : "$divisionId","LocationId" : "$locationId"'
          '"ToDivisionId" : "$toDivisionId","ToLocationId" : "$toLocationId","SupplierId" : "$supplierId","ItemId" : "$itemId","PackingId" : "$packingId"}';
}

