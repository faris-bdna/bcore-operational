part of 'shelf_printing_bloc.dart';

abstract class ShelfPrintingState extends Equatable {
  const ShelfPrintingState();

  @override
  List<Object> get props => [];
}

/// Get Stock Reference Details

class GetStockReferenceDetailsInitial extends ShelfPrintingState{}

class GetStockReferenceDetailsInProgress extends ShelfPrintingState{}

class GetStockReferenceDetailsComplete extends ShelfPrintingState{
  final GetStockReferenceDetailsResp resp;

  GetStockReferenceDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetStockReferenceDetailsFailure extends ShelfPrintingState {
  final String error;

  const GetStockReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetStockReferenceDetailsFailure { error : $error }';
}


/// Load Items

class LoadItemInitial extends ShelfPrintingState{}

class LoadItemInProgress extends ShelfPrintingState{}

class LoadItemComplete extends ShelfPrintingState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends ShelfPrintingState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends ShelfPrintingState{}

class LoadPackingInProgress extends ShelfPrintingState{}

class LoadPackingComplete extends ShelfPrintingState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends ShelfPrintingState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing by packing id

class LoadPackingByIdInitial extends ShelfPrintingState{}

class LoadPackingByIdInProgress extends ShelfPrintingState{}

class LoadPackingByIdComplete extends ShelfPrintingState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends ShelfPrintingState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends ShelfPrintingState{}

class LoadItemByPackingInProgress extends ShelfPrintingState{}

class LoadItemByPackingComplete extends ShelfPrintingState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends ShelfPrintingState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends ShelfPrintingState{}

class LoadCountryByIdInProgress extends ShelfPrintingState{}

class LoadCountryByIdComplete extends ShelfPrintingState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends ShelfPrintingState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends ShelfPrintingState{}

class LoadBrandByIdInProgress extends ShelfPrintingState{}

class LoadBrandByIdComplete extends ShelfPrintingState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends ShelfPrintingState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get WareHouse

class GetWareHouseInitial extends ShelfPrintingState{}

class GetWareHouseInProgress extends ShelfPrintingState{}

class GetWareHouseComplete extends ShelfPrintingState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends ShelfPrintingState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends ShelfPrintingState{}

class GetWareHouseZoneInProgress extends ShelfPrintingState{}

class GetWareHouseZoneComplete extends ShelfPrintingState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends ShelfPrintingState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends ShelfPrintingState{}

class GetWareHouseRackInProgress extends ShelfPrintingState{}

class GetWareHouseRackComplete extends ShelfPrintingState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends ShelfPrintingState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends ShelfPrintingState{}

class GetWareHouseBinInProgress extends ShelfPrintingState{}

class GetWareHouseBinComplete extends ShelfPrintingState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends ShelfPrintingState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get Packing Details
//
// class GetPackingDetailsInitial extends ShelfPrintingState{}
//
// class GetPackingDetailsInProgress extends ShelfPrintingState{}
//
// class GetPackingDetailsComplete extends ShelfPrintingState{
//   final GetScmItemPriceDetailsResp getPackingDetailsResp;
//
//   GetPackingDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);
//
//   @override
//   List<Object> get props => [getPackingDetailsResp];
// }
//
// class GetPackingDetailsFailure extends ShelfPrintingState {
//   final String error;
//
//   const GetPackingDetailsFailure({@required this.error});
//
//   @override
//   List<Object> get props => [error];
//
//   @override
//   String toString() => 'GetPackingDetailsFailure { error : $error }';
// }


/// LoadBatchById States

class LoadBatchByIdInitial extends ShelfPrintingState{}

class LoadBatchByIdInProgress extends ShelfPrintingState{}

class LoadBatchByIdComplete extends ShelfPrintingState{
  final List<ItemBatchData> getItemBatchResp;

  LoadBatchByIdComplete({@required this.getItemBatchResp}) : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class LoadBatchByIdFailure extends ShelfPrintingState {
  final String error;

  const LoadBatchByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBatchByIdFailure { error : $error }';
}



class GetReasonInitial extends ShelfPrintingState {}

class GetReasonInProgress extends ShelfPrintingState {}

class GetReasonComplete extends ShelfPrintingState {
  final GetCommonResp getReasonResp;

  GetReasonComplete({@required this.getReasonResp}) : assert(getReasonResp != null);

  @override
  List<Object> get props => [getReasonResp];
}

class GetReasonFailure extends ShelfPrintingState {
  final String error;

  const GetReasonFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetReasonFailure { error : $error }';
}


/// GetItemPriceDetails States
class GetScmItemPriceDetailsInitial extends ShelfPrintingState{}

class GetScmItemPriceDetailsInProgress extends ShelfPrintingState{}

class GetScmItemPriceDetailsComplete extends ShelfPrintingState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetScmItemPriceDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetScmItemPriceDetailsFailure extends ShelfPrintingState {
  final String error;

  const GetScmItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetScmItemPriceDetailsFailure { error : $error }';
}