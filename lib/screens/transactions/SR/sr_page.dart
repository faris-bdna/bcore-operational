import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/SR/srReq.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/ItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/ToAddressForm.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'dart:convert';
import 'package:intl/intl.dart';


final GlobalKey<CoolStepperState> coolSRStepperState =
    GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldSRKey = new GlobalKey<ScaffoldState>();

class SRPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const SRPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _SRPageState createState() => _SRPageState(repository, appLanguage, database);
}

class _SRPageState extends State<SRPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  bool needToUpdateStepper = false;
  CommonItem itemForStepperUpdate;
  DraftTransactionData draftTransactionData;

  int savedDraftId;

  TransBloc _transBloc;

  _SRPageState(this.repository, this.appLanguage, this.database);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    toAddress.clear();
    CommonItemViewModel.shared.cmnItemList.clear();
    SRReq.shared.clear();
    Utils.resetUniqueId();
  }

  getStepWithItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? ItemEntryForm(
              database: database,
              repository: repository,
              type: TransactionType.SR,
              appLanguage: appLanguage,
              item: itemForStepperUpdate)
          : ItemEntryForm(
              database: database,
              repository: repository,
              type: TransactionType.SR,
              appLanguage: appLanguage),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonItemViewModel.shared.cmnItemList.isNotEmpty) {
          SRReq.shared.fromViewJson(CommonItemViewModel.shared.toJson());
          updateDraftTransaction(TransactionType.SR.value);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSRKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<CoolStep> steps = [
      CoolStep(
        title: "To Address",
        subContent: Text("Enter Details"),
        content: AddressForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SR),
        validation: () {
          if (toAddress.transactionDate == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSRKey, message: Constants.transactionDateValidationMsg);
            return Constants.transactionDateValidationMsg;
          } else if (toAddress.storeIssueId == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSRKey, message: Constants.refPlaceHolderText);
            return Constants.refPlaceHolderText;
          }
          SRReq.shared.fromViewJson(toAddress.toJson());

          /// Generate draft transaction
          setDraftTransaction(TransactionType.SR.value);

          /// Save material request entry to draft trans tablet
          _transBloc.add(
              SaveDraftTransaction(draftTransactionData: draftTransactionData));
          return null;
        },
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (toAddress.companyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${toAddress.companyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.divisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${toAddress.divisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.locationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${toAddress.locationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.departmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${toAddress.departmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.SR,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              if (CommonItemViewModel.shared.cmnItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonItemViewModel
                      .shared.cmnItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolSRStepperState.currentState.onStepBack();
                });
                // needToUpdateStepper = false;
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolSRStepperState,
      onCompleted: () {
        print("Steps completed!");

        if (SRReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSRKey, message: Constants.noItemAddedMessage);
          return;
        }

        print(SRReq.shared.toJson());

        /**
         * Adds Line no for each item & Formats the expiry date to the company format
         */
        for (var index = 0; index < SRReq.shared.itemDetails.length; index++) {
          SRReq.shared.itemDetails[index].lineNum = index +1;

          if (SRReq.shared.itemDetails[index].expiryDateTemp != null) {
            try {
              var expiryDateInDateTimeFormat = DateFormat(
                  FromAddress.shared.fromCompanyDateFormat).parse(
                  SRReq.shared.itemDetails[index].expiryDateTemp);

              SRReq.shared.itemDetails[index].expiryDateTemp =
              // Utils.getDateByFormat(passedDateTime: DateTime.parse(SRReq.shared.itemDetails[index].expiryDateTemp), format: SRReq.shared.dateFormat);
              Utils.getDateByFormat(
                  passedDateTime: expiryDateInDateTimeFormat,
                  format: SRReq.shared.dateFormat);
            } on Exception catch (_) {}
          }
        }

        try {
          var entryDateInDateTimeFormat = DateFormat(FromAddress.shared.fromCompanyDateFormat).parse(SRReq.shared.entryDateTemp);

          /// Convert Entry date format to format from PRReferenceDetails
          SRReq.shared.entryDateTemp = Utils.getDateByFormat(
              passedDateTime:entryDateInDateTimeFormat,format: SRReq.shared.dateFormat);

        } on Exception catch (_) {}

        _transBloc.add(SaveStoreReceipt(req: jsonEncode(SRReq.shared)));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
        create: (context) =>
            TransBloc(repository: repository, database: database),
        child: BlocListener<TransBloc, TransState>(
          listener: (context, state) {

            if (state is SaveDraftTransactionInProgress ||
                state is SaveStoreReceiptInProgress ||
                state is UpdateDraftTransactionInProgress ||
                state is DeleteDraftTransactionInProgress
            ) {
              Common().showLoader(context);
            }
            else if (state is SaveDraftTransactionComplete) {
              savedDraftId = state.savedTransactionId;
              Common().setTransactionId(savedDraftId);
              Navigator.pop(context, true);
            }
            else if (state is SaveStoreReceiptComplete) {
              Navigator.pop(context, true);

              Common().showAlertMessage(
                  context: context,
                  title: 'Success',
                  message:
                      state.saveStoreReceiptResp.validationDetails.statusMessage,
                  okFunction: () {
                    resetTransactionData();
                    Navigator.pop(context, true);

                    _transBloc.add(DeleteDraftTransaction(
                        draftTransactionData: draftTransactionData));
                  });
            }
            else if (state is UpdateDraftTransactionComplete) {
              Navigator.pop(context, true);
            }
            else if (state is SaveStoreReceiptFailure) {
              Navigator.pop(context, true);
              String errorMessage = state.error.replaceAll('<br/>', '');
              errorMessage = errorMessage.replaceAll('<br>', '');
              errorMessage = errorMessage.replaceAll('Exception:', '');
              Common().showAlertMessage(
                  context: context,
                  title: "Sorry",
                  message: errorMessage,
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else if (state is DeleteDraftTransactionFailure) {
              Navigator.pop(context, true);
              Navigator.pop(context, true);
            }
            else if (state is UpdateDraftTransactionFailure) {
              Navigator.pop(context, true);
            }
            else if (state is DeleteDraftTransactionComplete) {
              Navigator.pop(context, true);
              Navigator.pop(context, true);
            }
          },
          child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
            _transBloc = BlocProvider.of<TransBloc>(context);

            return WillPopScope(
                onWillPop: () async {
                  Common().showAlertMessageWithAction(context: context,
                      title: Constants.alertMsgTitle,
                      message: Constants.transactionClearMsg,
                      okButtonTitle: 'Ok',
                      okFunction: (){
                        resetTransactionData();
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      actionButtonTitle: 'Cancel',
                      actionFunction: (){
                        Navigator.pop(context);
                      });
                  return null;
                },
                child: Scaffold(
                  key: scaffoldSRKey,
                  appBar: BCAppBar(
                    scaffoldKey: scaffoldSRKey,
                    title: 'Store Receipt',
                    isBack: false,
                    appBar: AppBar(),
                    database: database,
                    appLanguage: appLanguage,
                    repository: repository,
                    shouldHideActions: false,
                  ),
                  body: Scaffold(
                    drawer: NavigationDrawer(
                      context: context,
                      repository: repository,
                      appLanguage: appLanguage,
                      database: database,
                    ),
                    body: Container(
                      child: stepper,
                    ),
                  ),
                ));
          }),
        ));
  }

  void setDraftTransaction(String type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type + '_' + uuid.v1(),
        trans_type: type,
        trans_details: SRReq.shared.toJson().toString());
  }

  updateDraftTransaction(String type) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type + '_' + uuid.v1(),
          trans_type: type,
          trans_details: SRReq.shared.toJson().toString());
    }
  }
}
