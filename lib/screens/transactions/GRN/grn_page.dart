import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/grn/saveGrnRequest.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemFocDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemOtherCostDetail.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemSerialNoDetail.dart';
import 'package:bcore_inventory_management/models/view/highItem/common_highItem_details.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/supplierDetails.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/High/highItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Supplier/supplier_Form.dart';
import 'package:bcore_inventory_management/screens/transactions/PrintPreview/printPreview.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolStepperGRNState =
    GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldGRNKey = new GlobalKey<ScaffoldState>();

class GRNPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const GRNPage({Key key, this.repository, this.database, this.appLanguage})
      : super(key: key);

  @override
  _GRNPageState createState() =>
      _GRNPageState(repository, database, appLanguage);
}

class _GRNPageState extends State<GRNPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  List<OtherCostDetails> otherCostDetailsList;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  CommonHighItem itemForStepperUpdate;
  bool needToUpdateStepper = false;

  _GRNPageState(this.repository, this.database, this.appLanguage);
  String _grnRefNo;

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  getStepWithHighItemEntryForm() {
    return CoolStep(
/**
 * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
*/
      content: (needToUpdateStepper)
          ? HighItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.GRN,
              item: itemForStepperUpdate)
          : HighItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.GRN),
      validation: () {

        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonHighItemInfo.shared.cmnHighItemList.isNotEmpty) {
          GrnReq.shared.fromJson(CommonHighItemInfo.shared.toJson());
          GrnReq.shared.fOCDetails = grn_foc_details;

          updateDraftTransaction(TransactionType.GRN);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    cmnSupplierInfo.clear();
    CommonHighItemInfo.shared.cmnHighItemList.clear();
    GrnReq.shared.clear();
    grn_foc_details.clear();
    Utils.resetUniqueId();
  }

  _showAlertToPrint(BuildContext context, String grnRefNo) {

    Common().showAlertMessageWithAction(
        context: context,
        title: Constants.alertMsgTitle,
        message: 'Do you wish to print sticker ?',
        okButtonTitle: "Cancel",
        okFunction: (){
          Navigator.pop(context);
          Navigator.pop(context);
        },
        actionButtonTitle: "Ok",
        actionFunction: (){
          Navigator.pop(context);

          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => PrintPreview(
                      repository: repository,
                      database: Database(),
                      appLanguage: appLanguage,
                  grnRefNo: grnRefNo,)));
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    // GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    BuildContext blocContext;

    final List<CoolStep> steps = [
      CoolStep(
        title: "Select Supplier Details",
        subContent: SizedBox(height: 0),
        content: SupplierForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.GRN),
        validation: () {
          // Validate supplier details
          if (_validateSupplier(context: context)) {
            setDraftTransaction(TransactionType.GRN.value);
            BlocProvider.of<TransBloc>(blocContext).add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            /// Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithHighItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.GRN,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              if (CommonHighItemInfo.shared.cmnHighItemList[indexOfEditingItem] != null) {
                setState(() {
                  itemForStepperUpdate = CommonHighItemInfo.shared.cmnHighItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperGRNState.currentState.onStepBack();
                  Future.delayed(Duration(seconds: 1), (){
                    needToUpdateStepper = false;
                  });
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolStepperGRNState,
      onCompleted: () {
        print("Steps completed!");

        /// Validates item list is empty or not
        if (GrnReq.shared.itemDetails.isEmpty && GrnReq.shared.fOCDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldGRNKey, message: Constants.noItemAddedMessage);
          return;
        }

        /// Here the item line number will be assigned
        for (var index = 0; index < GrnReq.shared.itemDetails.length; index++) {
          GrnReq.shared.itemDetails[index].lineNum = index+1;
        }

        /// Here the line numbers of foc item and parent item will be assigned
        for (var index = 0; index < GrnReq.shared.fOCDetails.length; index++) {
          var parentItemLineNum = GrnReq.shared.itemDetails
              .where((item) =>
                  item.index == GrnReq.shared.fOCDetails[index].parentIndex)
              .toList()[0]
              .lineNum;
          var focItemLineNum = GrnReq.shared.itemDetails
              .where((item) =>
                  item.index == GrnReq.shared.fOCDetails[index].focIndex)
              .toList()[0]
              .lineNum;

          GrnReq.shared.fOCDetails[index].fOCItemLineNo = focItemLineNum;
          GrnReq.shared.fOCDetails[index].gRNItemLineNo = parentItemLineNum;
        }

        ///  If there is any serial nos in the items list then it is converted from list of String to  the list of SerialNoDetails
        int serialNoLineNo = 1;
        List <SerialNoDetails> serialNoDetails = [];

        for (var index = 0; index < GrnReq.shared.itemDetails.length; index++){
          for (String serialNo in GrnReq.shared.itemDetails[index].serialNoList){
            SerialNoDetails snDetails = SerialNoDetails(
              dtLineNum: GrnReq.shared.itemDetails[index].lineNum,
              itemId: GrnReq.shared.itemDetails[index].itemId,
              itemCode: GrnReq.shared.itemDetails[index].itemCode,
              serialNo: serialNo,
              serialLineNum: serialNoLineNo
            );
            serialNoDetails.add(snDetails);
            serialNoLineNo++;
          }
        }
        GrnReq.shared.serialNoDetails = serialNoDetails;

        _bindAddressToGRN();

        // Send Grn
        BlocProvider.of<TransBloc>(blocContext)
            .add(SaveGrn(grnReq: json.encode(GrnReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is SaveDraftTransactionInProgress ||
              state is SaveGrnInProgress) {
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          }
          else if (state is SaveGrnComplete) {
            _grnRefNo = state.saveGrnResp.validationDetails.statusMessage;
            _grnRefNo = _grnRefNo.substring(9,25);
            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.saveGrnResp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();
                  Navigator.pop(context, true);
                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                });
          }
          else if (state is SaveGrnFailure) {
            Navigator.pop(context,true);
            String errorMessage = state.error.replaceAll('<br/>', '');
            errorMessage = errorMessage.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is DeleteDraftTransactionComplete) {
            Navigator.pop(context, true);
            // Navigator.pop(context, true);
            _showAlertToPrint(context,_grnRefNo);
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            // Navigator.pop(context, true);
            _showAlertToPrint(context,_grnRefNo);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          blocContext = context;
          _transBloc = BlocProvider.of<TransBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldGRNKey,
              appBar: BCAppBar(
                scaffoldKey: scaffoldGRNKey,
                title: 'Goods Receiving Note',
                isBack: false,
                appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: Container(
                child: stepper,
              ),
            ),
          );
        }),
      ),
    );
  }

  void _bindAddressToGRN() {
    if (cmnSupplierInfo.gRNTypeId == GRNType.Direct.index + 1) {
      GrnReq.shared.companyId = int.parse(fromAddress.fromCompanyId ?? '0');
      GrnReq.shared.divisionId = int.parse(fromAddress.fromDivisionId ?? '0');
      GrnReq.shared.locId = int.parse(fromAddress.fromLocationId ?? '0');
      GrnReq.shared.departmentId = int.parse(fromAddress.fromDepartmentId ?? '0');
      GrnReq.shared.projectId = int.tryParse(fromAddress.fromProjectId ?? '0');
    } else {
      GrnReq.shared.companyId = cmnSupplierInfo.companyId;
      GrnReq.shared.divisionId = cmnSupplierInfo.divisionId;
      GrnReq.shared.locId = cmnSupplierInfo.locationId;
      GrnReq.shared.departmentId = cmnSupplierInfo.departmentId;
      GrnReq.shared.projectId = cmnSupplierInfo.projectId;
    }
  }

  void setDraftTransaction(String type) {
    String grnNo;
    if (GrnReq.shared.gRNTypeId != 1) {
      grnNo = GrnReq.shared.gRNTypeRefNo.toString();
    } else {
      var uuid = Uuid();
      grnNo = uuid.v1();
    }

    draftTransactionData = DraftTransactionData(
        trans_id: type + '_' + grnNo,
        trans_type: type,
        trans_details: GrnReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: GrnReq.shared.toJson().toString());
    }
  }

  bool _validateSupplier({BuildContext context}) {

    /// Checks when GRN Type is LPO or IPO  and user has not selected reference no
    if ((cmnSupplierInfo.gRNTypeId == 2 ||
            cmnSupplierInfo.gRNTypeId ==
                3 ) && (cmnSupplierInfo.gRNTypeRefNo == null)) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldGRNKey, message: "Please select a Reference");
      return false;
    }

    /// checks the user has entered the transaction date or not
     else if (cmnSupplierInfo.gRNDateTemp.isEmpty) {
      cmnSupplierInfo.gRNDateTemp = Utils.getDateByFormat(
          passedDateTime: DateTime.now(),
          format: fromAddress.fromCompanyDateFormat);
      Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: "Please select GRN date");
      return false;
    }
    /// Checks the user has selected the supplier or not
    else if (cmnSupplierInfo.suppId == null || cmnSupplierInfo.suppId == 0) {
      Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: "Please select Supplier");
      return false;
    }
    else if ((cmnSupplierInfo.gRNTypeId == 1) &&
        (cmnSupplierInfo.currencyId == null || cmnSupplierInfo.currencyId == 0)) {
      Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: "Please select Currency");
      return false;
    }
    /// Checks when with_invoice has enabled and invoice no, not been entered
    else if (cmnSupplierInfo.isInvoiceRcvd && (cmnSupplierInfo.invoiceNo == null || cmnSupplierInfo.invoiceNo.isEmpty)) {
      Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: "Please enter Invoice Details");
      return false;
    }
    /// Checks when with_invoice has not enabled and delivery note, not been entered
    else if (cmnSupplierInfo.isInvoiceRcvd == false && (cmnSupplierInfo.deliveryNoteNo == null || cmnSupplierInfo.deliveryNoteNo.isEmpty)) {
      Common().showStepperValidation(scaffoldKey: scaffoldGRNKey, message: "Please enter Delivery note");
      return false;
    } else {
      GrnReq.shared.fromJson(cmnSupplierInfo.toJson());
      return true;
    }
  }
}
