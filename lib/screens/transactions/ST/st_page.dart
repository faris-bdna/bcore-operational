import 'dart:convert';
import 'dart:io';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:intl/intl.dart';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicFormInfo.dart';
import 'package:bcore_inventory_management/models/view/CommonBasicItemInfo.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/BasicForm/basic_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/basicItem/basic_item_entry_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/basic_summary_form.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolSTStepperState = GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldSTKey = new GlobalKey<ScaffoldState>();

class STPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  STPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _STPageState createState() => _STPageState(repository, appLanguage, database);
}

class _STPageState extends State<STPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;
  int savedDraftId;

  bool needToUpdateStepper = false;
  CommonBasicItem itemForStepperUpdate;
  _STPageState(
      this.repository,
      this.appLanguage,
      this.database,
      );
//  BuildContext blocContext;


  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData(){
    cmnBasicFormInfo.clear();
    CommonBasicItemInfo.shared.cmnBasicItemList.clear();
    SaveSTReq.shared.clear();
    Utils.resetUniqueId();
  }

  getStepWithItemEntryForm() {
    return  CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? BasicItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.ST, item: itemForStepperUpdate)
          : BasicItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.ST),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonBasicItemInfo.shared.cmnBasicItemList.isNotEmpty) {
          SaveSTReq.shared.fromViewJson(CommonBasicItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.ST);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(scaffoldKey: scaffoldSTKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  bool _validateStockDetails({BuildContext context}) {
    if (cmnBasicFormInfo.stockReferenceNo == null) {
      Common().showStepperValidation(scaffoldKey: scaffoldSTKey, message: Constants.refPlaceHolderText);
      return false;
    }
    SaveSTReq.shared.fromViewJson(cmnBasicFormInfo.toJson());
    return true;
  }

  @override
  Widget build(BuildContext cont) {
    BuildContext blocContext;

    final formKey = GlobalKey<FormState>();
    List<CoolStep> steps = [
      CoolStep(
        title: "",
        content:
        BasicForm(key:formKey,repository: repository, appLanguage: appLanguage,
            type: TransactionType.ST),
        validation: () {
          // Validate Stock details
          if (_validateStockDetails(context: context)) {
            setDraftTransaction(TransactionType.ST);
            _transBloc.add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if(toAddress.companyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${toAddress.companyName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.divisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${toAddress.divisionName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.locationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${toAddress.locationName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.departmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${toAddress.departmentName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: BasicSummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.ST,
            navigateBackWithValueIndex: (int indexOfEditingItem){

              if (CommonBasicItemInfo.shared.cmnBasicItemList[indexOfEditingItem] != null) {
                setState(() {
                  itemForStepperUpdate = CommonBasicItemInfo.shared.cmnBasicItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolSTStepperState.currentState.onStepBack();
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];
    
    CoolStepper stepper = CoolStepper(
      key: coolSTStepperState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveSTReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSTKey, message: Constants.noItemAddedMessage);
          return;
        }

        /**
         * Adds Line no for each item
         */
        for (var index = 0; index < SaveSTReq.shared.itemDetails.length; index++) {
          SaveSTReq.shared.itemDetails[index].lineNum = index +1;
        }

        SaveSTReq.shared.companyId = fromAddress.fromCompanyId;

        /// Convert Entry date format to format from StockReferenceDetails
        SaveSTReq.shared.entryDateTemp = Utils.getDateByFormat(
            passedDateTime:DateFormat(FromAddress.shared.fromCompanyDateFormat).parse(SaveSTReq.shared.entryDateTemp),format: SaveSTReq.shared.dateFormat);


        print(json.encode(SaveSTReq.shared.toJson()));
        BlocProvider.of<TransBloc>(blocContext).add(SaveST(stReq: SaveSTReq.shared));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle: TextStyle(color: BCAppTheme().primaryColor,fontSize: 15),

      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0,right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {

          if (state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress ||
              state is SaveSTInProgress) {
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          }
          else if (state is SaveSTComplete) {
            Navigator.pop(context, true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();
                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          }
          else if (state is SaveSTFailure) {
            Navigator.pop(context, true);
            String errorMessage = state.error.replaceAll('<br/>', '');
            errorMessage = errorMessage.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is DeleteDraftTransactionComplete)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionComplete ) {
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionFailure ) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);
          blocContext = context;

          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldSTKey,
              appBar: BCAppBar(scaffoldKey:scaffoldSTKey,title: 'Stock Taking', isBack: false,appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(TransactionType type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type.value + '_' + uuid.v1(),
        trans_type: type.value,
        trans_details: SaveSTReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SaveSTReq.shared.toJson().toString());
    }
  }
}

