import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/printUtil.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/SO/soReq.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemOtherCostDetail.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Client/client_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/Sales/salesItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SalesSummaryForm.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:bdna_esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolStepperSOState =
    GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldSOKey = new GlobalKey<ScaffoldState>();

class SOPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const SOPage({Key key, this.repository, this.database, this.appLanguage})
      : super(key: key);

  @override
  _SOPageState createState() => _SOPageState(repository, database, appLanguage);
}

class _SOPageState extends State<SOPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  SavePRReq savePRReq = new SavePRReq();
  List<OtherCostDetails> otherCostDetailsList;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  CommonSalesItem itemForStepperUpdate;
  bool needToUpdateStepper = false;

  _SOPageState(this.repository, this.database, this.appLanguage);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  getStepWithSalesOrderForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? ClientForm(
          repository: repository,
          appLanguage: appLanguage,
          type: TransactionType.SO,
      )
          : ClientForm(
        repository: repository,
        appLanguage: appLanguage,
        type: TransactionType.SO,
      ),
      validation: () {
        if(Common().indexDraft != null){
          ClientForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SO,
          );
        }
        return null;
      },
    );
  }



  getStepWithSalesItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? SalesItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.SO,
             item: itemForStepperUpdate
            )
          : SalesItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.SO,
            ),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonSalesItemInfo.shared.cmnSalesItemList.isNotEmpty) {
          for(int index = 0; index < CommonSalesItemInfo.shared.cmnSalesItemList.length ; index++){
            CommonSalesItemInfo.shared.cmnSalesItemList[index].lineNum = index ;
          }
          SOReq.shared.fromJson(CommonSalesItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.SO);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSOKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
        return null;
      },
    );
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    cmnSalesClientInfo.clear();
    CommonSalesItemInfo.shared.cmnSalesItemList.clear();
    SOReq.shared.clear();
  }


  @override
  Widget build(BuildContext context) {

    final List<CoolStep> steps = [
      // CoolStep(
      //   title: "",
      //   subContent: SizedBox(height: 0),
      //   content: DraftDetailsForm(
      //       repository: repository,
      //       appLanguage: appLanguage,
      //       type: TransactionType.SO),
      // ),
      //   getStepWithSalesOrderForm(),
      CoolStep(
        title: "Select Client Details",
        subContent: SizedBox(height: 0),
        content: ClientForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SO),
        validation: () {
          // Validate Client details
          if (_validateClientForm(context: context)) {

            SOReq.shared.fromJson(cmnSalesClientInfo.toJson());
            if (SOReq.shared.expectedDeliveryDate == null)
              SOReq.shared.expectedDeliveryDate ="";
            setDraftTransaction(TransactionType.SO.value);
            _transBloc.add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithSalesItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SalesSummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.SO,
            navigateBackWithValueIndex: (int indexOfEditingItem) {

              if (CommonSalesItemInfo.shared.cmnSalesItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonSalesItemInfo.shared.cmnSalesItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperSOState.currentState.onStepBack();
                });
              }
            }),
        validation: () {
         return null;
        },

      ),
    ];

    final stepper = CoolStepper(
      key: coolStepperSOState,
      onCompleted: () {
        print("Steps completed!");

        if (SOReq.shared.salesOrderMaterialsListTemp.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSOKey, message: Constants.noItemAddedMessage);
          return;
        }

        SOReq.shared.fromJson(fromAddress.toJson());

       // Save SalesOrder
        _transBloc.add(SaveSalesOrder(req: json.encode(SOReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {

          if (state is SaveDraftTransactionInProgress ||
              state is SaveSalesOrderInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress
          )
          {
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);

          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
          }
          else if (state is SaveSalesOrderComplete) {
            Navigator.pop(context,true);

            Common().showAlertMessage(
               context: context,
               title: 'Success',
               message: state.saveSalesOrderResp.statusMessage,
               okFunction: () {
                 // resetTransactionData();

                 // _transBloc.add(DeleteDraftTransaction(
                 //     draftTransactionData: draftTransactionData)
                 // );
                 Navigator.pop(context,true);
               });
          } else if (
              state is UpdateDraftTransactionFailure ||
              state is SaveDraftTransactionFailure) {
            Navigator.pop(context, true);
          } else if (state is SaveSalesOrderFailure) {
            Navigator.pop(context, true);

            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
          else if (state is DeleteDraftTransactionComplete) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);

          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldSOKey,
              appBar: BCAppBar(
                  scaffoldKey: scaffoldSOKey,
                  title: 'Sales Order',
                  isBack: false,
                  appBar: AppBar(),
                  database: database,
                  appLanguage: appLanguage,
                  repository: repository,
                  shouldHideActions: false),
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: Container(
                child: stepper,
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(String type) {
    String prNo;
    if (SavePRReq.shared.pRetTypeId != 1) {
      prNo = SavePRReq.shared.pRetTypeRefNo.toString();
    } else {
      var uuid = Uuid();
      prNo = uuid.v1();
    }

    draftTransactionData = DraftTransactionData(
        trans_id: type + '_' + prNo,
        trans_type: type,
        trans_details: SavePRReq.shared.toJson().toString(),
        stepper_data1: cmnSalesClientInfo.toJson().toString(),
        stepper_data2: CommonSalesItemInfo.shared.cmnSalesItemList.toString(),
        stepper_data3: CommonSalesItemInfo.shared.cmnSalesItemList.toString(),
        stepper_data4: "",
        stepper_data5: "",
        stepper_data6: "");  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SOReq.shared.toJson().toString(),
          stepper_data1: cmnSalesClientInfo.toJson().toString(),
          stepper_data2: CommonSalesItemInfo.shared.cmnSalesItemList.toString(),
          stepper_data3: CommonSalesItemInfo.shared.cmnSalesItemList.toString(),
          stepper_data4: "",
          stepper_data5: "",
          stepper_data6: "",
      );
    }
  }

  bool _validateClientForm({BuildContext context}) {
   if (cmnSalesClientInfo.billingPartyType == null) {
     Common().showStepperValidation(
         scaffoldKey: scaffoldSOKey, message: "Please select Account Types");
     return false;
   } else if (cmnSalesClientInfo.billingPartyID == null) {
     String typeName = cmnSalesClientInfo.billingPartyTypeName.replaceAll("ts", "t");
     Common().showStepperValidation(
         scaffoldKey: scaffoldSOKey, message: "Please select " + typeName);
     return false;
   } else if (cmnSalesClientInfo.salesmanId == null) {
     Common().showStepperValidation(
         scaffoldKey: scaffoldSOKey, message: "Please select Salesman");
     return false;
   } else {
      return true;
   }
  }



}
