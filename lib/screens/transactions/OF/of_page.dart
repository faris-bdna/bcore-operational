import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/OF/saveOrderFormReq.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderFormInfo.dart';
import 'package:bcore_inventory_management/models/view/orderForm/orderItemInfo.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/OrderForm/order_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/OrderItem/order_itemEntry_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/OrderSummaryForm.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolSTStepperState = GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldOFKey = new GlobalKey<ScaffoldState>();

class OFPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  OFPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _OFPageState createState() => _OFPageState(repository, appLanguage, database);
}

class _OFPageState extends State<OFPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  DraftTransactionData draftTransactionData;
  TransBloc _transBloc;
  int savedDraftId;

  bool needToUpdateStepper = false;
  OrderItem itemForStepperUpdate;

  _OFPageState(
      this.repository,
      this.appLanguage,
      this.database,
      );

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData(){
    cmnOrderFormInfo.clear();
    OrderItemInfo.shared.orderItemList.clear();
    SaveOFReq.shared.clear();
    Utils.resetUniqueId();
  }

  getStepWithItemEntryForm() {
    return  CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? OrderItemEntryForm(database: database, repository: repository, appLanguage: appLanguage,item: itemForStepperUpdate)
          : OrderItemEntryForm(database: database, repository: repository, appLanguage: appLanguage),
      validation: () {

    /**
     * Checks whether the Items List is empty or not. If so, shows the validation message to add item
     */
        if (OrderItemInfo.shared.orderItemList.isNotEmpty) {
          SaveOFReq.shared.fromJson(OrderItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.OF);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(scaffoldKey: scaffoldOFKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  bool _validateOrderFormDetails({BuildContext context}) {
    if (cmnOrderFormInfo.entryDateTemp == null) {
      Common().showStepperValidation(scaffoldKey: scaffoldOFKey, message: 'Please enter the Transaction Date');
      return false;
    }
    else if (cmnOrderFormInfo.tranGenType == null) {
      Common().showStepperValidation(scaffoldKey: scaffoldOFKey, message: 'Please enter Transaction Generation Type');
      return false;
    }
    else if (cmnOrderFormInfo.tranGenType == TransGenType.PO.index && cmnOrderFormInfo.supplierId == null) {
      Common().showStepperValidation(scaffoldKey: scaffoldOFKey, message: "Please select Supplier");
      return false;
    }
    else if (cmnOrderFormInfo.toCompanyId == null) {
     Common().showStepperValidation(scaffoldKey: scaffoldOFKey, message: Constants.companyValidationMsg);
      return false;
    }
    else if (cmnOrderFormInfo.toDivisionId == null) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldOFKey, message: Constants.divisionValidationMsg);
      return false;
    }
    else if (cmnOrderFormInfo.toLocId == null) {
      Common().showStepperValidation(scaffoldKey: scaffoldOFKey,message: Constants.locationValidationMsg);
      return false;
    }
    SaveOFReq.shared.fromJson(fromAddress.toJson());
    SaveOFReq.shared.fromJson(cmnOrderFormInfo.toJson());
    return true;
  }

  @override
  Widget build(BuildContext cont) {
    BuildContext blocContext;

    final formKey = GlobalKey<FormState>();
    List<CoolStep> steps = [
      CoolStep(
        title: "",
        content:
        OrderForm(key:formKey,repository: repository, appLanguage: appLanguage, database: database),
        validation: () {

          // Validate Order Form details
          if (_validateOrderFormDetails(context: context)) {
            setDraftTransaction(TransactionType.OF);
            _transBloc.add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          }
          else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithItemEntryForm(),
      // getStepWithItemEntryFormNew(),

      // CoolStep(
      //   title: "Summary",
      //   subContent: Wrap(
      //     spacing: 8.0, // gap between adjacent chips
      //     runSpacing: 0.0, // gap between lines
      //     children: <Widget>[
      //       if(toAddress.companyName != null)
      //         Chip(
      //           avatar: Icon(Icons.location_city, size: 15),
      //           label: Text('${toAddress.companyName}', style: TextStyle(fontSize: 12)),
      //           backgroundColor: Colors.white,
      //         ),
      //       if(toAddress.divisionName != null)
      //         Chip(
      //           avatar: Icon(Icons.call_split, size: 15),
      //           label: Text('${toAddress.divisionName}', style: TextStyle(fontSize: 12)),
      //           backgroundColor: Colors.white,
      //         ),
      //       if(toAddress.locationName != null)
      //         Chip(
      //           avatar: Icon(Icons.location_searching, size: 15),
      //           label: Text('${toAddress.locationName}', style: TextStyle(fontSize: 12)),
      //           backgroundColor: Colors.white,
      //         ),
      //       if(toAddress.departmentName != null)
      //         Chip(
      //           avatar: Icon(Icons.dashboard, size: 15),
      //           label: Text('${toAddress.departmentName}', style: TextStyle(fontSize: 12)),
      //           backgroundColor: Colors.white,
      //         ),
      //       SizedBox()
      //     ],
      //   ),
      //   content: OrderSummaryForm(
      //       repository: repository,
      //       appLanguage: appLanguage,
      //       database: database,
      //       navigateBackWithValueIndex: (int indexOfEditingItem){
      //         if (OrderItemInfo.shared.orderItemList[indexOfEditingItem] != null) {
      //           setState(() {
      //             itemForStepperUpdate = OrderItemInfo.shared.orderItemList[indexOfEditingItem];
      //             needToUpdateStepper = true;
      //             coolSTStepperState.currentState.onStepBack();
      //           });
      //         }
      //       }),
      //   validation: () {
      //     return null;
      //   },
      // ),
    ];

    CoolStepper stepper = CoolStepper(
      key: coolSTStepperState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveOFReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldOFKey, message: Constants.noItemAddedMessage);
          return;
        }

        for (var index = 0; index < SaveOFReq.shared.itemDetails.length; index++) {
          SaveOFReq.shared.itemDetails[index].lineNum = index + 1;
        }

        SaveOFReq.shared.companyId = fromAddress.fromCompanyId;

        // Send OF
        print(json.encode(SaveOFReq.shared.toJson()));
        BlocProvider.of<TransBloc>(blocContext).add(SaveOF(ofReq: json.encode(SaveOFReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle: TextStyle(color: BCAppTheme().primaryColor,fontSize: 15),

      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0,right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {

          if (state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress ||
              state is SaveOfInProgress) {
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          }

          else if (state is SaveOfComplete) {
            Navigator.pop(context, true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();

                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          }
          else if (state is SaveOfFailure) {
            Navigator.pop(context, true);
            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is DeleteDraftTransactionComplete)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionComplete ) {
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionFailure ) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          _transBloc = BlocProvider.of<TransBloc>(context);
          blocContext = context;

          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldOFKey,
              appBar: BCAppBar(scaffoldKey:scaffoldOFKey,title: 'Order Form', isBack: false,appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(TransactionType type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type.value + '_' + uuid.v1(),
        trans_type: type.value,
        trans_details: SaveOFReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SaveOFReq.shared.toJson().toString());
    }
  }


}

