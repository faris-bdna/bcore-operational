import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/printUtil.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemOtherCostDetail.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnPaymentMethods.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnTandCInfo.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/AddCharge/addCharges_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Client/client_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/Sales/salesItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/PaymentMethod/paymentMethod_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SalesSummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/TermsAndConditions/termsAndConditions_form.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bdna_esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'package:bcore_inventory_management/models/request/sai/saveSAIReq.dart';

final GlobalKey<CoolStepperState> coolStepperSAIState =
    GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldSAIKey = new GlobalKey<ScaffoldState>();

class SAIPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const SAIPage({Key key, this.repository, this.database, this.appLanguage})
      : super(key: key);

  @override
  _SAIPageState createState() =>
      _SAIPageState(repository, database, appLanguage);
}

class _SAIPageState extends State<SAIPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  List<OtherCostDetails> otherCostDetailsList;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  CommonSalesItem itemForStepperUpdate;
  bool needToUpdateStepper = false;
  int indexOfItemDetailForm = 1;

  _SAIPageState(this.repository, this.database, this.appLanguage);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  getStepWithSalesItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? SalesItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.SAI,
              item: itemForStepperUpdate)
          : SalesItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.SAI,
            ),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonSalesItemInfo.shared.cmnSalesItemList.isNotEmpty) {
          /**
           * Adds Line no for each item
           */
          for (int index = 0;
              index < CommonSalesItemInfo.shared.cmnSalesItemList.length;
              index++) {
            CommonSalesItemInfo.shared.cmnSalesItemList[index].lineNum = index;
          }

          SaveSAIReq.shared.fromJson(CommonSalesItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.SAI);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSAIKey,
              message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    cmnSalesClientInfo.clear();
    CommonAddChargesInfo.shared.clear();
    CmnPaymentMethods.shared.clear();
    CommonTandCInfo.shared.clear();
    CommonSalesItemInfo.shared.cmnSalesItemList.clear();
    SaveSAIReq.shared.clear();
  }

  _getTotalInvoiceAmount() {
    double totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(
        0, (prevVal, item) => prevVal + double.parse(item.netCost ?? 0.0));
    return totalAmount.trim();
  }

  _getTotalAdditionalCharges() {
    double totalAmount = CommonAddChargesInfo.shared.cmnAddChargesList
        .fold(0, (prevVal, item) => prevVal + (item.netAmount ?? 0.0));
    return totalAmount.trim();
  }

  _getFinalTotalAmount() {
    double finalTotal = _getTotalInvoiceAmount() + _getTotalAdditionalCharges();
    return finalTotal;
  }

  @override
  Widget build(BuildContext context) {
    BuildContext blocContext;
    final List<CoolStep> steps = [
      CoolStep(
        title: "Select Client Details",
        subContent: SizedBox(height: 0),
        content: ClientForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SAI),
        validation: () {
          // Validate Client details
          if (_validateClientDetails(context: context)) {
            setDraftTransaction(TransactionType.SAI);
            BlocProvider.of<TransBloc>(blocContext).add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithSalesItemEntryForm(),
      CoolStep(
        title: "Additional Charges",
        subContent: SizedBox(height: 0),
        content: AddChargesForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SAI),
        validation: () {
          if (CommonAddChargesInfo.shared.cmnAddChargesList.isNotEmpty) {
            SaveSAIReq.shared.fromJson(CommonAddChargesInfo.shared.toJson());

            updateDraftTransaction(TransactionType.SAI);
            _transBloc.add(UpdateDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            return null;
          }
        },
      ),
      CoolStep(
        title: "Terms & Conditions",
        subContent: SizedBox(height: 0),
        content: TermsAndConditions(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SAI),
        validation: () {
          if (CommonTandCInfo.shared.termsAndConditionList.isNotEmpty) {
            SaveSAIReq.shared.fromJson(CommonTandCInfo.shared.toJson());

            setDraftTransaction(TransactionType.DN);
            _transBloc.add(UpdateDraftTransaction(
                draftTransactionData: draftTransactionData));
          }
          return null;
        },
      ),
      CoolStep(
        title: "Payment Method",
        subContent: SizedBox(height: 0),
        content: PaymentMethodForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SAI),
        validation: () {
          if (validatePayment()) {
            SaveSAIReq.shared.fromJson(CmnPaymentMethods.shared.toJson());

            updateDraftTransaction(TransactionType.SAI);
            _transBloc.add(UpdateDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSAIKey,
                message: "Payment not matching for the net Invoice Amount");
            return 'failed';
          }
        },
      ),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SalesSummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.SAI,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              if (CommonSalesItemInfo
                      .shared.cmnSalesItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonSalesItemInfo
                      .shared.cmnSalesItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperSAIState.currentState
                      .goToStep(index: indexOfItemDetailForm);
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolStepperSAIState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveSAIReq.shared.materialDetailsListTemp.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSAIKey,
              message: Constants.noItemAddedMessage);
          return;
        }

        for (var index = 0;
            index < SaveSAIReq.shared.materialDetailsListTemp.length;
            index++) {
          SaveSAIReq.shared.materialDetailsListTemp[index].itemLineNo =
              index + 1;
        }

        SaveSAIReq.shared.fromJson(fromAddress.toJson());
        SaveSAIReq.shared.sameAsBillingAdd = true;
        SaveSAIReq.shared.netAmount = _getFinalTotalAmount() ?? 0.0;

        BlocProvider.of<TransBloc>(blocContext)
            .add(SaveSAI(saiReq: json.encode(SaveSAIReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress ||
              state is SaveSaiInProgress) {
            Common().showLoader(context);
          } else if (state is SaveDraftTransactionFailure) {
            Navigator.pop(context, true);
          } else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          } else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
          }
          else if (state is SaveSaiComplete) {
            Navigator.pop(context, true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.statusMessage,
                okFunction: () async{

                  PrintUtil().printRecipt( "MLP 360", await saiReceipt());
                  resetTransactionData();

                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          } else if (state is SaveSaiFailure) {
            Navigator.pop(context, true);

            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          } else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          } else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          } else if (state is DeleteDraftTransactionComplete) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          blocContext = context;
          _transBloc = BlocProvider.of<TransBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(
                  context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: () {
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: () {
                    Navigator.pop(context);
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldSAIKey,
              appBar: BCAppBar(
                  scaffoldKey: scaffoldSAIKey,
                  title: 'Sales Invoice',
                  isBack: false,
                  appBar: AppBar(),
                  database: database,
                  appLanguage: appLanguage,
                  repository: repository,
                  shouldHideActions: false),
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: SafeArea(
                child: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(TransactionType type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: savedDraftId,
        trans_id: type.value + '_' + uuid.v1(),
        trans_type: type.value,
        trans_details: SaveSAIReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SaveSAIReq.shared.toJson().toString());
    }
  }

  bool _validateClientDetails({BuildContext context}) {
    if (cmnSalesClientInfo.entryDate.isEmpty) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldSAIKey, message: "Please select Entry Date");
      return false;
    } else if (cmnSalesClientInfo.billingPartyID == null) {
      String typeName =
          cmnSalesClientInfo.billingPartyTypeName.replaceAll("ts", "t");
      Common().showStepperValidation(
          scaffoldKey: scaffoldSAIKey, message: "Please select " + typeName);
      return false;
    } else if (cmnSalesClientInfo.billingPartyType == null) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldSAIKey,
          message: "Please select Billing Party Type");
      return false;
    } else if (cmnSalesClientInfo.salesmanId == null) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldSAIKey, message: "Please select Salesman");
      return false;
    } else {
      SaveSAIReq.shared.fromJson(cmnSalesClientInfo.toJson());
      return true;
    }
  }

  bool validatePayment() {
    double totalItemCost = CommonSalesItemInfo.shared.cmnSalesItemList
        .fold(0.0, (prevVal, item) => prevVal + item.netCost.toDouble());
    double totalAddCost = CommonAddChargesInfo.shared.cmnAddChargesList
        .fold(0.0, (prevVal, charge) => prevVal + charge.netAmount);

    double totalPayment = CmnPaymentMethods.shared.cmnPaymentMethodList
        .fold(0.0, (prevVal, payment) => prevVal + payment.amount);

    return (totalItemCost + totalAddCost).toDouble() == totalPayment.toDouble();
  }

  saiReceipt() async {
    // TODO Don't forget to choose printer's paper
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final Generator ticket = Generator(paper, profile);
    List<int> bytes = [];

    bytes += ticket.text('${fromAddress.fromCompanyName}',
        styles: PosStyles(
          align: PosAlign.center,
          // height: PosTextSize.size1,
          // width: PosTextSize.size2,
        ));

    bytes += ticket.text('${fromAddress.fromDivisionName}',
        styles: PosStyles(align: PosAlign.center));
    bytes += ticket.text('${fromAddress.fromLocationName}',
        styles: PosStyles(align: PosAlign.center));
    // bytes += ticket.text('Tel: 830-221-1234',
    //     styles: PosStyles(align: PosAlign.center));
    // bytes += ticket.text('Web: www.example.com',
    //     styles: PosStyles(align: PosAlign.center), linesAfter: 1);

    bytes += ticket.hr();
    bytes += ticket.row([
      PosColumn(text: 'Item', width: 5, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: 'Qty', width: 1, styles: PosStyles(align: PosAlign.center)),
      PosColumn(
          text: 'Price', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(text: 'Disc', width: 1, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'Total', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(text: 'Tax', width: 1, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.hr();
    CommonSalesItemInfo.shared.cmnSalesItemList.forEach((s) {

      bytes += ticket.row([
        PosColumn(text: '${s.itemName}', width: 5, styles: PosStyles(align: PosAlign.left)),
        PosColumn(text: '${s.quantity}', width: 1, styles: PosStyles(align: PosAlign.center)),
        PosColumn(
            text: '${s.rate}', width: 2, styles: PosStyles(align: PosAlign.right)),
        PosColumn(text: '${s.discount}', width: 1, styles: PosStyles(align: PosAlign.right)),
        PosColumn(
            text: '${s.netCost}', width: 2, styles: PosStyles(align: PosAlign.right)),
        PosColumn(text: '01', width: 1, styles: PosStyles(align: PosAlign.right)),
      ]);
    });

    bytes += ticket.hr();
    if(SaveSAIReq.shared.discount != 0.0)
      bytes += ticket.row([
        PosColumn(
            text: 'Gross Disc',
            width: 6,
            styles: PosStyles(
                align: PosAlign.center
            )),
        PosColumn(
            text: '${SaveSAIReq.shared.discount.trim().toString()}',
            width: 6,
            styles: PosStyles(
                align: PosAlign.right
            )),
      ]);

    bytes += ticket.row([
      PosColumn(
          text: 'TOTAL',
          width: 6,
          styles: PosStyles(
            height: PosTextSize.size2,
            width: PosTextSize.size2,
          )),
      PosColumn(
          text: '${_getTotalNetAmount()}',
          width: 6,
          styles: PosStyles(
            align: PosAlign.right,
            height: PosTextSize.size2,
            width: PosTextSize.size2,
          )),
    ]);

    bytes += ticket.hr(ch: '=', linesAfter: 1);
    bytes += ticket.row([
      PosColumn(text: 'VAT', width: 3),
      PosColumn(text: 'Rate', width: 3),
      PosColumn(
          text: 'Sales', width: 3, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'VAT', width: 3, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.row([
      PosColumn(text: '01', width: 3),
      PosColumn(text: '5%', width: 3),
      PosColumn(
          text: '${_getTotalNetAmount()}', width: 3, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: '${_getTotalVATAmount()}', width: 3, styles: PosStyles(align: PosAlign.right)),
    ]);
    bytes += ticket.hr();
    // bytes += ticket.row([
    //   PosColumn(
    //       text: 'Cash',
    //       width: 7,
    //       styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    //   PosColumn(
    //       text: '\$15.00',
    //       width: 5,
    //       styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    // ]);
    // bytes += ticket.row([
    //   PosColumn(
    //       text: 'Change',
    //       width: 7,
    //       styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    //   PosColumn(
    //       text: '\$4.03',
    //       width: 5,
    //       styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    // ]);

    bytes += ticket.feed(1);
    bytes += ticket.text('Thank you!',
        styles: PosStyles(align: PosAlign.center, bold: true));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy H:m');
    final String timestamp = formatter.format(now);
    bytes += ticket.text(timestamp,
        styles: PosStyles(align: PosAlign.center), linesAfter: 2);

    // Print QR Code from image
    // try {
    //   const String qrData = 'example.com';
    //   const double qrSize = 200;
    //   final uiImg = await QrPainter(
    //     data: qrData,
    //     version: QrVersions.auto,
    //     gapless: false,
    //   ).toImageData(qrSize);
    //   final dir = await getTemporaryDirectory();
    //   final pathName = '${dir.path}/qr_tmp.png';
    //   final qrFile = File(pathName);
    //   final imgFile = await qrFile.writeAsBytes(uiImg.buffer.asUint8List());
    //   final img = decodeImage(imgFile.readAsBytesSync());

    //   bytes += ticket.image(img);
    // } catch (e) {
    //   print(e);
    // }

    // Print QR Code using native function
    // bytes += ticket.qrcode('example.com');

    ticket.feed(2);
    return bytes;
  }


  _getTotalNetAmount() {
    double totalNetAmount = 0.0;
    totalNetAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + (item.netCost ?? '0.0').toDouble());

    return totalNetAmount .trim().toString();
  }

  _getTotalVATAmount() {
    double totalAmount = 0.0;
    double totalNetAmount = 0.0;
    double totalVATAmount = 0.0;
    totalNetAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + (item.netCost ?? '0.0').toDouble());
    totalAmount = CommonSalesItemInfo.shared.cmnSalesItemList.fold(0, (prevVal, item) => prevVal + (item.totalCost ?? '0.0').toDouble());
    totalVATAmount = totalNetAmount - totalAmount;

    return totalVATAmount.trim().toString();
  }

}
