import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/MTI/SaveTRReq.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/ItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/ToAddressForm.dart';
import 'package:flutter/material.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolMTIStepperState = GlobalKey<CoolStepperState>();

GlobalKey<ScaffoldState> scaffoldMTIKey = new GlobalKey<ScaffoldState>();

class MTIPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const MTIPage({Key key, this.repository, this.appLanguage, this.database}) : super(key: key);

  @override
  _MTIPageState createState() => _MTIPageState(repository,appLanguage,database);
}

class _MTIPageState extends State<MTIPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  _MTIPageState(this.repository, this.appLanguage, this.database);

  bool needToUpdateStepper = false;
  CommonItem itemForStepperUpdate;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    toAddress.clear();
    CommonItemViewModel.shared.cmnItemList.clear();
    SaveTRReq.shared.clear();
    Utils.resetUniqueId();
  }

  getStepWithItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? ItemEntryForm(
          database: database,
          repository: repository,
          type: TransactionType.MTI,
          appLanguage: appLanguage,
          item: itemForStepperUpdate)
          : ItemEntryForm(
          database: database,
          repository: repository,
          type: TransactionType.MTI,
          appLanguage: appLanguage),
      validation: () {

        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonItemViewModel.shared.cmnItemList.isNotEmpty) {
          SaveTRReq.shared.fromViewJson(CommonItemViewModel.shared.toJson());
          updateDraftTransaction(TransactionType.MTI.value);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldMTIKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    final List<CoolStep> steps = [
      CoolStep(
        title: "Select Details",
        subContent: SizedBox(height: 0),
        content: AddressForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.MTI),
        validation: () {
          if (toAddress.transactionDate == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldMTIKey, message: Constants.transactionDateValidationMsg);
            return Constants.transactionDateValidationMsg;
          } else if (toAddress.storeIssueId == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldMTIKey, message: Constants.refPlaceHolderText);
            return Constants.refPlaceHolderText;
          }

          SaveTRReq.shared.fromViewJson(toAddress.toJson());
          SaveTRReq.shared.toCompanyId = toAddress.toCompanyId;

          /// Generate draft transaction
          setDraftTransaction(TransactionType.MTI.value);

          /// Save material request entry to draft trans tablet
          _transBloc.add(
              SaveDraftTransaction(draftTransactionData: draftTransactionData));
          return null;
          },
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.MTI,
            navigateBackWithValueIndex: (int indexOfEditingItem) {

              if (CommonItemViewModel.shared.cmnItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonItemViewModel
                      .shared.cmnItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolMTIStepperState.currentState.onStepBack();
                });
                // needToUpdateStepper = false;
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolMTIStepperState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveTRReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldMTIKey, message: Constants.noItemAddedMessage);
          return;
        }

        /**
         * Adds Line no for each item & formats expiry date to company format
         */
        for (var index = 0; index < SaveTRReq.shared.itemDetails.length; index++) {
          SaveTRReq.shared.itemDetails[index].lineNum = index +1;
          if (SaveTRReq.shared.itemDetails[index].expiryDateTemp != null){
            SaveTRReq.shared.itemDetails[index].expiryDateTemp =
            Utils.getDateByFormat(
                passedDateTime:DateFormat(FromAddress.shared.fromCompanyDateFormat).parse(SaveTRReq.shared.itemDetails[index].expiryDateTemp),format: SaveTRReq.shared.dateFormat);
          }
        }

        /// Convert Entry date format to format from PRReferenceDetails
        SaveTRReq.shared.entryDateTemp = Utils.getDateByFormat(
            passedDateTime:DateFormat(FromAddress.shared.fromCompanyDateFormat).parse(SaveTRReq.shared.entryDateTemp),format: SaveTRReq.shared.dateFormat);


        // Save Transfer Receipt
        _transBloc.add(SaveTransferReceipt(req: json.encode(SaveTRReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle: TextStyle(color: BCAppTheme().primaryColor,fontSize: 15),

      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0,right: 16.0),
    );

    return BlocProvider<TransBloc>(
        create: (context) =>
        TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if(state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is SaveTransferReceiptInProgress ||
              state is DeleteDraftTransactionInProgress
          ){
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionComplete){
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context);
          }
          else if (state is SaveTransferReceiptComplete){
            Navigator.pop(context, true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message:
                state.resp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();

                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          }
          else if (state is UpdateDraftTransactionComplete){
            Navigator.pop(context);
          }
          else if (state is SaveTransferReceiptFailure){
            Navigator.pop(context);
            String errorMessage = state.error.replaceAll('<br/>', '\n\n');
            errorMessage = errorMessage.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: (){
                  Navigator.pop(context);
                });
          }
          else if (state is DeleteDraftTransactionFailure){
            Navigator.pop(context);
            Navigator.pop(context);
          }
          else if (state is UpdateDraftTransactionFailure){
            Navigator.pop(context);
          }
          else if(state is DeleteDraftTransactionComplete){
            Navigator.pop(context);
            Navigator.pop(context);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(
          builder: (context, state){
            _transBloc = BlocProvider.of<TransBloc>(context);

            return WillPopScope(
              onWillPop: () async{
                Common().showAlertMessageWithAction(context: context,
                    title: Constants.alertMsgTitle,
                    message: Constants.transactionClearMsg,
                    okButtonTitle: 'Ok',
                    okFunction: (){
                      resetTransactionData();
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    actionButtonTitle: 'Cancel',
                    actionFunction: (){
                      Navigator.pop(context);
                    });
                return null;
              },
              child: Scaffold(
                key: scaffoldMTIKey,
                appBar: BCAppBar(
                    scaffoldKey: scaffoldMTIKey,
                    title: 'Material Transfer In',
                    isBack: false,
                    appBar: AppBar(),
                    database: database,
                    appLanguage: appLanguage,
                    repository: repository,
                    shouldHideActions: false),
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Container(
                  child:stepper,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void setDraftTransaction(String type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type + '_' + uuid.v1(),
        trans_type: type,
        trans_details: SaveTRReq.shared.toJson().toString());
  }

  updateDraftTransaction(String type) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type + '_' + uuid.v1(),
          trans_type: type,
          trans_details: SaveTRReq.shared.toJson().toString());
    }
  }
}
