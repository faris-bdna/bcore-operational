import 'dart:convert';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:intl/intl.dart';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/grn/grnItemOtherCostDetail.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/highItem/common_highItem_details.dart';
import 'package:bcore_inventory_management/models/view/supplierDetails.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/High/highItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Supplier/supplier_Form.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolStepperPRState =
    GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldPRKey = new GlobalKey<ScaffoldState>();

class PRPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const PRPage({Key key, this.repository, this.database, this.appLanguage})
      : super(key: key);

  @override
  _PRPageState createState() => _PRPageState(repository, database, appLanguage);
}

class _PRPageState extends State<PRPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  SavePRReq savePRReq = new SavePRReq();
  List<OtherCostDetails> otherCostDetailsList;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  CommonHighItem itemForStepperUpdate;
  bool needToUpdateStepper = false;

  _PRPageState(this.repository, this.database, this.appLanguage);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  getStepWithHighItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? HighItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.PR,
              item: itemForStepperUpdate)
          : HighItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
              type: TransactionType.PR),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonHighItemInfo.shared.cmnHighItemList.isNotEmpty) {
          SavePRReq.shared.fromJson(CommonHighItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.PR);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        }
        else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldPRKey, message: Constants.noItemAddedMessage);

          return Constants.noItemAddedMessage;
        }
      },
    );
  }
  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    cmnSupplierInfo.clear();
    if (CommonHighItemInfo.shared.cmnHighItemList != null) {
      CommonHighItemInfo.shared.cmnHighItemList.clear();
    }
    SavePRReq.shared.clear();
    Utils.resetUniqueId();
  }

  @override
  Widget build(BuildContext context) {

    BuildContext blocContext;
    final List<CoolStep> steps = [
      CoolStep(
        title: "Select Supplier Details",
        subContent: SizedBox(height: 0),
        content: SupplierForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.PR),
        validation: () {
          // Validate supplier details
          if (_validateSupplier(context: context)) {
            setDraftTransaction(TransactionType.PR.value);
            BlocProvider.of<TransBloc>(blocContext).add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithHighItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.PR,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              /*  if (indexOfEditingItem == null){
                needToUpdateStepper = false;
                coolStepperPRState.currentState.onStepBack();
              } else */
              if (CommonHighItemInfo
                      .shared.cmnHighItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonHighItemInfo
                      .shared.cmnHighItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperPRState.currentState.onStepBack();
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolStepperPRState,
      onCompleted: () {
        print("Steps completed!");

        if (SavePRReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldPRKey, message: Constants.noItemAddedMessage);
          return;
        }

        /**
         * Adds Line no for each item
         */
        for (var index = 0;
            index < SavePRReq.shared.itemDetails.length;
            index++) {
          SavePRReq.shared.itemDetails[index].lineNum = index+1;
        }

        _bindAddressToPR();

        if (cmnSupplierInfo.pRetTypeId == (PRType.GRN.index + 1))
        {
          /// Convert Entry date format to format from PRReferenceDetails
          SavePRReq.shared.entryDateTemp = Utils.getDateByFormat(
              passedDateTime: DateFormat(
                  FromAddress.shared.fromCompanyDateFormat).parse(
                  SavePRReq.shared.entryDateTemp),
              format: SavePRReq.shared.dateFormat);
        }
        // Send PR
        BlocProvider.of<TransBloc>(blocContext)
            .add(SavePR(prReq: json.encode(SavePRReq.shared.toJson())));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 0.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is SaveDraftTransactionInProgress ||
              state is SavePrInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress) {
            Common().showLoader(context);
          } else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
          }
          else if (state is SavePrComplete) {
            Navigator.pop(context, true);

            // CommonHighItemInfo.shared.clear();
            // SavePRReq.shared.clear();

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();

                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          }
          else if (state is SavePrFailure){
            Navigator.pop(context,true);

            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
          else if (state is DeleteDraftTransactionComplete)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          blocContext = context;
          _transBloc = BlocProvider.of<TransBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldPRKey,
              appBar: BCAppBar(
                  scaffoldKey: scaffoldPRKey,
                  title: 'Purchase Return',
                  isBack: false,
                  appBar: AppBar(),
                  database: database,
                  appLanguage: appLanguage,
                  repository: repository,
                  shouldHideActions: false),
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: Container(
                child: stepper,
              ),
            ),
          );
        }),
      ),
    );
  }

  void _bindAddressToPR() {
    SavePRReq.shared.companyId = int.tryParse(fromAddress.fromCompanyId ?? '');
    SavePRReq.shared.divisionId = int.tryParse(fromAddress.fromDivisionId ?? '');
    SavePRReq.shared.locId = int.tryParse(fromAddress.fromLocationId ?? '');
    SavePRReq.shared.departmentId = int.tryParse(fromAddress.fromDepartmentId ?? '');
    SavePRReq.shared.projectId = null;
//    SavePRReq.shared.discPercentage = 0.0;
  }

  void setDraftTransaction(String type) {
    String prNo;
    if (SavePRReq.shared.pRetTypeId != 1) {
      prNo = SavePRReq.shared.pRetTypeRefNo.toString();
    } else {
      var uuid = Uuid();
      prNo = uuid.v1();
    }

    draftTransactionData = DraftTransactionData(
        trans_id: type + '_' + prNo,
        trans_type: type,
        trans_details: SavePRReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SavePRReq.shared.toJson().toString());
    }
  }

  bool _validateSupplier({BuildContext context}) {
    if ((cmnSupplierInfo.pRetTypeId ==
            2) && // When GRN Type is GRN  and user has not entered the reference no
        (cmnSupplierInfo.pRetTypeRefNo == null)) {
      // showMessage(context: context, message: "Please enter Reference no");
      Common().showStepperValidation(
          scaffoldKey: scaffoldPRKey, message: "Please enter Reference no");
      return false;
    } else if (cmnSupplierInfo.entryDateTemp.isEmpty) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldPRKey,
          message: "Please select Purchase Return date");
      return false;
    } else if (cmnSupplierInfo.pRetTypeId == null ||
        cmnSupplierInfo.pRetTypeId == 0) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldPRKey,
          message: "Please select Purchase Return Type");
      return false;
    } else if (cmnSupplierInfo.priceTypeId == null ||
        cmnSupplierInfo.priceTypeId == 0) {
      // showMessage(context: context, message: "Please select Price Type");
      Common().showStepperValidation(
          scaffoldKey: scaffoldPRKey,
          message: "Please select Price Type");

      return false;
    } else if (cmnSupplierInfo.suppId == null || cmnSupplierInfo.suppId == 0) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldPRKey,
          message: "Please select Supplier");
      return false;
    } else {
      SavePRReq.shared.fromJson(cmnSupplierInfo.toJson());
      return true;
    }
  }
}
