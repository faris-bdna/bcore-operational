import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/request/mrReq.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'mr_page_event.dart';

part 'mr_page_state.dart';

class MrPageBloc extends Bloc<MrPageEvent, MrPageState> {
  final BCRepository repository;
  final Database database;

  MrPageBloc({@required this.repository, @required this.database}) : assert(repository != null), super(SendMaterialRequestInitial());

  @override
  Stream<MrPageState> mapEventToState(MrPageEvent event) async* {
    MRResp matReqResp;
    int draftTransactionResp;
    int draftTransactionData;
    List<DraftTransactionData> drafts;
    bool updateTransactionResp;
    if(event is SaveDraftTransaction) {
      yield SaveDraftTransactionInProgress();

      try {
        draftTransactionData = await database.draftTransactionDao.insertDraftTransaction(event.draftTransactionData);
        yield SaveDraftTransactionComplete(savedTransactionId: draftTransactionData);
      } catch (error) {
        yield SaveDraftTransactionFailure(error: error.toString());
      }
    }

    if(event is UpdateDraftTransaction) {
      yield UpdateDraftTransactionInProgress();

      try {
        updateTransactionResp = await database.draftTransactionDao.updateDraftTransaction(event.draftTransactionData);
        yield UpdateDraftTransactionComplete(updateTransactionResp: updateTransactionResp);
      } catch (error) {
        yield UpdateDraftTransactionFailure(error: error.toString());
      }
    }

    else if(event is DeleteDraftTransaction) {
      yield DeleteDraftTransactionInProgress();

      try {
        draftTransactionResp = await database.draftTransactionDao.deleteDraftTransaction(event.draftTransactionData);
//        drafts = await database.draftTransactionDao.getAllDraftTransaction();
        yield DeleteDraftTransactionComplete( deleteDraftTransactionResp: draftTransactionResp);
      } catch (error) {
        yield DeleteDraftTransactionFailure(error: error.toString());
      }
    }

    if(event is SendMaterialRequest) {
      yield SendMaterialRequestInProgress();

      try {
         matReqResp = await repository.sendMaterialRequest(event.materialRequest);
        yield SendMaterialRequestComplete(saveMaterialReqResp: matReqResp);
      } catch (error) {
        yield SendMaterialRequestFailure(error: error.toString());
      }
    }

  }
}