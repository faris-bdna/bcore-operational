part of 'mr_page_bloc.dart';

abstract class MrPageEvent extends Equatable {
  const  MrPageEvent();
}
class SaveDraftTransaction extends MrPageEvent {
  final DraftTransactionData draftTransactionData;
  const SaveDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'SaveDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class UpdateDraftTransaction extends MrPageEvent {
  final DraftTransactionData draftTransactionData;
  const UpdateDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'UpdateDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class DeleteDraftTransaction extends MrPageEvent {
  final DraftTransactionData draftTransactionData;
  const DeleteDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'DeleteDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class SendMaterialRequest extends MrPageEvent {
  final MRReq materialRequest;
  const SendMaterialRequest({@required this.materialRequest});

  @override
  List<Object> get props => [materialRequest];

  @override
  String toString() => 'SendMaterialRequest {"MaterialRequest" : "$materialRequest"}';
}
