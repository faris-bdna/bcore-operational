import 'dart:io';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/mrReq.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/ItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/ToAddressForm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'dart:convert';
import 'mr_page_bloc.dart';

final GlobalKey<CoolStepperState> coolStepperState = GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldMRKey = new GlobalKey<ScaffoldState>();
class MRPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  MRPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _MRPageState createState() => _MRPageState(repository, appLanguage, database);
}

class _MRPageState extends State<MRPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  File toAddressJson;
  File matReqDetJson;
  Directory dir;
  String fileName = "to_address.json";
  String matReqFileName = "mat_req.json";
  bool fileExists = false;
  Map<String, dynamic> fileContent;
  DraftTransactionData draftTransactionData;
  MrPageBloc _mrPageBloc;

  bool needToUpdateStepper = false;
  CommonItem itemForStepperUpdate;
  _MRPageState(
      this.repository,
      this.appLanguage,
      this.database,
      );

  @override
  void initState() {
    super.initState();

   resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData(){
    toAddress.clear();
    CommonItemViewModel.shared.cmnItemList.clear();
    MRReq.shared.clear();
  }

  getStepWithItemEntryForm() {
    return  CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? ItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.MR, item: itemForStepperUpdate, editMode: EditMode.On)
          : ItemEntryForm(database: database, repository: repository, appLanguage: appLanguage, type: TransactionType.MR, editMode: EditMode.Off,),
      validation: () {

        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if ( CommonItemViewModel.shared.cmnItemList.isNotEmpty)
        {
          MRReq.shared.fromViewJson(CommonItemViewModel.shared.toJson());
          updateDraftTransaction(TransactionType.MR.value);
          _mrPageBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        }
        else {
          Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.noItemAddedMessage);

          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  @override
  Widget build(BuildContext cont) {

    final formKey = GlobalKey<FormState>();
    List<CoolStep> steps = [
      CoolStep(
        title: "To Address",
        // subContent: Text("Enter your To Company Address"),
        content:
        AddressForm(key:formKey,repository: repository, appLanguage: appLanguage,
            type: TransactionType.MR),
        validation: () {
          if (toAddress.transactionDate ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.transactionDateValidationMsg);
            return Constants.transactionDateValidationMsg;
          } else if (toAddress.expectedReceiveDate ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.expReceiveDateValidationMsg);
            return Constants.expReceiveDateValidationMsg;
          }  else if (toAddress.deliveryPriority ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.priorityValidationMsg);
            return Constants.priorityValidationMsg;
          }else if (toAddress.companyId ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message:Constants.companyValidationMsg);
            return Constants.companyValidationMsg;
          } else if (toAddress.divisionId ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.divisionValidationMsg);
            return Constants.divisionValidationMsg;
          } else if (toAddress.locationId ==null) {
            Common().showStepperValidation(scaffoldKey: scaffoldMRKey,message: Constants.locationValidationMsg);
            return Constants.locationValidationMsg;
          }

           MRReq.shared.fromViewJson(toAddress.toJson());
           MRReq.shared.fromViewJson(fromAddress.toJson());

          /// Generate draft transaction
          setDraftTransaction(TransactionType.MR.value);
          /// Save material request entry to draft trans tablet
          _mrPageBloc.add(SaveDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        },
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if(toAddress.companyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${toAddress.companyName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.divisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${toAddress.divisionName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.locationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${toAddress.locationName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if(toAddress.departmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${toAddress.departmentName}', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.MR,
            navigateBackWithValueIndex: (int indexOfEditingItem){
              if ( MRReq.shared.itemDetails[indexOfEditingItem] != null) {
                setState(() {
                  itemForStepperUpdate = MRReq.shared.itemDetails[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolStepperState.currentState.onStepBack();
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    CoolStepper stepper = CoolStepper(
      key: coolStepperState,
      onCompleted: () {
        print("Steps completed!");

        if (MRReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldMRKey, message: Constants.noItemAddedMessage);
          return;
        }

        /**
         * Adds Line no for each item
         */
        for (var index = 0; index < MRReq.shared.itemDetails.length; index++) {
          MRReq.shared.itemDetails[index].LineNum = index + 1;
        }
        _mrPageBloc.add(SendMaterialRequest(materialRequest: MRReq.shared));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle: TextStyle(color: BCAppTheme().primaryColor,fontSize: 15),

      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0,right: 16.0),
    );

    return BlocProvider<MrPageBloc>(
      create: (context) =>
          MrPageBloc(repository: repository, database: database),
      child: BlocListener<MrPageBloc, MrPageState>(
        listener: (context, state) {
          if (state is SendMaterialRequestInProgress ||
              state is SaveDraftTransactionInProgress ||
              state is DeleteDraftTransactionInProgress)
          {
            Common().showLoader(context);
          }
          else if (state is SendMaterialRequestComplete)
          {
            Navigator.pop(context,true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.saveMaterialReqResp.validationDetails.statusMessage,
                okFunction: () {
                  resetTransactionData();

                  _mrPageBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context,true);
                });
          }
          else if (state is DeleteDraftTransactionComplete)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }
          else if (state is SaveDraftTransactionComplete)
          {
            Navigator.pop(context,true);

            print("${state.savedTransactionId}");
            if (state.savedTransactionId != null) {
              Common().setTransactionId(state.savedTransactionId);
            }
          }

          else if (state is SendMaterialRequestFailure)
          {
            Navigator.pop(context,true);
            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }

          else if (state is DeleteDraftTransactionFailure)
          {
            Navigator.pop(context,true);
            Navigator.pop(context,true);
          }

          else if (state is SaveDraftTransactionFailure){
            Navigator.pop(context,true);
          }
        },
        child: BlocBuilder<MrPageBloc, MrPageState>(builder: (context, state) {
          _mrPageBloc = BlocProvider.of<MrPageBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldMRKey,
              appBar: BCAppBar(scaffoldKey:scaffoldMRKey,
                title: 'Material Request',
                isBack: false,appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(String type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type + '_' + uuid.v1(),
        trans_type: type,
        trans_details: MRReq.shared.toJson().toString());
  }

  updateDraftTransaction(String type) async{
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type + '_' + uuid.v1(),
          trans_type: type,
          trans_details: MRReq.shared.toJson().toString());
    }
  }
}

getToAddressPref(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(key)) {
    return prefs.getString(key);
  } else {
    return null;
  }
}
