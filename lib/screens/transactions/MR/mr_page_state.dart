part of 'mr_page_bloc.dart';

abstract class MrPageState extends Equatable {
  const MrPageState();

  @override
  List<Object> get props => [];
}

class SaveDraftTransactionInitial extends MrPageState {}

class SaveDraftTransactionInProgress extends MrPageState {}

class SaveDraftTransactionComplete extends MrPageState {
  final int savedTransactionId;

  SaveDraftTransactionComplete({@required this.savedTransactionId}) : assert(savedTransactionId != null);

  @override
  List<Object> get props => [savedTransactionId];
}

class SaveDraftTransactionFailure extends MrPageState {
  final String error;

  const SaveDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}


class UpdateDraftTransactionInitial extends MrPageState {}

class UpdateDraftTransactionInProgress extends MrPageState {}

class UpdateDraftTransactionComplete extends MrPageState {
  final bool updateTransactionResp;

  UpdateDraftTransactionComplete({@required this.updateTransactionResp}) : assert(updateTransactionResp != null);

  @override
  List<Object> get props => [updateTransactionResp];
}

class UpdateDraftTransactionFailure extends MrPageState {
  final String error;

  const UpdateDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'UpdateDraftTransactionFailure { error : $error }';
}


class SendMaterialRequestInitial extends MrPageState {}

class SendMaterialRequestInProgress extends MrPageState {}

class SendMaterialRequestComplete extends MrPageState {
  final MRResp saveMaterialReqResp;

  SendMaterialRequestComplete({@required this.saveMaterialReqResp}) : assert(saveMaterialReqResp != null);

  @override
  List<Object> get props => [saveMaterialReqResp];
}

class SendMaterialRequestFailure extends MrPageState {
  final String error;

  const SendMaterialRequestFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendMaterialRequestFailure { error : $error }';
}


class DeleteDraftTransactionInitial extends MrPageState {}

class DeleteDraftTransactionInProgress extends MrPageState {}

class DeleteDraftTransactionComplete extends MrPageState {
  final int deleteDraftTransactionResp;

  DeleteDraftTransactionComplete({@required this.deleteDraftTransactionResp}) : assert(deleteDraftTransactionResp != null);

  @override
  List<Object> get props => [deleteDraftTransactionResp];
}

class DeleteDraftTransactionFailure extends MrPageState {
  final String error;

  const DeleteDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DeleteDraftTransactionFailure { error : $error }';
}
