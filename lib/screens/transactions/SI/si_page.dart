import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/models/view/toAddress.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/ItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ToAddress/ToAddressForm.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolSIStepperState =
GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldSIKey = new GlobalKey<ScaffoldState>();

class SIPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const SIPage({Key key, this.repository, this.appLanguage, this.database})
      : super(key: key);

  @override
  _SIPageState createState() => _SIPageState(repository, appLanguage, database);
}

class _SIPageState extends State<SIPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  bool needToUpdateStepper = false;
  CommonItem itemForStepperUpdate;
  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  _SIPageState(this.repository, this.appLanguage, this.database);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    toAddress.clear();
    CommonItemViewModel.shared.cmnItemList.clear();
    SIReq.shared.clear();
    sub_details.clear();
  }

  getStepWithItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? ItemEntryForm(
          database: database,
          repository: repository,
          appLanguage: appLanguage,
          type: TransactionType.SI,
          editMode: EditMode.On,
          item: itemForStepperUpdate)
          : ItemEntryForm(
          database: database,
          repository: repository,
          appLanguage: appLanguage,
          type: TransactionType.SI,
          editMode: EditMode.Off),

      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonItemViewModel.shared.cmnItemList.isNotEmpty) {

          SIReq.shared.fromViewJson(CommonItemViewModel.shared.toJson());
          updateDraftTransaction(TransactionType.SI.value);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSIKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<CoolStep> steps = [
      CoolStep(
        title: "To Address",
        content: AddressForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.SI),
        validation: () {

          if (toAddress.transferTypeId == SIType.From_MR.index && toAddress.storeIssueId == null ){
            Common().showStepperValidation(
                scaffoldKey: scaffoldSIKey, message: Constants.referenceValidationMsg);
            return Constants.referenceValidationMsg;
          }
          else if (toAddress.transactionDate == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSIKey, message: Constants.transactionDateValidationMsg);
            return Constants.transactionDateValidationMsg;
          }
          else if (toAddress.companyId == null && toAddress.companyName == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSIKey, message: Constants.companyValidationMsg);
            return Constants.companyValidationMsg;
          }
          else if (toAddress.divisionId == null && toAddress.divisionName == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSIKey, message: Constants.divisionValidationMsg);
            return Constants.divisionValidationMsg;
          }
          else if (toAddress.locationId == null && toAddress.locationName == null) {
            Common().showStepperValidation(
                scaffoldKey: scaffoldSIKey, message: Constants.locationValidationMsg);
            return Constants.locationValidationMsg;
          }

          SIReq.shared.fromViewJson(toAddress.toJson());
          SIReq.shared.fromViewJson(fromAddress.toJson());
          SIReq.shared.issueTypeId = toAddress.transferTypeId + 1; /// In Our screens we are using enums (direct & from mr) and its' indexing starts from 0.
                                                                   /// In API, indexing starts from 1.

          /// Generate draft transaction
          setDraftTransaction(TransactionType.SI.value);

          /// Save Store Issue request entry to draft trans tablet
          _transBloc.add(SaveDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        },
      ),
      getStepWithItemEntryForm(),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (toAddress.companyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${toAddress.companyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.divisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${toAddress.divisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.locationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${toAddress.locationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (toAddress.departmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${toAddress.departmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.SI,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              if (CommonItemViewModel.shared.cmnItemList[indexOfEditingItem] !=
                  null) {
                setState(() {
                  itemForStepperUpdate = CommonItemViewModel
                      .shared.cmnItemList[indexOfEditingItem];
                  needToUpdateStepper = true;
                  coolSIStepperState.currentState.onStepBack();
                });
                // needToUpdateStepper = false;
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolSIStepperState,
      onCompleted: () {
        print(SIReq.shared.toJson());

        if (SIReq.shared.itemDetails.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldSIKey, message: Constants.noItemAddedMessage);
          return;
        }

        /// Bind substitute details if any

        if(sub_details.isNotEmpty) {
          SIReq.shared.substituteItemDetails = [];
          SIReq.shared.substituteItemDetails.addAll([...sub_details]);
        }

        /// Checks for same item with same packing. When found combine those item quantity to create a single item with total quantity

        List<ItemDetails> itemDetails = [];
        for (ItemDetails itemDt in SIReq.shared.itemDetails) {
          if  (itemDetails.where((element) => (element.itemCode == itemDt.itemCode) && (element.packingId == itemDt.packingId)).toList().isEmpty) {
            /// Finds duplicates if any
            List<ItemDetails> items = SIReq.shared.itemDetails.where((item) =>
            (item.itemCode == itemDt.itemCode &&
                item.packingId == itemDt.packingId)).toList();

            if (items.length > 1) {
              double totalQty = items.fold(
                  0, (prevValue, item) => prevValue + item.unitQty);
              items[0].unitQty = totalQty;
              itemDetails.add(items[0]);
            } else {
              itemDetails.add(itemDt);
            }
          }
        }
        SIReq.shared.itemDetails = itemDetails;

        /**
         * Adds Line no for each item
         */
        for (var index = 0; index < SIReq.shared.itemDetails.length; index++) {
          SIReq.shared.itemDetails[index].lineNum = index + 1;
        }

        _transBloc.add(SaveStoreIssue(siReq: SIReq.shared));
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
        TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
        create: (context) =>
            TransBloc(repository: repository, database: database),
        child: BlocListener<TransBloc, TransState>(
          listener: (context, state) {
            if (state is SaveDraftTransactionInProgress ||
                state is SaveStoreIssueInProgress ||
                state is UpdateDraftTransactionInProgress ||
                state is DeleteDraftTransactionInProgress
            ) {
              Common().showLoader(context);
            }
            else if (state is SaveDraftTransactionComplete) {
              savedDraftId = state.savedTransactionId;
              Common().setTransactionId(savedDraftId);
              Navigator.pop(context, true);
            }
            else if (state is SaveStoreIssueComplete) {
              Navigator.pop(context, true);

              Common().showAlertMessage(
                  context: context,
                  title: 'Success',
                  message:
                  state.saveStoreIssueResp.validationDetails.statusMessage,
                  okFunction: () {
                    resetTransactionData();

                    _transBloc.add(DeleteDraftTransaction(
                        draftTransactionData: draftTransactionData));
                    Navigator.pop(context, true);
                  });
            }
            else if (state is UpdateDraftTransactionComplete) {
              Navigator.pop(context, true);
            }
            else if (state is SaveStoreIssueFailure) {
              Navigator.pop(context, true);
              String errorMessage = state.error.replaceAll('<br>', '');
              errorMessage = errorMessage.replaceAll('Exception:', '');
              Common().showAlertMessage(
                  context: context,
                  title: "Sorry",
                  message: errorMessage,
                  okFunction: () {
                    Navigator.pop(context);
                  });
            }
            else if (state is DeleteDraftTransactionComplete)
            {
              Navigator.pop(context,true);
              Navigator.pop(context,true);
            }
            else if (state is DeleteDraftTransactionFailure) {
              Navigator.pop(context, true);
              Navigator.pop(context, true);
            }
            else if (state is UpdateDraftTransactionFailure) {
              Navigator.pop(context, true);
            }
          },
          child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
            _transBloc = BlocProvider.of<TransBloc>(context);
            return WillPopScope(
              onWillPop: () async {
                Common().showAlertMessageWithAction(context: context,
                    title: Constants.alertMsgTitle,
                    message: Constants.transactionClearMsg,
                    okButtonTitle: 'Ok',
                    okFunction: (){
                      resetTransactionData();
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    actionButtonTitle: 'Cancel',
                    actionFunction: (){
                      Navigator.pop(context);
                    });
                return null;
              },
              child: Scaffold(
                key: scaffoldSIKey,
                appBar: BCAppBar(
                  title: 'Store Issue',
                  scaffoldKey: scaffoldSIKey,
                  isBack: false,
                  appBar: AppBar(),
                  database: database,
                  appLanguage: appLanguage,
                  repository: repository,
                  shouldHideActions: false,
                ),
                body: Scaffold(
                  //key: scaffoldKey,
                  drawer: NavigationDrawer(
                    context: context,
                    repository: repository,
                    appLanguage: appLanguage,
                    database: database,
                  ),
                  body: Container(
                    child: stepper,
                  ),
                ),
              ),
            );
          }),
        ));
  }

  void setDraftTransaction(String type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: null,
        trans_id: type + '_' + uuid.v1(),
        trans_type: type,
        trans_details: SIReq.shared.toJson().toString());
  }

  updateDraftTransaction(String type) async {
    var uuid = Uuid();
    int transactionId = await Common().getTransactionId();
    if (transactionId != null) {
      draftTransactionData = DraftTransactionData(
          id: transactionId,
          trans_id: type + '_' + uuid.v1(),
          trans_type: type,
          trans_details: SIReq.shared.toJson().toString());
    }
  }
}
