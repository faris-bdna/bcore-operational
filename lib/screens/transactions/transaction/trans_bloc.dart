import 'dart:async';

import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/models/request/II/iiReq.dart';
import 'package:bcore_inventory_management/models/request/OB/saveOpeningBalanceReq.dart';
import 'package:bcore_inventory_management/models/request/SR/srReq.dart';
import 'package:bcore_inventory_management/models/request/mtoReq.dart';
import 'package:bcore_inventory_management/models/request/pr/SavePurchaseReturnRequest.dart';
import 'package:bcore_inventory_management/models/request/siReq.dart';
import 'package:bcore_inventory_management/models/request/st/saveStockTakingReq.dart';
import 'package:bcore_inventory_management/models/response/OrderItem/SaveOfResp.dart';
import 'package:bcore_inventory_management/models/response/SalesItem/getItemPriceDetailsResp.dart';
import 'package:bcore_inventory_management/models/response/mrResp.dart';
import 'package:bcore_inventory_management/models/saveGrn_resp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:bcore_inventory_management/models/response/SalesOrder/saveSalesOrderResp.dart';

part 'trans_event.dart';

part 'trans_state.dart';

class TransBloc extends Bloc<TransEvent, TransState> {
  final BCRepository repository;
  final Database database;

  TransBloc(
      {@required this.repository,
      @required this.database})
      : assert(repository != null),
        super(SaveDraftTransactionInitial());

  @override
  Stream<TransState> mapEventToState(TransEvent event) async* {
    int transactionResp;

    int draftTransactionResp;
    bool updateDraftTransactionResp;
    List<DraftTransactionData> getDraftTranId;
    List<DraftTransactionData> getDraftTran;


    if (event is SaveDraftTransaction) {
      yield SaveDraftTransactionInProgress();

      try {
        draftTransactionResp = await database.draftTransactionDao
            .insertDraftTransaction(event.draftTransactionData);
        yield SaveDraftTransactionComplete(
            savedTransactionId: draftTransactionResp);
      } catch (error) {
        yield SaveDraftTransactionFailure(error: error.toString());
      }
    }

    else if (event is UpdateDraftTransaction) {
      yield UpdateDraftTransactionInProgress();

      try {
        updateDraftTransactionResp = await database.draftTransactionDao
            .updateDraftTransaction(event.draftTransactionData);

        yield UpdateDraftTransactionComplete(
            updateDraftTransactionResp: updateDraftTransactionResp);
      } catch (error) {
        yield UpdateDraftTransactionFailure(error: error.toString());
      }
    }

    else if (event is GetDraftTransactionId) {
      yield GetDraftTransactionIdInProgress();

      try {
        getDraftTranId =
        await database.draftTransactionDao.getDraftId(event.draftType);
        yield GetDraftTransactionIdComplete(
            getDraftTransactionIdResp: getDraftTranId);
      } catch (error) {
        yield GetDraftTransactionIdFailure(error: error.toString());
      }
    }

    else if (event is GetDraftTransactionType) {
      yield GetDraftTransactionTypeInProgress();

      try {
        getDraftTran =
        await database.draftTransactionDao.getDraftId(event.draftType);
        yield GetDraftTransactionTypeComplete(
            getDraftTransactionResp: getDraftTran);
      } catch (error) {
        yield GetDraftTransactionTypeFailure(error: error.toString());
      }
    }

    else if (event is DeleteDraftTransaction) {
      yield DeleteDraftTransactionInProgress();

      try {
        transactionResp =
        await database.draftTransactionDao.deleteDraftTransaction(
            event.draftTransactionData);
        yield DeleteDraftTransactionComplete(
            deleteDraftTransactionResp: transactionResp);
      } catch (error) {
        yield DeleteDraftTransactionFailure(error: error.toString());
      }
    }

    else if (event is SaveGrn) {
      yield SaveGrnInProgress();
      try {
        final SaveGrnResp saveGrnResp =
        await repository.postGrn(grnReq: event.grnReq);
        yield SaveGrnComplete(saveGrnResp: saveGrnResp);
      } catch (error) {
        yield SaveGrnFailure(error: error.toString());
      }
    }

    else if (event is SavePR) {
      yield SavePrInProgress();
      try {
        final SaveGrnResp saveGrnResp =
        await repository.postPr(prReq: event.prReq);
        yield SavePrComplete(resp: saveGrnResp);
      } catch (error) {
        yield SavePrFailure(error: error.toString());
      }
    }

    else if (event is SaveST) {
      yield SaveSTInProgress();
      try {
        final SaveGrnResp saveGrnResp =
        await repository.saveStockTaking(req: event.stReq);
        yield SaveSTComplete(resp: saveGrnResp);
      } catch (error) {
        yield SaveSTFailure(error: error.toString());
      }
    }

    else if (event is SaveOB) {
      yield SaveOBInProgress();
      try {
        final SaveGrnResp saveGrnResp =
        await repository.saveOpeningBalance(req: event.obReq);
        yield SaveOBComplete(resp: saveGrnResp);
      } catch (error) {
        yield SaveOBFailure(error: error.toString());
      }
    }

    else if (event is SaveSAI) {
      yield SaveSaiInProgress();
      try {
        final SaveSalesResp saveGrnResp =
        await repository.saveSAI(saiReq: event.saiReq);
        yield SaveSaiComplete(resp: saveGrnResp);
      } catch (error) {
        yield SaveSaiFailure(error: error.toString());
      }
    }

    else if (event is SaveOF) {
      yield SaveOfInProgress();
      try {
        final SaveOFResp saveOFResp =
        await repository.saveOF(ofReq: event.ofReq);
        yield SaveOfComplete(resp: saveOFResp);
      } catch (error) {
        yield SaveOfFailure(error: error.toString());
      }
    }

    else if (event is SaveStoreIssue) {
      yield SaveStoreIssueInProgress();

      try {
        final MRResp resp = await repository.saveStoreIssue(event.siReq);
        yield SaveStoreIssueComplete(saveStoreIssueResp: resp);
      } catch (error) {
        yield SaveStoreIssueFailure(error: error.toString());
      }
    }

    else if (event is SendInternalIssue) {
      yield SendInternalIssueInProgress();

      try {
        final MRResp resp = await repository.sendInternalIssue(event.req);
        yield SendInternalIssueComplete(saveIIResp: resp);
      } catch (error) {
        yield SendInternalIssueFailure(error: error.toString());
      }
    }
    else if(event is SendMTOut) {
      yield SendMTOutInProgress();

      try {
        final MRResp resp = await repository.saveTransferOut(event.mtoReq);
        yield SendMTOutComplete(saveMTOutResp: resp);
      } catch (error) {
        yield SendMTOutFailure(error: error.toString());
      }
    }

    else if(event is SaveStoreReceipt) {
      yield SaveStoreReceiptInProgress();

      try {
        final MRResp resp  = await repository.saveStoreReceipt(event.req);
        yield SaveStoreReceiptComplete(saveStoreReceiptResp: resp);
      } catch (error) {
        yield SaveStoreReceiptFailure(error: error.toString());
      }
    }

    else if(event is SaveSalesOrder) {
      yield SaveSalesOrderInProgress();

      try {
        final SaveSalesResp resp  = await repository.salesOrder(event.req);
        yield SaveSalesOrderComplete(saveSalesOrderResp: resp);
      } catch (error) {
        yield SaveSalesOrderFailure(error: error.toString());
      }
    }

    else if (event is SaveDN) {
      yield SaveDNInProgress();
      try {
        final SaveSalesResp saveGrnResp = await repository.saveDN(event.req);
        yield SaveDNComplete(resp: saveGrnResp);
      } catch (error) {
        yield SaveDNFailure(error: error.toString());
      }
    }

    else if (event is SaveTransferReceipt) {
      yield SaveTransferReceiptInProgress();
      try {
        final MRResp resp = await repository.saveTransferReceipt(event.req);
        yield SaveTransferReceiptComplete(resp: resp);
      } catch (error) {
        yield SaveTransferReceiptFailure(error: error.toString());
      }
    }
  }
}
