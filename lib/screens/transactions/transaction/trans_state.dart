part of 'trans_bloc.dart';

abstract class TransState extends Equatable {
  const TransState();

  @override
  List<Object> get props => [];
}


class SaveDraftTransactionInitial extends TransState {}

class SaveDraftTransactionInProgress extends TransState {}

class SaveDraftTransactionComplete extends TransState {
  final int savedTransactionId;

  SaveDraftTransactionComplete({@required this.savedTransactionId})
      : assert(savedTransactionId != null);

  @override
  List<Object> get props => [savedTransactionId];
}

class SaveDraftTransactionFailure extends TransState {
  final String error;

  const SaveDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDraftTransactionFailure { error : $error }';
}



class UpdateDraftTransactionInitial extends TransState {}

class UpdateDraftTransactionInProgress extends TransState {}

class UpdateDraftTransactionComplete extends TransState {
  final bool updateDraftTransactionResp;

  UpdateDraftTransactionComplete({@required this.updateDraftTransactionResp})
      : assert(updateDraftTransactionResp != null);

  @override
  List<Object> get props => [updateDraftTransactionResp];
}

class UpdateDraftTransactionFailure extends TransState {
  final String error;

  const UpdateDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'UpdateDraftTransactionFailure { error : $error }';
}


class GetDraftTransactionIdInitial extends TransState {}

class GetDraftTransactionIdInProgress extends TransState {}

class GetDraftTransactionIdComplete extends TransState {
  final List<DraftTransactionData> getDraftTransactionIdResp;

  GetDraftTransactionIdComplete({@required this.getDraftTransactionIdResp});

  @override
  List<Object> get props => [getDraftTransactionIdResp];
}

class GetDraftTransactionIdFailure extends TransState {
  final String error;

  const GetDraftTransactionIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDraftTransactionIdFailure { error : $error }';
}


class GetDraftTransactionTypeInProgress extends TransState {}

class GetDraftTransactionTypeComplete extends TransState {
  final List<DraftTransactionData> getDraftTransactionResp;

  GetDraftTransactionTypeComplete({@required this.getDraftTransactionResp});

  @override
  List<Object> get props => [getDraftTransactionResp];
}

class GetDraftTransactionTypeFailure extends TransState {
  final String error;

  const GetDraftTransactionTypeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetDraftTransactionIdFailure { error : $error }';
}



class SaveGrnInitial extends TransState {}

class SaveGrnInProgress extends TransState {}

class SaveGrnComplete extends TransState {
  final SaveGrnResp saveGrnResp;

  const SaveGrnComplete({@required this.saveGrnResp})
      : assert(saveGrnResp != null);

  @override
  List<Object> get props => [saveGrnResp];
}

class SaveGrnFailure extends TransState {
  final String error;

  const SaveGrnFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveGrnFailure { error: $error }';
}


class DeleteDraftTransactionInitial extends TransState {}

class DeleteDraftTransactionInProgress extends TransState {}

class DeleteDraftTransactionComplete extends TransState {
  final int deleteDraftTransactionResp;

  DeleteDraftTransactionComplete({@required this.deleteDraftTransactionResp})
      : assert(deleteDraftTransactionResp != null);

  @override
  List<Object> get props => [deleteDraftTransactionResp];
}

class DeleteDraftTransactionFailure extends TransState {
  final String error;

  const DeleteDraftTransactionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DeleteDraftTransactionFailure { error : $error }';
}



class SavePrInitial extends TransState {}

class SavePrInProgress extends TransState {}

class SavePrComplete extends TransState {
  final SaveGrnResp resp;

  const SavePrComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SavePrFailure extends TransState {
  final String error;

  const SavePrFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveGrnFailure { error: $error }';
}



class SaveSTInitial extends TransState {}

class SaveSTInProgress extends TransState {}

class SaveSTComplete extends TransState {
  final SaveGrnResp resp;

  const SaveSTComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveSTFailure extends TransState {
  final String error;

  const SaveSTFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveStockTakingFailure { error: $error }';
}



class SaveOBInitial extends TransState {}

class SaveOBInProgress extends TransState {}

class SaveOBComplete extends TransState {
  final SaveGrnResp resp;

  const SaveOBComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveOBFailure extends TransState {
  final String error;

  const SaveOBFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveOBFailure { error: $error }';
}



class SaveSaiInitial extends TransState {}

class SaveSaiInProgress extends TransState {}

class SaveSaiComplete extends TransState {
  final SaveSalesResp resp;

  const SaveSaiComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveSaiFailure extends TransState {
  final String error;

  const SaveSaiFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveSaiFailure { error: $error }';
}



class SaveOfInitial extends TransState {}

class SaveOfInProgress extends TransState {}

class SaveOfComplete extends TransState {
  final SaveOFResp resp;

  const SaveOfComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveOfFailure extends TransState {
  final String error;

  const SaveOfFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveOfFailure { error: $error }';
}



class SaveStoreIssueInitial extends TransState {}

class SaveStoreIssueInProgress extends TransState {}

class SaveStoreIssueComplete extends TransState {
  final MRResp saveStoreIssueResp;

  SaveStoreIssueComplete({@required this.saveStoreIssueResp})
      : assert(saveStoreIssueResp != null);

  @override
  List<Object> get props => [saveStoreIssueResp];
}

class SaveStoreIssueFailure extends TransState {
  final String error;

  const SaveStoreIssueFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendStoreIssueFailure { error : $error }';
}



class SendInternalIssueInitial extends TransState {}

class SendInternalIssueInProgress extends TransState {}

class SendInternalIssueComplete extends TransState {
  final MRResp saveIIResp;

  SendInternalIssueComplete({@required this.saveIIResp})
      : assert(saveIIResp != null);

  @override
  List<Object> get props => [saveIIResp];
}

class SendInternalIssueFailure extends TransState {
  final String error;

  const SendInternalIssueFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendInternalIssueFailure { error : $error }';
}


/// GetItemPriceDetails States

class GetItemPriceDetailsInitial extends TransState {}

class GetItemPriceDetailsInProgress extends TransState {}

class GetItemPriceDetailsComplete extends TransState {
  final GetItemPriceDetailsResp getItemPriceDetailsResp;

  GetItemPriceDetailsComplete({@required this.getItemPriceDetailsResp})
      : assert(getItemPriceDetailsResp != null);

  @override
  List<Object> get props => [getItemPriceDetailsResp];
}

class GetItemPriceDetailsFailure extends TransState {
  final String error;

  const GetItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemPriceDetailsFailure { error : $error }';
}


class SendMTOutInitial extends TransState {}

class SendMTOutInProgress extends TransState {}

class SendMTOutComplete extends TransState {
  final MRResp saveMTOutResp;

  SendMTOutComplete({@required this.saveMTOutResp})
      : assert(saveMTOutResp != null);

  @override
  List<Object> get props => [saveMTOutResp];
}

class SendMTOutFailure extends TransState {
  final String error;

  const SendMTOutFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendMTOutFailure { error : $error }';
}


class SaveStoreReceiptInitial extends TransState {}

class SaveStoreReceiptInProgress extends TransState {}

class SaveStoreReceiptComplete extends TransState {
  final MRResp saveStoreReceiptResp;

  SaveStoreReceiptComplete({@required this.saveStoreReceiptResp})
      : assert(saveStoreReceiptResp != null);

  @override
  List<Object> get props => [saveStoreReceiptResp];
}

class SaveStoreReceiptFailure extends TransState {
  final String error;

  const SaveStoreReceiptFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveStoreReceiptFailure { error : $error }';
}


class SaveSalesOrderInitial extends TransState {}

class SaveSalesOrderInProgress extends TransState {}

class SaveSalesOrderComplete extends TransState {
  final SaveSalesResp saveSalesOrderResp;

  SaveSalesOrderComplete({@required this.saveSalesOrderResp})
      : assert(saveSalesOrderResp != null);

  @override
  List<Object> get props => [saveSalesOrderResp];
}

class SaveSalesOrderFailure extends TransState {
  final String error;

  const SaveSalesOrderFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveSalesOrderFailure { error : $error }';
}


class SaveDNInitial extends TransState {}

class SaveDNInProgress extends TransState {}

class SaveDNComplete extends TransState {
  final SaveSalesResp resp;

  const SaveDNComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveDNFailure extends TransState {
  final String error;

  const SaveDNFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveDNFailure { error: $error }';
}


class SaveTransferReceiptInitial extends TransState {}

class SaveTransferReceiptInProgress extends TransState {}

class SaveTransferReceiptComplete extends TransState {
  final MRResp resp;

  const SaveTransferReceiptComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class SaveTransferReceiptFailure extends TransState {
  final String error;

  const SaveTransferReceiptFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SaveTransferReceiptFailure { error: $error }';
}