part of 'trans_bloc.dart';

abstract class TransEvent extends Equatable {
  const TransEvent();
}

class SaveDraftTransaction extends TransEvent {
  final DraftTransactionData draftTransactionData;
  const SaveDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'SaveDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class UpdateDraftTransaction extends TransEvent {
  final DraftTransactionData draftTransactionData;
  final int draftId;
  const UpdateDraftTransaction({@required this.draftId, @required this.draftTransactionData});

  @override
  List<Object> get props => [draftId, draftTransactionData];

  @override
  String toString() => 'UpdateDraftTransaction {"DraftId" : "$draftId", "DraftTransaction" : "$draftTransactionData"}';
}

class GetDraftTransactionId extends TransEvent {
  final String draftType;
  const GetDraftTransactionId({@required this.draftType});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetDraftTransactionId {"DraftType" : "$draftType"}';
}

class GetDraftTransactionType extends TransEvent {
  final String draftType;
  const GetDraftTransactionType({@required this.draftType});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'GetDraftTransactionType {"DraftType" : "$draftType"}';
}

class DeleteDraftTransaction extends TransEvent {
  final DraftTransactionData draftTransactionData;
  const DeleteDraftTransaction({@required this.draftTransactionData});

  @override
  List<Object> get props => [draftTransactionData];

  @override
  String toString() => 'DeleteDraftTransaction {"DraftTransaction" : "$draftTransactionData"}';
}

class SaveGrn extends TransEvent {
  final String grnReq;

  const SaveGrn({@required this.grnReq});

  @override
  List<Object> get props =>
      [grnReq];

  @override
  String toString() => 'SaveGrn {''companyId: $grnReq}';
}

class SavePR extends TransEvent {
  final String prReq;

  const SavePR({@required this.prReq});

  @override
  List<Object> get props =>
      [prReq];

  @override
  String toString() => 'SaveGrn $prReq';
}

class SaveST extends TransEvent {
  final SaveSTReq stReq;

  const SaveST({@required this.stReq});

  @override
  List<Object> get props =>
      [stReq];

  @override
  String toString() => 'SaveST $stReq';
}

class SaveOB extends TransEvent {
  final SaveOBReq obReq;

  const SaveOB({@required this.obReq});

  @override
  List<Object> get props =>
      [obReq];

  @override
  String toString() => 'SaveOB $obReq';
}

class SaveSAI extends TransEvent {
  final String saiReq;

  const SaveSAI({@required this.saiReq});

  @override
  List<Object> get props =>
      [saiReq];

  @override
  String toString() => 'SaveSAI $saiReq';
}

class SaveOF extends TransEvent {
  final String ofReq;

  const SaveOF({@required this.ofReq});

  @override
  List<Object> get props =>
      [ofReq];

  @override
  String toString() => 'SaveOF $ofReq';
}

class SaveStoreIssue extends TransEvent {
  final SIReq siReq;
  const SaveStoreIssue({@required this.siReq});

  @override
  List<Object> get props => [siReq];

  @override
  String toString() => 'SendStoreIssue {"StoreIssue" : "$siReq"}';
}

class SendInternalIssue extends TransEvent {
  final SaveIIReq req;
  const SendInternalIssue({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'SendInternalIssue {"InternalIssue" : "$req"}';
}

class SendMTOut extends TransEvent {
  final MTOReq mtoReq;
  const SendMTOut({@required this.mtoReq});

  @override
  List<Object> get props => [mtoReq];

  @override
  String toString() => 'SendMTOut {"MTOut" : "$mtoReq"}';
}
//
// class SendStoreReceipt extends TransEvent {
//   final SIReq storeIssue;
//   const SendStoreReceipt({@required this.storeIssue});
//
//   @override
//   List<Object> get props => [storeIssue];
//
//   @override
//   String toString() => 'SendStoreReceipt {"StoreReceipt" : "$storeIssue"}';
// }

class SaveStoreReceipt extends TransEvent {
  final String req;
  const SaveStoreReceipt({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'SaveStoreReceipt {"StoreReceipt" : "$req"}';
}

class SaveSalesOrder extends TransEvent {
  final String req;
  const SaveSalesOrder({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'SaveSalesOrder {"req" : "$req"}';
}

class SaveDN extends TransEvent {
  final String req;
  const SaveDN({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'SaveDN {"req" : "$req"}';
}

class SaveTransferReceipt extends TransEvent {
  final String req;
  const SaveTransferReceipt({@required this.req});

  @override
  List<Object> get props => [req];

  @override
  String toString() => 'SaveTransferReceipt {"req" : "$req"}';
}