import 'dart:convert';

import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/cool_stepper.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/request/DN/saveDNReq.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesClientInfo.dart';
import 'package:bcore_inventory_management/models/view/salesItem/common_salesItemInfo.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnAddCharges.dart';
import 'package:bcore_inventory_management/models/view/utilities/cmnTandCInfo.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/commonWidget/AddCharge/addCharges_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Client/client_form.dart';
import 'package:bcore_inventory_management/screens/commonWidget/ItemEntry/DnItem/dnItemEntryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/Summary/SalesSummaryForm.dart';
import 'package:bcore_inventory_management/screens/commonWidget/TermsAndConditions/termsAndConditions_form.dart';
import 'package:bcore_inventory_management/screens/transactions/transaction/trans_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uuid/uuid.dart';

final GlobalKey<CoolStepperState> coolStepperDNState =
    GlobalKey<CoolStepperState>();
GlobalKey<ScaffoldState> scaffoldDNKey = new GlobalKey<ScaffoldState>();

class DNPage extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const DNPage({Key key, this.repository, this.database, this.appLanguage})
      : super(key: key);

  @override
  _DNPageState createState() => _DNPageState(repository, database, appLanguage);
}

class _DNPageState extends State<DNPage> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  DraftTransactionData draftTransactionData;

  int savedDraftId;
  TransBloc _transBloc;

  CommonSalesItem itemForStepperUpdate;
  bool needToUpdateStepper = false;
  int indexOfItemDetailForm = 1;
  _DNPageState(this.repository, this.database, this.appLanguage);

  @override
  void initState() {
    super.initState();

    resetTransactionData();
  }

  /// Clears all the data stored in references regarding this Transaction
  resetTransactionData() {
    cmnSalesClientInfo.clear();
    CommonSalesItemInfo.shared.clear();
    CommonAddChargesInfo.shared.clear();
    CommonTandCInfo.shared.clear();
    CommonTandCInfo.shared.clear();
    SaveDNReq.shared.clear();
  }

  getStepWithSalesItemEntryForm() {
    return CoolStep(
      /**
       * Checks whether an item is being edited or not. For that purpose, checks the flag and If the item is being edited the Item Entry Form will be passed with the item
       */
      content: (needToUpdateStepper)
          ? DnItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
         item: itemForStepperUpdate
            )
          : DnItemEntryForm(
              repository: repository,
              database: database,
              appLanguage: appLanguage,
            ),
      validation: () {
        /**
         * Checks whether the Items List is empty or not. If so, shows the validation message to add item
         */
        if (CommonSalesItemInfo.shared.cmnSalesItemList.isNotEmpty) {

          /**
           * Adds Line no for each item
           */
          for(int index = 0; index < CommonSalesItemInfo.shared.cmnSalesItemList.length ; index++){
            CommonSalesItemInfo.shared.cmnSalesItemList[index].lineNum = index ;
          }

          SaveDNReq.shared.fromJson(CommonSalesItemInfo.shared.toJson());

          updateDraftTransaction(TransactionType.DN);
          _transBloc.add(UpdateDraftTransaction(
              draftTransactionData: draftTransactionData));
          return null;
        } else {
          Common().showStepperValidation(
              scaffoldKey: scaffoldDNKey, message: Constants.noItemAddedMessage);
          return Constants.noItemAddedMessage;
        }
      },
    );
  }

  _saveDN() {
    /// Save DN SaveDN
    _transBloc.add(SaveDN(req: json.encode(SaveDNReq.shared.toJson())));
  }

  @override
  Widget build(BuildContext context) {
    BuildContext blocContext;

    final List<CoolStep> steps = [
      CoolStep(
        subContent: SizedBox(height: 0),
        content: ClientForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.DN),
        validation: () {
          // Validate Client details
          if (_validateClientDetails(context: context)) {
            setDraftTransaction(TransactionType.DN);
            _transBloc.add(SaveDraftTransaction(
                draftTransactionData: draftTransactionData));
            return null;
          } else {
            // Show alert message
            return 'Please fill the required fields';
          }
        },
      ),
      getStepWithSalesItemEntryForm(),
      CoolStep(
        title: "Additional Charges",
        subContent: SizedBox(height: 0),
        content: AddChargesForm(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.DN),
        validation: () {
          if (CommonAddChargesInfo.shared.cmnAddChargesList.isNotEmpty) {
            SaveDNReq.shared.fromJson(CommonAddChargesInfo.shared.toJson());

            setDraftTransaction(TransactionType.DN);
            _transBloc.add(UpdateDraftTransaction(
                draftTransactionData: draftTransactionData));
          }
          return null;
        },
      ),
      CoolStep(
        title: "Terms & Conditions",
        subContent: SizedBox(height: 0),
        content: TermsAndConditions(
            repository: repository,
            appLanguage: appLanguage,
            type: TransactionType.DN),
        validation: () {
          if (CommonTandCInfo.shared.termsAndConditionList.isNotEmpty) {
            SaveDNReq.shared.fromJson(CommonTandCInfo.shared.toJson());

            setDraftTransaction(TransactionType.DN);
            _transBloc.add(UpdateDraftTransaction(
                draftTransactionData: draftTransactionData));
          }
          return null;
        },
      ),
      CoolStep(
        title: "Summary",
        subContent: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 0.0, // gap between lines
          children: <Widget>[
            if (fromAddress.fromCompanyName != null)
              Chip(
                avatar: Icon(Icons.location_city, size: 15),
                label: Text('${fromAddress.fromCompanyName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDivisionName != null)
              Chip(
                avatar: Icon(Icons.call_split, size: 15),
                label: Text('${fromAddress.fromDivisionName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromLocationName != null)
              Chip(
                avatar: Icon(Icons.location_searching, size: 15),
                label: Text('${fromAddress.fromLocationName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            if (fromAddress.fromDepartmentName != null)
              Chip(
                avatar: Icon(Icons.dashboard, size: 15),
                label: Text('${fromAddress.fromDepartmentName}',
                    style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.white,
              ),
            SizedBox()
          ],
        ),
        content: SalesSummaryForm(
            repository: repository,
            appLanguage: appLanguage,
            database: database,
            type: TransactionType.DN,
            navigateBackWithValueIndex: (int indexOfEditingItem) {
              if ( CommonSalesItemInfo.shared.cmnSalesItemList[indexOfEditingItem] != null) {
                setState(() {
                  itemForStepperUpdate = CommonSalesItemInfo.shared.cmnSalesItemList[indexOfEditingItem] ;
                  needToUpdateStepper = true;
                  coolStepperDNState.currentState.goToStep(index: indexOfItemDetailForm);
                  Constants.isTransEditMode = true;
                });
              }
            }),
        validation: () {
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      key: coolStepperDNState,
      onCompleted: () {
        print("Steps completed!");

        if (SaveDNReq.shared.materialsList.isEmpty) {
          Common().showStepperValidation(
              scaffoldKey: scaffoldDNKey, message: Constants.noItemAddedMessage);
          return;
        }

        Common().showAlertMessageWithAction(
            context: blocContext,
            title: "Confirm",
            message: "Do you want to save Delivery Note as Draft",
            okButtonTitle: "No",
            okFunction: () {
              print("Tapped No");
              SaveDNReq.shared.isDraft = false;
              _saveDN();
              Navigator.pop(blocContext);
            },
            actionButtonTitle: "Yes",
            actionFunction: () {
              print("Tapped Yes");
              SaveDNReq.shared.isDraft = true;
              _saveDN();
              Navigator.pop(blocContext);
            });
      },
      steps: steps,
      config: CoolStepperConfig(
        headerColor: BCAppTheme().secondaryColor,
        nextText: "Next",
        backText: "Back",
        finalText: "Submit",
        nextTextStyle: TextStyle(color: BCAppTheme().primaryColor),
        stepTextStyle: TextStyle(fontSize: 12),
        iconColor: Colors.transparent,
        titleTextStyle:
            TextStyle(color: BCAppTheme().primaryColor, fontSize: 15),
      ),
      headerPadding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0),
    );

    return BlocProvider<TransBloc>(
      create: (context) =>
          TransBloc(repository: repository, database: database),
      child: BlocListener<TransBloc, TransState>(
        listener: (context, state) {
          if (state is SaveDraftTransactionInProgress ||
              state is UpdateDraftTransactionInProgress ||
              state is SaveDNInProgress ||
              state is DeleteDraftTransactionInProgress) {
            Common().showLoader(context);
          }
          else if (state is SaveDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
          else if (state is SaveDraftTransactionComplete) {
            savedDraftId = state.savedTransactionId;
            Common().setTransactionId(savedDraftId);
            Navigator.pop(context, true);
          }
          else if (state is UpdateDraftTransactionComplete) {
            Navigator.pop(context, true);
          }
          else if (state is SaveDNComplete) {
            Navigator.pop(context, true);

            Common().showAlertMessage(
                context: context,
                title: 'Success',
                message: state.resp.statusMessage,
                okFunction: () {
                  resetTransactionData();
                  _transBloc.add(DeleteDraftTransaction(
                      draftTransactionData: draftTransactionData));
                  Navigator.pop(context, true);
                });
          }
          else if (state is SaveDNFailure) {
            Navigator.pop(context, true);

            String errorMessage = state.error.replaceAll('<br>', '');
            errorMessage = errorMessage.replaceAll('Exception:', '');
            Common().showAlertMessage(
                context: context,
                title: "Sorry",
                message: errorMessage,
                okFunction: () {
                  Navigator.pop(context);
                });
          }
          else if (state is UpdateDraftTransactionFailure) {
            Navigator.pop(context, true);
          }
          else if (state is DeleteDraftTransactionFailure) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
          else if (state is DeleteDraftTransactionComplete) {
            Navigator.pop(context, true);
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<TransBloc, TransState>(builder: (context, state) {
          blocContext = context;
          _transBloc = BlocProvider.of<TransBloc>(context);
          return WillPopScope(
            onWillPop: () async {
              Common().showAlertMessageWithAction(context: context,
                  title: Constants.alertMsgTitle,
                  message: Constants.transactionClearMsg,
                  okButtonTitle: 'Ok',
                  okFunction: (){
                    resetTransactionData();
                    Navigator.pop(context);
                    Navigator.pop(context);
                    // return true;
                  },
                  actionButtonTitle: 'Cancel',
                  actionFunction: (){
                    Navigator.pop(context);
                    // return false;
                  });
              return null;
            },
            child: Scaffold(
              key: scaffoldDNKey,
              appBar: BCAppBar(
                  scaffoldKey: scaffoldDNKey,
                  title: 'Delivery Note',
                  isBack: false,
                  appBar: AppBar(),
                  database: database,
                  appLanguage: appLanguage,
                  repository: repository,
                  shouldHideActions: false),
              drawer: NavigationDrawer(
                context: context,
                repository: repository,
                appLanguage: appLanguage,
                database: database,
              ),
              body: SafeArea(
                child: Container(
                  child: stepper,
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void setDraftTransaction(TransactionType type) {
    var uuid = Uuid();
    draftTransactionData = DraftTransactionData(
        id: savedDraftId,
        trans_id: type.value + '_' + uuid.v1(),
        trans_type: type.value,
        trans_details: SaveDNReq.shared.toJson().toString());
  }

  updateDraftTransaction(TransactionType type) async {
    var uuid = Uuid();
//    int transactionId = await Common().getTransactionId();
    if (savedDraftId != null) {
      draftTransactionData = DraftTransactionData(
          id: savedDraftId,
          trans_id: type.value + '_' + uuid.v1(),
          trans_type: type.value,
          trans_details: SaveDNReq.shared.toJson().toString());
    }
  }

  bool _validateClientDetails({BuildContext context}) {
    if (cmnSalesClientInfo.salesOrderId == null) {
      Common().showStepperValidation(
          scaffoldKey: scaffoldDNKey, message: "Please select Sales Order");
      return false;
    } else {
      SaveDNReq.shared.fromJson(cmnSalesClientInfo.toJson());
      return true;
    }
  }
}
