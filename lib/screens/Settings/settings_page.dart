import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/printUtil.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/GetScmItemPriceDetails_resp.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';
import 'package:bcore_inventory_management/navigationDrawer/navigationDrawer_page.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Settings/settings_bloc.dart';
import 'package:bcore_inventory_management/screens/login/initialLogin/initialLogin_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/LocalDBSync.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/MyConfiguration.dart';
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wifi/wifi.dart';

class Settings extends StatefulWidget {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  const Settings(
      {Key key,
      this.repository,
      this.appLanguage,
      this.database,
      BuildContext context})
      : super(key: key);

  @override
  _SettingsState createState() =>
      _SettingsState(repository, appLanguage, database);
}

class _SettingsState extends State<Settings> {
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  CommonItem itemDetail;

  List<PrinterBluetooth> _devices = [];
  PrinterBluetooth _device;
  PrinterBluetooth bluetooth_device;

  PrintUtil printUtil;
  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  SettingsBloc _settingsBloc;
  List<ItemPackingData> itemPackingDataList;
  List<ItemBatchData> itemBatchDataList;
  List<String> network_devices = [];
  String network_device;
  int found = -1;
  int groupValue = 0;

  String rb_Printer, lb_Printer, ln_Printer;


  bool isDiscovering = false;
  String localIp = '';

  TextEditingController portController = TextEditingController(text: '9100');
  TextEditingController ipTextController = TextEditingController();

  bool isDropdown = false, isBluetooth = false, isOther = false;
  bool shouldValidatePackage = false;

  List<String> itemList = [
    'Bluetooth1',
    'Bluetooth2',
    'Bluetooth3',
    'Bluetooth4'
  ];
  var _searchController = TextEditingController();
  String selectedPrinter = '';
  String receiptPrinter;

  _SettingsState(this.repository, this.appLanguage, this.database);

  @override
  void initState() {
    super.initState();
    initPlatformState();
    checkGetForInitialScreen();
    itemDetail = CommonItem();
    Utils.resetUniqueId();
    printUtil = PrintUtil();
  }

  Future<void> initPlatformState() async {
    printerManager.scanResults.listen((devices) async {
      // print('UI: Devices found ${devices.length}');
      setState(() {
        _devices = devices;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    return BlocProvider<SettingsBloc>(
      create: (context) =>
          SettingsBloc(bcRepository: repository, database: database),
      child: BlocListener<SettingsBloc, SettingsState>(
        listener: (context, state) {
          if (state is LoadItemInProgress ||
              state is LoadPackingByIdInProgress ||
              state is LoadPackingInProgress) {
            Common().showLoader(context);
          } else if (state is LoadItemFailure ||
              state is LoadPackingByIdFailure ||
              state is LoadPackingFailure) {
            Navigator.pop(context);
          } else if (state is LoadItemComplete) {
            Navigator.pop(context);
            print("LoadItemComplete");

            if (state.loadItemResult != null &&
                state.loadItemResult.isNotEmpty) {
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Item not found'),
                  duration: Duration(milliseconds: 500)));
            }
          } else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);
          } else if (state is LoadItemByPackingComplete) {
            Navigator.pop(context);
          } else if (state is LoadPackingComplete) {
            Navigator.pop(context);

            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;
                itemDetail.ProdpkgId =
                    int.parse(itemPackingDataList[0].packing_value);
                itemDetail.PackageName = itemPackingDataList[0].packing_text;
              });
            }
          } else if (state is GetScmItemPriceDetailsComplete) {
            Navigator.pop(context);

            if (state.getPackingDetailsResp != null &&
                state.getPackingDetailsResp.masterDataList.length > 0) {
              setState(() {
                List<GetScmItemPriceList> packingDetails =
                    state.getPackingDetailsResp.masterDataList;
                if (packingDetails.isNotEmpty) {
                  itemDetail.UnitCost =
                      state.getPackingDetailsResp.masterDataList[0].unitPrice ??
                          (itemDetail.UnitCost ?? 0.0);
                  itemDetail.AvailableQty = state.getPackingDetailsResp
                          .masterDataList[0].availableQty ??
                      0.0;
                }
              });
            }
          } else if (state is LoadCountryByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadCountryResult != null &&
                state.loadCountryResult.length > 0) {
              setState(() {
                List<CountryOriginData> countryOfOriginList =
                    state.loadCountryResult;
                itemDetail.CountryName =
                    countryOfOriginList[0].country_name.trim();
              });
            }
          } else if (state is LoadBrandByIdComplete) {
            Navigator.pop(context);

            /// Load packing list in dropdown
            if (state.loadBrandResult != null &&
                state.loadBrandResult.length > 0) {
              setState(() {
                List<ProductBrandData> productBrandList = state.loadBrandResult;
                itemDetail.BrandName = productBrandList[0].brand_name.trim();
              });
            }
          } else if (state is LoadBatchByIdComplete) {
            Navigator.pop(context);

            if (state.getItemBatchResp != null &&
                state.getItemBatchResp.length > 0) {
              setState(() {
                itemBatchDataList = state.getItemBatchResp;
              });
            }
          } else if (state is LoadPackingByIdComplete) {
            Navigator.pop(context);
            if (state.loadPackingResult != null &&
                state.loadPackingResult.length > 0) {
              setState(() {
                itemPackingDataList = state.loadPackingResult;

                itemDetail.PackageName = itemPackingDataList
                    .where((packing) =>
                        packing.packing_value ==
                        itemDetail.ProdpkgId.toString())
                    .toList()[0]
                    .packing_text;
              });
            }
          }
        },
        child:
            BlocBuilder<SettingsBloc, SettingsState>(builder: (context, state) {
          _settingsBloc = BlocProvider.of<SettingsBloc>(context);
          // var groupValue;
          var _handleRadioValueChange1;
          return Scaffold(
              appBar: BCAppBar(
                title: 'Settings',
                scaffoldKey: scaffoldKey,
                isBack: true,
                appBar: AppBar(),
                database: database,
                appLanguage: appLanguage,
                repository: repository,
                shouldHideActions: false,
              ),
              body: Scaffold(
                key: scaffoldKey,
                drawer: NavigationDrawer(
                  context: context,
                  repository: repository,
                  appLanguage: appLanguage,
                  database: database,
                ),
                body: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FromAddressForm(
                                      repository: repository,
                                      appLanguage: appLanguage,
                                      database: database,
                                    )));
                          },
                          child: Text(
                            'My Address',
                            style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                            textAlign: TextAlign.left,
                          )),
                      SizedBox(height: 8.0,),
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LocalDBSync(
                                      repository: repository,
                                      appLanguage: appLanguage,
                                      database: database,
                                    )));
                          },
                          child: Text(
                            'Sync Data',
                            style: TextStyle(
                              fontSize: 16,fontWeight: FontWeight.bold),
                            textAlign: TextAlign.left,)),
                      SizedBox(height: 8.0,),
                      Divider(),
                      SizedBox(height: 8.0,),

                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Receipt Printer',
                            style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                            textAlign: TextAlign.left,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: _getBlueToothPrinterFieldNew(),
                              ),
                              SizedBox(width: 10),
                            ],
                          ),
                        ],
                      ),

                      SizedBox(height: 8.0,),
                      Divider(),
                      SizedBox(height: 8.0,),

                      Text(
                        'Label Printer',
                        style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Radio(
                                  value: 0,
                                  groupValue: groupValue,
                                  onChanged: (value) {
                                    setState(() {
                                      groupValue = value;
                                      showBluetooth();
                                      _getBlueToothPrinterField();
                                      checkSetForInitialScreen();
                                    });
                                  },
                                  activeColor: BCAppTheme().headingTextColor,
                                ),
                                new Icon(Icons.bluetooth, size: 16,),
                                new Text('Bluetooth'),

                                new Radio(
                                  value: 1,
                                  groupValue: groupValue,
                                  onChanged: (value) {
                                    setState(() {
                                      groupValue = value;
                                      print(groupValue);
                                      showOther();
                                      _getNetworkPrinterField();
                                      checkSetForInitialScreen();
                                    });
                                  },
                                  activeColor: BCAppTheme().headingTextColor,
                                  toggleable: true,
                                ),
                                new Icon(Icons.wifi, size: 16,),
                                new Text('Network')
                              ],
                            ),

                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, top: 4),
                            child: Visibility(
                              visible: isBluetooth,
                              child: _getBlueToothPrinterField(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, top: 4),
                            child: Visibility(
                              visible: isOther,
                              child: _getNetworkPrinterField(),
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: 8.0,),
                      Divider(),
                      SizedBox(height: 8.0,),

                      GestureDetector(
                          onTap: () async {
                            clearCacheAndNavigate();
                          },
                          child: Text(
                            'Sign Out',
                            style: TextStyle(
                              fontSize: 16,fontWeight: FontWeight.bold),
                            textAlign: TextAlign.left,
                          )),


                      Padding(
                        padding: const EdgeInsets.only(top: 80,bottom: 8),
                        child: Container(
                            child: Align(
                                alignment: FractionalOffset.bottomCenter,
                                child: Column(
                                  children: <Widget>[
                                    Divider(),
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 8.0),
                                      child: Text(Constants().version,
                                          // TODO: change the version number on every build time.
                                          style: new TextStyle(
                                            fontSize: 15.0,
                                            letterSpacing: 1.0,
                                            color: BCAppTheme().subTextColor,
                                          )),
                                    )
                                  ],
                                ))),
                      ),
                    ],
                  ),
                ),
              ));
        }),
      ),
    );
  }

  ///visibility shown in dropdown
  void showPrinter() {
    setState(() {
      isDropdown = true;
    });
  }

  ///visibility shown in bluetooth
  void showBluetooth() {
    setState(() {
      isBluetooth = true;
      isOther = false;
    });
  }

  ///visibility shown in dropdown
  void showOther() {
    setState(() {
      isOther = true;
      isBluetooth = false;
    });
  }

  clearCacheAndNavigate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("Token");

    if (prefs.containsKey("hasConfiguredPin")) {
      if (prefs.getBool("hasConfiguredPin")) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Login()));
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => InitialLogin()));
      }
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => InitialLogin()));
    }
  }

  _getBlueToothPrinterField() {
    return Column(
      children: [
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemDetail != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Bluetooth Printer"),
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() {
                        // network_device = "";
                        _device = value;
                      }),
                      value: _device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
                  Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: StreamBuilder<bool>(
                stream: printerManager.isScanningStream,
                initialData: false,
                builder: (c, snapshot) {
                  if (snapshot.data) {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().blueColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          _stopScanDevices();
                        });
                  } else {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().primaryColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          // Common().showLoader(context);
                          _startScanDevices();
                        });
                  }
                },
              ),
            )
          ],
        )
      ],
    );
  }

  _getNetworkPrinterField() {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemDetail.ProdpkgId != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Network Printer"),
                      // value: "Printer",
                      items: network_devices != null
                          ? network_devices.map((String item) {
                              return DropdownMenuItem(
                                  child: Text(item,
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        color: BCAppTheme().subTextColor,
                                      )),
                                  value: item);
                            }).toList()
                          : [],
                      onChanged: (value) {
                        setState(() {
                          // _device= null;
                          network_device = value;
                        });
                      },
                      value: network_device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
                  Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: IconButton(
                  icon: Icon(
                    Icons.search,
                    color: BCAppTheme().primaryColor,
                  ),
                  iconSize: 20,
                  onPressed: () {
                    showPrinterDialogue();
                  }),
            )
          ],
        )
      ],
    );
  }

  _getBlueToothPrinterFieldNew() {
    return Column(
      children: [
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemDetail != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Bluetooth Printer"),
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() {
                        // network_device = "";
                        bluetooth_device.
                        bluetooth_device = value;
                        rb_Printer = bluetooth_device.name;
                        Constants.bc_selectedPrinter = bluetooth_device;
                         checkSetForInitialScreen(saveCase: 0, rect_blue_printer: bluetooth_device.name);
                      }),
                      value: Constants.bc_selectedPrinter ?? bluetooth_device,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
              decoration:
                  Common().getBCoreSD(isMandatory: true, isValidated: true),
              width: 50,
              height: 50,
              child: StreamBuilder<bool>(
                stream: printerManager.isScanningStream,
                initialData: false,
                builder: (c, snapshot) {
                  if (snapshot.data) {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().blueColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          _stopScanDevices();
                        });
                  } else {
                    return IconButton(
                        icon: Icon(
                          Icons.search,
                          color: BCAppTheme().primaryColor,
                        ),
                        iconSize: 20,
                        onPressed: () {
                          // Common().showLoader(context);
                          _startScanDevices();
                          // Navigator.pop(context);
                        });
                  }
                },
              ),
            )
          ],
        )
      ],
    );
  }

  _getBlueToothPrinter() {
    return Column(
      children: [
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: SizedBox(
                height: 50,
                child: Container(
                  decoration: Common().getBCoreSD(
                      isMandatory: shouldValidatePackage,
                      isValidated: itemList != null),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: DropdownButton(
                      underline: Text(""),
                      isDense: false,
                      isExpanded: true,
                      hint: Text("Select Bluetooth Printer"),
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() {
                        // network_device = "";
                        _device = value;
                      }),
                      value: _device,
                    ),
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  void _startScanDevices() {
    setState(() {
      _devices = [];
    });
    printerManager.startScan(Duration(seconds: 4));
    // Navigator.pop(context);

  }

  _stopScanDevices() {
    printerManager.stopScan();
  }

  List<DropdownMenuItem<PrinterBluetooth>> _getDeviceItems() {
    List<DropdownMenuItem<PrinterBluetooth>> items = [];
    if (_devices.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }

  showPrinterDialogue() {
    Dialog printerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(child: StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Printer IP", textAlign: TextAlign.center),
              SizedBox(height: 16.0),
              Container(
                height: 45,
                child: TextField(
                  controller: ipTextController,
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    dropDownState(() {});
                  },
                  decoration: Common().getBCoreMandatoryID(
                      isMandatory: false,
                      isValidated: true,
                      hintText: 'Search',
                      icon: Icons.search),
                ),
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 35,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              width: 0.5,
                              style: BorderStyle.solid,
                              color: BCAppTheme().primaryColor),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: FlatButton(
                          child: Text("Cancel",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 11)),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Container(
                      height: 35,
                      child: FlatButton(
                          color: BCAppTheme().primaryColor,
                          child: Text("Ok",
                              style: TextStyle(
                                  color: BCAppTheme().secondaryColor,
                                  fontSize: 11)),
                          onPressed: () {
                            discover(context);
                            Navigator.pop(context);
                            // setState(() {
                            // });
                          }),
                    ),
                  )
                ],
              )
            ],
          );
        })),
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return printerDialog;
      },
    );
  }

  void discover(BuildContext ctx) async {
    setState(() {
      isDiscovering = true;
      network_devices.clear();
      found = -1;
    });

    String ip;
    try {
      ip = await Wifi.ip;
      print('local ip:\t$ip');
    } catch (e) {
      final snackBar = SnackBar(
          content: Text('WiFi is not connected', textAlign: TextAlign.center));
      Scaffold.of(ctx).showSnackBar(snackBar);
      return;
    }
    setState(() {
      localIp = ipTextController.text;
    });

    final String subnet = localIp.substring(0, ip.lastIndexOf('.'));
    int port = 9100;
    try {
      port = int.parse(portController.text);
    } catch (e) {
      portController.text = port.toString();
    }
    print('subnet:\t$subnet, port:\t$port');

    final stream = NetworkAnalyzer.discover2(subnet, port);

    stream.listen((NetworkAddress addr) {
      if (addr.exists) {
        print('Found device: ${addr.ip}');
        setState(() {
          network_devices.add(addr.ip);
          found = network_devices.length;
        });
      }
    })
      ..onDone(() {
        setState(() {
          isDiscovering = false;
          found = network_devices.length;
        });
      })
      ..onError((dynamic e) {
        final snackBar = SnackBar(
            content: Text('Unexpected exception', textAlign: TextAlign.center));
        Scaffold.of(ctx).showSnackBar(snackBar);
      });
  }

  checkSetForInitialScreen({int saveCase, String rect_blue_printer, String lbl_blue_printer,
    String lbl_ntwrk_printer}) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    switch (saveCase){
      case 0:
        pref.setString("RBPrinter", _device.name);
        break;
      case 1:
        pref.setString("LBPrinter", receiptPrinter);
        break;
      case 2:
        pref.setString("LNPrinter", receiptPrinter);
        break;
      case 3:
        pref.setInt("LABELPrintConfig", groupValue);
        break;

    }

  }

  checkGetForInitialScreen() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    rb_Printer = pref.getString("RBPrinter");
    lb_Printer = pref.getString("LBPrinter");
    ln_Printer = pref.getString("LNPrinter");
    groupValue = pref.getInt("LABELPrintConfig");
  }
}
