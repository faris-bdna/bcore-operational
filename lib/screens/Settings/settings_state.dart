part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

/// Get Stock Reference Details

class GetStockReferenceDetailsInitial extends SettingsState{}

class GetStockReferenceDetailsInProgress extends SettingsState{}

class GetStockReferenceDetailsComplete extends SettingsState{
  final GetStockReferenceDetailsResp resp;

  GetStockReferenceDetailsComplete({@required this.resp}) : assert(resp != null);

  @override
  List<Object> get props => [resp];
}

class GetStockReferenceDetailsFailure extends SettingsState {
  final String error;

  const GetStockReferenceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetStockReferenceDetailsFailure { error : $error }';
}


/// Load Items

class LoadItemInitial extends SettingsState{}

class LoadItemInProgress extends SettingsState{}

class LoadItemComplete extends SettingsState{
  final List<ItemBarcodeData> loadItemResult;

  LoadItemComplete({@required this.loadItemResult}) : assert(loadItemResult != null);

  @override
  List<Object> get props => [loadItemResult];
}

class LoadItemFailure extends SettingsState {
  final String error;

  const LoadItemFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadPackingInitial extends SettingsState{}

class LoadPackingInProgress extends SettingsState{}

class LoadPackingComplete extends SettingsState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingFailure extends SettingsState {
  final String error;

  const LoadPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing by packing id

class LoadPackingByIdInitial extends SettingsState{}

class LoadPackingByIdInProgress extends SettingsState{}

class LoadPackingByIdComplete extends SettingsState{
  final List<ItemPackingData> loadPackingResult;

  LoadPackingByIdComplete({@required this.loadPackingResult}) : assert(loadPackingResult != null);

  @override
  List<Object> get props => [loadPackingResult];
}

class LoadPackingByIdFailure extends SettingsState {
  final String error;

  const LoadPackingByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemFailure { error : $error }';
}


/// Load packing list

class LoadItemByPackingInitial extends SettingsState{}

class LoadItemByPackingInProgress extends SettingsState{}

class LoadItemByPackingComplete extends SettingsState{
  final List<ItemBarcodeData> loadItemByPackingResult;

  LoadItemByPackingComplete({@required this.loadItemByPackingResult}) : assert(loadItemByPackingResult != null);

  @override
  List<Object> get props => [loadItemByPackingResult];
}

class LoadItemByPackingFailure extends SettingsState {
  final String error;

  const LoadItemByPackingFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadItemByPackingFailure { error : $error }';
}


/// Load country by id

class LoadCountryByIdInitial extends SettingsState{}

class LoadCountryByIdInProgress extends SettingsState{}

class LoadCountryByIdComplete extends SettingsState{
  final List<CountryOriginData> loadCountryResult;

  LoadCountryByIdComplete({@required this.loadCountryResult}) : assert(loadCountryResult != null);

  @override
  List<Object> get props => [loadCountryResult];
}

class LoadCountryByIdFailure extends SettingsState {
  final String error;

  const LoadCountryByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadCountryByIdFailure { error : $error }';
}


/// Load brand by id

class LoadBrandByIdInitial extends SettingsState{}

class LoadBrandByIdInProgress extends SettingsState{}

class LoadBrandByIdComplete extends SettingsState{
  final List<ProductBrandData> loadBrandResult;

  LoadBrandByIdComplete({@required this.loadBrandResult}) : assert(loadBrandResult != null);

  @override
  List<Object> get props => [loadBrandResult];
}

class LoadBrandByIdFailure extends SettingsState {
  final String error;

  const LoadBrandByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBrandByIdFailure { error : $error }';
}


/// Get WareHouse

class GetWareHouseInitial extends SettingsState{}

class GetWareHouseInProgress extends SettingsState{}

class GetWareHouseComplete extends SettingsState{
  final GetCommonResp getCommonResp;

  GetWareHouseComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseFailure extends SettingsState {
  final String error;

  const GetWareHouseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseFailure { error : $error }';
}


/// Get WarehouseZone

class GetWareHouseZoneInitial extends SettingsState{}

class GetWareHouseZoneInProgress extends SettingsState{}

class GetWareHouseZoneComplete extends SettingsState{
  final GetCommonResp getCommonResp;

  GetWareHouseZoneComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseZoneFailure extends SettingsState {
  final String error;

  const GetWareHouseZoneFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseZoneFailure { error : $error }';
}


/// Get WarehouseRack

class GetWareHouseRackInitial extends SettingsState{}

class GetWareHouseRackInProgress extends SettingsState{}

class GetWareHouseRackComplete extends SettingsState{
  final GetCommonResp getCommonResp;

  GetWareHouseRackComplete({@required this.getCommonResp}) : assert(getCommonResp != null);

  @override
  List<Object> get props => [getCommonResp];
}

class GetWareHouseRackFailure extends SettingsState {
  final String error;

  const GetWareHouseRackFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseRackFailure { error : $error }';
}


/// Get WarehouseBin

class GetWareHouseBinInitial extends SettingsState{}

class GetWareHouseBinInProgress extends SettingsState{}

class GetWareHouseBinComplete extends SettingsState{
  final GetWarehouseBinMasterResp getWarehouseBinMasterResp;

  GetWareHouseBinComplete({@required this.getWarehouseBinMasterResp}) : assert(getWarehouseBinMasterResp != null);

  @override
  List<Object> get props => [getWarehouseBinMasterResp];
}

class GetWareHouseBinFailure extends SettingsState {
  final String error;

  const GetWareHouseBinFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetWareHouseBinFailure { error : $error }';
}


/// Get Packing Details
//
// class GetPackingDetailsInitial extends SettingsState{}
//
// class GetPackingDetailsInProgress extends SettingsState{}
//
// class GetPackingDetailsComplete extends SettingsState{
//   final GetScmItemPriceDetailsResp getPackingDetailsResp;
//
//   GetPackingDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);
//
//   @override
//   List<Object> get props => [getPackingDetailsResp];
// }
//
// class GetPackingDetailsFailure extends SettingsState {
//   final String error;
//
//   const GetPackingDetailsFailure({@required this.error});
//
//   @override
//   List<Object> get props => [error];
//
//   @override
//   String toString() => 'GetPackingDetailsFailure { error : $error }';
// }


/// LoadBatchById States

class LoadBatchByIdInitial extends SettingsState{}

class LoadBatchByIdInProgress extends SettingsState{}

class LoadBatchByIdComplete extends SettingsState{
  final List<ItemBatchData> getItemBatchResp;

  LoadBatchByIdComplete({@required this.getItemBatchResp}) : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class LoadBatchByIdFailure extends SettingsState {
  final String error;

  const LoadBatchByIdFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoadBatchByIdFailure { error : $error }';
}



class GetReasonInitial extends SettingsState {}

class GetReasonInProgress extends SettingsState {}

class GetReasonComplete extends SettingsState {
  final GetCommonResp getReasonResp;

  GetReasonComplete({@required this.getReasonResp}) : assert(getReasonResp != null);

  @override
  List<Object> get props => [getReasonResp];
}

class GetReasonFailure extends SettingsState {
  final String error;

  const GetReasonFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetReasonFailure { error : $error }';
}


/// GetItemPriceDetails States
class GetScmItemPriceDetailsInitial extends SettingsState{}

class GetScmItemPriceDetailsInProgress extends SettingsState{}

class GetScmItemPriceDetailsComplete extends SettingsState{
  final GetScmItemPriceDetailsResp getPackingDetailsResp;

  GetScmItemPriceDetailsComplete({@required this.getPackingDetailsResp}) : assert(getPackingDetailsResp != null);

  @override
  List<Object> get props => [getPackingDetailsResp];
}

class GetScmItemPriceDetailsFailure extends SettingsState {
  final String error;

  const GetScmItemPriceDetailsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetScmItemPriceDetailsFailure { error : $error }';
}