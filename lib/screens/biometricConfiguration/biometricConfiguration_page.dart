import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/sizes_helpers.dart';
import 'package:bcore_inventory_management/common_widget/app_bar.dart';
import 'package:bcore_inventory_management/common_widget/bCoreButton.dart';
import 'package:bcore_inventory_management/screens/configurationComplete/configurationComplete_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BiometricConfiguration extends StatefulWidget {

  @override
  _BiometricConfigurationState createState() => _BiometricConfigurationState();
}

class _BiometricConfigurationState extends State<BiometricConfiguration> {
  int tapCount = 0;
  String userPin;
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  double contentPadding;
  bool shouldDisplaySkip = true;

  Future<void> _authenticateUser() async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason:
        "Please authenticate to configure",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    isAuthenticated
        ? print('User is authenticated!')
        : print('User is not authenticated.');

    if (isAuthenticated) {

      changeFPColor();
      storeFpCredentials();
    } else{
      Common().showAlertMessageWithAction(
          context: context,
          title: "Failed",
          message: 'Please try again',
          okButtonTitle: "Skip",
          okFunction: (){
            Navigator.pop(context);
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
          },
          actionButtonTitle: "Retry",
          actionFunction: (){
            Navigator.pop(context);
            Future.delayed(Duration(seconds: 1),() {
              _authenticateUser();
            });
          }
      );
//      Common().showAlertMessage(
//          context: context,
//          title:'Failed',
//          message: 'Please try again',
//          okFunction: (){
//            Navigator.pop(context);
//            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
//          }
//      );
    }
  }

  storeFpCredentials() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("FpUPin", pref.getString("PinToStore") ?? "");
    clearTempPin();
  }

  clearTempPin() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove("PinToStore");
  }

  changeFPColor(){
    setState(() {
      tapCount++;
      shouldDisplaySkip = false;
    });
  }

  @override
  void initState(){
    super.initState();

    Future.delayed(Duration(seconds: 1),() {
      _authenticateUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    contentPadding = (safeDisplayWidth(context) <= 320) ? 16.0 : 32.0 ;
    return Scaffold(
      appBar: BCAppBar.setInitialAppBar(),
      body: Container(
        color: Colors.white,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: contentPadding,right: contentPadding),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Image.asset("assets/common/bCore_logo/bCore_logo.png"),
                  SizedBox(height: (safeDisplayHeight(context) > 568) ? 72 : 36),
                  Text("Place your finger on Fingerprint Scanner to Configure", style: TextStyle(fontSize: 13, color: Color(hexToColor("#221F1F"))),textAlign: TextAlign.center,),
                  Image.asset( (tapCount == 0)
                      ? "assets/biometricConfiguration/biometricConfiguration_fpUnverified_icon/biometricConfiguration_fpUnverified_icon.png"
                      : "assets/biometricConfiguration/biometricConfiguration_fpVerified_icon/biometricConfiguration_fpVerified_icon.png",
                    height: (safeDisplayHeight(context) > 568) ? 210.0 : 168.0,
                  ),
                  BCoreButton(
                      title: 'NEXT',
                      onPressed: (){
                        if (tapCount==1) {
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (
                              context) => ConfigurationComplete()));
                        }else{
                          Common().showAlertMessage(
                              context: context,
                              title:'Failed',
                              message: 'Please try again',
                              okFunction: (){
                                Navigator.pop(context);
                              }
                          );
                        }
                      }
                  ),
                  SizedBox(height: 24),
                  if (shouldDisplaySkip)
                    FlatButton(
                        padding: EdgeInsets.only(left: 16.0,right: 0.0),
                        onPressed: (){
                          clearTempPin();
                          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
//                        Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                        },
                        child: Text("SKIP FINGERPRINT SETUP",
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Color(hexToColor("#CEA406"),
                                )
                            )
                        )
                    ),
//                  SizedBox(height: 20)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}
