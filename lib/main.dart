import 'package:bcore_inventory_management/repository/bCore_api_client.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/splash/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'app_config/bcAppTheme.dart';
import 'authentication/authentication_bloc.dart';
import 'authentication/authentication_event.dart';
import 'loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/data/database.dart';

class BCoreBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  // @override
  // void onError(Cubit cubit, Object error, StackTrace stackTrace) {
  //   print(error);
  //   super.onError(cubit, error, stackTrace);
  // }
}
//void main() => runApp(new SampleApp());

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = BCoreBlocDelegate();
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  final BCRepository repository = BCRepository(
    bcApiClient: BCApiClient(
      httpClient: http.Client(),
    ),
  );

  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) {
        return AuthenticationBloc()..add(AuthenticationStarted());
      },
      child: Provider<Database>(
        create: (context) => Database(),
        child: MyApp(bcRepository: repository, appLanguage: appLanguage),
        dispose: (context, db) => db.close(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  final BCRepository bcRepository;
  final AppLanguage appLanguage;

  const MyApp({Key key, @required this.bcRepository, this.appLanguage})
      : assert(bcRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppLanguage>(
      create: (BuildContext context) {
        return appLanguage;
      },
      child: Consumer<AppLanguage>(builder: (context, model, child) {
        return GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: MaterialApp(
              /* locale: model.appLocal,
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            Locale('en', 'US'),
            Locale('ar', ''),
          ],*/
              home:
                  Splash(bcRepository: bcRepository, appLanguage: appLanguage),
              title: 'Business Core',
              debugShowCheckedModeBanner: false,
              theme: BCAppTheme(languageCode: appLanguage.appLocal.languageCode)
                  .themeData,
            ));
      }),
    );
  }
}
