import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:flutter/material.dart';

class BCoreButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;

  BCoreButton({@required this.onPressed, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
        ),
        boxShadow: [
          BoxShadow(
            color: Color(Utils.hexToColor("#A88D2A")).withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 10,
            offset: Offset(-1, 15), // changes position of shadow
          ),
        ],
      ),
      child: RaisedButton(
          padding: EdgeInsets.only(right: 4,left: 4, top: 4,bottom: 4),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50.0))),
          onPressed: onPressed,
          textColor: Colors.white,
          color: BCAppTheme().headingTextColor,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Container(
                  height: 42,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      FittedBox(
                        fit: BoxFit.contain,
                        child: Text(title,
                            style: TextStyle(color: Colors.white,fontSize: 16))
                        ),
                    ],
                  ),
                ),
              ),
             /* Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset("assets/initialLogin/initialLogin_configureLogin_icon/initialLogin_configureLogin_icon.png")
              ),*/
            ],
          )
      ),
    );
  }
}
