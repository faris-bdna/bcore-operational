
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_step.dart';
import 'package:bcore_inventory_management/common_widget/Cool_Stepper/models/cool_stepper_config.dart';
import 'package:flutter/material.dart';

class CoolStepperView extends StatelessWidget {
  final CoolStep step;
  final VoidCallback onStepNext;
  final VoidCallback onStepBack;
  final EdgeInsetsGeometry contentPadding;
  final EdgeInsetsGeometry headerPadding;
  final CoolStepperConfig config;

  const CoolStepperView({
    Key key,
    @required this.step,
    this.onStepNext,
    this.onStepBack,
    this.contentPadding,
    this.config,
    this.headerPadding,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final title = Container(
      width: double.infinity,
//      margin: EdgeInsets.only(bottom: 20.0),
      padding: headerPadding ?? EdgeInsets.all(8.0),
//      decoration: BoxDecoration(
//        color: config.headerColor ??
//            Theme.of(context).primaryColor.withOpacity(0.1),
//      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (step.title == null ) ? SizedBox() : Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(
                  step.title,
                  style: config.titleTextStyle ??
                      TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black38,
                      ),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
              ),
//              SizedBox(width: 5.0),
//              Visibility(
//                  visible: config.icon == null,
//                  child: Icon(
//                    Icons.help_outline,
//                    size: 18,
//                    color: config.iconColor ?? Colors.black38,
//                  ),
//                  replacement: config.icon ?? Container())
            ],
          ),
          (step.title == null || step.subContent == null) ? SizedBox() : SizedBox(height: 8.0),
          (step.subContent == null) ? SizedBox() : step.subContent
        ],
      ),
    );

    final content = Expanded(
      child: SingleChildScrollView(
        padding: contentPadding,
        child: step.content,
      ),
    );

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          title,
          content
        ],
      ),
    );
  }
}
