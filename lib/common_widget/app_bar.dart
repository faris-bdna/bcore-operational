
import 'package:bcore_inventory_management/screens/myConfiguration/LocalDBSync.dart';
import 'package:bcore_inventory_management/screens/myConfiguration/MyConfiguration.dart';
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/common/common.dart';
import 'package:bcore_inventory_management/common/utils.dart';
import 'package:bcore_inventory_management/common/master_util/master_bloc.dart';
import 'package:bcore_inventory_management/common/network_util/bloc.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart' ;
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moor/moor.dart';
// import 'package:moor_db_viewer/moor_db_viewer.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BCAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> widgets;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final bool isBack;
  final AppBar appBar;
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;
  final shouldHideActions;

  GetItemBarcodeMasterResp getItemBarcodeMasterResp =
  new GetItemBarcodeMasterResp();
  GetItemPackingMasterResp getItemPackingMasterResp =
  new GetItemPackingMasterResp();
  GetItemBatchMasterResp getItemBatchMasterResp = new GetItemBatchMasterResp();
  GetCommonResp getCommonResp = new GetCommonResp();

  String lastFetchDate="";
  ScmMasters masterToFetch;
  bool masterSaved;
  SharedPreferences pref;

  MasterBloc masterBlocProvider;

  BCAppBar(
      {this.title,
        this.widgets,
        this.scaffoldKey,
        this.isBack,
        this.appBar,
        this.repository,
        this.appLanguage,
        this.database,
        this.shouldHideActions});


  /// this app bar using on the initial setup screens,
  static setInitialAppBar(
      {String title = "", List<Widget> widgets = const <Widget>[]}) {
    return AppBar(
      primary: true,
      elevation: 0.0,
      title: Text(
        title,
        style: TextStyle(
          fontSize: 16.0,
          color: BCAppTheme().headingTextColor,
        ),
      ),

      centerTitle: true,
      iconTheme: IconThemeData(color: BCAppTheme().headingTextColor),
      actions: widgets,
      backgroundColor: BCAppTheme().secondaryColor,
      // automaticallyImplyLeading: false,
    );
  }


  storeLastFetchDate(String fetchDate) async {
    pref = await SharedPreferences.getInstance();
    pref.setString("lastFetchDate", fetchDate);
  }

  getLastFetchDate() async {
    pref = await SharedPreferences.getInstance();
    lastFetchDate =   pref.get("lastFetchDate") != null ? Utils.getDateByFormat(
        format: fromAddress.fromCompanyDateFormat,
        passedDateTime:  /*DateTime.now())*/ DateTime.parse(pref.get("lastFetchDate"))) : '';
    print(lastFetchDate);
  }

  void setMasterToFetch(ScmMasters scmMaster) {
    masterToFetch = scmMaster;
  }

  @override
  Widget build(BuildContext context) {
    getLastFetchDate();
    print("lastFetchDate $lastFetchDate");
    ScmMasters loadedMaster;
    bool con = true;
    masterBlocProvider =  MasterBloc(bcRepository: repository);
    return MultiBlocProvider(
        providers: [
          BlocProvider<MasterBloc>(
            create: (BuildContext context) =>
            masterBlocProvider/*..add(GetItemBarcodeMaster())*/,
          ),
          BlocProvider<NetworkBloc>(
            create: (BuildContext context) =>
            NetworkBloc()..add(ListenConnection()),
          ),
        ],
        child: MultiBlocListener(
            listeners: [
              BlocListener<MasterBloc, MasterState>(
                listener: (context, state) async{
                  if (state is GetItemBarcodeMasterInProgress ||
                      state is GetItemBatchMasterInProgress ||
                      state is GetCommonMasterInProgress ||
                      state is GetItemPackingMasterInProgress) {
                    Common().showDownloadStatus(context,"Master Data Downloading...");
                  }
                  else if (state is GetItemBarcodeMasterLoaded) {
                    var db = Provider.of<Database>(context, listen: false);
                    getItemBarcodeMasterResp = state.getItemBarcodeResp;
                    if(getItemBarcodeMasterResp.masterDataList.isNotEmpty)
                   await saveItemBarcodeMaster(getItemBarcodeMasterResp, db);
                    Navigator.pop(context);
                    BlocProvider.of<MasterBloc>(context).add(GetItemPackingMaster(
                        companyId: fromAddress.fromCompanyId,
                        divisionId: fromAddress.fromDivisionId,
                        locationId: fromAddress.fromLocationId,
                        supplierId: '',
                        lastFetchDate: lastFetchDate ));
                  }
                  else if (state is GetItemPackingMasterLoaded) {
                    var db = Provider.of<Database>(context, listen: false);
                    getItemPackingMasterResp = state.getItemPackingResp;
                    if(getItemPackingMasterResp.masterDataList.isNotEmpty)
                    await savePackingMaster(getItemPackingMasterResp, db);
                    Navigator.pop(context);
                    BlocProvider.of<MasterBloc>(context).add(GetItemBatchMaster(
                        companyId: fromAddress.fromCompanyId,
                        divisionId: fromAddress.fromDivisionId,
                        locationId: fromAddress.fromLocationId,
                        supplierId: '',
                        lastFetchDate: lastFetchDate));
                  }
                  else if (state is GetItemBatchMasterLoaded) {

                    var db = Provider.of<Database>(context, listen: false);
                    getItemBatchMasterResp = state.getItemBatchResp;
                    if(getItemBatchMasterResp.masterDataList.isNotEmpty)
                    await saveBatchMaster(getItemBatchMasterResp, db);
                    // Store date after saving main masters(Item Barcode, Item Packing and Item Batch)
                    // Set next master to fetch as country of origin master
                    Navigator.pop(context);
                    setMasterToFetch(ScmMasters.CountryOfOrigin);
                    BlocProvider.of<MasterBloc>(context)
                        .add(GetCommonMaster(scmMaster: masterToFetch));
                  }

                  /// Country Of Origin master loaded state
                  else if (state is GetCountryOfOriginMasterLoaded) {
                    var db = Provider.of<Database>(context, listen: false);
                    getCommonResp = state.getCommonResp;
                    loadedMaster = state.scmMaster;
                    if(getCommonResp.masterDataList.isNotEmpty)
                    await saveCountryOfOriginMaster(getCommonResp, db);
                    Navigator.pop(context);
                    // saveCommonMaster(getCommonResp, db, masterToFetch);
                    // Save common master to database
                    // Set next master to fetch as country of origin master
                    if (loadedMaster == ScmMasters.CountryOfOrigin) {
                      setMasterToFetch(ScmMasters.ProductBrand);
                      BlocProvider.of<MasterBloc>(context)
                          .add(GetCommonMaster(scmMaster: masterToFetch));
                    }
                  }

                  /// Product brand master loaded state
                  else if (state is GetProductBrandMasterLoaded) {
                    var db = Provider.of<Database>(context, listen: false);
                    getCommonResp = state.getCommonResp;
                    loadedMaster = state.scmMaster;
                    if(getCommonResp.masterDataList.isNotEmpty)
                    await saveProductBrandMaster(getCommonResp, db);
                    Navigator.pop(context);
                    // saveCommonMaster(getCommonResp, db, masterToFetch);
                    // Save common master to database
                    // Set next master to fetch as country of origin master
                    if (loadedMaster == ScmMasters.ProductBrand) {
                      storeLastFetchDate(DateTime.now().toString());
                      Common().showAlertMessage(
                          context: context,
                          title: 'Success',
                          message: 'Data download success.',
                          okFunction: () {
                            Navigator.pop(context);
                          });
                    }
                  }
                  else if (state is GetItemBarcodeMasterFailure) {
                    Navigator.pop(context);
                    Common().showAlertMessage(
                        context: context,
                        title: 'Failed',
                        message: 'Barcode Data download failed.',
                        okFunction: () {
                          Navigator.pop(context);
                        });
                  }
                  else if (state is GetItemPackingMasterFailure) {
                    Navigator.pop(context);
                    Common().showAlertMessage(
                        context: context,
                        title: 'Failed',
                        message: 'Packing Data download failed.',
                        okFunction: () {
                          Navigator.pop(context);
                        });
                  }
                  else if (state is GetItemBatchMasterFailure) {
                    Navigator.pop(context);
                    Common().showAlertMessage(
                        context: context,
                        title: 'Failed',
                        message: 'Batch Data download failed.',
                        okFunction: () {
                          Navigator.pop(context);
                        });
                  }
                  else if (state is GetCommonMasterFailure) {
                    Navigator.pop(context);
                    Common().showAlertMessage(
                        context: context,
                        title: 'Failed',
                        message: 'Master data download failed.',
                        okFunction: () {
                          Navigator.pop(context);
                        });
                  }
                },
              ),
              BlocListener<NetworkBloc, NetworkState>(
                listener: (context, state) {
                  if (state is ConnectionFailure) {
                    con = false;
                  }
                  if (state is ConnectionSuccess) {
                    con = true;
                  }
                },
              ),
            ],
            child: BlocProvider(
                create: (context) => NetworkBloc()..add(ListenConnection()),
                child: BlocBuilder<NetworkBloc, NetworkState>(
                    builder: (context, state) {
                      return AppBar(
                        primary: true,
                        elevation: 1.0,
                        title: Text(
                          title,textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: BCAppTheme().headingTextColor,
                          ),
                        ),
                        shadowColor: BCAppTheme().primaryColor,
                        leading: isBack
                            ? IconButton(
                            icon: new Image.asset(
                                'assets/common/menu/menu.png'),
                            onPressed: () {
                              if (scaffoldKey.currentState.isDrawerOpen ==
                                  false) {
                                scaffoldKey.currentState.openDrawer();
                              } else {
                                scaffoldKey.currentState.openEndDrawer();
                              }
                            })
                            : BackButton(),
                        centerTitle: false,
                        iconTheme:
                        IconThemeData(color: BCAppTheme().headingTextColor),
                        // actions: widgets,
                        actions: shouldHideActions
                            ? []
                            : <Widget>[
                          IconButton(icon: Icon(Icons.sync), onPressed: (){
                      Navigator.of(context).push(
                      MaterialPageRoute(
                      builder: (context) =>
                      LocalDBSync(
                      repository: repository,
                      appLanguage: appLanguage,
                      database: database,
                      )));

                         /*   print(lastFetchDate);
                            masterBlocProvider.add(
                                GetItemBarcodeMaster(
                                    companyId: fromAddress.fromCompanyId,
                                    divisionId: fromAddress.fromDivisionId,
                                    locationId: fromAddress.fromLocationId,
                                    supplierId: '0',
                                    lastFetchDate: lastFetchDate));*/
                          }),
                          IconButton(icon: Icon(Icons.settings), onPressed: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        FromAddressForm(
                                          repository: repository,
                                          appLanguage: appLanguage,
                                          database: database,
                                        )));
                          }),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                            child: con ? Icon(Icons.wifi) :Icon(Icons.wifi_off),
                          ),
                        ],
                        backgroundColor: BCAppTheme().secondaryColor,
                      );
                    }
                )))
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);

   saveItemBarcodeMaster(
      GetItemBarcodeMasterResp getItemBarcodeMasterResp, Database db) async{
    ItemBarcodeData barcodeData;
    await db.itemPackingDao.batch((b) {
    for (var item in getItemBarcodeMasterResp.masterDataList) {
      barcodeData = ItemBarcodeData(
          Text: item.text,
          ItemBarcodeId: item.value,
          ProductGroupId: item.productGroupId,
          ProductId: item.productId,
          ProductName: item.productName,
          ItemId: item.itemId,
          ItemCode: item.itemCode,
          ItemName: item.itemName,
          ItemPackingId: item.itemPackingId,
          MajorPackId: item.majorPackId,
          BasePackId: item.basePackId,
          PkgQty: item.pkgQty.toString(),
          ScaleGroupId: item.scaleGroupId,
          BrandId: item.brandId,
          OriginCountryId: item.originCountryId,
          ProductStockTypeId: item.productStockTypeId,
          MinShelfLife: item.minShelfLife,
          TotalShelfLife: item.totalShelfLife,
          IsSalesAllowed: item.isSalesAllowed,
          StockAllocTypeId: item.stockAllocTypeId,
          IsTaxApplicable: item.isTaxApplicable,
          ItemStatus: item.itemStatus,
          pur_restriction: item.purchaseRestriction,
          IsPurchaseAllowed: item.isPurchaseAllowed,
          ExcessQty: item.excessQty.toString(),
          IsMinOrderQtyApp: item.isMinOrderQtyApp,
          MinOrderQty: item.minOrderQty.toString(),
          SuppItemCode: item.suppItemCode,
          SuppTypeId: 0,//item.suppTypeId,
          TaxPercent: item.taxPercent.toString(),
          IsBatchEnabled: item.isBatchEnabled,
          HasPurchaseGroupAccess: item.hasPurchaseGroupAccess,
          StockBasedItem: item.stockBasedItem,
          ConsignmentItem: item.consignmentItem,
          LastPurchaseQty: item.lastPurchaseQty.toString(),
          LastPurchaseDate: item.lastPurchaseDate,
          LastReceivedAmount: item.lastReceivedAmount.toString(),
          LastReceivedSupplier: item.lastReceivedSupplier,
          AverageCost: item.averageCost.toString(),
          AvailableQty: item.availableQty.toString(),
          NetCost: item.netCost.toString());

      try {
       b.update(db.itemBarcode, barcodeData);
      } catch (e) {
        print(e); // this line will be printed if there is any errors
      }
    }
    });
  }

   savePackingMaster(
      GetItemPackingMasterResp getItemPackingMasterResp, Database db) async{
    
    ItemPackingData packingData;
    await db.itemPackingDao.batch((b) {
    for (var item in getItemPackingMasterResp.masterDataList) {
      packingData = ItemPackingData(
          packing_text: item.text,
          packing_value: item.batch_value,
          item_id: item.itemId);
      b.update(db.itemPacking, packingData);
    }
    });
  }

   saveBatchMaster(
      GetItemBatchMasterResp getItemBatchMasterResp, Database db)async {
    ItemBatchData batchData;
    await db.itemPackingDao.batch((b) {
    for (var item in getItemBatchMasterResp.masterDataList) {
      batchData = ItemBatchData(
          batch_text: item.text,
          batch_value: item.value,
          item_id: item.itemId,
          prod_date: item.prodDate,
          expiry_date: item.expiryDate);
      b.update(db.itemBatch, batchData);
    }
   });
  }

   saveProductBrandMaster(GetCommonResp getCommonResp, Database db)async {
    ProductBrandData productBrandData;
    await db.itemPackingDao.batch((b) {
    for (var item in getCommonResp.masterDataList) {
      productBrandData =
          ProductBrandData(brand_name: item.text, brand_id: item.value);
      b.update(db.productBrand, productBrandData);
    }
    });
  }

   saveCountryOfOriginMaster(GetCommonResp getCommonResp, Database db)async {
    CountryOriginData countryOriginData;
    await db.itemPackingDao.batch((b) {
    for (var item in getCommonResp.masterDataList) {
      countryOriginData =
          CountryOriginData(country_name: item.text, country_id: item.value);
      b.update(db.countryOrigin, countryOriginData);
    }
    });
  }
}
