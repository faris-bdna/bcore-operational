library pin_entry_text_field;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PinEntryTextField extends StatefulWidget {
  final String lastPin;
  final int fields;
  final ValueChanged<String> onSubmit;
  final num fieldWidth;
  final num fontSize;
  final bool isTextObscure;
  final bool showFieldAsBox;
  final cursorColor;  // Leaving the data type dynamic for adding Color or Material Color
  final boxColor;
  final textColor;

  PinEntryTextField(
      {this.lastPin,
        this.fields: 4,
        this.onSubmit,
        this.fieldWidth: 40.0,
        this.fontSize: 18.0,
        this.isTextObscure: false,
        this.showFieldAsBox: false,
        this.cursorColor: Colors.blue,  // Adding a Material Color so that if the user want black, it get accepted too
        this.boxColor: Colors.blue,
        this.textColor: Colors.blue,

      })
      : assert(fields > 0);

  @override
  State createState() {
    return PinEntryTextFieldState();
  }
}

class PinEntryTextFieldState extends State<PinEntryTextField> {
  List<String> _pin;
  List<FocusNode> _focusNodes;
  List<TextEditingController> _textControllers;

  Widget textFields = Container();

  @override
  void initState() {
    super.initState();
    _pin = List<String>(widget.fields);
    _focusNodes = List<FocusNode>(widget.fields);
    _textControllers = List<TextEditingController>(widget.fields);

//  Called one time just after the layout has been completed
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
//        Splits the last pin to pin array(_pin)
        if (widget.lastPin != null) {
          for (var i = 0; i < widget.lastPin.length; i++) {
            _pin[i] = widget.lastPin[i];
          }
        }
//        create container with textfields and assign
        textFields = generateTextFields(context);
      });
    });
  }

  @override
  void dispose() {
    _textControllers.forEach((TextEditingController t) => t.dispose());
    super.dispose();
  }

  Widget generateTextFields(BuildContext context) {
    // Creates list of required no of text fields in container
    List<Widget> textFields = List.generate(widget.fields, (int i) {
      return buildTextField(i, context, i == 0);
    });

    // if there is no value entered currently the focus set to first text field
    if (_pin.first == null) {
      FocusScope.of(context).requestFocus(_focusNodes[0]);
    }

//    returns row with text fields(contained in container)
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        verticalDirection: VerticalDirection.down,
        children: textFields);
  }

  void clearTextFields() {
    _textControllers.forEach(
        (TextEditingController tEditController) => tEditController.clear());
    _pin.clear();
  }

  Widget buildTextField(int i, BuildContext context, [bool autofocus = false]) {
    // Creates focus node and assign on array if the corresponding one is not created already
    if (_focusNodes[i] == null) {
      _focusNodes[i] = FocusNode();
    }
    // Creates TDC if the corresponding one is not created already
    if (_textControllers[i] == null) {
      _textControllers[i] = TextEditingController();
      // if lastpin contains some value, the corresponding value is assigned on related textfield
      if (widget.lastPin != null) {
        _textControllers[i].text = widget.lastPin[i];
      }
    }

//    _focusNodes[i].addListener(() {
//      if (_focusNodes[i].hasFocus) {
//
//      }
//    }
//    );

    final String lastDigit = _textControllers[i].text;

    return Container(
      width: widget.fieldWidth,
      height: widget.fieldWidth,
//      color: Colors.lightGreen,
      padding: EdgeInsets.all(0),
      child: TextField(
        controller: _textControllers[i],
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        textAlignVertical: TextAlignVertical.bottom,
        cursorColor: widget.cursorColor,
        showCursor: false,
        obscuringCharacter: "●",//"⬤",
        maxLength: 2,
        autofocus: autofocus,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: widget.boxColor,
            fontSize: widget.fontSize),
        focusNode: _focusNodes[i],
        obscureText: widget.isTextObscure,
        decoration: InputDecoration(
//          border: InputBorder.none,
          hintText: "〇",
            hintStyle: TextStyle(color: Colors.grey),
            fillColor: widget.textColor,
            counterText: "",
            enabledBorder : widget.showFieldAsBox
                  ? (( (_pin[i] == null) ||  (_pin[i] == "")) ?
                  OutlineInputBorder(borderSide: BorderSide(width: 1.0,color: Colors.grey))
                : OutlineInputBorder(borderSide: BorderSide(width: 1.0,color: widget.boxColor)))
                : null ,
            focusedBorder:widget.showFieldAsBox
                ? OutlineInputBorder(borderSide: BorderSide(width: 1.0,color: widget.boxColor))
                : null) ,
        onChanged: (String newStr) {
          setState(() {
//            checks the first item is not null, pin (array) with current index is not null, new entered value is not null and current index is not the last field
            if (((_pin.first != null) && (_pin.first!="")) && ((_pin[i] != null) || (_pin[i] != "")) && (newStr != "") && (i + 1 != widget.fields)){
              _textControllers[i].text = _pin[i];
              _textControllers[i].selection = TextSelection.collapsed(offset: _textControllers[i].text.length);
              _textControllers[i+1].text = newStr[1];
              _textControllers[i+1].selection = TextSelection.collapsed(offset: _textControllers[i+1].text.length);
              _pin[i+1] = newStr[1];

//              _focusNodes[i].unfocus();
              FocusScope.of(context).unfocus();
              if((i+2)!= widget.fields) {
                FocusScope.of(context).requestFocus(_focusNodes[i + 1]);
              }
//              _focusNodes[i + 1].requestFocus();
            }
            else{
              _pin[i] = newStr;
            }
              textFields = generateTextFields(context);
          });

          // checks whether the value entered field is not the last one
          if (i + 2 != widget.fields) {
          // removes the focus from current value entered field
//            _focusNodes[i].unfocus();
            // checks whether user actually entered a '' and not null (checks whether user has actually cleared this text field value or not)
            if (lastDigit != null && _pin[i] == '') {
              if ((i-1)>=0) {
                // change focus to previous text field
                FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
              }else{
                FocusScope.of(context).requestFocus(_focusNodes[0]);
              }
            } else {
              // change focus to next textfield
//              FocusScope.of(context).requestFocus(_focusNodes[i + 1]);
            }
          } else {
            _focusNodes[i].unfocus();

            // checks whether user actually entered a '' and not null (checks whether user has actually cleared this text field value or not)
            if (lastDigit != null && _pin[i] == '') {
              FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
            }
          }

          // checks every text fields got entered digits
//          if (_pin.every((String digit) => digit != null && digit != '')) {
          List abc = _pin.where((val) => (val != null)).toList();
          widget.onSubmit(abc.join());
//          widget.onSubmit(_pin.join());
//          }
        },
        onSubmitted: (String str) {
          // checks every text fields got entered digits
          if (_pin.every((String digit) => digit != null && digit != '')) {
            widget.onSubmit(_pin.join());
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return textFields;
  }
}
