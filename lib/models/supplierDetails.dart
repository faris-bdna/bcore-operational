class SupplierDetails {
  String gRNDateTemp;
  int suppId;
  int currencyId;
  int currencyRate;
  bool isTaxApp;
  bool isQualityChecked;
  bool isCashPurchase;
  bool isInvoiceRcvd;
  String docDateTemp;
  String invoiceNo;
  String deliveryNoteNo;
  String remarks;

  SupplierDetails(
      {
        this.gRNDateTemp,
        this.suppId,
        this.currencyId,
        this.currencyRate,
        this.isTaxApp,
        this.isQualityChecked,
        this.isCashPurchase,
        this.isInvoiceRcvd,
        this.docDateTemp,
        this.invoiceNo,
        this.deliveryNoteNo,
        this.remarks
      });

  SupplierDetails.fromJson(Map<String, dynamic> json) {
    gRNDateTemp = json['gRNDateTemp'];
    suppId = json['SuppId'];
    currencyId = json['CurrencyId'];
    currencyRate = json['CurrencyRate'];
    isTaxApp = json['IsTaxApp'];
    isQualityChecked = json['IsQualityChecked'];
    isCashPurchase = json['IsCashPurchase'];
    isInvoiceRcvd = json['IsInvoiceRcvd'];
    docDateTemp = json['DocDateTemp'];
    invoiceNo = json['InvoiceNo'];
    deliveryNoteNo = json['DeliveryNoteNo'];
    remarks = json['Remarks'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gRNDateTemp'] = this.gRNDateTemp;
    data['SuppId'] = this.suppId;
    data['CurrencyId'] = this.currencyId;
    data['CurrencyRate'] = this.currencyRate;
    data['IsTaxApp'] = this.isTaxApp;
    data['IsQualityChecked'] = this.isQualityChecked;
    data['IsCashPurchase'] = this.isCashPurchase;
    data['IsInvoiceRcvd'] = this.isInvoiceRcvd;
    data['DocDateTemp'] = this.docDateTemp;
    data['InvoiceNo'] = this.invoiceNo;
    data['DeliveryNoteNo'] = this.deliveryNoteNo;
    data['Remarks'] = this.remarks;

    return data;
  }

  clear() {
    gRNDateTemp = null;
    suppId = null;
    currencyId = null;
    currencyRate = null;
    isTaxApp = null;
    isQualityChecked = null;
    isCashPurchase = null;

    isInvoiceRcvd = null;
    docDateTemp = null;
    invoiceNo = null;
    deliveryNoteNo = null;
    remarks = null;
  }
}

final supplierDetails = SupplierDetails();
