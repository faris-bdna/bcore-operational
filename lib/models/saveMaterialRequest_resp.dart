

import 'masters/getCompany_resp.dart';

class MaterialReqResp {
  String data;
  ValidationDetails validationDetails;

  MaterialReqResp({this.data, this.validationDetails});

  MaterialReqResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'];
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Data'] = this.data;
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}