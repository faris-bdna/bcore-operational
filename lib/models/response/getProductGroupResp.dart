import 'SalesItem/getItemPriceDetailsResp.dart';

class GetProductGroupResp {
  List<Level> level1;
  List<Level> level2;
  List<Level> level3;
  List<Level> level4;
  ValidationDetails validationDetails;

  GetProductGroupResp(
      {this.level1,
        this.level2,
        this.level3,
        this.level4,
        this.validationDetails});

  GetProductGroupResp.fromJson(Map<String, dynamic> json) {
    if (json['Level1'] != null) {
      level1 = new List<Level>();
      json['Level1'].forEach((v) {
        level1.add(new Level.fromJson(v));
      });
    }
    if (json['Level2'] != null) {
      level2 = new List<Level>();
      json['Level2'].forEach((v) {
        level2.add(new Level.fromJson(v));
      });
    }
    if (json['Level3'] != null) {
      level3 = new List<Level>();
      json['Level3'].forEach((v) {
        level3.add(new Level.fromJson(v));
      });
    }
    if (json['Level4'] != null) {
      level4 = new List<Level>();
      json['Level4'].forEach((v) {
        level4.add(new Level.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.level1 != null) {
      data['Level1'] = this.level1.map((v) => v.toJson()).toList();
    }
    if (this.level2 != null) {
      data['Level2'] = this.level2.map((v) => v.toJson()).toList();
    }
    if (this.level3 != null) {
      data['Level3'] = this.level3.map((v) => v.toJson()).toList();
    }
    if (this.level4 != null) {
      data['Level4'] = this.level4.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Level {
  int subGroupId;
  String subGroupName;
  int subGroupParentId;

  Level({this.subGroupId, this.subGroupName, this.subGroupParentId});

  Level.fromJson(Map<String, dynamic> json) {
    subGroupId = json['SubGroupId'];
    subGroupName = json['SubGroupName'];
    subGroupParentId = json['SubGroupParentId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SubGroupId'] = this.subGroupId;
    data['SubGroupName'] = this.subGroupName;
    data['SubGroupParentId'] = this.subGroupParentId;
    return data;
  }
}



