import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetStockReferenceDetailsResp {
  Data data;

  ValidationDetails validationDetails;

  GetStockReferenceDetailsResp({this.data, this.validationDetails});

  GetStockReferenceDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  String dateFormat;
  List<MasterDataList> masterDataList;

  Data({this.dateFormat, this.masterDataList});

  Data.fromJson(Map<String, dynamic> json) {
    dateFormat = json['DateFormat'];
    if (json['ItemDetails'] != null) {
      masterDataList = new List<MasterDataList>();
      json['ItemDetails'].forEach((v) {
        masterDataList.add(new MasterDataList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateFormat'] = this.dateFormat;
    if (this.masterDataList != null) {
      data['ItemDetails'] = this.masterDataList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MasterDataList {
  int itemId;
  String itemCode;
  String itemName;
  String itemBarcode;
  int packingId;
  String pkgName;
  String basePackId;
  String basePkgName;
  String packingDesc;
  double systemQty;
  double unitCost;
  double totalCost;
  String batchNo;
  String expiryDate;
  int reasonId;

  MasterDataList(
      {this.itemId,
        this.itemCode,
        this.itemName,
        this.itemBarcode,
        this.packingId,
        this.pkgName,
        this.basePackId,
        this.basePkgName,
        this.packingDesc,
        this.systemQty,
        this.unitCost,
        this.totalCost,
        this.batchNo,
        this.expiryDate,
        this.reasonId});

  MasterDataList.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    itemBarcode = json['ItemBarcode'];
    packingId = json['PackingId'];
    pkgName = json['PkgName'];
    basePackId = json['BasePackId'];
    basePkgName = json['BasePkgName'];
    packingDesc = json['PackingDesc'];
    systemQty = json['SystemQty'];
    unitCost = json['UnitCost'];
    totalCost = json['TotalCost'];
    batchNo = json['BatchNo'];
    expiryDate = json['ExpiryDate'];
    reasonId = json['ReasonId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['ItemBarcode'] = this.itemBarcode;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['BasePackId'] = this.basePackId;
    data['BasePkgName'] = this.basePkgName;
    data['PackingDesc'] = this.packingDesc;
    data['SystemQty'] = this.systemQty;
    data['UnitCost'] = this.unitCost;
    data['TotalCost'] = this.totalCost;
    data['BatchNo'] = this.batchNo;
    data['ExpiryDate'] = this.expiryDate;
    data['ReasonId'] = this.reasonId;
    return data;
  }
}