import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';


class GetStockReferenceNoResp {
  List<MasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetStockReferenceNoResp({this.masterDataList, this.validationDetails});

  GetStockReferenceNoResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<MasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new MasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class MasterDataList {
  String text;
  String value;
  int departmentId;
  int stockTypeId;
  String stockType;
  String warehouseName;
  String zoneName;
  String rackName;
  String binName;
  String productGroupName;
  String brandName;
  String itemName;

  MasterDataList(
      {this.text,
        this.value,
        this.departmentId,
        this.stockTypeId,
        this.stockType,
        this.warehouseName,
        this.zoneName,
        this.rackName,
        this.binName,
        this.productGroupName,
        this.brandName,
        this.itemName});

  MasterDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
    departmentId = json['DepartmentId'];
    stockTypeId = json['StockTypeId'];
    stockType = json['StockType'];
    warehouseName = json['WarehouseName'];
    zoneName = json['ZoneName'];
    rackName = json['RackName'];
    binName = json['BinName'];
    productGroupName = json['ProductGroupName'];
    brandName = json['BrandName'];
    itemName = json['ItemName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['DepartmentId'] = this.departmentId;
    data['StockTypeId'] = this.stockTypeId;
    data['StockType'] = this.stockType;
    data['WarehouseName'] = this.warehouseName;
    data['ZoneName'] = this.zoneName;
    data['RackName'] = this.rackName;
    data['BinName'] = this.binName;
    data['ProductGroupName'] = this.productGroupName;
    data['BrandName'] = this.brandName;
    data['ItemName'] = this.itemName;
    return data;
  }
}