class UpdateUserPinResponse {
  String statusMessage;
  int statusCode;

  UpdateUserPinResponse({this.statusMessage, this.statusCode});

  UpdateUserPinResponse.fromJson(Map<String, dynamic> json) {
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}