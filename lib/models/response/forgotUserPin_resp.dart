class ForgotUserPinResponse {
  String oTP;
  String expiresIn;
  String statusMessage;
  int statusCode;

  ForgotUserPinResponse(
      {this.oTP, this.expiresIn, this.statusMessage, this.statusCode});

  ForgotUserPinResponse.fromJson(Map<String, dynamic> json) {
    oTP = json['OTP'];
    expiresIn = json['ExpiresIn'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OTP'] = this.oTP;
    data['ExpiresIn'] = this.expiresIn;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}