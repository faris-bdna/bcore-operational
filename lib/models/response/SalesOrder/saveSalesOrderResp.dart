import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class SaveSalesResp {
  List<ErrorDetails> errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  SaveSalesResp(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  SaveSalesResp.fromJson(Map<String, dynamic> json) {
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}