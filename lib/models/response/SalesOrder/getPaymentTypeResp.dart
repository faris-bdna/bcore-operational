import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetPaymentTypeResp {
  List<PaymentTypeMasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetPaymentTypeResp({this.masterDataList, this.validationDetails});

  GetPaymentTypeResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<PaymentTypeMasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new PaymentTypeMasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class PaymentTypeMasterDataList {
  String text;
  String value;

  PaymentTypeMasterDataList({this.text, this.value});

  PaymentTypeMasterDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    return data;
  }
}