import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetSRRefDetailsResp {
  SRRefData data;
  ValidationDetails validationDetails;

  GetSRRefDetailsResp({this.data, this.validationDetails});

  GetSRRefDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new SRRefData.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SRRefData {
  String dateFormat;
  List<SRRefItemDetails> itemDetails;
  List<SerialNoDetails> serialNoDetails;

  SRRefData({this.dateFormat, this.itemDetails, this.serialNoDetails});

  SRRefData.fromJson(Map<String, dynamic> json) {
    dateFormat = json['DateFormat'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<SRRefItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new SRRefItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateFormat'] = this.dateFormat;

    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.serialNoDetails != null) {
      data['SerialNoDetails'] =
          this.serialNoDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SRRefItemDetails {
  int referenceDtId;
  int clientId;
  int lineNum;
  int itemId;
  String itemCode;
  String itemName;
  int packingId;
  String pkgName;
  String itemBarcode;
  double unitPrice;
  double unitQty;
  double netAmount;
  double availableQty;
  double pkgQty;
  String batchNo;
  double batchcost;
  int isAssetItem;
  String brandName;
  String countryName;
  int brandId;
  int countryId;
  String productionDate;
  String expiryDate;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int productStockTypeId;
  String packingDesc;

  SRRefItemDetails(
      {this.referenceDtId,
        this.clientId,
        this.lineNum,
        this.itemId,
        this.itemCode,
        this.itemName,
        this.packingId,
        this.pkgName,
        this.itemBarcode,
        this.unitPrice,
        this.unitQty,
        this.netAmount,
        this.availableQty,
        this.pkgQty,
        this.batchNo,
        this.batchcost,
        this.isAssetItem,
        this.brandName,
        this.countryName,
        this.brandId,
        this.countryId,
        this.productionDate,
        this.expiryDate,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.packingDesc,
        this.productStockTypeId});

  SRRefItemDetails.fromJson(Map<String, dynamic> json) {
    referenceDtId = json['ReferenceDtId'];
    clientId = json['ClientId'];
    lineNum = json['LineNum'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    packingId = json['PackingId'];
    pkgName = json['PkgName'];
    itemBarcode = json['ItemBarcode'];
    unitPrice = json['UnitPrice'];
    unitQty = json['UnitQty'];
    netAmount = json['NetAmount'];
    availableQty = json['AvailableQty'];
    pkgQty = json['PkgQty'];
    batchNo = json['BatchNo'];
    batchcost = json['batchcost'];
    isAssetItem = json['IsAssetItem'];
    brandName = json['BrandName'];
    countryName = json['CountryName'];
    brandId = json['BrandId'];
    countryId = json['CountryId'];
    productionDate = json['ProductionDate'];
    expiryDate = json['ExpiryDate'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    productStockTypeId = json['ProductStockTypeId'];

    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ReferenceDtId'] = this.referenceDtId;
    data['ClientId'] = this.clientId;
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['ItemBarcode'] = this.itemBarcode;
    data['UnitPrice'] = this.unitPrice;
    data['UnitQty'] = this.unitQty;
    data['NetAmount'] = this.netAmount;
    data['AvailableQty'] = this.availableQty;
    data['PkgQty'] = this.pkgQty;
    data['BatchNo'] = this.batchNo;
    data['batchcost'] = this.batchcost;
    data['IsAssetItem'] = this.isAssetItem;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['BrandId'] = this.brandId;
    data['CountryId'] = this.countryId;
    data['ProductionDate'] = this.productionDate;
    data['ExpiryDate'] = this.expiryDate;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['PackingDesc'] = this.packingDesc;
    return data;
  }
}

class SerialNoDetails {
  int dtLineNum;
  int gRNSerialNoId;
  int clientId;
  String serialNo;
  int itemId;

  SerialNoDetails(
      {this.dtLineNum,
        this.gRNSerialNoId,
        this.clientId,
        this.serialNo,
        this.itemId});

  SerialNoDetails.fromJson(Map<String, dynamic> json) {
    dtLineNum = json['DtLineNum'];
    gRNSerialNoId = json['GRNSerialNoId'];
    clientId = json['ClientId'];
    serialNo = json['SerialNo'];
    itemId = json['ItemId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DtLineNum'] = this.dtLineNum;
    data['GRNSerialNoId'] = this.gRNSerialNoId;
    data['ClientId'] = this.clientId;
    data['SerialNo'] = this.serialNo;
    data['ItemId'] = this.itemId;
    return data;
  }
}