import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

import 'GetSRRefDetailsResp.dart';

class GetTrReferenceDetailsResp {
  Data data;
  ValidationDetails validationDetails;

  GetTrReferenceDetailsResp({this.data, this.validationDetails});

  GetTrReferenceDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  String dateFormat;
  List<SRRefItemDetails> itemDetails;

  Data({this.dateFormat, this.itemDetails});

  Data.fromJson(Map<String, dynamic> json) {
    dateFormat = json['DateFormat'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<SRRefItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new SRRefItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateFormat'] = this.dateFormat;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
