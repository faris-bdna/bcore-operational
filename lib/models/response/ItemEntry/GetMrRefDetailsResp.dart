import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetMrRefDetailsResp {
  Data data;
  ValidationDetails validationDetails;

  GetMrRefDetailsResp({this.data, this.validationDetails});

  GetMrRefDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  String dateFormat;
  List<MrRefItemsList> itemDetails;

  Data({this.dateFormat, this.itemDetails});

  Data.fromJson(Map<String, dynamic> json) {
    dateFormat = json['DateFormat'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<MrRefItemsList>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new MrRefItemsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateFormat'] = this.dateFormat;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MrRefItemsList {
  int mtlReqHdId;
  int mtlReqDtId;
  int itemId;
  String itemCode;
  String itemName;
  int packingId;
  String pkgName;
  String packingTemp;
  String packingDesc;
  String itemBarcode;
  double unitPrice;
  double approvedQty;
  double availableQty;
  double issueTransferQty;
  double pkgQty;
  double umQty;
  String brandName;
  String countryName;
  int brandId;
  int countryId;
  int productStockTypeId;
  int isSubstituteAllowed;
  int isSubstitute;
  int substituteId;
  int substitutePackId;

  MrRefItemsList(
      {this.mtlReqHdId,
        this.mtlReqDtId,
        this.itemId,
        this.itemCode,
        this.itemName,
        this.packingId,
        this.pkgName,
        this.packingTemp,
        this.packingDesc,
        this.itemBarcode,
        this.unitPrice,
        this.approvedQty,
        this.availableQty,
        this.issueTransferQty,
        this.pkgQty,
        this.umQty,
        this.brandName,
        this.countryName,
        this.brandId,
        this.countryId,
        this.productStockTypeId,
        this.isSubstituteAllowed,
        this.isSubstitute,
        this.substituteId,
        this.substitutePackId});

  MrRefItemsList.fromJson(Map<String, dynamic> json) {
    mtlReqHdId = json['MtlReqHdId'];
    mtlReqDtId = json['MtlReqDtId'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    packingId = json['PackingId'];
    pkgName = json['PkgName'];
    packingTemp = json['PackingTemp'];
    packingDesc = json['PackingDesc'];
    itemBarcode = json['ItemBarcode'];
    unitPrice = json['UnitPrice'];
    approvedQty = json['ApprovedQty'];
    availableQty = json['AvailableQty'];
    issueTransferQty = json['IssueTransferQty'];
    pkgQty = json['PkgQty'];
    umQty = json['UmQty'];
    brandName = json['BrandName'];
    countryName = json['CountryName'];
    brandId = json['BrandId'];
    countryId = json['CountryId'];
    productStockTypeId = json['ProductStockTypeId'];
    isSubstituteAllowed = json['IsSubstituteAllowed'];
    isSubstitute = json['IsSubstitute'];
    substituteId = json['SubstituteId'];
    substitutePackId = json['SubstitutePackId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MtlReqHdId'] = this.mtlReqHdId;
    data['MtlReqDtId'] = this.mtlReqDtId;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['PackingTemp'] = this.packingTemp;
    data['PackingDesc'] = this.packingDesc;
    data['ItemBarcode'] = this.itemBarcode;
    data['UnitPrice'] = this.unitPrice;
    data['ApprovedQty'] = this.approvedQty;
    data['AvailableQty'] = this.availableQty;
    data['IssueTransferQty'] = this.issueTransferQty;
    data['PkgQty'] = this.pkgQty;
    data['UmQty'] = this.umQty;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['BrandId'] = this.brandId;
    data['CountryId'] = this.countryId;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['IsSubstituteAllowed'] = this.isSubstituteAllowed;
    data['IsSubstitute'] = this.isSubstitute;
    data['SubstituteId'] = this.substituteId;
    data['SubstitutePackId'] = this.substitutePackId;
    return data;
  }
}