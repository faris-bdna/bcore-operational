class GetSOListResp {
  List<SalesOrderList> masterDataList;
  ValidationDetails validationDetails;

  GetSOListResp({this.masterDataList, this.validationDetails});

  GetSOListResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<SalesOrderList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new SalesOrderList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SalesOrderList {
  bool disabled;
  Group group;
  bool selected;
  String text;
  String value;

  SalesOrderList(
      {this.disabled, this.group, this.selected, this.text, this.value});

  SalesOrderList.fromJson(Map<String, dynamic> json) {
    disabled = json['Disabled'];
    group = json['Group'] != null ? new Group.fromJson(json['Group']) : null;
    selected = json['Selected'];
    text = json['Text'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Disabled'] = this.disabled;
    if (this.group != null) {
      data['Group'] = this.group.toJson();
    }
    data['Selected'] = this.selected;
    data['Text'] = this.text;
    data['Value'] = this.value;
    return data;
  }
}

class Group {
  bool disabled;
  String name;

  Group({this.disabled, this.name});

  Group.fromJson(Map<String, dynamic> json) {
    disabled = json['Disabled'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Disabled'] = this.disabled;
    data['Name'] = this.name;
    return data;
  }
}

class ValidationDetails {
  List<ErrorDetails> errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}

class ErrorDetails {
  String errorMessageDescription;
  String errorCode;

  ErrorDetails({this.errorMessageDescription, this.errorCode});

  ErrorDetails.fromJson(Map<String, dynamic> json) {
    errorMessageDescription = json['ErrorMessageDescription'];
    errorCode = json['ErrorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessageDescription'] = this.errorMessageDescription;
    data['ErrorCode'] = this.errorCode;
    return data;
  }
}
