import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetAdditionalServicesResp {
  List<AdditionalServiceMasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetAdditionalServicesResp({this.masterDataList, this.validationDetails});

  GetAdditionalServicesResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<AdditionalServiceMasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new AdditionalServiceMasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class AdditionalServiceMasterDataList {
  int productId;
  String productName;
  String productCode;
  String productDescription;
  String productUnit;
  String categoryName;
  String subCategoryName;
  double unitPrice;
  String tax;
  String taxSlab;
  double productCost;
  double profitPercentage;
  Null parentProduct;
  String divisionName;
  String departmentName;
  double taxPercent;

  int unitId;
  int taxId;
  int taxSlabId;

  AdditionalServiceMasterDataList(
      {this.unitId,
      this.taxId,
      this.taxSlabId,
      this.productId,
      this.productName,
      this.productCode,
      this.productDescription,
      this.productUnit,
      this.categoryName,
      this.subCategoryName,
      this.unitPrice,
      this.tax,
      this.taxSlab,
      this.productCost,
      this.profitPercentage,
      this.parentProduct,
      this.divisionName,
      this.departmentName,
      this.taxPercent});

  AdditionalServiceMasterDataList.fromJson(Map<String, dynamic> json) {
    productId = json['ProductId'];
    productName = json['ProductName'];
    productCode = json['ProductCode'];
    productDescription = json['ProductDescription'];
    productUnit = json['ProductUnit'];
    categoryName = json['CategoryName'];
    subCategoryName = json['SubCategoryName'];
    unitPrice = json['UnitPrice'];
    tax = json['Tax'];
    taxSlab = json['TaxSlab'];
    productCost = json['ProductCost'];
    profitPercentage = json['ProfitPercentage'];
    parentProduct = json['ParentProduct'];
    divisionName = json['DivisionName'];
    departmentName = json['DepartmentName'];
    taxPercent = json['TaxPercent'];

    unitId = json['UnitId'];
    taxId = json['TaxId'];
    taxSlabId = json['TaxSlabId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProductId'] = this.productId;
    data['ProductName'] = this.productName;
    data['ProductCode'] = this.productCode;
    data['ProductDescription'] = this.productDescription;
    data['ProductUnit'] = this.productUnit;
    data['CategoryName'] = this.categoryName;
    data['SubCategoryName'] = this.subCategoryName;
    data['UnitPrice'] = this.unitPrice;
    data['Tax'] = this.tax;
    data['TaxSlab'] = this.taxSlab;
    data['ProductCost'] = this.productCost;
    data['ProfitPercentage'] = this.profitPercentage;
    data['ParentProduct'] = this.parentProduct;
    data['DivisionName'] = this.divisionName;
    data['DepartmentName'] = this.departmentName;
    data['TaxPercent'] = this.taxPercent;
    data['UnitId'] = this.unitId;
    data['TaxSlab'] = this.taxSlab;
    data['TaxSlabId'] = this.taxSlabId;
    data['TaxId'] = this.taxId;

    return data;
  }
}
