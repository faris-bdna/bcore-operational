class GetSODetailsResp {

  List<SODetailsList> masterDataList;
  ValidationDetails validationDetails;

  GetSODetailsResp({this.masterDataList, this.validationDetails});

  GetSODetailsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<SODetailsList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new SODetailsList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SODetailsList {
  int orderId;
  String salesOrderNo;
  String companyName;
  String departmentName;
  String divisionName;
  String locationName;
  String projectName;
  String createdOn;
  String modifiedOn;
  String createdBy;
  String modifiedBy;
  bool isSameAsBillingAdd;
  String billingAddress;
  String deliveryAddress;
  String customerName;
  int companySettingsValue;
  bool isHolded;
  bool isAdvanceApplicable;
  bool isPorformaInvoiced;
  List<MaterialDetailsList> materialDetailsList;
  List<ServiceDetailsList> serviceDetailsList;
  List<TermsAndCondionList> termsAndCondionList;
  List<AdditionalChargesDetailsList> additionalChargesDetailsList;

  SODetailsList(
      {this.orderId,
        this.salesOrderNo,
        this.companyName,
        this.departmentName,
        this.divisionName,
        this.locationName,
        this.projectName,
        this.createdOn,
        this.modifiedOn,
        this.createdBy,
        this.modifiedBy,
        this.isSameAsBillingAdd,
        this.billingAddress,
        this.deliveryAddress,
        this.customerName,
        this.companySettingsValue,
        this.isHolded,
        this.isAdvanceApplicable,
        this.isPorformaInvoiced,
        this.materialDetailsList,
        this.serviceDetailsList,
        this.termsAndCondionList,
        this.additionalChargesDetailsList});

  SODetailsList.fromJson(Map<String, dynamic> json) {
    orderId = json['OrderId'];
    salesOrderNo = json['SalesOrderNo'];
    companyName = json['CompanyName'];
    departmentName = json['DepartmentName'];
    divisionName = json['DivisionName'];
    locationName = json['LocationName'];
    projectName = json['ProjectName'];
    createdOn = json['CreatedOn'];
    modifiedOn = json['ModifiedOn'];
    createdBy = json['CreatedBy'];
    modifiedBy = json['ModifiedBy'];
    isSameAsBillingAdd = json['IsSameAsBillingAdd'];
    billingAddress = json['BillingAddress'];
    deliveryAddress = json['DeliveryAddress'];
    customerName = json['CustomerName'];
    companySettingsValue = json['CompanySettingsValue'];
    isHolded = json['IsHolded'];
    isAdvanceApplicable = json['IsAdvanceApplicable'];
    isPorformaInvoiced = json['IsPorformaInvoiced'];
    if (json['MaterialDetailsList'] != null) {
      materialDetailsList = new List<MaterialDetailsList>();
      json['MaterialDetailsList'].forEach((v) {
        materialDetailsList.add(new MaterialDetailsList.fromJson(v));
      });
    }
    if (json['ServiceDetailsList'] != null) {
      serviceDetailsList = new List<ServiceDetailsList>();
      json['ServiceDetailsList'].forEach((v) {
        serviceDetailsList.add(new ServiceDetailsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderId'] = this.orderId;
    data['SalesOrderNo'] = this.salesOrderNo;
    data['CompanyName'] = this.companyName;
    data['DepartmentName'] = this.departmentName;
    data['DivisionName'] = this.divisionName;
    data['LocationName'] = this.locationName;
    data['ProjectName'] = this.projectName;
    data['CreatedOn'] = this.createdOn;
    data['ModifiedOn'] = this.modifiedOn;
    data['CreatedBy'] = this.createdBy;
    data['ModifiedBy'] = this.modifiedBy;
    data['IsSameAsBillingAdd'] = this.isSameAsBillingAdd;
    data['BillingAddress'] = this.billingAddress;
    data['DeliveryAddress'] = this.deliveryAddress;
    data['CustomerName'] = this.customerName;
    data['CompanySettingsValue'] = this.companySettingsValue;
    data['IsHolded'] = this.isHolded;
    data['IsAdvanceApplicable'] = this.isAdvanceApplicable;
    data['IsPorformaInvoiced'] = this.isPorformaInvoiced;
    if (this.materialDetailsList != null) {
      data['MaterialDetailsList'] =
          this.materialDetailsList.map((v) => v.toJson()).toList();
    }
    if (this.serviceDetailsList != null) {
      data['ServiceDetailsList'] =
          this.serviceDetailsList.map((v) => v.toJson()).toList();
    }
    if (this.termsAndCondionList != null) {
      data['TermsAndCondionList'] =
          this.termsAndCondionList.map((v) => v.toJson()).toList();
    }
    if (this.additionalChargesDetailsList != null) {
      data['AdditionalChargesDetailsList'] =
          this.additionalChargesDetailsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MaterialDetailsList {
  int orderDetId;
  int itemId;
  String itemName;
  String itemCode;
  String barCode;
  int unitId;
  String unitName;
  double deliveryQuantity;
  double fOCQuantity;
  int itemTypeId;
  String brandName;
  String orginCountryName;
  String majorPackName;
  String packingDetails;
  String orderPackName;
  int productStockTypeId;
  bool isSerializedItem;
  List<BatchDetailsList> batchDetailsList;

  MaterialDetailsList(
      {this.orderDetId,
        this.itemId,
        this.itemName,
        this.itemCode,
        this.barCode,
        this.unitId,
        this.unitName,
        this.deliveryQuantity,
        this.fOCQuantity,
        this.itemTypeId,
        this.brandName,
        this.orginCountryName,
        this.majorPackName,
        this.packingDetails,
        this.orderPackName,
        this.productStockTypeId,
        this.isSerializedItem,
        this.batchDetailsList});

  MaterialDetailsList.fromJson(Map<String, dynamic> json) {
    orderDetId = json['OrderDetId'];
    itemId = json['ItemId'];
    itemName = json['ItemName'];
    itemCode = json['ItemCode'];
    barCode = json['BarCode'];
    unitId = json['UnitId'];
    unitName = json['UnitName'];
    deliveryQuantity = json['DeliveryQuantity'];
    fOCQuantity = json['FOCQuantity'];
    itemTypeId = json['ItemTypeId'];
    brandName = json['BrandName'];
    orginCountryName = json['OrginCountryName'];
    majorPackName = json['MajorPackName'];
    packingDetails = json['PackingDetails'];
    orderPackName = json['OrderPackName'];
    productStockTypeId = json['ProductStockTypeId'];
    isSerializedItem = json['IsSerializedItem'];
    if (json['BatchDetailsList'] != null) {
      batchDetailsList = new List<BatchDetailsList>();
      json['BatchDetailsList'].forEach((v) {
        batchDetailsList.add(new BatchDetailsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderDetId'] = this.orderDetId;
    data['ItemId'] = this.itemId;
    data['ItemName'] = this.itemName;
    data['ItemCode'] = this.itemCode;
    data['BarCode'] = this.barCode;
    data['UnitId'] = this.unitId;
    data['UnitName'] = this.unitName;
    data['DeliveryQuantity'] = this.deliveryQuantity;
    data['FOCQuantity'] = this.fOCQuantity;
    data['ItemTypeId'] = this.itemTypeId;
    data['BrandName'] = this.brandName;
    data['OrginCountryName'] = this.orginCountryName;
    data['MajorPackName'] = this.majorPackName;
    data['PackingDetails'] = this.packingDetails;
    data['OrderPackName'] = this.orderPackName;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['IsSerializedItem'] = this.isSerializedItem;
    if (this.batchDetailsList != null) {
      data['BatchDetailsList'] =
          this.batchDetailsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BatchDetailsList {
  int itemBatchDetId;
  int itemId;
  int packId;
  int batchLineNum;
  int dtLineNum;
  int isManualSelect;
  String pOPBatchNo;
  String pOPProductionDate;
  String pOPExpiryDate;
  double pOPBatchCost;
  double pOPAvailableQty;
  double batchWiseQty;
  String batch;
  int pOPWarehouseId;
  int pOPWHouseZoneId;
  int pOPWHouseZoneBinId;
  int pOPWHouseZoneBinRackId;
  String pOPAvailableQtyTemp;
  String pOPExpiryDateTemp;
  String pOPProductionDateTemp;
  String pOPWarehouseName;
  String pOPWHouseZoneName;
  String pOPWHouseZoneBinName;
  String pOPWHouseZoneBinRackName;
  String errorMessage;
  int itemLineNo;
  int refTypeId;
  int refId;
  int refDetId;
  bool isActive;
  int createdBy;
  String createdOn;
  int modifiedBy;
  String modifiedOn;
  bool isDelete;
  double pOPBatchCostTemp;
  String compDecimal;

  BatchDetailsList(
      {this.itemBatchDetId,
        this.itemId,
        this.packId,
        this.batchLineNum,
        this.dtLineNum,
        this.isManualSelect,
        this.pOPBatchNo,
        this.pOPProductionDate,
        this.pOPExpiryDate,
        this.pOPBatchCost,
        this.pOPAvailableQty,
        this.batchWiseQty,
        this.batch,
        this.pOPWarehouseId,
        this.pOPWHouseZoneId,
        this.pOPWHouseZoneBinId,
        this.pOPWHouseZoneBinRackId,
        this.pOPAvailableQtyTemp,
        this.pOPExpiryDateTemp,
        this.pOPProductionDateTemp,
        this.pOPWarehouseName,
        this.pOPWHouseZoneName,
        this.pOPWHouseZoneBinName,
        this.pOPWHouseZoneBinRackName,
        this.errorMessage,
        this.itemLineNo,
        this.refTypeId,
        this.refId,
        this.refDetId,
        this.isActive,
        this.createdBy,
        this.createdOn,
        this.modifiedBy,
        this.modifiedOn,
        this.isDelete,
        this.pOPBatchCostTemp,
        this.compDecimal});

  BatchDetailsList.fromJson(Map<String, dynamic> json) {
    itemBatchDetId = json['ItemBatchDetId'];
    itemId = json['ItemId'];
    packId = json['PackId'];
    batchLineNum = json['BatchLineNum'];
    dtLineNum = json['DtLineNum'];
    isManualSelect = json['IsManualSelect'];
    pOPBatchNo = json['POPBatchNo'];
    pOPProductionDate = json['POPProductionDate'];
    pOPExpiryDate = json['POPExpiryDate'];
    pOPBatchCost = json['POPBatchCost'];
    pOPAvailableQty = json['POPAvailableQty'];
    batchWiseQty = json['BatchWiseQty'];
    batch = json['Batch'];
    pOPWarehouseId = json['POPWarehouseId'];
    pOPWHouseZoneId = json['POPWHouseZoneId'];
    pOPWHouseZoneBinId = json['POPWHouseZoneBinId'];
    pOPWHouseZoneBinRackId = json['POPWHouseZoneBinRackId'];
    pOPAvailableQtyTemp = json['POPAvailableQtyTemp'];
    pOPExpiryDateTemp = json['POPExpiryDateTemp'];
    pOPProductionDateTemp = json['POPProductionDateTemp'];
    pOPWarehouseName = json['POPWarehouseName'];
    pOPWHouseZoneName = json['POPWHouseZoneName'];
    pOPWHouseZoneBinName = json['POPWHouseZoneBinName'];
    pOPWHouseZoneBinRackName = json['POPWHouseZoneBinRackName'];
    errorMessage = json['ErrorMessage'];
    itemLineNo = json['ItemLineNo'];
    refTypeId = json['RefTypeId'];
    refId = json['RefId'];
    refDetId = json['RefDetId'];
    isActive = json['IsActive'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifiedBy = json['ModifiedBy'];
    modifiedOn = json['ModifiedOn'];
    isDelete = json['IsDelete'];
    pOPBatchCostTemp = json['POPBatchCostTemp'];
    compDecimal = json['CompDecimal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemBatchDetId'] = this.itemBatchDetId;
    data['ItemId'] = this.itemId;
    data['PackId'] = this.packId;
    data['BatchLineNum'] = this.batchLineNum;
    data['DtLineNum'] = this.dtLineNum;
    data['IsManualSelect'] = this.isManualSelect;
    data['POPBatchNo'] = this.pOPBatchNo;
    data['POPProductionDate'] = this.pOPProductionDate;
    data['POPExpiryDate'] = this.pOPExpiryDate;
    data['POPBatchCost'] = this.pOPBatchCost;
    data['POPAvailableQty'] = this.pOPAvailableQty;
    data['BatchWiseQty'] = this.batchWiseQty;
    data['Batch'] = this.batch;
    data['POPWarehouseId'] = this.pOPWarehouseId;
    data['POPWHouseZoneId'] = this.pOPWHouseZoneId;
    data['POPWHouseZoneBinId'] = this.pOPWHouseZoneBinId;
    data['POPWHouseZoneBinRackId'] = this.pOPWHouseZoneBinRackId;
    data['POPAvailableQtyTemp'] = this.pOPAvailableQtyTemp;
    data['POPExpiryDateTemp'] = this.pOPExpiryDateTemp;
    data['POPProductionDateTemp'] = this.pOPProductionDateTemp;
    data['POPWarehouseName'] = this.pOPWarehouseName;
    data['POPWHouseZoneName'] = this.pOPWHouseZoneName;
    data['POPWHouseZoneBinName'] = this.pOPWHouseZoneBinName;
    data['POPWHouseZoneBinRackName'] = this.pOPWHouseZoneBinRackName;
    data['ErrorMessage'] = this.errorMessage;
    data['ItemLineNo'] = this.itemLineNo;
    data['RefTypeId'] = this.refTypeId;
    data['RefId'] = this.refId;
    data['RefDetId'] = this.refDetId;
    data['IsActive'] = this.isActive;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifiedBy'] = this.modifiedBy;
    data['ModifiedOn'] = this.modifiedOn;
    data['IsDelete'] = this.isDelete;
    data['POPBatchCostTemp'] = this.pOPBatchCostTemp;
    data['CompDecimal'] = this.compDecimal;
    return data;
  }
}

class ServiceDetailsList {
  int orderDetId;
  int itemId;
  String serviceCode;
  String serviceName;
  int unitId;
  String unitName;
  double deliveryQuantity;

  ServiceDetailsList(
      {this.orderDetId,
        this.itemId,
        this.serviceCode,
        this.serviceName,
        this.unitId,
        this.unitName,
        this.deliveryQuantity});

  ServiceDetailsList.fromJson(Map<String, dynamic> json) {
    orderDetId = json['OrderDetId'];
    itemId = json['ItemId'];
    serviceCode = json['ServiceCode'];
    serviceName = json['ServiceName'];
    unitId = json['UnitId'];
    unitName = json['UnitName'];
    deliveryQuantity = json['DeliveryQuantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderDetId'] = this.orderDetId;
    data['ItemId'] = this.itemId;
    data['ServiceCode'] = this.serviceCode;
    data['ServiceName'] = this.serviceName;
    data['UnitId'] = this.unitId;
    data['UnitName'] = this.unitName;
    data['DeliveryQuantity'] = this.deliveryQuantity;
    return data;
  }
}

class TermsAndCondionList {
  int salesOrderTermsId;
  int termsId;
  String termsAndConditions;
  String type;
  int sortOrder;
  int paymentTypeId;
  double paymentValue;
  String scopeTypeName;
  String paymentTypeName;
  int scopeTypeId;

  TermsAndCondionList(
      {this.salesOrderTermsId,
        this.termsId,
        this.termsAndConditions,
        this.type,
        this.sortOrder,
        this.paymentTypeId,
        this.paymentValue,
        this.scopeTypeName,
        this.paymentTypeName,
        this.scopeTypeId});

  TermsAndCondionList.fromJson(Map<String, dynamic> json) {
    salesOrderTermsId = json['SalesOrderTermsId'];
    termsId = json['TermsId'];
    termsAndConditions = json['TermsAndConditions'];
    type = json['Type'];
    sortOrder = json['SortOrder'];
    paymentTypeId = json['PaymentTypeId'];
    paymentValue = json['PaymentValue'];
    scopeTypeName = json['ScopeTypeName'];
    paymentTypeName = json['PaymentTypeName'];
    scopeTypeId = json['ScopeTypeId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SalesOrderTermsId'] = this.salesOrderTermsId;
    data['TermsId'] = this.termsId;
    data['TermsAndConditions'] = this.termsAndConditions;
    data['Type'] = this.type;
    data['SortOrder'] = this.sortOrder;
    data['PaymentTypeId'] = this.paymentTypeId;
    data['PaymentValue'] = this.paymentValue;
    data['ScopeTypeName'] = this.scopeTypeName;
    data['PaymentTypeName'] = this.paymentTypeName;
    data['ScopeTypeId'] = this.scopeTypeId;
    return data;
  }
}

class AdditionalChargesDetailsList {
  int itemId;
  String itemName;
  String unitName;
  String taxSlab;
  double quantity;
  double rate;
  double total;
  double taxPercent;
  String tax;
  double taxAmount;
  double netAmount;
  String bOQDescription;
  int unitId;
  int taxSlabId;
  int taxID;

  AdditionalChargesDetailsList(
      {this.itemId,
        this.itemName,
        this.unitName,
        this.taxSlab,
        this.quantity,
        this.rate,
        this.total,
        this.taxPercent,
        this.tax,
        this.taxAmount,
        this.netAmount,
        this.bOQDescription,
        this.unitId,
        this.taxSlabId,
        this.taxID});

  AdditionalChargesDetailsList.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    itemName = json['ItemName'];
    unitName = json['UnitName'];
    taxSlab = json['TaxSlab'];
    quantity = json['Quantity'];
    rate = json['Rate'];
    total = json['Total'];
    taxPercent = json['TaxPercent'];
    tax = json['Tax'];
    taxAmount = json['TaxAmount'];
    netAmount = json['NetAmount'];
    bOQDescription = json['BOQDescription'];
    unitId = json['UnitId'];
    taxSlabId = json['TaxSlabId'];
    taxID = json['TaxID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['ItemName'] = this.itemName;
    data['UnitName'] = this.unitName;
    data['TaxSlab'] = this.taxSlab;
    data['Quantity'] = this.quantity;
    data['Rate'] = this.rate;
    data['Total'] = this.total;
    data['TaxPercent'] = this.taxPercent;
    data['Tax'] = this.tax;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['BOQDescription'] = this.bOQDescription;
    data['UnitId'] = this.unitId;
    data['TaxSlabId'] = this.taxSlabId;
    data['TaxID'] = this.taxID;
    return data;
  }
}

class ValidationDetails {
  List<ErrorDetails> errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}

class ErrorDetails {
  String errorMessageDescription;
  String errorCode;

  ErrorDetails({this.errorMessageDescription, this.errorCode});

  ErrorDetails.fromJson(Map<String, dynamic> json) {
    errorMessageDescription = json['ErrorMessageDescription'];
    errorCode = json['ErrorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessageDescription'] = this.errorMessageDescription;
    data['ErrorCode'] = this.errorCode;
    return data;
  }
}
