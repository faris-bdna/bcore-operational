import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class TermsAndConditionsResp {
  List<TandAMasterDataList> masterDataList;
  ValidationDetails validationDetails;

  TermsAndConditionsResp({this.masterDataList, this.validationDetails});

  TermsAndConditionsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<TandAMasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new TandAMasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class TandAMasterDataList {
  int termsId;
  String termsAndConditionsName;
  String categoryName;
  int sortOrder;
  int parentTermsId;
  String parentTermsName;

  TandAMasterDataList(
      {this.termsId,
        this.termsAndConditionsName,
        this.categoryName,
        this.sortOrder,
        this.parentTermsId,
        this.parentTermsName});

  TandAMasterDataList.fromJson(Map<String, dynamic> json) {
    termsId = json['TermsId'];
    termsAndConditionsName = json['TermsAndConditionsName'];
    categoryName = json['CategoryName'];
    sortOrder = json['SortOrder'];
    parentTermsId = json['ParentTermsId'];
    parentTermsName = json['ParentTermsName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['TermsId'] = this.termsId;
    data['TermsAndConditionsName'] = this.termsAndConditionsName;
    data['CategoryName'] = this.categoryName;
    data['SortOrder'] = this.sortOrder;
    data['ParentTermsId'] = this.parentTermsId;
    data['ParentTermsName'] = this.parentTermsName;
    return data;
  }
}