class SalesTypeResponse {
  List<SalesTypeList> salesTypeList;
  ValidationDetails validationDetails;

  SalesTypeResponse({this.salesTypeList, this.validationDetails});

  SalesTypeResponse.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      salesTypeList = new List<SalesTypeList>();
      json['MasterDataList'].forEach((v) {
        salesTypeList.add(new SalesTypeList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.salesTypeList != null) {
      data['MasterDataList'] =
          this.salesTypeList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SalesTypeList {
  String text;
  String value;

  SalesTypeList({this.text, this.value});

  SalesTypeList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    return data;
  }
}

class ValidationDetails {
  Null errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    errorDetails = json['ErrorDetails'];
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorDetails'] = this.errorDetails;
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}