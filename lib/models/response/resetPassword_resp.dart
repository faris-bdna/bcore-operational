class ResetPasswordResponse {
  String uSLogId;
  String successMessage;
  List<ErrorDetails> errorDetails;
  String statusMessage;
  int statusCode;

  ResetPasswordResponse(
      {this.uSLogId,
        this.successMessage,
        this.errorDetails,
        this.statusMessage,
        this.statusCode});

  ResetPasswordResponse.fromJson(Map<String, dynamic> json) {
    uSLogId = json['USLogId'];
    successMessage = json['SuccessMessage'];
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['USLogId'] = this.uSLogId;
    data['SuccessMessage'] = this.successMessage;
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}

class ErrorDetails {
  String errorMessageDescription;
  String errorCode;

  ErrorDetails({this.errorMessageDescription, this.errorCode});

  ErrorDetails.fromJson(Map<String, dynamic> json) {
    errorMessageDescription = json['ErrorMessageDescription'];
    errorCode = json['ErrorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessageDescription'] = this.errorMessageDescription;
    data['ErrorCode'] = this.errorCode;
    return data;
  }
}

