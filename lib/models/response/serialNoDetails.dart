class SubstituteItemDetails {
  int mtlReqHdId;
  int mtlReqDtId;
  int substituteId;
  int packingId;
  double unitQty;

  SubstituteItemDetails(
      {this.mtlReqHdId,
        this.mtlReqDtId,
        this.substituteId,
        this.packingId,
        this.unitQty});

  SubstituteItemDetails.fromJson(Map<String, dynamic> json) {
    mtlReqHdId = json['MtlReqHdId'];
    mtlReqDtId = json['MtlReqDtId'];
    substituteId = json['SubstituteId'];
    packingId = json['PackingId'];
    unitQty = json['UnitQty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MtlReqHdId'] = this.mtlReqHdId;
    data['MtlReqDtId'] = this.mtlReqDtId;
    data['SubstituteId'] = this.substituteId;
    data['PackingId'] = this.packingId;
    data['UnitQty'] = this.unitQty;
    return data;
  }
}

final List <SubstituteItemDetails> sub_details = List<SubstituteItemDetails>();