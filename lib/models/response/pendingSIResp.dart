class PendingSITOResp {

  // static final PendingSIResp _pendingSIResp = new PendingSIResp._internal();
  // factory PendingSIResp() => _pendingSIResp;
  // PendingSIResp._internal();
  // static PendingSIResp get shared => _pendingSIResp;

  List<PendingRefList> pendingRefList;
  ValidationDetails validationDetails;

  PendingSITOResp({this.pendingRefList, this.validationDetails});

  PendingSITOResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      pendingRefList = new List<PendingRefList>();
      json['MasterDataList'].forEach((v) {
        pendingRefList.add(new PendingRefList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pendingRefList != null) {
      data['MasterDataList'] =
          this.pendingRefList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class PendingRefList {
  String text;
  String value;

  String fromCompany;
  String fromDivision;
  String fromLocation;
  String fromDepartment;

  String toCompany;
  String toDivision;
  String toLocation;
  String toDepartment;
  String toProject;

  int fromCompanyId;

  int toCompanyId;
  int toDivisionId;
  int toLocationId;

  // String entryDate;
  // String fromProject;

  PendingRefList(
      {this.text,
        this.value,
        this.fromCompany,
        this.fromDivision,
        this.fromLocation,
        this.fromDepartment,
        this.toCompany,
        this.toDivision,
        this.toLocation,
        this.toDepartment,
        this.toProject,
        this.fromCompanyId,
        this.toCompanyId,
        this.toDivisionId,
        this.toLocationId});

  PendingRefList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
    fromCompany = json['FromCompany'];
    fromDivision = json['FromDivision'];
    fromLocation = json['FromLocation'];
    fromDepartment = json['FromDepartment'];
    toCompany = json['ToCompany'];
    toDivision = json['ToDivision'];
    toLocation = json['ToLocation'];
    toDepartment = json['ToDepartment'];
    toProject = json['ToProject'];
    fromCompanyId = json['FromCompanyId'];
    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocationId = json['ToLocationId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['FromCompany'] = this.fromCompany;
    data['FromDivision'] = this.fromDivision;
    data['FromLocation'] = this.fromLocation;
    data['FromDepartment'] = this.fromDepartment;
    data['ToCompany'] = this.toCompany;
    data['ToDivision'] = this.toDivision;
    data['ToLocation'] = this.toLocation;
    data['ToDepartment'] = this.toDepartment;
    data['ToProject'] = this.toProject;
    data['FromCompanyId'] = this.fromCompanyId;
    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocationId'] = this.toLocationId;
    return data;
  }
}
//
//   PendingSIResp.fromJson(Map<String, dynamic> json) {
//     if (json['MasterDataList'] != null) {
//       pendingSIList = new List<PendingSIList>();
//       json['MasterDataList'].forEach((v) {
//         pendingSIList.add(new PendingSIList.fromJson(v));
//       });
//     }
//     validationDetails = json['ValidationDetails'] != null
//         ? new ValidationDetails.fromJson(json['ValidationDetails'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.pendingSIList != null) {
//       data['MasterDataList'] =
//           this.pendingSIList.map((v) => v.toJson()).toList();
//     }
//     if (this.validationDetails != null) {
//       data['ValidationDetails'] = this.validationDetails.toJson();
//     }
//     return data;
//   }
// }
//
// class PendingSIList {
//   String text;
//   String value;
//   String entryDate;
//   int fromCompanyId;
//   int fromDivisionId;
//   int fromLocationId;
//   int fromDepartmentId;
//   int fromProjectId;
//   int toCompanyId;
//   int toDivisionId;
//   int toLocationId;
//   int toDepartmentId;
//   int toProjectId;
//
//   PendingSIList(
//       {this.text,
//         this.value,
//         this.entryDate,
//         this.fromCompanyId,
//         this.fromDivisionId,
//         this.fromLocationId,
//         this.fromDepartmentId,
//         this.fromProjectId,
//         this.toCompanyId,
//         this.toDivisionId,
//         this.toLocationId,
//         this.toDepartmentId,
//         this.toProjectId});
//
//   PendingSIList.fromJson(Map<String, dynamic> json) {
//     text = json['Text'];
//     value = json['Value'];
//     entryDate = json['EntryDate'];
//     fromCompanyId = json['FromCompanyId'];
//     fromDivisionId = json['FromDivisionId'];
//     fromLocationId = json['FromLocationId'];
//     fromDepartmentId = json['FromDepartmentId'];
//     fromProjectId = json['FromProjectId'];
//     toCompanyId = json['ToCompanyId'];
//     toDivisionId = json['ToDivisionId'];
//     toLocationId = json['ToLocationId'];
//     toDepartmentId = json['ToDepartmentId'];
//     toProjectId = json['ToProjectId'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['Text'] = this.text;
//     data['Value'] = this.value;
//     data['EntryDate'] = this.entryDate;
//     data['FromCompanyId'] = this.fromCompanyId;
//     data['FromDivisionId'] = this.fromDivisionId;
//     data['FromLocationId'] = this.fromLocationId;
//     data['FromDepartmentId'] = this.fromDepartmentId;
//     data['FromProjectId'] = this.fromProjectId;
//     data['ToCompanyId'] = this.toCompanyId;
//     data['ToDivisionId'] = this.toDivisionId;
//     data['ToLocationId'] = this.toLocationId;
//     data['ToDepartmentId'] = this.toDepartmentId;
//     data['ToProjectId'] = this.toProjectId;
//     return data;
//   }
// }

class ValidationDetails {
  List<ErrorDetails> errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}

class ErrorDetails {
  String errorMessageDescription;
  String errorCode;

  ErrorDetails({this.errorMessageDescription, this.errorCode});

  ErrorDetails.fromJson(Map<String, dynamic> json) {
    errorMessageDescription = json['ErrorMessageDescription'];
    errorCode = json['ErrorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessageDescription'] = this.errorMessageDescription;
    data['ErrorCode'] = this.errorCode;
    return data;
  }
}
