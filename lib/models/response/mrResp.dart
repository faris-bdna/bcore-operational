import 'package:bcore_inventory_management/models/response/common_validation_resp.dart' as mrResp;

class MRResp {
  String data;
  mrResp.ValidationDetails validationDetails;

  MRResp({this.data, this.validationDetails});

  MRResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'];
    validationDetails = json['ValidationDetails'] != null
        ? new mrResp.ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Data'] = this.data;
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class TransactionResp extends MRResp{

}