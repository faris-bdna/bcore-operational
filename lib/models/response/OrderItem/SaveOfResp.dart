import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class SaveOFResp {
  String data;
  ValidationDetails validationDetails;

  SaveOFResp({this.data, this.validationDetails});

  SaveOFResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'];
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Data'] = this.data;
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}