import '../common_validation_resp.dart';

class GetSalesStockDetailsResp {
  List<SalesStockDetailsList> masterDataList;
  ValidationDetails validationDetails;

  GetSalesStockDetailsResp({this.masterDataList, this.validationDetails});

  GetSalesStockDetailsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<SalesStockDetailsList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new SalesStockDetailsList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SalesStockDetailsList {
  int itemId;
  int packingId;
  double currentAvailableStock;
  double currentDaySaleQtyLY;
  double currentDaySaleQtyLW;
  double lastDayWastageQty;
  double stockHolding;
  double orderQty;
  double avgMTDSaleQty;
  double estimatedOrderQty;

  SalesStockDetailsList(
      {this.itemId,
        this.packingId,
        this.currentAvailableStock,
        this.currentDaySaleQtyLY,
        this.currentDaySaleQtyLW,
        this.lastDayWastageQty,
        this.stockHolding,
        this.orderQty,
        this.avgMTDSaleQty,
        this.estimatedOrderQty});

  SalesStockDetailsList.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    packingId = json['PackingId'];
    currentAvailableStock = json['CurrentAvailableStock'];
    currentDaySaleQtyLY = json['CurrentDaySaleQtyLY'];
    currentDaySaleQtyLW = json['CurrentDaySaleQtyLW'];
    lastDayWastageQty = json['LastDayWastageQty'];
    stockHolding = json['StockHolding'];
    orderQty = json['OrderQty'];
    avgMTDSaleQty = json['AvgMTDSaleQty'];
    estimatedOrderQty = json['EstimatedOrderQty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['CurrentAvailableStock'] = this.currentAvailableStock;
    data['CurrentDaySaleQtyLY'] = this.currentDaySaleQtyLY;
    data['CurrentDaySaleQtyLW'] = this.currentDaySaleQtyLW;
    data['LastDayWastageQty'] = this.lastDayWastageQty;
    data['StockHolding'] = this.stockHolding;
    data['OrderQty'] = this.orderQty;
    data['AvgMTDSaleQty'] = this.avgMTDSaleQty;
    data['EstimatedOrderQty'] = this.estimatedOrderQty;
    return data;
  }
}

