import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetPRReferenceDetailsResp {
  Data data;
  ValidationDetails validationDetails;

  GetPRReferenceDetailsResp({this.data, this.validationDetails});

  GetPRReferenceDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  String dateFormat;

  List<ItemDetails> itemDetails;

  Data({this.dateFormat, this.itemDetails});

  Data.fromJson(Map<String, dynamic> json) {
    dateFormat = json['DateFormat'];

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateFormat'] = this.dateFormat;

    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemDetails {

  int itemId;
  String itemCode;
  String itemName;
  int packingId;
  String suppItemCode;
  String pkgName;
  String itemBarcode;
  int itemBatchDetailsId;
  String batchNo;
  double gRNQty;
  double unitPrice;
  double totalPrice;
  double taxAmount;
  double netAmount;
  int reasonId;
  String returnComments;
  int brandId;
  int orginId;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int gRNDetailId;
  String taxTypevalues;
  double itemTaxPer;
  bool batchNoEnableItem;
  double uOMQty;
  String packingDesc;
  double printQty;

  ItemDetails(
      {this.itemId,
        this.itemCode,
        this.itemName,
        this.pkgName,
        this.packingId,
        this.suppItemCode,
        this.itemBarcode,
        this.itemBatchDetailsId,
        this.batchNo,
        this.gRNQty,
        this.unitPrice,
        this.totalPrice,
        this.taxAmount,
        this.netAmount,
        this.reasonId,
        this.returnComments,
        this.brandId,
        this.orginId,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.gRNDetailId,
        this.taxTypevalues,
        this.itemTaxPer,
        this.batchNoEnableItem,
        this.uOMQty,
        this.packingDesc,
      });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    pkgName = json['PkgName'];
    packingId = json['PackingId'];
    suppItemCode = json['SuppItemCode'];
    itemBarcode = json['ItemBarcode'];
    itemBatchDetailsId = json['ItemBatchDetailsId'];
    batchNo = json['BatchNo'];
    gRNQty = json['GRNQty'];
    unitPrice = json['UnitPrice'];
    totalPrice = json['TotalPrice'];
    taxAmount = json['TaxAmount'];
    netAmount = json['NetAmount'];
    reasonId = json['ReasonId'];
    returnComments = json['ReturnComments'];
    brandId = json['BrandId'];
    orginId = json['OrginId'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    gRNDetailId = json['GRNDetailId'];
    taxTypevalues = json['TaxTypevalues'];
    itemTaxPer = json['ItemTaxPer'];
    batchNoEnableItem = json['BatchNoEnableItem'];
    uOMQty = json['UOMQty'];

    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['SuppItemCode'] = this.suppItemCode;
    data['ItemBarcode'] = this.itemBarcode;
    data['ItemBatchDetailsId'] = this.itemBatchDetailsId;
    data['BatchNo'] = this.batchNo;
    data['GRNQty'] = this.gRNQty;
    data['UnitPrice'] = this.unitPrice;
    data['TotalPrice'] = this.totalPrice;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['ReasonId'] = this.reasonId;
    data['ReturnComments'] = this.returnComments;
    data['BrandId'] = this.brandId;
    data['OrginId'] = this.orginId;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['GRNDetailId'] = this.gRNDetailId;
    data['TaxTypevalues'] = this.taxTypevalues;
    data['ItemTaxPer'] = this.itemTaxPer;
    data['BatchNoEnableItem'] = this.batchNoEnableItem;
    data['UOMQty'] = this.uOMQty;
    data['PackingDesc'] = this.packingDesc;

    return data;
  }
}
