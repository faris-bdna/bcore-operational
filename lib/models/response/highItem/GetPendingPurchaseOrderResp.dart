import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class ReferencePOGrnResp {
  
  List<ReferencePOGrn> masterDataList;
  ValidationDetails validationDetails;

  ReferencePOGrnResp({this.masterDataList, this.validationDetails});

  ReferencePOGrnResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<ReferencePOGrn>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new ReferencePOGrn.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class ReferencePOGrn {
  String text;
  String value;
  String entryDate;
  int supplierId;
  int deliveryTypeId;
  int priceTypeId;
  int companyId;
  int divisionId;
  int locationId;
  int departmentId;
  int projectId;
  int currencyId;

  String priceTypeName;
  String supplierName;
  String currencyName;
  double currencyRate;
  int isTaxApplicable;

  ReferencePOGrn(
      {this.text,
        this.value,
        this.entryDate,
        this.supplierId,
        this.deliveryTypeId,
        this.companyId,
        this.divisionId,
        this.locationId,
        this.departmentId,
        this.projectId,
        this.currencyId,
        this.priceTypeId,
        this.supplierName,
        this.priceTypeName,
        this.currencyName,
        this.currencyRate,
        this.isTaxApplicable
      });

  ReferencePOGrn.fromJson(Map<String, dynamic> json) {
    if (json['Text'] != null) {
      text = json['Text'];
    }
    if (json['Value'] != null) {
      value = json['Value'];
    }
    if (json['EntryDate'] != null) {
      entryDate = json['EntryDate'];
    }
    if (json['SupplierId'] != null) {
      supplierId = json['SupplierId'];
    }
    if (json['DeliveryTypeId'] != null) {
      deliveryTypeId = json['DeliveryTypeId'];
    }
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocationId'] != null) {
      locationId = json['LocationId'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['PriceTypeId'] != null) {
      priceTypeId = json['PriceTypeId'];
    }
    if (json['SupplierName'] != null) {
      supplierName = json['SupplierName'];
    }
    if (json['PriceTypeName'] != null) {
      priceTypeName = json['PriceTypeName'];
    }
    if (json['CurrencyName'] != null) {
      currencyName = json['CurrencyName'];
    }
    if (json['CurrencyRate'] != null) {
      currencyRate = json['CurrencyRate'];
    }
    if (json['IsTaxApplicable'] != null) {
      isTaxApplicable = json['IsTaxApplicable'];
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['Text'] != null) {
      text = json['Text'];
    }
    if (json['Value'] != null) {
      value = json['Value'];
    }
    if (json['EntryDate'] != null) {
      entryDate = json['EntryDate'];
    }
    if (json['SupplierId'] != null) {
      supplierId = json['SupplierId'];
    }
    if (json['DeliveryTypeId'] != null) {
      deliveryTypeId = json['DeliveryTypeId'];
    }
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocationId'] != null) {
      locationId = json['LocationId'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['PriceTypeId'] != null) {
      priceTypeId = json['PriceTypeId'];
    }
    if (json['SupplierName'] != null) {
      supplierName = json['SupplierName'];
    }
    if (json['PriceTypeName'] != null) {
      priceTypeName = json['PriceTypeName'];
    }
    if (json['CurrencyName'] != null) {
      currencyName = json['CurrencyName'];
    }
    if (json['CurrencyRate'] != null) {
      currencyRate = json['CurrencyRate'];
    }
    if (json['IsTaxApplicable'] != null) {
      isTaxApplicable = json['IsTaxApplicable'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['EntryDate'] = this.entryDate;
    data['SupplierId'] = this.supplierId;
    data['DeliveryTypeId'] = this.deliveryTypeId;
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['DepartmentId'] = this.departmentId;
    data['ProjectId'] = this.projectId;
    data['CurrencyId'] = this.currencyId;
    data['PriceTypeId'] = this.priceTypeId;
    data['SupplierName'] = this.supplierName;
    data['PriceTypeName'] = this.priceTypeName;
    data['CurrencyName'] = this.currencyName;
    data['CurrencyRate'] = this.currencyRate;
    data['IsTaxApplicable'] = this.isTaxApplicable;

    return data;
  }
}

final ReferencePOGrn pendingPurchaseOrder = ReferencePOGrn();
