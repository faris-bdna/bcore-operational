import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetGRNReferenceDetailsResp {
  Data data;
  ValidationDetails validationDetails;

  GetGRNReferenceDetailsResp({this.data, this.validationDetails});

  GetGRNReferenceDetailsResp.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  List<ItemDetails> itemDetails;
  List<OtherCostDetails> otherCostDetails;
  List<OtherCostTaxDetails> otherCostTaxDetails;
  List<FOCDetails> fOCDetails;

  Data(
      {this.itemDetails,
        this.otherCostDetails,
        this.otherCostTaxDetails,
        this.fOCDetails});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['OtherCostDetails'] != null) {
      otherCostDetails = new List<OtherCostDetails>();
      json['OtherCostDetails'].forEach((v) {
        otherCostDetails.add(new OtherCostDetails.fromJson(v));
      });
    }
    if (json['OtherCostTaxDetails'] != null) {
      otherCostTaxDetails = new List<OtherCostTaxDetails>();
      json['OtherCostTaxDetails'].forEach((v) {
        otherCostTaxDetails.add(new OtherCostTaxDetails.fromJson(v));
      });
    }
    if (json['FOCDetails'] != null) {
      fOCDetails = new List<FOCDetails>();
      json['FOCDetails'].forEach((v) {
        fOCDetails.add(new FOCDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.otherCostDetails != null) {
      data['OtherCostDetails'] =
          this.otherCostDetails.map((v) => v.toJson()).toList();
    }
    if (this.otherCostTaxDetails != null) {
      data['OtherCostTaxDetails'] =
          this.otherCostTaxDetails.map((v) => v.toJson()).toList();
    }
    if (this.fOCDetails != null) {
      data['FOCDetails'] = this.fOCDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemDetails {
  int referenceDtId;
  int lineNum;
  int itemId;
  String itemBarcode;
  String itemCode;
  String suppItemCode;
  String itemName;
  String pkgName;
  int packingId;
  String brandName;
  String countryName;
  bool isFoc;
  double pendingQty;
  double excessQty;
  double unitPrice;
  double actualPrice;
  double discPercent;
  double discAmount;
  double invDiscAmount;
  double taxAmount;
  double netAmount;
  double uOMQty;
  double totalUOMQty;
  int minShelfLife;
  int shelfLife;
  double lastPurchPrice;
  double lastReceiveQty;
  double taxPerc;
  bool batchNoEnableItem;
  int productStockTypeId;
  int isSuppContractPrice;
  int suppTypeId;
  bool isTaxApplicable;
  String packingDesc;


  ItemDetails(
      {this.referenceDtId,
        this.lineNum,
        this.itemId,
        this.itemBarcode,
        this.itemCode,
        this.itemName,
        this.suppItemCode,
        this.packingId,
        this.pkgName,
        this.brandName,
        this.countryName,
        this.isFoc,
        this.pendingQty,
        this.excessQty,
        this.unitPrice,
        this.actualPrice,
        this.discPercent,
        this.discAmount,
        this.invDiscAmount,
        this.taxAmount,
        this.netAmount,
        this.uOMQty,
        this.totalUOMQty,
        this.minShelfLife,
        this.shelfLife,
        this.lastPurchPrice,
        this.lastReceiveQty,
        this.taxPerc,
        this.batchNoEnableItem,
        this.productStockTypeId,
        this.isSuppContractPrice,
        this.suppTypeId,
        this.isTaxApplicable,
        this.packingDesc,
      });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    referenceDtId = json['ReferenceDtId'];
    lineNum = json['LineNum'];
    itemId = json['ItemId'];
    itemBarcode = json['ItemBarcode'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    suppItemCode = json['SuppItemCode'];
    packingId = json['PackingId'];
    pkgName = json['PkgName'];
    brandName = json['BrandName'];
    countryName = json['CountryName'];
    isFoc = json['IsFoc'];
    pendingQty = json['PendingQty'];
    excessQty = json['ExcessQty'];
    unitPrice = json['UnitPrice'];
    actualPrice = json['ActualPrice'];
    discPercent = json['DiscPercent'];
    discAmount = json['DiscAmount'];
    invDiscAmount = json['InvDiscAmount'];
    taxAmount = json['TaxAmount'];
    netAmount = json['NetAmount'];
    uOMQty = json['UOMQty'];
    totalUOMQty = json['TotalUOMQty'];
    minShelfLife = json['MinShelfLife'];
    shelfLife = json['ShelfLife'];
    lastPurchPrice = json['LastPurchPrice'];
    lastReceiveQty = json['LastReceiveQty'];
    taxPerc = json['TaxPerc'];
    batchNoEnableItem = json['BatchNoEnableItem'];
    productStockTypeId = json['ProductStockTypeId'];
    isSuppContractPrice = json['IsSuppContractPrice'];
    suppTypeId = json['SuppTypeId'];
    isTaxApplicable = json['IsTaxApplicable'];
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ReferenceDtId'] = this.referenceDtId;
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemBarcode'] = this.itemBarcode;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['SuppItemCode'] = this.suppItemCode;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['IsFoc'] = this.isFoc;
    data['PendingQty'] = this.pendingQty;
    data['ExcessQty'] = this.excessQty;
    data['UnitPrice'] = this.unitPrice;
    data['ActualPrice'] = this.actualPrice;
    data['DiscPercent'] = this.discPercent;
    data['DiscAmount'] = this.discAmount;
    data['InvDiscAmount'] = this.invDiscAmount;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['UOMQty'] = this.uOMQty;
    data['TotalUOMQty'] = this.totalUOMQty;
    data['MinShelfLife'] = this.minShelfLife;
    data['ShelfLife'] = this.shelfLife;
    data['LastPurchPrice'] = this.lastPurchPrice;
    data['LastReceiveQty'] = this.lastReceiveQty;
    data['TaxPerc'] = this.taxPerc;
    data['BatchNoEnableItem'] = this.batchNoEnableItem;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['IsSuppContractPrice'] = this.isSuppContractPrice;
    data['SuppTypeId'] = this.suppTypeId;
    data['IsTaxApplicable'] = this.isTaxApplicable;
    data['PackingDesc'] = this.packingDesc;
    return data;
  }
}

class OtherCostDetails {
  int serviceId;
  int serviceCategoryId;
  int supplierId;
  double serviceAmount;
  int uOM;
  double unitQty;
  double actualPrice;
  double discPerc;
  double discAmount;
  double grossAmount;
  double taxAmount;
  double netAmount;
  int taxSlabId;
  int currencyId;
  int fxCurrencyCnvId;
  double currencyRate;
  double fxAmount;
  double fxNetAmount;
  int serviceSuppTypeId;
  int serviceLinNum;
  double taxPerc;
  bool isTaxApplicable;

  OtherCostDetails(
      {this.serviceId,
        this.serviceCategoryId,
        this.supplierId,
        this.serviceAmount,
        this.uOM,
        this.unitQty,
        this.actualPrice,
        this.discPerc,
        this.discAmount,
        this.grossAmount,
        this.taxAmount,
        this.netAmount,
        this.taxSlabId,
        this.currencyId,
        this.fxCurrencyCnvId,
        this.currencyRate,
        this.fxAmount,
        this.fxNetAmount,
        this.serviceSuppTypeId,
        this.serviceLinNum,
        this.taxPerc,
        this.isTaxApplicable});

  OtherCostDetails.fromJson(Map<String, dynamic> json) {
    serviceId = json['ServiceId'];
    serviceCategoryId = json['ServiceCategoryId'];
    supplierId = json['SupplierId'];
    serviceAmount = json['ServiceAmount'];
    uOM = json['UOM'];
    unitQty = json['UnitQty'];
    actualPrice = json['ActualPrice'];
    discPerc = json['DiscPerc'];
    discAmount = json['DiscAmount'];
    grossAmount = json['GrossAmount'];
    taxAmount = json['TaxAmount'];
    netAmount = json['NetAmount'];
    taxSlabId = json['TaxSlabId'];
    currencyId = json['CurrencyId'];
    fxCurrencyCnvId = json['FxCurrencyCnvId'];
    currencyRate = json['CurrencyRate'];
    fxAmount = json['FxAmount'];
    fxNetAmount = json['FxNetAmount'];
    serviceSuppTypeId = json['ServiceSuppTypeId'];
    serviceLinNum = json['ServiceLinNum'];
    taxPerc = json['TaxPerc'];
    isTaxApplicable = json['IsTaxApplicable'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ServiceId'] = this.serviceId;
    data['ServiceCategoryId'] = this.serviceCategoryId;
    data['SupplierId'] = this.supplierId;
    data['ServiceAmount'] = this.serviceAmount;
    data['UOM'] = this.uOM;
    data['UnitQty'] = this.unitQty;
    data['ActualPrice'] = this.actualPrice;
    data['DiscPerc'] = this.discPerc;
    data['DiscAmount'] = this.discAmount;
    data['GrossAmount'] = this.grossAmount;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['TaxSlabId'] = this.taxSlabId;
    data['CurrencyId'] = this.currencyId;
    data['FxCurrencyCnvId'] = this.fxCurrencyCnvId;
    data['CurrencyRate'] = this.currencyRate;
    data['FxAmount'] = this.fxAmount;
    data['FxNetAmount'] = this.fxNetAmount;
    data['ServiceSuppTypeId'] = this.serviceSuppTypeId;
    data['ServiceLinNum'] = this.serviceLinNum;
    data['TaxPerc'] = this.taxPerc;
    data['IsTaxApplicable'] = this.isTaxApplicable;
    return data;
  }
}

class OtherCostTaxDetails {
  int serviceId;
  int serviceLinNum;
  int taxSlabId;

  OtherCostTaxDetails({this.serviceId, this.serviceLinNum, this.taxSlabId});

  OtherCostTaxDetails.fromJson(Map<String, dynamic> json) {
    serviceId = json['ServiceId'];
    serviceLinNum = json['ServiceLinNum'];
    taxSlabId = json['TaxSlabId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ServiceId'] = this.serviceId;
    data['ServiceLinNum'] = this.serviceLinNum;
    data['TaxSlabId'] = this.taxSlabId;
    return data;
  }
}

class FOCDetails {
  int fOCItemId;
  int fOCItemLineNo;
  int parentItemLineNo;
  int parentItemId;
  String fOCItemCode;

  FOCDetails(
      {this.fOCItemId,
        this.fOCItemLineNo,
        this.parentItemLineNo,
        this.parentItemId,
        this.fOCItemCode});

  FOCDetails.fromJson(Map<String, dynamic> json) {
    fOCItemId = json['FOCItemId'];
    fOCItemLineNo = json['FOCItemLineNo'];
    parentItemLineNo = json['ParentItemLineNo'];
    parentItemId = json['ParentItemId'];
    fOCItemCode = json['FOCItemCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FOCItemId'] = this.fOCItemId;
    data['FOCItemLineNo'] = this.fOCItemLineNo;
    data['ParentItemLineNo'] = this.parentItemLineNo;
    data['ParentItemId'] = this.parentItemId;
    data['FOCItemCode'] = this.fOCItemCode;
    return data;
  }
}
