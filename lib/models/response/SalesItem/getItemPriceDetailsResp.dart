class GetItemPriceDetailsResp {
  List<MasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetItemPriceDetailsResp({this.masterDataList, this.validationDetails});

  GetItemPriceDetailsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<MasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new MasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class MasterDataList {
  double unitPrice;
  double availableQty;

  MasterDataList({this.unitPrice, this.availableQty});

  MasterDataList.fromJson(Map<String, dynamic> json) {
    unitPrice = json['UnitPrice'];
    availableQty = json['AvailableQty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UnitPrice'] = this.unitPrice;
    data['AvailableQty'] = this.availableQty;
    return data;
  }
}

class ValidationDetails {
  List<ErrorDetails> errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    if (json['ErrorDetails'] != null) {
      errorDetails = new List<ErrorDetails>();
      json['ErrorDetails'].forEach((v) {
        errorDetails.add(new ErrorDetails.fromJson(v));
      });
    }
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorDetails != null) {
      data['ErrorDetails'] = this.errorDetails.map((v) => v.toJson()).toList();
    }
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}

class ErrorDetails {
  String errorMessageDescription;
  String errorCode;

  ErrorDetails({this.errorMessageDescription, this.errorCode});

  ErrorDetails.fromJson(Map<String, dynamic> json) {
    errorMessageDescription = json['ErrorMessageDescription'];
    errorCode = json['ErrorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorMessageDescription'] = this.errorMessageDescription;
    data['ErrorCode'] = this.errorCode;
    return data;
  }
}
