import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';
import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';

class GetDivisionResponse {
  List<CommonDataList> masterDataList;
  ValidationDetails validationDetails;

  GetDivisionResponse({this.masterDataList, this.validationDetails});

  GetDivisionResponse.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<CommonDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new CommonDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}
