import 'common_validation_resp.dart';

class AccountsResp {
  List<AccountsList> accountsList;
  ValidationDetails validationDetails;

  AccountsResp({this.accountsList, this.validationDetails});

  AccountsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      accountsList = new List<AccountsList>();
      json['MasterDataList'].forEach((v) {
        accountsList.add(new AccountsList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.accountsList != null) {
      data['MasterDataList'] =
          this.accountsList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class AccountsList {

  int clientTypeId;
  String name;
  String contactNo;
  String accountName;

  AccountsList({this.clientTypeId, this.name, this.contactNo, this.accountName});

  AccountsList.fromJson(Map<String, dynamic> json) {
    clientTypeId = json['ClientTypeId'];
    name = json['Name'];
    contactNo = json['ContactNo'];
    accountName = json['AccountName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ClientTypeId'] = this.clientTypeId;
    data['Name'] = this.name;
    data['ContactNo'] = this.contactNo;
    data['AccountName']=this.accountName;
    return data;
  }
}

