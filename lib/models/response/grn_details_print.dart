class GRNDetails {
  Data data;
  ValidationDetails validationDetails;

  GRNDetails({this.data, this.validationDetails});

  GRNDetails.fromJson(Map<String, dynamic> json) {
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class Data {
  List<ItemDetails> itemDetails;

  Data({this.itemDetails});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemDetails {
  int itemId;
  String itemCode;
  String itemName;
  int packingId;
  String pkgName;
  String packingDesc;
  String brandName;
  String countryName;
  String itemBarcode;
  String batchNo;
  double gRNQty;
  String productionDateTemp;
  String expiryDateTemp;
  bool isFoc;
  double unitPrice;
  double totalPrice;
  double discPercent;
  double discAmount;
  double taxAmount;
  double netAmount;

  ItemDetails(
      {this.itemId,
        this.itemCode,
        this.itemName,
        this.packingId,
        this.pkgName,
        this.packingDesc,
        this.brandName,
        this.countryName,
        this.itemBarcode,
        this.batchNo,
        this.gRNQty,
        this.productionDateTemp,
        this.expiryDateTemp,
        this.isFoc,
        this.unitPrice,
        this.totalPrice,
        this.discPercent,
        this.discAmount,
        this.taxAmount,
        this.netAmount});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    packingId = json['PackingId'];
    pkgName = json['PkgName'];
    packingDesc = json['PackingDesc'];
    brandName = json['BrandName'];
    countryName = json['CountryName'];
    itemBarcode = json['ItemBarcode'];
    batchNo = json['BatchNo'];
    gRNQty = json['GRNQty'];
    productionDateTemp = json['ProductionDateTemp'];
    expiryDateTemp = json['ExpiryDateTemp'];
    isFoc = json['IsFoc'];
    unitPrice = json['UnitPrice'];
    totalPrice = json['TotalPrice'];
    discPercent = json['DiscPercent'];
    discAmount = json['DiscAmount'];
    taxAmount = json['TaxAmount'];
    netAmount = json['NetAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['PackingId'] = this.packingId;
    data['PkgName'] = this.pkgName;
    data['PackingDesc'] = this.packingDesc;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['ItemBarcode'] = this.itemBarcode;
    data['BatchNo'] = this.batchNo;
    data['GRNQty'] = this.gRNQty;
    data['ProductionDateTemp'] = this.productionDateTemp;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['IsFoc'] = this.isFoc;
    data['UnitPrice'] = this.unitPrice;
    data['TotalPrice'] = this.totalPrice;
    data['DiscPercent'] = this.discPercent;
    data['DiscAmount'] = this.discAmount;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    return data;
  }
}

class ValidationDetails {
  Null errorDetails;
  String statusMessage;
  int statusCode;

  ValidationDetails({this.errorDetails, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    errorDetails = json['ErrorDetails'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorDetails'] = this.errorDetails;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}
