class SaveOBReq {

  static final SaveOBReq _saveOBReq = new SaveOBReq._internal();

  factory SaveOBReq() => _saveOBReq;

  SaveOBReq._internal();

  static SaveOBReq get shared => _saveOBReq;

  String entryDateTemp;
  int companyId;
  int divisionId;
  int locationId;
  String remarks;
  List<ItemDetails> itemDetails;

  fromJson(Map<String, dynamic> json) {
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocationId'] != null) {
      locationId = json['LocationId'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  SaveOBReq.fromJson(Map<String, dynamic> json) {
    entryDateTemp = json['EntryDateTemp'];
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
    remarks = json['Remarks'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EntryDateTemp'] = this.entryDateTemp;
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['Remarks'] = this.remarks;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    entryDateTemp = companyId = null;
    locationId = divisionId = remarks = null;
    if (itemDetails != null){
      itemDetails.clear();
    }
  }
}
class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  double qty;
  double averageCost;
  double netCost;
  int isBatchEnabledItem;
  double batchCost;
  String batchNo;
  String expiryDateTemp;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;

  ItemDetails(
      {this.lineNum,
        this.itemId,
        this.itemCode,
        this.packingId,
        this.qty,
        this.averageCost,
        this.netCost,
        this.isBatchEnabledItem,
        this.batchCost,
        this.batchNo,
        this.expiryDateTemp,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['PackingId'] != null) {
      packingId = json['PackingId'];
    }
    if (json['PhyQty'] != null) {
      qty = json['PhyQty'];
    }
    if (json['AverageCost'] != null && json['AverageCost'] != '') {
      averageCost = double.parse(json['AverageCost']);
    }
    if (json['NetCost'] != null && json['NetCost'] != '') {
      netCost = double.parse(json['NetCost']);
    }
    if (json['IsBatchEnabledItem'] != null) {
      isBatchEnabledItem = json['IsBatchEnabledItem'];
    }
    if (json['BatchCost'] != null && json['BatchCost'] != '') {
      batchCost = double.parse(json['BatchCost']);
    }
    if (json['BatchNo'] != null) {
      batchNo = json['BatchNo'];
    }
    if (json['ExpiryDateTemp'] != null) {
      expiryDateTemp = json['ExpiryDateTemp'];
    }
    if (json['WarehouseId'] != null) {
      warehouseId = json['WarehouseId'];
    }
    if (json['WHouseZoneId'] != null) {
      wHouseZoneId = json['WHouseZoneId'];
    }
    if (json['WHouseZoneBinId'] != null) {
      wHouseZoneBinId = json['WHouseZoneBinId'];
    }
    if (json['WHouseZoneBinRackId'] != null) {
      wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['Qty'] = this.qty;
    data['AverageCost'] = this.averageCost;
    data['NetCost'] = this.netCost;
    data['IsBatchEnabledItem'] = this.isBatchEnabledItem;
    data['BatchCost'] = this.batchCost;
    data['BatchNo'] = this.batchNo;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    return data;
  }
}
