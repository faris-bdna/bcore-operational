import 'package:bcore_inventory_management/models/view/item_detail.dart';

class MRReq {
  static final MRReq _mrReq = new MRReq._internal();
  factory MRReq() => _mrReq;
  MRReq._internal();
  static MRReq get shared => _mrReq;

  String mtlReqHdId;
  String reqDateTemp;
  String fromCompanyId;
  String fromDivisionId;
  String fromLocId;
  String fromDepartmentId;
  String fromProjectId;
  String toCompanyId;
  String toDivisionId;
  String toLocId;
  String toDepartmentId;
  String expReceivingDateTemp;
  String expReceivingTime;
  String deliveryLocId;
  String deliveryPriorityId;
  String comments;
  // String reqDate;
  // String expReceivingDate;
  List<CommonItem> itemDetails;

  fromViewJson(Map<String, dynamic> json) {
    if (json['FromCompanyId'] != null) {
      fromCompanyId = json['FromCompanyId'];
    }
    if (json['FromDivisionId'] != null) {
      fromDivisionId = json['FromDivisionId'];
    }
    if (json['ToCompanyId'] != null) {
      toCompanyId = json['ToCompanyId'];
    }
    if (json['ToDivisionId'] != null) {
      toDivisionId = json['ToDivisionId'];
    }
    if (json['FromLocationId'] != null) {
      fromLocId = json['FromLocationId'];
    }
    if (json['ToLocationId'] != null) {
      toLocId = json['ToLocationId'];
    }
    if (json['FromDepartmentId'] != null) {
      fromDepartmentId = json['FromDepartmentId'];
    }
    if (json['ToDepartmentId'] != null) {
      toDepartmentId = json['ToDepartmentId'];
    }
    if (json['FromProjectId'] != null) {
      fromProjectId = json['FromProjectId'];
    }
    if (json['FromDeliveryAddressId'] != null) {
      deliveryLocId = json['FromDeliveryAddressId'];
    }
    if (json['ToDeliveryPriority'] != null) {
      deliveryPriorityId = json['ToDeliveryPriority'];
    }
    if (json['ToTransactionDate'] != null) {
      reqDateTemp = json['ToTransactionDate'];
    }
    if (json['ToExpiryDate'] != null) {
      expReceivingDateTemp = json['ToExpiryDate'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<CommonItem>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new CommonItem.fromJson(v));
      });
    }

  }


  MRReq.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      itemDetails = new List<CommonItem>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new CommonItem.fromJson(v));
      });
    }
    mtlReqHdId = json['MtlReqHdId'];
    reqDateTemp = json['ReqDateTemp'];
    fromCompanyId = json['FromCompanyId'];
    fromDivisionId = json['FromDivisionId'];
    fromLocId = json['FromLocId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromProjectId = json['FromProjectId'];
    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    toDepartmentId = json['ToDepartmentId'];
    expReceivingDateTemp = json['ExpReceivingDateTemp'];
    expReceivingTime = json['ExpReceivingTime'];
    deliveryLocId = json['DeliveryLocId'];
    deliveryPriorityId = json['DeliveryPriorityId'];
    comments = json['Comments'];
    // reqDate = json['ReqDate'];
    // expReceivingDate = json['ExpReceivingDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    data['MtlReqHdId'] = this.mtlReqHdId;
    data['ReqDateTemp'] = this.reqDateTemp;
    data['FromCompanyId'] = this.fromCompanyId;
    data['FromDivisionId'] = this.fromDivisionId;
    data['FromLocId'] = this.fromLocId;
    data['FromDepartmentId'] = this.fromDepartmentId;
    data['FromProjectId'] = this.fromProjectId;
    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['ToDepartmentId'] = this.toDepartmentId;
    data['ExpReceivingDateTemp'] = this.expReceivingDateTemp;
    data['ExpReceivingTime'] = this.expReceivingTime;
    data['DeliveryLocId'] = this.deliveryLocId;
    data['DeliveryPriorityId'] = this.deliveryPriorityId;
    data['Comments'] = this.comments;
    // data['ReqDate'] = this.reqDate;
    // data['ExpReceivingDate'] = this.expReceivingDate;
    return data;
  }

  clear() {
     mtlReqHdId = reqDateTemp = fromCompanyId = null;
     fromDivisionId = fromLocId = fromDepartmentId = null;
     fromProjectId = toCompanyId = toDivisionId = null;
     toLocId = toDepartmentId = expReceivingDateTemp = null;
     expReceivingTime = deliveryLocId = deliveryPriorityId = null;
     comments = null;
         // reqDate = expReceivingDate = null;
     itemDetails = null;
  }
}
