class SOReq {

  static final SOReq _soReq = new SOReq._internal();
  factory SOReq() => _soReq;
  SOReq._internal();
  static SOReq get shared => _soReq;

  String orderRemarks;

  int projectId;
  int companyId;
  int divisionId;
  int locationId;
  int departmentId;

  String entryDate;
  int salesmanId;
  int custContactTypeId;
  int refContactId;
  int salesType;
  String expectedDeliveryDate;

  double discountOnGross;      /// Discount as amount

  List<SalesOrderMaterialsListTemp> salesOrderMaterialsListTemp;

  SOReq.fromJson(Map<String, dynamic> json) {
    projectId = json['ProjectId'];
    orderRemarks = json['OrderRemarks'];
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
    entryDate = json['EntryDate'];
    departmentId = json['DepartmentId'];
    salesmanId = json['SalesmanId'];
    discountOnGross = json['DiscountOnGross'];
    custContactTypeId = json['CustContactTypeId'];
    refContactId = json['RefContactId'];
    salesType = json['SalesType'];
    expectedDeliveryDate = json['ExpectedDeliveryDate'];
    if (json['SalesOrderMaterialsListTemp'] != null) {
      salesOrderMaterialsListTemp = new List<SalesOrderMaterialsListTemp>();
      json['SalesOrderMaterialsListTemp'].forEach((v) {
        salesOrderMaterialsListTemp
            .add(new SalesOrderMaterialsListTemp.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['FromProjectId'] != null) {
      projectId = int.parse(json['FromProjectId']);
  }
    if (json['OrderRemarks'] != null) {
      orderRemarks = json['OrderRemarks'];
    }
    if (json['FromCompanyId'] != null) {
      companyId = int.parse(json['FromCompanyId']);
    }
    if (json['FromDivisionId'] != null) {
      divisionId = int.parse(json['FromDivisionId']);
    }
    if (json['FromLocationId'] != null) {
      locationId = int.parse(json['FromLocationId']);
    }
    if (json['EntryDate'] != null) {
      entryDate = json['EntryDate'];
    }
    if (json['FromDepartmentId'] != null) {
      departmentId = int.parse(json['FromDepartmentId']);
    }
    if (json['SalesmanId'] != null) {
      salesmanId = json['SalesmanId'];
    }
    if (json['DiscountOnGross'] != null) {
      discountOnGross = json['DiscountOnGross'];
    }
    if (json['BillingPartyType'] != null) {
      custContactTypeId = json['BillingPartyType'];
    }
    if (json['BillingPartyID'] != null) {
      refContactId = json['BillingPartyID'];
    }
    if (json['SalesTypeId'] != null) {
      salesType = int.parse(json['SalesTypeId']);
    }
    if (json['ExpectedDelivDate'] != null) {
      expectedDeliveryDate = json['ExpectedDelivDate'];
    }
    if (json['ItemDetails'] != null) {
      salesOrderMaterialsListTemp = new List<SalesOrderMaterialsListTemp>();
      json['ItemDetails'].forEach((v) {
        salesOrderMaterialsListTemp
            .add(new SalesOrderMaterialsListTemp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProjectId'] = this.projectId;
    data['OrderRemarks'] = this.orderRemarks;
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['EntryDateTemp'] = this.entryDate;
    data['DepartmentId'] = this.departmentId;
    data['SalesmanId'] = this.salesmanId;
    data['DiscountOnGross'] = this.discountOnGross;
    data['CustContactTypeId'] = this.custContactTypeId;
    data['RefContactId'] = this.refContactId;
    data['SalesType'] = this.salesType;
    data['ExpectedDeliveryDateTemp'] = this.expectedDeliveryDate;
    if (this.salesOrderMaterialsListTemp != null) {
      data['SalesOrderMaterialsListTemp'] =
          this.salesOrderMaterialsListTemp.map((v) => v.toJson()).toList();
    }
    return data;
  }


  clear() {
    projectId = orderRemarks = companyId = divisionId = locationId = null;
    entryDate = departmentId = salesmanId = discountOnGross = null;
    custContactTypeId = refContactId = salesType = expectedDeliveryDate = null;
    salesOrderMaterialsListTemp = null;
  }
}

class SalesOrderMaterialsListTemp {
  int itemId;
  int unitId;
  double orderQty;
  double rate;
  double discountPercentage;
  double discount;
  String itemBarcode;
  bool isFromBackEnd;
  double fOCQuantity = 0.0;

  SalesOrderMaterialsListTemp(
      {this.itemId,
        this.unitId,
        this.orderQty,
        this.rate,
        this.discountPercentage,
        this.discount,
        this.itemBarcode,
        this.isFromBackEnd,
        this.fOCQuantity});

  SalesOrderMaterialsListTemp.fromJson(Map<String, dynamic> json) {
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      orderQty = json['Quantity'];
    }
    if (json['Rate'] != null) {
      rate = json['Rate'];
    }
    if (json['DiscountPercentage'] != null) {
      discountPercentage = json['DiscountPercentage'];
    }
    if (json['Discount'] != null) {
      discount = json['Discount'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'].toString();
    }
    if (json['IsFromBackEnd'] != null) {
      isFromBackEnd = json['IsFromBackEnd'];
    }
    if (json['FocQuantity'] != null) {
      fOCQuantity = json['FocQuantity'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['UnitId'] = this.unitId;
    data['OrderQty'] = this.orderQty;
    data['Rate'] = this.rate;
    data['DiscountPercentage'] = this.discountPercentage;
    data['Discount'] = this.discount;
    data['ItemBarcode'] = this.itemBarcode;
    data['IsFromBackEnd'] = this.isFromBackEnd;
    data['FOCQuantity'] = this.fOCQuantity;
    return data;
  }
}
