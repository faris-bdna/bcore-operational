class SaveOFReq {

  static final SaveOFReq _saveOFReq = new SaveOFReq._internal();
  factory SaveOFReq() => _saveOFReq;
  SaveOFReq._internal();
  static SaveOFReq get shared => _saveOFReq;

  String entryDateTemp;
  int tranGenType;
  String companyId;
  String divisionId;
  String locationId;
  String toCompanyId;
  String toDivisionId;
  String toLocId;
  String deliveryLocId;
  int supplierId;
  String remarks;
  List<OrderItemDetails> itemDetails;

  SaveOFReq.fromJson(Map<String, dynamic> json) {
    entryDateTemp = json['EntryDateTemp'];
    tranGenType = json['TranGenType'];
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    deliveryLocId = json['DeliveryLocId'];
    supplierId = json['SupplierId'];
    remarks = json['Remarks'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<OrderItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new OrderItemDetails.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['TranGenType'] != null) {
      tranGenType = json['TranGenType'];
    }
    if (json['FromCompanyId'] != null) {
      companyId = json['FromCompanyId'];
    }
    if (json['FromDivisionId'] != null) {
      divisionId = json['FromDivisionId'];
    }
    if (json['FromLocationId'] != null) {
      locationId = json['FromLocationId'];
    }
    if (json['FromDeliveryAddressId'] != null) {
      deliveryLocId = json['FromDeliveryAddressId'];
    }
    if (json['ToCompanyId'] != null) {
      toCompanyId = json['ToCompanyId'];
    }
    if (json['ToDivisionId'] != null) {
      toDivisionId = json['ToDivisionId'];
    }
    if (json['ToLocId'] != null) {
      toLocId = json['ToLocId'];
    }
    if (json['DeliveryLocId'] != null) {
      deliveryLocId = json['DeliveryLocId'];
    }
    if (json['SupplierId'] != null) {
      supplierId = json['SupplierId'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }

    if (json['ItemDetails'] != null) {
      itemDetails = new List<OrderItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new OrderItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EntryDateTemp'] = this.entryDateTemp;
    data['TranGenType'] = this.tranGenType;
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['DeliveryLocId'] = this.deliveryLocId;
    data['SupplierId'] = this.supplierId;
    data['Remarks'] = this.remarks;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    entryDateTemp = tranGenType = null;
    companyId = divisionId = null;
    locationId = toCompanyId = null;
    toDivisionId = toLocId = itemDetails = null;
    deliveryLocId = supplierId = remarks = null;
  }
}

class OrderItemDetails {
  int lineNum;
  String itemCode;
  String itemBarcode;
  int itemId;
  int packingId;
  double orderQty;

  OrderItemDetails(
      {this.lineNum,
        this.itemCode,
        this.itemBarcode,
        this.itemId,
        this.packingId,
        this.orderQty});

  OrderItemDetails.fromJson(Map<String, dynamic> json) {
    lineNum = json['LineNum'];
    itemCode = json['ItemCode'];
    itemBarcode = json['ItemBarcode'];
    itemId = json['ItemId'];
    packingId = json['PackingId'];
    orderQty = json['OrderQty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemCode'] = this.itemCode;
    data['ItemBarcode'] = this.itemBarcode;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['OrderQty'] = this.orderQty;
    return data;
  }

}