class SaveSTReq {

  static final SaveSTReq _saveSTReq = new SaveSTReq._internal();
  factory SaveSTReq() => _saveSTReq;
  SaveSTReq._internal();
  static SaveSTReq get shared => _saveSTReq;

  String stockReferenceNo;
  int stockTypeId;
  String entryDateTemp;
  String remarks;
  String companyId;
  List<ItemDetails> itemDetails;

  String dateFormat;

  fromViewJson(Map<String, dynamic> json) {
    if (json['StockReferenceNo'] != null) {
      stockReferenceNo = json['StockReferenceNo'];
    }
    if (json['StockTypeId'] != null) {
      stockTypeId = json['StockTypeId'];
    }
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  SaveSTReq.fromJson(Map<String, dynamic> json) {
    stockReferenceNo = json['StockReferenceNo'];
    stockTypeId = json['StockTypeId'];
    entryDateTemp = json['EntryDateTemp'];
    remarks = json['Remarks'];
    companyId = json['CompanyId'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['StockReferenceNo'] = this.stockReferenceNo;
    data['StockTypeId'] = this.stockTypeId;
    data['EntryDateTemp'] = this.entryDateTemp;
    data['Remarks'] = this.remarks;
    data['CompanyId'] = this.companyId;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }


  clear() {
    stockReferenceNo = stockTypeId = remarks = null;
    entryDateTemp = companyId = null;
    if (itemDetails != null){
      itemDetails.clear();
    }
  }
}

class ItemDetails {
  int lineNum;
  String itemCode;
  int itemId;
  int packingId;
  String itemBarcode;
  double phyQty;
  int reasonId;
  String expiryDate;
  String batchno;

  ItemDetails(
      {this.lineNum,
        this.itemCode,
        this.itemId,
        this.packingId,
        this.itemBarcode,
        this.phyQty,
        this.reasonId,
        this.expiryDate,
        this.batchno});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['PackingId'] != null) {
      packingId = json['PackingId'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'];
    }
    if (json['PhyQty'] != null) {
      phyQty = json['PhyQty'];
    }

    if (json['ReasonId'] != null) {
      reasonId = json['ReasonId'];
    }
    if (json['ExpiryDate'] != null) {
      expiryDate = json['ExpiryDate'];
    }
    if (json['BatchNo'] != null) {
      batchno = json['BatchNo'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['ItemBarcode'] = this.itemBarcode;
    data['PhyQty'] = this.phyQty;
    data['ReasonId'] = this.reasonId;
    data['ExpiryDate'] = this.expiryDate;
    data['Batchno'] = this.batchno;
    return data;
  }
}
