class StockDetailReq {
  int companyId;
  int divisionId;
  int locationId;
  String entryDateTemp;
  List<ItemDetails> itemDetails;

  StockDetailReq(
      {this.companyId,
        this.divisionId,
        this.locationId,
        this.entryDateTemp,
        this.itemDetails});

  StockDetailReq.fromJson(Map<String, dynamic> json) {
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
    entryDateTemp = json['EntryDateTemp'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['EntryDateTemp'] = this.entryDateTemp;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemDetails {
  int itemId;
  int packingId;

  ItemDetails({this.itemId, this.packingId});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    itemId = json['ItemId'];
    packingId = json['PackingId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    return data;
  }
}
