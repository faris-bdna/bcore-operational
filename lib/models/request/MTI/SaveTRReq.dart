class SaveTRReq {

  static final SaveTRReq _trReq = new SaveTRReq.internal();
  factory SaveTRReq() => _trReq;
  SaveTRReq.internal();
  static SaveTRReq get shared => _trReq;

  int itemTransferId;
  int toCompanyId;
  String entryDateTemp;
  String remarks;
  List<ItemDetails> itemDetails;

  String dateFormat;

  SaveTRReq.fromJson(Map<String, dynamic> json) {
    itemTransferId = json['ItemTransferId'];
    toCompanyId = json['ToCompanyId'];
    entryDateTemp = json['EntryDateTemp'];
    remarks = json['Remarks'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['StoreIssueId'] != null) {
      itemTransferId = json['StoreIssueId'];
    }
    if (json['ToCompanyId'] != null) {
      toCompanyId = int.parse(json['ToCompanyId']);
    }
    if (json['ToTransactionDate'] != null) {
      entryDateTemp = json['ToTransactionDate'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemTransferId'] = this.itemTransferId;
    data['ToCompanyId'] = this.toCompanyId;
    data['EntryDateTemp'] = this.entryDateTemp;
    data['Remarks'] = this.remarks;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    itemTransferId = toCompanyId = toCompanyId = entryDateTemp = null;
    remarks = itemDetails = null;
  }
}

class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  String batchNo;
  double unitQty;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int storeTransferDetailId;
  String productionDateTemp;
  String expiryDateTemp;

  ItemDetails(
      {this.lineNum,
        this.itemId,
        this.itemCode,
        this.packingId,
        this.batchNo,
        this.unitQty,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.storeTransferDetailId,
        this.productionDateTemp,
        this.expiryDateTemp});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['ProdpkgId'] != null) {
      packingId = json['ProdpkgId'];
    }
    if (json['BatchNo'] != null) {
      batchNo = json['BatchNo'];
    }
    if (json['ReqQty'] != null) {
      unitQty = json['ReqQty'];
    }
    if (json['WarehouseId'] != null) {
      warehouseId = json['WarehouseId'];
    }
    if (json['WHouseZoneId'] != null) {
      wHouseZoneId = json['WHouseZoneId'];
    }
    if (json['WHouseZoneBinId'] != null) {
      wHouseZoneBinId = json['WHouseZoneBinId'];
    }
    if (json['WHouseZoneBinRackId'] != null) {
      wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    }
    if (json['StoreIssueDetailId'] != null) {
      storeTransferDetailId = json['StoreIssueDetailId'];
    }
    if (json['ProductionDateTemp'] != null) {
      productionDateTemp = json['ProductionDateTemp'];
    }
    if (json['ExpiryDate'] != null) {
      expiryDateTemp = json['ExpiryDate'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['BatchNo'] = this.batchNo;
    data['UnitQty'] = this.unitQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['StoreTransferDetailId'] = this.storeTransferDetailId;
    data['ProductionDateTemp'] = this.productionDateTemp;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    return data;
  }
}
