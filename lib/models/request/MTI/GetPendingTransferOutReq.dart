class GetPendingTransferOutReq {
  String companyId;
  String divisionId;
  String locationId;

  GetPendingTransferOutReq({this.companyId, this.divisionId, this.locationId});

  GetPendingTransferOutReq.fromJson(Map<String, dynamic> json) {
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    return data;
  }
}
