class SaveDNReq {

  static final SaveDNReq _saveDNReq = new SaveDNReq._internal();
  factory SaveDNReq() => _saveDNReq;
  SaveDNReq._internal();
  static SaveDNReq get shared => _saveDNReq;

  int salesOrderId;
  String entryDate;
  bool isDraft;
  String remarks;

  List<MaterialsList> materialsList;
  List<TermsAndCondionList> termsAndCondionList;
  List<AdditionalChargesDetailsList> additionalChargesDetailsList;

  clear(){
    salesOrderId = entryDate = isDraft = materialsList = remarks = null;
    termsAndCondionList = additionalChargesDetailsList = null;
  }

  SaveDNReq.fromJson(Map<String, dynamic> json) {
    salesOrderId = json['SalesOrderId'];
    entryDate = json['EntryDate'];
    isDraft = json['IsDraft'];
    remarks = json['Remarks'];
    if (json['MaterialsList'] != null) {
      materialsList = new List<MaterialsList>();
      json['MaterialsList'].forEach((v) {
        materialsList.add(new MaterialsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['SalesOrderId'] != null) {
      salesOrderId = json['SalesOrderId'];
    }
    if (json['EntryDate'] != null) {
      entryDate = json['EntryDate'];
    }
    if (json['IsDraft'] != null) {
      isDraft = json['IsDraft'];
    }
    if (json['OrderRemarks'] != null) {
      remarks = json['OrderRemarks'];
    }
    if (json['ItemDetails'] != null) {
      materialsList = new List<MaterialsList>();
      json['ItemDetails'].forEach((v) {
        materialsList.add(new MaterialsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SalesOrderId'] = this.salesOrderId;
    data['EntryDate'] = this.entryDate;
    data['IsDraft'] = this.isDraft;
    data['Remarks'] = this.remarks;
    if (this.materialsList != null) {
      data['MaterialsList'] =
          this.materialsList.map((v) => v.toJson()).toList();
    }
    if (this.termsAndCondionList != null) {
      data['TermsAndCondionList'] =
          this.termsAndCondionList.map((v) => v.toJson()).toList();
    }
    if (this.additionalChargesDetailsList != null) {
      data['AdditionalChargesDetailsList'] =
          this.additionalChargesDetailsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MaterialsList {
  int itemId;
  int unitId;
  double quantity;
  int productStockTypeId;
  double fOCQuantity;
  List<SerialNumbersList> serialNumbersList;

  MaterialsList(
      {this.itemId,
        this.unitId,
        this.quantity,
        this.productStockTypeId,
        this.fOCQuantity,
        this.serialNumbersList});

  MaterialsList.fromJson(Map<String, dynamic> json) {
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      quantity = json['Quantity'];
    }
    if (json['ProductStockTypeId'] != null) {
      productStockTypeId = json['ProductStockTypeId'];
    }
    if (json['FocQuantity'] != null) {
      fOCQuantity = json['FocQuantity'];
    }
    if (json['SerialNumbersList'] != null) {
      serialNumbersList = new List<SerialNumbersList>();
      json['SerialNumbersList'].forEach((v) {
        serialNumbersList.add(new SerialNumbersList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['UnitId'] = this.unitId;
    data['Quantity'] = this.quantity;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['FOCQuantity'] = this.fOCQuantity;
    if (this.serialNumbersList != null) {
      data['SerialNumbersList'] =
          this.serialNumbersList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SerialNumbersList {
  String serialNo;
  int gRNSerialNoId;

  SerialNumbersList({this.serialNo, this.gRNSerialNoId});

  SerialNumbersList.fromJson(Map<String, dynamic> json) {
    serialNo = json['SerialNo'];
    gRNSerialNoId = json['GRNSerialNoId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SerialNo'] = this.serialNo;
    data['GRNSerialNoId'] = this.gRNSerialNoId;
    return data;
  }
}

class TermsAndCondionList {
  int termsId;
  String termsAndConditions;
  int sortOrder;
  int paymentTypeId = 0;
  double paymentValue = 0.0;
  int scopeTypeId = 0;

  TermsAndCondionList(
      {this.termsId,
        this.termsAndConditions,
        this.sortOrder,
        this.paymentTypeId,
        this.paymentValue,
        this.scopeTypeId});

  TermsAndCondionList.fromJson(Map<String, dynamic> json) {
    if (json['TermsId'] != null){
      termsId = json['TermsId'];
    }
    if (json['TermsAndConditions'] != null) {
      termsAndConditions = json['TermsAndConditions'];
    }
    if (json['SortOrder'] != null) {
      sortOrder = json['SortOrder'];
    }
    if (json['PaymentTypeId'] != null) {
      paymentTypeId = json['PaymentTypeId'];
    }
    if (json['PaymentValue'] != null) {
      paymentValue = json['PaymentValue'];
    }
    if (json['ScopeTypeId'] != null) {
      scopeTypeId = json['ScopeTypeId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['TermsId'] = this.termsId;
    data['TermsAndConditions'] = this.termsAndConditions;
    data['SortOrder'] = this.sortOrder;
    data['PaymentTypeId'] = this.paymentTypeId;
    data['PaymentValue'] = this.paymentValue;
    data['ScopeTypeId'] = this.scopeTypeId;
    return data;
  }
}

class AdditionalChargesDetailsList {
  int itemId;
  int unitId;
  double quantity;
  double rate;
  double total;
  double taxAmount;
  double taxPercent;
  int taxSlabId;
  double netAmount;
  int taxID;
  String bOQDescription;

  AdditionalChargesDetailsList(
      {this.itemId,
        this.unitId,
        this.quantity,
        this.rate,
        this.total,
        this.taxAmount,
        this.taxPercent,
        this.taxSlabId,
        this.netAmount,
        this.taxID,
        this.bOQDescription});

  AdditionalChargesDetailsList.fromJson(Map<String, dynamic> json) {
    if (json["ProductId"] != null) {
      itemId = json["ProductId"];
    }
    if (json["UnitId"] != null) {
      unitId = json["UnitId"];
    }
    if (json["Quantity"] != null) {
      quantity = json["Quantity"];
    }
    if (json["UnitPrice"] != null) {
      rate = json["UnitPrice"];
    }
    if (json["Total"] != null) {
      total = json["Total"];
    }
    if (json["TaxAmount"] != null) {
      taxAmount = json["TaxAmount"];
    }
    if (json["TaxPercent"] != null) {
      taxPercent = json["TaxPercent"];
    }
    if (json["TaxSlabId"] != null) {
      taxSlabId = json["TaxSlabId"];
    }
    if (json["NetAmount"] != null) {
      netAmount = json["NetAmount"];
    }
    if (json["TaxId"] != null) {
      taxID = json["TaxId"];
    }
    if (json["BOQDescription"] != null) {
      bOQDescription = json["BOQDescription"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['UnitId'] = this.unitId;
    data['Quantity'] = this.quantity;
    data['Rate'] = this.rate;
    data['Total'] = this.total;
    data['TaxAmount'] = this.taxAmount;
    data['TaxPercent'] = this.taxPercent;
    data['TaxSlabId'] = this.taxSlabId;
    data['NetAmount'] = this.netAmount;
    data['TaxID'] = this.taxID;
    data['BOQDescription'] = this.bOQDescription;
    return data;
  }
}
