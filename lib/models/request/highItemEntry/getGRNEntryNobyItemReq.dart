class GetGRNEntryNobyItemReq {
  String companyId;
  String divisionId;
  String locationId;
  String departmentId;
  String projectId;
  String supplierId;
  String priceType;
  String itemId;
  String packingId;
  String batchNo;

  GetGRNEntryNobyItemReq(
      {this.companyId,
        this.divisionId,
        this.locationId,
        this.departmentId,
        this.projectId,
        this.supplierId,
        this.priceType,
        this.itemId,
        this.packingId,
        this.batchNo});

  GetGRNEntryNobyItemReq.fromJson(Map<String, dynamic> json) {
    companyId = json['FromCompanyId'];
    divisionId = json['FromDivisionId'];
    locationId = json['FromLocationId'];
    departmentId = json['FromDepartmentId'];
    projectId = json['ProjectId'];
    supplierId = json['SupplierId'];
    priceType = json['PriceType'];
    itemId = json['ItemId'];
    packingId = json['PackingId'];
    batchNo = json['BatchNo'];
  }

  fromJson(Map<String, dynamic> json) {
    if (json['CompanyId'] != null){
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocationId'] != null) {
      locationId = json['LocationId'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['SupplierId'] != null) {
      supplierId = json['SupplierId'];
    }
    if (json['PriceType'] != null) {
      priceType = json['PriceType'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['PackingId'] != null) {
      packingId = json['PackingId'];
    }
    if (json['BatchNo'] != null) {
      batchNo = json['BatchNo'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['DepartmentId'] = this.departmentId;
    data['ProjectId'] = this.projectId;
    data['SupplierId'] = this.supplierId;
    data['PriceType'] = this.priceType;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['BatchNo'] = this.batchNo;
    return data;
  }
}
