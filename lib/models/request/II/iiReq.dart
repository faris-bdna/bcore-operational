import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';

class SaveIIReq {

  static final SaveIIReq _iiReq = new SaveIIReq._internal();
  factory SaveIIReq() => _iiReq;
  SaveIIReq._internal();
  static SaveIIReq get shared => _iiReq;

  String entryDateTemp;

  int fromCompanyId;
  int fromDivisionId;
  int fromLocId;
  int fromDepartmentId;
  int fromProjectId;

  int toCompanyId;
  int toDivisionId;
  int toDepartmentId;
  int toLocId;
  int toProjectId;

  int issueTypeId;
  int issueSrcId;

  String dateFormat;

  String remarks;

  List<ItemDetails> itemDetails;

  List<SubstituteItemDetails> substituteItemDetails = [];

  SaveIIReq.fromJson(Map<String, dynamic> json) {
    entryDateTemp = json['EntryDateTemp'];

    fromCompanyId = json['FromCompanyId'];
    fromDivisionId = json['FromDivisionId'];
    fromLocId = json['FromLocId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromProjectId = json['FromProjectId'];

    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    toDepartmentId = json['ToDepartmentId'];
    toProjectId = json['ToProjectId'];

    remarks = json['Remarks'];

    issueTypeId = json['IssueTypeId'];
    issueSrcId = json['IssueSrcId'];

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }

    if (json['SubstituteItemDetails'] != null) {
      substituteItemDetails = new List<SubstituteItemDetails>();
      json['SubstituteItemDetails'].forEach((v) {
        substituteItemDetails.add(new SubstituteItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EntryDateTemp'] = this.entryDateTemp;

    data['FromCompanyId'] = this.fromCompanyId;
    data['FromDivisionId'] = this.fromDivisionId;
    data['FromLocId'] = this.fromLocId;
    data['FromDepartmentId'] = this.fromDepartmentId;
    data['FromProjectId'] = this.fromProjectId;

    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['ToDepartmentId'] = this.toDepartmentId;
    data['ToProjectId'] = this.toProjectId;

    data['Remarks'] = this.remarks;

    data['IssueSrcId'] = this.issueSrcId;
    data['IssueTypeId'] = this.issueTypeId;

    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.substituteItemDetails != null) {
      data['SubstituteItemDetails'] =
          this.substituteItemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['FromCompanyId'] != null) {
      fromCompanyId = int.parse(json['FromCompanyId']);
    }
    if (json['FromDivisionId'] != null) {
      fromDivisionId = int.parse(json['FromDivisionId']);
    }
    if (json['FromLocationId'] != null) {
      fromLocId = int.parse(json['FromLocationId']);
    }
    if (json['FromDepartmentId'] != null) {
      fromDepartmentId = int.parse(json['FromDepartmentId']);
    }
    if (json['FromProjectId'] != null) {
      fromProjectId = int.parse(json['FromProjectId']);
    }

    if (json['ToCompanyId'] != null) {
      toCompanyId = int.parse(json['ToCompanyId']);
    }
    if (json['ToDivisionId'] != null) {
      toDivisionId = int.parse(json['ToDivisionId']);
    }
    if (json['ToLocationId'] != null) {
      toLocId = int.parse(json['ToLocationId']);
    }
    if (json['ToDepartmentId'] != null) {
      toDepartmentId = int.parse(json['ToDepartmentId']);
    }
    if (json['ToProjectId'] != null) {
      toProjectId = int.parse(json['ToProjectId']);
    }

    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ToTransactionDate'] != null) {
      entryDateTemp = json['ToTransactionDate'];
    }


    if (json['TransferTypeId'] != null) {
      issueTypeId = json['TransferTypeId'];
    }
    if (json['StoreIssueId'] != null) {
      issueSrcId = json['StoreIssueId'];
    }


    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['SubstituteItemDetails'] != null) {
      substituteItemDetails = new List<SubstituteItemDetails>();
      json['SubstituteItemDetails'].forEach((v) {
        substituteItemDetails.add(new SubstituteItemDetails.fromJson(v));
      });
    }
  }

  clear() {
    entryDateTemp = fromCompanyId = fromDivisionId = null;
    fromDepartmentId = fromProjectId = fromLocId = null;
    toCompanyId = toDivisionId = toLocId = null;
    toDepartmentId = toProjectId = remarks = null;
    itemDetails = substituteItemDetails =null;
    issueTypeId = issueSrcId = null;
  }
}

class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  double unitQty;
  double unitPrice;


  String itemBarcode;
  int referenceDtId;

  ItemDetails(
      {this.lineNum,
        this.itemId,
        this.itemCode,
        this.packingId,
        this.unitQty,
        this.unitPrice,
        this.itemBarcode, this.referenceDtId
      });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['ProdpkgId'] != null) {
      packingId = json['ProdpkgId'];
    }
    if (json['ReqQty'] != null) {
      unitQty = json['ReqQty'];
    }
    if (json['UnitCost'] != null) {
      unitPrice = json['UnitCost'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'];
    }
    if (json['ReferenceDtId'] != null) {
      referenceDtId = json['ReferenceDtId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['UnitQty'] = this.unitQty;
    data['UnitPrice'] = this.unitPrice;
    data['ItemBarcode'] = this.itemBarcode;
    data['ReferenceDtId'] = this.referenceDtId;
    return data;
  }
}
