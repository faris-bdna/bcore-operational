class OtherCostTaxDetails {
  int serviceId;
  String serviceCode;
  int serviceLinNum;
  int taxSlabId;

  OtherCostTaxDetails(
      {this.serviceId, this.serviceCode, this.serviceLinNum, this.taxSlabId});

  OtherCostTaxDetails.fromJson(Map<String, dynamic> json) {
    serviceId = json['ServiceId'];
    serviceCode = json['ServiceCode'];
    serviceLinNum = json['ServiceLinNum'];
    taxSlabId = json['TaxSlabId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ServiceId'] = this.serviceId;
    data['ServiceCode'] = this.serviceCode;
    data['ServiceLinNum'] = this.serviceLinNum;
    data['TaxSlabId'] = this.taxSlabId;
    return data;
  }
}