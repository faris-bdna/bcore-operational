class FOCDetails {
  int fOCItemId;
  int gRNItemLineNo;
  int parentItemId;
  int fOCItemLineNo;
  String parentItemCode;
  String focIndex;
  String parentIndex;

  FOCDetails(
      {this.fOCItemId,
        this.gRNItemLineNo,
        this.parentItemId,
        this.fOCItemLineNo,
        this.parentItemCode,
        this.focIndex,
        this.parentIndex
      });

  FOCDetails.fromJson(Map<String, dynamic> json) {
    fOCItemId = json['FOCItemId'];
    gRNItemLineNo = json['GRNItemLineNo'];
    parentItemId = json['ParentItemId'];
    fOCItemLineNo = json['FOCItemLineNo'];
    parentItemCode = json['ParentItemCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FOCItemId'] = this.fOCItemId;
    data['GRNItemLineNo'] = this.gRNItemLineNo;
    data['ParentItemId'] = this.parentItemId;
    data['FOCItemLineNo'] = this.fOCItemLineNo;
    data['ParentItemCode'] = this.parentItemCode;
    return data;
  }
}

// ignore: non_constant_identifier_names
final List <FOCDetails> grn_foc_details = List<FOCDetails>();