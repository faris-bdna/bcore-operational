class SerialNoDetails {
  int dtLineNum;
  int serialLineNum;
  String serialNo;
  int itemId;
  String itemCode;

  SerialNoDetails(
      {this.dtLineNum,
        this.serialLineNum,
        this.serialNo,
        this.itemId,
        this.itemCode});

  SerialNoDetails.fromJson(Map<String, dynamic> json) {
    dtLineNum = json['DtLineNum'];
    serialLineNum = json['SerialLineNum'];
    serialNo = json['SerialNo'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DtLineNum'] = this.dtLineNum;
    data['SerialLineNum'] = this.serialLineNum;
    data['SerialNo'] = this.serialNo;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    return data;
  }
}