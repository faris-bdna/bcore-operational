import 'grnItemDetail.dart';
import 'grnItemOtherCostDetail.dart';
import 'grnItemFocDetail.dart';
import 'grnItemSerialNoDetail.dart';
import 'grnItemTaxDetail.dart';

class GrnReq {

  static final GrnReq _grnReq = new GrnReq._internal();
  factory GrnReq() => _grnReq;
  GrnReq._internal();
  static GrnReq get shared => _grnReq;

  int suppTypeId;
  int companyId;
  int divisionId;
  int locId;
  int departmentId;
  int projectId;
  int gRNTypeId;
  int gRNTypeRefNo;
  String gRNDateTemp;
  String docDateTemp;
  int suppId;
  bool isTaxApp;
  int currencyId;
  double discPercentage;
  String remarks;
  bool isQualityChecked;
  bool isCashPurchase;
  bool isInvoiceRcvd;
  String invoiceNo;
  String deliveryNoteNo;

  List<ItemDetails> itemDetails;

  List<FOCDetails> fOCDetails;
  List<SerialNoDetails> serialNoDetails;
  List<OtherCostDetails> otherCostDetails;
  List<OtherCostTaxDetails> otherCostTaxDetails;

  bool isSupplierInternal;

  String index;

  GrnReq.fromJson(Map<String, dynamic> json) {
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locId = json['LocId'];
    departmentId = json['DepartmentId'];
    projectId = json['ProjectId'];
    gRNTypeId = json['GRNTypeId'];
    gRNTypeRefNo = json['GRNTypeRefNo'];
    gRNDateTemp = json['GRNDateTemp'];
    docDateTemp = json['DocDateTemp'];
    suppId = json['SuppId'];
    isTaxApp = json['IsTaxApp'];
    currencyId = json['CurrencyId'];
    discPercentage = json['DiscPercentage'];
    remarks = json['Remarks'];
    isQualityChecked = json['IsQualityChecked'];
    isCashPurchase = json['IsCashPurchase'];
    isInvoiceRcvd = json['IsInvoiceRcvd'];
    invoiceNo = json['InvoiceNo'];
    deliveryNoteNo = json['DeliveryNoteNo'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['FOCDetails'] != null) {
      fOCDetails = new List<FOCDetails>();
      json['FOCDetails'].forEach((v) {
        fOCDetails.add(new FOCDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
    if (json['OtherCostDetails'] != null) {
      otherCostDetails = new List<OtherCostDetails>();
      json['OtherCostDetails'].forEach((v) {
        otherCostDetails.add(new OtherCostDetails.fromJson(v));
      });
    }
    if (json['OtherCostTaxDetails'] != null) {
      otherCostTaxDetails = new List<OtherCostTaxDetails>();
      json['OtherCostTaxDetails'].forEach((v) {
        otherCostTaxDetails.add(new OtherCostTaxDetails.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {

    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocId'] != null) {
      locId = json['LocId'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['GRNTypeId'] != null) {
      gRNTypeId = json['GRNTypeId'];
    }
    if (json['GRNTypeRefNo'] != null) {
      gRNTypeRefNo = json['GRNTypeRefNo'];
    }
    if (json['GRNDateTemp'] != null) {
      gRNDateTemp = json['GRNDateTemp'];
    }
    if (json['DocDateTemp'] != null) {
      docDateTemp = json['DocDateTemp'];
    }
    if (json['SuppId'] != null) {
      suppId = json['SuppId'];
    }
    if (json['IsTaxApp'] != null) {
      isTaxApp = json['IsTaxApp'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['DiscPercentage'] != null) {
      discPercentage = json['DiscPercentage'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['IsQualityChecked'] != null) {
      isQualityChecked = json['IsQualityChecked'];
    }
    if (json['IsCashPurchase'] != null) {
      isCashPurchase = json['IsCashPurchase'];
    }
    if (json['IsInvoiceRcvd'] != null) {
      isInvoiceRcvd = json['IsInvoiceRcvd'];
    }
    if (json['InvoiceNo'] != null) {
      invoiceNo = json['InvoiceNo'];
    }
    if (json['DeliveryNoteNo'] != null) {
      deliveryNoteNo = json['DeliveryNoteNo'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['FOCDetails'] != null) {
      fOCDetails = new List<FOCDetails>();
      json['FOCDetails'].forEach((v) {
        fOCDetails.add(new FOCDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
    if (json['OtherCostDetails'] != null) {
      otherCostDetails = new List<OtherCostDetails>();
      json['OtherCostDetails'].forEach((v) {
        otherCostDetails.add(new OtherCostDetails.fromJson(v));
      });
    }
    if (json['OtherCostTaxDetails'] != null) {
      otherCostTaxDetails = new List<OtherCostTaxDetails>();
      json['OtherCostTaxDetails'].forEach((v) {
        otherCostTaxDetails.add(new OtherCostTaxDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocId'] = this.locId;
    data['DepartmentId'] = this.departmentId;
    data['ProjectId'] = this.projectId;
    data['GRNTypeId'] = this.gRNTypeId;
    data['GRNTypeRefNo'] = this.gRNTypeRefNo;
    data['GRNDateTemp'] = this.gRNDateTemp;
    data['DocDateTemp'] = this.docDateTemp;
    data['SuppId'] = this.suppId;
    data['IsTaxApp'] = this.isTaxApp;
    data['CurrencyId'] = this.currencyId;
    data['DiscPercentage'] = this.discPercentage;
    data['Remarks'] = this.remarks;
    data['IsQualityChecked'] = this.isQualityChecked;
    data['IsCashPurchase'] = this.isCashPurchase;
    data['IsInvoiceRcvd'] = this.isInvoiceRcvd;
    data['InvoiceNo'] = this.invoiceNo;
    data['DeliveryNoteNo'] = this.deliveryNoteNo;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.fOCDetails != null) {
      data['FOCDetails'] = this.fOCDetails.map((v) => v.toJson()).toList();
    }
    if (this.serialNoDetails != null) {
      data['SerialNoDetails'] =
          this.serialNoDetails.map((v) => v.toJson()).toList();
    }
    if (this.otherCostDetails != null) {
      data['OtherCostDetails'] =
          this.otherCostDetails.map((v) => v.toJson()).toList();
    }
    if (this.otherCostTaxDetails != null) {
      data['OtherCostTaxDetails'] =
          this.otherCostTaxDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    suppTypeId = null;
    companyId = null;
    divisionId = null;
    locId = null;
    departmentId = null;
    projectId = null;
    gRNTypeId = null;
    gRNTypeRefNo = null;
    gRNDateTemp = null;
    docDateTemp = null;
    suppId = null;
    isTaxApp = null;
    currencyId = null;
    discPercentage = null;
    remarks = null;
    isQualityChecked = null;
    isCashPurchase = null;
    isInvoiceRcvd = null;
    invoiceNo = null;
    deliveryNoteNo = null;
    itemDetails = null;
    fOCDetails = null;
    serialNoDetails = null;
    otherCostDetails = null;
    otherCostTaxDetails = null;
    isSupplierInternal = null;
  }
}