class OtherCostDetails {
  int serviceLinNum;
  int serviceId;
  String serviceCode;
  int serviceCategoryId;
  int supplierId;
  int serviceAmount;
  int unitQty;
  int discPerc;
  bool isInvRcvd;
  int currencyId;
  String otherCostInvoiceNo;
  String otherCostInvoiceDateTemp;

  OtherCostDetails(
      {this.serviceLinNum,
        this.serviceId,
        this.serviceCode,
        this.serviceCategoryId,
        this.supplierId,
        this.serviceAmount,
        this.unitQty,
        this.discPerc,
        this.isInvRcvd,
        this.currencyId,
        this.otherCostInvoiceNo,
        this.otherCostInvoiceDateTemp});

  OtherCostDetails.fromJson(Map<String, dynamic> json) {
    serviceLinNum = json['ServiceLinNum'];
    serviceId = json['ServiceId'];
    serviceCode = json['ServiceCode'];
    serviceCategoryId = json['ServiceCategoryId'];
    supplierId = json['SupplierId'];
    serviceAmount = json['ServiceAmount'];
    unitQty = json['UnitQty'];
    discPerc = json['DiscPerc'];
    isInvRcvd = json['IsInvRcvd'];
    currencyId = json['CurrencyId'];
    otherCostInvoiceNo = json['OtherCostInvoiceNo'];
    otherCostInvoiceDateTemp = json['OtherCostInvoiceDateTemp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ServiceLinNum'] = this.serviceLinNum;
    data['ServiceId'] = this.serviceId;
    data['ServiceCode'] = this.serviceCode;
    data['ServiceCategoryId'] = this.serviceCategoryId;
    data['SupplierId'] = this.supplierId;
    data['ServiceAmount'] = this.serviceAmount;
    data['UnitQty'] = this.unitQty;
    data['DiscPerc'] = this.discPerc;
    data['IsInvRcvd'] = this.isInvRcvd;
    data['CurrencyId'] = this.currencyId;
    data['OtherCostInvoiceNo'] = this.otherCostInvoiceNo;
    data['OtherCostInvoiceDateTemp'] = this.otherCostInvoiceDateTemp;
    return data;
  }
}
//
//class TaxDetails {
//  int taxId;
//  int serviceId;
//  int serviceLinNum;
//  int taxSlabId;
//  String taxPercent;
//  String taxAmount;
//
//  TaxDetails(
//      {this.taxId,
//        this.serviceId,
//        this.serviceLinNum,
//        this.taxSlabId,
//        this.taxPercent,
//        this.taxAmount});
//
//  TaxDetails.fromJson(Map<String, dynamic> json) {
//    taxId = json['TaxId'];
//    serviceId = json['ServiceId'];
//    serviceLinNum = json['ServiceLinNum'];
//    taxSlabId = json['TaxSlabId'];
//    taxPercent = json['TaxPercent'];
//    taxAmount = json['TaxAmount'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['TaxId'] = this.taxId;
//    data['ServiceId'] = this.serviceId;
//    data['ServiceLinNum'] = this.serviceLinNum;
//    data['TaxSlabId'] = this.taxSlabId;
//    data['TaxPercent'] = this.taxPercent;
//    data['TaxAmount'] = this.taxAmount;
//    return data;
//  }
//}