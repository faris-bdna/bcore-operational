
class ItemDetails {
  String index;
  String batchNo;
  double discPercent;
  double exciseDuty;
  String expiryDateTemp;
  bool isFoc;
  bool isInstallationChecked;
  bool isItemQualityChecked;
  String itemCode;
  int itemId;
  int lineNum;
  int packingId;
  String productionDateTemp;
  int projectId;
  double unitPrice;
  double unitQty;
  int warehouseId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int wHouseZoneId;
  String itemName;
  String itemBarcode;
  double totalCost;

  List<String> serialNoList = [];

  ItemDetails(
      {this.batchNo,
        this.discPercent,
        this.exciseDuty,
        this.expiryDateTemp,
        this.isFoc,
        this.isInstallationChecked,
        this.isItemQualityChecked,
        this.itemCode,
        this.itemId,
        this.lineNum,
        this.packingId,
        this.productionDateTemp,
        this.projectId,
        this.unitPrice,
        this.unitQty,
        this.warehouseId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.wHouseZoneId,
        this.index,
        this.itemBarcode,
        this.serialNoList
      });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['Index'] != null) {
      index = json['Index'];
    }
    batchNo = json['BatchNo'];
    discPercent = json['DiscPercent'];
    exciseDuty = json['ExciseDuty'];
    expiryDateTemp = json['ExpiryDateTemp'];
    isFoc = json['IsFoc'];
    isInstallationChecked = json['IsInstallationChecked'];
    isItemQualityChecked = json['IsItemQualityChecked'];
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    lineNum = json['LineNum'];
    packingId = json['PackingId'];
    productionDateTemp = json['ProductionDateTemp'];
    projectId = json['ProjectId'];
    unitPrice = json['UnitPrice'];
    unitQty = json['UnitQty'];
    warehouseId = json['WarehouseId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    wHouseZoneId = json['WHouseZoneId'];

    if (json['SerialNoList'] != null) {
      serialNoList = json['SerialNoList'];
    }
    // itemBarcode = json['ItemBarcode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BatchNo'] = this.batchNo;
    data['DiscPercent'] = this.discPercent;
    data['ExciseDuty'] = this.exciseDuty;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['IsFoc'] = this.isFoc;
    data['IsInstallationChecked'] = this.isInstallationChecked;
    data['IsItemQualityChecked'] = this.isItemQualityChecked;
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['LineNum'] = this.lineNum;
    data['PackingId'] = this.packingId;
    data['ProductionDateTemp'] = this.productionDateTemp;
    data['ProjectId'] = this.projectId;
    data['UnitPrice'] = this.unitPrice;
    data['UnitQty'] = this.unitQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['WHouseZoneId'] = this.wHouseZoneId;

    // data['SerialNoList'] = this.serialNoList;
    // data['ItemBarcode'] = this.itemBarcode;

    return data;
  }
}

//final List <ItemDetails> grn_item_details = List<ItemDetails>();