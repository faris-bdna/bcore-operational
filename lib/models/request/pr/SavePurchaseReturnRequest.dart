class SavePRReq {

  static final SavePRReq _prReq = new SavePRReq._internal();
  factory SavePRReq() => _prReq;
  SavePRReq._internal();
  static SavePRReq get shared => _prReq;


  int companyId;
  int divisionId;
  int locId;
  int departmentId;
  int projectId;
  String entryDateTemp;
  int pRetTypeId;
  int pRetTypeRefNo;
  int priceTypeId;
  int suppId;
  String salesMan;
  int currencyId;
  String remarks;
  int reasonId;
  List<PRItemDetails> itemDetails;
  List<PRSerialNoDetails> serialNoDetails;

  String dateFormat;

  SavePRReq.fromJson(Map<String, dynamic> json) {
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locId = json['LocId'];
    departmentId = json['DepartmentId'];
    projectId = json['ProjectId'];
    entryDateTemp = json['EntryDateTemp'];
    pRetTypeId = json['PRetTypeId'];
    pRetTypeRefNo = json['PRetTypeRefNo'];
    priceTypeId = json['PriceTypeId'];
    suppId = json['SuppId'];
    salesMan = json['SalesMan'];
    currencyId = json['CurrencyId'];
    remarks = json['Remarks'];
    reasonId = json['ReasonId'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<PRItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new PRItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<PRSerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new PRSerialNoDetails.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocId'] != null) {
      locId = json['LocId'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['PRetTypeId'] != null) {
      pRetTypeId = json['PRetTypeId'];
    }
    if (json['PRetTypeRefNo'] != null) {
      pRetTypeRefNo = json['PRetTypeRefNo'];
    }
    if (json['PriceTypeId'] != null) {
      priceTypeId = json['PriceTypeId'];
    }
    if (json['SuppId'] != null) {
      suppId = json['SuppId'];
    }
    if (json['SalesMan'] != null) {
      salesMan = json['SalesMan'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ReasonId'] != null) {
      reasonId = json['ReasonId'];
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<PRItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new PRItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<PRSerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new PRSerialNoDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocId'] = this.locId;
    data['DepartmentId'] = this.departmentId;
    data['ProjectId'] = this.projectId;
    data['EntryDateTemp'] = this.entryDateTemp;
    data['PRetTypeId'] = this.pRetTypeId;
    data['PRetTypeRefNo'] = this.pRetTypeRefNo;
    data['PriceTypeId'] = this.priceTypeId;
    data['SuppId'] = this.suppId;
    data['SalesMan'] = this.salesMan;
    data['CurrencyId'] = this.currencyId;
    data['Remarks'] = this.remarks;
    data['ReasonId'] = this.reasonId;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.serialNoDetails != null) {
      data['SerialNoDetails'] =
          this.serialNoDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    companyId = null;
    divisionId = null;
    locId = null;
    departmentId = null;
    projectId = null;
    suppId = null;
    currencyId = null;
    remarks = null;
    itemDetails = null;
    serialNoDetails = null;
    entryDateTemp = null;
    pRetTypeId= null;
    pRetTypeRefNo = null;
    priceTypeId = null;
    salesMan = null;
    reasonId = null;
  }
}

class PRItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  String batchNo;
  int gRNHeaderId;
  double gRNQty;
  double returnedQty;
  double unitPrice;
  int reasonId;
  String returnComments;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  String itemBarcode;
  String itemName;

  PRItemDetails(
      {this.lineNum,
        this.itemId,
        this.itemCode,
        this.packingId,
        this.batchNo,
        this.gRNHeaderId,
        this.gRNQty,
        this.returnedQty,
        this.unitPrice,
        this.reasonId,
        this.returnComments,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId});

  PRItemDetails.fromJson(Map<String, dynamic> json) {
    lineNum = json['LineNum'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    packingId = json['PackingId'];
    batchNo = json['BatchNo'];
    gRNHeaderId = json['GRNHeaderId'];
    gRNQty = json['GRNQty'];
    returnedQty = json['ReturnedQty'];
    unitPrice = json['UnitPrice'];
    reasonId = json['ReasonId'];
    returnComments = json['ReturnComments'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['BatchNo'] = this.batchNo;
    data['GRNHeaderId'] = this.gRNHeaderId;
    data['GRNQty'] = this.gRNQty;
    data['ReturnedQty'] = this.returnedQty;
    data['UnitPrice'] = this.unitPrice;
    data['ReasonId'] = this.reasonId;
    data['ReturnComments'] = this.returnComments;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    return data;
  }
}

class PRSerialNoDetails {
  int dtLineNum;
  int serialLineNum;
  int gRNSerialNoId;
  int gRNHeaderId;
  int gRNDetailId;
  String serialNo;

  PRSerialNoDetails(
      {this.dtLineNum,
        this.serialLineNum,
        this.gRNSerialNoId,
        this.gRNHeaderId,
        this.gRNDetailId,
        this.serialNo});

  PRSerialNoDetails.fromJson(Map<String, dynamic> json) {
    dtLineNum = json['DtLineNum'];
    serialLineNum = json['SerialLineNum'];
    gRNSerialNoId = json['GRNSerialNoId'];
    gRNHeaderId = json['GRNHeaderId'];
    gRNDetailId = json['GRNDetailId'];
    serialNo = json['SerialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DtLineNum'] = this.dtLineNum;
    data['SerialLineNum'] = this.serialLineNum;
    data['GRNSerialNoId'] = this.gRNSerialNoId;
    data['GRNHeaderId'] = this.gRNHeaderId;
    data['GRNDetailId'] = this.gRNDetailId;
    data['SerialNo'] = this.serialNo;
    return data;
  }
}
