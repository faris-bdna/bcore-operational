class SRReq {

  static final SRReq _srReq = new SRReq._internal();
  factory SRReq() => _srReq;
  SRReq._internal();
  static SRReq get shared => _srReq;


  int storeIssueId;
  String remarks;
  String entryDateTemp;
  int toCompanyId;
  List<ItemDetails> itemDetails;
  List<SerialNoDetails> serialNoDetails;

  String dateFormat;

  SRReq.fromJson(Map<String, dynamic> json) {
    storeIssueId = json['StoreIssueId'];
    remarks = json['Remarks'];
    entryDateTemp = json['EntryDateTemp'];
    toCompanyId = json['ToCompanyId'];
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['StoreIssueId'] = this.storeIssueId;
    data['Remarks'] = this.remarks;
    data['EntryDateTemp'] = this.entryDateTemp;
    data['ToCompanyId'] = this.toCompanyId;
    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.serialNoDetails != null) {
      data['SerialNoDetails'] =
          this.serialNoDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['StoreIssueId'] != null) {
      storeIssueId = json['StoreIssueId'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ToTransactionDate'] != null) {
      entryDateTemp = json['ToTransactionDate'];
    }
    if (json['ToCompanyId'] != null) {
      toCompanyId = int.parse(json['ToCompanyId']);
    }
    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
  }

  clear() {
    storeIssueId = remarks = entryDateTemp = toCompanyId = null;
    if (itemDetails != null){
      itemDetails.clear();
    }
    if(serialNoDetails != null){
      serialNoDetails.clear();
    }
  }
}

class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  int storeIssueDetailId;
  int storeIssueId;
  double unitQty;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  String batchNo;
  String productionDateTemp;
  String expiryDateTemp;
  String remarks;
  bool isInstallationRequired;

  ItemDetails(
      {this.lineNum,
        this.itemId,
        this.itemCode,
        this.packingId,
        this.storeIssueDetailId,
        this.storeIssueId,
        this.unitQty,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.batchNo,
        this.productionDateTemp,
        this.expiryDateTemp,
        this.remarks,
        this.isInstallationRequired});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    lineNum = json['LineNum'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    packingId = json['ProdpkgId'];
    storeIssueDetailId = json['StoreIssueDetailId'];
    storeIssueId = json['StoreIssueId'];
    unitQty = json['ReqQty'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    batchNo = json['BatchNo'];
    productionDateTemp = json['ProductionDateTemp'];
    expiryDateTemp = json['ExpiryDate'];
    remarks = json['Remarks'];
    isInstallationRequired = json['IsInstallationChecked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['StoreIssueDetailId'] = this.storeIssueDetailId;
    data['StoreIssueId'] = this.storeIssueId;
    data['UnitQty'] = this.unitQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['BatchNo'] = this.batchNo;
    data['ProductionDateTemp'] = this.productionDateTemp;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['Remarks'] = this.remarks;
    data['IsInstallationRequired'] = this.isInstallationRequired;
    return data;
  }
}

class SerialNoDetails {
  int dtLineNum;
  String serialNo;
  int itemId;
  int serialLineNum;
  int storeIssueDetailId;
  int storeIssueId;
  int storeIssueSerialNoId;

  SerialNoDetails(
      {this.dtLineNum,
        this.serialNo,
        this.itemId,
        this.serialLineNum,
        this.storeIssueDetailId,
        this.storeIssueId,
        this.storeIssueSerialNoId});

  SerialNoDetails.fromJson(Map<String, dynamic> json) {
    dtLineNum = json['DtLineNum'];
    serialNo = json['SerialNo'];
    itemId = json['ItemId'];
    serialLineNum = json['SerialLineNum'];
    storeIssueDetailId = json['StoreIssueDetailId'];
    storeIssueId = json['StoreIssueId'];
    storeIssueSerialNoId = json['StoreIssueSerialNoId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DtLineNum'] = this.dtLineNum;
    data['SerialNo'] = this.serialNo;
    data['ItemId'] = this.itemId;
    data['SerialLineNum'] = this.serialLineNum;
    data['StoreIssueDetailId'] = this.storeIssueDetailId;
    data['StoreIssueId'] = this.storeIssueId;
    data['StoreIssueSerialNoId'] = this.storeIssueSerialNoId;
    return data;
  }
}
