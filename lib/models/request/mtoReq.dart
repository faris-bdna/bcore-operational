import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';

class MTOReq {


  static final MTOReq _mtoReq = new MTOReq._internal();
  factory MTOReq() => _mtoReq;
  MTOReq._internal();
  static MTOReq get shared => _mtoReq;


  int fromCompanyId;
  int fromDivisionId;
  int fromLocId;
  int fromDepartmentId;
  int fromProjectId;

  int toCompanyId;
  int toDivisionId;
  int toLocId;
  int toDepartmentId;
  int toProjectId;

  String remarks;
  String entryDateTemp;

  int tranTypeId;
  int tranTypeSrcId;

  List<ItemDetails> itemDetails;
  List<BatchDetails> batchDetails;
  List<SubstituteItemDetails> substituteItemDetails = [];


  MTOReq.fromJson(Map<String, dynamic> json) {
    fromCompanyId = json['FromCompanyId'];
    fromDivisionId = json['FromDivisionId'];
    fromLocId = json['FromLocId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromProjectId = json['FromProjectId'];

    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    toDepartmentId = json['ToDepartmentId'];
    toProjectId = json['ToProjectId'];

    remarks = json['Remarks'];
    entryDateTemp = json['EntryDateTemp'];

    tranTypeId = json['TranTypeId'];
    tranTypeSrcId = json['TranTypeSrcId'];

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['BatchDetails'] != null) {
      batchDetails = new List<BatchDetails>();
      json['BatchDetails'].forEach((v) {
        batchDetails.add(new BatchDetails.fromJson(v));
      });
    }
    if (json['SubstituteItemDetails'] != null) {
      substituteItemDetails = new List<SubstituteItemDetails>();
      json['SubstituteItemDetails'].forEach((v) {
        substituteItemDetails.add(new SubstituteItemDetails.fromJson(v));
      });
    }
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['FromCompanyId'] != null) {
      fromCompanyId = int.parse(json['FromCompanyId']);
    }
    if (json['FromDivisionId'] != null) {
      fromDivisionId = int.parse(json['FromDivisionId']);
    }
    if (json['FromLocationId'] != null) {
      fromLocId = int.parse(json['FromLocationId']);
    }
    if (json['FromDepartmentId'] != null) {
      fromDepartmentId = int.parse(json['FromDepartmentId']);
    }
    if (json['FromProjectId'] != null) {
      fromProjectId = int.parse(json['FromProjectId']);
    }

    if (json['ToCompanyId'] != null) {
      toCompanyId = int.parse(json['ToCompanyId']);
    }
    if (json['ToDivisionId'] != null) {
      toDivisionId = int.parse(json['ToDivisionId']);
    }
    if (json['ToLocationId'] != null) {
      toLocId = int.parse(json['ToLocationId']);
    }
    if (json['ToDepartmentId'] != null) {
      toDepartmentId = int.parse(json['ToDepartmentId']);
    }
    if (json['ToProjectId'] != null) {
      toProjectId = int.parse(json['ToProjectId']);
    }

    if (json['TransferTypeId'] != null) {
      tranTypeId = json['TransferTypeId'];
    }
    if (json['StoreIssueId'] != null) {
      tranTypeSrcId = json['StoreIssueId'];
    }

    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ToTransactionDate'] != null) {
      entryDateTemp = json['ToTransactionDate'];
    }

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['BatchDetails'] != null) {
      batchDetails = json['BatchDetails'];
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FromCompanyId'] = this.fromCompanyId;
    data['FromDivisionId'] = this.fromDivisionId;
    data['FromLocId'] = this.fromLocId;
    data['FromDepartmentId'] = this.fromDepartmentId;
    data['FromProjectId'] = this.fromProjectId;

    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['ToDepartmentId'] = this.toDepartmentId;
    data['ToProjectId'] = this.toProjectId;

    data['Remarks'] = this.remarks;
    data['EntryDateTemp'] = this.entryDateTemp;

    data['TranTypeSrcId'] = this.tranTypeSrcId;
    data['TranTypeId'] = this.tranTypeId;

    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.batchDetails != null) {
      data['BatchDetails'] = this.batchDetails.map((v) => v.toJson()).toList();
    }
    if (this.substituteItemDetails != null) {
      data['SubstituteItemDetails'] =
          this.substituteItemDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }


  clear() {
    entryDateTemp = fromCompanyId = fromDivisionId = null;
    fromDepartmentId = fromProjectId = fromLocId = null;
    toCompanyId = toDivisionId = toLocId = null;
    toDepartmentId = toProjectId = remarks = null;
    itemDetails = batchDetails = substituteItemDetails = null;
    tranTypeId = tranTypeSrcId = null;

  }
}

class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  double unitQty;

  String itemBarcode;
  int referenceDtId;

  ItemDetails(
      {this.lineNum, this.itemId, this.itemCode, this.packingId, this.unitQty,
        this.itemBarcode, this.referenceDtId });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['ProdpkgId'] != null) {
      packingId = json['ProdpkgId'];
    }
    if (json['ReqQty'] != null) {
      unitQty = json['ReqQty'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'];
    }
    if (json['ReferenceDtId'] != null) {
      referenceDtId = json['ReferenceDtId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['UnitQty'] = this.unitQty;
    data['ItemBarcode'] = this.itemBarcode;
    data['ReferenceDtId'] = this.referenceDtId;
    return data;
  }
}

class BatchDetails {
  String itemCode;
  int itemId;
  int packId;
  int batchLineNum;
  int dtLineNum;
  int isManualSelect;
  String batchNo;
  int batchWiseQty;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;

  BatchDetails(
      {this.itemCode,
        this.itemId,
        this.packId,
        this.batchLineNum,
        this.dtLineNum,
        this.isManualSelect,
        this.batchNo,
        this.batchWiseQty,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId});

  BatchDetails.fromJson(Map<String, dynamic> json) {
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    packId = json['PackId'];
    batchLineNum = json['BatchLineNum'];
    dtLineNum = json['DtLineNum'];
    isManualSelect = json['IsManualSelect'];
    batchNo = json['BatchNo'];
    batchWiseQty = json['BatchWiseQty'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PackId'] = this.packId;
    data['BatchLineNum'] = this.batchLineNum;
    data['DtLineNum'] = this.dtLineNum;
    data['IsManualSelect'] = this.isManualSelect;
    data['BatchNo'] = this.batchNo;
    data['BatchWiseQty'] = this.batchWiseQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    return data;
  }
}
