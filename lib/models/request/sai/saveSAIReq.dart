class SaveSAIReq {

  static final SaveSAIReq _saveSaiReq = new SaveSAIReq._internal();
  factory SaveSAIReq() => _saveSaiReq;
  SaveSAIReq._internal();
  static SaveSAIReq get shared => _saveSaiReq;

  int projectID;
  String orderRemarks;
  int companyID;
  int divisionID;
  int locationID;
  String entryDate;
  int departmentID;
  int salesmanId;
  double discount;
  bool sameAsBillingAdd;
  double netAmount;
  int billingPartyType;
  int billingPartyID;
  int appStatus;
  List<MaterialDetailsListTemp> materialDetailsListTemp;
  List<AdditionalChargesDetailsList> additionalChargesDetailsList;
  List<TermsAndCondionList> termsAndCondionList;
  List<SalesInvoiceReceiptList> salesInvoiceReceiptList;

  SaveSAIReq.fromJson(Map<String, dynamic> json) {
    projectID = json['ProjectID'];
    orderRemarks = json['OrderRemarks'];
    companyID = json['CompanyID'];
    divisionID = json['DivisionID'];
    locationID = json['LocationID'];
    entryDate = json['EntryDate'];
    departmentID = json['DepartmentID'];
    salesmanId = json['SalesmanId'];
    discount = json['Discount'];
    sameAsBillingAdd = json['SameAsBillingAdd'];
    netAmount = json['NetAmount'];
    billingPartyType = json['BillingPartyType'];
    billingPartyID = json['BillingPartyID'];
    appStatus = json['AppStatus'];
    if (json['MaterialDetailsListTemp'] != null) {
      materialDetailsListTemp = new List<MaterialDetailsListTemp>();
      json['MaterialDetailsListTemp'].forEach((v) {
        materialDetailsListTemp.add(new MaterialDetailsListTemp.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['SalesInvoiceReceiptList'] != null) {
      salesInvoiceReceiptList = new List<SalesInvoiceReceiptList>();
      json['SalesInvoiceReceiptList'].forEach((v) {
        salesInvoiceReceiptList.add(new SalesInvoiceReceiptList.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['FromProjectId'] != null) {
      projectID = int.parse(json['FromProjectId']);
    }
    if (json['OrderRemarks'] != null) {
      orderRemarks = json['OrderRemarks'];
    }
    if (json['FromCompanyId'] != null) {
      companyID = int.parse(json['FromCompanyId']);
    }
    if (json['FromDivisionId'] != null) {
      divisionID = int.parse(json['FromDivisionId']);
    }
    if (json['FromLocationId'] != null) {
      locationID = int.parse(json['FromLocationId']);
    }
    if (json['EntryDate'] != null) {
      entryDate = json['EntryDate'];
    }
    if (json['FromDepartmentId'] != null) {
      departmentID = int.parse(json['FromDepartmentId']);
    }
    if (json['SalesmanId'] != null) {
      salesmanId = json['SalesmanId'];
    }
    if (json['Discount'] != null) {
      discount = json['Discount'];
    }
    if (json['SameAsBillingAdd'] != null) {
      sameAsBillingAdd = json['SameAsBillingAdd'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['BillingPartyType'] != null) {
      billingPartyType = json['BillingPartyType'];
    }
    if (json['BillingPartyID'] != null) {
      billingPartyID = json['BillingPartyID'];
    }
    if (json['AppStatus'] != null) {
      appStatus = json['AppStatus'];
    }
    if (json['ItemDetails'] != null) {
      materialDetailsListTemp = new List<MaterialDetailsListTemp>();
      json['ItemDetails'].forEach((v) {
        materialDetailsListTemp.add(new MaterialDetailsListTemp.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['SalesInvoiceReceiptList'] != null) {
      salesInvoiceReceiptList = new List<SalesInvoiceReceiptList>();
      json['SalesInvoiceReceiptList'].forEach((v) {
        salesInvoiceReceiptList.add(new SalesInvoiceReceiptList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProjectID'] = this.projectID;
    data['OrderRemarks'] = this.orderRemarks;
    data['CompanyID'] = this.companyID;
    data['DivisionID'] = this.divisionID;
    data['LocationID'] = this.locationID;
    data['EntryDateTemp'] = this.entryDate;
    data['DepartmentID'] = this.departmentID;
    data['SalesmanId'] = this.salesmanId;
    data['Discount'] = this.discount;
    data['SameAsBillingAdd'] = this.sameAsBillingAdd;
    data['NetAmount'] = this.netAmount;
    data['BillingPartyType'] = this.billingPartyType;
    data['BillingPartyID'] = this.billingPartyID;
    data['AppStatus'] = this.appStatus;
    if (this.materialDetailsListTemp != null) {
      data['MaterialDetailsListTemp'] =
          this.materialDetailsListTemp.map((v) => v.toJson()).toList();
    }
    if (this.additionalChargesDetailsList != null) {
      data['AdditionalChargesDetailsList'] =
          this.additionalChargesDetailsList.map((v) => v.toJson()).toList();
    }
    if (this.termsAndCondionList != null) {
      data['TermsAndCondionList'] =
          this.termsAndCondionList.map((v) => v.toJson()).toList();
    }
    if (this.salesInvoiceReceiptList != null) {
      data['SalesInvoiceReceiptList'] =
          this.salesInvoiceReceiptList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear(){
    projectID = orderRemarks = companyID = divisionID = locationID = null;
    entryDate = departmentID = salesmanId = discount = sameAsBillingAdd = null;
    netAmount = billingPartyType = billingPartyID = appStatus = null;
    materialDetailsListTemp = additionalChargesDetailsList = null;
    termsAndCondionList = salesInvoiceReceiptList = null;
  }
}

class MaterialDetailsListTemp {
  int itemId;
  int unitId;
  double invoiceQty;
  double rate;
  double discount;
  double discountPercentage;
  int itemBarcodeId;
  bool isFromBackEnd;
  int itemLineNo;
  int productStockTypeId;
  String itemBarcode;
  double fOCQuantity = 0.0;
  List<SalesInvoiceSerialNoList> salesInvoiceSerialNoList;

  MaterialDetailsListTemp(
      {this.itemId,
        this.unitId,
        this.invoiceQty,
        this.rate,
        this.discount,
        this.discountPercentage,
        this.itemBarcodeId,
        this.isFromBackEnd,
        this.itemLineNo,
        this.productStockTypeId,
        this.itemBarcode,
        this.fOCQuantity,
        this.salesInvoiceSerialNoList});

  MaterialDetailsListTemp.fromJson(Map<String, dynamic> json) {
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      invoiceQty = json['Quantity'];
    }
    if (json['Rate'] != null) {
      rate = json['Rate'];
    }
    if (json['Discount'] != null) {
      discount = json['Discount'];
    }
    if (json['DiscountPercentage'] != null) {
      discountPercentage = json['DiscountPercentage'];
    }
    if (json['ItemBarcodeId'] != null) {
      itemBarcodeId = json['ItemBarcodeId'];
    }
    if (json['IsFromBackEnd'] != null) {
      isFromBackEnd = json['IsFromBackEnd'];
    }
    if (json['ItemLineNo'] != null) {
      itemLineNo = json['ItemLineNo'];
    }
    if (json['ProductStockTypeId'] != null) {
      productStockTypeId = json['ProductStockTypeId'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'].toString();
    }
    if (json['FocQuantity'] != null) {
      fOCQuantity = json['FocQuantity'];
    }
    if (json['SalesInvoiceSerialNoList'] != null) {
      salesInvoiceSerialNoList = new List<SalesInvoiceSerialNoList>();
      json['SalesInvoiceSerialNoList'].forEach((v) {
        salesInvoiceSerialNoList.add(new SalesInvoiceSerialNoList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['UnitId'] = this.unitId;
    data['InvoiceQty'] = this.invoiceQty;
    data['Rate'] = this.rate;
    data['Discount'] = this.discount;
    data['DiscountPercentage'] = this.discountPercentage;
    data['ItemBarcodeId'] = this.itemBarcodeId;
    data['IsFromBackEnd'] = this.isFromBackEnd;
    data['ItemLineNo'] = this.itemLineNo;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['ItemBarcode'] = this.itemBarcode;
    data['FOCQuantity'] = this.fOCQuantity;
    if (this.salesInvoiceSerialNoList != null) {
      data['SalesInvoiceSerialNoList'] =
          this.salesInvoiceSerialNoList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SalesInvoiceSerialNoList {
  int gRNSerialNoId;
  String serialNo;

  SalesInvoiceSerialNoList({this.gRNSerialNoId, this.serialNo});

  SalesInvoiceSerialNoList.fromJson(Map<String, dynamic> json) {
    gRNSerialNoId = json['GRNSerialNoId'];
    serialNo = json['SerialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['GRNSerialNoId'] = this.gRNSerialNoId;
    data['SerialNo'] = this.serialNo;
    return data;
  }
}

class AdditionalChargesDetailsList {
  int itemId;
  int unitId;
  double quantity;
  double rate;
  double total;
  int taxId;
  int taxSlabId;
  double taxAmount;
  double netAmount;
  double taxPercent;
  String bOQDescription;

  AdditionalChargesDetailsList(
      {this.itemId,
        this.unitId,
        this.quantity,
        this.rate,
        this.total,
        this.taxId,
        this.taxSlabId,
        this.taxAmount,
        this.netAmount,
        this.taxPercent,
        this.bOQDescription});

  AdditionalChargesDetailsList.fromJson(Map<String, dynamic> json) {
    if (json['ProductId'] != null) {
      itemId = json['ProductId'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      quantity = json['Quantity'];
    }
    if (json['UnitPrice'] != null) {
      rate = json['UnitPrice'];
    }
    if (json['Total'] != null) {
      total = json['Total'];
    }
    if (json['TaxId'] != null) {
      taxId = json['TaxId'];
    }
    if (json['TaxSlabId'] != null) {
      taxSlabId = json['TaxSlabId'];
    }
    if (json['TaxAmount'] != null) {
      taxAmount = json['TaxAmount'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['TaxPercent'] != null) {
      taxPercent = json['TaxPercent'];
    }
    if (json['BOQDescription'] != null) {
      bOQDescription = json['BOQDescription'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemId'] = this.itemId;
    data['UnitId'] = this.unitId;
    data['Quantity'] = this.quantity;
    data['Rate'] = this.rate;
    data['Total'] = this.total;
    data['TaxId'] = this.taxId;
    data['TaxSlabId'] = this.taxSlabId;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['TaxPercent'] = this.taxPercent;
    data['BOQDescription'] = this.bOQDescription;
    return data;
  }
}

class TermsAndCondionList {
  int termsId;
  int sortOrder;
  int paymentTypeId = 0;
  double paymentValue = 0;
  int scopeTypeId = 0;

  TermsAndCondionList(
      {this.termsId,
        this.sortOrder,
        this.paymentTypeId,
        this.paymentValue,
        this.scopeTypeId});

  TermsAndCondionList.fromJson(Map<String, dynamic> json) {
    if (json['TermsId'] != null) {
      termsId = json['TermsId'];
    }
    if (json['SortOrder'] != null) {
      sortOrder = json['SortOrder'];
    }
    if (json['PaymentTypeId'] != null) {
      paymentTypeId = json['PaymentTypeId'];
    }
    if (json['PaymentValue'] != null) {
      paymentValue = json['PaymentValue'];
    }
    if (json['ScopeTypeId'] != null) {
      scopeTypeId = json['ScopeTypeId'];
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['TermsId'] != null) {
      termsId = json['TermsId'];
    }
    if (json['SortOrder'] != null) {
      sortOrder = json['SortOrder'];
    }
    if (json['PaymentTypeId'] != null) {
      paymentTypeId = json['PaymentTypeId'];
    }
    if (json['PaymentValue'] != null) {
      paymentValue = json['PaymentValue'];
    }
    if (json['ScopeTypeId'] != null) {
      scopeTypeId = json['ScopeTypeId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['TermsId'] = this.termsId;
    data['SortOrder'] = this.sortOrder;
    data['PaymentTypeId'] = this.paymentTypeId;
    data['PaymentValue'] = this.paymentValue;
    data['ScopeTypeId'] = this.scopeTypeId;
    return data;
  }
}

class SalesInvoiceReceiptList {
  int paymentMethodId;
  int paymentModeId;
  String chequedate;
  int referenceId = 0;
  String paymentModeComments;
  String chequeName;
  String chequeNo;
  double receiptAmount;
  String remarks;
  String receiptEntryDate;
  bool isReceipt = true;

  SalesInvoiceReceiptList(
      {this.paymentMethodId,
        this.paymentModeId,
        this.chequedate,
        this.referenceId,
        this.paymentModeComments,
        this.chequeName,
        this.chequeNo,
        this.receiptAmount,
        this.remarks,
        this.receiptEntryDate,
        this.isReceipt});

  SalesInvoiceReceiptList.fromJson(Map<String, dynamic> json) {
    if (json['PaymentTypeValue'] != null) {
      paymentMethodId = int.parse(json['PaymentTypeValue']);
    }
    if (json['PaymentSubTypeValue'] != null) {
      paymentModeId = int.parse(json['PaymentSubTypeValue']);
    }
    if (json['ChequeDate'] != null) {
      chequedate = json['ChequeDate'];
    }
    if (json['ReferenceId'] != null) {
      referenceId = json['ReferenceId'];
    }
    if (json['PaymentModeComments'] != null) {
      paymentModeComments = json['PaymentModeComments'];
    }
    if (json['ChequeName'] != null) {
      chequeName = json['ChequeName'];
    }
    if (json['ChequeNo'] != null) {
      chequeNo = json['ChequeNo'];
    }
    if (json['Amount'] != null) {
      receiptAmount = json['Amount'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ReceiptEntryDate'] != null) {
      receiptEntryDate = json['ReceiptEntryDate'];
    }
    if (json['IsReceipt'] != null) {
      isReceipt = json['IsReceipt'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['PaymentMethodId'] = this.paymentMethodId;
    data['PaymentModeId'] = this.paymentModeId;
    data['Chequedate'] = this.chequedate;
    data['ReferenceId'] = this.referenceId;
    data['PaymentModeComments'] = this.paymentModeComments;
    data['ChequeName'] = this.chequeName;
    data['ChequeNo'] = this.chequeNo;
    data['ReceiptAmount'] = this.receiptAmount;
    data['Remarks'] = this.remarks;
    data['ReceiptEntryDate'] = this.receiptEntryDate;
    data['IsReceipt'] = this.isReceipt;
    return data;
  }
}
