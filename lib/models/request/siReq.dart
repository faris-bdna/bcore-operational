import 'package:bcore_inventory_management/models/response/serialNoDetails.dart';
import 'package:bcore_inventory_management/models/view/item_detail.dart';

class SIReq {

  static final SIReq _siReq = new SIReq._internal();
  factory SIReq() => _siReq;
  SIReq._internal();
  static SIReq get shared => _siReq;

  int fromCompanyId;
  int fromDivisionId;
  int fromDepartmentId;
  int fromProjectId;
  int fromLocId;

  int toCompanyId;
  int toDivisionId;
  int toLocId;
  int toDepartmentId;
  int toProjectId;

  int issueTypeId;
  int issueSrcId;

  String dateFormat;
  String entryDateTemp;

  String remarks;

  List<ItemDetails> itemDetails;
  List<SubstituteItemDetails> substituteItemDetails = [];
  // List<BatchDetails> batchDetails;
  List<SerialNoDetails> serialNoDetails;


  SIReq.fromJson(Map<String, dynamic> json) {
    fromCompanyId = json['FromCompanyId'];
    fromDivisionId = json['FromDivisionId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromLocId = json['FromLocId'];
    fromProjectId = json['FromProjectId'];

    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    toDepartmentId = json['ToDepartmentId'];
    toProjectId = json['ToProjectId'];

    remarks = json['Remarks'];
    entryDateTemp = json['EntryDateTemp'];

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    if (json['SubstituteItemDetails'] != null) {
      substituteItemDetails = new List<SubstituteItemDetails>();
      json['SubstituteItemDetails'].forEach((v) {
        substituteItemDetails.add(new SubstituteItemDetails.fromJson(v));
      });
    }
 /*   if (json['BatchDetails'] != null) {
      batchDetails = new List<BatchDetails>();
      json['BatchDetails'].forEach((v) {
        batchDetails.add(new BatchDetails.fromJson(v));
      });
    }*/
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = new List<SerialNoDetails>();
      json['SerialNoDetails'].forEach((v) {
        serialNoDetails.add(new SerialNoDetails.fromJson(v));
      });
    }
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['FromCompanyId'] != null) {
      fromCompanyId = int.parse(json['FromCompanyId']);
    }
    if (json['FromDivisionId'] != null) {
      fromDivisionId = int.parse(json['FromDivisionId']);
    }
    if (json['FromLocationId'] != null) {
      fromLocId = int.parse(json['FromLocationId']);
    }
    if (json['FromDepartmentId'] != null) {
      fromDepartmentId = int.parse(json['FromDepartmentId']);
    }
    if (json['FromProjectId'] != null) {
      fromProjectId = int.parse(json['FromProjectId']);
    }

    if (json['ToCompanyId'] != null) {
      toCompanyId = int.parse(json['ToCompanyId']);
    }
    if (json['ToDivisionId'] != null) {
      toDivisionId = int.parse(json['ToDivisionId']);
    }
    if (json['ToLocationId'] != null) {
      toLocId = int.parse(json['ToLocationId']);
    }
    if (json['ToDepartmentId'] != null) {
      toDepartmentId = int.parse(json['ToDepartmentId']);
    }
    if (json['ToProjectId'] != null) {
      toProjectId = int.parse(json['ToProjectId']);
    }

    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['ToTransactionDate'] != null) {
      entryDateTemp = json['ToTransactionDate'];
    }

    if (json['TransferTypeId'] != null) {
      issueTypeId = json['TransferTypeId'];
    }
    if (json['StoreIssueId'] != null) {
      issueSrcId = json['StoreIssueId'];
    }

    if (json['ItemDetails'] != null) {
      itemDetails = new List<ItemDetails>();
      json['ItemDetails'].forEach((v) {
        itemDetails.add(new ItemDetails.fromJson(v));
      });
    }
    /*   if (json['BatchDetails'] != null) {
      batchDetails = json['BatchDetails'];
    }*/
    if (json['SubstituteItemDetails'] != null) {
      substituteItemDetails = new List<SubstituteItemDetails>();
      json['SubstituteItemDetails'].forEach((v) {
        substituteItemDetails.add(new SubstituteItemDetails.fromJson(v));
      });
    }
    if (json['SerialNoDetails'] != null) {
      serialNoDetails = json['SerialNoDetails'];
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['FromCompanyId'] = this.fromCompanyId;
    data['FromDivisionId'] = this.fromDivisionId;
    data['FromLocId'] = this.fromLocId;
    data['FromDepartmentId'] = this.fromDepartmentId;
    data['FromProjectId'] = this.fromProjectId;

    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['ToDepartmentId'] = this.toDepartmentId;
    data['ToProjectId'] = this.toProjectId;

    data['Remarks'] = this.remarks;
    data['EntryDateTemp'] = this.entryDateTemp;

    data['IssueSrcId'] = this.issueSrcId;
    data['IssueTypeId'] = this.issueTypeId;

    if (this.itemDetails != null) {
      data['ItemDetails'] = this.itemDetails.map((v) => v.toJson()).toList();
    }
    if (this.substituteItemDetails != null) {
      data['SubstituteItemDetails'] =
          this.substituteItemDetails.map((v) => v.toJson()).toList();
    }
 /*   if (this.batchDetails != null) {
      data['BatchDetails'] = this.batchDetails.map((v) => v.toJson()).toList();
    }*/
    if (this.serialNoDetails != null) {
      data['SerialNoDetails'] =
          this.serialNoDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    entryDateTemp = fromCompanyId = fromDivisionId = null;
    fromDepartmentId = fromProjectId = fromLocId = null;
    toCompanyId = toDivisionId = toLocId = null;
    toDepartmentId = toProjectId = remarks = null;
    itemDetails = substituteItemDetails = serialNoDetails = null;
    issueTypeId = issueSrcId = null;
  }
}

class ItemDetails {
  int lineNum;
  int itemId;
  String itemCode;
  int packingId;
  double unitQty;

  String itemBarcode;
  int referenceDtId;

  ItemDetails(
      {this.lineNum, this.itemId, this.itemCode, this.packingId, this.unitQty,
        this.itemBarcode, this.referenceDtId });

  ItemDetails.fromJson(Map<String, dynamic> json) {
    lineNum = json['LineNum'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    packingId = json['ProdpkgId'];
    unitQty = json['ReqQty'];
    itemBarcode = json['ItemBarcode'];
    referenceDtId = json['ReferenceDtId'];
  }

  fromViewJson(Map<String, dynamic> json) {
    if (json['LineNum'] != null) {
      lineNum = json['LineNum'];
    }
    if (json['ItemId'] != null) {
      itemId = json['ItemId'];
    }
    if (json['ItemCode'] != null) {
      itemCode = json['ItemCode'];
    }
    if (json['PackingId'] != null) {
      packingId = json['ProdpkgId'];
    }
    if (json['UnitQty'] != null) {
      unitQty = json['ReqQty'];
    }
    if (json['ItemBarcode'] != null) {
      itemBarcode = json['ItemBarcode'];
    }
    if (json['ReferenceDtId'] != null) {
      referenceDtId = json['ReferenceDtId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['PackingId'] = this.packingId;
    data['UnitQty'] = this.unitQty;
    data['ItemBarcode'] = this.itemBarcode;
    data['ReferenceDtId'] = this.referenceDtId;

    return data;
  }
}
/*
class BatchDetails {
  String itemCode;
  int itemId;
  int packId;
  int batchLineNum;
  int dtLineNum;
  int isManualSelect;
  String batchNo;
  int batchWiseQty;
  int warehouseId;
  int wHouseZoneId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;

  BatchDetails(
      {this.itemCode,
        this.itemId,
        this.packId,
        this.batchLineNum,
        this.dtLineNum,
        this.isManualSelect,
        this.batchNo,
        this.batchWiseQty,
        this.warehouseId,
        this.wHouseZoneId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId});

  BatchDetails.fromJson(Map<String, dynamic> json) {
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    packId = json['PackId'];
    batchLineNum = json['BatchLineNum'];
    dtLineNum = json['DtLineNum'];
    isManualSelect = json['IsManualSelect'];
    batchNo = json['BatchNo'];
    batchWiseQty = json['BatchWiseQty'];
    warehouseId = json['WarehouseId'];
    wHouseZoneId = json['WHouseZoneId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PackId'] = this.packId;
    data['BatchLineNum'] = this.batchLineNum;
    data['DtLineNum'] = this.dtLineNum;
    data['IsManualSelect'] = this.isManualSelect;
    data['BatchNo'] = this.batchNo;
    data['BatchWiseQty'] = this.batchWiseQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    return data;
  }
}*/

class SerialNoDetails {
  int dtLineNum;
  int gRNSerialNoId;
  String serialNo;
  int itemId;

  SerialNoDetails(
      {this.dtLineNum, this.gRNSerialNoId, this.serialNo, this.itemId});

  SerialNoDetails.fromJson(Map<String, dynamic> json) {
    dtLineNum = json['DtLineNum'];
    gRNSerialNoId = json['GRNSerialNoId'];
    serialNo = json['SerialNo'];
    itemId = json['ItemId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DtLineNum'] = this.dtLineNum;
    data['GRNSerialNoId'] = this.gRNSerialNoId;
    data['SerialNo'] = this.serialNo;
    data['ItemId'] = this.itemId;
    return data;
  }
}
