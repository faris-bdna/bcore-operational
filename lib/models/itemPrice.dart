import 'package:bcore_inventory_management/models/view/fromAddress.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';

class ItemPrice {
  double unitPrice = 0.0;
  double quantity = 0.0;
  double actualPrice = 0.0;
  double discPercentage = 0.0;
  double discount = 0.0;
  double exciseDuty = 0.0;

  double refUnitPrice = 0.0;

  double grossPrice = 0.0;
  double taxPercentage = 0.0;
  double taxAmount = 0.0;
  double netAmount = 0.0;
  bool needToAddTax;
  bool needToCalculateTaxToDisplay;

  double tempUnitPrice = 0.0;
  double tempExciseDuty = 0.0;
  double tempDiscount = 0.0;
  double tempDiscPercentage = 0.0;
  double tempTaxAmount = 0.0;

  bool isEditModeOn;
  TransactionType type;

  ItemPrice(
      {this.unitPrice,
      this.quantity,
      this.actualPrice,
      this.discPercentage,
      this.discount,
      this.grossPrice,
      this.taxPercentage,
      this.taxAmount,
      this.netAmount,
      this.needToAddTax,
      this.needToCalculateTaxToDisplay,
      this.tempTaxAmount,
      this.type});

  ItemPrice.fromJson(Map<String, dynamic> json) {
    unitPrice = json['UnitPrice'];
    quantity = json['Quantity'];
    actualPrice = json['ActualPrice'];
    discPercentage = json['discountPercentage'];
    discount = json['Discount'];
    grossPrice = json['GrossPrice'];
    taxPercentage = json['TaxPercentage'];
    taxAmount = json['Tax'];
    netAmount = json['NetAmount'];
    needToAddTax = json['NeedToAddTax'];
    needToCalculateTaxToDisplay = json['NeedToCalculateTaxToDisplay'];
  }

  reset() {
    unitPrice = 0.0;
    quantity = 0.0;
    actualPrice = 0.0;
    discPercentage = 0.0;
    discount = 0.0;
    grossPrice = 0.0;
    taxPercentage = 0.0;
    exciseDuty = 0.0;
    refUnitPrice = 0.0;
    taxAmount = 0.0;
    netAmount = 0.0;
    needToAddTax = false;
    needToCalculateTaxToDisplay = false;
    isEditModeOn = false;
    tempDiscount = 0.0;
    tempDiscPercentage = 0.0;
    tempUnitPrice = 0.0;
    tempExciseDuty = 0.0;
    tempTaxAmount = 0.0;
  }

  enableEditMode() {
    isEditModeOn = true;
    tempDiscount = discount;
    tempDiscPercentage = discPercentage;
    tempUnitPrice = unitPrice;
    tempExciseDuty = exciseDuty;
    tempTaxAmount = taxAmount;
  }

  updatePrices() {
    discount = tempDiscount;
    discPercentage = tempDiscPercentage;
    unitPrice = tempUnitPrice;
    exciseDuty = tempExciseDuty;
    taxAmount = tempTaxAmount;
  }

  disableEditMode() {
    isEditModeOn = false;
    tempDiscount = 0.0;
    tempDiscPercentage = 0.0;
    tempUnitPrice = 0.0;
    tempExciseDuty = 0.0;
    tempTaxAmount = 0.0;
  }

  int getDeci() {
    if (type == TransactionType.GRN) {
      return int.parse(fromAddress.curDecimPlace);
    } else {
      return 2;
    }
  }

  double getActualPrice() {
    if (isEditModeOn) {
      if (this.quantity == 0.0) {
        this.actualPrice = this.tempUnitPrice + this.tempExciseDuty;
//        this.actualPrice = (this.tempUnitPrice.toDouble(pos: getDeci()) +  this.tempExciseDuty.toDouble(pos: getDeci()) ?? "0.0").toString();
      } else {
        this.actualPrice = (this.tempUnitPrice * this.quantity) + (this.tempExciseDuty ?? 0.0);
//        this.actualPrice = ((this.tempUnitPrice.toDouble(pos: getDeci()) * this.quantity.toDouble(pos: getDeci())) + this.tempExciseDuty.toDouble(pos: getDeci()) ??"0.0").toString();
      }
    } else {
      if (this.quantity == 0.0) {
        this.actualPrice = (this.unitPrice + this.exciseDuty ?? 0.0);
      } else {
        // this.actualPrice = (this.unitPrice ?? 0.0 * this.quantity ?? 0.0) + (this.exciseDuty ?? 0.0);
        this.actualPrice = (this.unitPrice * this.quantity) + (this.exciseDuty);

      }
    }
    return this.actualPrice;
  }

  double getDiscount() {
    if (isEditModeOn) {
      this.tempDiscount = (getActualPrice() * this.tempDiscPercentage) / 100.0;
    } else {
      this.discount = (getActualPrice() * this.discPercentage) / 100.0;
    }
    return (isEditModeOn) ? this.tempDiscount : this.discount;
  }

  double getDiscPercentage() {
    if (isEditModeOn) {
      this.tempDiscPercentage = (tempDiscount != 0.0)
          ? ((tempDiscount / getActualPrice()) * 100)
          : 0.0;
    } else {
      this.discPercentage = (discount != 0.0)
          ? ((discount / (getActualPrice() == 0.0 ? 1.0 : getActualPrice())) *
              100)
          : 0.0;
    }
    return (isEditModeOn) ? this.tempDiscPercentage : this.discPercentage;
  }

  double getGrossPrice() {
    if (isEditModeOn) {
      this.grossPrice = (getActualPrice() - this.tempDiscount);
    } else {
      this.grossPrice = getActualPrice() - this.discount;
    }
    return this.grossPrice;
  }

  double getTaxAmount() {
    this.taxAmount = ((getGrossPrice() * this.taxPercentage) / 100);
    return this.taxAmount;
  }

  double getNetAmount() {
    if (needToAddTax) {
      this.netAmount = (getGrossPrice() + this.getTaxAmount());
    } else {
      this.netAmount = getGrossPrice();
    }
    return this.netAmount;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UnitPrice'] = this.unitPrice;
    data['Quantity'] = this.quantity;
    data['ActualPrice'] = this.actualPrice;
    data['discountPercentage'] = this.discPercentage;
    data['Discount'] = this.discount;
    data['GrossPrice'] = this.grossPrice;
    data['TaxPercentage'] = this.taxPercentage;
    data['Tax'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;

    return data;
  }
}
