class CommonTandCInfo {

  static final CommonTandCInfo _commonTandCInfo =
  new CommonTandCInfo._internal();

  factory CommonTandCInfo() => _commonTandCInfo;

  CommonTandCInfo._internal();

  static CommonTandCInfo get shared => _commonTandCInfo;

  int orderId;

  List<TermsAndCondions> termsAndConditionList = [];

  CommonTandCInfo.fromJson(Map<String, dynamic> json) {
    if (json["OrderId"]) {
      orderId = json["OrderId"];
    }

    if (json['TermsAndCondionList'] != null) {
      termsAndConditionList = new List<TermsAndCondions>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndConditionList.add(new TermsAndCondions.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json["OrderId"]) {
      orderId = json["OrderId"];
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndConditionList = new List<TermsAndCondions>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndConditionList.add(new TermsAndCondions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderId != null) {
      data["OrderId"] = orderId;
    }
    if (this.termsAndConditionList != null) {
      data['TermsAndCondionList'] =
          this.termsAndConditionList.map((v) => v.toJson()).toList();
    }
    return data;
  }
  clear() {
    orderId = null;
    termsAndConditionList = [];
  }
}

class TermsAndCondions {
  bool editable;

  int salesOrderTermsId;
  int termsId;
  String termsAndConditions;
  String type;
  int sortOrder;
  int paymentTypeId;
  double paymentValue;
  String scopeTypeName;
  String paymentTypeName;
  int scopeTypeId;

  TermsAndCondions(
      {this.salesOrderTermsId,
        this.termsId,
        this.termsAndConditions,
        this.type,
        this.sortOrder,
        this.paymentTypeId,
        this.paymentValue,
        this.scopeTypeName,
        this.paymentTypeName,
        this.scopeTypeId});

  TermsAndCondions.fromJson(Map<String, dynamic> json) {
    if (json["Editable"] != null) {
      editable = json["Editable"];
    }
    if (json["SalesOrderTermsId"] != null) {
      salesOrderTermsId = json["SalesOrderTermsId"];
    }
    if (json["TermsId"] != null) {
      termsId = json["TermsId"];
    }
    if (json["TermsAndConditionsName"] != null) {
      termsAndConditions = json["TermsAndConditionsName"];
    }
    if (json["Type"] != null) {
      type = json["Type"];
    }
    if (json["SortOrder"] != null) {
      sortOrder = json["SortOrder"];
    }
    if (json["PaymentTypeId"] != null) {
      paymentTypeId = json["PaymentTypeId"];
    }
    if (json["PaymentValue"] != null) {
      paymentValue = json["PaymentValue"];
    }
    if (json["ScopeTypeName"] != null) {
      scopeTypeName = json["ScopeTypeName"];
    }
    if (json["PaymentTypeName"] != null) {
      paymentTypeName = json["PaymentTypeName"];
    }
    if (json["ScopeTypeId"] != null) {
      scopeTypeId = json["ScopeTypeId"];
    }
  }

  TermsAndCondions.fromSO(Map<String, dynamic> json) {
    if (json["Editable"] != null) {
      editable = json["Editable"];
    }
    if (json['SalesOrderTermsId'] != null) {
      salesOrderTermsId = json['SalesOrderTermsId'];
    }
    if (json['TermsId'] != null) {
      termsId = json['TermsId'];
    }
    if (json['TermsAndConditions'] != null) {
      termsAndConditions = json['TermsAndConditions'];
    }
    if (json['Type'] != null) {
      type = json['Type'];
    }
    if (json['SortOrder'] != null) {
      sortOrder = json['SortOrder'];
    }
    if (json['PaymentTypeId'] != null) {
      paymentTypeId = json['PaymentTypeId'];
    }
    if (json['PaymentValue'] != null) {
      paymentValue = json['PaymentValue'];
    }
    if (json['ScopeTypeName'] != null) {
      scopeTypeName = json['ScopeTypeName'];
    }
    if (json['PaymentTypeName'] != null) {
      paymentTypeName = json['PaymentTypeName'];
    }
    if (json['ScopeTypeId'] != null) {
      scopeTypeId = json['ScopeTypeId'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.editable != null) {
      data["Editable"] = editable;
    }
    data['SalesOrderTermsId'] = this.salesOrderTermsId;
    data['TermsId'] = this.termsId;
    data['TermsAndConditions'] = this.termsAndConditions;
    data['Type'] = this.type;
    data['SortOrder'] = this.sortOrder;
    data['PaymentTypeId'] = this.paymentTypeId;
    data['PaymentValue'] = this.paymentValue;
    data['ScopeTypeName'] = this.scopeTypeName;
    data['PaymentTypeName'] = this.paymentTypeName;
    data['ScopeTypeId'] = this.scopeTypeId;
    return data;
  }
}
