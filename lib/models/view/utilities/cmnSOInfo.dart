import 'package:bcore_inventory_management/models/response/utilities/getSODetailsResp.dart';

class CommonSoInfo {

  static final CommonSoInfo _commonSoInfo = new CommonSoInfo._internal();
  factory CommonSoInfo() => _commonSoInfo;
  CommonSoInfo._internal();
  static CommonSoInfo get shared => _commonSoInfo;

  List<MaterialDetailsList> materialDetailsList;
  List<ServiceDetailsList> serviceDetailsList;
  List<TermsAndCondionList> termsAndCondionList;
  List<AdditionalChargesDetailsList> additionalChargesDetailsList;

  CommonSoInfo.fromJson(Map<String, dynamic> json) {
    if (json['MaterialDetailsList'] != null) {
      materialDetailsList = new List<MaterialDetailsList>();
      json['MaterialDetailsList'].forEach((v) {
        materialDetailsList.add(new MaterialDetailsList.fromJson(v));
      });
    }
    if (json['ServiceDetailsList'] != null) {
      serviceDetailsList = new List<ServiceDetailsList>();
      json['ServiceDetailsList'].forEach((v) {
        serviceDetailsList.add(new ServiceDetailsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['MaterialDetailsList'] != null) {
      materialDetailsList = new List<MaterialDetailsList>();
      json['MaterialDetailsList'].forEach((v) {
        materialDetailsList.add(new MaterialDetailsList.fromJson(v));
      });
    }
    if (json['ServiceDetailsList'] != null) {
      serviceDetailsList = new List<ServiceDetailsList>();
      json['ServiceDetailsList'].forEach((v) {
        serviceDetailsList.add(new ServiceDetailsList.fromJson(v));
      });
    }
    if (json['TermsAndCondionList'] != null) {
      termsAndCondionList = new List<TermsAndCondionList>();
      json['TermsAndCondionList'].forEach((v) {
        termsAndCondionList.add(new TermsAndCondionList.fromJson(v));
      });
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      additionalChargesDetailsList = new List<AdditionalChargesDetailsList>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        additionalChargesDetailsList
            .add(new AdditionalChargesDetailsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.materialDetailsList != null) {
      data['MaterialDetailsList'] =
          this.materialDetailsList.map((v) => v.toJson()).toList();
    }
    if (this.serviceDetailsList != null) {
      data['ServiceDetailsList'] =
          this.serviceDetailsList.map((v) => v.toJson()).toList();
    }
    if (this.termsAndCondionList != null) {
      data['TermsAndCondionList'] =
          this.termsAndCondionList.map((v) => v.toJson()).toList();
    }
    if (this.additionalChargesDetailsList != null) {
      data['AdditionalChargesDetailsList'] =
          this.additionalChargesDetailsList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    materialDetailsList = [];
    serviceDetailsList = [];
    termsAndCondionList = [];
    additionalChargesDetailsList = [];
  }
}
