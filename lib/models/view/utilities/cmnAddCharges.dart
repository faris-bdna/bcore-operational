import 'package:bcore_inventory_management/models/request/DN/saveDNReq.dart';

class CommonAddChargesInfo {


  static final CommonAddChargesInfo _commonAddChargesInfo =
      new CommonAddChargesInfo._internal();
  factory CommonAddChargesInfo() => _commonAddChargesInfo;
  CommonAddChargesInfo._internal();
  static CommonAddChargesInfo get shared => _commonAddChargesInfo;

  int orderId;
  List<CommonAddCharges> cmnAddChargesList = [];

  CommonAddChargesInfo.fromJson(Map<String, dynamic> json) {
    if (json["OrderId"]) {
      orderId = json["OrderId"];
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      cmnAddChargesList = new List<CommonAddCharges>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        cmnAddChargesList.add(new CommonAddCharges.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json["OrderId"]) {
      orderId = json["OrderId"];
    }
    if (json['AdditionalChargesDetailsList'] != null) {
      cmnAddChargesList = new List<CommonAddCharges>();
      json['AdditionalChargesDetailsList'].forEach((v) {
        cmnAddChargesList.add(new CommonAddCharges.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderId != null) {
      data["OrderId"] = orderId;
    }
    if (this.cmnAddChargesList != null) {
      data['AdditionalChargesDetailsList'] =
          this.cmnAddChargesList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    orderId = null;
    cmnAddChargesList = [];
  }
}

class CommonAddCharges {
  bool editable;

  int productId;
  String productName;
  String productUnit;
  double unitPrice;
  int unitId;
  double quantity;
  // double rate;
  double total;
  int taxId;
  int taxSlabId;
  double taxAmount;
  double netAmount;
  double taxPercent;
  String bOQDescription;

  CommonAddCharges({
    // this.itemId,
    this.unitId,
    this.quantity,
    // this.rate,
    this.total,
    this.taxId,
    this.taxSlabId,
    this.taxAmount,
    this.netAmount,
    this.taxPercent,
    this.bOQDescription,
    this.productId,
    this.productName,
    this.productUnit,
    this.unitPrice,
  });

  CommonAddCharges.fromJson(Map<String, dynamic> json) {
    if (json["Editable"] != null) {
      editable = json["Editable"];
    }
    if (json['ProductId'] != null) {
      productId = json['ProductId'];
    }
    if (json['ProductName'] != null) {
      productName = json['ProductName'];
    }
    if (json['ProductUnit'] != null) {
      productUnit = json['ProductUnit'];
    }
    if (json['UnitPrice'] != null) {
      unitPrice = json['UnitPrice'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      quantity = json['Quantity'];
    }
    // if (json['Rate'] != null) {
    //   rate = json['Rate'];
    // }
    if (json['Total'] != null) {
      total = json['Total'];
    }
    if (json['TaxId'] != null) {
      taxId = json['TaxId'];
    }
    if (json['TaxSlabId'] != null) {
      taxSlabId = json['TaxSlabId'];
    }
    if (json['TaxAmount'] != null) {
      taxAmount = json['TaxAmount'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['TaxPercent'] != null) {
      taxPercent = json['TaxPercent'];
    }
    if (json['BOQDescription'] != null) {
      bOQDescription = json['BOQDescription'];
    }
  }

  CommonAddCharges.fromSO(Map<String, dynamic> json) {

    if (json["Editable"] != null) {
      editable = json["Editable"];
    }

    if (json['ItemId'] != null) {
      productId = json['ItemId'];
    }
    if (json['ItemName'] != null) {
      productName = json['ItemName'];
    }
    if (json['UnitName'] != null) {
      productUnit = json['UnitName'];
    }
    if (json['Rate'] != null) {
      unitPrice = json['Rate'];
    }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      quantity = json['Quantity'];
    }
    // if (json['Rate'] != null) {
    //   rate = json['Rate'];
    // }
    if (json['Total'] != null) {
      total = json['Total'];
    }
    if (json['TaxID'] != null) {
      taxId = json['TaxID'];
    }
    if (json['TaxSlabId'] != null) {
      taxSlabId = json['TaxSlabId'];
    }
    if (json['TaxAmount'] != null) {
      taxAmount = json['TaxAmount'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['TaxPercent'] != null) {
      taxPercent = json['TaxPercent'];
    }
    if (json['BOQDescription'] != null) {
      bOQDescription = json['BOQDescription'];
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json["Editable"] != null) {
      editable = json["Editable"];
    }
    productId = json['ProductId'];
    productName = json['ProductName'];
    // productCode = json['ProductCode'];
    // productDescription = json['ProductDescription'];
    productUnit = json['ProductUnit'];
    // categoryName = json['CategoryName'];
    // subCategoryName = json['SubCategoryName'];
    unitPrice = json['UnitPrice'];
    // tax = json['Tax'];
    // taxSlab = json['TaxSlab'];
    // productCost = json['ProductCost'];
    // profitPercentage = json['ProfitPercentage'];
    // parentProduct = json['ParentProduct'];
    // divisionName = json['DivisionName'];
    // departmentName = json['DepartmentName'];
    // taxPercent = json['TaxPercent'];
    //
    // taxAmount = json['TaxAmount'];
    // totalAmount = json['TotalAmount'];
    // quantity = json['Quantity'];
    // if (json['ItemId'] != null) {
    //   itemId = json['ItemId'];
    // }
    if (json['UnitId'] != null) {
      unitId = json['UnitId'];
    }
    if (json['Quantity'] != null) {
      quantity = json['Quantity'];
    }
    // if (json['Rate'] != null) {
    //   rate = json['Rate'];
    // }
    if (json['Total'] != null) {
      total = json['Total'];
    }
    if (json['TaxId'] != null) {
      taxId = json['TaxId'];
    }
    if (json['TaxSlabId'] != null) {
      taxSlabId = json['TaxSlabId'];
    }
    if (json['TaxAmount'] != null) {
      taxAmount = json['TaxAmount'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['TaxPercent'] != null) {
      taxPercent = json['TaxPercent'];
    }
    if (json['BOQDescription'] != null) {
      bOQDescription = json['BOQDescription'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.editable != null) {
      data["Editable"] = editable;
    }
    data['ProductId'] = this.productId;
    data['ProductName'] = this.productName;
    data['ProductUnit'] = this.productUnit;
    data['UnitPrice'] = this.unitPrice;
    data['UnitId'] = this.unitId;
    data['Quantity'] = this.quantity;
    data['Total'] = this.total;
    data['TaxId'] = this.taxId;
    data['TaxSlabId'] = this.taxSlabId;
    data['TaxAmount'] = this.taxAmount;
    data['NetAmount'] = this.netAmount;
    data['TaxPercent'] = this.taxPercent;
    data['BOQDescription'] = this.bOQDescription;
    return data;
  }
}
