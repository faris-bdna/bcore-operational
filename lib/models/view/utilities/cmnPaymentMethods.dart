
class CmnPaymentMethods {

  static final CmnPaymentMethods _cmnPaymentMethodsInfo = new CmnPaymentMethods
      ._internal();
  factory CmnPaymentMethods() => _cmnPaymentMethodsInfo;
  CmnPaymentMethods._internal();
  static CmnPaymentMethods get shared => _cmnPaymentMethodsInfo;

  List <CmnPaymentMethod> cmnPaymentMethodList = [];

  CmnPaymentMethods.fromJson(Map<String, dynamic> json) {
    if (json['SalesInvoiceReceiptList'] != null) {
      cmnPaymentMethodList = new List<CmnPaymentMethod>();
      json['SalesInvoiceReceiptList'].forEach((v) {
        cmnPaymentMethodList.add(new CmnPaymentMethod.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['SalesInvoiceReceiptList'] != null) {
      cmnPaymentMethodList = new List<CmnPaymentMethod>();
      json['SalesInvoiceReceiptList'].forEach((v) {
        cmnPaymentMethodList.add(new CmnPaymentMethod.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.cmnPaymentMethodList != null) {
      data['SalesInvoiceReceiptList'] = this.cmnPaymentMethodList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    cmnPaymentMethodList = [];
  }
}

class CmnPaymentMethod {
  String chequeName;
  double amount;
  String chequeNo;
  String paymentTypeValue;
  String paymentTypeValueName;
  String paymentSubTypeValue;
  String paymentSubTypeValueName;
  String chequeDate;

  CmnPaymentMethod(
      {this.paymentTypeValue,
        this.paymentSubTypeValue,
        this.chequeNo,
        this.paymentSubTypeValueName,
        this.chequeName,
        this.paymentTypeValueName,
        this.amount,
        this.chequeDate
      });

  CmnPaymentMethod.fromJson(Map<String, dynamic> json) {
    if(json['PaymentTypeValue'] != null) {
      paymentTypeValue = json['PaymentTypeValue'];
    }
    if(json['PaymentSubTypeValue'] != null) {
      paymentSubTypeValue = json['PaymentSubTypeValue'];
    }
    if(json['PaymentTypeValueName'] != null) {
      paymentTypeValueName = json['PaymentTypeValueName'];
    }
    if(json['PaymentSubTypeValueName'] != null) {
      paymentSubTypeValueName = json['PaymentSubTypeValueName'];
    }
    if(json['ChequeNo'] != null) {
      chequeNo = json['ChequeNo'];
    }
    if(json['ChequeName'] != null) {
      chequeName = json['ChequeName'];
    }
    if(json['Amount'] != null) {
      amount = json['Amount'];
    }
    if(json['ChequeDate'] != null) {
      chequeDate = json['ChequeDate'];
    }
  }

  fromJson(Map<String, dynamic> json) {

    if(json['PaymentTypeValue'] != null) {
      paymentTypeValue = json['PaymentTypeValue'];
    }
    if(json['PaymentSubTypeValue'] != null) {
      paymentSubTypeValue = json['PaymentSubTypeValue'];
    }
    if(json['PaymentTypeValueName'] != null) {
      paymentTypeValueName = json['PaymentTypeValueName'];
    }
    if(json['PaymentSubTypeValueName'] != null) {
      paymentSubTypeValueName = json['PaymentSubTypeValueName'];
    }
    if(json['ChequeNo'] != null) {
      chequeNo = json['ChequeNo'];
    }
    if(json['ChequeName'] != null) {
      chequeName = json['ChequeName'];
    }
    if(json['Amount'] != null) {
      amount = json['Amount'];
    }
    if(json['ChequeDate'] != null) {
      chequeDate = json['ChequeDate'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['PaymentTypeValue'] = this.paymentTypeValue;
    data['PaymentSubTypeValue'] = this.paymentSubTypeValue;
    data['ChequeNo'] = this.chequeNo;
    data['ChequeName'] = this.chequeName;

    data['PaymentTypeValueName'] = this.paymentTypeValueName;
    data['PaymentSubTypeValueName'] = this.paymentSubTypeValueName;
    data['Amount'] = this.amount;
    data['ChequeDate'] = this.chequeDate;
    return data;
  }

  clear(){
    chequeName = amount = chequeNo = paymentTypeValue = null;
    paymentTypeValueName = paymentSubTypeValue = paymentSubTypeValueName = null;
    chequeDate = null;
  }
}

class ChequeInfo {
  String chequeName;
  String chequeNo;
  String chequeDate;

  ChequeInfo({this.chequeName, this.chequeNo, this.chequeDate});

  ChequeInfo.fromJson(Map<String, dynamic> json) {
    if(json['ChequeNo'] != null) {
      chequeNo = json['ChequeNo'];
    }
    if(json['ChequeName'] != null) {
      chequeName = json['ChequeName'];
    }
    if(json['ChequeDate'] != null) {
      chequeDate = json['ChequeDate'];
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json == null) return;

    if(json['ChequeNo'] != null) {
      chequeNo = json['ChequeNo'];
    }
    if(json['ChequeName'] != null) {
      chequeName = json['ChequeName'];
    }
    if(json['ChequeDate'] != null) {
      chequeDate = json['ChequeDate'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.chequeNo != null) {
      data['ChequeNo'] = this.chequeNo;
    }
    if(this.chequeName != null) {
      data['ChequeName'] = this.chequeName;
    }
    if(this.chequeDate != null) {
      data['ChequeDate'] = this.chequeDate;
    }
    return data;
  }

  clear(){
    chequeName = chequeNo = chequeDate = null;
  }
}