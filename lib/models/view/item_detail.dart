import 'package:bcore_inventory_management/common/common.dart';

class CommonItemViewModel {
  static final CommonItemViewModel _commonItemViewModel =
      new CommonItemViewModel._internal();

  factory CommonItemViewModel() => _commonItemViewModel;
  CommonItemViewModel._internal();
  static CommonItemViewModel get shared => _commonItemViewModel;
  List<CommonItem> cmnItemList = [];

  CommonItemViewModel.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnItemList = new List<CommonItem>();
      json['ItemDetails'].forEach((v) {
        cmnItemList.add(new CommonItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnItemList = new List<CommonItem>();
      json['ItemDetails'].forEach((v) {
        cmnItemList.add(new CommonItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.cmnItemList != null) {
      data['ItemDetails'] = this.cmnItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  // clear() {
  //   cmnItemList = null;
  // }
}

class CommonItem {
  int mtlReqHdId;
  int mtlReqDtId;
  int referenceDtId;
  bool isSubstitute;

  int LineNum;
  bool IsNewItem;
  String ItemCode;
  String ItemName;
  String ItemDesc;
  String ItemBarcode;
  int ProductId;
  int ItemId;
  int ProdpkgId;
  int ProdClassId;
  int BrandId;
  int CountryId;
  double ReqQty;
  String Comments;
  int AppStatus;
  bool IsSubstituteAllowed;
  double UnitCost;
  double NetCost;
  double TotalCost;
  String PackageName;
  String CountryName;
  String BrandName;
  double AvailableQty;
  bool isInstallationChecked;
  String batchNo;
  int IsBatchEnabled;

  int warehouseId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int wHouseZoneId;
  double unitQty;

  int storeIssueDetailId;
  int storeIssueId;
  String expiryDate;
  int isAssetItem;

  double approvedQty;
  double issueTransferQty;

  String packingDesc;
  double pkgQty; /// Quantity in terms of base (least) packing

  double qtyInPieces;

  int itIndex;

  CommonItem(
      {this.mtlReqHdId,
        this.mtlReqDtId,
        this.referenceDtId,
      this.LineNum,
      this.IsNewItem,
      this.ItemCode,
      this.ItemName,
      this.ItemDesc,
      this.ItemBarcode,
      this.ProductId,
      this.ItemId,
      this.ProdpkgId,
      this.ProdClassId,
      this.BrandId,
      this.CountryId,
      this.ReqQty,
      this.Comments,
      this.AppStatus,
      this.IsSubstituteAllowed,
      this.UnitCost,
      this.NetCost,
      this.TotalCost,
      this.PackageName,
      this.BrandName,
      this.CountryName,
      this.AvailableQty,
      this.isInstallationChecked,
      this.batchNo,
      this.IsBatchEnabled,
      this.warehouseId,
      this.wHouseZoneBinId,
      this.wHouseZoneBinRackId,
      this.wHouseZoneId,
      this.unitQty,
      this.storeIssueId,
      this.storeIssueDetailId,
      this.expiryDate,
        this.isAssetItem,
        this.issueTransferQty,
        this.approvedQty,
        this.packingDesc,
        this.pkgQty,
        this.isSubstitute,
        this.itIndex
      });

  CommonItem.fromJson(Map<String, dynamic> json) {
    if (json['MtlReqHdId'] != null) {
      mtlReqHdId = json['MtlReqHdId'];
    }
    if (json['MtlReqDtId'] != null) {
      mtlReqDtId = json['MtlReqDtId'];
    }
    if (json['ReferenceDtId'] != null) {
      referenceDtId = json['ReferenceDtId'];
    }
    if (json['LineNum'] != null) {
      LineNum = json['LineNum'];
    }
    if (json['IsNewItem'] != null) {
      IsNewItem = json['IsNewItem'];
    }
    if (json['ItemCode'] != null) {
      ItemCode = json['ItemCode'];
    }
    if (json['ItemName'] != null) {
      ItemName = json['ItemName'];
    }
    if (json['ItemDesc'] != null) {
      ItemDesc = json['ItemDesc'];
    }
    if (json['ItemBarcode'] != null) {
      ItemBarcode = json['ItemBarcode'];
    }
    if (json['ProductId'] != null) {
      ProductId = json['ProductId'];
    }
    if (json['ItemId'] != null) {
      ItemId = json['ItemId'];
    }
    if (json['ProdpkgId'] != null) {
      ProdpkgId = json['ProdpkgId'];
    }
    if (json['ProdClassId'] != null) {
      ProdClassId = json['ProdClassId'];
    }
    if (json['BrandId'] != null) {
      BrandId = json['BrandId'];
    }
    if (json['CountryId'] != null) {
      CountryId = json['CountryId'];
    }
    if (json['ReqQty'] != null) {
      ReqQty = json['ReqQty'] as double;
    }
    if (json['Comments'] != null) {
      Comments = json['Comments'];
    }
    if (json['AppStatus'] != null) {
      AppStatus = json['AppStatus'];
    }
    if (json['IsSubstituteAllowed'] != null) {
      IsSubstituteAllowed = json['IsSubstituteAllowed'];
    }
    if (json['UnitCost'] != null) {
      UnitCost = json['UnitCost'] as double;
    }
    if (json['NetCost'] != null) {
      NetCost = json['NetCost'] as double;
    }
    if (json['TotalCost'] != null) {
      TotalCost = json['TotalCost'] as double;
    }
    if (json['PackageName'] != null) {
      PackageName = json['PackageName'];
    }
    if (json['CountryName'] != null) {
      CountryName = json['CountryName'];
    }
    if (json['BrandName'] != null) {
      BrandName = json['BrandName'];
    }
    if (json['AvailableQty'] != null) {
      AvailableQty = json['AvailableQty'];
    }
    if (json['IsInstallationChecked'] != null) {
      isInstallationChecked = json['IsInstallationChecked'];
    }
    if (json['BatchNo'] != null) {
      batchNo = json['BatchNo'];
    }
    if (json['IsBatchEnabled'] != null) {
      IsBatchEnabled = json['IsBatchEnabled'];
    }
    if (json['WarehouseId'] != null) {
      warehouseId = json['WarehouseId'];
    }
    if (json['WHouseZoneBinId'] != null) {
      wHouseZoneBinId = json['WHouseZoneBinId'];
    }
    if (json['WHouseZoneBinRackId'] != null) {
      wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    }
    if (json['IteWHouseZoneIdmId'] != null) {
      wHouseZoneId = json['WHouseZoneId'];
    }
    if (json['UnitQty'] != null) {
      unitQty = json['UnitQty'];
    }
    if (json['StoreIssueId'] != null) {
      storeIssueId = json['StoreIssueId'];
    }
    if (json['StoreIssueDetailId'] != null) {
      storeIssueDetailId = json['StoreIssueDetailId'];
    }
    if (json['ExpiryDate'] != null) {
      expiryDate = json['ExpiryDate'];
    }
    if (json['IsAssetItem'] != null) {
      isAssetItem = json['IsAssetItem'];
    }

    if (json['ApprovedQty'] != null) {
      approvedQty = json['ApprovedQty'];
    }
    if (json['IssueTransferQty'] != null) {
      issueTransferQty = json['IssueTransferQty'];
    }
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
    if (json['PkgQty'] != null) {
      pkgQty = json['PkgQty'];
    }
    if (json['IsSubstitute'] != null) {
      isSubstitute = json['IsSubstitute'];
    }
    if (json['InputType'] != null) {
      itIndex = json['InputType'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MtlReqHdId'] = this.mtlReqHdId;
    data['MtlReqDtId'] = this.mtlReqDtId;
    data['ReferenceDtId'] = this.referenceDtId;

    data['LineNum'] = this.LineNum;
    data['IsNewItem'] = this.IsNewItem;
    data['ItemCode'] = this.ItemCode;
    data['ItemName'] = this.ItemName;
    data['ItemDesc'] = this.ItemDesc;
    data['ItemBarcode'] = this.ItemBarcode;
    data['ProductId'] = this.ProductId;
    data['ItemId'] = this.ItemId;
    data['ProdpkgId'] = this.ProdpkgId;
    data['ProdClassId'] = this.ProdClassId;
    data['BrandId'] = this.BrandId;
    data['CountryId'] = this.CountryId;
    data['ReqQty'] = this.ReqQty;
    data['Comments'] = this.Comments;
    data['AppStatus'] = this.AppStatus;
    data['IsSubstituteAllowed'] = this.IsSubstituteAllowed;
    data['UnitCost'] = this.UnitCost;
    data['NetCost'] = this.NetCost;
    data['TotalCost'] = this.TotalCost;
    data['PackageName'] = this.PackageName;
    data['CountryName'] = this.CountryName;
    data['BrandName'] = this.BrandName;
    data['AvailableQty'] = this.AvailableQty;
    data['IsInstallationChecked'] = this.isInstallationChecked;
    data['BatchNo'] = this.batchNo;
    data['IsBatchEnabled'] = this.IsBatchEnabled;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['UnitQty'] = this.unitQty;
    data['StoreIssueId'] = this.storeIssueId;
    data['StoreIssueDetailId'] = this.storeIssueDetailId;
    data['ExpiryDate'] = this.expiryDate;
    data['IsAssetItem'] = this.isAssetItem;

    data['ApprovedQty'] = this.approvedQty;
    data['IssueTransferQty'] = this.issueTransferQty;
    data['PackingDesc'] = this.packingDesc;
    data['PkgQty'] = this.pkgQty;
    data['IsSubstitute'] = this.isSubstitute;
    data['InputType'] = this.itIndex;

    return data;
  }
}

final List<CommonItem> all_items_List = List<CommonItem>();
