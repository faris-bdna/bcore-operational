import 'package:bcore_inventory_management/common/common.dart';

class CommonBasicItemInfo{

  static final CommonBasicItemInfo _commonBasicItemInfo = new CommonBasicItemInfo
      ._internal();
  factory CommonBasicItemInfo() => _commonBasicItemInfo;
  CommonBasicItemInfo._internal();
  static CommonBasicItemInfo get shared => _commonBasicItemInfo;

  List <CommonBasicItem> cmnBasicItemList = [];

  CommonBasicItemInfo.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnBasicItemList = new List<CommonBasicItem>();
      json['ItemDetails'].forEach((v) {
        cmnBasicItemList.add(new CommonBasicItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnBasicItemList = new List<CommonBasicItem>();
      json['ItemDetails'].forEach((v) {
        cmnBasicItemList.add(new CommonBasicItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.cmnBasicItemList != null) {
      data['ItemDetails'] = this.cmnBasicItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    cmnBasicItemList = [];
  }
}

class CommonBasicItem{
  String index;
  int lineNum;
  String itemCode;
  int itemId;
  int packingId;
  String itemBarcode;
  double phyQty;
  int reasonId;
  String reasonText;
  String expiryDate;
  String batchNo;
  String batchCost;
  String itemName;
  String averageCost;
  String netCost;
  int warehouseId;
  int zoneId;
  int binId;
  int rackId;
  String pkgName;

  int isBatchEnabledItem;
  String expiryDateTemp;

  int minShelfLife;
  int prodStkTypeId;
  // int isBatchEnabled;

  InputType inputType;
  String packingDesc;


  CommonBasicItem();

  CommonBasicItem.fromJson(Map<String, dynamic> json) {
    index = json["Index"];
    lineNum = json['LineNum'];
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    packingId = json['PackingId'];
    itemBarcode = json['ItemBarcode'];
    phyQty = json['PhyQty'];
    reasonId = json['ReasonId'];
    expiryDate = json['ExpiryDate'];
    batchNo = json['BatchNo'];
    itemName = json['ItemName'];
    pkgName = json['PkgName'];
    averageCost = json['AverageCost'];
    netCost = json['NetCost'];
    batchCost = json['BatchCost'];
    batchNo = json['BatchNo'];
    warehouseId = json['WarehouseId'];
    zoneId = json['WHouseZoneId'];
    binId = json['WHouseZoneBinId'];
    rackId = json['WHouseZoneBinRackId'];
    isBatchEnabledItem = json['IsBatchEnabledItem'];
    expiryDateTemp = json['ExpiryDateTemp'];
    reasonText = json['ReasonText'];
    if (json['InputType'] != null) {
      inputType = json['InputType'];
    }
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Index'] = this.index;
    data['LineNum'] = this.lineNum;
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['ItemBarcode'] = this.itemBarcode;
    data['PhyQty'] = this.phyQty;
    data['ReasonId'] = this.reasonId;
    data['ExpiryDate'] = this.expiryDate;
    data['BatchNo'] = this.batchNo;
    data['ItemName'] = this.itemName;
    data['PkgName'] = this.pkgName;

    data['AverageCost'] = this.averageCost;
    data['NetCost'] = this.netCost;
    data['BatchCost'] = this.batchCost;
    data['BatchNo'] = this.batchNo;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneId'] = this.zoneId;
    data['WHouseZoneBinId'] = this.binId;
    data['WHouseZoneBinRackId'] = this.rackId;

    data['IsBatchEnabledItem'] = this.isBatchEnabledItem;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['ReasonText'] = this.reasonText;
    data['InputType'] = this.inputType;
    data['PackingDesc'] = this.packingDesc;

    return data;
  }

  clear(){
    index = lineNum = itemCode = itemId = packingId = itemBarcode = null;
    phyQty = reasonId = reasonText = expiryDate = batchNo = null;
    batchCost = itemName = averageCost = netCost = warehouseId = zoneId = null;
    binId = rackId = pkgName = isBatchEnabledItem = expiryDateTemp = null;
    minShelfLife = prodStkTypeId = packingDesc = null;
  }

}