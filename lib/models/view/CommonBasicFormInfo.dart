class CommonBasicFormInfo {
  String refId;
  String stockReferenceNo;
  int stockTypeId;
  String entryDateTemp;
  String remarks;
  String stockTypeName;
  String stockWarehouseName, stockZoneName, stockRackName, stockBinName;
  String stockBrandName, stockItemName, stockProductGroupName;

  CommonBasicFormInfo();

  CommonBasicFormInfo.fromJson(Map<String, dynamic> json) {
    if (json['RefId'] != null){
      refId = json['RefId'];
    }
    if (json['StockReferenceNo'] != null) {
      stockReferenceNo = json['StockReferenceNo'];
    }
    if (json['StockTypeId'] != null) {
      stockTypeId = json['StockTypeId'];
    }
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.refId != null) {
      data['RefId'] = this.refId;
    }
    if (this.stockReferenceNo != null) {
      data['StockReferenceNo'] = this.stockReferenceNo;
    }
    if (this.stockTypeId != null) {
      data['StockTypeId'] = this.stockTypeId;
    }
    if (this.entryDateTemp != null) {
      data['EntryDateTemp'] = this.entryDateTemp;
    }
    if (this.remarks != null) {
      data['Remarks'] = this.remarks;
    }

    return data;
  }

  clear() {
    refId = stockReferenceNo = null;
    stockTypeId = entryDateTemp = null;
    remarks = stockTypeName = null;
    stockWarehouseName = stockZoneName = stockRackName = stockBinName = null;
    stockBrandName = stockItemName = stockProductGroupName = null;
  }
}

final cmnBasicFormInfo = CommonBasicFormInfo();
