class ToAddress {
  String companyDateFormat;

  String companyId;
  String divisionId;
  String locationId;
  String departmentId;

  String deliveryLocationId;
  String projectId;
  String deliveryPriority;

  String companyName;
  String divisionName;
  String locationName;
  String departmentName;
  String deliveryLocationName;
  String projectName;
  String remarks;

  int transferTypeId;

  String transactionDate;
  String expectedReceiveDate;

  int storeIssueId;
  String storeIssueText;

  int toCompanyId; /// for Save Store receipt

  ToAddress(
      {this.companyId,
      this.companyDateFormat,
      this.divisionId,
      this.locationId,
      this.departmentId,
      this.deliveryLocationId,
      this.companyName,
      this.divisionName,
      this.locationName,
      this.departmentName,
        this.deliveryLocationName,
        this.projectId,
        this.projectName,
        this.deliveryPriority,
        this.transactionDate,
        this.expectedReceiveDate,
        this.transferTypeId,
        this.remarks,
        this.storeIssueId,
        this.storeIssueText
      });

  ToAddress.fromJson(Map<String, dynamic> json) {
    storeIssueId = json['StoreIssueId'];
    companyId = json['ToCompanyId'];
    companyDateFormat = json['ToCompanyDateFormat'];
    divisionId = json['ToDivisionId'];
    locationId = json['ToLocationId'];
    departmentId = json['ToDepartmentId'];
    deliveryLocationId = json['ToDeliveryLocationId'];
    projectId = json['ToProjectId'];

    companyName = json['ToCompanyName'];
    divisionName = json['ToDivisionName'];
    locationName = json['ToLocationName'];
    departmentName = json['ToDepartmentName'];
    deliveryLocationName = json['ToDeliveryLocationName'];
    projectName = json['ToProjectName'];
    deliveryPriority = json['ToDeliveryPriority'];

    transactionDate = json['ToTransactionDate'];
    expectedReceiveDate = json['ToExpiryDate'];
    transferTypeId = json['TransferTypeId'];
    remarks = json['Remarks'];
    storeIssueText = json['StoreIssueText'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['StoreIssueId'] = this.storeIssueId;

    data['ToCompanyId'] = this.companyId;
    data['ToCompanyDateFormat'] = this.companyDateFormat;
    data['ToDivisionId'] = this.divisionId;
    data['ToLocationId'] = this.locationId;
    data['ToDepartmentId'] = this.departmentId;
    data['ToDeliveryLocationId'] = this.deliveryLocationId;
    data['ToProjectId'] = this.projectId;

    data['ToCompanyName'] = this.companyName;
    data['ToDivisionName'] = this.divisionName;
    data['ToLocationName'] = this.locationName;
    data['ToDepartmentName'] = this.departmentName;
    data['ToDeliveryLocationName'] = this.deliveryLocationName;
    data['ToProjectName'] = this.projectName;
    data['ToDeliveryPriority'] = this.deliveryPriority;

    data['ToTransactionDate'] = this.transactionDate;
    data['ToExpiryDate'] = this.expectedReceiveDate;
    data['TransferTypeId'] = this.transferTypeId;
    data['Remarks'] = this.remarks;
    data['StoreIssueText'] = this.storeIssueText;

    return data;
  }

  clear(){
    companyId = companyDateFormat = divisionId = null;
    locationId = departmentId = deliveryLocationId = null;
    projectId = companyName = divisionName = storeIssueText = null;
    locationName = departmentName = deliveryLocationName = null;
    projectName = deliveryPriority = transactionDate = null;
    expectedReceiveDate = transferTypeId = remarks = storeIssueId = null;
  }
}

final toAddress = ToAddress();