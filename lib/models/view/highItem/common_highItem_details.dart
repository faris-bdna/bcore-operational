import 'package:bcore_inventory_management/common/common.dart';

class CommonHighItemInfo {

  static final CommonHighItemInfo _commonHighItemInfo = new CommonHighItemInfo
      ._internal();
  factory CommonHighItemInfo() => _commonHighItemInfo;
  CommonHighItemInfo._internal();
  static CommonHighItemInfo get shared => _commonHighItemInfo;


  List <CommonHighItem> cmnHighItemList = [];

  CommonHighItemInfo.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnHighItemList = new List<CommonHighItem>();
      json['ItemDetails'].forEach((v) {
        cmnHighItemList.add(new CommonHighItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnHighItemList = new List<CommonHighItem>();
      json['ItemDetails'].forEach((v) {
        cmnHighItemList.add(new CommonHighItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.cmnHighItemList != null) {
      data['ItemDetails'] = this.cmnHighItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    cmnHighItemList = [];
  }
}

class CommonHighItem {
  String index;
  String batchNo;
  double discPercent;
  double exciseDuty;
  String expiryDateTemp;
  bool isFoc = false;
  bool isInstallationChecked = false;
  bool isItemQualityChecked;
  String itemCode;
  int itemId;
  int lineNum;
  int packingId;
  String productionDateTemp;
  int projectId;
  String pkgName;

  double unitPrice;
  double unitQty;
  int warehouseId;
  int wHouseZoneBinId;
  int wHouseZoneBinRackId;
  int wHouseZoneId;
  String itemName;
  String itemBarcode;
  double totalCost;
  double uOMQty;
  double totalUOMQty;


  int gRNHeaderId;
  double gRNQty;
  double returnedQty;
  int reasonId;
  String returnComments;

  String countryId;
  String brandId;
  String countryName;
  String brandName;

  bool suppContractPriceStatus;
  int isBatchEnabled;
  String taxPercent;
  String netCost;
  int minShelfLife;
  double pendingQty;

  bool hasParent;
  InputType inputType;
  List<String> serialNoList = [];

  String packingDesc;

  CommonHighItem(
      {this.index,
        this.batchNo,
        this.discPercent,
        this.exciseDuty,
        this.pkgName,
        this.expiryDateTemp,
        this.gRNHeaderId,
        this.isFoc,
        this.isInstallationChecked,
        this.isItemQualityChecked,
        this.itemCode,
        this.itemId,
        this.lineNum,
        this.packingId,
        this.productionDateTemp,
        this.projectId,
        this.unitPrice,
        this.unitQty,
        this.warehouseId,
        this.wHouseZoneBinId,
        this.wHouseZoneBinRackId,
        this.gRNQty,
        this.returnedQty,
        this.reasonId,
        this.returnComments,
        this.wHouseZoneId,
        this.itemName,
        this.itemBarcode,
        this.serialNoList,
        this.countryId,
        this.countryName,
        this.brandName,
        this.brandId,
        this.suppContractPriceStatus,
        this.isBatchEnabled,
        this.taxPercent,
        this.netCost,
        this.minShelfLife,
        this.pendingQty,
        this.hasParent,
        this.inputType,
        this.packingDesc,
        this.uOMQty,
        this.totalUOMQty
      });

  CommonHighItem.fromJson(Map<String, dynamic> json) {
    index = json['Index'];
    batchNo = json['BatchNo'];
    discPercent = json['DiscPercent'];
    exciseDuty = json['ExciseDuty'];
    expiryDateTemp = json['ExpiryDateTemp'];
    isFoc = json['IsFoc'];
    isInstallationChecked = json['IsInstallationChecked'];
    isItemQualityChecked = json['IsItemQualityChecked'];
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    pkgName = json['PkgName'];
    lineNum = json['LineNum'];
    packingId = json['PackingId'];
    productionDateTemp = json['ProductionDateTemp'];
    projectId = json['ProjectId'];
    unitPrice = json['UnitPrice'];
    unitQty = json['UnitQty'];
    warehouseId = json['WarehouseId'];
    wHouseZoneBinId = json['WHouseZoneBinId'];
    wHouseZoneBinRackId = json['WHouseZoneBinRackId'];
    wHouseZoneId = json['WHouseZoneId'];
    gRNHeaderId = json['GRNHeaderId'];
    gRNQty = json['GRNQty'];
    returnedQty = json['ReturnedQty'];
    reasonId = json['ReasonId'];
    returnComments = json['ReturnComments'];

    itemName = json['ItemName'];
    itemBarcode = json['ItemBarcode'];
    totalCost = json['TotalCost'];
    uOMQty = json['UOMQty'];
    totalUOMQty = json['TotalUOMQty'];
    serialNoList = json['SerialNoList'];

    brandId = json['BrandId'];
    brandName = json['BrandName'];
    countryId = json['CountryId'];
    countryName = json['CountryName'];
    suppContractPriceStatus = json['SuppContractPriceStatus'];
    isBatchEnabled = json['IsBatchEnabled'];
    taxPercent = json['TaxPercent'];
    netCost = json['NetCost'];
    minShelfLife = json['MinShelfLife'];
    pendingQty = json['PendingQty'];
    hasParent = json['HasParent'];
    if (json['InputType'] != null) {
      inputType = json['InputType'];
    }
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Index'] = this.index;

    data['BatchNo'] = this.batchNo;
    data['DiscPercent'] = this.discPercent;
    data['ExciseDuty'] = this.exciseDuty;
    data['ExpiryDateTemp'] = this.expiryDateTemp;
    data['IsFoc'] = this.isFoc;
    data['IsInstallationChecked'] = this.isInstallationChecked;
    data['IsItemQualityChecked'] = this.isItemQualityChecked;
    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PkgName'] = this.pkgName;
    data['LineNum'] = this.lineNum;
    data['PackingId'] = this.packingId;
    data['ProductionDateTemp'] = this.productionDateTemp;
    data['ProjectId'] = this.projectId;
    data['UnitPrice'] = this.unitPrice;
    data['UnitQty'] = this.unitQty;
    data['WarehouseId'] = this.warehouseId;
    data['WHouseZoneBinId'] = this.wHouseZoneBinId;
    data['WHouseZoneBinRackId'] = this.wHouseZoneBinRackId;
    data['WHouseZoneId'] = this.wHouseZoneId;
    data['GRNHeaderId'] = this.gRNHeaderId;
    data['GRNQty'] = this.gRNQty;
    data['ReturnedQty'] = this.returnedQty;
    data['ReasonId'] = this.reasonId;
    data['ReturnComments'] = this.returnComments;

    data['ItemBarcode'] = this.itemBarcode;
    data['ItemName'] = this.itemName;
    data['TotalCost'] = this.totalCost;
    data['UOMQty'] = this.uOMQty;
    data['TotalUOMQty'] = this.totalUOMQty;
    data['SerialNoList'] = this.serialNoList;

    data['BrandId'] = this.brandId;
    data['BrandName'] = this.brandName;
    data['CountryId'] = this.countryId;
    data['CountryName'] = this.countryName;
    data['SuppContractPriceStatus'] = this.suppContractPriceStatus;
    data['IsBatchEnabled'] = this.isBatchEnabled;
    data['TaxPercent'] = this.taxPercent;
    data['NetCost'] = this.netCost;
    data['MinShelfLife'] = this.minShelfLife;
    data['PendingQty'] = this.pendingQty;
    data['HasParent'] = this.hasParent;
    data['InputType'] = this.inputType;
    data['PackingDesc'] = this.packingDesc;

    return data;
  }
}