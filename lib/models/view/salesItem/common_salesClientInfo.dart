class CommonSalesClientInfo {
  int projectId;
  String orderRemarks;
  int companyId;
  int divisionId;
  int locationId;
  String entryDate;
  int departmentId;
  int salesmanId;
  String salesmanText;
  int discount;
  bool sameAsBillingAdd;
  int netAmount;
  int billingPartyType; /// Account Type  -  Eg. Business Account, Contact
  String billingPartyTypeName;
  int billingPartyID;  /// Customer Id
  String accountName;
  int appStatus;
  String salesTypeId; /// Sales type - eg. Credit, Walk in Client
  String selSalesType;

  String expectedDelivDate;

  int salesOrderId;
  String salesOrderNo;


  CommonSalesClientInfo();

  CommonSalesClientInfo.fromJson(Map<String, dynamic> json) {

    if (json['ProjectId'] != null){
      projectId = json['ProjectId'];
    }
    if (json['OrderRemarks'] != null) {
      orderRemarks = json['OrderRemarks'];
    }
    if (json['CompanyId'] != null) {
      companyId = json['CompanyId'];
    }
    if (json['DivisionId'] != null) {
      divisionId = json['DivisionId'];
    }
    if (json['LocationId'] != null) {
      locationId = json['LocationId'];
    }
    if (json['EntryDate'] != null){
      entryDate = json['EntryDate'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['SalesmanId'] != null) {
      salesmanId = json['SalesmanId'];
    }
    if (json['Discount'] != null) {
      discount = json['Discount'];
    }
    if (json['SameAsBillingAdd'] != null) {
      sameAsBillingAdd = json['SameAsBillingAdd'];
    }
    if (json['NetAmount'] != null) {
      netAmount = json['NetAmount'];
    }
    if (json['BillingPartyType'] != null) {
      billingPartyType = json['BillingPartyType'];
    }
    if (json['BillingPartyID'] != null) {
      billingPartyID = json['BillingPartyID'];
    }
    if (json['AppStatus'] != null) {
      appStatus = json['AppStatus'];
    }
    if (json['ExpectedDelivDate'] != null) {
      expectedDelivDate = json['ExpectedDelivDate'];
    }
    if (json['SalesTypeId'] != null) {
      salesTypeId = json['SalesTypeId'];
    }

    if (json['SelSalesType'] != null) {
      selSalesType = json['SelSalesType'];
    }

    if (json['BillingPartyTypeName'] != null) {
      billingPartyTypeName = json['BillingPartyTypeName'];
    }
    if (json['SalesmanText'] != null) {
      salesmanText = json['SalesmanText'];
    }
    if (json['AccountName'] != null) {
      accountName = json['AccountName'];
    }
    if (json['SalesOrderId'] != null){
      salesOrderId = json['SalesOrderId'];
    }
    if (json['SalesOrderNo'] != null){
      salesOrderNo = json['SalesOrderNo'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.projectId != null) {
      data['ProjectId'] = this.projectId;
    }
    if (this.orderRemarks != null) {
      data['OrderRemarks'] = this.orderRemarks;
    }
    if (this.companyId != null) {
      data['CompanyId'] = this.companyId;
    }
    if (this.divisionId != null) {
      data['DivisionId'] = this.divisionId;
    }
    if (this.locationId != null) {
      data['LocationId'] = this.locationId;
    }
    if (this.entryDate != null) {
      data['EntryDate'] = this.entryDate;
    }
    if (this.departmentId != null) {
      data['DepartmentId'] = this.departmentId;
    }
    if (this.salesmanId != null) {
      data['SalesmanId'] = this.salesmanId;
    }
    if (this.discount != null) {
      data['Discount'] = this.discount;
    }
    if (this.sameAsBillingAdd != null) {
      data['SameAsBillingAdd'] = this.sameAsBillingAdd;
    }
    if (this.netAmount != null) {
      data['NetAmount'] = this.netAmount;
    }
    if (this.billingPartyType != null) {
      data['BillingPartyType'] = this.billingPartyType;
    }
    if (this.billingPartyID != null) {
      data['BillingPartyID'] = this.billingPartyID;
    }
    if (this.appStatus != null) {
      data['AppStatus'] = this.appStatus;
    }
    if (this.expectedDelivDate != null) {
      data['ExpectedDelivDate'] = this.expectedDelivDate;
    }
    if (this.salesTypeId != null) {
      data['SalesTypeId'] = this.salesTypeId;
    }
    if (this.selSalesType != null) {
      data['SelSalesType'] = this.selSalesType;
    }
    if (this.billingPartyTypeName != null) {
      data['BillingPartyTypeName'] = this.billingPartyTypeName;
    }
    if (this.salesmanText != null) {
      data['SalesmanText'] = this.salesmanText;
    }
    if (this.accountName != null) {
      data['AccountName'] = this.accountName;
    }
    if (this.salesOrderId != null) {
      data['SalesOrderId'] = this.salesOrderId;
    }
    if (this.salesOrderNo != null) {
      data['SalesOrderNo'] = this.salesOrderNo;
    }
    return data;
  }

  clear() {
    projectId = orderRemarks = companyId = null;
    divisionId = locationId = entryDate = null;
    departmentId = salesmanId = discount = null;
    sameAsBillingAdd = netAmount = billingPartyType = null;
    billingPartyID = appStatus = selSalesType = expectedDelivDate = null;
    billingPartyTypeName = salesTypeId = salesmanText = null;
    accountName = salesOrderId = salesOrderNo = null;
  }
}

final cmnSalesClientInfo = CommonSalesClientInfo();