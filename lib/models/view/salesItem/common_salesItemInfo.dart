
import 'package:bcore_inventory_management/common/common.dart';

class CommonSalesItemInfo {

  static final CommonSalesItemInfo _commonSalesItemInfo = new CommonSalesItemInfo
      ._internal();
  factory CommonSalesItemInfo() => _commonSalesItemInfo;
  CommonSalesItemInfo._internal();
  static CommonSalesItemInfo get shared => _commonSalesItemInfo;

  List <CommonSalesItem> cmnSalesItemList = [];

  CommonSalesItemInfo.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnSalesItemList = new List<CommonSalesItem>();
      json['ItemDetails'].forEach((v) {
        cmnSalesItemList.add(new CommonSalesItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      cmnSalesItemList = new List<CommonSalesItem>();
      json['ItemDetails'].forEach((v) {
        cmnSalesItemList.add(new CommonSalesItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.cmnSalesItemList != null) {
      data['ItemDetails'] = this.cmnSalesItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    cmnSalesItemList = [];
  }
}

class CommonSalesItem {
  int lineNum;

  String itemCode;
  String pkgName;
  double focQuantity;
  String itemName;
  String countryId;
  String brandId;
  String countryName;
  String brandName;

  int itemId;
  int unitId; /// Package Id
  double quantity;  /// Quantity
  double rate;      /// Unit Price
  double discountPercentage;
  double discount;
  int itemBarcode;   /// Item barcode
  bool isFromBackEnd; /// This flag becomes true when the price is from local db or user has edited the amount. Turns in to false when the price is same as the api resp
  String averageCost;
  String netCost;
  int minShelfLife;
  String totalCost;

  int productStockTypeId;
  double totalReqQty; /// Total Required Quantity as per selected packing
  double totalBaseQty;

  int orderDetId;
  InputType inputType;

  String packingDesc;
  double rateFromApi;      /// price from api

  CommonSalesItem();

  CommonSalesItem.fromJson(Map<String, dynamic> json) {
    lineNum = json['LineNum'];
    if(json['OrderDetId'] != null) {
      orderDetId = json['OrderDetId'];
    }
    itemCode = json['ItemCode'];
    itemId = json['ItemId'];
    pkgName = json['PkgName'];
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
    itemName = json['ItemName'];
    brandId = json['BrandId'];
    countryId = json['CountryId'];
    brandName = json['BrandName'];
    countryName = json['CountryName'];
    focQuantity = json['FocQuantity'];
    unitId = json['UnitId'];
    quantity = json['Quantity'];
    rate = json['Rate'];
    discountPercentage = json['DiscountPercentage'];
    discount = json['Discount'];
    itemBarcode = json['ItemBarcode'];
    isFromBackEnd = json['IsFromBackEnd'];
    averageCost = json['AverageCost'];
    netCost = json['NetCost'];
    minShelfLife = json['MinShelfLife'];
    productStockTypeId = json['ProductStockTypeId'];
    totalReqQty = json['DeliveryQuantity'];
    totalBaseQty = json['TotalBaseQty'];
    totalCost = json['TotalCost'];

    if (json['InputType'] != null) {
      inputType = json['InputType'];
    }
    if (json['RateFromApi'] != null) {
      rateFromApi = json['RateFromApi'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LineNum'] = this.lineNum;
    data['OrderDetId'] = this.orderDetId;

    data['ItemCode'] = this.itemCode;
    data['ItemId'] = this.itemId;
    data['PkgName'] = this.pkgName;
    data['CountryId'] = this.countryId;
    data['BrandId'] = this.brandId;
    data['CountryName'] = this.countryName;
    data['BrandName'] = this.brandName;
    data['ItemName'] = this.itemName;
    data['FocQuantity'] = this.focQuantity;
    data['UnitId'] = this.unitId;
    data['Rate'] = this.rate;
    data['Quantity'] = this.quantity;
    data['DiscountPercentage'] = this.discountPercentage;
    data['Discount'] = this.discount;
    data['ItemBarcode'] = this.itemBarcode;
    data['IsFromBackEnd'] = this.isFromBackEnd;
    data['AverageCost'] = this.averageCost;
    data['NetCost'] = this.netCost;
    data['MinShelfLife'] = this.minShelfLife;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['DeliveryQuantity'] = this.totalReqQty;
    data['TotalBaseQty'] = this.totalBaseQty;
    data['TotalCost'] = this.totalCost;
    data['InputType'] = this.inputType;
    data['PackingDesc'] = this.packingDesc;
    data['RateFromApi'] = this.rateFromApi;

    return data;
  }
}
