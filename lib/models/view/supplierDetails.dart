class CommonSupplierInfo {
  int gRNTypeId;
  String gRNDateTemp;
  bool isTaxApp;
  bool isQualityChecked;
  bool isCashPurchase;
  bool isInvoiceRcvd;
  String docDateTemp;
  String invoiceNo;
  String deliveryNoteNo;
  String remarks;
  int currencyId;
  String currencyName;
  double currencyRate;

  int suppId;
  int suppTypeId;
  bool isSupplierInternal;
  int gRNTypeRefNo;

  String salesMan;
  String entryDateTemp;
  String priceTypeText;
  int priceTypeId;
  String reasonText;
  int reasonId;
  int pRetTypeId;
  int pRetTypeRefNo;

  int companyId;
  int divisionId;
  int locationId;
  int deliveryTypeId;

  int departmentId;
  int projectId;

  CommonSupplierInfo();

  CommonSupplierInfo.fromJson(Map<String, dynamic> json) {
    if (json['GRNTypeId'] != null){
      gRNTypeId = json['GRNTypeId'];
    }
    if (json['GRNDateTemp'] != null) {
      gRNDateTemp = json['GRNDateTemp'];
    }
    if (json['IsTaxApp'] != null) {
      isTaxApp = json['IsTaxApp'];
    }
    if (json['IsQualityChecked'] != null) {
      isQualityChecked = json['IsQualityChecked'];
    }
    if (json['IsCashPurchase'] != null) {
      isCashPurchase = json['IsCashPurchase'];
    }
    if (json['IsInvoiceRcvd'] != null) {
      isInvoiceRcvd = json['IsInvoiceRcvd'];
    }
    if (json['DocDateTemp'] != null) {
      docDateTemp = json['DocDateTemp'];
    }
    if (json['InvoiceNo'] != null) {
      invoiceNo = json['InvoiceNo'];
    }
    if (json['DeliveryNoteNo'] != null) {
      deliveryNoteNo = json['DeliveryNoteNo'];
    }
    if (json['Remarks'] != null) {
      remarks = json['Remarks'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['CurrencyName'] != null) {
      currencyName = json['CurrencyName'];
    }
    if (json['CurrencyRate'] != null) {
      currencyRate = json['CurrencyRate'];
    }
    if (json['SuppId'] != null) {
      suppId = json['SuppId'];
    }
    if (json['GRNTypeRefNo'] != null) {
      gRNTypeRefNo = json['GRNTypeRefNo'];
    }
    if (json['SuppTypeId'] != null) {
      suppTypeId = json['SuppTypeId'];
    }
    if (json['IsSupplierInternal'] != null) {
      isSupplierInternal = json['IsSupplierInternal'];
    }

    if (json['SalesMan'] != null) {
      salesMan = json['SalesMan'];
    }
    if (json['EntryDateTemp'] != null) {
      entryDateTemp = json['EntryDateTemp'];
    }
    if (json['PriceTypeId'] != null) {
      priceTypeId = json['PriceTypeId'];
    }
    if (json['ReasonId'] != null) {
      reasonId = json['ReasonId'];
    }
    if (json['PRetTypeId'] != null) {
      pRetTypeId = json['PRetTypeId'];
    }
    if (json['PRetTypeRefNo'] != null) {
      pRetTypeRefNo = json['PRetTypeRefNo'];
    }
    if (json['DepartmentId'] != null) {
      departmentId = json['DepartmentId'];
    }
    if (json['ProjectId'] != null) {
      projectId = json['ProjectId'];
    }
    if (json['PriceTypeText'] != null) {
      priceTypeText = json['PriceTypeText'];
    }
    if (json['ReasonText'] != null) {
      reasonText = json['ReasonText'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['GRNTypeId'] = this.gRNTypeId;
    data['GRNDateTemp'] = this.gRNDateTemp;
    data['IsTaxApp'] = this.isTaxApp;
    data['IsQualityChecked'] = this.isQualityChecked;
    data['IsCashPurchase'] = this.isCashPurchase;
    data['IsInvoiceRcvd'] = this.isInvoiceRcvd;
    data['DocDateTemp'] = this.docDateTemp;
    data['InvoiceNo'] = this.invoiceNo;
    data['DeliveryNoteNo'] = this.deliveryNoteNo;
    data['Remarks'] = this.remarks;
    data['CurrencyId'] = this.currencyId;
    data['CurrencyName'] = this.currencyName;

    data['SuppId'] = this.suppId;
    data['GRNTypeRefNo'] = this.gRNTypeRefNo;
    data['SuppTypeId'] = this.suppTypeId;
    data['IsSupplierInternal'] = this.isSupplierInternal;

    data['SalesMan'] = this.salesMan;
    data['EntryDateTemp'] = this.entryDateTemp;
    data['PriceTypeId'] = this.priceTypeId;
    data['ReasonId'] = this.reasonId;
    data['PRetTypeId'] = this.pRetTypeId;
    data['PRetTypeRefNo'] = this.pRetTypeRefNo;
    data['DepartmentId'] = this.departmentId;
    data['ProjectId'] = this.projectId;
    data['PriceTypeText'] = this.priceTypeText;
    data['ReasonText'] = this.reasonText;
    data['CurrencyRate'] = this.currencyRate;

    return data;
  }

  clear() {
    gRNTypeId = gRNDateTemp = isTaxApp = isQualityChecked = null;
    isCashPurchase = isInvoiceRcvd = docDateTemp = invoiceNo = null;
    deliveryNoteNo = remarks = currencyId = suppId = suppTypeId = null;
    isSupplierInternal = gRNTypeRefNo = salesMan = entryDateTemp = null;

    priceTypeId = priceTypeText = reasonId = pRetTypeId = null;
    pRetTypeRefNo = departmentId = projectId = reasonText = currencyRate = null;
    companyId = divisionId = locationId = deliveryTypeId = currencyName = null;
  }
}

final cmnSupplierInfo = CommonSupplierInfo();
