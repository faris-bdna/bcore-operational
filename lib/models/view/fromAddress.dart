class FromAddress {

  static final FromAddress _fromAddress = new FromAddress._internal();
  factory FromAddress() => _fromAddress;
  FromAddress._internal();
  static FromAddress get shared => _fromAddress;

  String fromCompanyId;
  String fromCompanyDateFormat;
  String fromDivisionId;
  String fromLocationId;
  String fromDepartmentId;
  String fromProjectId;
  String fromDeliveryLocationId;

  String fromCompanyName;
  String fromDivisionName;
  String fromLocationName;
  String fromDepartmentName;
  String fromProjectName;
  String fromDeliveryLocationName;

  String curDecimPlace;


  FromAddress.fromJson(Map<String, dynamic> json) {
    fromCompanyId = json['FromCompanyId'];
    fromCompanyDateFormat = json['FromCompanyDateFormat'];
    fromDivisionId = json['FromDivisionId'];
    fromLocationId = json['FromLocationId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromProjectId = json['FromProjectId'];
    fromDeliveryLocationId = json['FromDeliveryAddressId'];

    fromCompanyName = json['FromCompanyName'];
    fromDivisionName = json['FromDivisionName'];
    fromLocationName = json['FromLocationName'];
    fromDepartmentName = json['FromDepartmentName'];
    fromProjectName = json['FromProjectName'];
    fromDeliveryLocationName = json['FromDeliveryLocationName'];
    curDecimPlace = json['CurDecimPlace'];
  }

  fromJson(Map<String, dynamic> json) {
    fromCompanyId = json['FromCompanyId'];
    fromCompanyDateFormat = json['FromCompanyDateFormat'];
    fromDivisionId = json['FromDivisionId'];
    fromLocationId = json['FromLocationId'];
    fromDepartmentId = json['FromDepartmentId'];
    fromProjectId = json['FromProjectId'];
    fromDeliveryLocationId = json['FromDeliveryAddressId'];

    fromCompanyName = json['FromCompanyName'];
    fromDivisionName = json['FromDivisionName'];
    fromLocationName = json['FromLocationName'];
    fromDepartmentName = json['FromDepartmentName'];
    fromProjectName = json['FromProjectName'];
    fromDeliveryLocationName = json['FromDeliveryLocationName'];
    curDecimPlace = json['CurDecimPlace'];
  }
  
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FromCompanyId'] = this.fromCompanyId;
    data['FromCompanyDateFormat'] = this.fromCompanyDateFormat;
    data['FromDivisionId'] = this.fromDivisionId;
    data['FromLocationId'] = this.fromLocationId;
    data['FromDepartmentId'] = this.fromDepartmentId;
    data['FromProjectId'] = this.fromProjectId;
    data['FromDeliveryAddressId'] = this.fromDeliveryLocationId;

    data['FromCompanyName'] = this.fromCompanyName;
    data['FromDivisionName'] = this.fromDivisionName;
    data['FromLocationName'] = this.fromLocationName;
    data['FromDepartmentName'] = this.fromDepartmentName;
    data['FromProjectName'] = this.fromProjectName;
    data['FromDeliveryLocationName'] = this.fromDeliveryLocationName;
    data['CurDecimPlace'] = this.curDecimPlace;
    return data;
  }
}

final fromAddress = FromAddress();