class OrderFormInfo {
  String entryDateTemp;
  int tranGenType;
  String companyId;
  String divisionId;
  String locationId;
  String toCompanyId;
  String toCompanyName;

  String toBrandID;
  String toBrandName;

  String toOriginId;
  String toOriginName;



  String toDivisionId;
  String toDivisionName;

  String toLocId;
  String toLocName;

  String deliveryLocId;

  String supplierText;
  int supplierId;
  String remarks;

  OrderFormInfo();

  OrderFormInfo.fromJson(Map<String, dynamic> json) {
    entryDateTemp = json['EntryDateTemp'];
    tranGenType = json['TranGenType'];
    companyId = json['CompanyId'];
    divisionId = json['DivisionId'];
    locationId = json['LocationId'];
    toCompanyId = json['ToCompanyId'];
    toDivisionId = json['ToDivisionId'];
    toLocId = json['ToLocId'];
    deliveryLocId = json['DeliveryLocId'];
    supplierId = json['SupplierId'];
    remarks = json['Remarks'];
    toCompanyName = json['ToCompanyName'];
    toDivisionName = json['toDivisionName'];
    toLocName = json['ToLocName'];
    supplierText = json['SupplierText'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EntryDateTemp'] = this.entryDateTemp;
    data['TranGenType'] = this.tranGenType;
    data['CompanyId'] = this.companyId;
    data['DivisionId'] = this.divisionId;
    data['LocationId'] = this.locationId;
    data['ToCompanyId'] = this.toCompanyId;
    data['ToDivisionId'] = this.toDivisionId;
    data['ToLocId'] = this.toLocId;
    data['DeliveryLocId'] = this.deliveryLocId;
    data['SupplierId'] = this.supplierId;
    data['Remarks'] = this.remarks;
    data['ToCompanyName'] = this.toCompanyName;
    data['ToDivisionName'] = this.toDivisionName;
    data['ToLocName'] = this.toLocName;
    data['SupplierText'] = this.supplierText;

    return data;
  }

  clear() {
    entryDateTemp = tranGenType = supplierId = null;
    companyId = divisionId = locationId = remarks = null;
    toCompanyId = toDivisionId = toLocId = deliveryLocId = null;
    toCompanyName = toDivisionName = toLocName = supplierText = null;
  }
}

final cmnOrderFormInfo = OrderFormInfo();
