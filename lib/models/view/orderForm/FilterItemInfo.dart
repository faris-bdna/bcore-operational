import 'package:bcore_inventory_management/common/common.dart';

class FilterItemInfo {

  static final FilterItemInfo _filterItemInfo = new FilterItemInfo._internal();

  factory FilterItemInfo() => _filterItemInfo;

  FilterItemInfo._internal();

  static FilterItemInfo get shared => _filterItemInfo;

  List<FilterItem> filterItemList = [];


  FilterItemInfo.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      filterItemList = new List<FilterItem>();
      json['ItemDetails'].forEach((v) {
        filterItemList.add(new FilterItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      filterItemList = new List<FilterItem>();
      json['ItemDetails'].forEach((v) {
        filterItemList.add(new FilterItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.filterItemList != null) {
      data['ItemDetails'] = this.filterItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    filterItemList = null;
  }

}

class FilterItem {
  String index;
  String countryId;
  String brandId;
  String countryName;
  String brandName;
  String productGroupID;
  String productGroupName;
  InputType inputType;
  bool isSelected = false;


  int lineNum;
  String itemCode;
  String itemBarcode;
  int itemId;
  int packingId;
  double orderQty;
  String pkgName;
  String itemName;
  String packingDesc;


  FilterItem({ this.index,
    this.countryId,
    this.brandId,
    this.countryName,
    this.brandName,
    this.productGroupID,
    this.productGroupName,
    this.packingId,
    this.orderQty,
    this.pkgName,
    this.itemName,
    this.inputType,
    this.packingDesc,
    this.isSelected,


  });

  FilterItem.fromJson(Map<String, dynamic> json) {
    index = json['Index'];
    countryId = json['CountryId'];
    brandId = json['BrandId'];
    countryName = json['CountryName'];
    brandName = json['BrandName'];
    productGroupID = json['ProductGroupID'];
    productGroupName = json['productGroupName'];
    isSelected = json['IsItemQualityChecked'];

    if (json['InputType'] != null) {
      inputType = json['InputType'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Index'] = this.index;
    data['CountryId'] = this.countryId;
    data['countryName'] = this.itemBarcode;
    data['BrandId'] = this.brandId;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['OrderQty'] = this.orderQty;
    data['PkgName'] = this.pkgName;
    data['ItemName'] = this.itemName;
    data['BrandId'] = this.brandId;
    data['CountryId'] = this.countryId;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['InputType'] = this.inputType;
    data['PackingDesc'] = this.packingDesc;
    data['IsSelected'] = this.isSelected;


    return data;
  }

}


