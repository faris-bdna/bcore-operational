import 'package:bcore_inventory_management/common/common.dart';

class OrderItemInfo {

  static final OrderItemInfo _orderItemInfo = new OrderItemInfo._internal();
  factory OrderItemInfo() => _orderItemInfo;
  OrderItemInfo._internal();
  static OrderItemInfo get shared => _orderItemInfo;

  List<OrderItem> orderItemList = [];

  OrderItemInfo.fromJson(Map<String, dynamic> json) {
    if (json['ItemDetails'] != null) {
      orderItemList = new List<OrderItem>();
      json['ItemDetails'].forEach((v) {
        orderItemList.add(new OrderItem.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {

    if (json['ItemDetails'] != null) {
      orderItemList = new List<OrderItem>();
      json['ItemDetails'].forEach((v) {
        orderItemList.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.orderItemList != null) {
      data['ItemDetails'] = this.orderItemList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  clear() {
    orderItemList = null;
  }
}

class OrderItem {
  String index;

  int lineNum;
  String itemCode;
  String itemBarcode;
  int itemId;
  int packingId;
  double orderQty;
  String pkgName;
  String itemName;
  String countryId;
  String brandId;
  String countryName;
  String brandName;
  InputType inputType;
  String packingDesc;

  OrderItem(
      {this.index,
      this.lineNum,
      this.itemCode,
      this.itemBarcode,
      this.itemId,
      this.packingId,
      this.orderQty,
      this.pkgName,
      this.itemName,
      this.brandId,
      this.countryId,
      this.countryName,
      this.brandName,
        this.inputType,
        this.packingDesc
      });

  OrderItem.fromJson(Map<String, dynamic> json) {
    index = json['Index'];
    lineNum = json['LineNum'];
    itemCode = json['ItemCode'];
    itemBarcode = json['ItemBarcode'];
    itemId = json['ItemId'];
    packingId = json['PackingId'];
    orderQty = json['OrderQty'];
    pkgName = json['PkgName'];
    itemName = json['ItemName'];
    countryId = json['CountryId'];
    brandId = json['BrandId'];
    countryName = json['CountryName'];
    brandName = json['BrandName'];
    if (json['InputType'] != null) {
      inputType = json['InputType'];
    }
    if (json['PackingDesc'] != null) {
      packingDesc = json['PackingDesc'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Index'] = this.index;
    data['LineNum'] = this.lineNum;
    data['ItemCode'] = this.itemCode;
    data['ItemBarcode'] = this.itemBarcode;
    data['ItemId'] = this.itemId;
    data['PackingId'] = this.packingId;
    data['OrderQty'] = this.orderQty;
    data['PkgName'] = this.pkgName;
    data['ItemName'] = this.itemName;
    data['BrandId'] = this.brandId;
    data['CountryId'] = this.countryId;
    data['BrandName'] = this.brandName;
    data['CountryName'] = this.countryName;
    data['InputType'] = this.inputType;
    data['PackingDesc'] = this.packingDesc;

    return data;
  }

}