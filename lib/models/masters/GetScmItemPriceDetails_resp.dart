import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetScmItemPriceDetailsResp {
  List<GetScmItemPriceList> masterDataList;
  ValidationDetails validationDetails;

  GetScmItemPriceDetailsResp({this.masterDataList, this.validationDetails});

  GetScmItemPriceDetailsResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<GetScmItemPriceList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new GetScmItemPriceList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class GetScmItemPriceList {
  double unitPrice;
  double availableQty;
  String availableQtyText;
  int isSuppContractPrice;

  GetScmItemPriceList({this.unitPrice, this.availableQty, this.availableQtyText, this.isSuppContractPrice});

  GetScmItemPriceList.fromJson(Map<String, dynamic> json) {
    unitPrice = json['UnitPrice'];
    availableQty = json['AvailableQty'];
    availableQtyText = json['AvailableQtyText'];
    isSuppContractPrice = json['IsSuppContractPrice'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UnitPrice'] = this.unitPrice;
    data['AvailableQty'] = this.availableQty;
    data['AvailableQtyText'] = this.availableQtyText;
    data['IsSuppContractPrice'] = this.isSuppContractPrice;
    return data;
  }
}