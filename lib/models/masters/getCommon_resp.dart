import 'package:bcore_inventory_management/models/masters/common_master_data_resp.dart';
import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetCommonResp {
  List<CommonDataList> masterDataList;
  ValidationDetails validationDetails;

  GetCommonResp({this.masterDataList, this.validationDetails});

  GetCommonResp.fromJson(Map<String, dynamic> json) {
    var list = json['MasterDataList'] as List;
    masterDataList = list.map((i) => CommonDataList.fromJson(i)).toList();
    validationDetails = ValidationDetails.fromJson(json['ValidationDetails']);
  }
}
