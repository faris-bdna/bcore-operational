import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetWarehouseBinMasterResp {
  List<WarehouseBinList> masterDataList;
  ValidationDetails validationDetails;

  GetWarehouseBinMasterResp({this.masterDataList, this.validationDetails});

  GetWarehouseBinMasterResp.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<WarehouseBinList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new WarehouseBinList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class WarehouseBinList {
  String text;
  String value;
  int rackId;
  int zoneId;
  int warehouseId;

  WarehouseBinList(
      {this.text, this.value, this.rackId, this.zoneId, this.warehouseId});

  WarehouseBinList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
    rackId = json['RackId'];
    zoneId = json['ZoneId'];
    warehouseId = json['WarehouseId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['RackId'] = this.rackId;
    data['ZoneId'] = this.zoneId;
    data['WarehouseId'] = this.warehouseId;
    return data;
  }
}