class GetCompanyResponse {
  List<CompanyDataList> masterDataList;
  ValidationDetails validationDetails;

  GetCompanyResponse({this.masterDataList, this.validationDetails});

  GetCompanyResponse.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<CompanyDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new CompanyDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class CompanyDataList {
  String value;
  String text;
  String dateFormat;
  String decimalPlace;
  int currencyId;

  CompanyDataList(
      {this.value,
        this.text,
        this.dateFormat,
        this.decimalPlace,
        this.currencyId});

  CompanyDataList.fromJson(Map<String, dynamic> json) {
    value = json['Value'];
    text = json['Text'];
    dateFormat = json['DateFormat'];
    decimalPlace = json['DecimalPlace'];
    currencyId = json['CurrencyId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Value'] = this.value;
    data['Text'] = this.text;
    data['DateFormat'] = this.dateFormat;
    data['DecimalPlace'] = this.decimalPlace;
    data['CurrencyId'] = this.currencyId;
    return data;
  }
}

class ValidationDetails {
  Null errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    errorDetails = json['ErrorDetails'];
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorDetails'] = this.errorDetails;
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}
