class CommonDataList {
  String text;
  String value;

  CommonDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
  }

  CommonDataList.fromRefJson(Map<String, dynamic> json) {
    if (json['PriceTypeName'] != null){
      text = json['PriceTypeName'];
    }
    if (json['PriceTypeId'] != null) {
      value = json['PriceTypeId'].toString() ?? '0.0';
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    return data;
  }

}