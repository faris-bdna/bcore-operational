import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetItemBatchMasterResp {
  List<MasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetItemBatchMasterResp({this.masterDataList, this.validationDetails});

  GetItemBatchMasterResp.fromJson(Map<String, dynamic> json) {
    var list = json['MasterDataList'] as List;
    masterDataList = list.map((i) => MasterDataList.fromJson(i)).toList();
    validationDetails = ValidationDetails.fromJson(json['ValidationDetails']);
  }
}

class MasterDataList {
  String text;
  String value;
  int itemId;
  String prodDate;
  String expiryDate;

  MasterDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['Value'];
    itemId = json['ItemId'];
    prodDate = json['ProdDate'];
    expiryDate = json['ExpiryDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['ItemId'] = this.itemId;
    data['ProdDate'] = this.prodDate;
    data['ExpiryDate'] = this.expiryDate;
    return data;
  }
}
