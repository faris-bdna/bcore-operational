import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetItemPackingMasterResp {
  List<MasterDataList> masterDataList;
  ValidationDetails validationDetails;

  GetItemPackingMasterResp({this.masterDataList, this.validationDetails});

  GetItemPackingMasterResp.fromJson(Map<String, dynamic> json) {
    var list = json['MasterDataList'] as List;
    masterDataList = list.map((i) => MasterDataList.fromJson(i)).toList();
    validationDetails = ValidationDetails.fromJson(json['ValidationDetails']);
  }
}

class MasterDataList {
  String text;
  String batch_value;
  int itemId;

  MasterDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    batch_value = json['Value'];
    itemId = json['ItemId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.batch_value;
    data['ItemId'] = this.itemId;
    return data;
  }
}
