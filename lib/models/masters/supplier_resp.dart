class SupplierResponse {
  List<SupMasterDataList> masterDataList;
  ValidationDetails validationDetails;

  SupplierResponse({this.masterDataList, this.validationDetails});

  SupplierResponse.fromJson(Map<String, dynamic> json) {
    if (json['MasterDataList'] != null) {
      masterDataList = new List<SupMasterDataList>();
      json['MasterDataList'].forEach((v) {
        masterDataList.add(new SupMasterDataList.fromJson(v));
      });
    }
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class SupMasterDataList {
  String text;
  String value;
  int supplierTypeId;
  int currencyId;
  double currencyRate;
  int currencyCnvId;
  int isAllowedCashPurchase;
  int isTaxApplicable;
  String taxAppText;
  int blockingStatusId;
  int isInternal;

  String priceTypeName;
  String currencyName;

  @override
  String toString() {
    return value;
  }

  SupMasterDataList({this.text,
    this.value,
    this.supplierTypeId,
    this.currencyId,
    this.currencyRate,
    this.currencyCnvId,
    this.isAllowedCashPurchase,
    this.isTaxApplicable,
    this.taxAppText,
    this.blockingStatusId,
    this.isInternal,
    this.priceTypeName,
    this.currencyName
  });

  SupMasterDataList.fromJson(Map<String, dynamic> json) {
    if (json['Text'] != null) {
      text = json['Text'];
    }
    if (json['Value'] != null) {
      value = json['Value'];
    }
    if (json['SupplierTypeId'] != null) {
      supplierTypeId = json['SupplierTypeId'];
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['CurrencyRate'] != null) {
      currencyRate = json['CurrencyRate'];
    }
    if (json['CurrencyCnvId'] != null) {
      currencyCnvId = json['CurrencyCnvId'];
    }
    if (json['IsAllowedCashPurchase'] != null) {
      isAllowedCashPurchase = json['IsAllowedCashPurchase'];
    }
    if (json['IsTaxApplicable'] != null) {
      isTaxApplicable = json['IsTaxApplicable'];
    }
    if (json['TaxAppText'] != null) {
      taxAppText = json['TaxAppText'];
    }
    if (json['BlockingStatusId'] != null) {
      blockingStatusId = json['BlockingStatusId'];
    }
    if (json['IsInternal'] != null) {
      isInternal = json['IsInternal'];
    }
  }

  SupMasterDataList.fromRefJson(Map<String, dynamic> json) {
    if (json['SupplierName'] != null) {
      text = json['SupplierName'];
    }
    if (json['SupplierId'] != null) {
      value = json['SupplierId'].toString();
    }
    if (json['CurrencyId'] != null) {
      currencyId = json['CurrencyId'];
    }
    if (json['CurrencyRate'] != null) {
      currencyRate = json['CurrencyRate'];
    }
    if (json['CurrencyName'] != null) {
      currencyName = json['CurrencyName'];
    }
    if (json['IsTaxApplicable'] != null) {
      isTaxApplicable = json['IsTaxApplicable'];
    }
    if (json['PriceTypeName'] != null) {
      priceTypeName = json['PriceTypeName'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['Value'] = this.value;
    data['SupplierTypeId'] = this.supplierTypeId;
    data['CurrencyId'] = this.currencyId;
    data['CurrencyRate'] = this.currencyRate;
    data['CurrencyCnvId'] = this.currencyCnvId;
    data['IsAllowedCashPurchase'] = this.isAllowedCashPurchase;
    data['IsTaxApplicable'] = this.isTaxApplicable;
    data['TaxAppText'] = this.taxAppText;
    data['BlockingStatusId'] = this.blockingStatusId;
    data['IsInternal'] = this.isInternal;

    data['PriceTypeName'] = this.priceTypeName;
    return data;
  }
}

class ValidationDetails {
  Null errorDetails;
  int pid;
  String statusMessage;
  int statusCode;

  ValidationDetails(
      {this.errorDetails, this.pid, this.statusMessage, this.statusCode});

  ValidationDetails.fromJson(Map<String, dynamic> json) {
    errorDetails = json['ErrorDetails'];
    pid = json['Pid'];
    statusMessage = json['StatusMessage'];
    statusCode = json['StatusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorDetails'] = this.errorDetails;
    data['Pid'] = this.pid;
    data['StatusMessage'] = this.statusMessage;
    data['StatusCode'] = this.statusCode;
    return data;
  }
}