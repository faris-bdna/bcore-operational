import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class GetItemBarcodeMasterResp {
  List<ItemBarcodeDataList> masterDataList;
  ValidationDetails validationDetails;

  GetItemBarcodeMasterResp({this.masterDataList, this.validationDetails});

  GetItemBarcodeMasterResp.fromJson(Map<String, dynamic> json) {
    var list = json['MasterDataList'] as List;
    masterDataList = list.map((i) => ItemBarcodeDataList.fromJson(i)).toList();
    validationDetails = ValidationDetails.fromJson(json['ValidationDetails']);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.masterDataList != null) {
      data['MasterDataList'] =
          this.masterDataList.map((v) => v.toJson()).toList();
    }
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}

class ItemBarcodeDataList {
  String text;
  String value;
  int productGroupId;
  int productId;
  String productName;
  int itemId;
  String itemCode;
  String itemName;
  int itemPackingId;
  int majorPackId;
  int basePackId;
  double pkgQty;
  String packingDesc;
  int scaleGroupId;
  int brandId;
  int originCountryId;
  int productStockTypeId;
  int minShelfLife;
  int totalShelfLife;
  int isSalesAllowed;
  int stockAllocTypeId;
  int isTaxApplicable;
  int itemStatus;
  bool purchaseRestriction;
  int isPurchaseAllowed;
  double excessQty;
  bool isMinOrderQtyApp;
  double minOrderQty;
  String suppItemCode;
  int suppTypeId;
  double taxPercent;
  int isBatchEnabled;
  int hasPurchaseGroupAccess;
  int stockBasedItem;
  int consignmentItem;
  double lastPurchaseQty;
  String lastPurchaseDate;
  double lastReceivedAmount;
  int lastReceivedSupplier;
  double averageCost;
  double availableQty;
  double netCost;

  ItemBarcodeDataList.fromJson(Map<String, dynamic> json) {
    text = json['Text'];
    value = json['ItemBarcodeId'];
    productGroupId = json['ProductGroupId'];
    productId = json['ProductId'];
    productName = json['ProductName'];
    itemId = json['ItemId'];
    itemCode = json['ItemCode'];
    itemName = json['ItemName'];
    itemPackingId = json['ItemPackingId'];
    majorPackId = json['MajorPackId'];
    basePackId = json['BasePackId'];
    pkgQty = json['PkgQty'] as double;
    packingDesc = json['PackingDesc'];
    scaleGroupId = json['ScaleGroupId'];
    brandId = json['BrandId'];
    originCountryId = json['OriginCountryId'];
    productStockTypeId = json['ProductStockTypeId'];
    minShelfLife = json['MinShelfLife'];
    totalShelfLife = json['TotalShelfLife'];
    isSalesAllowed = json['IsSalesAllowed'];
    stockAllocTypeId = json['StockAllocTypeId'];
    isTaxApplicable = json['IsTaxApplicable'];
    itemStatus = json['ItemStatus'];
    isPurchaseAllowed = json['IsPurchaseAllowed'];
    excessQty = json['ExcessQty'] as double;
    isMinOrderQtyApp = json['IsMinOrderQtyApp'];
    minOrderQty = json['MinOrderQty'] as double;
    suppItemCode = json['SuppItemCode'];
    suppTypeId = json['SuppTypeId'];
    taxPercent = json['TaxPercent'] as double;
    isBatchEnabled = json['IsBatchEnabled'];
    hasPurchaseGroupAccess = json['HasPurchaseGroupAccess'];
    stockBasedItem = json['StockBasedItem'];
    consignmentItem = json['ConsignmentItem'];
    lastPurchaseQty = json['LastPurchaseQty'] as double;
    lastPurchaseDate = json['LastPurchaseDate'];
    lastReceivedAmount = json['LastReceivedAmount'] as double;
    lastReceivedSupplier = json['LastReceivedSupplier'];
    averageCost = json['AverageCost'] as double;
    availableQty = json['AvailableQty'] as double;
    netCost = json['NetCost'];
  }




  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Text'] = this.text;
    data['ItemBarcodeId'] = this.value;
    data['ProductGroupId'] = this.productGroupId;
    data['ProductId'] = this.productId;
    data['ProductName'] = this.productName;
    data['ItemId'] = this.itemId;
    data['ItemCode'] = this.itemCode;
    data['ItemName'] = this.itemName;
    data['ItemPackingId'] = this.itemPackingId;
    data['MajorPackId'] = this.majorPackId;
    data['BasePackId'] = this.basePackId;
    data['PkgQty'] = this.pkgQty;
    data['PackingDesc'] = this.packingDesc;
    data['ScaleGroupId'] = this.scaleGroupId;
    data['BrandId'] = this.brandId;
    data['OriginCountryId'] = this.originCountryId;
    data['ProductStockTypeId'] = this.productStockTypeId;
    data['MinShelfLife'] = this.minShelfLife;
    data['TotalShelfLife'] = this.totalShelfLife;
    data['IsSalesAllowed'] = this.isSalesAllowed;
    data['StockAllocTypeId'] = this.stockAllocTypeId;
    data['IsTaxApplicable'] = this.isTaxApplicable;
    data['ItemStatus'] = this.itemStatus;
    data['IsPurchaseAllowed'] = this.isPurchaseAllowed;
    data['ExcessQty'] = this.excessQty;
    data['IsMinOrderQtyApp'] = this.isMinOrderQtyApp;
    data['MinOrderQty'] = this.minOrderQty;
    data['SuppItemCode'] = this.suppItemCode;
    data['SuppTypeId'] = this.suppTypeId;
    data['TaxPercent'] = this.taxPercent;
    data['IsBatchEnabled'] = this.isBatchEnabled;
    data['HasPurchaseGroupAccess'] = this.hasPurchaseGroupAccess;
    data['StockBasedItem'] = this.stockBasedItem;
    data['ConsignmentItem'] = this.consignmentItem;
    data['LastPurchaseQty'] = this.lastPurchaseQty;
    data['LastPurchaseDate'] = this.lastPurchaseDate;
    data['LastReceivedAmount'] = this.lastReceivedAmount;
    data['LastReceivedSupplier'] = this.lastReceivedSupplier;
    data['AverageCost'] = this.averageCost;
    data['AvailableQty'] = this.availableQty;
    data['NetCost'] = this.netCost;
    return data;
  }

  ItemBarcodeDataList({
      this.text,
      this.value,
    this.productGroupId,
      this.productId,
      this.productName,
      this.itemId,
      this.itemCode,
      this.itemName,
      this.itemPackingId,
      this.majorPackId,
      this.basePackId,
      this.pkgQty,
      this.packingDesc,
      this.scaleGroupId,
      this.brandId,
      this.originCountryId,
      this.productStockTypeId,
      this.minShelfLife,
      this.totalShelfLife,
      this.isSalesAllowed,
      this.stockAllocTypeId,
      this.isTaxApplicable,
      this.itemStatus,
      this.purchaseRestriction,
      this.isPurchaseAllowed,
      this.excessQty,
      this.isMinOrderQtyApp,
      this.minOrderQty,
      this.suppItemCode,
      this.suppTypeId,
      this.taxPercent,
      this.isBatchEnabled,
      this.hasPurchaseGroupAccess,
      this.stockBasedItem,
      this.consignmentItem,
      this.lastPurchaseQty,
      this.lastPurchaseDate,
      this.lastReceivedAmount,
      this.lastReceivedSupplier,
      this.averageCost,
      this.availableQty,
      this.netCost});
}
final item_BarCode = ItemBarcodeDataList();