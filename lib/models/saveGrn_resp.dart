import 'package:bcore_inventory_management/models/response/common_validation_resp.dart';

class SaveGrnResp {
  String data;
  ValidationDetails validationDetails;

  SaveGrnResp({this.data, this.validationDetails});

  SaveGrnResp.fromJson(Map<String, dynamic> json) {
    validationDetails = json['ValidationDetails'] != null
        ? new ValidationDetails.fromJson(json['ValidationDetails'])
        : null;
    data = json['Data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Data'] = this.data;
    if (this.validationDetails != null) {
      data['ValidationDetails'] = this.validationDetails.toJson();
    }
    return data;
  }
}
