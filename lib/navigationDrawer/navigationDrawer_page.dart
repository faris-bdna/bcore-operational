
import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/data/database.dart';
import 'package:bcore_inventory_management/loclization/AppLanguage.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bcore_inventory_management/screens/Dashboard/dashboard_page.dart';
import 'package:bcore_inventory_management/screens/Settings/settings_page.dart';
import 'package:bcore_inventory_management/screens/login/initialLogin/initialLogin_page.dart';
import 'package:bcore_inventory_management/screens/login/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavigationDrawer extends StatelessWidget {
  final BuildContext context;
  final BCRepository repository;
  final AppLanguage appLanguage;
  final Database database;

  NavigationDrawer(
      {Key key, @required this.context, this.repository, this.appLanguage, this.database})
      : super(key: key);
  @override
  Widget build(newContext) {
    return Drawer(
      elevation: 1.0,
      child: Container(
        color: BCAppTheme().drawerBg,
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    color: BCAppTheme().drawerBg,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 15.0, bottom: 10.0),
                      child: Text(
                        "MAIN MENU",
                        style: new TextStyle(
                          fontSize: 15.0,
                          letterSpacing: 2.0,
                          color: BCAppTheme().subTextColor,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 5.0, bottom: 10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      //color: Colors.white,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                              dense: true,
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/module/module.png'),
                              ),
                              trailing: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/arrow/arrow.png'),
                              ),
                              title: Text(
                                'Dashboard',
                                style: TextStyle(
                                  color: BCAppTheme().textColor,
                                ),
                              ),
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Dashboard(appLanguage: appLanguage,
                                          repository: repository,database: database,)));
                              },
                            ),

                            Padding(
                              padding: const EdgeInsets.only(left: 4, right: 4),
                              child: Divider(height: 1.0),
                            ),

                            ListTile(
                              dense: true,
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/settings/settings.png'),
                              ),
                              trailing: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/arrow/arrow.png'),
                              ),
                              title: Text(
                                'Settings',
                                style: TextStyle(
                                  color: BCAppTheme().textColor,
                                ),
                              ),
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Settings(appLanguage: appLanguage,
                                          repository: repository,database: database,)));
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 4, right: 4),
                              child: Divider(height: 1.0),
                            ),

                            ListTile(
                              dense: true,
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/signout/signout.png'),
                              ),
                              trailing: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                    'assets/common/side_menu/arrow/arrow.png'),
                              ),
                              title: Text(
                                'Sign Out',
                                style: TextStyle(
                                  color: BCAppTheme().textColor,
                                ),
                              ),
                              onTap: () async {
                                clearCacheAndNavigate();
                              },
                            ),
                          ]),
                    ),
                  )
                ],
              ),
            ),
            Container(
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Column(
                      children: <Widget>[
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(Constants().version, // TODO: change the version number on every build time.
                              style: new TextStyle(
                                fontSize: 15.0,
                                letterSpacing: 1.0,
                                color: BCAppTheme().subTextColor,
                              )),
                        )
                      ],
                    ))),
          ],
        ),
      ),
    );
  }

  clearCacheAndNavigate() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("Token");

    if (prefs.containsKey("hasConfiguredPin")) {
      if (prefs.getBool("hasConfiguredPin")) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Login()));
      } else{
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => InitialLogin()));
      }
    } else{
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => InitialLogin()));
    }


  }
}
