

import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'database.g.dart';

class ItemBarcode extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get Text => text().withLength(min: 1)();

  IntColumn get ProductGroupId => integer()();

  TextColumn get ItemBarcodeId => text().withLength(min: 1).nullable()();

  IntColumn get ProductId => integer()();

  TextColumn get ProductName => text().withLength(max: 250).nullable()();

  IntColumn get ItemId => integer()();

  TextColumn get ItemCode => text().withLength(max: 50)();

  TextColumn get ItemName => text().withLength(max: 500)();

  IntColumn get ItemPackingId => integer()();

  IntColumn get MajorPackId => integer().nullable()();

  IntColumn get BasePackId => integer().nullable()();

  TextColumn get PkgQty => text().withLength(max: 50).nullable()();

  TextColumn get PackingDesc => text().nullable()();

  IntColumn get ScaleGroupId => integer().nullable()();

  IntColumn get BrandId => integer().nullable()();

  IntColumn get OriginCountryId => integer().nullable()();

  IntColumn get ProductStockTypeId => integer().nullable()();

  IntColumn get MinShelfLife => integer().nullable()();

  IntColumn get TotalShelfLife => integer().nullable()();

  IntColumn get IsSalesAllowed => integer().nullable()();

  IntColumn get StockAllocTypeId => integer().nullable()();

  IntColumn get IsTaxApplicable => integer().nullable()();

  IntColumn get ItemStatus => integer().nullable()();

  BoolColumn get pur_restriction =>
      boolean().withDefault(Constant(false)).nullable()();

  IntColumn get IsPurchaseAllowed => integer().nullable()();

  TextColumn get ExcessQty => text().withLength(max: 50).nullable()();

  BoolColumn get IsMinOrderQtyApp =>
      boolean().withDefault(Constant(false))();

  TextColumn get MinOrderQty => text().withLength(max: 50).nullable()();

  TextColumn get SuppItemCode => text().withLength(max: 50).nullable()();

  IntColumn get SuppTypeId => integer().nullable()();

  TextColumn get TaxPercent => text().withLength(max: 50).nullable()();

  IntColumn get IsBatchEnabled => integer().nullable()();

  IntColumn get HasPurchaseGroupAccess => integer().nullable()();

  IntColumn get StockBasedItem => integer().nullable()();

  IntColumn get ConsignmentItem => integer().nullable()();

  TextColumn get LastPurchaseQty => text().withLength(max: 50)();

  TextColumn get LastPurchaseDate => text().withLength(max: 50).nullable()();

  TextColumn get LastReceivedAmount => text().withLength(max: 50)();

  IntColumn get LastReceivedSupplier => integer().nullable()();

  TextColumn get AverageCost => text().withLength(max: 50).nullable()();

  TextColumn get AvailableQty => text().withLength(max: 50).nullable()();

  TextColumn get NetCost => text().withLength(max: 50).nullable()();
}

class ItemPacking extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get packing_text => text().withLength(min: 1, max: 250)();

  TextColumn get packing_value => text().withLength(min: 1)();

  IntColumn get item_id => integer()();
}

class ItemBatch extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get batch_text => text().withLength(min: 1, max: 250)();
  TextColumn get batch_value => text().withLength(min: 1)();
  IntColumn get item_id => integer()();
  TextColumn get prod_date => text().withLength(max: 250).nullable()();
  TextColumn get expiry_date => text().withLength(max: 250).nullable()();
}

class ProductBrand extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get brand_id => text().withLength(min: 1)();
  TextColumn get brand_name => text().withLength(min: 1, max: 250).nullable()();
}

class CountryOrigin extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get country_id => text().withLength(min: 1)();
  TextColumn get country_name => text().withLength(min: 1, max: 250)();
}

class DraftTransaction extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get trans_id => text().withLength(min: 1)();
  TextColumn get trans_type => text().withLength(min: 1, max: 250)();
  TextColumn get trans_details => text().withLength(min: 1, max: 5000)();
  TextColumn get stepper_data1 => text().withLength(min: 1, max: 250)();
  TextColumn get stepper_data2 => text().withLength(min: 1, max: 250)();
  TextColumn get stepper_data3 => text().withLength(min: 1, max: 250)();
  TextColumn get stepper_data4 => text().withLength(min: 1, max: 250)();
  TextColumn get stepper_data5 => text().withLength(min: 1, max: 250)();
  TextColumn get stepper_data6 => text().withLength(min: 1, max: 250)();
}


class MaterialRequest extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get req_date_temp => text().withLength(max: 100)();
  IntColumn get from_comp_id => integer()();
  IntColumn get from_div_id => integer()();
  IntColumn get from_loc_id => integer()();
  IntColumn get from_dept_id => integer()();
  IntColumn get from_proj_id => integer()();
  IntColumn get to_comp_id => integer()();
  IntColumn get to_div_id => integer()();
  IntColumn get to_loc_id => integer()();
  IntColumn get to_dept_id => integer()();
  TextColumn get exp_rec_date_temp => text().withLength(max: 100)();
  TextColumn get exp_rec_date_time => text().withLength(max: 50)();
  IntColumn get del_loc_id => integer()();
  IntColumn get del_priority_id => integer()();
  TextColumn get comments => text().withLength(max: 250).nullable()();
  IntColumn get req_by => integer()();
  TextColumn get req_date => text().withLength(max: 50)();
  TextColumn get exp_rec_date => text().withLength(max: 50)();
}

class MaterialRequestDetails extends Table {
  IntColumn get id => integer()();
  IntColumn get line_num => integer()();
  BoolColumn get is_new_item =>
      boolean().withDefault(Constant(true))();
  TextColumn get item_code => text().withLength(max: 100)();
  TextColumn get item_name => text().withLength(max: 250)();
  TextColumn get item_desc => text().withLength(max: 250)();
  TextColumn get item_barcode => text().withLength(max: 20)();
  IntColumn get prod_id => integer()();
  IntColumn get item_id => integer()();
  IntColumn get prod_pkg_id => integer()();
  IntColumn get prod_cls_id => integer()();
  IntColumn get brand_id => integer()();
  IntColumn get country_id => integer()();
  TextColumn get req_qty => text().withLength(min: 1, max: 50)();
  TextColumn get comments => text().withLength(max: 250)();
  IntColumn get app_status => integer()();
  BoolColumn get is_sub_allowed =>
      boolean().withDefault(Constant(true))();
  TextColumn get unit_cost => text().withLength(max: 50).nullable()();
  TextColumn get net_cost => text().withLength(max: 50).nullable()();
  TextColumn get total_cost => text().withLength(max: 50).nullable()();

  TextColumn get batch_text => text().withLength(min: 1, max: 20)();

  TextColumn get batch_value => text().withLength(min: 5, max: 13)();

  TextColumn get prod_date => text().withLength(max: 250)();

  TextColumn get expiry_date => text().withLength(max: 250)();
}


@UseMoor(
    tables: [ItemBarcode, ItemPacking, ItemBatch, CountryOrigin, ProductBrand, DraftTransaction, MaterialRequest, MaterialRequestDetails],
    daos: [ItemBarcodeDao, ItemPackingDao, ItemBatchDao, CountryOriginDao, ProductBrandDao, DraftTransactionDao, MaterialRequestDao, MaterialRequestDetailsDao])
class Database extends _$Database {
  Database()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'bcore_db.sqlite', logStatements: true));

  @override
  int get schemaVersion => 1;
}

@UseDao(tables: [ItemBarcode])
class ItemBarcodeDao extends DatabaseAccessor<Database>
    with _$ItemBarcodeDaoMixin {
  final Database db;

  ItemBarcodeDao(this.db) : super(db);

  Future<List<ItemBarcodeData>> getAllItemBarcode() => select(itemBarcode).get();

  // Get item by barcode
  Future<List<ItemBarcodeData>> getItemByBarcode(String barcode) {
    return (select(itemBarcode)..where((item) => item.Text.equals(barcode))).get();

  }

  // Get item by item code
  Future<List<ItemBarcodeData>> getItemByItemCode(String itemCode) {
    return (select(itemBarcode)..where((item) => item.ItemCode.equals(itemCode.trim()))).get();
  }

  // Get item by packing
  Future<List<ItemBarcodeData>> getItemByPacking(String itemId, String prodPkgId) {
    return (select(itemBarcode)..where((item) => item.ItemId.equals(int.parse(itemId)) & item.ItemPackingId.equals(int.parse(prodPkgId)))).get();
  }

  // Get item by filtering
  Future<List<ItemBarcodeData>> getItemByFilter(String brandId,
      String originId, String productGrpId) {

      final _select = select(itemBarcode);
      if (brandId != "") {
        _select..where((item) => item.BrandId.equals(int.parse(brandId)));
      }
      if (originId != "") {
        _select..where((item) => item.OriginCountryId.equals(int.parse(originId)));
      }
      if (productGrpId != "") {
        _select..where((item) => item.ProductGroupId.equals(int.parse(productGrpId)));
      }

      return _select.get();
  }


  // Insert item into barcode master
  Future insertItemBarcode(Insertable<ItemBarcodeData> item) =>
      into(itemBarcode).insert(item);


  // Update item into barcode master
  Future updateItemBarcode(Insertable<ItemBarcodeData> item) =>
      update(itemBarcode).replace(item);

  // Delete item from barcode master
  Future deleteItemBarcode(Insertable<ItemBarcodeData> item) =>
      delete(itemBarcode).delete(item);

  deleteAllBarCode() {
    try{
      return delete(itemBarcode).go();//like this
    }catch (e){
      print("error is $e");
    }
  }
}

@UseDao(tables: [ItemPacking])
class ItemPackingDao extends DatabaseAccessor<Database>
    with _$ItemPackingDaoMixin {
  final Database db;

  ItemPackingDao(this.db) : super(db);

  Future<List<ItemPackingData>> getAllItemPacking() =>
      select(itemPacking).get();

  // Get packing list by item id
  Future<List<ItemPackingData>> getPackingByItemId(String itemId) {
    return (select(itemPacking)..where((packing) => packing.item_id.equals(int.parse(itemId)))).get();
  }

  // Get packing list by id
  Future<List<ItemPackingData>> getPackingById(String packId, String itemId) {
    return (select(itemPacking)..where((packing) => packing.packing_value.equals(packId) & packing.item_id.equals(int.parse(itemId)))).get();
  }

  Future insertItemPacking(Insertable<ItemPackingData> item) =>
      into(itemPacking).insert(item);

  Future updateItemPacking(Insertable<ItemPackingData> item) =>
      update(itemPacking).replace(item);

  Future deleteItemPacking(Insertable<ItemPackingData> item) =>
      delete(itemPacking).delete(item);
  deleteAllPkg() {
    try{
      return delete(itemPacking).go();//like this
    }catch (e){
      print("error is $e");
    }
  }
}

@UseDao(tables: [ItemBatch])
class ItemBatchDao extends DatabaseAccessor<Database>
    with _$ItemBatchDaoMixin {
  final Database db;

  ItemBatchDao(this.db) : super(db);

  Future<List<ItemBatchData>> getAllItemBatch() => select(itemBatch).get();

  Future insertItemBatch(Insertable<ItemBatchData> item) =>
      into(itemBatch).insert(item);

  Future updateItemBatch(Insertable<ItemBatchData> item) =>
      update(itemBatch).replace(item);

  Future deleteItemBatch(Insertable<ItemBatchData> item) =>
      delete(itemBatch).delete(item);

  deleteAllBatch() {
    try{
      return delete(itemBatch).go();//like this
    }catch (e){
      print("error is $e");
    }
  }

  // Get Batch nos from
  Future<List<ItemBatchData>> getItemBatchByItemId(int itemId) {
    return (select(itemBatch)..where((item) => item.item_id.equals(itemId))).get();
  }
}

@UseDao(tables: [CountryOrigin])
class CountryOriginDao extends DatabaseAccessor<Database>
    with _$CountryOriginDaoMixin {
  final Database db;
  CountryOriginDao(this.db) : super(db);

  Future<List<CountryOriginData>> getAllCountryOrigin() => select(countryOrigin).get();

  // Get country name by id
  Future<List<CountryOriginData>> getCountryNameById(String id) {
    return (select(countryOrigin)..where((country) => country.country_id.equals(id))).get();
  }


  Future insertCountryOrigin(Insertable<CountryOriginData> item) =>
      into(countryOrigin).insert(item);
  Future updateCountryOrigin(Insertable<CountryOriginData> item) =>
      update(countryOrigin).replace(item);
  Future deleteCountryOrigin(Insertable<CountryOriginData> item) =>
      delete(countryOrigin).delete(item);

  deleteAllCountry() {
    try{
      return delete(countryOrigin).go();//like this
    }catch (e){
      print("error is $e");
    }
  }
}

@UseDao(tables: [ProductBrand])
class ProductBrandDao extends DatabaseAccessor<Database>
    with _$ProductBrandDaoMixin {
  final Database db;
  ProductBrandDao(this.db) : super(db);

  Future<List<ProductBrandData>> getAllProductBrand() => select(productBrand).get();

  // Get product brand name by id
  Future<List<ProductBrandData>> getBrandNameById(String id) {
    return (select(productBrand)..where((brand) => brand.brand_id.equals(id))).get();
  }

  Future insertProductBrand(Insertable<ProductBrandData> item) =>
      into(productBrand).insert(item);
  Future updateProductBrand(Insertable<ProductBrandData> item) =>
      update(productBrand).replace(item);
  Future deleteProductBrand(Insertable<ProductBrandData> item) =>
      delete(productBrand).delete(item);
  deleteAllBrand() {
    try{
      return delete(productBrand).go();//like this
    }catch (e){
      print("error is $e");
    }
  }
}

@UseDao(tables: [DraftTransaction])
class DraftTransactionDao extends DatabaseAccessor<Database>
    with _$DraftTransactionDaoMixin {
  final Database db;
  DraftTransactionDao(this.db) : super(db);

  Future<List<DraftTransactionData>>  getAllDraftTransaction() => select(draftTransaction).get();

  Future<List<DraftTransactionData>> getDraftId(String type) {
    return (select(draftTransaction)..where((draft) => draft.trans_type.equals(type))..orderBy([(draft) => OrderingTerm(expression: draft.id, mode: OrderingMode.desc)])).get();
  }

  /// Get draft trans by id
  Future<List<DraftTransactionData>> getDraftTransactionById(String id, String type) {
    return (select(draftTransaction)..where((draft) => draft.trans_id.equals(id) & draft.trans_type.equals(type))).get();
  }

  Future<List<DraftTransactionData>> getDraftTransactionByType( String type) {
    return (select(draftTransaction)..where((draft) =>  draft.trans_type.equals(type))).get();
  }

  Future insertDraftTransaction(Insertable<DraftTransactionData> item) =>
      into(draftTransaction).insert(item); /// asd
  Future updateDraftTransaction(Insertable<DraftTransactionData> item) =>
      update(draftTransaction).replace(item);
  Future deleteDraftTransaction(Insertable<DraftTransactionData> item) =>
      delete(draftTransaction).delete(item);
}

@UseDao(tables: [MaterialRequest])
class MaterialRequestDao extends DatabaseAccessor<Database>
    with _$MaterialRequestDaoMixin {
  final Database db;
  MaterialRequestDao(this.db) : super(db);

  Future<List<MaterialRequestData>> getAllItemBarcode() =>
      select(materialRequest).get();

  Future<List<MaterialRequestData>> getId(String compId, String divId, String locId, String deptId) {
    return (select(materialRequest)
      ..where((materialRequest) => materialRequest.to_comp_id.equals(int.parse(compId)) & materialRequest.to_div_id.equals(int.parse(divId)) & materialRequest.to_loc_id.equals(int.parse(locId)) & materialRequest.to_dept_id.equals(int.parse(deptId)))
      ..orderBy([(materialRequest) => OrderingTerm(expression: materialRequest.id)])).get();
  }

  // Insert item into material request
  Future<int> insertMaterialRequest(Insertable<MaterialRequestData> matReq) =>
      into(materialRequest).insert(matReq);

  // Update item into material request
  Future updateMaterialRequest(Insertable<MaterialRequestData> matReq) =>
      update(materialRequest).replace(matReq);

  // Delete item from material request
  Future deleteMaterialRequest(Insertable<MaterialRequestData> matReq) =>
      delete(materialRequest).delete(matReq);
}

@UseDao(tables: [MaterialRequestDetails])
class MaterialRequestDetailsDao extends DatabaseAccessor<Database>
    with _$MaterialRequestDetailsDaoMixin {
  final Database db;
  MaterialRequestDetailsDao(this.db) : super(db);

  Future<List<MaterialRequestDetail>> getAllMaterialRequestDetails() =>
      select(materialRequestDetails).get();

  // Get material request details by id
  Future<List<MaterialRequestDetail>> getMaterialRequestDetails(int matReqId) {
    return (select(materialRequestDetails)..where((materialRequestDetails) => materialRequestDetails._id.equals(matReqId))).get();
  }

  Future<MaterialRequestDetail> getLastLineNumber(int matReqId) {
    return (select(materialRequestDetails)
      ..where((materialRequestDetails) => materialRequestDetails._id.equals(matReqId))
      ..orderBy([(detail) => OrderingTerm(expression: detail.line_num, mode: OrderingMode.desc)])).getSingle();
  }

  // Insert item into material request details
  Future insertMaterialRequestDetails(Insertable<MaterialRequestDetail> matReqDetail) =>
      into(materialRequestDetails).insert(matReqDetail);

  // Update item into material request details
  Future updateMaterialRequestDetails(Insertable<MaterialRequestDetail> matReqDetail) =>
      update(materialRequestDetails).replace(matReqDetail);

  // Delete item from material request details
  Future deleteMaterialRequestDetails(Insertable<MaterialRequestDetail> matReqDetail) =>
      delete(materialRequestDetails).delete(matReqDetail);
}
