import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';

class Constants {

  // final baseUrl = 'https://devapi.businesscore.ae';
  // String version = 'VERSION (DEV) 1.1.0';

 final baseUrl = 'https://qaapi.businesscore.ae';
 String version = 'VERSION (QA) 4.2.0';

 // final baseUrl = 'https://uatapi.businesscore.ae';
 // String version = 'VERSION (UAT) 7.0.2';

 // final baseUrl = 'https://api.businesscore.ae';
 // String version = 'VERSION 1.4.0';

 // final baseUrl = 'http://192.169.0.200:1002';

  static String accessMode = "Mode";

  static String transactionIdKey = "TransactionId";

  static String divisionRequiredKey = "IsDivisionRequired";

  static String departmentRequiredKey = "IsDepartmentRequired";

  static String backDateAllowedKey = "BackDateAllowed";

  static String isSuppContractPriceKey = "IsSuppContractPrice";

  static String isInitialNavigationToDashboardKey = "isInitialNavigationToDashboard";

  static bool isTransEditMode = false;

  /// Messages
  static String noItemAddedMessage = "No items added !";
  static String transactionDateValidationMsg = 'Select transaction date';
  static String expReceiveDateValidationMsg = 'Select Expected Receiving Date';
  static String priorityValidationMsg = 'Select Priority';
  static String companyValidationMsg = 'Select a Company';
  static String divisionValidationMsg = 'Select a Division';
  static String locationValidationMsg = 'Select a Location';
  static String departmentValidationMsg = 'Select a Department';
  static String refPlaceHolderText = "Select Reference";
  static String transactionClearMsg = "Your transaction details will be cleared. Do you want to continue ?";
  static String alertMsgTitle = "Alert";
  static String sele = "Alert";
  static String referenceValidationMsg ="Please select a Reference";
  static String prfIsIBDSync = "prfIsIBDSync";///Item Barcode Data Sync
 static String prfIsIBDDate = "prfIsIBDDate";///Item Barcode Data Last Sync Date
 static String prfIsIPDSync = "prfIsIPDSync";///Item Packing Data Sync
 static String prfIsIPDDate = "prfIsIPDDate";///Item Packing Data Last Sync Date
 static String prfIsIBHDSync = "prfIsIBHDSync";///Item Batch Data Sync
 static String prfIsIBHDDate = "prfIsIBHDDate";///Item Batch Data Last Sync Date
 static String prfIsCOODSync = "prfIsCOODSync";///Country Of Origin Data Sync
 static String prfIsCOODDate = "prfIsCOODDate";///Country Of Origin Data Last Sync Date
 static String prfIsBRDDSync = "prfIsBRDDSync";///Brand Data Sync
 static String prfIsBRDDDate = "prfIsBRDDDate";///Brand Data Last Sync Date
 static PrinterBluetooth bc_selectedPrinter;
}

enum ScmMasters {
  ProductStockType,
  ProductBrand,
  CountryOfOrigin,
  DeliveryPriority,
  ClientUsers,
  GrnType,
  ServiceCategory,
  Currency,
  Unit,
  PurchaseReturnType,
  PriceType,
  ConsignmentType,
  JobOrderType,
  ModuleSettings
}
