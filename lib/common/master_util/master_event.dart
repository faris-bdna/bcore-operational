part of 'master_bloc.dart';

abstract class MasterEvent extends Equatable {
  const MasterEvent();
}

class GetItemBarcodeMaster extends MasterEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String recordCount;
  final String lastFetchDate;

  const GetItemBarcodeMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate];

  @override
  String toString() => 'GetItemBarcodeMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'recordCount: $recordCount,'
      'lastFetchDate: $lastFetchDate}';
}

class GetItemPackingMaster extends MasterEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String recordCount;
  final String lastFetchDate;

  const GetItemPackingMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate];

  @override
  String toString() => 'GetItemPackingMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'recordCount: $recordCount,'
      'lastFetchDate: $lastFetchDate}';
}

class GetItemBatchMaster extends MasterEvent {
  final String companyId;
  final String divisionId;
  final String locationId;
  final String supplierId;
  final String recordCount;
  final String lastFetchDate;

  const GetItemBatchMaster({
    @required this.companyId,
    @required this.divisionId,
    @required this.locationId,
    @required this.supplierId,
    @required this.recordCount,
    this.lastFetchDate,
  });

  @override
  List<Object> get props =>
      [companyId, divisionId, locationId, supplierId, lastFetchDate];

  @override
  String toString() => 'GetItemBatchMaster { '
      'companyId: $companyId, '
      'divisionId: $divisionId, '
      'locationId: $locationId, '
      'supplierId: $supplierId, '
      'recordCount: $recordCount,'
      'lastFetchDate: $lastFetchDate}';
}

class GetCommonMaster extends MasterEvent {
  final ScmMasters scmMaster;
  const GetCommonMaster({@required this.scmMaster});

  @override
  List<Object> get props => [scmMaster];

  @override
  String toString() => 'GetCommonMaster { ''scmMaster: $scmMaster}';
}

// class GetCountryOfOriginMaster extends MasterEvent {
//   final ScmMasters scmMaster;
//   const GetCountryOfOriginMaster({@required this.scmMaster});
//
//   @override
//   List<Object> get props => [scmMaster];
//
//   @override
//   String toString() => 'GetCountryOfOriginMaster { ''scmMaster: $scmMaster}';
// }
//
// class GetBrandMaster extends MasterEvent {
//   final ScmMasters scmMaster;
//   const GetCountryOfOriginMaster({@required this.scmMaster});
//
//   @override
//   List<Object> get props => [scmMaster];
//
//   @override
//   String toString() => 'GetCountryOfOriginMaster { ''scmMaster: $scmMaster}';
// }
