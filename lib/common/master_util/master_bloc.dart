import 'dart:async';

import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/models/masters/getCommon_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBarcode_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemBatch_resp.dart';
import 'package:bcore_inventory_management/models/masters/getItemPacking_resp.dart';
import 'package:bcore_inventory_management/repository/bCore_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'master_event.dart';
part 'master_state.dart';

class MasterBloc extends Bloc<MasterEvent, MasterState> {
  final BCRepository bcRepository;

  MasterBloc({@required this.bcRepository})
      : assert(bcRepository != null),
        super(GetItemBarcodeMasterInitial());

  @override
  Stream<MasterState> mapEventToState(MasterEvent event) async* {

    if (event is GetItemBarcodeMaster) {
      yield GetItemBarcodeMasterInProgress();
      try {
        final GetItemBarcodeMasterResp getItemBarcodeMasterResp =
        await bcRepository.getItemBarcodeMaster(
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            supplierId: event.supplierId,
            recordCount: event.recordCount,
            lastFetchDate: event.lastFetchDate);
        yield GetItemBarcodeMasterLoaded(
            getItemBarcodeResp: getItemBarcodeMasterResp);
      } catch (error) {
        yield GetItemBarcodeMasterFailure(error: error.toString());
      }
    }

    else if (event is GetItemBatchMaster) {
      yield GetItemBatchMasterInProgress();
      try {
        final GetItemBatchMasterResp getItemBatchMasterResp =
        await bcRepository.getItemBatchMaster(
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            supplierId: event.supplierId,
            recordCount: event.recordCount,
            lastFetchDate: event.lastFetchDate);
        yield GetItemBatchMasterLoaded(
            getItemBatchResp: getItemBatchMasterResp);
      } catch (error) {
        yield GetItemBatchMasterFailure(error: error.toString());
      }
    }

    else if (event is GetItemPackingMaster) {
      yield GetItemPackingMasterInProgress();
      try {
        final GetItemPackingMasterResp getItemPackingMasterResp =
        await bcRepository.getItemPackingMaster(
            companyId: event.companyId,
            divisionId: event.divisionId,
            locationId: event.locationId,
            supplierId: event.supplierId,
            recordCount: event.recordCount,
            lastFetchDate: event.lastFetchDate);
        yield GetItemPackingMasterLoaded(
            getItemPackingResp: getItemPackingMasterResp);
      } catch (error) {
        yield GetItemPackingMasterFailure(error: error.toString());
      }
    }

    else if (event is GetCommonMaster) {
      yield GetCommonMasterInProgress();
      try {
        final GetCommonResp getItemPackingMasterResp =
        await bcRepository.getCommonMaster(event.scmMaster);
        if(event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterLoaded(
              getCommonResp: getItemPackingMasterResp, scmMaster: event.scmMaster);
        } else if(event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterLoaded(
              getCommonResp: getItemPackingMasterResp, scmMaster: event.scmMaster);
        }
      } catch (error) {
        if(event.scmMaster == ScmMasters.CountryOfOrigin) {
          yield GetCountryOfOriginMasterFailure(error: error.toString());
        } else if(event.scmMaster == ScmMasters.ProductBrand) {
          yield GetProductBrandMasterFailure(error: error.toString());
        }
      }
    }
  }
}