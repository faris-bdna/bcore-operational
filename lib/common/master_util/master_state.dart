part of 'master_bloc.dart';

abstract class MasterState extends Equatable {
  const MasterState();

  @override
  List<Object> get props => [];
}

class GetItemBarcodeMasterInitial extends MasterState {}

class GetItemBarcodeMasterInProgress extends MasterState {}

class GetItemBarcodeMasterLoaded extends MasterState {
  final GetItemBarcodeMasterResp getItemBarcodeResp;

  const GetItemBarcodeMasterLoaded({@required this.getItemBarcodeResp})
      : assert(getItemBarcodeResp != null);

  @override
  List<Object> get props => [getItemBarcodeResp];
}

class GetItemBarcodeMasterFailure extends MasterState {
  final String error;

  const GetItemBarcodeMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemBarcodeMasterFailure { error: $error }';
}

class GetItemPackingMasterInitial extends MasterState {}

class GetItemPackingMasterInProgress extends MasterState {}

class GetItemPackingMasterLoaded extends MasterState {
  final GetItemPackingMasterResp getItemPackingResp;

  const GetItemPackingMasterLoaded({@required this.getItemPackingResp})
      : assert(getItemPackingResp != null);

  @override
  List<Object> get props => [getItemPackingResp];
}

class GetItemPackingMasterFailure extends MasterState {
  final String error;

  const GetItemPackingMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemPackingMasterFailure { error: $error }';
}

class GetItemBatchMasterInitial extends MasterState {}

class GetItemBatchMasterInProgress extends MasterState {}

class GetItemBatchMasterLoaded extends MasterState {
  final GetItemBatchMasterResp getItemBatchResp;

  const GetItemBatchMasterLoaded({@required this.getItemBatchResp})
      : assert(getItemBatchResp != null);

  @override
  List<Object> get props => [getItemBatchResp];
}

class GetItemBatchMasterFailure extends MasterState {
  final String error;

  const GetItemBatchMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetItemBatchMasterFailure { error: $error }';
}

// Get country of origin master
class GetCommonMasterInitial extends MasterState {}

class GetCommonMasterInProgress extends MasterState {}

class GetCommonMasterLoaded extends MasterState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetCommonMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetCommonMasterFailure extends MasterState {
  final String error;

  const GetCommonMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCommonMasterFailure { error: $error }';
}

// Get country of origin master
class GetCountryOfOriginMasterInitial extends MasterState {}

class GetCountryOfOriginMasterInProgress extends MasterState {}

class GetCountryOfOriginMasterLoaded extends MasterState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetCountryOfOriginMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetCountryOfOriginMasterFailure extends MasterState {
  final String error;

  const GetCountryOfOriginMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetCountryOfOriginMasterFailure { error: $error }';
}

// Get product brand master
class GetProductBrandMasterInitial extends MasterState {}

class GetProductBrandMasterInProgress extends MasterState {}

class GetProductBrandMasterLoaded extends MasterState {
  final GetCommonResp getCommonResp;
  final ScmMasters scmMaster;

  const GetProductBrandMasterLoaded({@required this.getCommonResp, @required this.scmMaster})
      : assert(getCommonResp != null );

  @override
  List<Object> get props => [getCommonResp];
}

class GetProductBrandMasterFailure extends MasterState {
  final String error;

  const GetProductBrandMasterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GetProductBrandMasterFailure { error: $error }';
}
