import 'package:flutter/material.dart';


Size displaySize(BuildContext context) {
  debugPrint('Size = ' + MediaQuery.of(context).size.toString());
  return MediaQuery.of(context).size;
}

EdgeInsets displayPadding(BuildContext context) {
  debugPrint('Width = ' + MediaQuery.of(context).padding.toString());
  return MediaQuery.of(context).padding;
}

double displayHeight(BuildContext context) {
  debugPrint('Height = ' + displaySize(context).height.toString());
  return displaySize(context).height;
}

double displayWidth(BuildContext context) {
  debugPrint('Width = ' + displaySize(context).width.toString());
  return displaySize(context).width;
}

double safeDisplayHeight(BuildContext context) {
  return displayHeight(context) - displayPadding(context).top -
      displayPadding(context).bottom;
}

double safeDisplayWidth(BuildContext context) {
  return displayWidth(context) - displayPadding(context).left -
      displayPadding(context).right;
}