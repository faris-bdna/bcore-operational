import 'dart:typed_data';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as img;


class PrintUtil {

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();

  bcPrint({PrinterBluetooth printer,String brand, String package,
      String barcode, String itemName, String itemCode, String itemPrice}) async {

    printerManager.selectPrinter(printer);

    // TODO Don't forget to choose printer's paper
    const PaperSize paper = PaperSize.mm80;


  }
  printRecipt(String printer, List<int> content) async {


    printerManager.selectPrinter(Constants.bc_selectedPrinter);

    final PosPrintResult res =
    await printerManager.printTicket(content);

  }
   findPrinter(String printername){
     printerManager.scanResults.listen((devices) async {

       devices.forEach((printer) {
           print(printer);
           if(printername == ""){
             return devices[0];
           }
           else if(printer.address == printername){
         return printer;
       }
       }
       );
     });
   }

}