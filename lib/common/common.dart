import 'package:bcore_inventory_management/app_config/bcAppTheme.dart';
import 'package:bcore_inventory_management/constant/constant.dart';
import 'package:bcore_inventory_management/models/view/fromAddress.dart';

import 'package:flutter/material.dart';
import 'package:moor/moor.dart' hide Column;
import 'package:numberpicker/numberpicker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class Common {

  /// Alerts


  showMessage({BuildContext context, String message}){
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message), duration: Duration(milliseconds: 500)));
  }

  showAlert({BuildContext context, String message}){
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message), duration: Duration(milliseconds: 1000)));
  }

  showAlertMessage(
      {@required BuildContext context,
        String title,
        String message,
        @required VoidCallback okFunction}) {
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: SingleChildScrollView(child: Text(message)),
      actions: <Widget>[
        FlatButton(
            color: BCAppTheme().primaryColor,
            child: Text("OK", style: TextStyle(color: BCAppTheme().secondaryColor)),
            onPressed: okFunction ?? (){Navigator.pop(context);}),
      ],
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return alert;
      },
    );
  }

  showAlertMessageWithAction(
      {@required BuildContext context,
        String title,
        String message,
        String okButtonTitle,
        VoidCallback okFunction,
        @required String actionButtonTitle,
        @required VoidCallback actionFunction}) {
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
            color: BCAppTheme().secondaryColor,
            child: Text(actionButtonTitle,
                style: TextStyle(
                    color: BCAppTheme().headingTextColor,
                    fontWeight: FontWeight.bold)),
            onPressed: actionFunction),
        FlatButton(
            color: BCAppTheme().secondaryColor,
            child: Text(okButtonTitle ?? "Ok",
                style: TextStyle(
                    color: BCAppTheme().headingTextColor,
                    fontWeight: FontWeight.bold)),
            onPressed: okFunction ?? (){ Navigator.pop(context); }),
      ],
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext myContext) {
        return alert;
      },
    );
  }

  showLoader(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(
              backgroundColor: BCAppTheme().secondaryColor),
          Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showDownloadStatus(BuildContext context,String status) {
    AlertDialog alert = AlertDialog(
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
              margin: EdgeInsets.only(left: 8, right: 8,bottom: 8),
              child: Text(status)),
          LinearProgressIndicator(minHeight: 5.0,
              backgroundColor: BCAppTheme().secondaryColor),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  bool validatePassword(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  /// Preferences

  setTransactionId(int transactionId) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt(Constants.transactionIdKey, transactionId);
  }

  getTransactionId() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getInt(Constants.transactionIdKey) ?? 0;
  }

  setDivisionRequired(bool isDivisionRequired) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(Constants.divisionRequiredKey, isDivisionRequired);
  }

  getDivisionRequired() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(Constants.divisionRequiredKey) ?? false;
  }

  setDepartmentRequired(bool isDepartmentRequired) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(Constants.departmentRequiredKey, isDepartmentRequired);
  }

  getDepartmentRequired() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(Constants.departmentRequiredKey) ?? false;
  }

  setBackDateAllowed(bool isBackDateAllowed) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(Constants.backDateAllowedKey, isBackDateAllowed);
  }

  getBackDateAllowed() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(Constants.backDateAllowedKey) ?? false;
  }

  setSuppContractPriceStatus(bool isBackDateAllowed) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(Constants.isSuppContractPriceKey, isBackDateAllowed);
  }

  getSuppContractPriceStatus() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(Constants.isSuppContractPriceKey) ?? false;
  }

  setInitialNavigationToDashboardStatus(bool isInitial) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool(Constants.isInitialNavigationToDashboardKey, isInitial);
  }

  /// This is used to identify whether the user is navigating to the Dashboard screen for the first time or not
  hasInitialNavigationToDashboardCompleted() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(Constants.isInitialNavigationToDashboardKey) ?? false;
  }

  /// Toast

  // showStepperValidation({GlobalKey<ScaffoldState> scaffoldKey, String message}){
  //   scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message), duration: Duration(milliseconds: 500)));
  //
  // }

  showStepperValidation({GlobalKey<ScaffoldState> scaffoldKey, String message}){
    showTopSnackBar(
      scaffoldKey.currentContext,
      CustomSnackBar.info(
        message: message,
        backgroundColor: BCAppTheme().headingTextColor,
      ),
      displayDuration: Duration(milliseconds: 500),
    );

  }



  /// Decoration

  /// Old
  getInputDecoration(bool isMandatory, String hintText, {IconData icon = Icons.calendar_today,Function iconPress}) {
    return InputDecoration(
      isDense: true,
      border: OutlineInputBorder(),
      hintText: hintText,
      hintStyle: TextStyle(color: BCAppTheme().grayColor, fontSize: 14),
      filled: true,
      fillColor: Colors.white70,
      suffixIcon:
      GestureDetector(
        onTap: iconPress,
          child: Icon(icon, color: BCAppTheme().headingTextColor)),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color: isMandatory ?
        BCAppTheme().redColor:BCAppTheme().headingTextColor, width: 0.5),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color:isMandatory ?
        BCAppTheme().redColor:BCAppTheme().headingTextColor, width: 0.5),
      ),
    );
  }

/// Revamping
  getBCoreID({bool validityStatus, String hintText, IconData icon}) {
    return InputDecoration(
      isDense: true,
      border: OutlineInputBorder(),
      hintText: hintText,
      hintStyle: TextStyle(color: BCAppTheme().grayColor, fontSize: 14),
      filled: true,
      fillColor: Colors.white70,
      suffixIcon: icon == null ? null :
      Icon(icon, color: BCAppTheme().headingTextColor),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color: validityStatus ?
        BCAppTheme().headingTextColor : BCAppTheme().redColor, width: 0.5),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color:validityStatus ?
        BCAppTheme().headingTextColor : BCAppTheme().redColor, width: 0.5),
      ),
    );
  }

  /// New
  getBCoreMandatoryID({bool isMandatory, bool isValidated = false, String hintText, IconData icon,Function iconPress}) {
    return InputDecoration(
      isDense: true,
      border: OutlineInputBorder(),
      hintText: hintText,
      hintStyle: TextStyle(color: BCAppTheme().grayColor, fontSize: 14),
      filled: true,
      fillColor: Colors.white70,
      suffixIcon: icon == null ? null :
      GestureDetector(onTap: iconPress,child
          : Icon(icon, color: BCAppTheme().headingTextColor)),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color: isMandatory ? ( isValidated ?
        BCAppTheme().headingTextColor : BCAppTheme().redColor) : BCAppTheme().headingTextColor,
            width: 0.5),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color: isMandatory ? ( isValidated ?
        BCAppTheme().headingTextColor : BCAppTheme().redColor) : BCAppTheme().headingTextColor,
            width: 0.5),
      ),
    );
  }

  /// This will provide common Shape Decoration for fields with Mandatory field red color
  getBCoreSD({bool isMandatory,bool isValidated = true}){
    return ShapeDecoration(
      shape: RoundedRectangleBorder(
        side: BorderSide(
            width: 0.5,
            style: BorderStyle.solid,
            color: isMandatory ? (isValidated ? BCAppTheme().headingTextColor : BCAppTheme().redColor) : BCAppTheme().headingTextColor),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
    );
  }

  /// This decoration has specific text positioning for handling editable and non-editable text field scenario
  getBCoreInputDecorationForPopup({String labelText}) {
    return InputDecoration(
      contentPadding: EdgeInsets.only(left: 12,right: 8,bottom: 8,top: 0),
      labelText: labelText,
      hintStyle: TextStyle(color: BCAppTheme().grayColor),
      filled: true,
      fillColor: Colors.white70,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color: BCAppTheme().headingTextColor, width: 0.5),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        borderSide:
        BorderSide(color:BCAppTheme().headingTextColor, width: 0.5),
      ),
    );
  }
}

showMessage({BuildContext context, String message}){
  // Scaffold.of(context).showSnackBar(SnackBar(content: Text(message), duration: Duration(milliseconds: 500)));
  showTopSnackBar(
    context,
    CustomSnackBar.info(
      message: message,
      backgroundColor: BCAppTheme().headingTextColor,
    ),
    displayDuration: Duration(milliseconds: 500),
  );

}

showAlert({BuildContext context, String message}){
  // Scaffold.of(context).showSnackBar(SnackBar(content: Text(message), duration: Duration(milliseconds: 1000)));
  showTopSnackBar(
    context,
    CustomSnackBar.info(
      message: message,
      backgroundColor: BCAppTheme().headingTextColor,
    ),
    displayDuration: Duration(milliseconds: 1000),
  );
}

int getDeci() {
    return int.tryParse(fromAddress.curDecimPlace ?? "2") ?? 2;
}

class Item {
  final String name;
  final String image;
  final String type;

  Item(this.name, this.image, this.type);
}

enum EditMode { On, Off }

enum ItemStatus { None, Active, Discontinued, DeletedItem, BlockedItem } /// There is no item status as None. We have added the same for easy coding

enum ProductStockType { None, Regular, Expiry, Serialized } /// There is no product stock type as None. We have added the same for easy coding

enum InputType { Barcode, ItemCode }

extension intToBool on int {
  bool toBool() {
    return (this == 1);
  }
}

extension boolToInt on bool {
  int toInt() {
    return this ? 1 : 0 ;
  }
}

/// Types with extension method for getting the values
enum GRNType { Direct, LPO, IPO }

extension GRNTypeVal on GRNType {
  String get value => this.toString().split('.').last;
}

enum PRType { Direct, GRN }

extension PRTypeVal on PRType {
  String get value => this.toString().split('.').last;
}

enum DeliveryType { Full, Partial }

extension DeliveryTypeVal on DeliveryType {
  String get value => this.toString().split('.').last;
}

enum SalesType { Credit,WalkInClient }

extension SalesTypeVal on SalesType {
  String get value => this.toString().split('.').last;
}

enum StockType { BatchWise, StockWise }

extension StockTypeVal on StockType {
  String get value => this.toString().split('.').last;
}

extension DoubleRounding on String {
  double toDouble({int pos = 0}) {
    if (pos == 0){
      pos = getDeci();
    }
    return double.parse(double.parse(this ?? "0.0").toStringAsFixed(pos));
  }
}

extension DoubleRoundoff on double {
  double trim({int pos = 0}) {
    if (pos == 0){
      pos = getDeci();
    }
    return double.parse((this ?? 0.0).toStringAsFixed(pos));
  }
}

enum TransGenType { NONE, MR, PR, PO }

extension TransGenTypeVal on TransGenType {
  String get value => this.toString().split('.').last;
}

enum AccessType { All, UserBased }

enum SIType { Direct, From_MR }

extension SITypeVal on SIType {
  String get value => this.toString().split('.').last;
}

enum UpdatingField { All, UP, DP, DA, TA}