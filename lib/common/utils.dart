import 'package:intl/intl.dart';

class Utils {

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }

  static String getDateByFormat({DateTime passedDateTime, String format}) {
    final DateFormat formatter = DateFormat(format);
    final String formatted = formatter.format(passedDateTime);
    return formatted;
  }

  static String getCurrentDateByFormat({String format}) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat(format);
    final String formatted = formatter.format(now);
    return formatted;
  }

  static getUniqueId(){
    itemIndex++;
    return "item$itemIndex";
  }

  static resetUniqueId(){
    itemIndex = -1;
  }
  //
//  static String getActualPrice({String unitPrice, String quantity}) {
//    return (double.parse(unitPrice) * double.parse(quantity)).toString();
//  }

//  static String getDiscount({String actualPrice, String discountPercentage}) {
//    return (double.parse(actualPrice) * double.parse(discountPercentage)).toString();
//  }
}

int itemIndex = -1;