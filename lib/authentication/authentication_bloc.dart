import 'dart:async';
import 'package:bloc/bloc.dart';
import 'authentication.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {

  AuthenticationBloc()
      : super(AuthenticationInitial());

  //final SPRepository spRepository;

/*  AuthenticationBloc({@required this.spRepository})
      : assert(spRepository != null),
        super(AuthenticationInitial());*/

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStarted) {
      final bool hasToken = false;//await spRepository.hasToken();

      if (hasToken) {
        yield AuthenticationSuccess();
      } else {
        yield AuthenticationFailure();
      }
    }

    if (event is AuthenticationLoggedIn) {
      yield AuthenticationInProgress();
     // await spRepository.persistToken(event.token);
      yield AuthenticationSuccess();
    }

    if (event is AuthenticationLoggedOut) {
      yield AuthenticationInProgress();
    //  await spRepository.deleteToken();
      yield AuthenticationFailure();
    }
  }
}
